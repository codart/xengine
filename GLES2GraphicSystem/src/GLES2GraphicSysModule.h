#pragma once

#include "x3d/PlugableModule.h"

_X3D_NS_BEGIN

class GLES2GraphicSystem;

class GLES2GraphicSysModule : public PlugableModule
{
public:
	GLES2GraphicSysModule(void);
	virtual ~GLES2GraphicSysModule(void);

public:
	override void
		Install();
	override void
		Uninstall();


protected:
	GLES2GraphicSystem*
		_pGraphicSystem;

};

_X3D_NS_END