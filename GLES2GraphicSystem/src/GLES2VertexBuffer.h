#pragma once

#include "x3d/graphic/VertexBuffer.h"

_X3D_NS_BEGIN

class GLES2VertexBuffer : public VertexBuffer
{
	friend class GLES2GraphicSystem;

public:
	GLES2VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic = FALSE);
	~GLES2VertexBuffer(void);

public:
	override void 
		WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer = false);

	override void*
		Lock(UINT OffsetToLock, UINT nByteCount, LockOptions dwLockOptions);
	override void 
		Unlock();


package:
	void 
		internalInit();
	void 
		internalReleaseAll();
	INLINE GLuint
		getGLES2VertexBuffer() { return __iOGLVertexBuffer; };


protected:
	override InternalObjectHandle
		_getInternalObjHandle();

	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);


private:
	GLuint
		__iOGLVertexBuffer;


};

_X3D_NS_END