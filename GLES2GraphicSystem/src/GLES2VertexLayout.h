#pragma once

#include "x3d/graphic/VertexLayout.h"


_X3D_NS_BEGIN

class GLES2VertexLayout : public VertexLayout
{
public:
	GLES2VertexLayout(GraphicSystem* pGraphicSystem);
	~GLES2VertexLayout(void);


public:
	override void
		BeginUpdate();
	override void 
		EndUpdate();


package:
	// ---


protected:


protected:


};

_X3D_NS_END