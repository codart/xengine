#pragma once

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

class _D3D9TextureBaseImpl;
class D3D9Texture;
class D3D9DepthStencilBuffer;


/*

RenderTaget(two surface)		FrameBuffer(two render buffer)

VertexBuffer					VBO
								
RenderTarget Surface			RenderBuffer


When to use renderbuffer object?

1. Certain image formats do not support texturing, such as stencil index values. A renderbuffer must be used instead.
								
2. If the image will not be used as a texture, using a renderbuffer may
have a performance advantage. This is because the implementation
might be able to store the renderbuffer in a much more efficient format,
better suited for rendering than for texturing. The implementation
can only do so, however, if it knows in advance that the image
will not be used as a texture.								

*/

class GLES2GraphicSystem :  public GraphicSystem
{
public:
	GLES2GraphicSystem(void);
	~GLES2GraphicSystem(void);


public:
	override void 
		Init(DWORD_PTR dwInitData);
	override void 
		Uninit();

	override VertexLayout* 
		CreateVertexLayout();

	override VertexBuffer* 
		CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic = FALSE);

	/*
	override BOOL
		IsReady();

	// Factory methods
	override VertexProgram*
		CreateVertexProgram();
	override FragmentProgram*
		CreateFragmentProgram();

	override WindowRenderTarget* 
		CreateWindowRenderTarget(Window* pWindow);
	override IndexBuffer* 
		CreateIndexBuffer(UINT nIndexCount, UINT nIndexSize, BOOL bDynamic = FALSE);

	override RenderTexture*
		CreateRenderTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat
							, PixelFormat depthStencilFormat);
	override DynamicTexture* 
		CreateDynamicTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat);
	virtual PixelBuffer*
		CreateDepthStencilBuffer(int nWidth, int nHeight, PixelFormat pixelFormat = PIXEL_FORMAT_D24S8);
	
	override INLINE void 
		SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	override void
		SetTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	override void
		SetTextureState(UINT texUnitIndex, TextureState* pTextureState);
	override void 
		SetTexture(UINT texUnitIndex, Texture* pTexture);

	override void 
		SetVertexTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	override void 
		SetVertexTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	override Texture* 
		CreateTexture(TextureType textureType, const String& strName);

	// ---
	override void 
		ClearFrameBuffer(DWORD dwBufferFlag = FRAME_BUFFER_COLOR | FRAME_BUFFER_DEPTH | FRAME_BUFFER_STENCIL
						, DWORD color = COLOR_ARGB(0, 0, 0, 0)
						, Real depth = 1.0f
						, unsigned short stencil = 0);


	INLINE override void
		SetVertexLayout(VertexLayout* pVertexDeclearation);

	override void
		SetViewport(const ViewportInfo* pViewportInfo);


	override BOOL
		SetRenderTarget(RenderTarget* pRenderTarget);
	override BOOL
		SetMultiRenderTarget(MultiRenderTarget* pMultiRenderTarget);
	virtual BOOL
		SetColorBuffer(int iBufferIndex, PixelBuffer* pColorBuffer);
	virtual BOOL
		SetDepthStencilBuffer(PixelBuffer* pColorBuffer);


	override DWORD
		BeginDrawing();
	override DWORD
		EndDrawing();

	override void
		BeginUpdateWindow(WindowRenderTarget* pWndTarget);
	override void
		EndUpdateWindow();

	override DWORD
		DrawPrimitive(const DrawPrimitiveCommand* pRenderOperation);

	INLINE override DWORD 
		SetVertexBuffer(int iStreamIndex, VertexBuffer* pVertexBuffer);

	INLINE override void
		SetVertexBuffers(const VertexBufferPtrList& vertexBuffers);
	INLINE override DWORD
		SetIndexBuffer(IndexBuffer* pIndexBuffer);

	override DWORD
		DrawPrimitive(DrawPrimitiveType primitiveType, int nVertexCount, int iStartIndex, int nPrimitiveCount);

	override void
		SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha);
	override void
		SetDepthBufferParams(BOOL bDepthTest, BOOL bDepthWrite
							, CompareFunction depthFunction = COMPARE_FUNC_LESS_EQUAL);
	override void
		SetStencilBufferParams(BOOL bEnableStencil = TRUE, CompareFunction func = COMPARE_FUNC_ALWAYS_PASS, 
								UINT refValue = 0, UINT mask = 0xFFFFFFFF, 
								StencilOperation stencilFailOp = STENCIL_OP_KEEP, 
								StencilOperation depthFailOp = STENCIL_OP_KEEP,
								StencilOperation stencilAndZPassOp = STENCIL_OP_KEEP, 
								BOOL bTwoSidedOperation = FALSE);

	override void 
		EnableColorBufferWrite(BOOL bEnable);

	override void 
		SetSceneBlending(FrameBlendFactor sourceFactor
						, FrameBlendFactor destFactor
						, FrameBlendOperation blendOp);

	override void
		SetAlphaTestParams(BOOL bEnable, CompareFunction func, BYTE value, BOOL alphaToCoverage = FALSE);

	override void 
		SetShader(Shader* pShader);
	override void 
		SetCullMode(HardwareCullMode cullMode);
	override void 
		SetClipPlane(UINT iIndex, BOOL bEnable = TRUE, const Plane& clipPlane = Plane(0, 1, 0, 0));

		*/


package:


protected:
	EGLDisplay 
		_eglDisplay;

	/// 要实现多窗口，则需要为每个窗口创建EGLContext和EGLSurface，然后用wglMakeCurrent切换渲染。
	/// 由于目前GLES2在Windows上是基于AngleProject的，该项目底层对多个context可能有bug，所以暂时
	/// OpenGL ES2不对多窗口进行支持。
	EGLContext 
		_eglContext;

	EGLSurface
		_eglSurface;


private:


};


_X3D_NS_END