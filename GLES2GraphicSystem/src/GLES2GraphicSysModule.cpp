#include "StdAfx.h"
#include "GLES2GraphicSysModule.h"

#include "GLES2GraphicSystem.h"

_X3D_NS_BEGIN

GLES2GraphicSysModule::GLES2GraphicSysModule(void)
{
	_pGraphicSystem = NULL;
}

GLES2GraphicSysModule::~GLES2GraphicSysModule(void)
{
}

void GLES2GraphicSysModule::Install()
{
	_pGraphicSystem = XNEW GLES2GraphicSystem;
	GetX3DModule()->installGraphicSystem(_pGraphicSystem);
}

void GLES2GraphicSysModule::Uninstall()
{
	GraphicSystem* pGraphicSystem = GetX3DModule()->uninstallGraphicSystem();
	XASSERT(_pGraphicSystem == pGraphicSystem);
	SAFE_CAST(pGraphicSystem, GLES2GraphicSystem*);
	SAFE_DELETE(pGraphicSystem);
}

_X3D_NS_END