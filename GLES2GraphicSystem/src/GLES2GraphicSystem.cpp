#include "StdAfx.h"
#include "GLES2GraphicSystem.h"

#include "x3d/graphic/GraphicSystemCap.h"

#include "X3D/graphic/DeviceEvent.h"
#include "x3d/resource/VertexData.h"
#include "x3d/graphic/MultiRenderTarget.h"

#include "x3d/resource/IndexData.h"
#include "x3d/material/TextureUnitState.h"
#include "x3d/material/Shader.h"
#include "x3d/graphic/DrawPrimitiveCommand.h"

#include "GLES2VertexLayout.h"
#include "GLES2VertexBuffer.h"

_X3D_NS_BEGIN

GLES2GraphicSystem::GLES2GraphicSystem(void)
{
	_eglDisplay = NULL;
}

GLES2GraphicSystem::~GLES2GraphicSystem(void)
{

}

void GLES2GraphicSystem::Init(DWORD_PTR dwInitData)
{
	GraphicSystem::Init(dwInitData);

	HWND hWnd = HWND(dwInitData);

	BOOL bCreateTempWindow = false;
	if(!hWnd)
	{
		bCreateTempWindow = true;
		hWnd = ::CreateWindowEx(0, TEXT("Button"), NULL, 0, 0, 0, 10, 10, NULL, 0
			, (HINSTANCE)GetX3DModule()->GetModuleHandle(), 0);
	}

	_eglDisplay = eglGetDisplay(GetDC(hWnd));
	if (_eglDisplay == EGL_NO_DISPLAY) { XTHROWEX("eglGetDisplay failed!"); }

	// Initialize EGL
	EGLint majorVersion, minorVersion;
	if (!eglInitialize(_eglDisplay, &majorVersion, &minorVersion)) XTHROWEX("eglInitialize failed!");

	EGLConfig config = NULL;
	EGLint numConfigs = 0;

	if (!eglGetConfigs(_eglDisplay, NULL, 0, &numConfigs) ) XTHROWEX("eglGetConfigs failed!");

/*
	EGLint configAttribList[] =
	{
		EGL_RED_SIZE,       5,
		EGL_GREEN_SIZE,     6,
		EGL_BLUE_SIZE,      5,
		EGL_ALPHA_SIZE,     (flags & ES_WINDOW_ALPHA) ? 8 : EGL_DONT_CARE,
		EGL_DEPTH_SIZE,     (flags & ES_WINDOW_DEPTH) ? 8 : EGL_DONT_CARE,
		EGL_STENCIL_SIZE,   (flags & ES_WINDOW_STENCIL) ? 8 : EGL_DONT_CARE,
		EGL_SAMPLE_BUFFERS, (flags & ES_WINDOW_MULTISAMPLE) ? 1 : 0,
		EGL_NONE
	};
*/
	// Choose config
	if (!eglChooseConfig(_eglDisplay, /*configAttribList*/ NULL, &config, 1, &numConfigs))
	{
		XTHROWEX("eglChooseConfig failed!");
	}

	// Create a GL context
	EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE, EGL_NONE };
	_eglContext = eglCreateContext(_eglDisplay, config, EGL_NO_CONTEXT, contextAttribs);
	if ( _eglContext == EGL_NO_CONTEXT )
	{
		XTHROWEX("eglCreateContext failed!");
	}   

	// Create a surface

	EGLint surfaceAttribList[] =
	{
		EGL_POST_SUB_BUFFER_SUPPORTED_NV, EGL_FALSE,
		EGL_NONE, EGL_NONE
	};
	_eglSurface = eglCreateWindowSurface(_eglDisplay, config, (EGLNativeWindowType)hWnd, surfaceAttribList);
	if ( _eglSurface == EGL_NO_SURFACE )
	{
		 XTHROWEX("eglInitialize failed!");
	}

	// Make the context current
	if ( !eglMakeCurrent(_eglDisplay, _eglSurface, _eglSurface, _eglContext) )
	{
		 XTHROWEX("eglInitialize failed!");
	}

}

void GLES2GraphicSystem::Uninit()
{


}

VertexLayout* GLES2GraphicSystem::CreateVertexLayout()
{
	GLES2VertexLayout* pVertexLayout = XNEW GLES2VertexLayout(this);
	return pVertexLayout;
}

VertexBuffer* GLES2GraphicSystem::CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
{
	GLES2VertexBuffer* pVertexBuffer = XNEW GLES2VertexBuffer(this, nVertexCount, nVertexSize, bDynamic);
	pVertexBuffer->internalInit();

	// If in default pool, should listen DEVICE_LOST/DEVICE_RESET event.
	if(bDynamic)
	{
		AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&GLES2VertexBuffer::_onDeviceLost, pVertexBuffer));
		AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&GLES2VertexBuffer::_onDeviceReset, pVertexBuffer));
	}

	return pVertexBuffer;
}


/*

BOOL GLES2GraphicSystem::IsReady()
{
	

	return FALSE;
}

void GLES2GraphicSystem::ClearFrameBuffer(DWORD dwBufferFlag
									 , DWORD color
									 , Real depth
									 , unsigned short stencil)
{

}

void GLES2GraphicSystem::SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha)
{

}

void GLES2GraphicSystem::EnableColorBufferWrite(BOOL bEnable)
{

}

void GLES2GraphicSystem::SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState)
{
}

*/

_X3D_NS_END