#include "x3d/PlugableModule.h"

#if defined(GLES2_EXPORT)
#define _XGLES2GSExport __declspec(dllexport)
#else
#define _XGLES2GSExport __declspec(dllimport)
#endif


extern "C"
{
	 _XGLES2GSExport x3d::PlugableModule* GetPlugableModule();
}
