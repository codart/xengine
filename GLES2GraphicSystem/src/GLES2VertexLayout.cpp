#include "StdAfx.h"
#include "GLES2VertexLayout.h"

#include "GLES2GraphicSystem.h"


_X3D_NS_BEGIN

GLES2VertexLayout::GLES2VertexLayout(GraphicSystem* pGraphicSystem)
: VertexLayout(pGraphicSystem)
{

}

GLES2VertexLayout::~GLES2VertexLayout(void)
{

}

void GLES2VertexLayout::BeginUpdate()
{
	VertexLayout::BeginUpdate();
}

void GLES2VertexLayout::EndUpdate()
{
	VertexLayout::EndUpdate();
}


_X3D_NS_END