#include "StdAfx.h"
#include "GLES2VertexBuffer.h"

#include "GLES2GraphicSystem.h"

_X3D_NS_BEGIN

GLES2VertexBuffer::GLES2VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
: VertexBuffer(pGraphicSystem, nVertexCount, nVertexSize, bDynamic)
{
	XASSERT(pGraphicSystem && nVertexCount >= 0 && nVertexSize >= 0);
	__iOGLVertexBuffer = 0;
}

GLES2VertexBuffer::~GLES2VertexBuffer(void)
{
	internalReleaseAll();
}

void GLES2VertexBuffer::internalInit()
{
	if(_nVertexCount == 0) return;

	glGenBuffers(1, &__iOGLVertexBuffer);
	if(glGetError() != GL_NO_ERROR)
	{
		XTHROWEX("GLES2VertexBuffer::internalInit: glGenBuffers() failed!!!");
	}

	glBindBuffer(GL_ARRAY_BUFFER, __iOGLVertexBuffer);

	UINT nSizeInBytes = _nVertexCount * _nVertexSize;
	GLenum dwUsage = _bDynamic? GL_DYNAMIC_DRAW : GL_STATIC_DRAW;
	glBufferData(GL_ARRAY_BUFFER, nSizeInBytes, NULL, dwUsage);

	glBindBuffer(GL_ARRAY_BUFFER, NULL);
	
	debug_check(bufferState == GBS_UNINIT);
	debug_assign(bufferState, GBS_INITED);
}

void GLES2VertexBuffer::internalReleaseAll()
{
	if(__iOGLVertexBuffer != 0)
	{
		glDeleteBuffers(1, &__iOGLVertexBuffer);
		__iOGLVertexBuffer = 0;
	}

	debug_assign(bufferState, GBS_UNINIT);
}

void GLES2VertexBuffer::_onDeviceLost(const Event* pEvent)
{
	//...internalReleaseAll();
}

void GLES2VertexBuffer::_onDeviceReset(const Event* pEvent)
{
	//...internalInit();
}

void GLES2VertexBuffer::WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer)
{
	debug_check(bufferState == GBS_INITED || bufferState == GBS_HAS_VALID_DATA);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	//glBufferSubData();
	//glMapBufferOES(0, 0);
	
	//XASSERT(hRet == D3D_OK);
}

void* GLES2VertexBuffer::Lock(UINT offsetToLock, UINT nByteCount, LockOptions dwLockOptions)
{
	debug_check(bufferState == GBS_INITED || bufferState == GBS_HAS_VALID_DATA);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	debug_check(bLocked == FALSE);
	debug_assign(bLocked, TRUE);

	BYTE* pBuffer = NULL;
	//HRESULT hRet = __pOGLVertexBuffer->Lock(offsetToLock, nByteCount, (void**)&pBuffer, dwLockOptions);
	//XASSERT(hRet == D3D_OK);
	
	return pBuffer;
}

void GLES2VertexBuffer::Unlock()
{
	debug_check(bLocked == TRUE);


	debug_assign(bLocked, FALSE);
}

InternalObjectHandle GLES2VertexBuffer::_getInternalObjHandle()
{
	return (InternalObjectHandle)__iOGLVertexBuffer;
}


_X3D_NS_END