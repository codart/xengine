#XEngine
业余时间开发的一个游戏引擎。
实现功能包括：
1. 渲染pipeline，支持forward rendering和defered rendering（只实现了基本的功能），或任意自定义渲染流程。
2. 支持PostProcess chain, 支持ShadowMap
3. 基于八叉树的场景管理
4. 材质系统
5. 简单的Max导出插件及导出格式
6. 可编辑网格（EditableMesh），可以用来实现Procedural Mesh 
7. 硬件蒙皮骨骼动画（只完成了30%），基于Command的简单几何图形渲染（10%）
8. 2D粒子系统，2D Grid渲染（可以用来实现一些2D特效）


注意：本项目已经停止开发，所有代码仅供参考。由于未进过严格单元测试，请勿直接用于正式产品。
