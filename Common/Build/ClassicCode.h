#pragma once

class ClassicCode
{
public:
	ClassicCode(void);
	virtual ~ClassicCode(void);


public:
	/// Duff's Device
	/// Unroll loop
	void DuffDevice()
	{
		register short *to, *from;
		to = new short;
		*to = 4;

		from = new short[100];
		memset((char*)from, 3, sizeof(short) * 100);

		register int count = 100;
		{
			register int n = (count + 7) / 8;
			switch(count % 8) {
			case 0:	do {*to = *from++;
			case 7:		*to = *from++;
			case 6:		*to = *from++;
			case 5:		*to = *from++;
			case 4:		*to = *from++;
			case 3:		*to = *from++;
			case 2:		*to = *from++;
			case 1:		*to = *from++;
				} while(--n > 0);
			}
		}

	}

};
