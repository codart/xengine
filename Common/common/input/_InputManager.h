#pragma once


#include "common/core/Singleton.h"
#include "common/input/InputObject.h"

_COMMON_NS_BEGIN


/*
	InputManager managers all input object such as keyboard/mouse/joystick. 
	Only the stage can create input object, and other object should listen stage's 
	input events(keyboard/mouse/joystick events).
*/
class _CommonExport InputManager : public Singleton<InputManager, AutoRelease>
{
public:
	InputManager(void);
	virtual ~InputManager(void);

public:
	void 
		Init(DWORD hModuleHandler);
	void 
		UnInit();
	INLINE void 
		Tick()
	{
		// Tick all input object
		FASTEST_VECTOR_ITERATE(InputObject*, _inputObjectList);
			CURRENT_DATA(_inputObjectList)->Tick();
			GET_NEXT(_inputObjectList);
		FASTEST_ITERATE_END();
	}

	InputObject* 
		AddInputObject(InputObjectType type, DWORD_PTR hWnd);
	void
		RemoveInputObject(InputObject* pInputObject); 


protected:
	typedef std::vector<InputObject*> InputObjectList;
	typedef InputObjectList::iterator InputObjectListIt;

	InputObjectList
		_inputObjectList;

private:
	LPDIRECTINPUT8
		__pDirectInput8; 


};


_COMMON_NS_END