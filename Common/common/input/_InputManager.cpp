#include "StdAfx.h"
#include "_InputManager.h"
#include "Keyboard.h"
#include "Mouse.h"
#include <algorithm>

_COMMON_NS_BEGIN

InputManager::InputManager(void) 
{
	__pDirectInput8 = NULL; 
}

InputManager::~InputManager(void)
{
}

void InputManager::Init(DWORD hModuleHandler)
{
	HINSTANCE hModule = (HINSTANCE)hModuleHandler;
	HRESULT hr = DirectInput8Create(hModule, DIRECTINPUT_VERSION, 
								IID_IDirectInput8, (void**)&__pDirectInput8, NULL); 

	if FAILED(hr) XTHROWEX("Create DirectInput8 failed!!!");
}

void InputManager::UnInit()
{
	if (!__pDirectInput8) return;

	InputObjectListIt it = _inputObjectList.begin();
	for ( ; it != _inputObjectList.end(); ++it)
	{	
		InputObject* pInputObject = *it;
		if (pInputObject->__pDXInputDevice) 
		{ 
			// Always unacquire device before calling Release(). 
			pInputObject->__pDXInputDevice->Unacquire(); 
			SAFE_RELEASE(pInputObject->__pDXInputDevice);
		} 

		delete pInputObject;
	}

	SAFE_RELEASE(__pDirectInput8);
}

InputObject* InputManager::AddInputObject(InputObjectType type, DWORD_PTR hWnd)
{
	InputObject* pInputObject = NULL;

	switch (type)
	{
	case INPUT_OBJ_KEYBOARD:
		pInputObject = new Keyboard;
		break;

	case INPUT_OBJ_MOSUE:
		pInputObject = new Mouse;
		break;
	}

	pInputObject->Init((DWORD_PTR)__pDirectInput8, hWnd);
	_inputObjectList.push_back(pInputObject);

	return pInputObject;
}

void InputManager::RemoveInputObject(InputObject* pInputObject)
{
	InputObjectListIt it = find(_inputObjectList.begin(), _inputObjectList.end(), pInputObject);
	XASSERT(it != _inputObjectList.end());

	if (pInputObject->__pDXInputDevice) 
	{ 
		// Always unacquire device before calling Release(). 
		pInputObject->__pDXInputDevice->Unacquire(); 
		SAFE_RELEASE(pInputObject->__pDXInputDevice);
	} 

	delete pInputObject;
	_inputObjectList.erase(it);
}

_COMMON_NS_END