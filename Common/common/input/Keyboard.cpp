#include "StdAfx.h"
#include "Keyboard.h"
#include "common/util/Unicode.h"

_COMMON_NS_BEGIN

#define KEYBOARD_DX_BUFFERSIZE 8

Keyboard::Keyboard() 
{
	memset(&_keyBuffer, 0, 256 );
	__deadKey = '\0';
	_bKeyboardStateUpdated = false;
}

Keyboard::~Keyboard(void)
{
}

void Keyboard::Init(DWORD pData, DWORD_PTR hWnd)
{
	LPDIRECTINPUT8 __pDirectInput8 = LPDIRECTINPUT8(pData);
	HRESULT hr = __pDirectInput8->CreateDevice(GUID_SysKeyboard, &__pDXInputDevice, NULL); 
	if FAILED(hr) XTHROWEX("Create keyboard device failed!!!"); 

	if(FAILED(__pDXInputDevice->SetDataFormat(&c_dfDIKeyboard)))
		XTHROWEX("Set keyboard format failed!!!"); 

	if(FAILED(__pDXInputDevice->SetCooperativeLevel((HWND)hWnd, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE)))
		XTHROWEX("Set cooperative level failed!");

	DIPROPDWORD dipdw;
	dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj        = 0;
	dipdw.diph.dwHow        = DIPH_DEVICE;
	dipdw.dwData            = KEYBOARD_DX_BUFFERSIZE;

	if (FAILED(__pDXInputDevice->SetProperty( DIPROP_BUFFERSIZE, &dipdw.diph )))
		XTHROWEX("Set buffer failed!");
}

void Keyboard::Tick()
{
	_bKeyboardStateUpdated = false;

	DIDEVICEOBJECTDATA diBuff[KEYBOARD_DX_BUFFERSIZE];
	DWORD entries = KEYBOARD_DX_BUFFERSIZE;

	HRESULT hr = __pDXInputDevice->GetDeviceData( sizeof(DIDEVICEOBJECTDATA), diBuff, &entries, 0 );
	if( hr != DI_OK )
	{
		hr = __pDXInputDevice->Acquire();
		while( hr == DIERR_INPUTLOST )
			hr = __pDXInputDevice->Acquire();
		return;
	}

	if(FAILED(hr)) XTHROWEX("Get Keyboard device data failed!!!");

	//Update keyboard and modifier states.. And, if listener, fire events
	for(unsigned int i = 0; i < entries; ++i )
	{
		//If the listener returns false, that means that we are probably deleted...
		//send no more events and just leave as the this pointer is invalid now...
		bool ret = true;
		KeyCode kc = (KeyCode)diBuff[i].dwOfs;

		if( diBuff[i].dwData & 0x80 )
		{
			KeyboardEvent event(KeyboardEvent::KEY_DOWN);
			event.keyCode = kc;
			
			DispatchEvent(event);

		}
		else
		{
			//Fire off event
			KeyboardEvent event(KeyboardEvent::KEY_UP);
			event.keyCode = kc;

			DispatchEvent(event);
		}

	}

}

BOOL Keyboard::IsKeyDown(KeyCode keyCode)
{
	if(!_bKeyboardStateUpdated)
	{
		_updateKeyboardState();
	}

	return ((_keyBuffer[keyCode] & 0x80) != 0);
}

void Keyboard::_updateKeyboardState()
{
	_bKeyboardStateUpdated = true;

	HRESULT hr;
	hr = __pDXInputDevice->GetDeviceState( sizeof(_keyBuffer), &_keyBuffer );

	if( hr == DIERR_INPUTLOST || hr == DIERR_NOTACQUIRED )
	{
		hr = __pDXInputDevice->Acquire();
		if (hr != DIERR_OTHERAPPHASPRIO)
			__pDXInputDevice->GetDeviceState(sizeof(__pDXInputDevice), &__pDXInputDevice);
	}
}

WCHAR Keyboard::ToChar(KeyCode keyCode)
{
	BYTE keyState[256];
	HKL  layout = GetKeyboardLayout(0);
	if( GetKeyboardState(keyState) == 0 )
		return 0;

	unsigned int vk = MapVirtualKeyEx(keyCode, 3, layout);
	if( vk == 0 )
		return 0;

	unsigned char buff[3] = {0,0,0};
	int ascii = ToAsciiEx(vk, keyCode, keyState, (LPWORD) buff, 0, layout);
	//WCHAR wide[3];
	//int ascii = ToUnicodeEx(vk, kc, keyState, wide, 3, 0, layout);
	if(ascii == 1 && __deadKey != '\0' )
	{
		// A dead key is stored and we have just converted a character key
		// Combine the two into a single character
		WCHAR wcBuff[3] = {buff[0], __deadKey, '\0'};
		WCHAR out[3];

		__deadKey = '\0';
		if(FoldStringW(MAP_PRECOMPOSED, (LPWSTR)wcBuff, 3, (LPWSTR)out, 3))
			return out[0];
	}
	else if (ascii == 1)
	{	// We have a single character
		__deadKey = '\0';
		return buff[0];
	}
	else if(ascii == 2)
	{	// Convert a non-combining diacritical mark into a combining diacritical mark
		// Combining versions range from 0x300 to 0x36F; only 5 (for French) have been mapped below
		// http://www.fileformat.info/info/unicode/block/combining_diacritical_marks/images.htm
		switch(buff[0])	{
		case 0x5E: // Circumflex accent: ?
			__deadKey = 0x302; break;
		case 0x60: // Grave accent: ?
			__deadKey = 0x300; break;
		case 0xA8: // Diaeresis: ?
			__deadKey = 0x308; break;
		case 0xB4: // Acute accent: ?
			__deadKey = 0x301; break;
		case 0xB8: // Cedilla: ?
			__deadKey = 0x327; break;
		default:
			__deadKey = buff[0]; break;
		}
	}

	return 0;
}

String Keyboard::ToString(KeyCode keyCode)
{
	String strKeyString;

	DIPROPSTRING prop;
	prop.diph.dwSize = sizeof(DIPROPSTRING);
	prop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	prop.diph.dwObj = static_cast<DWORD>(keyCode);
	prop.diph.dwHow = DIPH_BYOFFSET;

	if ( SUCCEEDED( __pDXInputDevice->GetProperty( DIPROP_KEYNAME, &prop.diph ) ) )
	{
		USING_STRING_CONVERT;
		return strKeyString = WStrToStr(prop.wsz);
	}

	StringStream ss;
	ss << TEXT("Key_") << (int)keyCode;
	return ss.str();
}

_COMMON_NS_END