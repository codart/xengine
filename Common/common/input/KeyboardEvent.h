#pragma once

#include "common/core/Event.h"
#include "common/input/keyCode.h"

_COMMON_NS_BEGIN

class _CommonExport KeyboardEvent : public Event
{
public:
	enum KeyboardEventType
	{
		KEY_EVENT_BASE = 200,
		KEY_DOWN,
		KEY_UP,

		KEY_EVENT_END,
	};

	KeyboardEvent(int nType);

	static BOOL IsValidEvent(int nType)
	{
		return nType > KEY_EVENT_BASE && nType < KEY_EVENT_END;
	}

public:
	KeyCode keyCode;

};

_COMMON_NS_END