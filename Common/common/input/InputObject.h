#pragma once

#define DIRECTINPUT_VERSION 0x0800

#include "initguid.h"
#include "Dinput.h"
#include "common/core/EventDispatcher.h"

#pragma comment(lib, "dinput8.lib") 

_COMMON_NS_BEGIN

enum InputObjectType
{
	INPUT_OBJ_MOSUE,
	INPUT_OBJ_KEYBOARD,
	INPUT_OBJ_JOYSTICK,
};


class _CommonExport InputObject : public EventDispatcher
{
	friend class InputManager;

protected:
	InputObject();
	virtual ~InputObject(void);

protected:
	virtual void
		Init(DWORD pData, DWORD_PTR hWnd){XASSERT(0);};
	virtual void 
		Tick(){XASSERT(0);};


protected:
	LPDIRECTINPUTDEVICE8  
		__pDXInputDevice;

};


_COMMON_NS_END