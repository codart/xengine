#include "StdAfx.h"
#include "Mouse.h"
#include "common/input/MouseEvent.h"

_COMMON_NS_BEGIN

#define MOUSE_DX_BUFFERSIZE 64

Mouse::Mouse(void)
{
	_dwButtonsBuffer = 0;
	__hWnd = NULL;
	mouseX = mouseY = 0;

	__bMousePositionUpdated = FALSE;
}

Mouse::~Mouse(void)
{
}

String Mouse::ToString(MouseButtonType button)
{
	switch (button)
	{
	case MOUSE_LEFT:
		return String(TEXT("Left Button"));
		break;
	case MOUSE_RIGHT:
		return String(TEXT("Right Button"));
		break;
	case MOUSE_MIDDLE:
		return String(TEXT("Middle Button"));
		break;
	}
	return String(TEXT("Unknown button"));
}

void Mouse::Init(DWORD pData, DWORD_PTR hWnd)
{
	__hWnd = (HWND)hWnd;
	LPDIRECTINPUT8 __pDirectInput8 = LPDIRECTINPUT8(pData);
	HRESULT hr = __pDirectInput8->CreateDevice(GUID_SysMouse, &__pDXInputDevice, NULL); 
	if FAILED(hr) XTHROWEX("Create mouse device failed!!!"); 

	if( FAILED(__pDXInputDevice->SetDataFormat(&c_dfDIMouse2)) )
		XTHROWEX("Set mouse data format failed!!!");


	if( FAILED(__pDXInputDevice->SetCooperativeLevel((HWND)hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)) )
		XTHROWEX("Failed to set mouse coop level");

	DIPROPDWORD dipdw;

	dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj        = 0;
	dipdw.diph.dwHow        = DIPH_DEVICE;
	dipdw.dwData            = MOUSE_DX_BUFFERSIZE;

	if(FAILED(__pDXInputDevice->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph )))
		XTHROWEX("Failed to set property");

	hr = __pDXInputDevice->Acquire();
	if (FAILED(hr) && hr != DIERR_OTHERAPPHASPRIO)
		XTHROWEX("Failed to aquire mouse!" );
}

BOOL Mouse::IsButtonDown(MouseButtonType button)
{
	DIMOUSESTATE2 mouseState;
	ZeroMemory(&mouseState,sizeof(mouseState));
	HRESULT hr;
	hr = __pDXInputDevice->GetDeviceState(sizeof(DIMOUSESTATE2), (LPVOID)&mouseState);

	if( hr == DIERR_INPUTLOST || hr == DIERR_NOTACQUIRED )
	{
		hr = __pDXInputDevice->Acquire();
		if (hr != DIERR_OTHERAPPHASPRIO)
		{
			__pDXInputDevice->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mouseState);
		}
	}

	return mouseState.rgbButtons[button] & 0x80;
}

void Mouse::_doMouseClick( int mouseButton, DIDEVICEOBJECTDATA& di )
{
	__updateMousePosition();

	if( di.dwData & 0x80 )
	{
		_dwButtonsBuffer |= 1 << mouseButton; //turn the bit flag on

		MouseEvent e(MouseEvent::MOUSE_DOWN);
		e.mouseX = mouseX;
		e.mouseY = mouseY;
		e.button = (MouseButtonType)mouseButton;
		DispatchEvent(e);
	}
	else
	{
		_dwButtonsBuffer &= ~(1 << mouseButton); //turn the bit flag off

		MouseEvent e(MouseEvent::MOUSE_UP);
		e.mouseX = mouseX;
		e.mouseY = mouseY;
		e.button = (MouseButtonType)mouseButton;
		DispatchEvent(e);
	}
}

void Mouse::Tick()
{
	__bMousePositionUpdated = FALSE;

	//Clear old relative values
	deltaX = deltaY = deltaScroll = 0;

	DIDEVICEOBJECTDATA diBuff[MOUSE_DX_BUFFERSIZE];
	DWORD entries = MOUSE_DX_BUFFERSIZE;

	HRESULT hr = __pDXInputDevice->GetDeviceData( sizeof(DIDEVICEOBJECTDATA), diBuff, &entries, 0 );
	if( hr != DI_OK )
	{
		hr = __pDXInputDevice->Acquire();
		while( hr == DIERR_INPUTLOST ) 
			hr = __pDXInputDevice->Acquire();

		hr = __pDXInputDevice->GetDeviceData( sizeof(DIDEVICEOBJECTDATA), diBuff, &entries, 0 );

		//Perhaps the user just tabbed away, and coop settings
		//are nonexclusive..so just ignore
		if( FAILED(hr) ) return;
	}

	BOOL bAxesMoved = FALSE;
	BOOL bScrolled = FALSE;
	//Accumulate all axis movements for one axesMove message..
	//Buttons are fired off as they are found
	for(unsigned int i = 0; i < entries; ++i )
	{
		switch( diBuff[i].dwOfs )
		{
		case DIMOFS_BUTTON0:
			_doMouseClick(0, diBuff[i]);
			break;
		case DIMOFS_BUTTON1:
			_doMouseClick(1, diBuff[i]);
			break;
		case DIMOFS_BUTTON2:
			_doMouseClick(2, diBuff[i]);
			break;
		case DIMOFS_BUTTON3:
			_doMouseClick(3, diBuff[i]);
			break;
		case DIMOFS_BUTTON4:
			_doMouseClick(4, diBuff[i]);
			break;	
		case DIMOFS_BUTTON5:
			_doMouseClick(5, diBuff[i]);
			break;
		case DIMOFS_BUTTON6:
			_doMouseClick(6, diBuff[i]);
			break;
		case DIMOFS_BUTTON7:
			_doMouseClick(7, diBuff[i]);
			break;
		case DIMOFS_X:
			deltaX += diBuff[i].dwData;
			bAxesMoved = true;
			break;
		case DIMOFS_Y:
			deltaY += diBuff[i].dwData;
			bAxesMoved = true;
			break;
		case DIMOFS_Z:
			deltaScroll += diBuff[i].dwData;
			bScrolled = true;
			break;
		default: break;
		}
	}

	if(bAxesMoved)
	{
		__updateMousePosition();

		MouseEvent e(MouseEvent::MOUSE_MOVE);
		e.mouseX = mouseX;
		e.mouseY = mouseY;
		e.deltaScroll = deltaScroll;
		DispatchEvent(e);
	}

	if(bScrolled)
	{
		scroll +=  deltaScroll;

		MouseEvent e(MouseEvent::MOUSE_WHEEL);
		e.deltaScroll = deltaScroll;
		DispatchEvent(e);
	}
}

void Mouse::__updateMousePosition()
{
	if(!__bMousePositionUpdated)
	{
		POINT point;
		GetCursorPos(&point);
		ScreenToClient(__hWnd, &point);
		mouseX = point.x;
		mouseY = point.y;

		__bMousePositionUpdated = TRUE;
	}
}

_COMMON_NS_END