#pragma once

#include "InputObject.h"
#include "common/input/MouseEvent.h"

_COMMON_NS_BEGIN

no_v_base class _CommonExport Mouse : public InputObject
{
	friend class InputManager;

protected:
	Mouse(void);
	virtual ~Mouse(void);

public:
	String 
		ToString(MouseButtonType button);
	int
		GetDeltaX() { return deltaX; }
	int
		GetDeltaY() { return deltaY; }
	int
		GetMouseX() { return mouseX; }
	int 
		GetMouseY() { return mouseY; }
	int
		GetDeltaScroll(){return deltaScroll;};

	BOOL
		IsButtonDown(MouseButtonType button);


protected:
	virtual void 
		Init(DWORD pData, DWORD_PTR hWnd);
	virtual void
		Tick();

	INLINE void 
		_doMouseClick(int mouseButton, DIDEVICEOBJECTDATA& di);


protected:
	int mouseX;
	int mouseY;
	int scroll;

	int deltaX;
	int deltaY;
	int deltaScroll;

	MouseButtonType
		button;

	// Use bit to store mouse button states
	DWORD
		_dwButtonsBuffer;


private:
	HWND
		__hWnd;

	// Mouse position will be incorrect if user switch from other window to the device's window.
	// So need use GetCursorPos to correct the mouse position.
	BOOL
		__bMousePositionUpdated;


private:
	inline void 
		__updateMousePosition();


};


_COMMON_NS_END