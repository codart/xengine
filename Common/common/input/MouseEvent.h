#pragma once

#include "common/core/Event.h"

_COMMON_NS_BEGIN

enum MouseButtonType
{
	MOUSE_LEFT = 0,
	MOUSE_RIGHT,
	MOUSE_MIDDLE,

	MOUSE_BUTTON3, 
	MOUSE_BUTTON4,	
	MOUSE_BUTTON5, 
	MOUSE_BUTTON6,	
	MOUSE_BUTTON7
};

class _CommonExport MouseEvent : public Event
{
public:
	enum MouseEventType
	{
		MOUSE_EVENT_BASE = 100,
		MOUSE_OVER,
		MOUSE_MOVE,
		MOUSE_WHEEL,
		MOUSE_DOWN,
		MOUSE_LEAVE,
		MOUSE_UP,
		MOUSE_CLICK,

		MOUSE_EVENT_END,
	};

	static bool IsValidEvent(int nType)
	{
		return nType > MOUSE_EVENT_BASE && nType < MOUSE_EVENT_END;
	}

	MouseEvent(int nType);


public:
	int 
		mouseX;
	int 
		mouseY;
	int 
		deltaScroll;

	MouseButtonType
		button;


protected:
	virtual int
		_prepareScriptArguments(ScriptBinder* pScriptBinder);


};

_COMMON_NS_END