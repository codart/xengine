#pragma once
#pragma warning(disable : 4275)

#include "InputObject.h"
#include "common/input/KeyboardEvent.h"

_COMMON_NS_BEGIN

/*
	Keyboard input object
*/
no_v_base class _CommonExport Keyboard : public InputObject
{
	friend class InputManager;

protected:
	virtual ~Keyboard(void);
	Keyboard();


public:
	WCHAR 
		ToChar(KeyCode keyCode);
	String 
		ToString(KeyCode keyCode);
	BOOL 
		IsKeyDown(KeyCode keyCode);


protected:
	virtual void 
		Init(DWORD pData, DWORD_PTR hWnd);
	virtual void 
		Tick();

	void 
		_updateKeyboardState();

private:
	unsigned char 
		_keyBuffer[256];
	BOOL 
		_bKeyboardStateUpdated;

private:
	WCHAR 
		__deadKey;

};


_COMMON_NS_END