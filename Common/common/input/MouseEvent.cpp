#include "StdAfx.h"
#include "MouseEvent.h"
#include "Common/script/ScriptBinder.h"

_COMMON_NS_BEGIN

MouseEvent::MouseEvent(int nType) : Event(nType)
{
	mouseX = 0;
	mouseY = 0;
}

int MouseEvent::_prepareScriptArguments(ScriptBinder* pScriptBinder)
{
	pScriptBinder->PushNumber(nType);
	pScriptBinder->PushNumber(mouseX);
	pScriptBinder->PushNumber(mouseY);
	pScriptBinder->PushNumber(button);
	return 4;
}


_COMMON_NS_END