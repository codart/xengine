#pragma once

#include <algorithm>
#include <typeinfo>

_COMMON_NS_BEGIN


#define __NEW_T(T) new (malloc(sizeof(T))) T
#define __DELETE_T(ptr, T) if(ptr){(ptr)->~T(); free((void*)ptr);}

/** 
	Variant type that can hold Any other type.
*/
class Any 
{
public: // constructors
	Any()
		: mContent(0)
	{
	}

	template<typename ValueType>
	Any(const ValueType & value)
		: mContent(__NEW_T(holder<ValueType>)(value))
	{
	}

	Any(const Any & other)
		: mContent(other.mContent ? other.mContent->clone() : 0)
	{
	}

	virtual ~Any()
	{
		destroy();
	}

public: // modifiers

	Any& swap(Any & rhs)
	{
		std::swap(mContent, rhs.mContent);
		return *this;
	}

	template<typename ValueType>
	Any& operator=(const ValueType & rhs)
	{
		Any(rhs).swap(*this);
		return *this;
	}

	Any & operator=(const Any & rhs)
	{
		Any(rhs).swap(*this);
		return *this;
	}

public: // queries

	bool isEmpty() const
	{
		return !mContent;
	}

	const std::type_info& getType() const
	{
		return mContent ? mContent->getType() : typeid(void);
	}

	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Any& v )
	{
		if (v.mContent)
			v.mContent->writeToStream(o);
		return o;
	}

	virtual void destroy()
	{
		__DELETE_T(mContent, placeholder);
		mContent = NULL;
	}

	// Extended type conversion utility methods
	int ToInt() const
	{
		XASSERT(this->getType() == typeid(int));
		return *any_cast<int>( (common::Any*)this );
	}

	uint32_t ToUint32() const
	{
		XASSERT(this->getType() == typeid(uint32_t));
		return *any_cast<uint32_t>( (common::Any*)this );
	}

	float ToFloat() const
	{
		XASSERT(this->getType() == typeid(float));
		return *any_cast<float>( (common::Any*)this );
	}

	double ToDouble() const
	{
		XASSERT(this->getType() == typeid(double));
		return *any_cast<double>( (common::Any*)this );
	}

	bool ToBool() const
	{
		XASSERT(this->getType() == typeid(bool));
		return *any_cast<bool>( (common::Any*)this );
	}

protected: // types

	class placeholder 
	{
	public: // structors

		virtual ~placeholder()
		{
		}

	public: // queries

		virtual const std::type_info& getType() const = 0;

		virtual placeholder * clone() const = 0;

		virtual void writeToStream(std::ostream& o) = 0;

	};

	template<typename ValueType>
	class holder : public placeholder
	{
	public: // structors

		holder(const ValueType & value)
			: held(value)
		{
		}

	public: // queries

		virtual const std::type_info & getType() const
		{
			return typeid(ValueType);
		}

		virtual placeholder * clone() const
		{
			return __NEW_T(holder)(held);
		}

		virtual void writeToStream(std::ostream& o)
		{
			o << held;
		}


	public: // representation

		ValueType held;

	};


protected: // representation
	placeholder * mContent;

	template<typename ValueType>
	friend ValueType * any_cast(Any *);


public: 

	template<typename ValueType>
	ValueType operator()() const
	{
		if (!mContent) 
		{
			throw Exception("Any::operator(), Bad cast from uninitialised Any");
		}
		else if(getType() == typeid(ValueType))
		{
			return static_cast<Any::holder<ValueType> *>(mContent)->held;
		}
		else
		{
			AStringStream str;
			str << "Any::operator() "<< "Bad cast from type '" << getType().name() << "' "
				<< "to '" << typeid(ValueType).name() << "'";
			throw Exception(str.str());
		}
	}

};


template<typename ValueType>
ValueType * any_cast(Any * operand)
{
	return operand && operand->getType() == typeid(ValueType)
		? &static_cast<Any::holder<ValueType> *>(operand->mContent)->held
		: 0;
}

template<typename ValueType>
const ValueType * any_cast(const Any * operand)
{
	return any_cast<ValueType>(const_cast<Any *>(operand));
}

template<typename ValueType>
ValueType any_cast(const Any & operand)
{
	const ValueType * result = any_cast<ValueType>(&operand);
	if(!result)
	{
		AStringStream str;
		str << "common::any_cast, Bad cast from type '" << operand.getType().name() << "' "
			<< "to '" << typeid(ValueType).name() << "'";

		throw Exception(str.str());
	}
	return *result;
}

_COMMON_NS_END
