#pragma once

_COMMON_NS_BEGIN

/*
	A property can be used by TweenMgr to do tween animation.
*/
template<typename UserClass>
class TAniProperty
{
public:
	class IPropertyImpl
	{
	public:
		virtual UserClass GetValue() = 0;	
		virtual void SetValue(const UserClass& value) = 0;	
	};

	typedef SharedPtr<IPropertyImpl, false> IPropertyImplPtr;

public:
	TAniProperty(IPropertyImplPtr pImpl)
	{
		_data = pImpl->GetValue();
		Attach(pImpl);
	}

	TAniProperty(const TAniProperty& safeValue)
	{
		_data = (UserClass)safeValue;
		_pImpl = NULL;
	}

	TAniProperty(UserClass value)
	{
		_data = value;
		_pImpl = NULL;
	}

	void Attach(IPropertyImplPtr pImpl)
	{
		_pImpl = pImpl;
	}

	void Detach()
	{
		_pImpl = NULL;
	}

	TAniProperty& operator=(const TAniProperty& srcValue) 
	{
		_data = srcValue._data;
		_pImpl->SetValue(srcValue);
		return *this;
	}

	operator UserClass() const
	{
		return _data;
	}

	UserClass* operator & ()
	{
		XASSERT(0);
		return &_data;
	}

	const UserClass* operator & () const
	{
		XASSERT(0);
		return &_data;
	}

	// -----------Operators that don't modify "this" but return a new value---------
	friend CONST TAniProperty operator + ( CONST TAniProperty& src, CONST TAniProperty& dest) {   return TAniProperty(src._data + dest._data); }
	friend CONST TAniProperty operator + ( CONST UserClass& src, CONST TAniProperty& dest) {  return TAniProperty(src + dest._data); }
	friend CONST TAniProperty operator + ( CONST TAniProperty& src, CONST UserClass& dest) {  return TAniProperty(src._data + dest); }
	
	friend CONST TAniProperty operator - ( CONST TAniProperty& src, CONST TAniProperty& dest) {   return TAniProperty(src._data - dest._data); }
	friend CONST TAniProperty operator - ( CONST UserClass& src, CONST TAniProperty& dest) {  return TAniProperty(src - dest._data); }
	friend CONST TAniProperty operator - ( CONST TAniProperty& src, CONST UserClass& dest) {  return TAniProperty(src._data - dest); }

	friend CONST TAniProperty operator * ( CONST TAniProperty& src, CONST TAniProperty& dest) {   return TAniProperty(src._data * dest._data); }
	friend CONST TAniProperty operator * ( CONST UserClass& src, CONST TAniProperty& dest) {  return TAniProperty(src * dest._data); }
	friend CONST TAniProperty operator * ( CONST TAniProperty& src, CONST UserClass& dest) {  return TAniProperty(src._data * dest); }

	friend CONST TAniProperty operator / ( CONST TAniProperty& src, CONST TAniProperty& dest) {   return TAniProperty(src._data / dest._data); }
	friend CONST TAniProperty operator / ( CONST UserClass& src, CONST TAniProperty& dest) {  return TAniProperty(src / dest._data); }
	friend CONST TAniProperty operator / ( CONST TAniProperty& src, CONST UserClass& dest) {  return TAniProperty(src._data / dest); }


	// -------------- Operators that will modify "this" -----------------------
	TAniProperty& operator += (CONST TAniProperty& value) 
	{
		_data += value._data; 
		_pImpl->SetValue(_data);
		return *this; 
	}
	TAniProperty& operator -= (CONST TAniProperty& value) 
	{ 
		_data -= value._data;
		_pImpl->SetValue(_data);
		return *this; 
	}
	TAniProperty& operator *= (CONST TAniProperty& value) 
	{
		_data *= value; 
		_pImpl->SetValue(_data);
		return *this; 
	}
	TAniProperty& operator /= (CONST TAniProperty& value)
	{
		_data /= value; 
		_pImpl->SetValue(_data);
		return *this; 
	}

	//-------------------------------------------------
	TAniProperty operator - () const  {  return TAniProperty(-_data); }

	//-------------------operator ++ & operator -- ------------------------------
	TAniProperty& operator ++ () {  (*this) += 1; return *this; }
	TAniProperty& operator -- () {  (*this) -= 1; return *this; }
	const TAniProperty operator ++ (int) { TAniProperty oldValue = *this; ++(*this); return oldValue; }
	const TAniProperty operator -- (int) { TAniProperty oldValue = *this; --(*this); return oldValue; }

	//--------------- compare operators --------------------------------------------
	bool operator <  ( const TAniProperty& dest ) const {   return _data < dest._data; }
	bool operator <  ( const UserClass& dest ) const {  return _data < dest; }
	bool operator <= ( const TAniProperty& dest ) const {   return _data <= dest._data; }
	bool operator <= ( const UserClass& dest ) const {  return _data <= dest; }
	bool operator == ( const TAniProperty& dest ) const {   return _data == dest._data; }
	bool operator == ( const UserClass& dest ) const {  return _data == dest; }
	bool operator != ( const TAniProperty& dest ) const {   return _data != dest._data; }
	bool operator != ( const UserClass& dest ) const {  return _data != dest; }
	bool operator >= ( const TAniProperty& dest ) const {   return _data >= dest._data; }
	bool operator >= ( const UserClass& dest ) const {  return _data >= dest; }
	bool operator >  ( const TAniProperty& dest ) const {   return _data > dest._data; }
	bool operator >  ( const UserClass& dest ) const {  return _data > dest; }

	//--------

protected:
	UserClass 
		_data;

	IPropertyImplPtr
		_pImpl;
};

typedef TAniProperty<REAL> FProperty;
typedef TAniProperty<REAL>::IPropertyImpl IFPropertyImpl;

_COMMON_NS_END
