#pragma once

#include "common/general/Any.h"

_COMMON_NS_BEGIN

#define NonConstInternalMap (* ( (map<String, GeneralTypePtr>*) (&_internalParamMap) ) )

#define ADD_GET_PARAM_METHOD(TypeName)		\
	INLINE TypeName Get##TypeName##Param(const String& strName) const	\
	{														\
		return NonConstInternalMap[strName]->GetValue().To##TypeName();	\
	}

template<typename GeneralTypePtr>
class _GeneralTypeList
{
protected:
	map<String, GeneralTypePtr>
		_internalParamMap;

public:
	template<typename ParamType>
	void AddItem(const String& strName, ParamType value)
	{
		XASSERT(_internalParamMap.find(strName) == _internalParamMap.end());
		_internalParamMap.insert(make_pair(strName, new Property(strName, Any(value))));
	}

	template<typename ParamType>
	void SetItem(const String& strName, ParamType value)
	{
		XASSERT(_internalParamMap.find(strName) != _internalParamMap.end());
		_internalParamMap[strName]->SetValue(value);
	}

	template<typename ParamType>
	ParamType GetItem(const String& strName) const
	{
		XASSERT(_internalParamMap.find(strName) != _internalParamMap.end());
		return any_cast<ParamType>(NonConstInternalMap[strName]->GetValue());
	}

	template<typename ParamType>
	ParamType GetItem(const String& strName, OUT ParamType* param) const
	{
		XASSERT(_internalParamMap.find(strName) != _internalParamMap.end());
		*param = any_cast<ParamType>(NonConstInternalMap[strName]->GetValue());
		return *param;
	}

	template<typename ParamType>
	ParamType GetItem(const String& strName, ParamType defaultValue) const
	{
		if(_internalParamMap.find(strName) == _internalParamMap.end())
		{
			return defaultValue;
		}
		else return any_cast<ParamType>(NonConstInternalMap[strName]->GetValue());
	}

	// ---- Simple param type "get" methods -------
	ADD_GET_PARAM_METHOD(Float);
	ADD_GET_PARAM_METHOD(Int);
	ADD_GET_PARAM_METHOD(Double);
	ADD_GET_PARAM_METHOD(Bool);

};


_COMMON_NS_END

