#pragma once

#include "Parameter.h"

_COMMON_NS_BEGIN

class PropertyEvent : public Event
{
public:
	enum PropertyEventType
	{
		PROPERTY_CHANGE, // Be dispatched before property change.
	};

	Any OldValue;
	Any NewValue;

	PropertyEvent(int type, Any oldValue = Any(0), Any newVlaue = Any(0)):Event(type)
	{
		OldValue = oldValue;
		NewValue = newVlaue;
	}
};

/// A property is an advanced Parameter. It will dispatch event(PropertyEvent) when value changed. 
/// You can listen to PropertyEvent to do additional function such as Update UI, send new 
/// property value to server, etc.
class Property : public EventDispatcher
				, public Parameter
{
public:
	Property(const String& strName)
		: Parameter(strName)
	{}

	virtual ~Property() 
	{}

	template<typename ValueType>
	Property(const String& strName, const ValueType & value)
		: Parameter(strName, value)
	{}

	Property(const String& strName, const Any& value)
		: Parameter(strName, value)
	{}

public:
	/// Update value without dispatch event.
	template<typename PropertyType>
	void UpdateValue(PropertyType newValue)
	{
		Parameter::SetValue(newValue);
	}

	/// Update value and dispatch an event.
	template<typename PropertyType>
	void SetValue(PropertyType newValue)
	{
		DispatchEvent(PropertyEvent(PropertyEvent::PROPERTY_CHANGE, _value, Any(newValue)));
		Parameter::SetValue(newValue);
	}

	void
		SetValue(const Any& newValue) 
	{ 
		DispatchEvent(PropertyEvent(PropertyEvent::PROPERTY_CHANGE, _value, newValue));
		Parameter::SetValue(newValue);
	}

};


typedef SharedPtr<Property, false> PropertyPtr;
typedef _GeneralTypeList<PropertyPtr> PropertyList;

_COMMON_NS_END

