#pragma once

_COMMON_NS_BEGIN

class Color
{
public:
	static const Color ZERO;
	static const Color Black;
	static const Color White;
	static const Color Red;
	static const Color Green;
	static const Color Blue;


public:
	Color(float rr = 1.0f, float gg = 1.0f, float bb = 1.0f, float aa = 1.0f)
		: r(rr), g(gg), b(bb), a(aa) {}


public:
	float r;
	float g;
	float b;
	float a;

protected:
private:
};

_COMMON_NS_END