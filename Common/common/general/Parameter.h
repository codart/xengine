#pragma once

#include "_GeneraTypeList.h"

_COMMON_NS_BEGIN


/// A property can manipulator the parameter of a class/component
no_v_base class Parameter
{
public:
	Parameter(const String& strName)
		: _strName(strName)
	{
		 
	}

	virtual ~Parameter()
	{

	}

	template<typename ValueType>
	Parameter(const String& strName, const ValueType & value)
		: _strName(strName)
		, _value(value)
	{
	}

	Parameter(const String& strName, const Any& value)
		: _strName(strName)
		, _value(value)
	{
	}

public:
	const String&
		GetName() const { return _strName; }

	template<typename PropertyType>
	void SetValue(PropertyType newValue)
	{
		_value = Any(newValue);
	}

	void
		SetValue(const Any& newValue) 
	{ 
		_value = newValue; 
	}

	const Any&
		GetValue() const { return _value; }


protected:
	String
		_strName;
	Any
		_value;
};

typedef SharedPtr<Parameter, false> ParameterPtr;
typedef _GeneralTypeList<ParameterPtr> ParameterList;

_COMMON_NS_END

