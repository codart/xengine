#include "stdafx.h"
#include "Color.h"

_COMMON_NS_BEGIN

const Color Color::ZERO = Color(0.0,0.0,0.0,0.0);
const Color Color::Black = Color(0.0,0.0,0.0);
const Color Color::White = Color(1.0,1.0,1.0);
const Color Color::Red = Color(1.0,0.0,0.0);
const Color Color::Green = Color(0.0,1.0,0.0);
const Color Color::Blue = Color(0.0,0.0,1.0);

_COMMON_NS_END