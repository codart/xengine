#pragma once

#include <algorithm>

#include "common/core/NonCopyble.h"
#include "common/util/callback.hpp"
#include "common/util/SharedPtr.h"

_COMMON_NS_BEGIN

/// ------ Trait base classes -----------
/// Base trait class for pure structure.
template<typename T>
struct PooledObjectStructTraitBase
{
	/// Default Create/Destroy implementation. 
	static T Create() { return T(); }
	static void Destroy(T& object) {};
	/// For value types, treat them not equal always.
	static bool IsNull(T& object)
	{
		T nullObject; memset(&nullObject, 0, sizeof(nullObject));
		int iRet = memcmp(&object, &nullObject, sizeof(T));
		return iRet == 0;
	}
	static void AssignNull(T& object) { memset(&object, 0, sizeof(object)); } 
	static bool IsUnique() { return false; }
	static unsigned int GetKey(T& object) { XASSERT(0); return 0; }
	static bool CheckRefCount(T& object, int nCount) { return true; }
};

/// Base trait class for raw pointer type.
template<typename T>
struct PooledObjectPointerTraitBase
{
	static T* Create() { return XNEW T(); }
	static void Destroy(T* object) { XDELETE object; };
	static bool IsNull(T* object) { return object == NULL; } 
	static void AssignNull(T*& object) { object = NULL; }
	static bool IsUnique() { return true; }
	static unsigned int GetKey(T* object) { return (unsigned int)object; }
	static bool CheckRefCount(T* object, int nCount) { return true; }
};

/// ------ Default traits -----------
template<typename T>
struct PooledObjectTrait;

template<typename T>
struct PooledObjectTrait : public PooledObjectStructTraitBase<T> { };

template<typename T>
struct PooledObjectTrait<T*> : public PooledObjectPointerTraitBase<T> { };

template<typename T, BOOL UseDestroy>
struct PooledObjectTrait<SharedPtr<T, UseDestroy>>
{
	typedef SharedPtr<T, UseDestroy> _Ptr;

	static _Ptr Create() { return _Ptr(XNEW T()); }
	static void Destroy(_Ptr& object) { XASSERT(object.GetReferenceCount() == 1); object = NULL; };
	static bool IsNull(_Ptr& object) { return object == _Ptr(NULL); }
	static void AssignNull(_Ptr& object) { object = NULL; }
	static bool IsUnique() { return true; }
	static unsigned int GetKey(_Ptr& object) { return (unsigned int)object.getPointer(); }
	static bool CheckRefCount(_Ptr& object, int nCount) { return object.GetReferenceCount() == nCount; }
}; 

/*
Pools objects to improve object create/destroy efficiency.

Basic usage:
	1. Pools objects with pointer, for example:
		ObjectPool<UserClass*> myPool;
	2. Pools objects with SharedPtr<>, for example:
		ObjectPool< SharedPtr<UserClass> > myPool;
	3. Pools objects with pure structure object, for example(not recommend):
		ObjectPool<UserStructure> myPool;
		
	Recommend use 2. to pooling your objects! 
	Never use ObjectPool with struct/classes that have 'Deep Copy' when do assignment!

Advanced usage:
	You can customize Pool Object's behavior by define your own PooledObjectTrait<>.
	Below example shows how to define a structure to store pointers, and pools wrapped 
	objects with ObjectPool<>.
	
	Code:
		/// This is the wrapper structure
		struct PointerWrapper
		{
			char* pszStr1;
			char* pszStr2;
		};

		/// Customize the PooledObjectTrait<>
		template<>
		struct PooledObjectTrait<PointerWrapper> : public PooledObjectStructTraitBase<PointerWrapper>
		{	
			static PointerWrapper Create() { XASSERT(0); return PointerWrapper(); }
			static void Destroy(PointerWrapper& object) { XASSERT(0); };
			static bool IsUnique() { return true; }
			static unsigned int GetKey(PointerWrapper& object) { return (unsigned int)object.pszStr1; }
		};

		/// Provide a Create/Destroy function (since the structure contains pointer).
		SmartPointer CreateObject()
		{
			SmartPointer p;
			p.pszStr1 = new char[128];
			strcpy_s(p.pszStr1, 128, "string1");

			p.pszStr2 = new char[128];
			strcpy_s(p.pszStr2, 128, "string2");

			return p;
		}

		void DestroyObject(SmartPointer& p)
		{
			delete p.pszStr1;
			delete p.pszStr2;
		}
		
		/// Finally, you can use your pool!
		ObjectPool<PointerWrapper> myPool;

*/
template<typename UserType>
class ObjectPool : public NonCopyable
{
public:
	static const int DEFAULT_MAXSIZE = 100;
	static const int DEFAULT_INIT_SIZE = 10;

	typedef	util::Callback<UserType ()> Creator;
	typedef	util::Callback<void ( UserType& pObject)> Destroyer;

	/*
		nMaxSize: Limit pool's max size.

		nInitSize: Init objects be create after construction.

		pCreator/pDestroyer: Provide your self's creator/destroyer. You should use MFUNC/FFUNC to 
							bind your method/function. For example:

			ObjectPool<RawData*> myPool1(20, 1, MFUNC(&TestObjectPool::_createRawData, this));		/// Bind member method
			ObjectPool<SmartPointer> myPool4(20, 10, FFUNC(CreateObject), FFUNC(DestroyObject));	/// Bind free function

			You can control the creation/destruction with PoolObjectTrait's Create/Destroy methods too. 
			If you neither provide creator/destroyer nor PoolObjectTrait's Create/Destroy, ObjectPool will use 
			PoolObjectTrait's default Create/Destroy implementation.

	*/
	ObjectPool(uint32_t nMaxSize = DEFAULT_MAXSIZE, uint32_t nInitSize = DEFAULT_INIT_SIZE
		, Creator pCreator = util::NullCallback()
		, Destroyer pDestroyer = util::NullCallback());

	virtual ~ObjectPool(void);


public:
	/// Get an instance from the pool
	INLINE UserType
		GetInstance();

	/// Free an instance to the pool
	INLINE void 
		FreeInstance(UserType& pInstance);

	INLINE uint32_t
		SetPoolSize(uint32_t nSize);
	INLINE uint32_t 
		GetPoolSize() { return _nPoolSize; }

	INLINE uint32_t 
		GetUsedCount() { XASSERT(_nPoolSize > _iCursor); return uint32_t(_nPoolSize - _iCursor - 1) ; }

	INLINE uint32_t 
		GetFreeCount() { return uint32_t(_iCursor + 1); }

	INLINE uint32_t
		GetMaxSize() { return _nMaxSize; }
	INLINE void
		SetMaxSize(uint32_t nMaxSize);

	INLINE bool 
		IsEmpty() { return _iCursor == -1; }
	INLINE bool 
		IsReachMaxSize() { XASSERT(!(_nPoolSize > _nMaxSize)); return _nPoolSize == _nMaxSize; }

	/// Shrink the pool. All free objects in the pool will be released. This can free unused objects but will cause continue
	/// calls to GetInstance() low performance. 
	INLINE uint32_t
		Shrink();
	/// Shrink the pool. Half of the free objects in the pool will be removed. This can free half of the unused objects 
	/// but may cause continue calls to GetInstance() low performance. 
	INLINE uint32_t
		HalfShrink();


protected:
	typedef PooledObjectTrait<UserType> _Trait;

	typedef std::vector<UserType> FreeObjectList;
	typedef typename FreeObjectList::iterator FreeObjectListIter;

	FreeObjectList
		_arrFreeObjects;
	int64_t 
		_iCursor;
	uint32_t
		_nMaxSize;
	uint32_t 
		_nPoolSize;

	Creator 
		_pCreator;
	Destroyer
		_pDestroyer;


private:
#ifdef _DEBUG
	enum PooledObjectState
	{
		OBJ_CREATED		= -1,		/// First created, not in pool
		OBJ_BORROWED	= 0,		/// Borrowed
		OBJ_FREE		= 1,		/// Free (In pool)
	};

	typedef std::map<unsigned int, int8_t> ManagedObjectMap;
	typedef typename ManagedObjectMap::iterator ManagedObjectMapIter;

	ManagedObjectMap
		__mapMangagedObjects;

#endif // _DEBUG

protected:
	INLINE UserType 
		_createInstance();

	INLINE void 
		_destroyInstance(UserType& pObject);

	INLINE UserType 
		_popInstance();

	INLINE void 
		_pushInstance(UserType& pObject);

	INLINE uint32_t 
		_removeFreeObjects(uint32_t nCount);

};

#include "common/algorithm/ObjectPool.inl"


_COMMON_NS_END

