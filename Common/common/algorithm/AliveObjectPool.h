#pragma once

_COMMON_NS_BEGIN

#include <vector>
#include "common/util/callback.hpp"


template<typename T>
struct PooledAliveObjectTraitBase
{
	static T Create() { return T(); }
	static void Destroy(T& object) {};

	/// Params:
	///			Object	the Object to be updated
	/// Return value:
	///			false	Object is dead
	///			true	Object is alive
	static bool UpdateObject(T& Object) { return false; }

	static void InitNewObject(T& Object) {}

	static void MarkDead(T& object) { memset(&object, 0, sizeof(object)); } 

	static bool IsAlive(T& object) { return false; }
};


template<typename T>
struct PooledAliveObjectTrait : public PooledAliveObjectTraitBase<T> { };



/// Cache friendly object list. 
template<typename ObjectType_>
class AliveObjectPool
{
	typedef PooledAliveObjectTrait<ObjectType_> _Trait;

public:
	typedef ObjectType_ ObjectType;

	typedef	util::Callback<ObjectType ()> Creator;
	typedef	util::Callback<void ( ObjectType& pObject)> Destroyer;

public:
	AliveObjectPool(int nReservedCount = 0
		, Creator pCreator = util::NullCallback()
		, Destroyer pDestroyer = util::NullCallback());

	virtual ~AliveObjectPool(void);

public:
	int 
		GetAliveCount() { return _nAliveObjectCount; }
	int 
		GetPoolSize() { return _arrObjectList.size(); } 
	void 
		Update();

	ObjectType& 
		GetAliveObject(unsigned int iIndex){ XASSERT(iIndex < _nAliveObjectCount); return _arrObjectList[iIndex]; }

	ObjectType& 
		SpawnNewObject();


protected:
	INLINE ObjectType
		_createInstance();
	INLINE void 
		_destroyInstance(ObjectType& object);


protected:
	std::vector<ObjectType>
		_arrObjectList;

	mutable int 
		_nAliveObjectCount;


	Creator 
		_pCreator;
	Destroyer
		_pDestroyer;

};

#include "common/algorithm/AliveObjectPool.inl"



_COMMON_NS_END
