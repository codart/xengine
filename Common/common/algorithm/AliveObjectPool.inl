template<typename ObjectType>
AliveObjectPool<ObjectType>::AliveObjectPool(int nReservedCount
				, Creator pCreator
				, Destroyer pDestroyer)
{
	_nAliveObjectCount = 0;

	_pCreator = pCreator;
	_pDestroyer = pDestroyer;

	ObjectType object = _createInstance();
	_Trait::MarkDead(object);

	for(int i=0; i<nReservedCount; ++i)
	{
		_arrObjectList.push_back(object);
	}
}


template<typename ObjectType>
AliveObjectPool<ObjectType>::~AliveObjectPool(void)
{
	/// Destroy all objects
	for(uint32_t i=0; i<_arrObjectList.size(); ++i) 
	{
		_destroyInstance(_arrObjectList[i]);
	}
}

template<typename ObjectType>
void AliveObjectPool<ObjectType>::Update()
{
	for(int i=0; i<_nAliveObjectCount; i++)
	{
		ObjectType& object = _arrObjectList[i];
		bool bIsAlive = _Trait::UpdateObject(object);

		if(!bIsAlive)
		{
			_arrObjectList[i] = _arrObjectList[_nAliveObjectCount - 1];
			_Trait::MarkDead(_arrObjectList[_nAliveObjectCount - 1]);
			i--;
			_nAliveObjectCount--;
		}

	}
}

template<typename ObjectType>
ObjectType& AliveObjectPool<ObjectType>::SpawnNewObject()
{
	if(_nAliveObjectCount == _arrObjectList.size()) 
	{
		ObjectType object = _createInstance();
		_Trait::MarkDead(object);
		_arrObjectList.push_back(object);
	}

	ObjectType& object= _arrObjectList[_nAliveObjectCount];
	_Trait::InitNewObject(object);

	_nAliveObjectCount++;

	assert(_Trait::IsAlive(object));
	return object;
}

template<typename ObjectType>
ObjectType AliveObjectPool<ObjectType>::_createInstance()
{
	ObjectType pInstance;
	if(_pCreator) pInstance = _pCreator();
	else pInstance = _Trait::Create();

	return pInstance;
}

template<typename ObjectType>
void AliveObjectPool<ObjectType>::_destroyInstance(ObjectType& object)
{
	if(_pDestroyer) _pDestroyer(object);
	else _Trait::Destroy(object);
}
