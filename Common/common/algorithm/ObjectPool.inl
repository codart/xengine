template <class T> void
_shrinkListToFit(T& container, uint32_t nRemoveCount)
{
	/// Shrink the vector
	while (nRemoveCount--) container.pop_back();

	if(container.capacity() != container.size())
	{
		T(container).swap(container);
	}
}

/// Implementation of ObjectPool<>
template<typename UserType>
ObjectPool<UserType>::ObjectPool(uint32_t nMaxSize, uint32_t nInitSize
								 , Creator pCreator, Destroyer pDestroyer)
{
	XASSERT(nMaxSize > 0 && nInitSize >= 0);

	_nMaxSize = nMaxSize;
	_iCursor = -1;
	_nPoolSize = 0;

	_pCreator = pCreator;
	_pDestroyer = pDestroyer;	

	for (uint32_t i=0; i<nInitSize; ++i)
	{
		UserType pNewInstance = _createInstance();
		_nPoolSize++;
		_pushInstance(pNewInstance);
	}		
}

template<typename UserType>
ObjectPool<UserType>::~ObjectPool(void)
{
	uint32_t nFreeCount = GetFreeCount();
	XASSERT(_nPoolSize == nFreeCount);

	if(nFreeCount == 0) return;

	for (uint32_t i=0; i<nFreeCount; ++i)
	{
		_destroyInstance(_arrFreeObjects[i]);
	}

}

/// Get an instance from the pool
template<typename UserType>
UserType ObjectPool<UserType>::GetInstance()
{
	XASSERT(_nPoolSize <= _nMaxSize);

	if(IsEmpty())
	{
		if (IsReachMaxSize()) XTHROWEX("Object pool exceed max size!");

		_nPoolSize++;
		UserType pObject = _createInstance();

#ifdef _DEBUG
		if(_Trait::IsUnique())
		{
			ManagedObjectMapIter iter = __mapMangagedObjects.find(_Trait::GetKey(pObject));
			iter->second = OBJ_BORROWED;
		}
#endif
		return pObject;
	}
	else
	{
		return _popInstance();
	}
}

/// Free an instance to the pool
template<typename UserType>
void ObjectPool<UserType>::FreeInstance(UserType& pObject)
{
	if (_iCursor + 1 >= _nPoolSize) XTHROWEX("Object pool exceed max size! Do you free an object	\
														that is not managed by ObjectPool?");

#ifdef _DEBUG
	XASSERT(!_Trait::IsNull(pObject)); 

	if(_Trait::IsUnique())
	{
		ManagedObjectMapIter iter = __mapMangagedObjects.find(_Trait::GetKey(pObject));
		if(iter == __mapMangagedObjects.end()) XTHROWEX("Object is not managed by ObjectPool cannot be	\
															freed by FreeInstance()!");
		if(iter->second != OBJ_BORROWED) XTHROWEX("Object's state is wrong! Is memory corrpted?");
		iter->second = OBJ_FREE;
	}

	bool bRet = _Trait::CheckRefCount(pObject, 1);
	if(!bRet) XTHROWEX("Object can be FreeInstance() only when reference cout is 1!!!");
#endif

	_pushInstance(pObject);
}

template<typename UserType>
void ObjectPool<UserType>::SetMaxSize(uint32_t nMaxSize) 
{
	XASSERT(nMaxSize > 0);

	if(nMaxSize < GetUsedCount()) XTHROWEX("The max size must bigger than current used count!");
	if(nMaxSize >= _nPoolSize) { _nMaxSize = nMaxSize; return; }

	uint32_t nRemoveCount = _nPoolSize - nMaxSize;
	XASSERT(nRemoveCount > 0);
	_removeFreeObjects(nRemoveCount);
	_nMaxSize = nMaxSize;
	XASSERT(_nPoolSize == nMaxSize);
}

template<typename UserType>
uint32_t ObjectPool<UserType>::SetPoolSize(uint32_t nPoolSize) 
{
	if(nPoolSize == GetPoolSize()) return nPoolSize;

	if(nPoolSize > _nMaxSize) XTHROWEX("The reserved pool size exceeds max limited size!");
	if(nPoolSize < GetUsedCount()) XTHROWEX("Pool size cannot be less than used count!");

	if(nPoolSize > GetPoolSize())
	{
		uint32_t nNewAddSize = nPoolSize - GetPoolSize();

		for (uint32_t i=0; i<nNewAddSize; ++i)
		{
			UserType pNewInstance = _createInstance();
			_nPoolSize ++;
			_pushInstance(pNewInstance);
		}
	}
	else
	{
		uint32_t nRemoveCount = _nPoolSize - nPoolSize;
		XASSERT(nRemoveCount > 0);
		_removeFreeObjects(nRemoveCount);		
		_shrinkListToFit(_arrFreeObjects, nRemoveCount);
		XASSERT(_arrFreeObjects.capacity() == GetPoolSize());
	}

	XASSERT(GetPoolSize() == nPoolSize);
	return nPoolSize;
}

template<typename UserType>
uint32_t ObjectPool<UserType>::Shrink() 
{
	uint32_t nShrinkCount = GetFreeCount();
	uint32_t nPoolSize = _removeFreeObjects(nShrinkCount);
	XASSERT(-1 == _iCursor);

	_shrinkListToFit(_arrFreeObjects, nShrinkCount);
	XASSERT(_arrFreeObjects.capacity() == GetPoolSize());

	return nPoolSize;
}

template<typename UserType>
uint32_t ObjectPool<UserType>::HalfShrink()  
{
	uint32_t nShrinkCount = GetFreeCount() / 2;
	uint32_t nPoolSize = _removeFreeObjects(nShrinkCount);

	_shrinkListToFit(_arrFreeObjects, nShrinkCount);
	XASSERT(_arrFreeObjects.capacity() == GetPoolSize());

	return nPoolSize;
}

template<typename UserType>
uint32_t ObjectPool<UserType>::_removeFreeObjects(uint32_t nCount) 
{
	_nPoolSize -= nCount;

	for (uint32_t i=0; i<nCount; ++i)
	{
		_destroyInstance(_arrFreeObjects[(uint32_t)_iCursor - i]);
#ifdef _DEBUG
		// Clear memory to null in debug mode
		_Trait::AssignNull(_arrFreeObjects[(uint32_t)_iCursor - i]);
#endif
	}

	_iCursor -= nCount;
	XASSERT(-1 <= _iCursor);

	return _nPoolSize;
}

template<typename UserType>
UserType ObjectPool<UserType>::_createInstance()
{
	UserType pInstance;
	if(_pCreator) pInstance = _pCreator();
	else pInstance = _Trait::Create();

#ifdef _DEBUG
	if(_Trait::IsUnique())
	{
		__mapMangagedObjects.insert(make_pair(_Trait::GetKey(pInstance), OBJ_FREE));
	}
#endif

	return pInstance;
}

template<typename UserType>
void ObjectPool<UserType>::_destroyInstance(UserType& pObject)
{
#ifdef _DEBUG
	if(_Trait::IsUnique())
	{
		__mapMangagedObjects.erase(_Trait::GetKey(pObject));
	}
#endif

	if(_pDestroyer) return _pDestroyer(pObject);
	else _Trait::Destroy(pObject);
}

template<typename UserType>
UserType ObjectPool<UserType>::_popInstance()
{
	XASSERT(_iCursor >= -1);

	UserType pObject = _arrFreeObjects[(uint32_t)_iCursor];

#ifdef _DEBUG
	// Clear memory to null in debug mode
	_Trait::AssignNull(_arrFreeObjects[(uint32_t)_iCursor]);

	if(_Trait::IsUnique())
	{
		ManagedObjectMapIter iter = __mapMangagedObjects.find(_Trait::GetKey(pObject));
		XASSERT(iter != __mapMangagedObjects.end());
		XASSERT(iter->second == OBJ_FREE);
		iter->second = OBJ_BORROWED;
	}
#endif

	_iCursor--;
	return pObject;	
}

template<typename UserType>
void ObjectPool<UserType>::_pushInstance(UserType& pObject)
{
	XASSERT(_iCursor + 1 < _nPoolSize);

	_iCursor++;
	if(_iCursor + 1 > _arrFreeObjects.size()) _arrFreeObjects.push_back(pObject); 
	else
	{
		XASSERT( _Trait::IsNull(_arrFreeObjects[(uint32_t)_iCursor]) );
		_arrFreeObjects[(uint32_t)_iCursor] = pObject;
	}

#ifdef _DEBUG
	if(_Trait::IsUnique())
	{
		ManagedObjectMapIter iter = __mapMangagedObjects.find(_Trait::GetKey(pObject));
		iter->second = OBJ_FREE;
	}
#endif

	_Trait::AssignNull(pObject);
}

