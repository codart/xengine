#include "StdAfx.h"
#include "ReferenceCounted.h"

#include "common/debug/Tracer.h"
#include "common/core/NoImplementationException.h"


_COMMON_NS_BEGIN

#ifdef DEBUG_NORMAL
int ReferenceCounted::s_objIndex = 0;
#endif // DEBUG_NORMAL

ReferenceCounted::ReferenceCounted(void)
{
	objType = OBJ_UNKNOWN;

#ifdef DEBUG_NORMAL
	if(PlatformUtil::Instance().IsStack((char*)this)) XTHROW(IncorrectAddressException);

	//... Instead CString with StringStream
	CString objName;
	objName.Format(L"Object %d", s_objIndex++);
	lstrcpyn((LPWSTR)strObjName, objName, objName.GetLength());

	objName.Format(L"Create Object %d", s_objIndex);
	Tracer::Instance().WriteTrace(objName.GetBuffer(0));
#endif // DEBUG_NORMAL
	_refCount = 0; 
};

int ReferenceCounted::Release()
{
	/** 
	Need introduce Garbage collector to optimize performance. When release a 
	object, only put it to garbage queue, and garbage collector will real 
	release it when idle.
	*/
	--_refCount;
	XASSERT(_refCount >=0);
	if(0 == _refCount)
	{
#ifdef DEBUG_NORMAL
		CString strIndex;
		strIndex.Format(L"Destroy Object %d", s_objIndex);
		Tracer::Instance().WriteTrace(strIndex.GetBuffer(0));
		s_objIndex--;
#endif // _DEBUG
		delete this;
		return 0;
	}

	return _refCount;
}

ReferenceCounted*ReferenceCounted::Clone(BOOL bUseReference)
{
	XTHROW(NoImplementationException);
	return NULL;
}

_COMMON_NS_END