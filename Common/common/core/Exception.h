#pragma once

#include <sstream>

_COMMON_NS_BEGIN


class Exception : public std::runtime_error
{
public:
	Exception(const AString& strReason) : std::runtime_error(strReason) { }
	virtual ~Exception() throw() { };

protected:
	//virtual void
	//	SetReason(const AString& strReason);
	virtual void
		SetFunctionName(const AString& strFunctionName)
	{
		//..._strFunctionName = strFunctionName;
	}


protected:
	// We can use __FILE__, __LINE__ to determine which file/line the exception occurred. But there is not a __FUNC__
	// to get the function name. So we need pass the function name manually with this field. 
	//...AString 
	//...	_strFunctionName;

};


_COMMON_NS_END