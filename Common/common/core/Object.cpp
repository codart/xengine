#include "stdafx.h"
#include "Object.h"

_COMMON_NS_BEGIN

void Object::Destroy()
{
	delete this;
}

_COMMON_NS_END