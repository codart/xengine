#pragma once
#include "common/core/exception.h"

_COMMON_NS_BEGIN


class _CommonExport InvalidParamException : public Exception
{
public:
	InvalidParamException(const AString& strReason, const AString& strFunctionName)
		: Exception(strReason)
	{
		//...SetFunctionName(strFunctionName);
	}
};


_COMMON_NS_END