#pragma once

_COMMON_NS_BEGIN

class _CommonExport AutoRelease
{
public:
	AutoRelease();
	virtual ~AutoRelease();
};

class _CommonExport ManualRelease{};


_COMMON_NS_END