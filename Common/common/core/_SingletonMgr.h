#pragma once

#include "SingletonBase.h"

_COMMON_NS_BEGIN

class _CommonExport _SingletonMgr
{
public:
	static void Init();
	static void UnInit();

	static void RegisterSingleton(AutoRelease* pInstance);

protected:
	typedef vector<AutoRelease*>::iterator ItVec_ISingleton;

	static vector<AutoRelease*>
		_singletonInstances;

};


_COMMON_NS_END