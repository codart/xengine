#pragma once

#include "common/plantform/PlatformUtil.h"
#include "common/core/IncorrectAddressException.h"

_COMMON_NS_BEGIN

enum OBJECT_TYPE
{
	OBJ_UNKNOWN					= 0,
	OBJ_STAGE_OBJECT			= 1<<1,
	OBJ_DISPLAY_OBJECT			= 1<<2,
	OBJ_BITMAP_OBJECT			= 1<<3,
	OBJ_INTERACTIVE_OBJECT		= 1<<4,
	OBJ_MOVIECLIP_OBJECT		= 1<<5,
	OBJ_OBJECT2D				= 1<<6,

	OBJ_MASK_OBJECT				= 1<<7,
	OBJ_TEST					= 1<<10,
};

class _CommonExport ReferenceCounted
{
public:
	unsigned long
		objType;

public:
	ReferenceCounted(void);
	virtual ~ReferenceCounted(void){};

public:
	/*
		Return the new reference count. 
	*/
	inline int 
		AddRef()
	{
		return ++_refCount;
	}

	/*
		Return the new reference count. 
	*/
	int
		Release();

	virtual ReferenceCounted*
		Clone(BOOL bUseReference = true);


protected:
	int _refCount;

public:
#ifdef DEBUG_NORMAL
	static int s_objIndex;
	WCHAR strObjName[MAX_PATH];
#endif // DEBUG_NORMAL

};

_COMMON_NS_END