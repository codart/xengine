#pragma once

_COMMON_NS_BEGIN

/// Abstract base class for all 'Object'
class _CommonExport Object
{
public:
	Object()
	{
	}

	virtual ~Object()
	{
	}


public:
	virtual void 
		Destroy();

};


_COMMON_NS_END