/************************************************************************
Description:
	Disable copy & assignment operation.
	
Usage:
	Make your class inherit from NonCopyable. If your class will be export, 
	you can use DECLARE_NON_COPYABLE macro instead (or else the compiler will
	generate a warning).
************************************************************************/

#pragma once

_COMMON_NS_BEGIN

class NonCopyable
{
protected:
	NonCopyable() {}
	~NonCopyable() {}

private:  
	NonCopyable( const NonCopyable& );
	const NonCopyable& operator=( const NonCopyable& );
};

#define DECLARE_NON_COPYABLE(UserClass)	\
private:	\
	UserClass( const UserClass& );	\
	const UserClass& operator=( const UserClass& );	\


_COMMON_NS_END