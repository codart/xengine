#pragma once

#include "common/util/callback.hpp"

_COMMON_NS_BEGIN

typedef	util::Callback<void (const Event*)> MemberFunctor;

typedef void (__stdcall *EventHandleFunc)(const Event* pEvent);

class ScriptEventDelegator;

class _CommonExport IEventDispatcher
{
public:
	virtual void  DispatchEvent(Event& event) = 0;

	/*
	If set bWeakReference to false, AddEventListener will add a reference to pReceiver. Incorrect use of this parameter 
	may cause recursive reference.
	*/
	virtual int AddEventListener(int nEventType
							, MemberFunctor functor) = 0;

	// For script support
	virtual int AddEventListener(int nEventType, ScriptEventDelegator* pDelegater) 
										{ XASSERT(0); return 0; }

	virtual int RemoveEventListener(int nEventType
									, MemberFunctor functor) = 0;

	/* 
	Return binder to BinderPool
	*/
	virtual void RemoveEvent(int nEventType, BOOL bLazyRelease = false) = 0;

	// Check whether the member func is listen the specified event.
	virtual BOOL IsEventListener(int nEventType, MemberFunctor functor)  = 0;

};

_COMMON_NS_END


