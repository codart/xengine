#include "StdAfx.h"
#include "_SingletonMgr.h"

_COMMON_NS_BEGIN

vector<AutoRelease*> _SingletonMgr::_singletonInstances;

void _SingletonMgr::Init()
{

}

void _SingletonMgr::UnInit()
{
	ItVec_ISingleton it = _singletonInstances.begin();
	for ( ; it != _singletonInstances.end(); ++it)
	{
		AutoRelease* pSingleton = *it;
		delete pSingleton;
	}

	_singletonInstances.clear();

	// Make sure vector's memory is released
	_singletonInstances.swap(vector<AutoRelease*>());
}

void _SingletonMgr::RegisterSingleton(AutoRelease* pInstance)
{
	_singletonInstances.push_back(pInstance);
}


_COMMON_NS_END