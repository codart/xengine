/************************************************************************
Description:
	The Final can make your class cannot be inherited (Can be inherited actually, but the 
	child class cannot be instantiate). To achieve the same purpose as the final keyword 
	in java.

	CForbidInherit can make your template's instance class cannot be inherited.

Usage:
	Make your class inherit from Final

		class MyFinalClass : public Final<MyFinalClass>

	Or use the CFobidInherit directly if your class is a template, 
	make sure use virtual public,

		template<typename UserClass>
		class MyFinalClass : virtual public CForbidInherit<UserClass, MyFinalClass<UserClass>>
************************************************************************/

#pragma once

_COMMON_NS_BEGIN

template<typename TDerive, typename TFinal> 
class ForbidInherit
{
	friend TDerive; 
	friend TFinal;

private: 
	ForbidInherit(){} 
	~ForbidInherit(){} 
}; 

template<typename UserClass> 
class Final: virtual public ForbidInherit<UserClass, Final<UserClass>> 
{ 
public: 
	Final(){} 
	~Final(){} 
}; 

_COMMON_NS_END