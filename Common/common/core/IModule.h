#pragma once

_COMMON_NS_BEGIN

class IModule
{
public:
	virtual void 
		Init(const Event* pEvent) {};
	virtual void 
		Tick(const Event* pEvent) { XASSERT(0); };
	virtual void 
		Uninit(const Event* pEvent) {};
};

_COMMON_NS_END