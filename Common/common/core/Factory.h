#pragma once

#include "Object.h"

_COMMON_NS_BEGIN

class _CommonExport Factory
{
public:
	Factory();
	virtual ~Factory();


public:
	virtual void
		Init(DWORD dwInitData) = 0;
	virtual void
		Destroy() = 0;

	virtual const vector<DWORD>&
		GetSupporttedTypes() = 0;
		
	virtual Object*
		CreateObject(DWORD dwCreationData) = 0;


};

DECLARE_MAP_TYPE_PTR(map, DWORD, Factory);


_COMMON_NS_END