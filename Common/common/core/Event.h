#pragma once

#include "common/core/ReferenceCounted.h"

_COMMON_NS_BEGIN

class ScriptBinder;

class _CommonExport Event
{
	friend class ScriptEventBind;

public:
	static const int EMPTY_TYPE = -1;

	union
	{
		int		nType;
		DWORD	timerID;
	};

	void* 
		pSourceObj;

	Event(int type)
	{
		XASSERT(type != EMPTY_TYPE);

		nType = type;
		pSourceObj = NULL;
	}

protected:
	virtual int
		_prepareScriptArguments(ScriptBinder* pScriptBinder);


};

_COMMON_NS_END