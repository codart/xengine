// EventDispatcher.cpp: implementation of the CEventDispatcher class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EventDispatcher.h"
#include "algorithm"

#include "common/script/ScriptBinder.h"
#include "Common/script/EventDelegator.h"

_COMMON_NS_BEGIN

//////////////////////////////////////////////////////////////////////////
EventBinder::EventBinder()
{
	__bHasCachedFunctors = FALSE;
}

EventBinder::EventBinder(int nEventType)
{
	this->nEventType = nEventType;
	__bHasCachedFunctors = FALSE;
}

EventBinder::~EventBinder()
{
	ResetBinder();
}

void EventBinder::ResetBinder()
{
	_processCachedFunctors();

	nEventType = 0;

	_aMemberFunctors.clear();
	__deadMemberFunctors.clear();
	__addedMemberFunctors.clear();
}

BOOL EventBinder::IsEventListener(MemberFunctor& functor)
{
	MemberFunctorListIt it = find(_aMemberFunctors.begin(), _aMemberFunctors.end(), functor);

	if(it != _aMemberFunctors.end())
	{
		// Check if dead.
		VecItMemberFunctorIt itIt = find(__deadMemberFunctors.begin(), __deadMemberFunctors.end(), it);
		
		if(itIt == __deadMemberFunctors.end())
		{
			// Not dead, the func is really a listener.
			return TRUE;
		}
		else
		{
			// If dead, still need Check cached added functors (see below)
		}
	}

	// Check cached added functors
	MemberFunctorListIt it2 = find(__addedMemberFunctors.begin(), 
		__addedMemberFunctors.end(), functor);

	if(it2 !=__addedMemberFunctors.end()) return TRUE;
	else return FALSE;
}

int EventBinder::BindMemberFunction(MemberFunctor* pMemberFunctor, BOOL bPushToAddedList)
{
	XASSERT(pMemberFunctor);

	if(bPushToAddedList)
	{
#ifdef _DEBUG
		MemberFunctorListIt it = GetMemFunctorIterator(pMemberFunctor);

		XASSERT(it == _aMemberFunctors.end());

		// Check if added more than once. It should be bug if bind a functor time and time.
		MemberFunctorListIt it2 = find(__addedMemberFunctors.begin(), 
							__addedMemberFunctors.end(), *pMemberFunctor);
		XASSERT(it2 == __addedMemberFunctors.end());

		// Check if the functor is dead. Is should be bug if unbind/bind a function at a time.
		VecItMemberFunctorIt it3 = find(__deadMemberFunctors.begin(), 
								__deadMemberFunctors.end(), it);

		XASSERT(it3 == __deadMemberFunctors.end());

#endif 
		__addedMemberFunctors.push_back(*pMemberFunctor);
		__bHasCachedFunctors = TRUE;
	}
	else
	{
		// Make sure not bind more than once.
		XASSERT(GetMemFunctorIterator(pMemberFunctor) == _aMemberFunctors.end());
		_aMemberFunctors.push_back(*pMemberFunctor);
	}

	return 1;
}

int EventBinder::UnbindMemberFunction(MemberFunctor* pMemberFunctor, BOOL bPushToDeadList)
{
	XASSERT(pMemberFunctor);

	MemberFunctorListIt it = GetMemFunctorIterator(pMemberFunctor);
	if(it == _aMemberFunctors.end()) return 0;

	MemberFunctor* pMemFunctor = &(*it);
	XASSERT(pMemFunctor);

	if(bPushToDeadList)
	{
		VecItMemberFunctorIt it2 = find(__deadMemberFunctors.begin(), 
			__deadMemberFunctors.end(), it);

		if(it2 == __deadMemberFunctors.end())
		{
			__deadMemberFunctors.push_back(it);
			__bHasCachedFunctors = TRUE;
		}
	}
	else _aMemberFunctors.erase(it);

	return 0;
}

int EventBinder::DispatchEvent(Event& event)
{
	FASTEST_VECTOR_ITERATE(MemberFunctor, _aMemberFunctors);
		GET_NEXT(_aMemberFunctors)(&event);
	FASTEST_ITERATE_END();

	_processCachedFunctors();

	return 0;
}

void EventBinder::_processCachedFunctors()
{
	// Process dead/added functors. These functor is remove/add during this event is dispatching, so need
	// cached them and processed here. 
	if (__bHasCachedFunctors)
	{
		FASTEST_VECTOR_ITERATE(MemberFunctorListIt, __deadMemberFunctors);
			_aMemberFunctors.erase(GET_NEXT(__deadMemberFunctors));
		FASTEST_ITERATE_END();

		__deadMemberFunctors.clear();

		FASTEST_VECTOR_ITERATE(MemberFunctor, __addedMemberFunctors);
			_aMemberFunctors.push_back(GET_NEXT(__addedMemberFunctors));
		FASTEST_ITERATE_END();

		__addedMemberFunctors.clear();

		__bHasCachedFunctors = FALSE;
	}
}

MemberFunctorListIt EventBinder::GetMemFunctorIterator(MemberFunctor* pMemberFunctor)
{
	XASSERT(pMemberFunctor);
	MemberFunctorListIt it = _aMemberFunctors.begin();
	while(it != _aMemberFunctors.end())
	{
		if( *it == *pMemberFunctor) return it;
		++it;
	}
	return _aMemberFunctors.end();
}

//////////////////////////////////////////////////////////////////////////
EventDispatcher::EventDispatcher()
{
	_curDispatchedEventType = Event::EMPTY_TYPE;
	_pScriptEventDelegator = NULL;
}

EventDispatcher::~EventDispatcher()  
{
	//Release binders info
	EventBindersIter it = _aEventBindersInfo.begin();
	while(it != _aEventBindersInfo.end())
	{
		XASSERT((*it).second);
		(*it).second->ResetBinder();
		_EventBinderPool::Instance().FreeInstance((*it).second);
		++it;
	}
	_aEventBindersInfo.clear();

	SAFE_DELETE(_pScriptEventDelegator);
}

void EventDispatcher::EnableScriptSupport(BOOL bEnable, ScriptBinder* pScriptBinder)
{
	if(bEnable)
	{
		XASSERT(pScriptBinder);
		if(!_pScriptEventDelegator)
		{
			_pScriptEventDelegator = XNEW ScriptEventDelegator(pScriptBinder);
		}
		else
		{
			_pScriptEventDelegator->SetScriptBinder(pScriptBinder);
		}
	}
	else
	{
		//... Remove all event binds on _pScriptEventDelegator
		SAFE_DELETE(_pScriptEventDelegator);
	}
}

int EventDispatcher::AddEventListener(int nEventType, const char* pszFuncName)
{
	XASSERT(_pScriptEventDelegator);
	ScriptEventBind* pEventBind = _pScriptEventDelegator->GetEventBind(pszFuncName);
	XASSERT(pEventBind);

	return AddEventListener(nEventType, MFUNC(&ScriptEventBind::OnEvent, pEventBind));
}

int EventDispatcher::AddEventListener(int nEventType, MemberFunctor functor)
{
	EventBinder* pBinder = GetBinderByEventType(nEventType, TRUE);

	pBinder->BindMemberFunction(&functor, _curDispatchedEventType == nEventType);

	return 0;
}

int EventDispatcher::RemoveEventListener(int nEventType, MemberFunctor functor)
{
	//.. Need optimize
	EventBinder* pBinder = GetBinderByEventType(nEventType);
	if(!pBinder) return -1;

	pBinder->UnbindMemberFunction(&functor, _curDispatchedEventType == nEventType);

	//.. Need delete binder here or use RemoveEvent() to remove binder explicitly?
	//.. Also, as the UnbindMemberFunction only push the functor to dead list, if 
	//.. we delete binder here, will cause crash here?
	//if(0 == pBinder->GetMemberFuncCount()) DeleteBinderByEventType(pBinder);

	return 0;
}


BOOL EventDispatcher::IsEventListener(int nEventType, MemberFunctor functor)
{
	EventBinder* pBinder = GetBinderByEventType(nEventType);
	if(!pBinder) return -1;

	return pBinder->IsEventListener(functor);
}

void EventDispatcher::DispatchEvent(Event& event)
{
	EventBinder* pBinder = GetBinderByEventType(event.nType, TRUE);
	XASSERT(pBinder);

	_curDispatchedEventType = event.nType;

	pBinder->DispatchEvent(event);

	_curDispatchedEventType = Event::EMPTY_TYPE;
}

void EventDispatcher::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	EventBindersIter it = _aEventBindersInfo.find(nEventType);
	if(it != _aEventBindersInfo.end())
	{
		DeleteBinderByEventType(it);
	}
}

_COMMON_NS_END