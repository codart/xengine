#include "common/core/exception.h"

_COMMON_NS_BEGIN

class _CommonExport SingletonException: public Exception
{
public:
	SingletonException():Exception("Singleton class cannot be instance directly!")
	{
	}

};



_COMMON_NS_END