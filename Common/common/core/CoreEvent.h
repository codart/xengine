#pragma once

#include "Event.h"

_COMMON_NS_BEGIN

class CoreEvent : public Event
{
public:
	enum CoreEventType
	{
		CORE_EVENT_BASE = 400,

		CORE_INIT,
		CORE_UNINIT,

		CORE_FRAME_TICK,
	};

	CoreEvent(int nType) : Event(nType) {}
};

_COMMON_NS_END