#pragma once

_COMMON_NS_BEGIN

class IModule;

typedef common::IModule* (*FuncCreateModule) ();
typedef void (*FuncDestroyModule) ();

/* 
	All modules should register itself to 'Core' when it is loaded. X3D's DllMain is an example.
	There are different module types. 
	1. Static linked modules. These modules must be static lined to application because they export classes. 
		'common' & 'x3d' are such type.
	2. Dynamic linked modules. These modules only export some 'manager functions', these functions can create 
	 'manager classes' and all other objects can be created with these 'manager class'.
	 "xsound" & "xnetwork" are such type.
	
*/
struct ModuleInfo
{
	ModuleInfo(FuncCreateModule pCreateFunc, FuncDestroyModule pDestroyFunc
		, BOOL bNeedTick = false, int iTickOrder = -1)
	{
		this->pModule = NULL;
		this->bNeedTick = bNeedTick;
		this->iTickOrder = iTickOrder;
		this->pCreateFunc = pCreateFunc;
		this->pDestroyFunc = pDestroyFunc;
	}

	void ConstructModule() { XASSERT(!pModule); pModule = pCreateFunc(); }

	IModule* pModule;
	FuncCreateModule pCreateFunc;
	FuncDestroyModule pDestroyFunc;
	BOOL bNeedTick;
	int iTickOrder;
};

DECLARE_LIST_TYPE(std::vector, ModuleInfo);

class _CommonExport Core : implements IEventDispatcher
{
public:
	Core();
	virtual ~Core();

public:
	virtual void
		LoadModulesFromINI(const String& iniFile = TEXT("core.ini"));

	virtual void 
		RegisterModule(FuncCreateModule pCreateFunc, FuncDestroyModule pDestroyFunc
					, BOOL bNeedTick = false, int iTickOrder = -1);

	virtual void 
		RegisterModule(const String& strModuleName, BOOL bNeedTick = FALSE, int iTickOrder = -1);


	virtual void 
		Init();
	virtual void 
		Start();
	virtual void 
		Uninit();

	virtual float
		GetFPS();
	// nFrameRate : Frame rate per second.
	virtual void
		SetMaxFPS(UINT nFrameRate = INFINITE);


	virtual DWORD_PTR
		GetModuleHandle() { return _hModuleHandle; }
	virtual void
		setModuleHandle(DWORD dwHandle) { _hModuleHandle = dwHandle; }

	virtual void 
		EnableConsole(BOOL bEnable);

	// Implements IEventDispatcher
	override void  
		DispatchEvent(Event& event);

	/*
	If set bWeakReference to false, AddEventListener will add a reference to pReceiver. Incorrect use of this parameter 
	may cause recursive reference.
	*/
	override int 
		AddEventListener(int nEventType
							, MemberFunctor functor);

	override int 
		RemoveEventListener(int nEventType
									, MemberFunctor functor);

	/* 
	Return binder to BinderPool
	*/
	override void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = false);

	// Check whether the member func is listen the specified event.
	override BOOL 
		IsEventListener(int nEventType, MemberFunctor functor){ XASSERT(0); return FALSE; }


protected:
	// return value indicate whether received WM_QUIT message.
	INLINE BOOL 
		_limitFrameRate(float fRealDeltaTime);
	INLINE void 
		_updateStatus(float fRealDeltaTime);


protected:
	ModuleInfoList
		_moduleList;

	float
		_fFPS;
	// When enabled, engine will limit frame interval to _bYieldCPUTime to free CPU time. Default is disabled.
	BOOL
		_bLimitFrameRate;
	// When _bYieldCPUTime is true, will limit frame interval to this value. Default value is 0.01.
	float 
		_fFrameInterval;
	DWORD_PTR
		_hModuleHandle;

	EventDispatcher*
		_pEventDispatcher;

private:
	HANDLE 
		__hInfiniteLockHandle;

	// The number of frames that have occurred.
	float 
		__nframeCount;   
	// The time that has elapsed since last reset.
	float 
		__fTimeElapsed;	

	// The file stream used for redirect the stdout
	FILE*
		__stdoutFileStream;

	std::streambuf*
		__consoleStreamBuf;

	std::streambuf*
		__oldConsoleStreamBuf;

	//---- for frame smoothing ----
	float 
		__fSmoothedDeltaTime;
	float 
		__fAvgDeltaTime;
	float 
		__fAveragePeriod;
	float 
		__fSmoothFactor;


};


_COMMON_NS_END