//////////////////////////////////////////////////////////////////////////
/*	EventDispatcher.h

  Description: Core classes that support bind event to member function

  Author: Spark Xie
	
  Simple Usage: 
	1. Make your event source class inherit EventDispatcher
	2. If you need reference counter, make your listener inherited from ReferenceCounted.
	3. Define handler member function in receiver class, it should has type like
		void OnXXXEvent(Event* pEvent);
	4. Use AddEventListener to bind receiver's member function to event
	5. Fire events in event source class with DispatchEvent(Event& event)

	eg. 
		class EventHandler : public ReferenceCounted
		{
		public:
			void OnEvent(Event* pEvent)
			{
			}
		};
		//....
		AutoPtr<EventHandler> pHandler = new EventHandler;
		AddEventListener(KeyboardEvent::KEY_DOWN, pHandler, MFUNC(&EventHandler::OnEvent), FALSE);
		RemoveEventListener(KeyboardEvent::KEY_DOWN, pHandler, MFUNC(&EventHandler::OnEvent));

  Advanced Usage:
  You can bind event to a virtual functions(such as functions inherit from pure virtual class) 
  too. But you must comply to following rules, or else it may lead to unexpect results.
  1. Use interface's pointer instead normal "this" pointer for pReceiver.
  2. and use MFUNC to get Interface's function address for for pHandleFunc
  eg.
	  class Interface
	  {
			virtual void IFunc1() = 0;
	  }
	  class Test : public Interface
	  {
			virtual void IFunc1()
			{
				...
			}
	  }

	  Test* pTest = new Test;
	  AddEventListener(KeyboardEvent::KEY_DOWN, (Interface*)pTest, MFUNC(&Interface::IFunc1));
	
*/////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#pragma warning(disable : 4251)

#include "common/core/ReferenceCounted.h"
#include "common/algorithm/ObjectPool.h"
#include "common/core/Event.h"
#include "common/core/IEventDispatcher.h"
#include "Common/core/Singleton.h"

_COMMON_NS_BEGIN

#define MFUNC	BIND_MEM_CB			/// Bind member function
#define FFUNC	BIND_FREE_CB		/// Bind free function (global/static function)

// ---------- Util macros -----------
#define DECLARE_EVENT_RECEIVER(ReceiverClass)		\
	typedef ReceiverClass Receiver;

#define AddListener(eventType, handlerFunc)		\
	AddEventListener(eventType, MFUNC(&Receiver::handlerFunc, this))

#define RemoveListener(eventType, handlerFunc)		\
	RemoveEventListener(eventType, MFUNC(&Receiver::handlerFunc, this))

#define IsListener(eventType, handlerFunc)	\
	IsEventListener(eventType, MFUNC(&Receiver::handlerFunc, this))


typedef std::vector<MemberFunctor> MemberFunctorList;
typedef MemberFunctorList::iterator MemberFunctorListIt;

typedef std::vector<MemberFunctor> FinalFunctorList;
typedef FinalFunctorList::iterator FinalFunctorListIt;

class EventBinder;

//////////////////////////////////////////////////////////////////////////
class _EventBinderPool : public Singleton<_EventBinderPool, AutoRelease>
						, public ObjectPool<EventBinder*> {};

class _CommonExport EventBinder
{
	friend class EventDispatcher;
public:
	int nEventType;

	MemberFunctorList
		_aMemberFunctors;


public:
	EventBinder(int nEventType);
	EventBinder();
	~EventBinder();

	final int 
		BindMemberFunction(MemberFunctor* pMemberFunctor, BOOL bPushToAddedList);
	final int 
		UnbindMemberFunction(MemberFunctor* pMemberFunctor, BOOL bPushToDeadList);

	INLINE final int 
		DispatchEvent(Event& event);
	
	INLINE final void
		ResetBinder();
	INLINE final BOOL
		IsEventListener(MemberFunctor& functor);


private:
	typedef std::vector<MemberFunctorListIt>::iterator VecItMemberFunctorIt;

	std::vector<MemberFunctorListIt>
		__deadMemberFunctors;
	MemberFunctorList
		__addedMemberFunctors;

	BOOL
		__bHasCachedFunctors;

private:
	INLINE void 
		_processCachedFunctors();
	INLINE MemberFunctorListIt 
		GetMemFunctorIterator(MemberFunctor* pFunc);

};

typedef std::map<int, EventBinder*>::iterator EventBindersIter;

class ScriptBinder;

class _CommonExport EventDispatcher : implements IEventDispatcher
{
public: 
	EventDispatcher();
	~EventDispatcher();


public:
	override final void 
		DispatchEvent(Event& event);

	INLINE override final int 
		AddEventListener(int nEventType, MemberFunctor functor);

	override final int 
		RemoveEventListener(int nEventType, MemberFunctor functor);

	override final void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = false);

	// Check whether the member func is listen the specified event.
	override BOOL
		IsEventListener(int nEventType, MemberFunctor memberFunctor);

	override void
		EnableScriptSupport(BOOL bEnable, ScriptBinder* pScriptBinder = NULL);
	override int 
		AddEventListener(int nEventType, const char* pszFuncName);


protected:	
	//Event type as map's key
	std::map<int, EventBinder*> 
		_aEventBindersInfo;
	int
		_curDispatchedEventType;
	ScriptEventDelegator* 
		_pScriptEventDelegator;


protected:
	INLINE final EventBinder*  
		GetBinderByEventType(int nEventType, BOOL bCreateNew = FALSE)
	{
		EventBindersIter it = _aEventBindersInfo.find(nEventType);

		if(it != _aEventBindersInfo.end()) return (*it).second;
		else if(bCreateNew)
		{
			//Create a new event binder for this event type
			EventBinder* pEventBinder = _EventBinderPool::Instance().GetInstance();
			pEventBinder->nEventType = nEventType;
			XASSERT(pEventBinder);
			_aEventBindersInfo.insert(std::map<int, EventBinder*>::value_type(nEventType, 
				pEventBinder));

			return pEventBinder;
		}
		return NULL;
	}

private:
	/************************************************************************
	Note: The following methods will allocate\free memory. These ensure the memory is 
	allocate\free in the same module (XEngine's module).
	************************************************************************/
	INLINE void
		DeleteBinderByEventType(EventBindersIter it)
	{
		(*it).second->ResetBinder();
		_EventBinderPool::Instance().FreeInstance((*it).second);
		_aEventBindersInfo.erase(it);
	}
};


_COMMON_NS_END