#include "common/core/exception.h"

_COMMON_NS_BEGIN

class SingletonNotInstanced: public Exception
{
public:
	SingletonNotInstanced():Exception("Singleton class be called before instanced")
	{
	}
	
};

_COMMON_NS_END