#include "StdAfx.h"
#include "SingletonBase.h"
#include "_SingletonMgr.h"

_COMMON_NS_BEGIN

AutoRelease::AutoRelease()
{
	_SingletonMgr::RegisterSingleton(this);
}

AutoRelease::~AutoRelease()
{
}

_COMMON_NS_END