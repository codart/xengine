#pragma once

#include <vector>
#include <algorithm>
#include <assert.h>

_COMMON_NS_BEGIN


// User EventClass to specify what event will be listener
template<typename EventClass>
class _EventListener
{
public:
	virtual void OnEvent(const EventClass& event) = 0;
};


// User EventClass to specify what event will be dispatched
template<typename EventClass>
class _EventDispatcher
{
public:
	typedef _EventListener<EventClass> ListenerClass;
	typedef vector<ListenerClass*> ListenerList;
	typedef typename ListenerList::iterator ListenerListIt;

public:
	INLINE void
		AddEventListener(ListenerClass* pListener)
	{
		XASSERT(_getListener(pListener) == _listeners.end());
		_listeners.push_back(pListener);
	}

	INLINE void
		RemoveEventListener(ListenerClass* pListener)
	{
		ListenerListIt it = _getListener(pListener);

		if(it != _listeners.end()) _listeners.erase(it);
	}

	INLINE void 
		DispatchEvent(const EventClass& event)
	{
		ListenerListIt it = _listeners.begin();
		ListenerListIt itEnd = _listeners.end();
		//for( ; it != itEnd; ++it)
		while(it != itEnd)
		{
			(*it)->OnEvent(event);
			++it;
		}
	}

protected:
	ListenerList
		_listeners;


protected:
	ListenerListIt _getListener(ListenerClass* pListener)
	{
		return find(_listeners.begin(), _listeners.end(), pListener);
	}

};


// Util macros for declare memeber dispacher
#define DECLARE_EVENT_DISPATCHER(EventClass)	\
	_EventDispatcher<EventClass>		_##EventClass##Dispatcher;	\
																\
	INLINE void Add##EventClass##Listener(_EventListener<EventClass>* pListener)	\
	{	\
		_##EventClass##Dispatcher.AddEventListener(pListener);	\
	}	\
		\
	INLINE void Remove##EventClass##Listener(_EventListener<EventClass>* pListener)	\
	{	\
		_##EventClass##Dispatcher.RemoveEventListener(pListener);	\
	}	\
		\
	INLINE void Dispatch##EventClass(const EventClass& event)	\
	{	\
		_##EventClass##Dispatcher.DispatchEvent(event);		\
	}	


_COMMON_NS_END
