#pragma once
#include "common/core/exception.h"

_COMMON_NS_BEGIN


class NoImplementationException : public Exception
{
public:
	NoImplementationException(void):Exception("Required Interface/method not implemented!!!")
	{
	}
};


_COMMON_NS_END