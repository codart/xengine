#include "stdafx.h"
#include "event.h"
#include "Common/script/ScriptBinder.h"

_COMMON_NS_BEGIN

int Event::_prepareScriptArguments(ScriptBinder* pScriptBinder)
{
	// Push first argument
	pScriptBinder->PushNumber(nType);

	return 1;
}

_COMMON_NS_END