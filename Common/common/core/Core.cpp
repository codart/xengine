#include "stdafx.h"
#include "core.h"

#include "CoreEvent.h"
#include "IModule.h"
#include "common/time/TimerMgr.h"
#include "common/core/_SingletonMgr.h"
#include <iostream>

#pragma warning(disable:4996) 

_COMMON_NS_BEGIN

using namespace std;


class __ConsoleOutBuf : public std::streambuf 
{
public:
	__ConsoleOutBuf()
	{
		setp(0, 0);
	}

	virtual int_type overflow(int_type c = traits_type::eof()) 
	{
		return fputc(c, stdout) == EOF ? traits_type::eof() : c;
	}
};


Core::Core()
{
	_fFPS = 0;
	__nframeCount = 0;
	__fTimeElapsed = 0;

	// 10 microseconds
	_fFrameInterval = 0.01f; 
	_bLimitFrameRate = FALSE;

	__hInfiniteLockHandle = NULL;

	_hModuleHandle = NULL;
	_pEventDispatcher = NULL;

	__stdoutFileStream = NULL;
	__consoleStreamBuf = NULL;

	__fSmoothedDeltaTime = 0.01666666666667f;
	__fAvgDeltaTime = 0.01666666666667f;
	__fAveragePeriod = 60.0f;
	__fSmoothFactor = 0.3f;
}

Core::~Core()
{
}

void Core::Init()
{
	_EventBinderPool::Instance().SetMaxSize(0xffffffff);
	_EventBinderPool::Instance().SetPoolSize(200);

	XASSERT(!_pEventDispatcher);
	_pEventDispatcher = XNEW EventDispatcher;

	/// Init plug-in modules
	ModuleInfoListIt it = _moduleList.begin();
	ModuleInfoListIt itEnd = _moduleList.end();

	for ( ; it != itEnd; ++it)
	{
		ModuleInfo& info = *it;

		info.ConstructModule();
		_pEventDispatcher->AddEventListener(CoreEvent::CORE_INIT, MFUNC(&IModule::Init, info.pModule));

		if(info.bNeedTick)
		{
			//if(iTickOrder == -1)
			{
				_pEventDispatcher->AddEventListener(CoreEvent::CORE_FRAME_TICK, MFUNC(&IModule::Tick, info.pModule));
			}
			//else
			{
				// Add to specified tick order
				// ...
			}
		}
	}

	vector<ModuleInfo>::reverse_iterator rIt = _moduleList.rbegin();
	vector<ModuleInfo>::reverse_iterator rItEnd = _moduleList.rend();

	for ( ; rIt != rItEnd; ++rIt)
	{
		ModuleInfo& info = *rIt;
		_pEventDispatcher->AddEventListener(CoreEvent::CORE_UNINIT, MFUNC(&IModule::Uninit, info.pModule));
	}

	//-----
	__hInfiniteLockHandle = CreateEvent(NULL, TRUE, FALSE, NULL);
	XASSERT(__hInfiniteLockHandle);

	CoreEvent e(CoreEvent::CORE_INIT);
	_pEventDispatcher->DispatchEvent(e);
}

void Core::Start()
{
	for(;;)
	{
		// Update timer
		float fRenderTime = _TimerMgr::EndFrame();
		
		if(_limitFrameRate(fRenderTime)) break;

		// Smooth frame time
		__fAvgDeltaTime = (::GetDeltaTime() + __fAvgDeltaTime * (__fAveragePeriod - 1)) / __fAveragePeriod;

		// Calculate a better approximation for smooth delta time
		// Smooth delta time will reduce flicker when FPS changed dramatically.
		__fSmoothedDeltaTime = __fSmoothedDeltaTime + (__fAvgDeltaTime - __fSmoothedDeltaTime) * __fSmoothFactor;
		_TimerMgr::_AdjustDeltaTime(__fSmoothedDeltaTime);

		_updateStatus(__fSmoothedDeltaTime);

		//----- frame start ---- current time
		_TimerMgr::BeginFrame();

		//--------
		_TimerMgr::DispatchTimerEvent();

		CoreEvent coreEvent(CoreEvent::CORE_FRAME_TICK);
		_pEventDispatcher->DispatchEvent(coreEvent);

		MSG msg;
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{ 
			if (msg.message == WM_QUIT)	return;

			if(msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST) continue;
			if(msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) continue;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

void Core::Uninit()
{
	CloseHandle(__hInfiniteLockHandle);
	__hInfiniteLockHandle = NULL;

	CoreEvent e(CoreEvent::CORE_UNINIT);
	_pEventDispatcher->DispatchEvent(e);

	ModuleInfoListIt it = _moduleList.begin();
	ModuleInfoListIt itEnd = _moduleList.end();

	for ( ; it != itEnd; ++it)
	{
		ModuleInfo& info = *it;

		XASSERT(info.pModule);
		info.pDestroyFunc();
		info.pModule = NULL;
	}

	SAFE_DELETE(_pEventDispatcher);

	_SingletonMgr::UnInit();

	//----
	if(__oldConsoleStreamBuf)
	{
		std::cout.rdbuf(__oldConsoleStreamBuf);
	}

	if(__stdoutFileStream)
	{
		fclose(__stdoutFileStream);
		__stdoutFileStream = NULL;
	}

	SAFE_DELETE(__consoleStreamBuf);
}

void Core::DispatchEvent(Event& event)
{
	XASSERT(_pEventDispatcher);
	_pEventDispatcher->DispatchEvent(event);
}

int Core::AddEventListener(int nEventType
							, MemberFunctor functor)
{
	XASSERT(_pEventDispatcher);
	return _pEventDispatcher->AddEventListener(nEventType, functor);
}


int Core::RemoveEventListener(int nEventType
									, MemberFunctor functor)
{
	XASSERT(_pEventDispatcher);
	return _pEventDispatcher->RemoveEventListener(nEventType, functor);
}

void Core::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	XASSERT(_pEventDispatcher);
	_pEventDispatcher->RemoveEvent(nEventType, bLazyRelease);
}

void Core::LoadModulesFromINI(const String& iniFile)
{
	XASSERT(0);
}

void Core::RegisterModule(FuncCreateModule pCreateFunc, FuncDestroyModule pDestroyFunc
						  , BOOL bNeedTick, int iTickOrder)
{
	_moduleList.push_back(ModuleInfo(pCreateFunc, pDestroyFunc, bNeedTick, iTickOrder));
}

void Core::RegisterModule(const String& strModuleName, BOOL bNeedTick, int iTickOrder)
{
	HDYNAMICLIB hDynamicLib = PlatformUtil::LoadDynamicLib(strModuleName);
	XASSERT(hDynamicLib);

	String strCreateFuncName;
	String strDestroyFuncName;
	int nPos = strModuleName.find(TEXT('.'));
	
	strCreateFuncName = TEXT("Create");
	strCreateFuncName += strModuleName.substr(0, nPos);
	strCreateFuncName += TEXT("Module");

	strDestroyFuncName = TEXT("Destroy");
	strDestroyFuncName += strModuleName.substr(0, nPos);
	strDestroyFuncName += TEXT("Module");

	FuncCreateModule pCreateFunc = (FuncCreateModule)PlatformUtil::GetFuncAddress(hDynamicLib, strCreateFuncName);
	FuncDestroyModule pDestroyFunc = (FuncDestroyModule)PlatformUtil::GetFuncAddress(hDynamicLib, strDestroyFuncName);

	ModuleInfo info(pCreateFunc, pDestroyFunc, bNeedTick, iTickOrder);
	XASSERT(!info.pModule);
	info.pModule = info.pCreateFunc();
	_moduleList.push_back(info);
}

float Core::GetFPS()
{
	return _fFPS; 
}

void Core::SetMaxFPS(UINT nframeRate)
{
	if(nframeRate == INFINITE) 
	{
		_bLimitFrameRate = FALSE;
	}
	else
	{
		_bLimitFrameRate = TRUE;
		_fFrameInterval = 1 / (float)nframeRate;
		__fSmoothedDeltaTime = __fAvgDeltaTime = _fFrameInterval;
	}
}

void Core::_updateStatus(float fDeltaTime)
{
	// Calculate FPS 
	__nframeCount++;
	__fTimeElapsed += fDeltaTime;

	if(__fTimeElapsed >= 1.0f)
	{
		_fFPS = __nframeCount / __fTimeElapsed;

		__fTimeElapsed	= 0;
		__nframeCount	= 0;
	}
}

/*
Hardware framebuffer swap will takes a considerable amount of time (especially if the 
video driver is caching instructions). Therefore you have to take that into account 
or you'll end with a lower frame rate than desired. So the thing should be:

somewhere at the start (like the constructor):
startTime = System.currentTimeMillis();

then in the render loop:
public void onDrawFrame(GL10 gl)
{    
	endTime = System.currentTimeMillis();
	dt = endTime - startTime;
	startTime = System.currentTimeMillis();

	if (dt < 33)
		Thread.Sleep(33 - dt);

	UpdateGame(dt);
	RenderGame(gl);
}

This way you will take into account the time it takes to swap the buffers and the time to draw the frame.
*/
BOOL Core::_limitFrameRate(float fRealDeltaTime)
{
	if(!_bLimitFrameRate) return FALSE;

	// Yield CPU time
	if(fRealDeltaTime >= _fFrameInterval) return FALSE;

	float fSleepInterval = _fFrameInterval - fRealDeltaTime;
	float fTimeBeforeWait = ::GetTime();

	_TimerMgr::_AdjustDeltaTime(fRealDeltaTime + fSleepInterval);

	BOOL bQuit = FALSE;
	BOOL bWaitLoopEnd = FALSE;

	while(!bWaitLoopEnd)
	{
		DWORD dwSleepTime = DWORD(fSleepInterval * 1000.0f);
		DWORD dwWaitRet = ::MsgWaitForMultipleObjects(1, &__hInfiniteLockHandle, FALSE
			, dwSleepTime, QS_ALLINPUT);

		switch (dwWaitRet)
		{
		case WAIT_OBJECT_0:
			XASSERT(0);
			break;

		case WAIT_TIMEOUT:
			bWaitLoopEnd = TRUE;
			break;

		default:
			float fCurrentTime = GetAccurateTime();
			float fTimeWaitted = fCurrentTime - fTimeBeforeWait;
			fTimeBeforeWait = fCurrentTime;
			fSleepInterval = fSleepInterval - fTimeWaitted;
			if(fSleepInterval <= 0) 
			{
				bWaitLoopEnd = TRUE;
				break;
			}

			MSG msg;
			while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{ 
				if (msg.message == WM_QUIT)
				{
					bQuit = TRUE;
					bWaitLoopEnd = TRUE;
					break;
				}

				if(msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST) continue;
				if(msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) continue;

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	return bQuit;
}

void Core::EnableConsole(BOOL bEnable)
{
	if(bEnable)
	{
		if(__stdoutFileStream)
		{
			fclose(__stdoutFileStream);
			__stdoutFileStream = NULL;
		}

		if(AllocConsole()) 
		{
			freopen("CONOUT$", "w", stdout);
			SetConsoleTitle(TEXT("Debug Console"));
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);  

			// set std::cout to use my custom streambuf			
			if(!__consoleStreamBuf)
			{
				__consoleStreamBuf = new __ConsoleOutBuf();
			}

			__oldConsoleStreamBuf = std::cout.rdbuf(__consoleStreamBuf);
		}
	}
	else
	{
		FreeConsole();

		if(!__stdoutFileStream)
		{
			__stdoutFileStream = freopen("Lua_error.txt", "w", stdout);
		}

		if(__consoleStreamBuf)
		{
			SAFE_DELETE(__consoleStreamBuf);
		}

		if(__stdoutFileStream == NULL)
		{
			XTHROWEX("Redirect stdout to Lua_error.txt failed!");
		}
	}
}

_COMMON_NS_END

