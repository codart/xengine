#pragma once


_COMMON_NS_BEGIN


class _CommonExport MessageQueue
{
public:
	 MessageQueue();
	 virtual ~MessageQueue();


public:
	/// Post a message into message queue
	virtual void PostMessage();
	/// Send a message into message queue, return util message be processed.
	virtual void SendMessage();
	
	/// The PeekMessage function dispatches incoming sent messages, checks the thread message queue for 
	/// a posted message, and retrieves the message (if any exist). 
	virtual void PeekMessage();

	/// The GetMessage function retrieves a message from the calling thread's message queue. The function dispatches 
	/// incoming sent messages until a posted message is available for retrieval. 
	virtual void GetMessage();

	/// The WaitMessage function yields control to other threads when a thread has no other messages in its message queue.
	/// The WaitMessage function suspends the thread and does not return until a new message is placed in the thread's 
	/// message queue. 
	virtual void WaitMessage();


protected:


protected:


};


_COMMON_NS_END