//////////////////////////////////////////////////////////////////////////
/*	Singleton template

Description: Class template that make use singleton more easy and automatic

Author: Spark Xie

Usage:
class TestSingleton : public Singleton<TestSingleton>
{}

If you want your singleon instance be released after APP end, use
class TestSingleton : public Singleton<TestSingleton, AutoRelease>
{}

*/////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/core/SingletonBase.h"
#include "common/core/singletonException.h"
#include "common/core/singletonNotInstanced.h"
#include "common/core/Final.h"

_COMMON_NS_BEGIN

enum SingletonState
{
	SINGLETON_UNINITIALIZED,
	SINGLETON_INITIALIZED,
	SINGLETON_DESTROYED,
};

template<typename UserClass, class Base = ManualRelease>
class Singleton : virtual public ForbidInherit<UserClass, Singleton<UserClass, Base>>
				, public Base
{
public:
	static inline UserClass& Instance()
	{
		if(s_pInstance == NULL)
		{
			if(s_singletonState == SINGLETON_DESTROYED)
			{
				XTHROWEX("Singleton instance already destroyed. May be the engine already uninitialized.");
			}
			else
			{
				s_singletonState = SINGLETON_INITIALIZED;
				s_pInstance = new UserClass;
			}
		}

		XASSERT(s_singletonState == SINGLETON_INITIALIZED);
		return *s_pInstance;
	}

protected:
	Singleton()
	{
		if(s_pInstance) XTHROW(SingletonException);
		if(!s_singletonState) XTHROW(SingletonException);
	}

	virtual ~Singleton()
	{
		s_singletonState = SINGLETON_DESTROYED;
		s_pInstance = NULL;
	}

protected:
	static UserClass*
		s_pInstance;
	static SingletonState
		s_singletonState;
	static BOOL
		s_bPreInit;
};

template<class UserClass, class Base>
UserClass* Singleton<UserClass, Base>::s_pInstance = NULL;

template<class UserClass, class Base>
SingletonState Singleton<UserClass, Base>::s_singletonState = SINGLETON_UNINITIALIZED;

template<class UserClass, class Base>
BOOL Singleton<UserClass, Base>::s_bPreInit = FALSE;

_COMMON_NS_END