#pragma once

#include "XmlNodeHandle.h"

_COMMON_NS_BEGIN

/*
	A wrapper for rapidxml::xml_document<char>
*/
class _CommonExport XmlDocument : public XmlNodeHandle
{
	friend class XmlNodeHandle;

public:
	XmlDocument(void);
	~XmlDocument();

public:
	// Parse a xml file.
	void 
		LoadFromFile(const AString& strFileName);
	void 
		SaveToFile(const AString& strFileName);
	void 
		ParseString(const AString& strXMLString);

	// Append to root node
	XmlNodeHandle
		AppendNode(const AString& strNodeName, const AString& strNodeValue = "");
	// Append child node to designated node.
	XmlNodeHandle
		AppendNode(XmlNodeHandle& parentNodeHandle, const AString& strNodeName
		, const AString& strNodeValue = "");

	// Assign attribute to designated node.
	XmlAttributeHandle
		AppendAttribute(XmlNodeHandle& parentNodeHandle, const AString strName, const AString strValue = "");

	XmlNodeHandle
		RootNode();


protected:
	XmlDocumentImpl*
		_pXmlDocumentImpl;

private:
	// This buffer will be modified by _pXmlDocumentImpl, so never used it directly!
	char*
		__pInternalBuffer;

};


_COMMON_NS_END
