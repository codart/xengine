#pragma once

#include <map>


_COMMON_NS_BEGIN

typedef std::map<void*, void*> _RefCounterList;
typedef std::map<void*, void*>::iterator _RefCounterListIt;

class _ReferenceCounterChecker
{
public:
	static void _CommonExport AddReferenceCounter(void* pRefCounter, void* pUserClass);
	static void _CommonExport RemoveReferenceCounter(void* pUserClass);


protected:
	static _RefCounterList _refCounterList;

};

//}

_COMMON_NS_END