#include "stdafx.h"
#include "XmlDocument.h"

#include "XmlAttributeHandle.h"
#include "rapidxml_print.hpp"
#include "fstream"

_COMMON_NS_BEGIN

XmlDocument::XmlDocument()
{
	_pXmlDocumentImpl = new xml_document<char>;
	XmlNodeHandle::attach(_pXmlDocumentImpl);
	__pInternalBuffer = NULL;
}

XmlDocument::~XmlDocument()
{
	SAFE_DELETE(_pXmlDocumentImpl);
	SAFE_DELETE(__pInternalBuffer);
}

XmlNodeHandle XmlDocument::AppendNode(XmlNodeHandle& parentNodeHandle, const AString& strNodeName
									, const AString& strNodeValue)
{
	return XmlNodeHandle::appendNode(_pXmlDocumentImpl, parentNodeHandle._pInternalImpl, strNodeName, strNodeValue);
}

XmlNodeHandle XmlDocument::AppendNode(const AString& strNodeName, const AString& strNodeValue)
{
	return XmlNodeHandle::appendNode(_pXmlDocumentImpl, _pInternalImpl, strNodeName, strNodeValue);
}

XmlAttributeHandle XmlDocument::AppendAttribute(XmlNodeHandle& parentNodeHandle, const AString strName
												, const AString strValue)
{
	return XmlNodeHandle::appendAttribute(_pXmlDocumentImpl, _pInternalImpl, strName, strValue);
}

void XmlDocument::SaveToFile(const AString& strFileName)
{
	// Print xml into string
	AString xmlString;
	print(std::back_inserter(xmlString), *_pXmlDocumentImpl, 0);

	ofstream xmlFile(strFileName.c_str(), ios::out|ios::binary);
	if(xmlFile.fail())
	{
		throw Exception("XmlDocument::SaveToFile: Create XML file failed!");
	}
	
	xmlFile << xmlString;
	if(!xmlFile.good())
	{
		throw Exception("XmlDocument::SaveToFile: Error occurred during write xml file!");
	}

	xmlFile.close();	
}

void XmlDocument::LoadFromFile(const AString& strFileName)
{
	ifstream xmlFile(strFileName.c_str(), ios::in | ios::binary);

	if(xmlFile.fail())
	{
		throw Exception("Open XML file failed!");
	}

	// Get file length
	ifstream::pos_type cur_pos = xmlFile.tellg(); 
	xmlFile.seekg( 0L, ios::end ); 
	uint32_t nFileSize = xmlFile.tellg(); 
	xmlFile.seekg( cur_pos, ios::beg ); 

	// Read xml file into buffer
	SAFE_DELETE(__pInternalBuffer);
	__pInternalBuffer = new char[nFileSize + 1];

	xmlFile.read(__pInternalBuffer, nFileSize);
	if(!xmlFile.good())
	{
		throw Exception("Read XML file failed!");
	}

	__pInternalBuffer[nFileSize] = 0;
	_pXmlDocumentImpl->parse<0>(__pInternalBuffer);

	xmlFile.close();
}

void XmlDocument::ParseString(const AString& strXMLString)
{
	SAFE_DELETE(__pInternalBuffer);

	__pInternalBuffer = new char[strXMLString.length() + 1];
	memcpy(__pInternalBuffer, strXMLString.c_str(), strXMLString.length() + 1);

	_pXmlDocumentImpl->parse<0>(__pInternalBuffer);
}

XmlNodeHandle XmlDocument::RootNode()
{
	return XmlNodeHandle::FirstChild();
}


_COMMON_NS_END
