#include "stdafx.h"
#include "_RefCounterChecker.h"

_COMMON_NS_BEGIN


_RefCounterList _ReferenceCounterChecker::_refCounterList;

void _ReferenceCounterChecker::AddReferenceCounter(void* pRefCounter, void* pUserClass)
{
	_RefCounterListIt it = _refCounterList.find(pUserClass);
	if(it != _refCounterList.end())
	{
		XTHROWEX("Same raw pointer be reference countered twice!!!");
	}

	_refCounterList.insert(std::make_pair(pUserClass, pRefCounter));
}

void _ReferenceCounterChecker::RemoveReferenceCounter(void* pUserClass)
{
	_RefCounterListIt it = _refCounterList.find(pUserClass);
	XASSERT(it != _refCounterList.end());
	_refCounterList.erase(it);
}


_COMMON_NS_END