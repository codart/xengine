#pragma once

_COMMON_NS_BEGIN

#define FASTEST_VECTOR_ITERATE(UserClass, UserVector)	\
	UserClass* p##UserVector##Cursor;	\
	UserClass* p##UserVector##Start;	\
	UserClass* p##UserVector##EndAll;	\
						\
	if(UserVector.size() != 0)								\
	{															\
		p##UserVector##Cursor = (UserClass*)&UserVector.at(0);		\
		p##UserVector##Start = p##UserVector##Cursor;				\
		p##UserVector##EndAll = (UserClass*)&UserVector.at(UserVector.size() - 1);		\
	}									\
	else								\
	{									\
		p##UserVector##EndAll = NULL;				\
		p##UserVector##Cursor = (UserClass*)1;	\
		p##UserVector##Start = p##UserVector##Cursor;	\
	}									\
	while(p##UserVector##Cursor <= p##UserVector##EndAll)				\
	{						


#define FASTEST_VECTOR_REITERATE(UserClass, UserVector)	\
	if(UserVector.size() != 0)								\
	{															\
		p##UserVector##Cursor = (UserClass*)&UserVector.at(0);		\
	}									\
	else								\
	{									\
		p##UserVector##Cursor = (UserClass*)1;	\
	}									\
	while(p##UserVector##Cursor <= p##UserVector##EndAll)				\
	{	

#define CURRENT_DATA(UserVector) (*p##UserVector##Cursor)

#define GET_NEXT(UserVector)	(*p##UserVector##Cursor++)
#define NEXT(UserVector)	p##UserVector##Cursor++
#define CONTINUE(UserVector)	{p##UserVector##Cursor++; continue; } 			
#define PEEK_NEXT(UserVector)  ( ((p##UserVector##Cursor+1) <= p##UserVector##EndAll)? ( *(p##UserVector##Cursor+1) ) : NULL )

#define CURRENT_INDEX(UserVector)	(p##UserVector##Cursor - p##UserVector##Start)

#define FASTEST_ITERATE_END()		\
	}

//////////////////////////////////////////////////////////////////////////
template<typename UserClass>
class FastVecIterator
{
public:
	typedef std::vector<UserClass> UserVectorType;
	typedef UserClass* UserClassPtr;

	INLINE FastVecIterator(UserVectorType& userVectorObject)
	{
		int nSize = userVectorObject.size();
		if(nSize == 0)
		{
			pEnd = NULL;
			pCursor = (UserClassPtr)1;
		}
		else
		{
			pCursor = &userVectorObject.at(0);
			pEnd = &userVectorObject.at(nSize - 1);
		}
	}

	INLINE UserClass CurrentData()
	{
		return *pCursor;
	}

	INLINE BOOL HasMore()
	{
		return pCursor <= pEnd;
	}

	INLINE void Next()
	{
		++pCursor;
	}

protected:
	UserClassPtr pCursor;
	UserClassPtr pEnd;
};



/*
	Util class for iterator a vector. 
	Note: after iterator a vector a time, need call reset for the next iteration.
*/
template<typename UserClass>
class VectorIterator
{
public:
	typedef std::vector<UserClass> UserVectorType;
	typedef typename UserVectorType::iterator UsrVecIterator;

	INLINE VectorIterator(UserVectorType& userVectorObject)
		: itBegin(userVectorObject.begin()), itEnd(userVectorObject.end())
	{}

	INLINE UserClass CurrentData()
	{
		return *itBegin;
	}

	INLINE UserClass PeekNextData()
	{
		UsrVecIterator itTemp = itBegin;
		if( ++itTemp != itEnd ) return *itTemp;
		else return NULL;
	}

	INLINE BOOL HasMore()
	{
		return itBegin != itEnd;
	}

	INLINE void Next()
	{
		itBegin++;
	}

	INLINE UsrVecIterator CurrentIt()
	{
		return itBegin;
	}

	INLINE UsrVecIterator End()
	{
		return itEnd;
	}

	INLINE UsrVecIterator Find(UserClass data)
	{
		return find(itBegin, itEnd, data);
	}

	// Call this method before do next iteration.
	INLINE void Reset(UserVectorType& userVectorObject)
	{
		itBegin = userVectorObject.begin();
		itEnd = userVectorObject.end();
	}


protected:
	UsrVecIterator 
		itBegin;
	UsrVecIterator 
		itEnd;

};

//////////////////////////////////////////////////////////////////////////
template<typename UserClass>
class ConstVectorIterator
{
public:
	typedef std::vector<UserClass> UserVectorType;
	typedef typename UserVectorType::const_iterator UsrVecIterator;

	INLINE ConstVectorIterator(const UserVectorType& userVectorObject)
	{
		itBegin = userVectorObject.begin();
		itEnd = userVectorObject.end();
	}

	INLINE UserClass CurrentData()
	{
		return *itBegin;
	}

	INLINE UserClass PeekNextData()
	{
		UsrVecIterator itTemp = itBegin;
		if( ++itTemp != itEnd ) return *itTemp;
		else return NULL;
	}

	INLINE BOOL HasMore()
	{
		return itBegin != itEnd;
	}

	INLINE void Next()
	{
		itBegin++;
	}

	INLINE UsrVecIterator CurrentIt()
	{
		return itBegin;
	}

	INLINE UsrVecIterator End()
	{
		return itEnd;
	}

	INLINE UsrVecIterator Find(UserClass data)
	{
		return find(itBegin, itEnd, data);
	}

	// Call this method before do next iteration.
	INLINE void Reset(UserVectorType& userVectorObject)
	{
		itBegin = userVectorObject.begin();
	}


protected:
	UsrVecIterator 
		itBegin;
	UsrVecIterator 
		itEnd;

};


_COMMON_NS_END