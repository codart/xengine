#pragma once

#ifdef USE_SHARED_PTR_STANDALONE
	#define INLINE inline
	#define XASSERT assert
	#define XDELETE delete
	#define package public
	#define XTHROWEX

	#define _COMMON_NS_BEGIN	namespace common {
	#define _COMMON_NS_END		}
#endif

#include "_ReferenceCounter.h"

_COMMON_NS_BEGIN


/**
	Node:
	1. If your want to destroy your object manually, you can provide a 'virtual void Destroy()' method
	for your class and then use SharedPtr like this: 
			SharedPtr<YourClass, true> pObjectPtr;

	3. The = operator should be used with 'new' operator. If used with some other pointer, make 
	sure it has not been assigned to any SharedPtr already.
	Example 1.
	SharedPtr<TestClass> ptr = new TestClass	// Correct!

	Example 2.
	TestClass* p = new TestClass;
	SharedPtr<TestClass> ptr = p;
	SharedPtr<TestClass> ptr2 = p; // Wrong!

	SharedPtr can detect this error automatically in debug mode if FORCE_CHECK_SHARED_PTR is defined.

	4. If you want to use SharedPtr without 'Common Library', you should do like this:
		1. Copy SharedPtr.h , _ReferenceCounter.h, _RefCounterChecker.h and _RefCounterChecker.cpp to your project
		2. Write code:
			#define USE_SHARED_PTR_STANDALONE
		    #include "SharedPtr.h"
*/
template<typename UserClass, bool UseDestroy = true>
class SharedPtr
{
public:
	INLINE SharedPtr(void)
	{
		_pRefCounter = NULL;
		_pUserClass = NULL;		
	}

	INLINE ~SharedPtr(void)
	{
		if(_pRefCounter)
		{
			Release();
		}
	}

	INLINE explicit SharedPtr(UserClass* pUserClass)
	{
		if(!pUserClass)
		{
			_pRefCounter = NULL;
			_pUserClass = NULL;
			return;
		}

		_pRefCounter = new _RefCounter(pUserClass);
		_pUserClass = pUserClass;

		INCREASE_REFERENCE();
	}

	INLINE SharedPtr(const SharedPtr<UserClass, UseDestroy>& sharedPtr)
	{
		_pUserClass = sharedPtr._pUserClass;
		_pRefCounter = sharedPtr._pRefCounter;
		if(_pRefCounter) INCREASE_REFERENCE();
	}

	INLINE SharedPtr& operator=(UserClass* pUserClass)
	{
		Release();
		if(!pUserClass) return *this;

		_pRefCounter = new _RefCounter(pUserClass);
		_pUserClass = pUserClass;

		INCREASE_REFERENCE();

		return *this;
	}

	INLINE SharedPtr& operator=(const SharedPtr<UserClass, UseDestroy>& sharedPtr)
	{
		Release();

		_pUserClass = sharedPtr._pUserClass;
		_pRefCounter = sharedPtr._pRefCounter;

		if(_pRefCounter) INCREASE_REFERENCE();

		return *this;
	}

	INLINE UserClass* operator->() const
	{
		XASSERT(_pUserClass);
		return _pUserClass;
	}

	virtual void Release()
	{
		if(!_pRefCounter) return;

		DECREASE_REFERENCE();

		_pUserClass = NULL;
		_pRefCounter = NULL;
	}

	INLINE operator bool () const { return _pUserClass != NULL; }

	INLINE bool operator!() const
	{
		return (_pUserClass == NULL);
	}

	INLINE bool operator==(UserClass* pUserClass) const
	{
		return _pUserClass == pUserClass;
	}

	INLINE operator UserClass*() const { return _pUserClass; }

	INLINE int	GetReferenceCount()
	{
		XASSERT(_pRefCounter);
		return _pRefCounter->_refCount;
	}


package:
	INLINE UserClass* 
		getPointer() const { return _pUserClass; }	
	INLINE UserClass* 
		getConstPointer() const { return _pUserClass; }
	INLINE _ReferenceCounter<UserClass, UseDestroy>*
		getReferenceCounter() { return _pRefCounter; }


protected:

	void INCREASE_REFERENCE() 
	{
#ifdef _DEBUG
		_pRefCounter->IncreaseReference(this);
#else
		_pRefCounter->IncreaseReference();
#endif
	}

	void DECREASE_REFERENCE()
	{
#ifdef _DEBUG
		int nRefCount = _pRefCounter->DecreaseReference(this);	
#else
		int nRefCount = _pRefCounter->DecreaseReference();
#endif
		XASSERT(nRefCount >= 0);								
		if(nRefCount == 0) _pUserClass = NULL;
	}

protected:
	UserClass* 
		_pUserClass;

	typedef _ReferenceCounter<UserClass, UseDestroy> _RefCounter;

	_RefCounter*
		_pRefCounter;

	/// Must use function pointer to ensure Release() be called from same module.
	//typedef SharedPtr<UserClass, UseDestroy> _SharedPtrClass;
	//typedef void (_SharedPtrClass::*FuncRelease)();

};


_COMMON_NS_END