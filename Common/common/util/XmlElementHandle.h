#pragma once
#pragma warning(disable : 4996) 

#include "rapidxml.hpp"
using namespace rapidxml;


_COMMON_NS_BEGIN

typedef xml_document<char> XmlDocumentImpl;
typedef xml_node<char> XmlNodeImpl;
typedef xml_attribute<char> XmlAttributeImpl;


template<class ImplClass>
class XmlElementHandle
{
	friend class XmlDocument;

public:
	XmlElementHandle() : _pInternalImpl(NULL) {}
	XmlElementHandle(ImplClass* pNodeImpl) : _pInternalImpl(pNodeImpl) {}


public:
	const char* GetValue() const
	{
		XASSERT(_pInternalImpl);
		return _pInternalImpl->value();
	}

	const char* GetName() const
	{
		XASSERT(_pInternalImpl);
		return _pInternalImpl->name();
	}

	std::string ValueToStr()
	{
		return std::string(GetValue());
	}

	std::string NameToStr()
	{
		return std::string(GetName());
	}

	Real ValueToReal()
	{
		return float(*this);
	}

	int ToHexInt() const 
	{ 
		const char* pszValue = GetValue();
		//SASSERT(Math::IsNumber(pszValue));

		uint32_t value;
		sscanf(pszValue, "%x", &value);
		return value;
	}

	operator bool () const { return _pInternalImpl != NULL; }

	operator int() const 
	{ 
		const char* pszValue = GetValue();

		//SASSERT(Math::IsNumber(pszValue));
		return atoi(pszValue);
	}

	operator uint8_t() const 
	{ 
		const char* pszValue = GetValue();

		//SASSERT(Math::IsNumber(pszValue));
		return (uint8_t)atoi(pszValue);
	}

	operator uint32_t() const 
	{ 
		const char* pszValue = GetValue();

		//SASSERT(Math::IsNumber(pszValue));
		char* pszStopScan;
		return (uint32_t)strtoul(pszValue, &pszStopScan, 10);
	}

	operator unsigned short() const
	{
		const char* pszValue = GetValue();
		//SASSERT(Math::IsNumber(pszValue));
		char* pszStopScan;
		return (unsigned short)strtoul(pszValue, &pszStopScan, 10);
	}

	operator float() const 
	{ 
		const char* pszValue = GetValue();
		//SASSERT(Math::IsNumber(pszValue));

		float fbuffer;
		sscanf(pszValue, "%f", &fbuffer);
		return fbuffer;
	}

	operator std::string() const
	{
		return std::string(GetValue());
	}

	operator const char*() const
	{
		return GetValue();
	}

	bool operator == (const char* pszString)
	{
		return ValueToStr() == pszString;
	}

protected:
	ImplClass*
		_pInternalImpl;


protected:
	void attach(ImplClass* pInternalImpl)
	{
		_pInternalImpl = pInternalImpl;
	}

	void detach()
	{
		_pInternalImpl = NULL;
	}

	XmlDocumentImpl*
		getXMLDocImpl()
	{
		XASSERT(_pInternalImpl);
		return _pInternalImpl->document();;
	}

};

_COMMON_NS_END