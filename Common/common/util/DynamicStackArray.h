#pragma once


_COMMON_NS_BEGIN


// Allocate from stack a memory buffer that holds n T objects.
// This does not handle the case that alloca fails. Handling that is platform specific.
#define NEW_ON_STACK(T,n)                static_cast<T*>(alloca(sizeof(T) * (n)))

// Auxilliary macro to be used with stack_array:
//   dsa::stack_array<T> arr(NEW_STACK_ARRAY(T,n));
#define NEW_STACK_ARRAY(T,n)             NEW_ON_STACK(T,(n)),(n)

// Convenience macro to create a stack_array object arr of n T objects on stack.
#define DYNAMIC_STACK_ARRAY(T, arr, n)   common::DynamicStackArray<T> arr(NEW_STACK_ARRAY(T,n))


/****************************************************************************************************
DynamicStackArray<T> constructs n elements of T type on a given memory buffer at initialization in ctor,
and destructs these elements in its dtor. It does not allocate or deallocate memory.

The given memory buffer should be at least sizeof(T)*n bytes.
T must be default constructible and its dtor should not throw.

Its main purpose is to simulate T arr[n]; on stack using alloca with n unknown at compile time.
This allocates the array really fast compared to using dynamic memory allocation from heap.
*****************************************************************************************************/
template <typename T>
class DynamicStackArray
{
public:
	DynamicStackArray(T* p, size_t n) : _pArray(p), _nSize(n) { _initElements(); }
	~DynamicStackArray() { _uninitElements(_nSize); }

public:
	// use like T ts[n], ts+i, ts[i] etc.
	//operator T* () { return p_; }

	T& operator [] (unsigned int iIndex){ XASSERT(iIndex >=0 && iIndex < _nSize); return _pArray[iIndex]; }

	// very limited container like support support.
	size_t 
		GetLength() { return _nSize; }
	T* 
		FirstElement() { return _pArray; }
	T* 
		LastElement() { return _pArray + _nSize; }

private:
	// Uninitialize the first n elements in reverse order.
	void _uninitElements(size_t n)
	{
		while(n>0)
			_pArray[--n].~T();
	}

	// default construct all elements, or none at exception.
	void _initElements()
	{
		size_t i = 0;
		try
		{
			for(; i<_nSize; ++i)
			{
				// note: boost::has_trivial_default_constructor avoids unnecessary ctor calls explicitly
				T* pi_ = new (_pArray+i) T;
				// if this breaks, initial buffer may not hold n elements for element alignment problems
				assert( pi_==_pArray+i );
			}
		}
		catch(...)
		{	// when T ctor throws
			_uninitElements(i);	// clean up initialized objects.
			throw;	// re-throw the exception.
		}
	}

private:
	T*       _pArray;
	size_t   _nSize;
};

_COMMON_NS_END
