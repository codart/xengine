#pragma once


_COMMON_NS_BEGIN


/// Util macros
#define IS_POLYMORPHIC(type) HasVTable<type>::Value


class _CommonExport Util
{
public:
	/// Usage:
	//		RawData rawData(1, 2);
	//		Util::MurmurHash3_x86_32(&rawData, sizeof(RawData), 1234, &hash);
	static void MurmurHash3_x86_32 ( const void * key, int len, uint32_t seed, void * out );

	static void MurmurHash3_x86_128 ( const void * key, int len, uint32_t seed, void * out );

	static void MurmurHash3_x64_128 ( const void * key, int len, uint32_t seed, void * out );

	/// Make container's capacity same size as its size. 
	template <class T> void ShrinkToFit(T& container)
	{
		if(container.capacity() != container.size())
		{
			T(container).swap(container);
		}
	}
};


template<class T>
class HasVTable
{
public :
	class _Derived_ : public T
	{
		virtual void _force_the_vtable(){}
	};

	enum { Value = (sizeof(T) == sizeof(_Derived_)) };
};

_COMMON_NS_END