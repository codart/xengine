#pragma once

#include "rapidxml.hpp"
#include "XmlElementHandle.h"

using namespace rapidxml;

_COMMON_NS_BEGIN

class XmlAttributeHandle;

/// These enums must has same value as rapidxml::node_type
enum NodeType
{
	node_type_invalid = -1,
	node_type_document,      //!< A document node. Name and value are empty.
	node_type_element,       //!< An element node. Name contains element name. Value contains text of first data node.
	node_type_data,          //!< A data node. Name is empty. Value contains data text.
	node_type_cdata,         //!< A CDATA node. Name is empty. Value contains data text.
	node_type_comment,       //!< A comment node. Name is empty. Value contains comment text.
	node_type_declaration,   //!< A declaration node. Name and value are empty. Declaration parameters (version, encoding and standalone) are in node attributes.
	node_type_doctype,       //!< A DOCTYPE node. Name is empty. Value contains DOCTYPE text.
	node_type_pi             //!< A PI node. Name contains target. Value contains instructions.
};

/*
	A wrapper for rapidxml::xml_node<char> pointer.
*/
class _CommonExport XmlNodeHandle : public XmlElementHandle<XmlNodeImpl>
{
	friend class XmlDocument;
	friend class XmlAttributeHandle;

public:
	XmlNodeHandle() {};

protected:
	XmlNodeHandle(XmlNodeImpl* pNodeImpl) : XmlElementHandle<XmlNodeImpl>(pNodeImpl) {}


public:
	NodeType
		GetNodeType();
	XmlNodeHandle
		operator [] (const char* pszNodeName) const;
	XmlNodeHandle
		FirstChild(const char* pszNodeName = 0, std::size_t nNameLength = 0, bool bCaseSensitive = true) const;
	XmlNodeHandle
		NextSiblingNode(const char* pszNodeName = 0, std::size_t nNameLength = 0, bool bCaseSensitive = true) const;
	XmlNodeHandle
		ParentNode();

	// Note: For better performance, use XmlDocument::AppendNode().
	XmlNodeHandle
		AppendNode(const std::string& strNodeName, const std::string& strNodeValue = "");

	bool 
		RemoveChildNode(XmlNodeHandle& xmlNodeHandle);
	bool 
		RemoveChildNode(const char* pszNodeName, std::size_t nNameLength = 0, bool bCaseSensitive = true);

	XmlAttributeHandle 
		FirstAttribute(const char* pszAttributeName = 0, std::size_t nAttributeLength = 0, bool bCaseSensitive = true) const;
	bool
		RemoveAttribute(XmlAttributeHandle& xmlAttributeHandle);
	bool
		RemoveAttribute(const char* pszAttributeName = 0, std::size_t nAttributeLength = 0, bool bCaseSensitive = true);

	// Note: For better performance, use XmlDocument::AppendAttribute().
	XmlAttributeHandle 
		AppendAttribute(const std::string strName, const std::string strValue);
	/* 
		Checks to see whether the XmlNode object contains simple content. An XmlNode object contains 
		simple content if it represents a text node, an attribute node, or an XmlNode element that has no 
		child elements. XmlNode objects that represent comments and processing instructions do not 
		contain simple content.
	*/
	BOOL 
		HasSimpleContent() const;


protected:
	XmlNodeHandle 
		appendNode(XmlDocumentImpl* pXmlDocImpl
					, XmlNodeImpl* pParentNodeImpl
					, const std::string& strNodeName
					, const std::string& strNodeValue);

	XmlAttributeHandle 
		appendAttribute(XmlDocumentImpl* pXmlDocImpl
					, XmlNodeImpl* pParentNodeImpl
					, const std::string& strName
					, const std::string& strValue);

};


_COMMON_NS_END


