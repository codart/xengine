#pragma once

_COMMON_NS_BEGIN

template <class UserClass>
class RefCountPtr
{
public:
	RefCountPtr()
	{
		_pUserClass = NULL;
	}

	RefCountPtr(UserClass* pUserClass)
	{
		if ((_pUserClass = pUserClass) != NULL)
			_pUserClass->AddRef();
	}

	RefCountPtr(const RefCountPtr<UserClass>& pObject)
	{
		if ((_pUserClass = pObject._pUserClass) != NULL)
			_pUserClass->AddRef();
	}

	~RefCountPtr()
	{
		if (_pUserClass)
			_pUserClass->Release();
	}

	void Release()
	{
		if(!_pUserClass) return;

		_pUserClass->Release();
		_pUserClass = NULL;
	}

	INLINE UserClass* operator->() const
	{
		XASSERT(_pUserClass);
		return _pUserClass;
	}

	INLINE UserClass* operator&()
	{
		XASSERT(_pUserClass);
		return _pUserClass;
	}

	INLINE RefCountPtr& operator=(UserClass* pUserClass)
	{
		if (pUserClass) pUserClass->AddRef();
		if (_pUserClass) _pUserClass->Release();
		_pUserClass = pUserClass;
		return *this;
	}
	
	INLINE RefCountPtr& operator=(RefCountPtr<UserClass>& pUserClass)
	{
		if (pUserClass) pUserClass->AddRef();
		if (_pUserClass) _pUserClass->Release();
		_pUserClass = pUserClass;
		return *this;
	}

	INLINE operator bool () const { return _pUserClass != NULL; }

	INLINE bool operator!() const
	{
		return (_pUserClass == NULL);
	}

	INLINE bool operator==(UserClass* pUserClass) const
	{
		return _pUserClass == pUserClass;
	}

	INLINE bool operator==(void* pUserClass) const
	{
		return (void*)_pUserClass == pUserClass;
	}

	INLINE bool operator==(int pNull) const
	{
		XASSERT(pNull == 0);
		return (void*)_pUserClass == (void*)pNull;
	}
	
	INLINE operator UserClass*() const { return _pUserClass; }

protected:
	//----
	UserClass* _pUserClass;
};


_COMMON_NS_END