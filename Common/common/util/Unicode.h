#pragma once

_COMMON_NS_BEGIN

/// Note: Currently not support unix/linux.  
/// To port to linux, use mbstowcs() wcstombs() instead MultiByteToWideChar() WideCharToMultiByte()

/* 
	Below macros cannot be used in function's parameters more than once. 
	eg. 
	foo(AnsiToWStr(xxxx));	// Correct
	foo(AnsiToWStr(xxx), AnsiToWStr(yyy));	// Wrong
*/
#define AStrToWStr(pszAnsi)	Unicode::AnsiToUtf16(pszAnsi, bufferStringW)
#define WStrToAStr(pszWStr)	Unicode::Utf16ToAnsi(pszWStr, bufferStringA)

#ifdef _UNICODE
	#define AStrToStr(pszAStr) AStrToWStr(pszAStr)
	#define ConstAStrToStr(pszAStr) AStrToWStr((const char*)pszAStr)
	#define WStrToStr(pszWStr) (wchar_t*)pszWStr
	#define ConstWStrToStr(pszWStr) (const wchar_t*)pszWStr

	#define StrToAStr(strString) WStrToAStr(strString)
	#define StrToWStr(strString) strString

#else
	#define AStrToStr(pszAStr) (char*)pszAStr
	#define ConstAStrToStr(pszAStr) (const char*)pszAStr
	#define WStrToStr(pszWStr) WStrToAStr(pszWStr)
	#define ConstWStrToStr(pszWStr) WStrToAStr((const wchar_t*)pszWStr)

	#define StrToAStr(strString) strString
	#define StrToWStr(strString) AStrToWStr(strString)
#endif

#define USING_STRING_CONVERT	\
	BufferStringW bufferStringW;	\
	BufferStringA bufferStringA

// Buffer util
class Unicode;

class _CommonExport BufferStringW
{
public:
	BufferStringW();
	~BufferStringW();

public:
	wchar_t*
		GetBufferString();
	wchar_t*&
		GetBufferAddress();

protected:
	wchar_t*
		pszwBuffer;

};

//---
class _CommonExport BufferStringA
{
public:
	BufferStringA();
	~BufferStringA();

public:
	char*
		GetBufferString();
	char*&
		GetBufferAddress();

protected:
	char*
		pszBuffer;

};

#ifdef _UNICODE
	#define BufferString BufferStringW
#else
	#define BufferString BufferStringA
#endif

// Below two macros can be used in CommonLib only
#define __WStrToStr(strSrc, strDest)	Unicode::Utf16ToAnsi(strSrc, strDest)
#define __WStrToStrPtr(strSrc, strDest)	Unicode::Utf16ToAnsi(strSrc, strDest).c_str()


/************************************************************************/
/* Util class for unicode conversion									*/
/************************************************************************/
class _CommonExport Unicode
{
public:
	////----------
	static WCHAR* 
		AnsiToUtf16(LPCSTR pAnsi, BufferStringW& bufferString);
	INLINE static WCHAR* 
		AnsiToUtf16(const AString& strAString, BufferStringW& bufferString)
	{
		return AnsiToUtf16(strAString.c_str(), bufferString);
	}

	INLINE static const WCHAR* 
		AnsiToUtf16(LPCWSTR pszWStr, BufferStringW& bufferString) { return pszWStr; }

	static char* 
		Utf16ToAnsi(LPCWSTR pszWStr, BufferStringA& bufferString);
	INLINE static char* 
		Utf16ToAnsi(const WString& strWString , BufferStringA& bufferString)
	{
		return Utf16ToAnsi(strWString.c_str(), bufferString);
	}

	INLINE static const char* 
		Utf16ToAnsi(LPCSTR pszStr, BufferStringA& bufferString) { return pszStr; }


	////----------
	INLINE static WCHAR* 
		Utf8ToUtf16(LPCSTR pUtf8, WCHAR** pBuf);
	INLINE static WCHAR* 
		AnsiToUtf16(LPCSTR pAnsi, WCHAR** pBuf);

	INLINE static char* 
		Utf16ToUtf8(WCHAR* pUtf16, char** pBuf);
	INLINE static char* 
		Utf16ToAnsi(const WCHAR* pUtf16, char** pBuf);
	
	static BOOL 
		IsUTF8(const char *str);

	static void
		FreeString(char* pString);
	static void
		FreeString(wchar_t* pString);

	////----------
	INLINE static WCHAR* 
		Utf8ToUtf16(LPCSTR pUtf8, WCHAR* pBuf, int bufSize);
	INLINE static WCHAR* 
		AnsiToUtf16(LPCSTR pAnsi, WCHAR* pBuf, int bufSize);

	INLINE static char* 
		Utf16ToUtf8(LPCWSTR pUtf16, char* pBuf, int bufSize);
	INLINE static char* 
		Utf16ToAnsi(LPCWSTR pUtf16, char* pBuf, int bufSize);

private:
	static inline BOOL  // Instead the CRT isalnum() which is depend on LC_CTYPE.
		isalnum(char val);
};



_COMMON_NS_END