#pragma once

#include "common/util/SharedPtr.h"
#include "common/util/ConcretePtr.h"

_COMMON_NS_BEGIN


/*
OwnerPtr is based on SharedPtr(or SharedPtr's subclass). OwnerPtr will check the reference count during destruction. 
If reference count is not 1, it will throw an exception.

Usage:
	typedef SharedPtr<Vector4> Vector4Ptr;
	typedef OwnerPtr<Vector4Ptr, Vector4> Vector4OwnerPtr;
	Vector4OwnerPtr pOwnerPtr = new Vector4;		// Correct!

	map<String, Vector4OwnerPtr> testMap;
	Vector4* pVV = new Vector4;
	testMap.insert(make_pair(TEXT("test"), pVV));	// Wrong!
	testMap[TEXT("test")] = new Vector4;			// Correct!

*/

template<class _SmartPtrClass>
class OwnerPtr;

template<class UserClass, bool UseDestroy>
class OwnerPtr<SharedPtr<UserClass, UseDestroy>>
{
public:
	typedef SharedPtr<UserClass, UseDestroy> SmartPtrClass;
	#include "_OwnerPtrImpl.inl"
};

///----
template<class UserClass, class AbstractClass>
class OwnerPtr<ConcretePtr<UserClass, AbstractClass>>
{
public:
	typedef ConcretePtr<UserClass, AbstractClass> SmartPtrClass;
	#include "_OwnerPtrImpl.inl"
};


_COMMON_NS_END

