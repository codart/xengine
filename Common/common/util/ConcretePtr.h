#pragma once

#include "common/util/SharedPtr.h"

_COMMON_NS_BEGIN

/* 
	Based on SharedPtr. Allow us to use a Concrete class' ConcretePtr point to Abstract class' SharedPtr.
	For example, RenderTexturePtr is a concrete shared ptr of TexturePtr

	Example:
	class Abstract
	{
	}

	class Concrete : public Abstract
	{
	public:
		void Func();
	}

	SharedPtr<Abstract> pBasePtr = new Concrete;
	ConcretePtr<Concrete, Abstract> pConcretePtr = pBasePtr;
	pConcretePtr->Func();

*/
template<typename ConcreteClass, typename AbstractClass>
class ConcretePtr
{
public:
	typedef SharedPtr<ConcreteClass> ConcreteClassPtr;
	typedef SharedPtr<AbstractClass> AbstractClassPtr;


public:
	ConcretePtr()
	{

	}

	ConcretePtr(const AbstractClassPtr& pSharedPtr)
	{
		XASSERT(SAFE_CAST(pSharedPtr.getPointer(), ConcreteClass*));
		_pAbstractPtr = pSharedPtr;
	}


public:
	INLINE int GetReferenceCount()
	{
		XASSERT(_pAbstractPtr);
		return _pAbstractPtr.GetReferenceCount();
	}

	INLINE AbstractClassPtr& GetAbstractPtr() { return _pAbstractPtr; }

	INLINE operator AbstractClassPtr&() { return _pAbstractPtr; }

	INLINE ConcretePtr& operator=(const AbstractClassPtr& pAbstractPtr)
	{
		_pAbstractPtr = pAbstractPtr;
		return *this;
	}

	INLINE ConcretePtr& operator=(ConcreteClass* pConcreteClass)
	{
		_pAbstractPtr = pConcreteClass;
		return *this;
	}

	INLINE ConcreteClass* operator->() const
	{
		return SAFE_CAST(_pAbstractPtr.getPointer(), ConcreteClass*);
	}

	INLINE ConcreteClass* operator&()
	{
		XASSERT(_pAbstractPtr);
		return SAFE_CAST(_pAbstractPtr.getPointer(), ConcreteClass*);
	}

	INLINE operator bool () const { return _pAbstractPtr != NULL; }

	INLINE void Release()
	{
		_pAbstractPtr.Release();
	}

package:
	INLINE ConcreteClass* 
		getConstPointer() const { return SAFE_CAST(_pAbstractPtr.getPointer(), ConcreteClass*); }
	INLINE ConcreteClass* 
		getPointer() { return SAFE_CAST(_pAbstractPtr.getPointer(), ConcreteClass*); }


protected:
	AbstractClassPtr
		_pAbstractPtr;

};

_COMMON_NS_END