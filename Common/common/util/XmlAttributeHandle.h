#pragma once

#include "rapidxml.hpp"
#include "XmlElementHandle.h"
#include "XmlNodeHandle.h"

using namespace rapidxml;

_COMMON_NS_BEGIN

class _CommonExport XmlAttributeHandle : public XmlElementHandle<XmlAttributeImpl>
{
	friend class XmlNodeHandle;
	friend class XmlDocument;

public:
	XmlAttributeHandle() {}

protected:
	XmlAttributeHandle(XmlAttributeImpl* pXmlAttributeImpl) : XmlElementHandle<XmlAttributeImpl>(pXmlAttributeImpl){}


public:
	XmlAttributeHandle 
		NextAttribute(const char* pszAttributeName = 0, std::size_t nAttributeLength = 0, bool bCaseSensitive = true);
	XmlNodeHandle 
		ParentNode();

};


_COMMON_NS_END
