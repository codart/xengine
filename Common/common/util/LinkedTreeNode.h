#pragma once

#include "common/core/ReferenceCounted.h"

_COMMON_NS_BEGIN

/// A node in the tree, combining links to other nodes as well as the actual data.
template<class UserClass>
class LinkedTreeNode : public ReferenceCounted
{ 
public:
	//.. Need optimize
	LinkedTreeNode()
	{
		pParent = pFirstChild = pLastChild		\
			= pPrevSibling = pNextSibling = NULL;
	}

public:
	template<class UserClass>
	void AddChildNode(LinkedTreeNode<UserClass>* pNode)
	{
		pNode->AddRef();

		if (pNode->pParent)
		{
			((LinkedTreeNode<UserClass>*)(pNode->pParent))->RemoveChildNode(pNode);
		}

		pNode->pNextSibling = pNode->pPrevSibling = NULL;
		pNode->pParent = this;

		if(pLastChild)
		{
			// Insert after pLastChild
			pNode->pNextSibling = NULL;
			pLastChild->pNextSibling = pNode;
			pNode->pPrevSibling = pLastChild;
			pLastChild = pNode;
		}
		else
		{
			// If pLastChild is empty, the pFirstChild is empty too
			pFirstChild = pLastChild = pNode;
		}
	}

	template<class UserClass>
	BOOL RemoveChildNode(LinkedTreeNode<UserClass>* pNode)
	{
		BOOL bFind = FALSE;
		LinkedTreeNode<UserClass>* pCurNode = pFirstChild;	
		while (pCurNode)					
		{	
			if(pCurNode == pNode)
			{
				bFind = TRUE;

				// Update linked list
				if(pNode->pPrevSibling)
				{
					pNode->pPrevSibling->pNextSibling = pNode->pNextSibling;
				}
				if(pNode->pNextSibling)
				{
					pNode->pNextSibling->pPrevSibling = pNode->pPrevSibling;
				}

				// Update first/last child pointer
				if(pNode == pFirstChild) 
				{
					pFirstChild = pNode->pNextSibling;
				}

				if(pNode == pLastChild) 
				{
					pLastChild = pNode->pPrevSibling;
				}

				pNode->Release();
				pNode->pParent = NULL;
				break;
			}
			pCurNode = pCurNode->pNextSibling;
		}	

		return bFind;
	}

public:
	LinkedTreeNode<UserClass> *pParent;
	LinkedTreeNode<UserClass> *pFirstChild, *pLastChild;
	LinkedTreeNode<UserClass> *pPrevSibling, *pNextSibling;

	// Need it?
	UserClass data;
}; // __attribute__((packed));

// Utility classes/macros
typedef LinkedTreeNode<DWORD>* NodeTypePtr;


#define TRAVERSE_CHILD_NODE_HEAD			\
	NodeTypePtr pCurNode = pFirstChild;	\
	while (pCurNode)					\
{														

#define TRAVERSE_CHILD_NODE_TAIL			\
	pCurNode = pCurNode->pNextSibling;	\
}	

_COMMON_NS_END