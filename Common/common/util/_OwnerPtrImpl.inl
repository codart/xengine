/// implementation of OwnerPtr

OwnerPtr()
{

}

OwnerPtr(UserClass* pPointer)
{
	_pSmartPtr = pPointer;
}

OwnerPtr(const SmartPtrClass& pSmartPtr)
{
	_pSmartPtr = pSmartPtr;
}

operator UserClass*() const 
{
	return _pSmartPtr; 
}

~OwnerPtr()
{
	if(_pSmartPtr && _pSmartPtr.GetReferenceCount() != 1)
	{
		throw Exception("Instance still be referenced by other object!");
	}
}


public:
	INLINE int GetReferenceCount()
	{
		XASSERT(_pSmartPtr);
		return _pSmartPtr.GetReferenceCount();
	}

	INLINE UserClass* getPointer() const { return _pSmartPtr.getPointer(); }	

	INLINE SmartPtrClass& GetSmartPtr() { return _pSmartPtr; }

	INLINE operator SmartPtrClass&() { return _pSmartPtr; }

	INLINE OwnerPtr& operator=(const SmartPtrClass& pSmartPtr)
	{
		_pSmartPtr = pSmartPtr;
		return *this;
	}

	INLINE OwnerPtr& operator=(UserClass* pConcreteClass)
	{
		if(pConcreteClass == NULL && _pSmartPtr)
		{
			XASSERT(_pSmartPtr.GetReferenceCount() == 1);
		}

		_pSmartPtr = pConcreteClass;
		return *this;
	}

	INLINE UserClass* operator->() const
	{
		return SAFE_CAST(_pSmartPtr.getConstPointer(), UserClass*);
	}

	INLINE UserClass* operator&()
	{
		XASSERT(_pSmartPtr);
		return SAFE_CAST(_pSmartPtr.getPointer(), UserClass*);
	}

	INLINE operator bool () const { return _pSmartPtr != NULL; }

	INLINE void Release()
	{
		_pSmartPtr.Release();
	}


protected:
	SmartPtrClass
		_pSmartPtr;