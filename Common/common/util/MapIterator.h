#pragma once

_COMMON_NS_BEGIN

template<typename key, typename UserClass>
class MapIterator
{
public:
	typedef std::map<key, UserClass> UserMapType;
	typedef typename UserMapType::iterator UsrMapIterator;

public:
	INLINE MapIterator(UserMapType& userVectorObject)
		: itCursor(userVectorObject.begin()), itEnd(userVectorObject.end())
	{}

	INLINE UserClass& CurrentData()
	{
		return itCursor->second;
	}

	INLINE UserClass& PeekNextData()
	{
		UsrMapIterator itTemp = itCursor;
		if( ++itTemp != itEnd ) return *itTemp;
		else return NULL;
	}

	INLINE BOOL HasMore()
	{
		return itCursor != itEnd;
	}

	INLINE void Next()
	{
		itCursor++;
	}

	INLINE UsrMapIterator CurrentIt()
	{
		return itCursor;
	}

	INLINE UsrMapIterator End()
	{
		return itEnd;
	}

	// Call this method before do next iteration.
	INLINE void Reset(UserMapType& userVectorObject)
	{
		itCursor = userVectorObject.begin();
		itEnd = userVectorObject.end();
	}


protected:
	UsrMapIterator 
		itCursor;
	UsrMapIterator 
		itEnd;
};

//////////////////////////////////////////////////////////////////////////
template<typename key, typename UserClass>
class ConstMapIterator
{
public:
	typedef std::map<key, UserClass> UserMapType;
	typedef typename UserMapType::const_iterator UsrMapIterator;

public:
	INLINE ConstMapIterator(const UserMapType& userVectorObject)
		: itCursor(userVectorObject.begin()), itEnd(userVectorObject.end())
	{}

	INLINE UserClass CurrentData()
	{
		return itCursor->second;
	}

	INLINE UserClass PeekNextData()
	{
		UsrMapIterator itTemp = itCursor;
		if( ++itTemp != itEnd ) return *itTemp;
		else return NULL;
	}

	INLINE BOOL HasMore()
	{
		return itCursor != itEnd;
	}

	INLINE void Next()
	{
		itCursor++;
	}

	INLINE UsrMapIterator CurrentIt()
	{
		return itCursor;
	}

	INLINE UsrMapIterator End()
	{
		return itEnd;
	}

	// Call this method before do next iteration.
	INLINE void Reset(UserMapType& userVectorObject)
	{
		itCursor = userVectorObject.begin();
		itEnd = userVectorObject.end();
	}


protected:
	UsrMapIterator 
		itCursor;
	UsrMapIterator 
		itEnd;
};


_COMMON_NS_END