#pragma once

_COMMON_NS_BEGIN

class TreeNode;

DECLARE_LIST_TYPE_PTR(vector, TreeNode);

class _CommonExport TreeNode
{
public:
	TreeNode();
	virtual ~TreeNode();


public:
	INLINE TreeNode* 
		GetParent() { return _pParent; }
	void
		AddChild(TreeNode* pChild);

	INLINE const TreeNodeList&
		GetChildNodes() const {return _children; }

	INLINE TreeNode*
		GetChildAt(UINT iIndex) const
	{
		XASSERT(iIndex >= 0 && iIndex < _children.size());
		return _children[iIndex];
	}

	INLINE size_t
		GetChildCount() { return _children.size(); }

	void 
		setParent(TreeNode* pParent);


protected:
	TreeNode* 
		_pParent;
	TreeNodeList
		_children;

};



_COMMON_NS_END