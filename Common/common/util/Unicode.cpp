// Utf8.cpp: implementation of the CUnicode class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Unicode.h"
#include "stdio.h"

_COMMON_NS_BEGIN

BufferStringW::BufferStringW()
{
	pszwBuffer = NULL;
}

BufferStringW::~BufferStringW()
{
	if(pszwBuffer)
	{
		Unicode::FreeString(pszwBuffer);
	}
}

wchar_t*& BufferStringW::GetBufferAddress()
{
	if(pszwBuffer)
	{
		Unicode::FreeString(pszwBuffer);
		pszwBuffer = NULL;
	}
	
	return pszwBuffer;
}

wchar_t* BufferStringW::GetBufferString()
{
	return pszwBuffer;
}

//------

BufferStringA::BufferStringA()
{
	pszBuffer = NULL;
}

BufferStringA::~BufferStringA()
{
	if(pszBuffer)
	{
		Unicode::FreeString(pszBuffer);
	}
}

char*& BufferStringA::GetBufferAddress()
{
	if(pszBuffer)
	{
		Unicode::FreeString(pszBuffer);
		pszBuffer = NULL;
	}

	return pszBuffer;
}

char* BufferStringA::GetBufferString()
{
	return pszBuffer;
}

//////////////////////////////////////////////////////////////////////////

void Unicode::FreeString(char* pString)
{
	XASSERT(pString);
	XDELETE pString;
}

void Unicode::FreeString(wchar_t* pString)
{
	XASSERT(pString);
	XDELETE pString;
}

WCHAR* Unicode::AnsiToUtf16(LPCSTR pAnsi, BufferStringW& bufferString)
{
	return AnsiToUtf16(pAnsi, &bufferString.GetBufferAddress());
}

char* Unicode::Utf16ToAnsi(LPCWSTR pszWStr, BufferStringA& bufferString)
{
	return Utf16ToAnsi(pszWStr, &bufferString.GetBufferAddress());
}

WCHAR* Unicode::Utf8ToUtf16(LPCSTR pUtf8, WCHAR* pBuf, int bufSize)
{
	if(!pUtf8) return NULL;

	int srcLen = strlen(pUtf8) + 1;

	if(0 == MultiByteToWideChar(CP_UTF8, 0 , LPCSTR(pUtf8), 
						srcLen, pBuf, bufSize))
	{
		//Error!
		return NULL;
	}

	return pBuf;
}

char* Unicode::Utf16ToUtf8(LPCWSTR pUtf16, char* pBuf, int bufSize)
{
	if(!pUtf16) return NULL;
	
	int srcLen = wcslen(pUtf16) + 1;
	if (0 == WideCharToMultiByte(CP_UTF8, 0 , pUtf16, 
				srcLen, pBuf, bufSize, NULL, NULL)) 
	{
		//Error!
		return NULL;

	}
	return pBuf;
}

WCHAR* Unicode::AnsiToUtf16(LPCSTR pAnsi, WCHAR* pBuf, int bufSize)
{
	if(!pAnsi) return NULL;

	int srcLen = strlen(pAnsi) + 1;
	if(0 == MultiByteToWideChar(CP_ACP, 0 , LPCSTR(pAnsi), srcLen, pBuf, bufSize))
	{
		//Error!
		return NULL;
	}
	return pBuf;
}

char* Unicode::Utf16ToAnsi(LPCWSTR pUtf16, char* pBuf, int bufSize)
{
	if(!pUtf16) return NULL;

	int srcLen = wcslen(pUtf16) + 1;
	if(0 == WideCharToMultiByte(CP_ACP, 0 , pUtf16, srcLen, pBuf, bufSize, NULL, NULL))
	{
		//Error!
		return NULL;
	}
	return pBuf;
}

///--------------
WCHAR* Unicode::Utf8ToUtf16(LPCSTR pUtf8, WCHAR** pBuf)
{
	if(!pUtf8) return NULL;

	int srcLen = strlen(pUtf8) + 1;

	//Get required wide char count.
	int cchWChar = MultiByteToWideChar(CP_UTF8, 0 , LPCSTR(pUtf8), 
						srcLen, *pBuf, 0);

	if(0 == cchWChar)
	{
		//Error!
		return NULL;
	}
	
	*pBuf = XNEW WCHAR[cchWChar];
	memset(*pBuf, 0, cchWChar * 2);
	
	return Utf8ToUtf16(pUtf8, *pBuf, cchWChar);
}

WCHAR* Unicode::AnsiToUtf16(LPCSTR pAnsi, WCHAR** pBuf)
{
	if(!pAnsi) return NULL;

	int srcLen = strlen(pAnsi) + 1;

	//Get required wide char count.
	int cchWChar = MultiByteToWideChar(CP_ACP, 0 , LPCSTR(pAnsi), 
						srcLen, *pBuf, 0);

	if(0 == cchWChar)
	{
		//Error!
		return NULL;
	}
	
	*pBuf = XNEW WCHAR[cchWChar];
	memset(*pBuf, 0, cchWChar * 2);
	
	return AnsiToUtf16(pAnsi, *pBuf, cchWChar);
}

char* Unicode::Utf16ToUtf8(WCHAR* pUtf16, char** pBuf)
{
	if(!pUtf16) return NULL;

	int srcLen = wcslen(pUtf16) + 1;

	//Get required wide char count.
	int cchWChar = WideCharToMultiByte(CP_UTF8, 0 , pUtf16, 
						srcLen, *pBuf, 0, NULL, NULL);

	if(0 == cchWChar)
	{
		//Error!
		return NULL;
	}
	
	*pBuf = XNEW char[cchWChar];
	memset(*pBuf, 0, cchWChar);
	
	return Utf16ToUtf8(pUtf16, *pBuf, cchWChar);
}

char* Unicode::Utf16ToAnsi(const WCHAR* pUtf16, char** pBuf)
{
	if(!pUtf16) return NULL;

	int srcLen = wcslen(pUtf16) + 1;

	//Get required wide char count.
	int cchWChar = WideCharToMultiByte(CP_ACP, 0 , pUtf16, 
						srcLen, *pBuf, 0, NULL, NULL);

	if(0 == cchWChar)
	{
		//Error!
		return NULL;
	}
	
	int wBufSize = cchWChar;
	*pBuf = XNEW char[wBufSize];
	memset(*pBuf, 0, wBufSize);

	return Utf16ToAnsi(pUtf16, *pBuf, wBufSize);
}

#define BETWEEN(c, x, y)	(c >= x && c <= y)
BOOL Unicode::isalnum(char val)
{
	if(BETWEEN(val, 'a', 'z') || BETWEEN(val, 'A', 'Z') 
			|| BETWEEN(val, '0', '9')) return TRUE;
	return FALSE;
}

//0000 0000-0000 007F - 0xxxxxxx  (ascii converts to 1 octet!)
//0000 0080-0000 07FF - 110xxxxx 10xxxxxx    ( 2 octet format)
//0000 0800-0000 FFFF - 1110xxxx 10xxxxxx 10xxxxxx (3 octet format) 
BOOL Unicode::IsUTF8(const char *str)
{
    int   i;
    BYTE cOctets;  // octets to go in this UTF-8 encoded character
    BYTE chr;
    BOOL  bAllAscii= TRUE;
    long iLen = strlen(str);
	
    cOctets= 0;
    for( i=0; i <iLen; i++ ) {
		
		chr = (BYTE)str[i];
		
		if( (chr & 0x80) != 0 ) bAllAscii= FALSE;
		
		if( cOctets == 0 ) {
			if( chr>= 0x80 )  {
				do  {
					chr <<= 1;
					cOctets++;
				}
				while( (chr & 0x80) != 0 );
				
				cOctets--;                        
				if( cOctets == 0 ) return FALSE;  
			}
		}
		else  {
			if( (chr & 0xC0) != 0x80 ) 
				return FALSE;
			
			cOctets--;                       
		}
    }
	
    if( cOctets> 0 ) 
		return FALSE;
	
    if( bAllAscii ) 
		return FALSE;
	
    return TRUE;	
} 


_COMMON_NS_END
