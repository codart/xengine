#include "stdafx.h"
#include "XmlAttributeHandle.h"
#include "XmlNodeHandle.h"


_COMMON_NS_BEGIN

XmlAttributeHandle XmlAttributeHandle::NextAttribute(const char* pszAttributeName
													, std::size_t nAttributeLength
													, bool bCaseSensitive)
{
	if(_pInternalImpl)
	{
		return _pInternalImpl->next_attribute(pszAttributeName, nAttributeLength, bCaseSensitive);
	}
	else return XmlAttributeHandle(NULL);
}

XmlNodeHandle XmlAttributeHandle::ParentNode()
{
	if(_pInternalImpl) return XmlNodeHandle(_pInternalImpl->parent());
	else return XmlNodeHandle(NULL);
}


_COMMON_NS_END
