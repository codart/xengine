#include "stdafx.h"
#include "XmlNodeHandle.h"

#include "XmlAttributeHandle.h"
#include "XmlDocument.h"

_COMMON_NS_BEGIN

NodeType XmlNodeHandle::GetNodeType()
{
	if(_pInternalImpl)
	{
		return (NodeType)_pInternalImpl->type();
	}
	else return node_type_invalid;
}

XmlNodeHandle XmlNodeHandle::operator [] (const char* pszNodeName) const
{
	if(_pInternalImpl) return XmlNodeHandle(_pInternalImpl->first_node(pszNodeName, strlen(pszNodeName), true));
	else return XmlNodeHandle(NULL);
}

XmlNodeHandle XmlNodeHandle::FirstChild(const char* pszNodeName, std::size_t nNameLength, bool bCaseSensitive) const
{
	if(_pInternalImpl) return XmlNodeHandle(_pInternalImpl->first_node(pszNodeName, nNameLength, bCaseSensitive));
	else return XmlNodeHandle(NULL);
}

XmlNodeHandle XmlNodeHandle::NextSiblingNode(const char* pszNodeName, std::size_t nNameLength, bool bCaseSensitive) const
{
	if(_pInternalImpl) return XmlNodeHandle(_pInternalImpl->next_sibling(pszNodeName, nNameLength, bCaseSensitive));
	else return XmlNodeHandle(NULL);
}

XmlNodeHandle XmlNodeHandle::ParentNode()
{
	if(_pInternalImpl) return XmlNodeHandle(_pInternalImpl->parent());
	else return XmlNodeHandle(NULL);
}

bool XmlNodeHandle::RemoveChildNode(XmlNodeHandle& xmlNodeHandle)
{
	if(!_pInternalImpl) return false;

	_pInternalImpl->remove_node(xmlNodeHandle._pInternalImpl);
	xmlNodeHandle._pInternalImpl = NULL;
	return true;
}

bool XmlNodeHandle::RemoveChildNode(const char* pszNodeName, std::size_t nNameLength, bool bCaseSensitive)
{
	XmlNodeHandle childNode = FirstChild(pszNodeName, nNameLength, bCaseSensitive);
	if(!childNode) return false;

	return RemoveChildNode(childNode);
}

bool XmlNodeHandle::RemoveAttribute(XmlAttributeHandle& xmlAttributeHandle)
{
	if(!_pInternalImpl) return false;

	_pInternalImpl->remove_attribute(xmlAttributeHandle._pInternalImpl);
	xmlAttributeHandle._pInternalImpl = NULL;
	return true;	
}

bool XmlNodeHandle::RemoveAttribute(const char* pszAttributeName, std::size_t nAttributeLength, bool bCaseSensitive)
{
	XmlAttributeHandle attribute = FirstAttribute(pszAttributeName, nAttributeLength, bCaseSensitive);
	if(!attribute) return false;

	return RemoveAttribute(attribute);
}

XmlAttributeHandle XmlNodeHandle::FirstAttribute(const char* pszAttributeName, std::size_t nAttributeLength, bool bCaseSensitive) const
{
	if(_pInternalImpl) return XmlAttributeHandle(_pInternalImpl->first_attribute(pszAttributeName, nAttributeLength, bCaseSensitive));
	else return XmlAttributeHandle(NULL);
}

XmlNodeHandle XmlNodeHandle::AppendNode(const std::string& strNodeName, const std::string& strNodeValue)
{
	if(!_pInternalImpl) return XmlNodeHandle(NULL);

	XmlDocumentImpl* pXmlDocImpl = getXMLDocImpl();
	if(!pXmlDocImpl) return XmlNodeHandle(NULL);

	return appendNode(pXmlDocImpl, _pInternalImpl, strNodeName, strNodeValue);
}

XmlAttributeHandle XmlNodeHandle::AppendAttribute(const std::string strName, const std::string strValue)
{
	if(!_pInternalImpl) return XmlAttributeHandle(NULL);

	XmlDocumentImpl* pXmlDocImpl = getXMLDocImpl();
	if(!pXmlDocImpl) return XmlAttributeHandle(NULL);

	return appendAttribute(pXmlDocImpl, _pInternalImpl, strName, strValue);
}

XmlNodeHandle XmlNodeHandle::appendNode(XmlDocumentImpl* pXmlDocImpl
										, XmlNodeImpl* pParentNodeImpl
										, const std::string& strNodeName
										, const std::string& strNodeValue)
{
	char* pszNodeName = pXmlDocImpl->allocate_string(strNodeName.c_str());
	XmlNodeImpl* pNode;
	if(strNodeValue.length() > 0)
	{
		char* pszNodeValue = pXmlDocImpl->allocate_string(strNodeValue.c_str());
		pNode = pXmlDocImpl->allocate_node(node_element, pszNodeName, pszNodeValue);
	}
	else
	{
		pNode = pXmlDocImpl->allocate_node(node_element, pszNodeName);
	}

	pParentNodeImpl->append_node(pNode);
	return XmlNodeHandle(pNode);
}

XmlAttributeHandle XmlNodeHandle::appendAttribute(XmlDocumentImpl* pXmlDocImpl
												, XmlNodeImpl* pParentNodeImpl
												, const std::string& strName
												, const std::string& strValue)
{
	char* pszName = pXmlDocImpl->allocate_string(strName.c_str());
	xml_attribute<>* pAttribute;
	if(strValue.length() > 0)
	{
		char* pszValue = pXmlDocImpl->allocate_string(strValue.c_str());
		pAttribute = pXmlDocImpl->allocate_attribute(pszName, pszValue);
	}
	else
	{
		pAttribute = pXmlDocImpl->allocate_attribute(pszName);
	}

	pParentNodeImpl->append_attribute(pAttribute);
	return XmlAttributeHandle(pAttribute);
}

BOOL XmlNodeHandle::HasSimpleContent() const
{
	if(!this->FirstChild())
	{
		return true;
	}
	else if(this->FirstChild().GetNodeType() == node_type_data)
	{
		return true;
	}
	else return true;
}

_COMMON_NS_END
