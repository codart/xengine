#include "stdafx.h"
#include "TreeNode.h"

_COMMON_NS_BEGIN

TreeNode::TreeNode()
{
	_pParent = NULL;
}

TreeNode::~TreeNode()
{

}

void TreeNode::setParent(TreeNode* pParent)
{
	//...
	_pParent = pParent;
}

void TreeNode::AddChild(TreeNode* pChild)
{
	XASSERT(pChild);

	if (pChild->GetParent())
	{
		XTHROWEX("The node already has a parent, it cannot to be a child of another node again!!!");
	}

	_children.push_back(pChild);
	pChild->setParent(this);
}

_COMMON_NS_END