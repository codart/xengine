#pragma once

#include <set>

#ifdef _DEBUG
	#include "_RefCounterChecker.h"
#endif

_COMMON_NS_BEGIN

template<typename UserClass, bool UseDestroy>
class SharedPtr;

template<typename UserClass, bool UseDestroy>
class _ReferenceCounter;

/// Create/Destroy in the CPP file to ensure _ReferenceCounter be create/destroy in same CRT.
///_CommonExport _ReferenceCounter* _CreateReferenceCounter(void* pPtr);

//... Not support multi-thread now
template<typename UserClass, bool UseDestroy>
class _ReferenceCounter
{
public:
	//typedef SharedPtr<UserClass> UserClassSharedPtr;
	typedef void UserClassSharedPtr;
	//typedef void UserClass;


	_ReferenceCounter(UserClass* pPtr)
	{
#ifdef _DEBUG
		_ReferenceCounterChecker::AddReferenceCounter(this, pPtr);
#endif
		_pUserClass = pPtr;
		_refCount = 0;
	}

	~_ReferenceCounter()
	{
#ifdef _DEBUG
		_ReferenceCounterChecker::RemoveReferenceCounter(_pUserClass);
#endif
	}


public:
#ifdef _DEBUG
	/// Mark 'virtual' to ensure the memory allocated by __referencedPtrs is in same heap (CRT).
	virtual void 
		IncreaseReference(UserClassSharedPtr* pSharedPtr)
	{
		XASSERT(pSharedPtr);
		__referencedPtrs.insert(pSharedPtr);
		++_refCount;
	}

	/// Mark 'virtual' to ensure the memory allocated by __referencedPtrs is in same heap (CRT).
	virtual int
		DecreaseReference(UserClassSharedPtr* pSharedPtr)

	{
		UserClassPtrSetIt it = __referencedPtrs.find(pSharedPtr);
		XASSERT(it != __referencedPtrs.end());
		__referencedPtrs.erase(it);

		--_refCount;
		if(_refCount == 0)
		{
			ReleaseFunc<UseDestroy>::Release(_pUserClass);
			XDELETE this;
			return 0;
		}
		else return _refCount;
	}
#endif

	void 
		IncreaseReference()
	{
		++_refCount;
	}

	virtual int
		DecreaseReference()
	{
		--_refCount;
		if(_refCount == 0)
		{
			ReleaseFunc<UseDestroy>::Release(_pUserClass);
			XDELETE this;
			return 0;
		}
		else return _refCount;
	}

public:
	int 
		_refCount;

	UserClass*
		_pUserClass;

#ifdef _DEBUG
	typedef std::set<void*> UserClassPtrSet;
	typedef UserClassPtrSet::iterator UserClassPtrSetIt;

	UserClassPtrSet
		__referencedPtrs;
#endif

	template<bool UseDestroy>
	struct ReleaseFunc;

	template<>
	struct ReleaseFunc<true> {  static void Release(UserClass* p) { p->Destroy(); }  };

	template<>
	struct ReleaseFunc<false>{  static void Release(UserClass* p) { XDELETE p; }  };

};


_COMMON_NS_END