
#pragma once

_COMMON_NS_BEGIN

/// To provide boolean tests for a class but restricting it from taking participation in unwanted expressions.
/// See http://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Safe_bool for details.

/* Example

	class Testable_with_virtual : public SafeBool<> 
	{
	public:
		virtual ~Testable_with_virtual () {}

		protected:
			virtual bool doBooleanTest() const 
			{
				// Perform Boolean logic here
				return true;
			}
	};

	class Testable_without_virtual : public SafeBool <Testable_without_virtual>
	{
	public:
		bool doBooleanTest() const 
		{
			// Perform Boolean logic here
			return false;
		}
	};

	...

	Testable_with_virtual t1, t2;
	Testable_without_virtual p1, p2;

	if(t1) ... /// OK
	if(p1) ... /// OK

	if(t1 < t2) ... /// Compile Error!
	if(p1 < p2) ... /// Compile Error!
	
	if(t1 == t2) .../// Compile Error!
	
*/

class SafeBoolBase
{
public:
	typedef void (SafeBoolBase::*AgentBoolType)() const;
	void _dummyFunction() const {}

protected:
	SafeBoolBase() {}
	SafeBoolBase(const SafeBoolBase&) {}
	SafeBoolBase& operator=(const SafeBoolBase&) {return *this;}
	~SafeBoolBase() {}
};

// For testability without virtual function.
template <typename UserClass=void> 
class SafeBool : private SafeBoolBase 
{
	// private or protected inheritance is very important here as it triggers the
	// access control violation in main.
public:
	operator AgentBoolType() const 
	{
		return (static_cast<const UserClass*>(this))->doBooleanTest()
			? &SafeBoolBase::_dummyFunction : 0;
	}
protected:
	~SafeBool() {}
};


// For testability with a virtual function.
template<> 
class SafeBool<void> : private SafeBoolBase 
{
	// private or protected inheritance is very important here as it triggers the
	// access control violation in main.
public:
	operator AgentBoolType() const 
	{
		return doBooleanTest() ? &SafeBoolBase::_dummyFunction : 0;
	}
protected:
	virtual bool doBooleanTest() const=0;
	virtual ~SafeBool() {}
};

template <typename UserClass> 
bool operator==(const SafeBool<UserClass>& lhs, bool b) 
{
	if (b)
	{
		if (lhs) return true;
		else return false;
	}
	else
	{
		if (lhs) return false;
		else return true;
	}
}

template <typename UserClass> 
bool operator==(bool b, const SafeBool<UserClass>& rhs) 
{
	if (b)
	{
		if (rhs) return true;
		else return false;
	}
	else
	{
		if (rhs) return false;
		else return true;
	}
}

template <typename UserClass1, typename UserClass2> 
bool operator==(const SafeBool<UserClass1>& lhs,const SafeBool<UserClass2>& rhs) 
{
	lhs._dummyFunction();
	return false;
}

template <typename UserClass1,typename UserClass2> 
bool operator!=(const SafeBool<UserClass1>& lhs,const SafeBool<UserClass2>& rhs) 
{
	lhs._dummyFunction();
	return false;
}


_COMMON_NS_END