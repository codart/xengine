// dllmain.cpp : Implementation of DllMain.
//#include <stdlib.h>
//#include <crtdbg.h>

#include "stdafx.h"
#include "common.h"
#include "common/CommonModule.h"
#include "common/time/TimerMgr.h"

HINSTANCE __hInstance = NULL;
common::Core* __pCore;
common::CommonModule* __pCommonModule = NULL;

// DLL Entry Point
///... should use __attribute__((constructor)) and __attribute__((destructor)) in linux
/// see http://tdistler.com/2007/10/05/implementing-dllmain-in-a-linux-shared-library
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	//_CrtSetBreakAlloc(144);

	HRESULT hRes = 0;

	__hInstance = hInstance;

	static HINSTANCE hInstRich = NULL;

	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		CreateCore();
		GetCore()->RegisterModule(CreateCommonModule, DestroyCommonModule, TRUE);
		hInstRich = LoadLibrary(TEXT("RICHED20.DLL"));
		//__pCommonModule = XNEW common::CommonModule();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		//...SAFE_DELETE(__pCommonModule);
		XASSERT(hInstRich);
		FreeLibrary(hInstRich);
		hInstRich = NULL;
		//_CrtDumpMemoryLeaks();
		DestroyCore();
		break;
	}
	return TRUE;
}

extern "C"
{
	using namespace common;

	_CommonExport IModule* CreateCommonModule()
	{
		XASSERT(!__pCommonModule);
		return __pCommonModule = XNEW CommonModule;
	}

	_CommonExport CommonModule* GetCommonModule()
	{
		return __pCommonModule;
	}

	_CommonExport void DestroyCommonModule()
	{
		SAFE_DELETE(__pCommonModule);
	}

	//----
	_CommonExport void CreateCore()
	{
		if(__pCore) return;

		__pCore = XNEW common::Core;
		__pCore->setModuleHandle((DWORD_PTR)__hInstance);
	}

	_CommonExport Core* GetCore()
	{
		return __pCore;
	}

	_CommonExport void DestroyCore()
	{
		SAFE_DELETE(__pCore);
	}

	//--- Util Global APIs ---
	_CommonExport float GetTime()
	{
		return _TimerMgr::GetTime();
	}

	_CommonExport float GetAccurateTime()
	{
		return _TimerMgr::GetAccurateTime();
	}

	_CommonExport float GetDeltaTime()
	{
		return _TimerMgr::GetDeltaTime();
	}

	_CommonExport float TestMethod(int a)
	{
		return float(a+1);
	}

}
