#pragma once

#include "SafeTypeException.h"

_COMMON_NS_BEGIN


/*
	Safe type will do following checking:
	1. Check whether data is initialized
*/
template<typename UserClass>
class TSafeType
{
public:
	enum VariableState
	{
		STATE_INITED = 0,
		STATE_UNINITED = 1,
	};

public:
	TSafeType()
	{
		_state = STATE_UNINITED;
	}

	TSafeType(const TSafeType& safeValue)
	{
		_state = STATE_INITED;
		_data = (UserClass)safeValue;
	}

	TSafeType(UserClass value)
	{
		_state = STATE_INITED;
		_data = value;
	}

	TSafeType& operator=(const TSafeType& srcValue) 
	{
		_checkInit(srcValue);
		_data = srcValue._data;
		_state = STATE_INITED;
		return *this;
	}

	operator UserClass() const
	{
		_checkInit(*this);
		return _data;
	}

	UserClass* operator & ()
	{
		_checkInit(*this);
		return &_data;
	}

	const UserClass* operator & () const
	{
		_checkInit(*this);
		return &_data;
	}

	// -----------Operators that don't modify "this" but return a new value---------
	friend CONST TSafeType operator + ( CONST TSafeType& src, CONST TSafeType& dest) { _checkInit(src); _checkInit(dest); return TSafeType(src._data + dest._data); }
	friend CONST TSafeType operator + ( CONST UserClass& src, CONST TSafeType& dest) { _checkInit(dest); return TSafeType(src + dest._data); }
	friend CONST TSafeType operator + ( CONST TSafeType& src, CONST UserClass& dest) { _checkInit(src); return TSafeType(src._data + dest); }
	
	friend CONST TSafeType operator - ( CONST TSafeType& src, CONST TSafeType& dest) { _checkInit(src); _checkInit(dest); return TSafeType(src._data - dest._data); }
	friend CONST TSafeType operator - ( CONST UserClass& src, CONST TSafeType& dest) { _checkInit(dest); return TSafeType(src - dest._data); }
	friend CONST TSafeType operator - ( CONST TSafeType& src, CONST UserClass& dest) { _checkInit(src); return TSafeType(src._data - dest); }

	friend CONST TSafeType operator * ( CONST TSafeType& src, CONST TSafeType& dest) { _checkInit(src); _checkInit(dest); return TSafeType(src._data * dest._data); }
	friend CONST TSafeType operator * ( CONST UserClass& src, CONST TSafeType& dest) { _checkInit(dest); return TSafeType(src * dest._data); }
	friend CONST TSafeType operator * ( CONST TSafeType& src, CONST UserClass& dest) { _checkInit(src); return TSafeType(src._data * dest); }

	friend CONST TSafeType operator / ( CONST TSafeType& src, CONST TSafeType& dest) { _checkInit(src); _checkInit(dest); return TSafeType(src._data / dest._data); }
	friend CONST TSafeType operator / ( CONST UserClass& src, CONST TSafeType& dest) { _checkInit(dest); return TSafeType(src / dest._data); }
	friend CONST TSafeType operator / ( CONST TSafeType& src, CONST UserClass& dest) { _checkInit(src); return TSafeType(src._data / dest); }


	// -------------- Operators that will modify "this" -----------------------
	TSafeType& operator += (CONST TSafeType& value) { _checkInit(*this); _checkInit(value); _data += value._data; return *this; }
	TSafeType& operator -= (CONST TSafeType& value) { _checkInit(value); _checkInit(*this); _data -= value._data; return *this; }
	TSafeType& operator *= (CONST TSafeType& value) { _checkInit(*this); _data *= value; return *this; }
	TSafeType& operator /= (CONST TSafeType& value) { _checkInit(*this); _data /= value; return *this; }

	//-------------------------------------------------
	TSafeType operator - () const  { _checkInit(*this); return TSafeType(-_data); }

	//-------------------operator ++ & operator -- ------------------------------
	TSafeType& operator ++ () { _checkInit(*this); (*this) += 1; return *this; }
	TSafeType& operator -- () { _checkInit(*this); (*this) -= 1; return *this; }
	const TSafeType operator ++ (int) { TSafeType oldValue = *this; ++(*this); return oldValue; }
	const TSafeType operator -- (int) { TSafeType oldValue = *this; --(*this); return oldValue; }

	//--------------- compare operators --------------------------------------------
	bool operator <  ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data < dest._data; }
	bool operator <  ( const UserClass& dest ) const { _checkInit(*this); return _data < dest; }
	bool operator <= ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data <= dest._data; }
	bool operator <= ( const UserClass& dest ) const { _checkInit(*this); return _data <= dest; }
	bool operator == ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data == dest._data; }
	bool operator == ( const UserClass& dest ) const { _checkInit(*this); return _data == dest; }
	bool operator != ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data != dest._data; }
	bool operator != ( const UserClass& dest ) const { _checkInit(*this); return _data != dest; }
	bool operator >= ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data >= dest._data; }
	bool operator >= ( const UserClass& dest ) const { _checkInit(*this); return _data >= dest; }
	bool operator >  ( const TSafeType& dest ) const { _checkInit(*this); _checkInit(dest); return _data > dest._data; }
	bool operator >  ( const UserClass& dest ) const { _checkInit(*this); return _data > dest; }

	//--------

protected:
	UserClass 
		_data;
	VariableState
		_state;


protected:
	static INLINE void _checkInit(const TSafeType& type)
	{
		if(type._state != STATE_INITED)
		{
			throw SafeTypeException("Error: uninitialized variable used!", SafeTypeException::ERR_UNINIT_VALUE);
		}
	}

};


#ifdef _DEBUG
	typedef TSafeType<char> Byte;
	typedef TSafeType<int> Int;
	typedef TSafeType<UINT> Uint;
	typedef TSafeType<short> Short;
	typedef TSafeType<bool> Bool;
	typedef TSafeType<BOOL> Boolean;
	typedef TSafeType<float> Float;
	typedef TSafeType<double> Double;
	typedef TSafeType<uint32_t> DWord;

	#if USE_DOUBLE_PRECISION_REAL == 1
		/** Software floating point type.
		@note Not valid as a pointer to GPU buffers / parameters
		*/
		typedef TSafeType<double> Real;
	#else
		/** Software floating point type.
		@note Not valid as a pointer to GPU buffers / parameters
		*/
		typedef TSafeType<float>  Real;
	#endif

#else
	typedef char Byte;
	typedef int Int;
	typedef UINT Uint;
	typedef short Short;
	typedef bool Bool;
	typedef BOOL Boolean;
	typedef float Float;
	typedef double Double;
	typedef uint32_t DWord;

	#if USE_DOUBLE_PRECISION_REAL == 1
		/** Software floating point type.
		@note Not valid as a pointer to GPU buffers / parameters
		*/
		typedef double Real;
	#else
		/** Software floating point type.
		@note Not valid as a pointer to GPU buffers / parameters
		*/
		typedef float  Real;
	#endif

#endif

#ifdef _DEBUG
	_CommonExport bool operator == ( const Uint& src, const int& dest );
	_CommonExport bool operator > ( const Uint& src, const int& dest );
	_CommonExport bool operator >= ( const Uint& src, const int& dest );
	_CommonExport bool operator < ( const Uint& src, const int& dest );
	_CommonExport bool operator <= ( const Uint& src, const int& dest );

	_CommonExport bool operator == ( const Int& src, const UINT& dest );
	_CommonExport bool operator > ( const Int& src, const UINT& dest );
	_CommonExport bool operator >= ( const Int& src, const UINT& dest );
	_CommonExport bool operator < ( const Int& src, const UINT& dest );
	_CommonExport bool operator <= ( const Int& src, const UINT& dest );

	_CommonExport bool operator == ( const Float& src, const double& dest );
	_CommonExport bool operator > ( const Float& src, const double& dest );
	_CommonExport bool operator >= ( const Float& src, const double& dest );
	_CommonExport bool operator < ( const Float& src, const double& dest );
	_CommonExport bool operator <= ( const Float& src, const double& dest );

	_CommonExport bool operator == ( const Double& src, const float& dest );
	_CommonExport bool operator > ( const Double& src, const float& dest );
	_CommonExport bool operator >= ( const Double& src, const float& dest );
	_CommonExport bool operator < ( const Double& src, const float& dest );
	_CommonExport bool operator <= ( const Double& src, const float& dest );
#endif

//------ Safe pointer ----------------
template<typename UserClass>
class Ptr
{
public:
	INLINE Ptr(const Ptr& pPtr)
	{
		(*this) = pPtr;
	}

	INLINE Ptr(UserClass* value)
	{
#ifdef _DEBUG
		_state = STATE_INITED;
#endif
		_pPointer = value;
	}

	INLINE Ptr()
	{
#ifdef _DEBUG
		_state = STATE_UNINITED;
#endif
	}

	Ptr& operator=(const Ptr& srcValue) 
	{
		_pPointer = (UserClass*)srcValue;
#ifdef _DEBUG
		_state = STATE_INITED;
#endif
		return *this;
	}

	Ptr& operator=(const UserClass* srcValue) 
	{
		_pPointer = (UserClass*)srcValue;
#ifdef _DEBUG
		_state = STATE_INITED;
#endif
		return *this;
	}

	INLINE UserClass* operator->() const
	{
		_checkInit(*this);
		XASSERT(_pPointer);
		return _pPointer;
	}

	INLINE operator UserClass*() const 
	{
		_checkInit(*this);
		return _pPointer; 
	}

	INLINE UserClass** operator & ()
	{
		return &_pPointer;
	}

	INLINE Ptr& operator += (CONST int& value) { _checkInit(*this); _pPointer += value; return *this; }
	INLINE Ptr& operator -= (CONST int& value) { _checkInit(*this); _pPointer -= value; return *this; }

	INLINE Ptr& operator ++ () { _checkInit(*this); (*this) += 1; return *this; }
	INLINE Ptr& operator -- () { _checkInit(*this); (*this) -= 1; return *this; }

	INLINE const Ptr operator ++ (int) { Ptr oldValue = *this; ++(*this); return oldValue; }
	INLINE const Ptr operator -- (int) { Ptr oldValue = *this; --(*this); return oldValue; }


protected:
	enum VariableState
	{
		STATE_INITED = 0,
		STATE_UNINITED = 1,
	};

	UserClass*
		_pPointer;

#ifdef _DEBUG
	VariableState
		_state;
#endif

protected:
	static INLINE void _checkInit(const Ptr& type)
	{
#ifdef _DEBUG
		if(type._state != STATE_INITED)
		{
			throw SafeTypeException("Error: uninitialized variable used!", SafeTypeException::ERR_UNINIT_VALUE);
		}
#endif
	}

};

_COMMON_NS_END
