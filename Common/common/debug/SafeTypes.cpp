#include "StdAfx.h"
#include "SafeTypes.h"

_COMMON_NS_BEGIN

#ifdef _DEBUG
bool operator == ( const Uint& src, const int& dest ) 
{
	return src == (UINT)dest; 
}

bool operator > ( const Uint& src, const int& dest ) 
{
	return src > (UINT)dest; 
}

bool operator >= ( const Uint& src, const int& dest ) 
{
	return src >= (UINT)dest; 
}

bool operator < ( const Uint& src, const int& dest ) 
{
	return src <= (UINT)dest; 
}

bool operator <= ( const Uint& src, const int& dest ) 
{
	return src <= (UINT)dest; 
}

//------------

bool operator == ( const Int& src, const UINT& dest )
{
	return src == (int)dest; 
}

bool operator > ( const Int& src, const UINT& dest )
{
	return src > (int)dest; 
}

bool operator >= ( const Int& src, const UINT& dest )
{
	return src >= (int)dest; 
}

bool operator < ( const Int& src, const UINT& dest )
{
	return src < (int)dest; 
}

bool operator <= ( const Int& src, const UINT& dest )
{
	return src <= (int)dest; 
}

//-----

bool operator == ( const Float& src, const double& dest )
{
	return src == (float)dest; 
}

bool operator > ( const Float& src, const double& dest )
{
	return src > (float)dest; 
}

bool operator >= ( const Float& src, const double& dest )
{
	return src >= (float)dest; 
}

bool operator < ( const Float& src, const double& dest )
{
	return src < (float)dest; 
}

bool operator <= ( const Float& src, const double& dest )
{
	return src <= (float)dest; 
}

//-----

bool operator == ( const Double& src, const float& dest )
{
	return src == (double)dest; 
}

bool operator > ( const Double& src, const float& dest )
{
	return src > (double)dest; 
}

bool operator >= ( const Double& src, const float& dest )
{
	return src >= (double)dest; 
}

bool operator < ( const Double& src, const float& dest )
{
	return src < (double)dest; 
}

bool operator <= ( const Double& src, const float& dest )
{
	return src <= (double)dest; 
}
#endif

_COMMON_NS_END