#include "stdafx.h"
#include "_TracerImpl.h"

#include "common/math/Rect.h"
#include "common/util/Unicode.h"

_COMMON_NS_BEGIN

_TracerImpl::_TracerImpl(void)
{
	// All trace is open
	_traceType = TRACE_ALL;
}

_TracerImpl::~_TracerImpl(void)
{
	__strTrace.Empty();
}

LRESULT _TracerImpl::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CRect rect;
	GetClientRect(rect);
	_traceEdit.Create(m_hWnd, rect, TEXT("Trace Editor")
		, WS_CHILD | WS_VISIBLE | ES_MULTILINE | WS_VSCROLL);
	_traceEdit.SetWindowText(TEXT("Begin...\n"));
	return 0;
}

BOOL _TracerImpl::Destroy()
{
	if(!IsWindow()) return TRUE;
	return CWindowImpl::DestroyWindow();
}

void _TracerImpl::WriteTrace(const char* pszTrace, BOOL bShowAlertDlg)
{
	WCHAR* pWStr = NULL;
	Unicode::AnsiToUtf16(pszTrace, &pWStr);
	WriteTrace(pWStr, bShowAlertDlg);
	SAFE_DELETE(pWStr);
}

void _TracerImpl::WriteTrace(const WCHAR* pStr, BOOL bShowAlertDlg)
{
	CString strTrace;
	strTrace+=pStr;
	strTrace += L"\n";
	//_traceEdit.AppendText(strTrace);
	//_traceEdit.LineScroll(100000);

	_tprintf(strTrace);

	if(bShowAlertDlg)
	{
		MessageBox(strTrace, TEXT("Alert"), MB_ICONWARNING);
	}
}

void _TracerImpl::TraceObjectTree(NodeTypePtr pNode)
{
	__strTrace.Empty();
	__indent = 0;
	GenerateTreeTrace(pNode);
	_traceEdit.SetWindowText(__strTrace);
}


void _TracerImpl::GenerateTreeTrace(NodeTypePtr pNode)
{
	CString temp;

	// Indent
	for (int i = 0; i < __indent; ++i)
	{
		__strTrace += L"\t";
	}

	// Obj info
#ifdef DEBUG_NORMAL
	temp.Format(L"ObjName: %s, Address: %p", ((ReferenceCounted*)(pNode))->strObjName, pNode);
#else
	temp.Format(TEXT("Address: %p"), pNode);
#endif // DEBUG_NORMAL
	temp += L"\n";
	__strTrace += temp;

	// Traverse node's children
	__indent++;

	NodeTypePtr pCurNode = pNode->pFirstChild;	
	while (pCurNode)									
	{														
		GenerateTreeTrace(pCurNode);
		pCurNode = pCurNode->pNextSibling;		
	}
	__indent--;
}

LRESULT _TracerImpl::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Rect rect;
	GetClientRect(rect);
	rect.right -=80;
	_traceEdit.MoveWindow(rect);
	return 0;
}

_COMMON_NS_END