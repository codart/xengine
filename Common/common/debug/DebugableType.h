#pragma once

_COMMON_NS_BEGIN

#define  UNKNOWN_FILE_PATH "Unknown file"

#ifdef _DEBUG
	#define ENABLE_DEBUG_TYPE
#endif

#ifdef ENABLE_DEBUG_TYPE
#define ASSIGN_VALUE(debugData, newData)	\
	debugData.SetCurrentDbgInfo(__FILE__, __LINE__);	\
	debugData = newData;

	//----
	#define ASSIGN_NAME(debugData, szName)	\
	debugData.SetName(szName);

	//----
	#define ADD_CHECK_VALUE(debugData, checkType, checkValue)	\
	debugData.AddCheckValue(checkType, checkValue);

#else

	#define ASSIGN_VALUE(debugData, newData)	\
	debugData = newData;

	//----
	#define ASSIGN_NAME(debugData, szName)

	//----
	#define ADD_CHECK_VALUE(debugData, checkType, checkValue)	\
	
#endif // ENABLE_DEBUG_TYPE

enum CheckType
{
	EQUAL_CHECK,
	GREATER_CHECK,
	LESS_CHECK,
};

class _CommonExport DebugableType
{
public:
	DebugableType()
	{
		_pszCurFile = UNKNOWN_FILE_PATH;
		_nCurLine = NULL;
	}

	virtual ~DebugableType() { };

public:
	void 
		SetCurrentDbgInfo(LPCSTR pszFile, int nLine);
	void 
		SetName(LPCSTR pszName);
	
	
protected:
	// Data name
	AString
		_name;
	
	// Debug info
	LPCSTR 
		_pszCurFile;
	int 
		_nCurLine;
	
};

_COMMON_NS_END
