#include "StdAfx.h"
#include "Tracer.h"

#include "common/debug/_TracerImpl.h"

_COMMON_NS_BEGIN


Tracer::Tracer()
{
	_pTracerImpl = NULL;
}

void Tracer::Create(unsigned long pUserData)
{
	if(!_pTracerImpl) _pTracerImpl = new _TracerImpl;

	_pTracerImpl->Create((HWND)pUserData);
}

LONG Tracer::Destroy()
{
	XASSERT(_pTracerImpl);
	long bRet = _pTracerImpl->Destroy();
	SAFE_DELETE(_pTracerImpl); 
	return bRet;
}

void Tracer::ShowWindow(BOOL bShow)
{
	_pTracerImpl->ShowWindow(bShow? SW_SHOW : SW_HIDE);
}

void Tracer::Resize(int width, int height)
{
	_pTracerImpl->ResizeClient(width, height, TRUE);
}

void Tracer::WriteTrace(const wchar_t* pStrTrace, BOOL bShowAlertDlg)
{
	_pTracerImpl->WriteTrace(pStrTrace, bShowAlertDlg);
}

void Tracer::WriteTrace(const char* pszTrace, BOOL bShowAlertDlg)
{
	_pTracerImpl->WriteTrace(pszTrace, bShowAlertDlg);
}

void Tracer::TraceObjectTree(NodeTypePtr pNode)
{
	_pTracerImpl->TraceObjectTree(pNode);
}

_COMMON_NS_END