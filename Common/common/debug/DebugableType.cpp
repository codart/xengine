#include "stdafx.h"
#include "DebugableType.h"

_COMMON_NS_BEGIN

void DebugableType::SetCurrentDbgInfo(LPCSTR pszFile, int nLine)
{
	_pszCurFile = pszFile;
	_nCurLine = nLine;
}

void DebugableType::SetName(LPCSTR pszName)
{
	_name = pszName;
}


_COMMON_NS_END