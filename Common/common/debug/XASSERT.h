#pragma once

#include "assert.h"

_COMMON_NS_BEGIN

void _CommonExport OnErrorTrap();
void _CommonExport EnableErrorTrap(bool bEnable);

_COMMON_NS_END