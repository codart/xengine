#pragma once
#pragma warning(disable: 4275)
#pragma warning(disable: 4251)

#include "common/util/LinkedTreeNode.h"
#include "common/core/Singleton.h"

_COMMON_NS_BEGIN

enum TRACE_METHOD
{
	TRACE_TO_ALL		= 0xffffffff,
	TRACE_TO_FILE		= 1<<0,
	TRACE_TO_DBGWND		= 1<<1,
	TRACE_TO_OUTPUT		= 1<<2,
};

class _TracerImpl;

/***********************************************************************
	*. Add console for Stage. Trace can be write to stage's console.
***********************************************************************/
class _CommonExport Tracer : public Singleton<Tracer, AutoRelease>
{
public:
	Tracer();

public:
	void Create(unsigned long pUserData);
	long Destroy();
	void WriteTrace(const wchar_t* pStrTrace, BOOL bShowAlertDlg = false);
	void WriteTrace(const char* pszTrace, BOOL bShowAlertDlg = false);
	void TraceObjectTree(NodeTypePtr pNode);
	void ShowWindow(BOOL bShow);
	void Resize(int width, int height);


protected:
	_TracerImpl*
		_pTracerImpl;

private:

};

_COMMON_NS_END