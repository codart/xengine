#include "stdafx.h"
#include "xassert.h"

_COMMON_NS_BEGIN

static bool g_bAssertTrap = true;

void OnErrorTrap()
{
	if(g_bAssertTrap)
	{
		// Some assert will not popup any dialog during app underutilization. 
		// Use this to make sure the app is break when assert failed. 
		__asm 
		{
			int 3 
		}
	}
}

void EnableErrorTrap(bool bEnable)
{
	g_bAssertTrap = bEnable;
}

_COMMON_NS_END