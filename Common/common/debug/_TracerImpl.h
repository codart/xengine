#pragma once

#include "common/util/LinkedTreeNode.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

#include <atlbase.h>
#include <atlapp.h>

#include <atlcom.h>
#include <atlctl.h>

#include <atlwin.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlmisc.h>
#include <atlctrls.h>

using namespace ATL;


typedef CWinTraits<WS_OVERLAPPED | WS_SYSMENU | WS_THICKFRAME> CTracerWndTraits;

_COMMON_NS_BEGIN


enum TRACE_TYPE
{
	TRACE_ALL		= 0xffffffff,
	TRACE_INFO		= 1<<0,
	TRACE_WARNING	= 1<<1,
	TRACE_ERROR		= 1<<2,
	TRACE_FATAL_ERR = 1<<3,
};


class _TracerImpl : public CWindowImpl<_TracerImpl, CWindow, CTracerWndTraits>
{
public:
	_TracerImpl(void);
	virtual ~_TracerImpl(void);


public:
	BOOL Destroy();
	void WriteTrace(const WCHAR* pStrTrace, BOOL bShowAlertDlg = FALSE);
	void WriteTrace(const char* pszTrace, BOOL bShowAlertDlg = FALSE);
	void TraceObjectTree(NodeTypePtr pNode);


protected:
	void GenerateTreeTrace(NodeTypePtr tree);

public:

	BEGIN_MSG_MAP(Tracer)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		//MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
	END_MSG_MAP()

private:
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return ShowWindow(SW_HIDE);
	}
	LRESULT OnMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return 0;
	}
	LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

public:
	CRichEditCtrl 
		_traceEdit;

	DWORD
		_traceMethod; // Combination of TRACE_METHOD
	DWORD
		_traceType; // Combination of TRACE_TYPE

private:
	CString 
		__strTrace;
	int 
		__indent;
};

_COMMON_NS_END