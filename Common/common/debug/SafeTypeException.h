#pragma once

#include "common/core/Exception.h"

_COMMON_NS_BEGIN

class SafeTypeException : public Exception
{
public:
	enum SafeTypeErrorCode
	{
		ERR_UNKNOWN = 0,
		ERR_UNINIT_VALUE, 
	};

public:
	SafeTypeException(const std::string& strReason) : Exception(strReason) { _errorCode = ERR_UNKNOWN; };
	SafeTypeException(const std::string& strReason, SafeTypeErrorCode errorCode):Exception(strReason) { _errorCode = errorCode; };
	virtual ~SafeTypeException() throw () { };


protected:
	SafeTypeErrorCode
		_errorCode;

};


_COMMON_NS_END

