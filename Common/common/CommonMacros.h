#pragma once

/// Note: Set this value to 1 will cause 'REAL' equal 'double'. And this will cause error for all math types
/// such as Vector2/Vector3/Vector4/Matrix when using with DX/OpenGL (since they both use float as 'REAL').
/// So when use with DX/OpenGL, make sure aways set this value to 0!
//#define USE_DOUBLE_PRECISION_REAL 0

#if defined( COMMON_EXPORTS )
#define _CommonExport __declspec( dllexport )
#else
#define _CommonExport __declspec( dllimport )
#endif


#define _COMMON_NS_BEGIN namespace common {
#define _COMMON_NS_END		}


#ifdef _DEBUG
#define INLINE
#else
#define INLINE __forceinline
#endif // _DEBUG


// --- Switchs ---
/* 
	define them in project pre-define macros
*/
/* Enable SharedPtr checking. After enabled, if raw pointer be assigned to more than one SharedPtr, will throw an exception. */
 #define FORCE_CHECK_SHARED_PTR
// #define SERIOUS_CHECK  

#ifndef FORCE_CHECK_SHARED_PTR
	#ifdef _DEBUG
		#define FORCE_CHECK_SHARED_PTR
	#endif
#endif

// Macro values
// Note: XXXX_VALUE macros can be used in #if to compose expression, such as #if DEBUG_VALUE || XXX_VALUE
#ifdef _DEBUG
	#define DEBUG_VALUE	1
#else
	#define DEBUG_VALUE 0
#endif


// --- Language extension macros
#define debug		public
#ifdef _DEBUG
	#define debug_variable(type, field)	type field
	#define debug_assign(field, value) field = value
	#define debug_ptr_assign(pObject, field, value) pObject->field = value
	#define debug_check(expression)	XASSERT(expression)
	#define debug_exp(expression) expression
#else
	#define debug_variable(type, field)
	#define debug_assign(field, value)
	#define debug_exp(expression)
	#define debug_ptr_assign(pObject, field, value)
	#define debug_check(expression)
#endif // _DEBUG

		
#define debug_field_end

#define implements public
#define package public
#define IN
#define OUT
#define overload
#define override virtual
#define final
#define no_v_base
#define CONST const
#define temporary 
#define transient	// The pointer of reference is transient, should not store the pointer/reference.
#define no_direct_concrete
#define need_direct_concrete

// --- basic types
//#define String wstring
#ifdef _UNICODE
	#define StringStream wstringstream
#else
	#define StringStream stringstream
#endif

#define AStringStream stringstream
#define AString string
#define WString wstring
#define Hashmap stdext::hash_map

#ifdef TEXT
#undef TEXT
#endif

#ifdef _UNICODE
	#define TEXT(str) L##str
#else
	#define TEXT(str) str
#endif


#ifndef DWORD
	#define DWORD unsigned long
#endif

#ifndef DWORD_PTR
	#define DWORD_PTR DWORD
#endif

#ifndef LONG
	#define LONG long
#endif

#ifndef BYTE
	#define BYTE unsigned char
#endif

#ifndef BOOL
	#define BOOL int
#endif

#ifndef TRUE
	#define TRUE -1
#else 
	#undef TRUE
	#define TRUE -1
#endif

#ifndef FALSE
	#define FALSE 0
#endif


// Type definition
#ifdef _WIN32
		typedef __int64	int64_t;
		typedef unsigned __int64	uint64_t;

		typedef	__int32 int32_t;
		typedef	unsigned int uint32_t;

		typedef unsigned __int16 int16_t;
		typedef unsigned __int8 int8_t;

		typedef unsigned __int16 uint16_t;
		typedef unsigned __int8 uint8_t;
#endif


// --- Utility //////////////////////////////////////////////////////////////////////////
#if _DEBUG
	#define RELEASE_CHECK(pUserData, nCheckedRefCount)		\
		if(pUserData)							\
			{										\
			int nRef = pUserData->AddRef();		\
			XASSERT((nCheckedRefCount + 2) == nRef);					\
			pUserData->Release();				\
			}

	#define RELEASE_CHECK_EX(bDoCheck, pUserData, nCheckedRefCount)	\
		if(bDoCheck) RELEASE_CHECK(pUserData, nCheckedRefCount)

	#define SAFE_CHECKED_RELEASE(p, checkValue) RELEASE_CHECK(p, checkValue); SAFE_RELEASE(p)
#else
	#define RELEASE_CHECK(pUserData, nCheckedRefCount)

	#define RELEASE_CHECK_EX(bDoCheck, pUserData, nCheckedRefCount)

	#define SAFE_CHECKED_RELEASE(p, checkValue) SAFE_RELEASE(p)
#endif

// ---
#define DECLARE_LIST_TYPE(containerType, elementType)	\
	typedef containerType<elementType> elementType##List;	 \
	typedef elementType##List::iterator	elementType##ListIt; \
	typedef elementType##List::const_iterator elementType##ListConstIt

#define DECLARE_LIST_TYPE_PTR(containerType, elementType)	\
	typedef containerType<elementType*> elementType##List;	\
	typedef elementType##List::iterator	elementType##ListIt;	\
	typedef elementType##List::const_iterator	elementType##ListConstIt;	\
	typedef VectorIterator<elementType*> elementType##Iterator;	\
	typedef ConstVectorIterator<elementType*> elementType##Const##Iterator

#define DECLARE_MAP_TYPE(mapContainerType, keyType, elementType)	\
	typedef mapContainerType<keyType, elementType> elementType##Map;	\
	typedef elementType##Map::iterator	elementType##MapIt;				\
	typedef MapIterator<keyType, elementType> elementType##Map##Iterator

#define DECLARE_MAP_TYPE_PTR(mapContainerType, keyType, elementType)	\
	typedef mapContainerType<keyType, elementType*> elementType##Map;	\
	typedef elementType##Map::iterator	elementType##MapIt;				\
	typedef elementType##Map::const_iterator	elementType##Map##Const##It;	\
	typedef MapIterator<keyType, elementType*> elementType##Map##Iterator

// ---
//...Implement new with memory pool later(reference OGRE)
#ifdef _DEBUG
	#define XNEW new
	#define XDELETE delete
	#define XFREE free
	#define XALLOCA malloc
#else
	#define XNEW new
	#define XDELETE delete
	#define XFREE free
	#define XALLOCA malloc	
#endif // _DEBUG

//---
#define GPU_PROGRAM_SCRIPT_PATH common::ModuleUtil::GetGPUProgramScriptPath()
#define GPU_PROGRAM_PATH common::ModuleUtil::GetGPUProgramPath()
#define MATERIAL_PATH common::ModuleUtil::GetMaterialPath()
#define TEXTURE_PATH common::ModuleUtil::GetTexturePath()
#define MODEL_PATH common::ModuleUtil::GetModelPath()
#define UI_PATH common::ModuleUtil::GetUIPath()
#define RESOURCE_PATH common::ModuleUtil::GetResourcePath()
#define MODULE_PATH common::ModuleUtil::GetModulePath()

#ifdef _DEBUG
	#define XASSERT(expression)		\
		if(!(expression))	\
		{	\
			common::OnErrorTrap();	\
		}	\
		assert(expression)
#else
	#define XASSERT(expression)
#endif

#ifdef SERIOUS_CHECK
	#define SASSERT(expression)		\
	if(!(expression))	\
	{	\
		common::OnErrorTrap();	\
	}	\
	assert(expression)
#else
	#define SASSERT(expression)
#endif

#define XWARNING(expression) // Implement latter

// --------- Compile time message alert support ---------------
#define LINE_TO_STR_HELPER0(line) #line
#define LINE_TO_STR_HELPER1(_LINE_) LINE_TO_STR_HELPER0(_LINE_)
#define LINE_TO_STRING() LINE_TO_STR_HELPER1(__LINE__)
#define XALERT(msg) message(__FILE__ "(" LINE_TO_STRING() ") : alert: "msg)

/*
	Usage:

	foo()
	{
		#pragma XALERT("Should modify later!!!")
	}

	when foo() be compiled, it will generate following message:
	e:\....\foo.cpp(275) : alert: Should modify later!!!
*/

// --------------------------------------------------------------

#define SAFE_RELEASE(p) if(p) p->Release(); p = NULL
#define SAFE_DELETE(p) if(p) XDELETE p; p = NULL

/* 
	Simple wrap for dynamic_cast. 
	Note: dynamic cast is low efficiency in condition of complex inheritance. 
	//... Should implement a quick RTTI later!!!!
*/
#define DYNAMIC_CAST(pPointer, ToType) dynamic_cast<ToType>(pPointer)

// ------------------- Runtime check safe cast support ---------------
template<class fromType, class ToType>
ToType* CastCheck(fromType* pPointer, ToType* pDummmy)
{
	ToType* pDynamicCastResult = dynamic_cast<ToType*>(pPointer);
	// Add a static_cast to make it generate compiler error if conversion is invalid
	ToType* pStaticCastResult = static_cast<ToType*>(pPointer); 
	if(!pDynamicCastResult || pDynamicCastResult != pStaticCastResult)
	{
		__asm int 3;
	}
	else return pDynamicCastResult;
}

/* 
	This cast will do safe check in debug mode. 

	Note: SAFE_CAST will use dynamic_cast/static_cast in debug mode and force type conversion in release mode. 
	It will issue an assert in debug mode if the cast is incorrect. It means your code need at least be executed
	once, or else the SAFE_CAST is not safe :)
*/
#ifdef _DEBUG
	#define SAFE_CAST(pPointer, ToType)	CastCheck((pPointer), (ToType)NULL)
#else
	#define SAFE_CAST(pPointer, ToType) (static_cast<ToType>(pPointer))
#endif

#define EVENT_CAST(pPointer, ToType) (const ToType) ( SAFE_CAST(const_cast<Event*>(pPointer), ToType) )

// -----------------------------------------------------------------------------

#define DbgMsgBox(string) ::MessageBox(::GetActiveWindow(), string, TEXT("Error"), 0)
#define DbgMsgBoxA(string) ::MessageBoxA(::GetActiveWindow(), string, "Error", 0)


#define ShowTraceWindow(bShow) common::Tracer::Instance().ShowWindow(bShow)
#define Trace(str) common::Tracer::Instance().WriteTrace(str)
#define TraceString(str) common::Tracer::Instance().WriteTrace(str.c_str())
#define ResizeTraceWindow(width, height) common::Tracer::Instance().Resize(width, height)

#ifdef _DEBUG
#define _XTHROW(exceptionType, strReason, strFunctionName)	\
		{	\
		std::stringstream logSS;	\
		Trace(L"Exception occured: ");	\
		Trace(#exceptionType);	\
		logSS << "File Name: " <<__FILE__ << " Function Name: "<<__FUNCTION__<< " Line: " << __LINE__;	\
		Trace(logSS.str().c_str()); \
		OnErrorTrap();	\
		throw exceptionType(strReason, strFunctionName);	\
		}		

#define XTHROW(exceptionType)	 \
		{	\
		std::stringstream logSS;	\
		Trace(L"Exception occured: ");	\
		Trace(#exceptionType);	\
		logSS << "File Name: " <<__FILE__ << " Function Name: "<<__FUNCTION__<< " Line: " << __LINE__;	\
		Trace(logSS.str().c_str()); \
		OnErrorTrap();	\
		throw exceptionType();	\
		}

#define XTHROWEX(pszReason)	\
		{	\
		std::stringstream logSS;	\
		Trace(L"Exception occured: \n Exception String: ");	\
		Trace(pszReason);		\
		logSS << "File Name: " <<__FILE__ << " Function Name: "<<__FUNCTION__<< " Line: " << __LINE__;		\
		Trace(logSS.str().c_str()); \
		OnErrorTrap();	\
		throw Exception(pszReason);	\
		}
#else
#define _XTHROW(exceptionType, strReason, strFunctionName) throw exceptionType(strReason, strFunctionName)
#define XTHROW(exceptionType) throw exceptionType();
#define XTHROWEX(pszReason) throw common::Exception(pszReason);
#endif

