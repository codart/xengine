#include "StdAfx.h"

#include "common/algorithm/ObjectPool.h"
#include "TimerMgr.h"

_COMMON_NS_BEGIN

// TimerData's Pool ////////////////////////////////////////////////////
class _TimerDataPool : public Singleton<_TimerDataPool, AutoRelease>
					, public ObjectPool<TimerData*> {};

//////////////////////////////////////////////////////////////////////////

list<TimerData*> _TimerMgr::_timerDataList;
vector<list<TimerData*>::iterator> _TimerMgr::_vecDeadTimerDataIts;
BOOL _TimerMgr::_bHasDeadTimer = FALSE;

float _TimerMgr::_fCurrentTime	= 0;
float _TimerMgr::_fDeltaTime		= 0;
float _TimerMgr::_fLastUpdateTime	= 0;

__int64 _TimerMgr::_performanceCounterFrequency = 0;
float _TimerMgr::_timeResolution = 0;

ULONG _TimerMgr::_startTime = 0;

BOOL _TimerMgr::_bUsePerformanceCounter = 0;
__int64 _TimerMgr::_startPerformanceCount = 0;

DWORD_PTR _TimerMgr::dwProcessorMask = 0;

_TimerMgr::_TimerMgr(void)
{
}

_TimerMgr::~_TimerMgr()
{
	//ResetTimerMgr();
}

void _TimerMgr::Initialize()
{
	timeBeginPeriod(1);
	ResetTimerMgr();
}

void _TimerMgr::Uninitialize()
{
#ifdef _DEBUG
	// It is recommend to Kill all timer before exit
	BOOL bAllTimerKilled = TRUE;
	ListTimerDataIt it = _timerDataList.begin();
	for ( ;it != _timerDataList.end(); ++it)
	{
		TimerData* pTimerData = *it;
		if(pTimerData->state != TIMER_DEADED)
		{
			bAllTimerKilled = FALSE;
			break;
		}
	}
	XASSERT(bAllTimerKilled);
#endif // _DEBUG

	ResetTimerMgr();
	timeEndPeriod(1);
}

float _TimerMgr::GetAccurateTime()
{
	return _getCurrentTime();
};

TimerData* _TimerMgr::CreateTimerData()
{
	TimerData* pTimerData = _TimerDataPool::Instance().GetInstance();
	XASSERT(pTimerData);

	_timerDataList.push_back(pTimerData);
	return pTimerData;
}

void _TimerMgr::ReleaseTimerData(TimerData* pTimerData)
{
	pTimerData->state = TIMER_DEADED;
	_bHasDeadTimer = TRUE;
}

void _TimerMgr::ReleaseTimerData(Timer* pTimer)
{
	ListTimerDataIt it = _timerDataList.begin();
	ListTimerDataIt itEnd = _timerDataList.end();

	for ( ; it != itEnd; ++it)
	{
		TimerData* pTimerData = *it;
		if(pTimerData->pTimer != pTimer) continue;

		pTimerData->state = TIMER_DEADED;
		_bHasDeadTimer = TRUE;
	}
}

void _TimerMgr::ResetTimerMgr()
{
	// Release all timerData
	ListTimerDataIt it = _timerDataList.begin();
	for ( it; it != _timerDataList.end(); ++it)
	{
		TimerData* pTimerData = *it;
		XASSERT(pTimerData);
		pTimerData->pTimer->Release();
		_TimerDataPool::Instance().FreeInstance(pTimerData);
	}
	_timerDataList.clear();
	
	_vecDeadTimerDataIts.clear();

	// Set Affinity to support multi-processor.
	DWORD_PTR procMask;
	DWORD_PTR sysMask;
	GetProcessAffinityMask(GetCurrentProcess(), &procMask, &sysMask);

	// If procMask is 0, consider there is only one core available
	// (using 0 as procMask will cause an infinite loop below)
	if (procMask == 0)
		procMask = 1;

	// Find the lowest core that this process uses
	if( dwProcessorMask == 0 )
	{
		dwProcessorMask = 1;
		while( ( dwProcessorMask & procMask ) == 0 )
		{
			dwProcessorMask <<= 1;
		}
	}

	HANDLE thread = GetCurrentThread();

	// Set affinity to the first core
	DWORD_PTR oldMask = SetThreadAffinityMask(thread, dwProcessorMask);	

	// Check if performance counter is available
	if (QueryPerformanceFrequency((LARGE_INTEGER *) &_performanceCounterFrequency))
	{
		// Performance counter Is Available
		_bUsePerformanceCounter = TRUE;

		QueryPerformanceCounter((LARGE_INTEGER *) &_startPerformanceCount);

		_timeResolution = (float) (((double)1.0f)/((double)_performanceCounterFrequency));
	}
	else
	{
		// No performance counter available
		_bUsePerformanceCounter = FALSE;
		_startTime	= timeGetTime();
		_fLastUpdateTime = 0;

		// Set Our timer resolution to .001f
		_timeResolution = 1.0f/1000.0f;	
	}

	// Reset affinity
	SetThreadAffinityMask(thread, oldMask);
}

void _TimerMgr::_removeDeadTimers()
{
	if(!_bHasDeadTimer) return;

	if(!_vecDeadTimerDataIts.empty())
	{
		VecTimerDataItIt timerDataItIt = _vecDeadTimerDataIts.begin();
		TimerData* pTimerData;
		for ( ; timerDataItIt != _vecDeadTimerDataIts.end(); ++timerDataItIt)
		{
			pTimerData = (TimerData*)(*(*timerDataItIt));

			XASSERT(pTimerData);
			XASSERT(pTimerData->pTimer);

			pTimerData->pTimer->RemoveEvent((int)pTimerData);
			pTimerData->pTimer->Release();
			_TimerDataPool::Instance().FreeInstance(pTimerData);

			_timerDataList.erase(*timerDataItIt);
		}

		_vecDeadTimerDataIts.clear();
		_bHasDeadTimer = FALSE;
	}
}

void _TimerMgr::_dispatchTimerEvents()
{
	ListTimerDataIt it = _timerDataList.begin();

	TimerEvent timerEvent(NULL);
	if(FALSE == _bHasDeadTimer)
	{
		for ( it; it != _timerDataList.end(); ++it)
		{
			XASSERT((*it));
			TimerData* pTimerData = *it;
			if( TIMER_RUNNING != pTimerData->state ||
				(_fCurrentTime - pTimerData->fLastUpdateTime) <  pTimerData->fInterval
				)
				continue;

			pTimerData->fLastUpdateTime = _fCurrentTime;			
			timerEvent.timerID = (DWORD)pTimerData;
			XASSERT(pTimerData->pTimer);
			pTimerData->pTimer->DispatchEvent(timerEvent);
		}
	}
	else
	{
		for ( it; it != _timerDataList.end(); ++it)
		{
			XASSERT((*it));
			TimerData* pTimerData = *it;

			if( TIMER_DEADED == pTimerData->state )
			{
				_vecDeadTimerDataIts.push_back(it);
				continue;
			}
			else if( (_fCurrentTime - pTimerData->fLastUpdateTime) < pTimerData->fInterval 
					|| TIMER_PAUSED == pTimerData->state ) 
			{
				continue;
			}

			pTimerData->fLastUpdateTime = _fCurrentTime;
			timerEvent.timerID = (DWORD)pTimerData;
			XASSERT(pTimerData->pTimer);
			pTimerData->pTimer->DispatchEvent(timerEvent);
		}
	}
}

float _TimerMgr::_getCurrentTime()
{
	HANDLE thread = GetCurrentThread();

	// Set affinity to the first core
	DWORD_PTR oldMask = SetThreadAffinityMask(thread, dwProcessorMask);

	if (_bUsePerformanceCounter)
	{
		// Get current performance count
		__int64 currentPerformanceCount;
		QueryPerformanceCounter((LARGE_INTEGER*) &currentPerformanceCount);	

		// Reset affinity
		SetThreadAffinityMask(thread, oldMask);

		// Convert to millisecond
		return (float) (currentPerformanceCount - _startPerformanceCount) 
			* _timeResolution;
	}
	else
	{
		// Reset affinity
		SetThreadAffinityMask(thread, oldMask);

		// Convert to millisecond
		return (float) ( timeGetTime() - _startTime) * _timeResolution;
	}
}

_COMMON_NS_END