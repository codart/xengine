#pragma once

_COMMON_NS_BEGIN

class StopWatch
{
public:
	StopWatch()
	{
		_fDeltaTime = 0;
		debug_assign(state, stopped);
	}


public:
	/// Start time
	INLINE void 
		Start() 
	{ 
		debug_check(state == stopped); 
		_fDeltaTime = ::GetAccurateTime(); 
		debug_assign(state, timing); 
	}

	/// Stop time, and return the delta time between 'Start - Stop'.
	INLINE Real 
		Stop() 
	{ 
		debug_check(state == timing); 
		debug_exp(state = stopped); 
		return _fDeltaTime = (REAL)::GetAccurateTime() - _fDeltaTime; 
	}

	/// Return the las delta time.
	INLINE Real 
		GetDeltaTime() { XASSERT(state == stopped); return _fDeltaTime; }


protected:
	Real 
		_fDeltaTime;

debug:
	enum _State
	{
		stopped,
		timing,
	};

	debug_variable(_State, state);
};

_COMMON_NS_END