#pragma once

#include "mmsystem.h"

#pragma comment(lib,"winmm.lib")

#include "common/core/object.h"
#include "common/core/EventDispatcher.h"
#include "common/time/TimerEvent.h"
#include "common/time/Timer.h"

_COMMON_NS_BEGIN

/*
	TimerMgr managed all timer's data (TimerData) and scheduler them to dispatch the timer 
	event. 
	The TimerMgr's Update() need to be called in the main loop to update time data.
*/
class _CommonExport _TimerMgr
{
	friend class Timer;

public:
	_TimerMgr(void);
	virtual ~_TimerMgr(void);

public:
	static void 
		Initialize();

	static void
		Uninitialize();

	static void 
		ResetTimerMgr();

	INLINE static void
		BeginFrame()
	{
		_fLastUpdateTime = _getCurrentTime();
	}

	INLINE static float 
		EndFrame() 
	{
		_fCurrentTime = _getCurrentTime();
		_fDeltaTime = _fCurrentTime - _fLastUpdateTime;
		return _fDeltaTime;
	}

	INLINE static void 
		DispatchTimerEvent()
	{
		_dispatchTimerEvents();
		_removeDeadTimers();
	}

	// Retrieves the number of milliseconds that have elapsed since last frame
	INLINE static float 
		GetDeltaTime()
	{ 
		return _fDeltaTime; 
	}

	/* 
		Retrieves the milliseconds that had elapsed since the engine was started.
		This function is depend on the engine loop. It will return a incorrect value 
		if the engine loop is blocked. 

		For example, if you call GetTime() during a for() loop, the return value will 
		be all the same since the _fCurrentTime is not updated. Use GetAccurateTime() 
		instead for similar scenario.
	*/
	INLINE static float 
		GetTime()
	{
		return _fCurrentTime;
	}

	static float 
		GetAccurateTime();

	INLINE static void
		_AdjustDeltaTime(float fSmoothedDeltaTime)
	{
		_fDeltaTime = fSmoothedDeltaTime;
	}


protected:
	// Add\Remove timer data from _mapTimerDatas. 
	static TimerData*
		CreateTimerData();
	static void
		ReleaseTimerData(TimerData* pTimerData);
	// Release all timer data create by specified Timer
	static void
		ReleaseTimerData(Timer* pTimer);

	INLINE static float 
		_getCurrentTime();
	static void 
		_updateTimeVariables();
	static void 
		_dispatchTimerEvents();
	static void 
		_removeDeadTimers();


protected:
	typedef list<TimerData*>::iterator ListTimerDataIt;
	typedef vector<ListTimerDataIt>::iterator VecTimerDataItIt;

	static list<TimerData*>
		_timerDataList;

	static vector<ListTimerDataIt>
		_vecDeadTimerDataIts;

	static BOOL
		_bHasDeadTimer;

protected:
	static float 
		_fCurrentTime;
	static float 
		_fDeltaTime;
	static float 
		_fLastUpdateTime;

	static __int64       
		_performanceCounterFrequency;	

	// Interval of two CPU circle (1/_performanceCounterFrequency) 
	static float         
		_timeResolution;	

	// Multimedia timer Start Value
	static ULONG 
		_startTime;

	static BOOL		  
		_bUsePerformanceCounter;			

	// Performance timer Start Value
	static __int64       
		_startPerformanceCount;


	static DWORD_PTR
		dwProcessorMask;
};

_COMMON_NS_END