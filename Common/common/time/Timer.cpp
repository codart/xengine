#include "StdAfx.h"
#include "Timer.h"
#include "TimerMgr.h"

_COMMON_NS_BEGIN

Timer::Timer(void)
{
}

Timer::~Timer(void)
{
}

DWORD Timer::SetTimer(int nInterval, int nTimes)
{
	float fInterval = (float)nInterval/1000;
	return SetTimer(fInterval, nTimes);
}

DWORD Timer::SetTimer(float fInterval, int nTimes)
{
	TimerData* pTimerData = _TimerMgr::CreateTimerData();
	XASSERT(pTimerData);

	pTimerData->fInterval = fInterval;
	pTimerData->state = TIMER_RUNNING;
	pTimerData->pTimer = this;
	pTimerData->fLastUpdateTime = _TimerMgr::GetTime();
	pTimerData->nTimes = nTimes;
	pTimerData->nTimesCount = nTimes;

	this->AddRef();

	if(nTimes != INFINITE)
	{
		AddEventListener((DWORD)pTimerData, MFUNC(&Timer::_onTimer, this));
	}

	return (DWORD)pTimerData;
}

void Timer::KillTimer(DWORD nTimerID)
{
	XASSERT(nTimerID);
	_TimerMgr::ReleaseTimerData((TimerData*)nTimerID);
}

void Timer::KillAllTimers()
{
	_TimerMgr::ReleaseTimerData(this);
}

void Timer::PauseTimer(DWORD nTimerID)
{
	XASSERT(nTimerID);
	((TimerData*)nTimerID)->state = TIMER_PAUSED;
}

void Timer::ResumeTimer(DWORD nTimerID)
{
	XASSERT(nTimerID);
	((TimerData*)nTimerID)->state = TIMER_RUNNING;
}

void Timer::SetTimerInterval(DWORD nTimerID, int nInterval)
{
	SetTimerInterval(nTimerID, (float)nInterval/1000);
}

void Timer::SetTimerInterval(DWORD nTimerID, float fInterval)
{
	XASSERT(nTimerID);
	if(NULL == (DWORD_PTR)nTimerID)
	{
		XTHROW(IncorrectAddressException);
	}

	((TimerData*)nTimerID)->fInterval = fInterval;
}

void Timer::_onTimer(const Event* pEvent)
{
	if( --((TimerData*) ( ((TimerEvent*)pEvent)->timerID ))->nTimesCount == 0)
	{
		PauseTimer( ((TimerEvent*)pEvent)->timerID );
	}
}

TimerState Timer::GetTimerState(DWORD nTimerID) const
{
	XASSERT(nTimerID);
	return ((TimerData*)nTimerID)->state;
}

void Timer::ResetTimer(DWORD nTimerID)
{
	XASSERT(nTimerID);
	((TimerData*)nTimerID)->nTimesCount = ((TimerData*)nTimerID)->nTimes;
	((TimerData*)nTimerID)->fLastUpdateTime = GetTime();
	((TimerData*)nTimerID)->state = TIMER_RUNNING;
}


_COMMON_NS_END