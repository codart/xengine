
/*************** Timer & TimerData ****************************************

	TimerDatas in TimerMgr::_mapTimerDatas will be traversed during TimerMgr::Update(); 
	If the currentTime - TimerData::fLastUpdateTime > TimerData::nInterval, it will fires a
	TimerEvent(by TimerData::pTimer).

/************************************************************************/

#pragma once

#include "common/core/EventDispatcher.h"
#include "common/time/TimerEvent.h"
#include "common/core/Final.h"

_COMMON_NS_BEGIN

class Timer;

enum TimerState
{
	TIMER_RUNNING,
	TIMER_PAUSED,
	TIMER_DEADED,
};

/*
	TimerData stores data related with a timer event.
	Note: The address of the TimerData is the timerID
*/
struct TimerData 
{
	float 
		fInterval; 
	float 
		fLastUpdateTime;
	TimerState
		state;
	Timer* 
		pTimer;
	int
		nTimes;
	int
		nTimesCount;
};

class _CommonExport Timer : public Final<Timer>
							, public ReferenceCounted
							, public EventDispatcher
{
public:
	Timer(void);
	virtual ~Timer(void);

	/**
		nInterval: interval in Milliseconds.
	*/
	DWORD
		SetTimer(int nInterval, int nTimes = INFINITE);

	/**
		fInterval: interval in seconds.
	*/
	DWORD
		SetTimer(float fInterval, int nTimes = INFINITE);

	TimerState
		GetTimerState(DWORD nTimerID) const;

	void 
		KillTimer(DWORD nTimerID);

	void 
		KillAllTimers();

	void 
		PauseTimer(DWORD nTimerID);

	void 
		ResumeTimer(DWORD nTimerID);

	void
		SetTimerInterval(DWORD nTimerID, int nInterval);

	void
		SetTimerInterval(DWORD nTimerID, float nInterval);

	void 
		ResetTimer(DWORD nTimerID);

private:
	void 
		_onTimer(const Event* pEvent);

};


_COMMON_NS_END