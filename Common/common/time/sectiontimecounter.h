#pragma once

_COMMON_NS_BEGIN

class SectionTimeCounter
{
public:
	SectionTimeCounter(Real& value)
		: _value(value)
	{
		_fConstructTime = GetAccurateTime();
	}
	
	~SectionTimeCounter()
	{
		_value += (REAL)GetAccurateTime() - _fConstructTime;
	}

protected:
	Real 
		_fConstructTime;
	Real& 
		_value;

};

_COMMON_NS_END