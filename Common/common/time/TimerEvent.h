#pragma once
#include "common/core/event.h"

_COMMON_NS_BEGIN

/*
	TimerEvent's event type(nType) is just the TimerData's address
	
	Below is the code to dispatching a TimerEvent: 
	TimerEvent timerEvent((DWORD)pTimerData);
	(*it).second->pTimer->DispatchEvent(timerEvent);

	See _TimerMgr::_dispatchTimerEvents() for more details.

*/
class _CommonExport TimerEvent : public Event
{
public:
	TimerEvent(int nType);

};

_COMMON_NS_END