#pragma once


_COMMON_NS_BEGIN

class ScriptBinder;


class ScriptEventBind
{
	friend class EventDispatcher;

public:
	ScriptEventBind(ScriptBinder* pScriptBinder);

public:
	virtual void 
		BindScriptFunc(const AString& strFuncName);


protected:
	void 
		OnEvent(const Event* pEvent);


protected:
	ScriptBinder*
		_pScriptBinder;
	AString
		_strLuaFuncName;

};

DECLARE_MAP_TYPE_PTR(map, AString, ScriptEventBind);


class _CommonExport ScriptEventDelegator
{
	friend class EventDispatcher;

public:
	ScriptEventDelegator(ScriptBinder* pBinder);
	~ScriptEventDelegator();


public:
	void 
		SetScriptBinder(ScriptBinder* pBinder);

	INLINE ScriptEventBind*
		GetEventBind(const AString& strFuncName)
	{
		ScriptEventBindMapIt it = eventBindMap.find(strFuncName);
		if(it != eventBindMap.end()) return it->second;

		ScriptEventBind* pEventBind = new ScriptEventBind(_pScriptBinder);
		pEventBind->BindScriptFunc(strFuncName);
		eventBindMap.insert(make_pair(strFuncName, pEventBind));
		return pEventBind;
	}

protected:
	ScriptBinder*
		_pScriptBinder;

	ScriptEventBindMap
		eventBindMap;

};

_COMMON_NS_END