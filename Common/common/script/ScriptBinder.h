#pragma once

#include "lua_tinker.h"
#include "common/util/Unicode.h"

_COMMON_NS_BEGIN

class ScriptEventDelegator;

class _CommonExport ScriptBinder
{
	friend class ScriptEventBind;

public:
	ScriptBinder();
	virtual ~ScriptBinder();


public:
	template<class UserClass>
	void BindClass(const AString& strClassName)
	{
		XASSERT(__pLuaState);
		lua_tinker::class_add<UserClass>(__pLuaState, strClassName.c_str());	
	}

	template<class UserClass, typename Func>
	void BindMethod(const AString& strMethodName, Func func)
	{
		XASSERT(__pLuaState);
		lua_tinker::class_def<UserClass>(__pLuaState, strMethodName.c_str(), func);
	}

	template<class UserClass, typename PropertyType>
	void BindProperty(const AString& strPropertyName, PropertyType property)
	{
		XASSERT(__pLuaState);
		lua_tinker::class_mem<UserClass>(__pLuaState, strPropertyName.c_str(), property);
	}

	template<typename UserClass>
	void BindObject(const AString& strObjectName, UserClass pUserObject)
	{
		lua_tinker::set(__pLuaState, strObjectName.c_str(), pUserObject);
	}

	INLINE void 
		RunFile(const AString& strObjectName) { lua_tinker::dofile(__pLuaState, strObjectName.c_str()); }

	INLINE void 
		RunString(const AString& strObjectName) { lua_tinker::dostring(__pLuaState, strObjectName.c_str()); }

	// Methods for manipulate stack
	INLINE void 
		PushNumber(int nNumber)
	{
		lua_pushnumber(__pLuaState, nNumber);
	}

	INLINE void CallScriptFunc(const AString& strFileName, const AString& strFuncName)
	{
		USING_STRING_CONVERT;
		AString strLuaLibName = WStrToAStr(RESOURCE_PATH.c_str());
		strLuaLibName += "script\\";
		strLuaLibName += strFileName;
		RunFile(strLuaLibName);		

		//... Call function

	}


private:
	lua_State*
		__pLuaState;

};

_COMMON_NS_END