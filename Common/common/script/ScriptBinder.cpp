#include "stdafx.h"
#include "ScriptBinder.h"

_COMMON_NS_BEGIN

ScriptBinder::ScriptBinder()
{
	__pLuaState = lua_open();
	luaopen_base(__pLuaState);
	luaopen_string(__pLuaState);
}

ScriptBinder::~ScriptBinder()
{
	XASSERT(__pLuaState);
	lua_close(__pLuaState);
	__pLuaState = NULL;
}


_COMMON_NS_END