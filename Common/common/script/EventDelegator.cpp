#include "stdafx.h"
#include "EventDelegator.h"

#include "ScriptBinder.h"

_COMMON_NS_BEGIN

ScriptEventBind::ScriptEventBind(ScriptBinder* pScriptBinder)
{
	_pScriptBinder = pScriptBinder;
}

void ScriptEventBind::BindScriptFunc(const AString& strFuncName)
{
	_strLuaFuncName = strFuncName;
}

void ScriptEventBind::OnEvent(const Event* pEvent)
{
	// Push script method name
	lua_getglobal(_pScriptBinder->__pLuaState, _strLuaFuncName.c_str());

	int nEventArgumentCount = 0;
	nEventArgumentCount += const_cast<Event*>(pEvent)->_prepareScriptArguments(_pScriptBinder);

	// Call method
	lua_call(_pScriptBinder->__pLuaState, nEventArgumentCount, 0);

	return;
}


//--------------------------------
ScriptEventDelegator::~ScriptEventDelegator()
{
	ScriptEventBindMapIterator eventBindMapIterator(eventBindMap);

	while(eventBindMapIterator.HasMore())
	{
		XDELETE(eventBindMapIterator.CurrentData());
		eventBindMapIterator.Next();
	}
}

ScriptEventDelegator::ScriptEventDelegator(ScriptBinder* pBinder)
{
	_pScriptBinder = pBinder;
}

void ScriptEventDelegator::SetScriptBinder(ScriptBinder* pBinder)
{
	_pScriptBinder = pBinder;
}


_COMMON_NS_END