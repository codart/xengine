#include "stdafx.h"
#include "CommonModule.h"

#include "Common/debug/Tracer.h"
#include "Common/time/TimerMgr.h"
#include "Common/core/_SingletonMgr.h"
#include "Common/input/_InputManager.h"

_COMMON_NS_BEGIN

CommonModule::CommonModule()
{
	__commonLibState = 0;
}

CommonModule::~CommonModule()
{

}

void CommonModule::Init(const Event* pEvent)
{
	XASSERT(__commonLibState != -1);
	if(__commonLibState == 1) return;

	__commonLibState = 1;

	PlatformUtil::EnableWriteMiniDump(TRUE);

	_SingletonMgr::Init();
	_TimerMgr::Initialize();

	InputManager::Instance().Init(GetCore()->GetModuleHandle());

	Tracer::Instance().Create(NULL); 
}

void CommonModule::Tick(const Event* pEvent)
{
	InputManager& inputManager = InputManager::Instance();

	// Update input manager
	inputManager.Tick();
}

void CommonModule::Uninit(const Event* pEvent)
{
	XASSERT(__commonLibState != 0);
	if(__commonLibState == -1) return;

	__commonLibState = -1;

	Tracer::Instance().Destroy(); //..????

	_TimerMgr::Uninitialize();

	ModuleUtil::Uninit();
}

_COMMON_NS_END


