#pragma once

#include <malloc.h>

#include <map>
#include <vector>
#include <list>
#include <assert.h>
#include <string>
#include <hash_map>

using namespace std;


// Type defines

_COMMON_NS_BEGIN

#ifdef _UNICODE
	typedef std::wstring String;
#else
	typedef std::string String;
#endif


#if USE_DOUBLE_PRECISION_REAL == 1
	/** Software floating point type.
	@note Not valid as a pointer to GPU buffers / parameters
	*/
	typedef double REAL;	
	#define REAL_BIT_COUNT	64
	typedef uint64_t UINT_REAL; 
#else
	/** Software floating point type.
	@note Not valid as a pointer to GPU buffers / parameters
	*/
	//typedef float  Real;
	typedef float REAL;
	#define REAL_BIT_COUNT	32
	typedef uint32_t UINT_REAL;
#endif


_COMMON_NS_END

#include "common/debug/xassert.h"
#include "common/debug/SafeTypes.h"
#include "common/core/Object.h"
#include "common/util/RefCountPtr.h"
#include "common/util/SharedPtr.h"
#include "common/util/ConcretePtr.h"
#include "common/util/OwnerPtr.h"

#include "common/math/point.h"
#include "common/core/Exception.h"
#include "common/debug/SafeTypes.h"
#include "common/general/Any.h"
#include "common/general/AnyNumber.h"

// ------ constant values -----------
#include "common/debug/Tracer.h"

#include "common/File/ModuleUtil.h"
#include "common/util/VectorIterator.h"
#include "common/util/MapIterator.h"

#include "common/core/CommonExceptions.h"
#include "Common/colordefs.h"
#include "Common/time/Timer.h"
#include "common/core/Core.h"
#include "common/core/CoreEvent.h"
#include "common/CommonModule.h"

#include "common/general/Property.h"