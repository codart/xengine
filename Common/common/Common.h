#pragma once

#include "commonMacros.h"
#include "commonHeaders.h"

extern "C"
{
	_CommonExport void CreateCore();
	_CommonExport common::Core* GetCore();
	_CommonExport void DestroyCore();

	_CommonExport common::IModule* CreateCommonModule();
	_CommonExport common::CommonModule* GetCommonModule();
	_CommonExport void DestroyCommonModule();

	_CommonExport float GetTime();
	_CommonExport float GetAccurateTime();
	_CommonExport float GetDeltaTime();

	_CommonExport float TestMethod(int a);

}
