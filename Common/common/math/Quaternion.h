#pragma once

#include "common/math/Math.h"
#include "Common/math/Vector3.h"
#include "Common/math/Angle.h"

_COMMON_NS_BEGIN

class Matrix3;

class _CommonExport Quaternion
{
public:
	static const Quaternion ZERO;
	static const Quaternion IDENTITY;


public:
	INLINE Quaternion (REAL fW = 1.0, REAL fX = 0.0, REAL fY = 0.0, REAL fZ = 0.0)
	{
		w = fW;
		x = fX;
		y = fY;
		z = fZ; 
	}

	/// Construct a quaternion from an angle/axis
	INLINE Quaternion(const Radian& rfAngle, const Vector3& rkAxis) { FromAngleAxis(rfAngle, rkAxis); }


public:
	INLINE const Vector3&
		Vector() { return *(Vector3*)&x; }
	INLINE void
		SetVector(const Vector3& v) { *(Vector3*)&x = v; }

	INLINE void 
		FromAngleAxis (const Radian& rAngle, const Vector3& vRotateAxis);
	void 
		FromRotationMatrix (const Matrix4& vRotateAxis);
	void 
		ToRotationMatrix (Matrix3& vRotateAxis) const;

	INLINE void 
		FromTwoVector(const Vector3& v0, const Vector3 v1)
	{
		XASSERT(v0.IsNormalized(0.01f));
		XASSERT(v1.IsNormalized(0.01f));
		double dot = v0.DotProduct(v1) + 1.0f;
		if (dot > 0.0001) 
		{
			double vx=v1.y*v0.z-v1.z*v0.y; 
			double vy=v1.z*v0.x-v1.x*v0.z; 
			double vz=v1.x*v0.y-v1.y*v0.x; 
			double d = 1.0 / (double)Math::Sqrt((REAL)(dot*dot + vx*vx+vy*vy+vz*vz));
			w = REAL(dot * d);
			x = REAL(vx * d);
			y = REAL(vy * d);
			z = REAL(vz * d);
		}
		else
		{
			w=0;
			SetVector(v0.GetOrthogonal().GetNormalized());
		}
	}

	INLINE REAL 
		Normalise(void)
	{
		REAL len = _sqr();
		REAL factor = 1.0f / Math::Sqrt(len);
		*this = *this * factor;
		return len;
	}

	REAL 
		Dot(const Quaternion& rkQ) const
	{
		return w*rkQ.w+x*rkQ.x+y*rkQ.y+z*rkQ.z;
	}

	INLINE bool 
		IsUnit(REAL e = Math::EPSILON) const
	{
		return Math::Abs(1 - this->Dot(*this) ) < e;
	}

	INLINE Quaternion 
		Inverse () const
	{
		REAL fNorm = w*w+x*x+y*y+z*z;
		if ( fNorm > 0.0 )
		{
			REAL fInvNorm = 1.0f/fNorm;
			return Quaternion(w*fInvNorm, -x*fInvNorm, -y*fInvNorm, -z*fInvNorm);
		}
		else
		{
			// return an invalid result to flag the error
			return ZERO;
		}
	}

	Quaternion 
		UnitInverse() const
	{
		XASSERT(IsUnit());
		return Quaternion(w, -x, -y, -z);
	}

	INLINE Vector3 
		RotateVector(Vector3 vVector)
	{
		Vector3 uv, uuv;
		Vector3 qvec(x, y, z);
		uv = vVector.CrossProduct(qvec);
		uuv = uv.CrossProduct(qvec);
		uv *= (2.0f * w);
		uuv *= 2.0f;

		return vVector + uv + uuv;
	}

	// --- Operators ---
	INLINE Quaternion operator* (REAL fScalar) const
	{
		return Quaternion(fScalar*w, fScalar*x, fScalar*y, fScalar*z);
	}

	INLINE Quaternion operator* (const Quaternion& qQuaternion) const
	{
		// NOTE:  Multiplication is not generally commutative, so in most
		// cases p*q != q*p.

		return Quaternion
			(	w * qQuaternion.w - x * qQuaternion.x - y * qQuaternion.y - z * qQuaternion.z,
				w * qQuaternion.x + x * qQuaternion.w + y * qQuaternion.z - z * qQuaternion.y,
				w * qQuaternion.y + y * qQuaternion.w + z * qQuaternion.x - x * qQuaternion.z,
				w * qQuaternion.z + z * qQuaternion.w + x * qQuaternion.y - y * qQuaternion.x );
	}

	INLINE Quaternion& operator *= ( const Quaternion& qQuaternion)
	{
		REAL x_ = w * qQuaternion.x + x * qQuaternion.w + y * qQuaternion.z - z * qQuaternion.y;
		REAL y_ = w * qQuaternion.y + y * qQuaternion.w + z * qQuaternion.x - x * qQuaternion.z;
		REAL z_ = w * qQuaternion.z + z * qQuaternion.w + x * qQuaternion.y - y * qQuaternion.x;
		REAL w_ = w * qQuaternion.w - x * qQuaternion.x - y * qQuaternion.y - z * qQuaternion.z;

		x = x_;
		y = y_;
		z = z_;
		w = w_;
		return *this;
	}

	INLINE Quaternion& operator= (const Quaternion& qSource)
	{
		w = qSource.w;
		x = qSource.x;
		y = qSource.y;
		z = qSource.z;
		return *this;
	}


public:
	REAL x;
	REAL y;
	REAL z; 
	REAL w;

protected:
	REAL _sqr() const
	{
		return w*w+x*x+y*y+z*z;
	}

};


_COMMON_NS_END