#include "StdAfx.h"
#include "Matrix3.h"

#include "Vector3.h"
#include "Quaternion.h"

_COMMON_NS_BEGIN

const Matrix3 Matrix3::ZERO(
							0, 0, 0, 
							0, 0, 0, 
							0, 0, 0 );

const Matrix3 Matrix3::IDENTITY( 1, 0, 0,
								 0, 1, 0,
								 0, 0, 1 );


Matrix3::Matrix3(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis)
{
	m[0][0] = xAxis.x;
	m[0][1] = xAxis.y;
	m[0][2] = xAxis.z;

	m[1][0] = yAxis.x;
	m[1][1] = yAxis.y;
	m[1][2] = yAxis.z;

	m[2][0] = zAxis.x;
	m[2][1] = zAxis.y;
	m[2][2] = zAxis.z;
}

BOOL Matrix3::Inverse (Matrix3& rkInverse, Real fTolerance) const
{
	// Invert a 3x3 using cofactors.  This is about 8 times faster than
	// the Numerical Recipes code which uses Gaussian elimination.

	rkInverse[0][0] = m[1][1]*m[2][2] -
		m[1][2]*m[2][1];
	rkInverse[0][1] = m[0][2]*m[2][1] -
		m[0][1]*m[2][2];
	rkInverse[0][2] = m[0][1]*m[1][2] -
		m[0][2]*m[1][1];
	rkInverse[1][0] = m[1][2]*m[2][0] -
		m[1][0]*m[2][2];
	rkInverse[1][1] = m[0][0]*m[2][2] -
		m[0][2]*m[2][0];
	rkInverse[1][2] = m[0][2]*m[1][0] -
		m[0][0]*m[1][2];
	rkInverse[2][0] = m[1][0]*m[2][1] -
		m[1][1]*m[2][0];
	rkInverse[2][1] = m[0][1]*m[2][0] -
		m[0][0]*m[2][1];
	rkInverse[2][2] = m[0][0]*m[1][1] -
		m[0][1]*m[1][0];

	Real fDet =
		m[0][0]*rkInverse[0][0] +
		m[0][1]*rkInverse[1][0]+
		m[0][2]*rkInverse[2][0];

	if ( Math::Abs(fDet) <= fTolerance )
		return false;

	Real fInvDet = (REAL)1.0/fDet;
	for (size_t iRow = 0; iRow < 3; iRow++)
	{
		for (size_t iCol = 0; iCol < 3; iCol++)
			rkInverse[iRow][iCol] *= fInvDet;
	}

	return true;
}

BOOL Matrix3::IsOrthogonal(REAL fTolerance) const
{
	Matrix3 matMultiTranspose = (*this) * this->Transpose();

	if(Math::Abs(matMultiTranspose._11 - 1) < fTolerance
		&& Math::Abs(matMultiTranspose._22 - 1) < fTolerance
		&& Math::Abs(matMultiTranspose._33 - 1) < fTolerance )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void Matrix3::MakeTransform(const Quaternion& orientation)
{
	orientation.ToRotationMatrix(*this);
}

void Matrix3::MakeTransform(const Vector3& scale, const Quaternion& orientation)
{
	*this = IDENTITY;
	// Ordering:
	//    1. Scale
	//    2. Rotate

	Matrix3 rot3x3;
	orientation.ToRotationMatrix(rot3x3);

	// Set up final matrix with scale, rotation and translation
	m[0][0] = scale.x * rot3x3[0][0]; m[1][0] = scale.y * rot3x3[0][1]; m[2][0] = scale.z * rot3x3[0][2]; 
	m[0][1] = scale.x * rot3x3[1][0]; m[1][1] = scale.y * rot3x3[1][1]; m[2][1] = scale.z * rot3x3[1][2]; 
	m[0][2] = scale.x * rot3x3[2][0]; m[1][2] = scale.y * rot3x3[2][1]; m[2][2] = scale.z * rot3x3[2][2];
}

void Matrix3::MakeTransform(const Vector3& scale)
{
	*this = IDENTITY;

	this->_11 = scale.x;
	this->_22 = scale.y;
	this->_33 = scale.z;
}

Vector3 Matrix3::TransformVector(const Vector3& vNormal)
{
	Vector3 vResult;
	vResult.x = ( m[0][0] * vNormal.x + m[1][0] * vNormal.y + m[2][0] * vNormal.z);
	vResult.y = ( m[0][1] * vNormal.x + m[1][1] * vNormal.y + m[2][1] * vNormal.z);
	vResult.z = ( m[0][2] * vNormal.x + m[1][2] * vNormal.y + m[2][2] * vNormal.z);

	return vResult;
}

void Matrix3::FromAxisAngle (const Vector3& rkAxis, const Radian& fRadians)
{
	REAL fCos = Math::Cos(fRadians);
	REAL fSin = Math::Sin(fRadians);
	REAL fOneMinusCos = (REAL)1.0f-fCos;
	REAL fX2 = rkAxis.x*rkAxis.x;
	REAL fY2 = rkAxis.y*rkAxis.y;
	REAL fZ2 = rkAxis.z*rkAxis.z;
	REAL fXYM = rkAxis.x*rkAxis.y*fOneMinusCos;
	REAL fXZM = rkAxis.x*rkAxis.z*fOneMinusCos;
	REAL fYZM = rkAxis.y*rkAxis.z*fOneMinusCos;
	REAL fXSin = rkAxis.x*fSin;
	REAL fYSin = rkAxis.y*fSin;
	REAL fZSin = rkAxis.z*fSin;

	m[0][0] = fX2*fOneMinusCos+fCos;
	m[1][0] = fXYM-fZSin;
	m[2][0] = fXZM+fYSin;
	m[0][1] = fXYM+fZSin;
	m[1][1] = fY2*fOneMinusCos+fCos;
	m[2][1] = fYZM-fXSin;
	m[0][2] = fXZM-fYSin;
	m[1][2] = fYZM+fXSin;
	m[2][2] = fZ2*fOneMinusCos+fCos;
}

_COMMON_NS_END

