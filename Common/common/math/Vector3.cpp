#include "stdafx.h"
#include "Vector3.h"

#include "Quaternion.h"

_COMMON_NS_BEGIN

const Vector3 Vector3::ZERO( 0, 0, 0 );

const Vector3 Vector3::UNIT_X( 1, 0, 0 );
const Vector3 Vector3::UNIT_Y( 0, 1, 0 );
const Vector3 Vector3::UNIT_Z( 0, 0, 1 );
const Vector3 Vector3::NEGATIVE_UNIT_X( -1,  0,  0 );
const Vector3 Vector3::NEGATIVE_UNIT_Y(  0, -1,  0 );
const Vector3 Vector3::NEGATIVE_UNIT_Z(  0,  0, -1 );
const Vector3 Vector3::UNIT_SCALE(1, 1, 1);

Vector3 Vector3::operator* ( const Quaternion& qQuaternion ) const
{	
	Vector3 uv, uuv;
	Vector3 qvec(qQuaternion.x, qQuaternion.y, qQuaternion.z);

	uv = CrossProduct(qvec);
	uuv = uv.CrossProduct(qvec);
	uv *= (2.0f * qQuaternion.w);
	uuv *= 2.0f;

	return *this + uv + uuv;

/*
	// Another algorithm. But it seem there use a right hand coordinate system, and 
	// the calculated result has an invert rotate direction.

	Vector3& vThis = *this;
	Vector3 qv(qQuaternion.x, qQuaternion.y, qQuaternion.z);
	Vector3 vOut = 2.f * qQuaternion.w * (qv ^ vThis);
	vOut += ((qQuaternion.w * qQuaternion.w) - (qv | qv)) * vThis;
	vOut += (2.f * (qv | vThis)) * qv;

	return vOut;
*/
}


_COMMON_NS_END