#pragma once

#include "Vector3.h"
#include "Vector4.h"
#include "AxisAlignedBox.h"

_COMMON_NS_BEGIN

/** Defines a plane in 3D space.
    @remarks
        A plane is defined in 3D space by the equation
        Ax + By + Cz + D = 0
    @par
        This equates to a vector (the normal of the plane, whose x, y
        and z components equate to the coefficients A, B and C
        respectively), and a constant (D) which is the distance along
        the normal you have to go to move the plane back to the origin.
 */
class _CommonExport Plane
{
public:
	Vector3 normal;
	REAL distance;

public:
	/** Default constructor - sets everything to 0.
	*/
	Plane ();
	Plane (const Plane& rhs);
	/** Construct a plane through a normal, and a distance to move the plane along the normal.*/
	Plane (const Vector3& rkNormal, REAL fConstant);
	/** Construct a plane using the 4 constants directly **/
	Plane (REAL a, REAL b, REAL c, REAL d);
	Plane (const Vector3& rkNormal, const Vector3& rkPoint);
	Plane (const Vector3& rkPoint0, const Vector3& rkPoint1,
		const Vector3& rkPoint2);

	/** The "positive side" of the plane is the half space to which the
	plane normal points. The "negative side" is the other half
	space. The flag "no side" indicates the plane itself.
	*/
	enum Side
	{
		NO_SIDE,
		POSITIVE_SIDE,
		NEGATIVE_SIDE,
		BOTH_SIDE
	};

	Side GetSide (const Vector3& rkPoint) const;

	/**
	returns the side where the aligneBox is. the flag BOTH_SIDE indicates an intersecting box.
	one corner ON the plane is sufficient to consider the box and the plane intersecting.
	*/
	Side GetSide (const AxisAlignedBox& rkBox) const;

	/** Returns which side of the plane that the given box lies on.
	The box is defined as centre/half-size pairs for effectively.
	@param centre The centre of the box.
	@param halfSize The half-size of the box.
	@returns
	POSITIVE_SIDE if the box complete lies on the "positive side" of the plane,
	NEGATIVE_SIDE if the box complete lies on the "negative side" of the plane,
	and BOTH_SIDE if the box intersects the plane.
	*/
	Side GetSide (const Vector3& centre, const Vector3& halfSize) const;

	/** This is a pseudodistance. The sign of the return value is
	positive if the point is on the positive side of the plane,
	negative if the point is on the negative side, and zero if the
	point is on the plane.
	@par
	The absolute value of the return value is the true distance only
	when the plane normal is a unit length vector.
	*/
	REAL GetDistance (const Vector3& rkPoint) const;

	/** Redefine this plane based on 3 points. */
	void Redefine(const Vector3& rkPoint0, const Vector3& rkPoint1,
		const Vector3& rkPoint2);

	/** Redefine this plane based on a normal and a point. */
	void Redefine(const Vector3& rkNormal, const Vector3& rkPoint);

	/** Project a vector onto the plane. 
	@remarks This gives you the element of the input vector that is perpendicular 
	to the normal of the plane. You can get the element which is parallel
	to the normal of the plane by subtracting the result of this method
	from the original vector, since parallel + perpendicular = original.
	@param v The input vector
	*/
	Vector3 ProjectVector(const Vector3& v) const;

	/** Normalises the plane.
	@remarks
	This method normalises the plane's normal and the length scale of distance
	is as well.
	@note
	This function will not crash for zero-sized vectors, but there
	will be no changes made to their components.
	@returns The previous length of the plane's normal.
	*/
	REAL Normalise(void);

	/// Comparison operator
	bool operator==(const Plane& rhs) const
	{
		return (rhs.distance == distance && rhs.normal == normal);
	}
	bool operator!=(const Plane& rhs) const
	{
		return (rhs.distance != distance && rhs.normal != normal);
	}

/*
	INLINE Plane Plane::TransformBy( const Matrix4& M ) const
	{
		Matrix4 tmpTA = M.TransposeAdjoint();
		float DetM = M.Determinant();
		return this->TransformByUsingAdjointT(M, DetM, tmpTA);
	}
*/

	INLINE Plane operator * (const Matrix4& matrix) const
	{
		Plane ret;

		Matrix4 invTrans = matrix.Inverse().Transpose();
		Vector4 v4(normal.x, normal.y, normal.z, distance);
		v4 = v4 * invTrans;
		ret.normal.x = v4.x; 
		ret.normal.y = v4.y; 
		ret.normal.z = v4.z;
		ret.distance = v4.w / ret.normal.Normalise();
		return ret;
	}

	_CommonExport friend std::ostream& operator<< (std::ostream& o, const Plane& p);
};


_COMMON_NS_END