#include "stdafx.h"
#include "Vector4.h"

#include "Vector3.h"

_COMMON_NS_BEGIN

const Vector4 Vector4::ZERO( 0, 0, 0, 0 );

Vector4::Vector4(const Vector3& rhs)
: x(rhs.x), y(rhs.y), z(rhs.z), w(1.0f)
{

}

Vector4& Vector4::operator = (const Vector3& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	w = 1.0f;
	return *this;
}


_COMMON_NS_END

