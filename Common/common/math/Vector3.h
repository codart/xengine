#pragma once

#include "d3dx9math.h"
#include "math.h"
#include "matrix.h"

_COMMON_NS_BEGIN

class Quaternion;

no_v_base class _CommonExport Vector3
{
public:
	static const Vector3 ZERO;
	static const Vector3 UNIT_X;
	static const Vector3 UNIT_Y;
	static const Vector3 UNIT_Z;
	static const Vector3 NEGATIVE_UNIT_X;
	static const Vector3 NEGATIVE_UNIT_Y;
	static const Vector3 NEGATIVE_UNIT_Z;
	static const Vector3 UNIT_SCALE;


public:
	REAL x, y, z;


public:
	INLINE Vector3(){};

	INLINE Vector3( REAL x, REAL y, REAL z )
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	/// Check whether this vector contains valid values
	INLINE BOOL IsNaN() const
	{
		return Math::IsNaN(x) || Math::IsNaN(y) || Math::IsNaN(z);
	}

	INLINE void SetVector( REAL x, REAL y, REAL z )
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	INLINE BOOL IsNormalized(REAL epsilon = Math::EPSILON) const
	{
		REAL fLength = Math::Sqrt( x * x + y * y + z * z );
		if(Math::Abs(fLength - 1) < Math::EPSILON)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	INLINE REAL Normalise()
	{
		REAL fLength = Math::Sqrt( x * x + y * y + z * z );

		// Will also work for zero-sized vectors, but will change nothing
		if ( fLength > 1e-08 )
		{
			REAL fInvLength = REAL(1.0f) / fLength;
			x *= fInvLength;
			y *= fInvLength;
			z *= fInvLength;
		}
		return fLength;
	}

	INLINE Vector3 GetNormalized()
	{
		Vector3 v = *this;
		v.Normalise();
		return v;
	}

	INLINE REAL GetLengthSquared() const
	{
		return x*x + y*y + z*z;
	}

	INLINE Vector3 GetOrthogonal() const
	{
		int i = Math::IsNegetive(Math::Square((float)0.9) * GetLengthSquared()-x*x);
		Vector3 result;
		result[i] = 0; 
		result[Math::LoopIncN<3>(i)] = (*this)[Math::LoopDecN<3>(i)];
		result[Math::LoopDecN<3>(i)] = -(*this)[Math::LoopIncN<3>(i)];
		return result;
	}

	INLINE REAL &operator [] (int32_t index)
	{ 
		XASSERT(index >= 0 && index <= 2); 
		return ((REAL*)this)[index]; 
	}

	INLINE REAL operator [] (int32_t index) const 
	{
		XASSERT(index>=0 && index<=2); 
		return ((float*)this)[index]; 
	}


    /** Returns the Length (magnitude) of the vector.
        @warning
            This operation requires a square root and is expensive in
            terms of CPU operations. If you don't need to know the exact
            Length (e.g. for just comparing lengths) use squaredLength()
            instead.
    */
    INLINE REAL Length () const
    {
        return Math::Sqrt( x * x + y * y + z * z );
    }

	INLINE Vector3 CrossProduct( const Vector3& vVector ) const
	{
		return Vector3(
			y * vVector.z - z * vVector.y,
			z * vVector.x - x * vVector.z,
			x * vVector.y - y * vVector.x);
	}

	INLINE Vector3 operator - ( const Vector3& vVector ) const
	{
		return Vector3(
			x - vVector.x,
			y - vVector.y,
			z - vVector.z);
	}

	INLINE Vector3 operator - () const
	{
		return Vector3(-x, -y, -z);
	}

	// Same as crossProduct
	INLINE Vector3 operator^( const Vector3& vVector ) const
	{
		return CrossProduct(vVector);
	}

	INLINE Vector3& operator += ( const Vector3& rkVector )
	{
		x += rkVector.x;
		y += rkVector.y;
		z += rkVector.z;

		return *this;
	}

	INLINE Vector3 operator / (const float fValue) const
	{
		return Vector3(x / (REAL)fValue, y / (REAL)fValue, z / (REAL)fValue);
	}

	INLINE Vector3 operator / ( const Vector3& rhs) const
	{
		return Vector3(
			x / rhs.x,
			y / rhs.y,
			z / rhs.z);
	}

	INLINE Vector3 operator * (const REAL fValue) const
	{
		return Vector3(x * (REAL)fValue, y * (REAL)fValue, z * (REAL)fValue);
	}

	INLINE Vector3 operator * (const Vector3& vVector) const
	{
		return Vector3(
			x * vVector.x,
			y * vVector.y,
			z * vVector.z);
	}

	/** Vector transformation using '*'.
		@remarks
			Transforms the given 3-D vector by the matrix, projecting the 
			result back into <i>w</i> = 1.
		@note
			This means that the initial <i>w</i> is considered to be 1.0,
			and then all the tree elements of the resulting 3-D vector are
			divided by the resulting <i>w</i>.
	*/
	INLINE Vector3 operator * (const Matrix4& vMatrix) const
	{
		Vector3 vResult;

		REAL fInvertW = (REAL)1.0f / ( vMatrix[0][3] * x + vMatrix[1][3] * y + vMatrix[2][3] * z + vMatrix[3][3] );

		vResult.x = ( vMatrix[0][0] * x + vMatrix[1][0] * y + vMatrix[2][0] * z + vMatrix[3][0] ) * fInvertW;
		vResult.y = ( vMatrix[0][1] * x + vMatrix[1][1] * y + vMatrix[2][1] * z + vMatrix[3][1] ) * fInvertW;
		vResult.z = ( vMatrix[0][2] * x + vMatrix[1][2] * y + vMatrix[2][2] * z + vMatrix[3][2] ) * fInvertW;

		return vResult;
	}

	/** Vector transformation using '*='.
		@remarks
			Transforms the given 3-D vector by the matrix, projecting the 
			result back into <i>w</i> = 1.
		@note
			This means that the initial <i>w</i> is considered to be 1.0,
			and then all the tree elements of the resulting 3-D vector are
			divided by the resulting <i>w</i>.
	*/
	INLINE Vector3& operator *= (const Matrix4& vMatrix)
	{
		Vector3 vTmp(x, y, z);

		REAL fInvertW = 1.0f / ( vMatrix[0][3] * x + vMatrix[1][3] * y + vMatrix[2][3] * z + vMatrix[3][3] );

		x = ( vMatrix[0][0] * vTmp.x + vMatrix[1][0] * vTmp.y + vMatrix[2][0] * vTmp.z + vMatrix[3][0] ) * fInvertW;
		y = ( vMatrix[0][1] * vTmp.x + vMatrix[1][1] * vTmp.y + vMatrix[2][1] * vTmp.z + vMatrix[3][1] ) * fInvertW;
		z = ( vMatrix[0][2] * vTmp.x + vMatrix[1][2] * vTmp.y + vMatrix[2][2] * vTmp.z + vMatrix[3][2] ) * fInvertW;
		return *this;
	}

	INLINE Vector3& operator *= ( const REAL fScalar )
	{
		x *= fScalar;
		y *= fScalar;
		z *= fScalar;
		return *this;
	}

	INLINE Vector3 operator + ( const Vector3& vVector ) const
	{
		return Vector3(
			x + vVector.x,
			y + vVector.y,
			z + vVector.z);
	}

	INLINE REAL operator| ( const Vector3& vVector ) const
	{
		return x*vVector.x + y*vVector.y + z*vVector.z;
	}

	INLINE bool operator == ( const Vector3& rkVector ) const
	{
		return ( x == rkVector.x && y == rkVector.y && z == rkVector.z );
	}

	INLINE bool operator != ( const Vector3& rkVector ) const
	{
		return ( x != rkVector.x || y != rkVector.y || z != rkVector.z );
	}

	// Rotate a vector by quaternion
	Vector3 operator* ( const Quaternion& qQuaternion ) const;

	// overloaded operators to help Vector3
	INLINE friend Vector3 operator * ( const REAL fScalar, const Vector3& vVector )
	{
		return Vector3(
			fScalar * vVector.x,
			fScalar * vVector.y,
			fScalar * vVector.z);
	}

    /** Returns true if the vector's scalar components are all smaller
        that the ones of the vector it is compared against.
    */
    INLINE bool operator > ( const Vector3& rhs ) const
    {
        if( x > rhs.x && y > rhs.y && z > rhs.z )
            return true;
        return false;
    }

	/** Returns true if the vector's scalar components are all greater
	that the ones of the vector it is compared against.
	*/
	INLINE bool operator < ( const Vector3& rhs ) const
	{
		if( x < rhs.x && y < rhs.y && z < rhs.z )
			return true;
		return false;
	}

    /** Sets this vector's components to the minimum of its own and the
        ones of the passed in vector.
        @remarks
            'Minimum' in this case means the combination of the lowest
            value of x, y and z from both vectors. Lowest is taken just
            numerically, not magnitude, so -1 < 0.
    */
    INLINE void MakeFloor( const Vector3& cmp )
    {
        if( cmp.x < x ) x = cmp.x;
        if( cmp.y < y ) y = cmp.y;
        if( cmp.z < z ) z = cmp.z;
    }

    /** Sets this vector's components to the maximum of its own and the
        ones of the passed in vector.
        @remarks
            'Maximum' in this case means the combination of the highest
            value of x, y and z from both vectors. Highest is taken just
            numerically, not magnitude, so 1 > -3.
    */
    INLINE void MakeCeil( const Vector3& cmp )
    {
        if( cmp.x > x ) x = cmp.x;
        if( cmp.y > y ) y = cmp.y;
        if( cmp.z > z ) z = cmp.z;
    }

    /** Calculates the dot (scalar) product of this vector with another.
        @remarks
            The dot product can be used to calculate the angle between 2
            vectors. If both are unit vectors, the dot product is the
            cosine of the angle; otherwise the dot product must be
            divided by the product of the lengths of both vectors to get
            the cosine of the angle. This result can further be used to
            calculate the distance of a point from a plane.
        @param
            vec Vector with which to calculate the dot product (together
            with this one).
        @returns
            A float representing the dot product value.
    */
    INLINE REAL DotProduct(const Vector3& vec) const
    {
        return x * vec.x + y * vec.y + z * vec.z;
    }

   /** Calculates the absolute dot (scalar) product of this vector with another.
        @remarks
            This function work similar dotProduct, except it use absolute value
            of each component of the vector to computing.
        @param
            vec Vector with which to calculate the absolute dot product (together
            with this one).
        @returns
            A REAL representing the absolute dot product value.
    */
    INLINE REAL AbsDotProduct(const Vector3& vec) const
    {
        return Math::Abs(x * vec.x) + Math::Abs(y * vec.y) + Math::Abs(z * vec.z);
    }

	/** Returns a vector at a point half way between this and the passed
		in vector.
	*/
	INLINE Vector3 MidPoint( const Vector3& vec ) const
	{
		return Vector3(
			( x + vec.x ) * 0.5f,
			( y + vec.y ) * 0.5f,
			( z + vec.z ) * 0.5f );
	}

	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Vector3& vector )
	{
		o << "Vector3(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
		return o;
	}

};

_COMMON_NS_END