#include "StdAfx.h"
#include "Math.h"
#include <limits>
#include "matrix.h"
#include "Plane.h"

_COMMON_NS_BEGIN

/*
	The smallest float number:	b ^ emin
	The biggest float number:	b ^ emax * ( b - b ^ (1 - p) ) 

	For 32bit float: b = 2, p = 24, emin = 126, emax = 127

	Reference: 
		http://en.wikipedia.org/wiki/Normal_number_(computing)
		http://libg.org/2008/09/26/floating-point-bit-value-of-inf-nan-den/
*/
const REAL Math::POS_INFINITY = std::numeric_limits<REAL>::infinity();
const REAL Math::NEG_INFINITY = -std::numeric_limits<REAL>::infinity();
const REAL Math::PI = REAL( 4.0 * atan( 1.0 ) );
const REAL Math::TWO_PI = REAL( 2.0 * PI );
const REAL Math::HALF_PI = REAL( 0.5 * PI );
const REAL Math::fDeg2Rad = PI / REAL(180.0);
const REAL Math::fRad2Deg = REAL(180.0) / PI;
const REAL Math::LOG2 = log(REAL(2.0));
const REAL Math::EPSILON = REAL(0.01f);

const char Math::s_numberChars[] = {'+','-', 'e', '.', VK_BACK, VK_RETURN, VK_DELETE, ' '		\
									, 'a', 'A', 'B', 'B', 'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F'}; 
const int Math::s_numberCharsCount = sizeof(s_numberChars)/sizeof(char);

uint32_t Math::s_nRandomSeed = 0;

Math::Math(void)
{
}

Math::~Math(void)
{
}


Matrix4 Math::BuildReflectionMatrix(const Plane& P)
{
	return Matrix4(
		-2.0f * P.normal.x * P.normal.x + 1.0f, -2.0f * P.normal.y * P.normal.x, -2.0f * P.normal.z * P.normal.x, 0,
		-2.0f * P.normal.x * P.normal.y, -2.0f * P.normal.y * P.normal.y + 1.0f, -2.0f * P.normal.z * P.normal.y, 0,
		-2.0f * P.normal.x * P.normal.z, -2.0f * P.normal.y * P.normal.z, -2.0f * P.normal.z * P.normal.z + 1.0f, 0,
		-2.0f * P.normal.x * P.distance, -2.0f * P.normal.y * P.distance, -2.0f * P.normal.z * P.distance, 1.0f );
}


_COMMON_NS_END