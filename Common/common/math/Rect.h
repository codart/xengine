#pragma once

_COMMON_NS_BEGIN

template<typename T>
class RectT
{
public:
	RectT()
	{
		left = 0;
		top = 0;
		right = 0;
		bottom = 0;
	}

	RectT(T left, T top, T right, T bottom)
	{
		SetRect(left, top, right, bottom);
	}

	RectT(const RectT<T>& rect)
	{
		left = rect.left;
		top = rect.top;
		right = rect.right;
		bottom = rect.bottom;
	}

	~RectT(void){}

#ifdef _WINDOWS_
	operator LPRECT ()
	{
		return NoImplementationException;
	}
#endif

	INLINE T Width() const
	{
		return right - left;
	}

	INLINE T Height() const
	{
		return bottom - top;
	}

	INLINE void SetRectEmpty()
	{
		left = right = top = bottom = 0;
	}

	INLINE void SetRect(T left, T top, T right, T bottom)
	{
		this->left = left;
		this->top = top;
		this->right = right;
		this->bottom = bottom;
	}

	INLINE void OffsetRect(T x, T y)
	{
		left += x;
		right += x;
		top += y;
		bottom += y;
	}

	INLINE bool IsRectEmpty() const
	{
		return !(left || right || top || bottom);
	}

	INLINE void UnionRect( const RectT<T>* lprcSrc1,  // first rectangle
							const RectT<T>* lprcSrc2   // second rectangle 
							)
	{
		if(lprcSrc1->IsRectEmpty())
		{
			*this =  *lprcSrc2;
		}
		else if(lprcSrc2->IsRectEmpty())
		{
			*this =  *lprcSrc1;
		}
		else
		{
			left = lprcSrc1->left < lprcSrc2->left? lprcSrc1->left : lprcSrc2->left;
			right = lprcSrc1->right > lprcSrc2->right? lprcSrc1->right : lprcSrc2->right;
			top = lprcSrc1->top < lprcSrc2->top? lprcSrc1->top : lprcSrc2->top;
			bottom = lprcSrc1->bottom > lprcSrc2->bottom? lprcSrc1->bottom : lprcSrc2->bottom;
		}
	}


	INLINE BOOL IntersectRect(const RectT<T>* lprcSrc1,  // first rectangle
								const RectT<T>* lprcSrc2   // second rectangle 
								)
	{
		left = lprcSrc1->left > lprcSrc2->left? lprcSrc1->left : lprcSrc2->left;
		right = lprcSrc1->right < lprcSrc2->right? lprcSrc1->right : lprcSrc2->right;
		top = lprcSrc1->top > lprcSrc2->top? lprcSrc1->top : lprcSrc2->top;
		bottom = lprcSrc1->bottom < lprcSrc2->bottom? lprcSrc1->bottom : lprcSrc2->bottom;

		if(top > bottom || left > right)
		{
			left = 0;
			top = 0;
			right = 0;
			bottom = 0;
			return FALSE;
		}
		else return TRUE;
	}

	INLINE BOOL PtInRect(const PointT<T>* pt) const
	{
		return (pt->x > left && pt->x < right
			&& pt->y > top && pt->y < bottom)? TRUE:FALSE;
	}

	INLINE BOOL PtInRect(T x, T y) const
	{
		return (x > left && x < right
			&& y > top && y < bottom)? TRUE:FALSE;
	}


public:
	T	left;
	T	top;
	T	right;
	T	bottom;

};


#ifdef _WINDOWS_
template<> 
RectT<int>::operator LPRECT()
{
	return (LPRECT)this;
}
#endif

class _CommonExport Rect : public RectT<int>
{
public:
	Rect(){}
	Rect(int left, int top, int right, int bottom) : RectT<int>(left, top
															, right, bottom) 
	{}

};

typedef RectT<float> RectF;

_COMMON_NS_END