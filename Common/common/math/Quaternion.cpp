#include "StdAfx.h"
#include "Quaternion.h"
#include "matrix3.h"

_COMMON_NS_BEGIN

const Quaternion Quaternion::ZERO(0.0, 0.0, 0.0, 0.0);
const Quaternion Quaternion::IDENTITY(1.0, 0.0, 0.0, 0.0);

void Quaternion::FromAngleAxis (const Radian& rfAngle,
								const Vector3& rotateAxis)
{
	// assert:  axis[] is unit length
	//
	// The quaternion representing the rotation is
	//   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

	Radian fHalfAngle ( rfAngle * 0.5 );
	REAL fSin = Math::Sin(fHalfAngle);
	w = Math::Cos(fHalfAngle);
	x = fSin * rotateAxis.x;
	y = fSin * rotateAxis.y;
	z = fSin * rotateAxis.z;
}

void Quaternion::FromRotationMatrix (const Matrix4& vRotateAxis)
{
	// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
	// article "Quaternion Calculus and Fast Animation".

	REAL fTrace = vRotateAxis[0][0] + vRotateAxis[1][1] + vRotateAxis[2][2];
	REAL fRoot;

	if ( fTrace > 0.0 )
	{
		// |w| > 1/2, may as well choose w > 1/2
		fRoot = Math::Sqrt(fTrace + 1.0f);  // 2w
		w = 0.5f*fRoot;
		fRoot = 0.5f/fRoot;  // 1/(4w)
		x = (vRotateAxis[1][2]-vRotateAxis[2][1])*fRoot;
		y = (vRotateAxis[2][0]-vRotateAxis[0][2])*fRoot;
		z = (vRotateAxis[0][1]-vRotateAxis[1][0])*fRoot;
	}
	else
	{
		// |w| <= 1/2
		static size_t s_iNext[3] = { 1, 2, 0 };
		size_t i = 0;
		if ( vRotateAxis[1][1] > vRotateAxis[0][0] )
			i = 1;
		if ( vRotateAxis[2][2] > vRotateAxis[i][i] )
			i = 2;
		size_t j = s_iNext[i];
		size_t k = s_iNext[j];

		fRoot = Math::Sqrt(vRotateAxis[i][i]-vRotateAxis[j][j]-vRotateAxis[k][k] + 1.0f);
		REAL* apkQuat[3] = { &x, &y, &z };
		*apkQuat[i] = 0.5f*fRoot;
		fRoot = 0.5f/fRoot;
		w = (vRotateAxis[j][k]-vRotateAxis[k][j])*fRoot;
		*apkQuat[j] = (vRotateAxis[i][j]+vRotateAxis[j][i])*fRoot;
		*apkQuat[k] = (vRotateAxis[i][k]+vRotateAxis[k][i])*fRoot;
	}
}

void Quaternion::ToRotationMatrix (Matrix3& vRotateAxis) const
{
	REAL fTx  = x+x;
	REAL fTy  = y+y;
	REAL fTz  = z+z;
	REAL fTwx = fTx*w;
	REAL fTwy = fTy*w;
	REAL fTwz = fTz*w;
	REAL fTxx = fTx*x;
	REAL fTxy = fTy*x;
	REAL fTxz = fTz*x;
	REAL fTyy = fTy*y;
	REAL fTyz = fTz*y;
	REAL fTzz = fTz*z;

	vRotateAxis[0][0] = 1.0f-(fTyy+fTzz);
	vRotateAxis[1][0] = fTxy-fTwz;
	vRotateAxis[2][0] = fTxz+fTwy;
	vRotateAxis[0][1] = fTxy+fTwz;
	vRotateAxis[1][1] = 1.0f-(fTxx+fTzz);
	vRotateAxis[2][1] = fTyz-fTwx;
	vRotateAxis[0][2] = fTxz-fTwy;
	vRotateAxis[1][2] = fTyz+fTwx;
	vRotateAxis[2][2] = 1.0f-(fTxx+fTyy);
}

_COMMON_NS_END