#pragma once

_COMMON_NS_BEGIN

class Vector3;
class Quaternion;
class Radian;

class _CommonExport Matrix3
{
public:
	static const Matrix3 IDENTITY;
	static const Matrix3 ZERO;

public:
	union {
		struct 
		{
			REAL        _11, _12, _13;
			REAL        _21, _22, _23;
			REAL        _31, _32, _33;
		};
		REAL m[3][3];
		REAL _m[9];
	};

	Matrix3(){};

	INLINE Matrix3(
		float m00, float m01, float m02, 
		float m10, float m11, float m12, 
		float m20, float m21, float m22  )
	{
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;
		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;
		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
	}

	INLINE Matrix3(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis);

	INLINE Matrix3(CONST Matrix3& matrix)
	{
		memcpy(&_m[0], &matrix, sizeof(Matrix3));
	}

	INLINE Matrix3& operator = (CONST Matrix3& matrix)
	{
		memcpy(&_m[0], &matrix, sizeof(Matrix3));
		return *this;
	}

	INLINE Matrix3& operator *= (CONST Matrix3& mat)
	{
		*this = (*this) * mat;
		return *this;
	}

	INLINE Matrix3 operator* (const Matrix3& rkMatrix) const
	{
		Matrix3 kProd;
		for (size_t iRow = 0; iRow < 3; iRow++)
		{
			for (size_t iCol = 0; iCol < 3; iCol++)
			{
				kProd.m[iRow][iCol] =
					m[iRow][0]*rkMatrix.m[0][iCol] +
					m[iRow][1]*rkMatrix.m[1][iCol] +
					m[iRow][2]*rkMatrix.m[2][iCol];
			}
		}
		return kProd;
	}

	INLINE void Identify()
	{
		*this = IDENTITY;
	}

	REAL Determinant () const
	{
		REAL fCofactor00 = m[1][1]*m[2][2] -
			m[2][1]*m[1][2];
		REAL fCofactor10 = m[2][1]*m[0][2] -
			m[0][1]*m[2][2];
		REAL fCofactor20 = m[0][1]*m[1][2] -
			m[1][1]*m[0][2];

		REAL fDet =
			m[0][0]*fCofactor00 +
			m[1][0]*fCofactor10 +
			m[2][0]*fCofactor20;

		return fDet;
	}

	INLINE Matrix3 Transpose() const
	{
		Matrix3	Result;

		Result.m[0][0] = m[0][0];
		Result.m[0][1] = m[1][0];
		Result.m[0][2] = m[2][0];

		Result.m[1][0] = m[0][1];
		Result.m[1][1] = m[1][1];
		Result.m[1][2] = m[2][1];

		Result.m[2][0] = m[0][2];
		Result.m[2][1] = m[1][2];
		Result.m[2][2] = m[2][2];

		return Result;
	}

	BOOL 
		Inverse (Matrix3& rkInverse, Real fTolerance) const;

	INLINE Matrix3 
		Inverse (REAL fTolerance = 1e-06) const
	{
		Matrix3 kInverse = Matrix3::ZERO;
		Inverse(kInverse,fTolerance);
		return kInverse;
	}

	INLINE Matrix3 
		InverseOrthogonal() const
	{
		XASSERT(IsOrthogonal());
		return Transpose();
	}

	BOOL 
		IsOrthogonal(REAL fTolerance = 1e-06) const;

	//-----------------------------------------------------------------------	

	inline REAL* operator [] ( size_t iRow )
	{
		assert( iRow < 3 );
		return m[iRow];
	}

	inline const REAL* operator [] ( size_t iRow ) const
	{
		assert( iRow < 3 );
		return m[iRow];
	}

	// access grants
	INLINE REAL& operator () ( UINT iRow, UINT iCol )
	{
		return m[iRow][iCol];
	}

	INLINE REAL operator () ( UINT iRow, UINT iCol ) const
	{
		return m[iRow][iCol];
	}

	INLINE bool operator == ( const Matrix3& m2 ) const
	{
		if( 
			m[0][0] != m2.m[0][0] || m[0][1] != m2.m[0][1] || m[0][2] != m2.m[0][2] ||
			m[1][0] != m2.m[1][0] || m[1][1] != m2.m[1][1] || m[1][2] != m2.m[1][2] ||
			m[2][0] != m2.m[2][0] || m[2][1] != m2.m[2][1] || m[2][2] != m2.m[2][2]  )
			return false;
		return true;
	}

	void 
		MakeTransform(const Vector3& scale, const Quaternion& orientation);

	void 
		MakeTransform(const Vector3& scale);

	void 
		MakeTransform(const Quaternion& orientation);

	void 
		MakeScaleTransform(const Vector3& scale);

	Vector3 
		TransformVector(const Vector3& vNormal);

	void 
		FromAxisAngle (const Vector3& rkAxis, const Radian& fRadians);	

	/**
		Function for writing to a stream.
	*/
	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Matrix3& mat )
	{
		o << "Matrix3{ row0(" << mat[0][0] << ", " << mat[0][1] << ", " << mat[0][2] << ") , " 
			<< "row1(" << mat[1][0] << ", " << mat[1][1] << ", " << mat[1][2] << ") , " 
			<< "row2(" << mat[2][0] << ", " << mat[2][1] << ", " << mat[2][2] << ") }";
		return o;
	}

};


_COMMON_NS_END