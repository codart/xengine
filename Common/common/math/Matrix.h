#pragma once

_COMMON_NS_BEGIN

class Vector3;
class Quaternion;

class _CommonExport Matrix4
{
public:
	static const Matrix4 IDENTITY;
	static const Matrix4 ZERO;


public:
	union {
		struct 
		{
			REAL        _11, _12, _13, _14;
			REAL        _21, _22, _23, _24;
			REAL        _31, _32, _33, _34;
			REAL        _41, _42, _43, _44;
		};
		REAL m[4][4];
		REAL _m[16];
	};

	Matrix4(){};

	INLINE Matrix4(
		REAL m00, REAL m01, REAL m02, REAL m03,
		REAL m10, REAL m11, REAL m12, REAL m13,
		REAL m20, REAL m21, REAL m22, REAL m23,
		REAL m30, REAL m31, REAL m32, REAL m33 )
	{
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;
		m[0][3] = m03;
		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;
		m[1][3] = m13;
		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
		m[2][3] = m23;
		m[3][0] = m30;
		m[3][1] = m31;
		m[3][2] = m32;
		m[3][3] = m33;
	}

	INLINE Matrix4(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis, Vector3 position);

	INLINE Matrix4(CONST Matrix4& matrix)
	{
		memcpy(&_m[0], &matrix, sizeof(Matrix4));
	}

	INLINE Matrix4& operator *= (CONST Matrix4& mat)
	{
		__asm
		{
			mov		eax,[mat]
			mov		ecx,[this]

			movups	xmm4,[eax]				// mat.m[0][0-3]
			movups	xmm5,[eax+16]			// mat.m[1][0-3]
			movups	xmm6,[eax+32]			// mat.m[2][0-3]
			movups	xmm7,[eax+48]			// mat.m[3][0-3]

			// Begin first row of result.
			movss	xmm0,[ecx]				// m[0][0] 
			shufps	xmm0,xmm0,0
			mulps	xmm0,xmm4

			movss	xmm1,[ecx+4]			// m[0][1]
			shufps	xmm1,xmm1,0
			mulps	xmm1,xmm5

			movss	xmm2,[ecx+8]			// m[0][2]
			shufps	xmm2,xmm2,0
			mulps	xmm2,xmm6

			addps	xmm1,xmm0				// First row done with xmm0

			movss	xmm3,[ecx+12]			// m[0][3]
			shufps	xmm3,xmm3,0
			mulps	xmm3,xmm7

			// Begin second row of result.
			movss	xmm0,[ecx+16]			// m[1][0] 
			shufps	xmm0,xmm0,0
			mulps	xmm0,xmm4

			addps	xmm3,xmm2				// First row done with xmm2

			movss	xmm2,[ecx+20]			// m[1][1]
			shufps	xmm2,xmm2,0
			mulps	xmm2,xmm5

			addps	xmm3,xmm1				// First row done with xmm1

			movss	xmm1,[ecx+24]			// m[1][2]
			shufps	xmm1,xmm1,0
			mulps	xmm1,xmm6

			movups	[ecx],xmm3				// Store m[0][0-3]
			// Done computing first row.

			addps	xmm2,xmm0				// Second row done with xmm0

			movss	xmm3,[ecx+28]			// m[1][3]
			shufps	xmm3,xmm3,0
			mulps	xmm3,xmm7

			// Begin third row of result.
			movss	xmm0,[ecx+32]			// m[2][0] 
			shufps	xmm0,xmm0,0
			mulps	xmm0,xmm4

			addps	xmm3,xmm1				// Second row done with xmm1

			movss	xmm1,[ecx+36]			// m[2][1]
			shufps	xmm1,xmm1,0
			mulps	xmm1,xmm5

			addps	xmm3,xmm2				// Second row done with xmm2

			movss	xmm2,[ecx+40]			// m[2][2]
			shufps	xmm2,xmm2,0
			mulps	xmm2,xmm6

			movups	[ecx+16],xmm3			// Store m[1][0-3]
			// Done computing second row.

			addps	xmm1,xmm0				// Third row done with xmm0

			movss	xmm3,[ecx+44]			// m[2][3]
			shufps	xmm3,xmm3,0
			mulps	xmm3,xmm7

			// Begin fourth row of result.
			movss	xmm0,[ecx+48]			// m[3][0]
			shufps	xmm0,xmm0,0
			mulps	xmm0,xmm4

			addps	xmm3,xmm2				// Third row done with xmm2

			movss	xmm2,[ecx+52]			// m[3][1]
			shufps	xmm2,xmm2,0
			mulps	xmm2,xmm5

			addps	xmm3,xmm1				// Third row done with xmm1

			movss	xmm1,[ecx+56]			// m[3][2]
			shufps	xmm1,xmm1,0
			mulps	xmm1,xmm6

			movups	[ecx+32],xmm3			// Store m[2][0-3]
			// Done computing third row.

			addps	xmm2,xmm0

			movss	xmm3,[ecx+60]			// m[3][3]
			shufps	xmm3,xmm3,0
			mulps	xmm3,xmm7

			// stall

			addps	xmm3,xmm1

			// stall

			addps	xmm3,xmm2

			movups	[ecx+48],xmm3			// Store m[3][0-3]
			// Done computing fourth row.
		}

		//*this = concatenate(mat);
		return *this;
	}

	INLINE Matrix4 operator * (CONST Matrix4& mat) const
	{
		return concatenate( mat );
	}

	INLINE void Identify()
	{
		*this = IDENTITY;
	}

	inline REAL Determinant() const
	{
		return	m[0][0] * (
			m[1][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][1] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) +
			m[3][1] * (m[1][2] * m[2][3] - m[1][3] * m[2][2])
			) -
			m[1][0] * (
			m[0][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][1] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][1] * (m[0][2] * m[2][3] - m[0][3] * m[2][2])
			) +
			m[2][0] * (
			m[0][1] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) -
			m[1][1] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][1] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			) -
			m[3][0] * (
			m[0][1] * (m[1][2] * m[2][3] - m[1][3] * m[2][2]) -
			m[1][1] * (m[0][2] * m[2][3] - m[0][3] * m[2][2]) +
			m[2][1] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			);
	}

	FORCEINLINE Matrix4 Transpose() const
	{
		Matrix4	Result;

		Result.m[0][0] = m[0][0];
		Result.m[0][1] = m[1][0];
		Result.m[0][2] = m[2][0];
		Result.m[0][3] = m[3][0];

		Result.m[1][0] = m[0][1];
		Result.m[1][1] = m[1][1];
		Result.m[1][2] = m[2][1];
		Result.m[1][3] = m[3][1];

		Result.m[2][0] = m[0][2];
		Result.m[2][1] = m[1][2];
		Result.m[2][2] = m[2][2];
		Result.m[2][3] = m[3][2];

		Result.m[3][0] = m[0][3];
		Result.m[3][1] = m[1][3];
		Result.m[3][2] = m[2][3];
		Result.m[3][3] = m[3][3];

		return Result;
	}

	INLINE Matrix4 Inverse() const
	{
		Matrix4 Result;
		REAL	Det = Determinant();

		if(Det == 0.0f)
			return Matrix4::IDENTITY;

		REAL RDet = 1.0f / Det;

		Result.m[0][0] = RDet * (
			m[1][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][1] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) +
			m[3][1] * (m[1][2] * m[2][3] - m[1][3] * m[2][2])
			);
		Result.m[0][1] = -RDet * (
			m[0][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][1] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][1] * (m[0][2] * m[2][3] - m[0][3] * m[2][2])
			);
		Result.m[0][2] = RDet * (
			m[0][1] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) -
			m[1][1] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][1] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			);
		Result.m[0][3] = -RDet * (
			m[0][1] * (m[1][2] * m[2][3] - m[1][3] * m[2][2]) -
			m[1][1] * (m[0][2] * m[2][3] - m[0][3] * m[2][2]) +
			m[2][1] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			);

		Result.m[1][0] = -RDet * (
			m[1][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][0] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) +
			m[3][0] * (m[1][2] * m[2][3] - m[1][3] * m[2][2])
			);
		Result.m[1][1] = RDet * (
			m[0][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
			m[2][0] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][0] * (m[0][2] * m[2][3] - m[0][3] * m[2][2])
			);
		Result.m[1][2] = -RDet * (
			m[0][0] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) -
			m[1][0] * (m[0][2] * m[3][3] - m[0][3] * m[3][2]) +
			m[3][0] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			);
		Result.m[1][3] = RDet * (
			m[0][0] * (m[1][2] * m[2][3] - m[1][3] * m[2][2]) -
			m[1][0] * (m[0][2] * m[2][3] - m[0][3] * m[2][2]) +
			m[2][0] * (m[0][2] * m[1][3] - m[0][3] * m[1][2])
			);

		Result.m[2][0] = RDet * (
			m[1][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) -
			m[2][0] * (m[1][1] * m[3][3] - m[1][3] * m[3][1]) +
			m[3][0] * (m[1][1] * m[2][3] - m[1][3] * m[2][1])
			);
		Result.m[2][1] = -RDet * (
			m[0][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) -
			m[2][0] * (m[0][1] * m[3][3] - m[0][3] * m[3][1]) +
			m[3][0] * (m[0][1] * m[2][3] - m[0][3] * m[2][1])
			);
		Result.m[2][2] = RDet * (
			m[0][0] * (m[1][1] * m[3][3] - m[1][3] * m[3][1]) -
			m[1][0] * (m[0][1] * m[3][3] - m[0][3] * m[3][1]) +
			m[3][0] * (m[0][1] * m[1][3] - m[0][3] * m[1][1])
			);
		Result.m[2][3] = -RDet * (
			m[0][0] * (m[1][1] * m[2][3] - m[1][3] * m[2][1]) -
			m[1][0] * (m[0][1] * m[2][3] - m[0][3] * m[2][1]) +
			m[2][0] * (m[0][1] * m[1][3] - m[0][3] * m[1][1])
			);

		Result.m[3][0] = -RDet * (
			m[1][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) -
			m[2][0] * (m[1][1] * m[3][2] - m[1][2] * m[3][1]) +
			m[3][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
			);
		Result.m[3][1] = RDet * (
			m[0][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) -
			m[2][0] * (m[0][1] * m[3][2] - m[0][2] * m[3][1]) +
			m[3][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1])
			);
		Result.m[3][2] = -RDet * (
			m[0][0] * (m[1][1] * m[3][2] - m[1][2] * m[3][1]) -
			m[1][0] * (m[0][1] * m[3][2] - m[0][2] * m[3][1]) +
			m[3][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1])
			);
		Result.m[3][3] = RDet * (
			m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
			m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]) +
			m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1])
			);

		return Result;
	}

	Matrix4 Matrix4::InverseAffine(void) const
	{
		assert(IsAffine());

		REAL m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
		REAL m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

		REAL t00 = m22 * m11 - m21 * m12;
		REAL t10 = m20 * m12 - m22 * m10;
		REAL t20 = m21 * m10 - m20 * m11;

		REAL m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];

		REAL invDet = 1 / (m00 * t00 + m01 * t10 + m02 * t20);

		t00 *= invDet; t10 *= invDet; t20 *= invDet;

		m00 *= invDet; m01 *= invDet; m02 *= invDet;

		REAL r00 = t00;
		REAL r01 = m02 * m21 - m01 * m22;
		REAL r02 = m01 * m12 - m02 * m11;

		REAL r10 = t10;
		REAL r11 = m00 * m22 - m02 * m20;
		REAL r12 = m02 * m10 - m00 * m12;

		REAL r20 = t20;
		REAL r21 = m01 * m20 - m00 * m21;
		REAL r22 = m00 * m11 - m01 * m10;

		REAL m30 = m[3][0], m31 = m[3][1], m32 = m[3][2];

		//REAL r03 = - (r00 * m30 + r01 * m31 + r02 * m32);
		//REAL r13 = - (r10 * m30 + r11 * m31 + r12 * m32);
		//REAL r23 = - (r20 * m30 + r21 * m31 + r22 * m32);

		REAL r03 = - (r00 * m30 + r10 * m31 + r20 * m32);
		REAL r13 = - (r01 * m30 + r11 * m31 + r21 * m32);
		REAL r23 = - (r02 * m30 + r12 * m31 + r22 * m32);

		return Matrix4(
			r00, r01, r02, 0,
			r10, r11, r12, 0,
			r20, r21, r22, 0,
			r03, r13, r23, 1);
	}
	
	// When the matrix is affine, and its 3x3 sub matrix is a rotation matrix, can use this method 
	// to do inverse.
	Matrix4 Matrix4::InverseOrthogonal(void) const
	{
		XASSERT(0);
		return IDENTITY;
	}

	//-----------------------------------------------------------------------	

	inline REAL* operator [] ( size_t iRow )
	{
		assert( iRow < 4 );
		return m[iRow];
	}

	inline const REAL* operator [] ( size_t iRow ) const
	{
		assert( iRow < 4 );
		return m[iRow];
	}

	// access grants
	INLINE REAL& operator () ( UINT iRow, UINT iCol )
	{
		return m[iRow][iCol];
	}

	INLINE REAL operator () ( UINT iRow, UINT iCol ) const
	{
		return m[iRow][iCol];
	}

	INLINE bool operator == ( const Matrix4& m2 ) const
	{
		if( 
			m[0][0] != m2.m[0][0] || m[0][1] != m2.m[0][1] || m[0][2] != m2.m[0][2] || m[0][3] != m2.m[0][3] ||
			m[1][0] != m2.m[1][0] || m[1][1] != m2.m[1][1] || m[1][2] != m2.m[1][2] || m[1][3] != m2.m[1][3] ||
			m[2][0] != m2.m[2][0] || m[2][1] != m2.m[2][1] || m[2][2] != m2.m[2][2] || m[2][3] != m2.m[2][3] ||
			m[3][0] != m2.m[3][0] || m[3][1] != m2.m[3][1] || m[3][2] != m2.m[3][2] || m[3][3] != m2.m[3][3] )
			return false;
		return true;
	}

	void MakeTransform(const Vector3& position, const Vector3& scale, const Quaternion& orientation);
	void MakeTransform(const Vector3& position, const Vector3& scale);
	void MakeTransform(const Vector3& position, const Quaternion& orientation);
	void MakeTransform(const Quaternion& orientation);
	void MakeScaleTransform(const Vector3& scale);

	//-------
	static Matrix4 BuildTransform(const Vector3& position);
	static Matrix4 BuildTransform(const Quaternion& orientation);
	static Matrix4 BuildTransform(const Vector3& position, const Vector3& scale, const Quaternion& orientation);
	static Matrix4 BuildTransform(const Vector3& position, const Vector3& scale);
	static Matrix4 BuildTransform(const Vector3& position, const Quaternion& orientation);
	static Matrix4 BuildScaleTransform(const Vector3& scale);
	//-------
	INLINE const Vector3* GetPosition() const { return (Vector3*)&_41; }
	void SetPosition(const Vector3& position);

	// Extracts the translation transformation part of the matrix.
	void GetTranslation(OUT Vector3& vTranslation) const;

   /** 3-D Vector transformation specially for an affine matrix.
        @remarks
            Transforms the given 3-D vector by the matrix, projecting the 
            result back into <i>w</i> = 1.
        @note
            The matrix must be an affine matrix. @see Matrix4::IsAffine.
    */
    INLINE Vector3 TransformAffine(const Vector3& v) const;

    /** Check whether or not the matrix is affine matrix.
        @remarks
            An affine matrix is a 4x4 matrix with column 3 equal to (0, 0, 0, 1),
            e.g. no projective coefficients.
    */
    INLINE bool IsAffine(void) const
    {
        return m[0][3] == 0 && m[1][3] == 0 && m[2][3] == 0 && m[3][3] == 1;
    }

	Vector3 TransformNormal(const Vector3& vNormal);

	/** 
		Function for writing to a stream.
	*/
	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Matrix4& mat )
	{
		o << "Matrix4{ row0(" << mat[0][0] << ", " << mat[0][1] << ", " << mat[0][2] << ", " << mat[0][3] << ") , " 
			<< "row1(" << mat[1][0] << ", " << mat[1][1] << ", " << mat[1][2] << ", " << mat[1][3] << ") , " 
			<< "row2(" << mat[2][0] << ", " << mat[2][1] << ", " << mat[2][2] << ", " << mat[2][3] << ") , "	
	 		<< "row3(" << mat[3][0] << ", " << mat[3][1] << ", " << mat[3][2] << ", " << mat[3][3] << ") }";
		return o;
	}


protected:
	inline Matrix4 concatenate(const Matrix4 &m2) const
	{
		Matrix4	matResult;

		// Cpp implementation ---
		matResult.m[0][0] = m[0][0] * m2.m[0][0] + m[0][1] * m2.m[1][0] + m[0][2] * m2.m[2][0] + m[0][3] * m2.m[3][0];
		matResult.m[0][1] = m[0][0] * m2.m[0][1] + m[0][1] * m2.m[1][1] + m[0][2] * m2.m[2][1] + m[0][3] * m2.m[3][1];
		matResult.m[0][2] = m[0][0] * m2.m[0][2] + m[0][1] * m2.m[1][2] + m[0][2] * m2.m[2][2] + m[0][3] * m2.m[3][2];
		matResult.m[0][3] = m[0][0] * m2.m[0][3] + m[0][1] * m2.m[1][3] + m[0][2] * m2.m[2][3] + m[0][3] * m2.m[3][3];

		matResult.m[1][0] = m[1][0] * m2.m[0][0] + m[1][1] * m2.m[1][0] + m[1][2] * m2.m[2][0] + m[1][3] * m2.m[3][0];
		matResult.m[1][1] = m[1][0] * m2.m[0][1] + m[1][1] * m2.m[1][1] + m[1][2] * m2.m[2][1] + m[1][3] * m2.m[3][1];
		matResult.m[1][2] = m[1][0] * m2.m[0][2] + m[1][1] * m2.m[1][2] + m[1][2] * m2.m[2][2] + m[1][3] * m2.m[3][2];
		matResult.m[1][3] = m[1][0] * m2.m[0][3] + m[1][1] * m2.m[1][3] + m[1][2] * m2.m[2][3] + m[1][3] * m2.m[3][3];

		matResult.m[2][0] = m[2][0] * m2.m[0][0] + m[2][1] * m2.m[1][0] + m[2][2] * m2.m[2][0] + m[2][3] * m2.m[3][0];
		matResult.m[2][1] = m[2][0] * m2.m[0][1] + m[2][1] * m2.m[1][1] + m[2][2] * m2.m[2][1] + m[2][3] * m2.m[3][1];
		matResult.m[2][2] = m[2][0] * m2.m[0][2] + m[2][1] * m2.m[1][2] + m[2][2] * m2.m[2][2] + m[2][3] * m2.m[3][2];
		matResult.m[2][3] = m[2][0] * m2.m[0][3] + m[2][1] * m2.m[1][3] + m[2][2] * m2.m[2][3] + m[2][3] * m2.m[3][3];

		matResult.m[3][0] = m[3][0] * m2.m[0][0] + m[3][1] * m2.m[1][0] + m[3][2] * m2.m[2][0] + m[3][3] * m2.m[3][0];
		matResult.m[3][1] = m[3][0] * m2.m[0][1] + m[3][1] * m2.m[1][1] + m[3][2] * m2.m[2][1] + m[3][3] * m2.m[3][1];
		matResult.m[3][2] = m[3][0] * m2.m[0][2] + m[3][1] * m2.m[1][2] + m[3][2] * m2.m[2][2] + m[3][3] * m2.m[3][2];
		matResult.m[3][3] = m[3][0] * m2.m[0][3] + m[3][1] * m2.m[1][3] + m[3][2] * m2.m[2][3] + m[3][3] * m2.m[3][3];

		return matResult;
	}



};


_COMMON_NS_END