#pragma once

#include <Math.h>
#include "Angle.h"

_COMMON_NS_BEGIN


class Plane;
class Matrix4;

//////////////////////////////////////////////////////////////////////////
class _CommonExport Math
{
public:
	static const REAL POS_INFINITY;
	static const REAL NEG_INFINITY;
	static const REAL PI;
	static const REAL TWO_PI;
	static const REAL HALF_PI;
	static const REAL fDeg2Rad;
	static const REAL fRad2Deg;
	static const REAL LOG2;
	static const REAL EPSILON;

	static uint32_t s_nRandomSeed;

public:
	Math(void);
	~Math(void);

public:
	//--- Random methods ---
	// ���ͬ�෨
	static INLINE REAL 
		Rand(REAL min, REAL max)
	{
		s_nRandomSeed = 214013 * s_nRandomSeed + 2531011;
		return min + (s_nRandomSeed >> 16) * (1.0f/65535.0f) * (max-min);
	}

	static INLINE int 
		Rand(int min, int max)
	{
		s_nRandomSeed = 214013 * s_nRandomSeed + 2531011;
		return min + (s_nRandomSeed ^ s_nRandomSeed >> 15) % (max - min + 1);
	}

	static INLINE void 
		Seed(int iSeed)
	{
		s_nRandomSeed = iSeed;
	}

	static INLINE void 
		SeedByTime()
	{
		REAL fCurTime = GetTime();
		int iCurTime = *((DWORD*)(&fCurTime));
		s_nRandomSeed = iCurTime;
	}

	INLINE static void 
		SeedByAccurateTime()
	{
		REAL fCurTime = GetAccurateTime();
		int iCurTime = *((DWORD*)(&fCurTime));
		s_nRandomSeed = iCurTime;
	}

	//----------
	static BOOL
		Equal(int a, int b) { return a == b; }
	static BOOL
		Equal(REAL a, REAL b, REAL e = Math::EPSILON) { return Math::Abs(a-b) < e; }

	// Loop increment i in range of 0 ~ n-1
	// For example: LoopIncN<3>(i), when i = 0, 1, 2  result is 1, 2, 0
	template<int n>
	static int32_t 
		LoopIncN(int32_t i) { return (i+1) & ( i- (n-1) )>>31; }

	// Loop decrement i in range of 0 ~ n-1
	// For example: LoopDecN<3>(i), when i = 2, 1, 0  result is 1, 0, 2
	template<int n>
	static int32_t 
		LoopDecN(int32_t i) { return (i-1) + ( (i-1)>>31 & n ); }

	static INLINE int32_t 
		IsNegetive(REAL x) 
	{
		union { REAL f; UINT_REAL i; } u;
		u.f = x; 
		return (int32_t)(u.i>>(REAL_BIT_COUNT - 1));
	}

	static INLINE int32_t 
		IsNegetive(int32_t x)
	{
		return (int32_t)((uint32_t)x>>31);
	}

	static INLINE REAL 
		Square(REAL value) { return(value * value); }

	static Matrix4 BuildReflectionMatrix(const Plane& p);

	static INLINE BOOL IsNaN(REAL fValue)
	{
		// std::isnan() is C99, not supported by all compilers
		// However NaN always fails this next test, no other number does.
		return fValue != fValue;
	}

	static INLINE BOOL IsFinite(REAL fValue)
	{
		return !IsNaN(fValue) && fValue != NEG_INFINITY && fValue != POS_INFINITY; 
	}

	static INLINE REAL Sin (const Radian& fValue) 
	{
		return REAL(sin(fValue.ValueRadians()));
	}

	static INLINE REAL Cos (const Radian& fValue) 
	{
		return REAL(cos(fValue.ValueRadians()));
	}

	// Low resolution sin, but fast speed
	static INLINE REAL LowSin(REAL x)
	{
		const REAL B = 4.0f/PI;
		const REAL C = 4/(PI * PI);

		if (x < -PI)
		{
			x += TWO_PI;
		}
		else
		{
			if (x > PI)
			{
				x -= TWO_PI;
			}
		}

		REAL sin;

		if (x < 0)
			sin = B * x + C * x * x;
		else
			sin = B * x - C * x * x;

		return sin;
	}

	// Low resolution cos, but fast speed
	static INLINE REAL LowCos(REAL x)
	{
		const REAL B = 4/PI;
		const REAL C = 4/(PI * PI);

		x += HALF_PI;
		if (x < -PI)
		{
			x += TWO_PI;
		}
		else
		{
			if (x > PI)
			{
				x -= TWO_PI;
			}
		}

		REAL cos;
		if (x < 0)
		{
			cos = B * x + C * x * x;
		}
		else
		{
			cos = B * x - C * x * x;
		}
		return cos;
	}

	static inline REAL Sqrt (REAL fValue) { return REAL(sqrt(fValue)); }

	static inline float InvSqrt(float fValue)
	{
		float xhalf = 0.5f * fValue;
		int i = *(int*)&fValue;
		i = 0x5f3759df - (i>>1);
		fValue = *(float*)&i;
		fValue = fValue*(1.5f - xhalf*fValue*fValue);
		return fValue;
	}

	static const char s_numberChars[]; 
	static const int s_numberCharsCount;

	static INLINE BOOL IsNumberChar(char cchar)
	{
		if ( (cchar >= '0' && cchar <= '9') ) return TRUE;

		for (int i=0; i<s_numberCharsCount; ++i)
		{
			if(cchar == s_numberChars[i]) return TRUE;
		}

		return FALSE;
	}

	static INLINE BOOL IsNumber(char* pszNumString)
	{
		int nLength = strlen(pszNumString);
		if( nLength < 0) return false;

		bool bIsNumber = true;
		for (int i=0; i < nLength; ++i)
		{
			char cchar = pszNumString[i];

			if(!IsNumberChar(cchar))
			{
				bIsNumber = false;
				break;
			}
		}

		return bIsNumber;		
	}

	static inline REAL Abs (REAL fValue) { return REAL(fabs(fValue)); }

	static inline REAL DegreesToRadians(REAL degrees) { return degrees * fDeg2Rad; }
	static inline REAL RadiansToDegrees(REAL radians) { return radians * fRad2Deg; }
};


// these functions must be defined down here, because they rely on the
// angle unit conversion functions in class Math:
inline REAL Degree::valueRadians() const
{
	return Math::DegreesToRadians ( mDeg );
}

_COMMON_NS_END