#include "StdAfx.h"
#include "AxisAlignedBox.h"

_COMMON_NS_BEGIN

const AxisAlignedBox AxisAlignedBox::BOX_NULL;
const AxisAlignedBox AxisAlignedBox::BOX_INFINITE(AxisAlignedBox::EXTENT_INFINITE);

_COMMON_NS_END