#pragma once

#include "d3dx9math.h"
#include "math.h"
#include "matrix.h"

_COMMON_NS_BEGIN

no_v_base class _CommonExport Vector2
{
public:
	static const Vector2 ZERO;
	static const Vector2 UNIT_X;
	static const Vector2 UNIT_Y;
	static const Vector2 NEGATIVE_UNIT_X;
	static const Vector2 NEGATIVE_UNIT_Y;
	static const Vector2 UNIT_SCALE;

public:
	Vector2(){};

	Vector2(REAL x, REAL y)
	{
		this->x = x;
		this->y = y;
	}


public:
	REAL x, y;


public:
	INLINE REAL Normalise()
	{
		REAL fLength = Math::Sqrt( x * x + y * y);

		// Will also work for zero-sized vectors, but will change nothing
		if ( fLength > 1e-08 )
		{
			REAL fInvLength = REAL(1.0f) / fLength;
			x *= fInvLength;
			y *= fInvLength;
		}
		return fLength;
	}

	INLINE Vector2 operator + ( const Vector2& v) const
	{
		return Vector2(x + v.x, y + v.y);
	}

	Vector2 operator - ( CONST Vector2& v) const
	{
		return Vector2(x - v.x, y - v.y);
	}

	INLINE Vector2 operator * ( FLOAT value) const
	{
		return Vector2(x * value, y * value);
	}

	INLINE Vector2 operator / ( float value) const
	{
		return Vector2(x / value, y / value);
	}

	INLINE Vector2 GetOrthogonal() const
	{
		return Vector2(y, -x);
	}

    /** Calculates the dot (scalar) product of this vector with another.
        @remarks
            The dot product can be used to calculate the angle between 2
            vectors. If both are unit vectors, the dot product is the
            cosine of the angle; otherwise the dot product must be
            divided by the product of the lengths of both vectors to get
            the cosine of the angle. This result can further be used to
            calculate the distance of a point from a plane.
        @param
            vec Vector with which to calculate the dot product (together
            with this one).
        @returns
            A float representing the dot product value.
    */
    INLINE REAL DotProduct(const Vector2& dest) const
    {
        return x * dest.x + y * dest.y;
    }

	/**
		Calculates the 2 dimensional cross-product of 2 vectors, which results
		in a single floating point value which is 2 times the area of the triangle.
	*/
	INLINE REAL CrossProduct( const Vector2& dest ) const
	{
		return x * dest.y - y * dest.x;
	}

	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Vector2& vector )
	{
		o << "Vector2(" << vector.x << ", " << vector.y << ")";
		return o;
	}

};

_COMMON_NS_END