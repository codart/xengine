#include "StdAfx.h"
#include "Matrix.h"

#include "Matrix3.h"
#include "Vector3.h"
#include "Quaternion.h"

_COMMON_NS_BEGIN

const Matrix4 Matrix4::ZERO(
							0, 0, 0, 0,
							0, 0, 0, 0,
							0, 0, 0, 0,
							0, 0, 0, 0 );

const Matrix4 Matrix4::IDENTITY(
								1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 1, 0,
								0, 0, 0, 1 );



INLINE Matrix4::Matrix4(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis, Vector3 position)
{
	m[0][0] = xAxis.x;
	m[0][1] = xAxis.y;
	m[0][2] = xAxis.z;
	m[0][3] = 1;

	m[1][0] = yAxis.x;
	m[1][1] = yAxis.y;
	m[1][2] = yAxis.z;
	m[1][3] = 1;

	m[2][0] = zAxis.x;
	m[2][1] = zAxis.y;
	m[2][2] = zAxis.z;
	m[2][3] = 1;

	m[3][0] = position.x;
	m[3][1] = position.y;
	m[3][2] = position.z;
	m[3][3] = 1;
}

void Matrix4::GetTranslation(OUT Vector3& vTranslation) const
{
	vTranslation.x = _41;
	vTranslation.y = _42;
	vTranslation.z = _43;
}

Vector3 Matrix4::TransformAffine(const Vector3& v) const
{
	assert(IsAffine());

	return Vector3(m[0][0] * v.x + m[1][0] * v.y + m[2][0] * v.z + m[3][0], 
					m[0][1] * v.x + m[1][1] * v.y + m[2][1] * v.z + m[3][1],
					m[0][2] * v.x + m[1][2] * v.y + m[2][2] * v.z + m[3][2]);
}

void Matrix4::MakeTransform(const Quaternion& orientation)
{
	Matrix3 rot3x3;
	orientation.ToRotationMatrix(rot3x3);

	// Set up final matrix with rotation and translation
	m[0][0] = rot3x3[0][0]; m[1][0] = rot3x3[0][1]; m[2][0] = rot3x3[0][2]; m[3][0] = 0;
	m[0][1] = rot3x3[1][0]; m[1][1] = rot3x3[1][1]; m[2][1] = rot3x3[1][2]; m[3][1] = 0;
	m[0][2] = rot3x3[2][0]; m[1][2] = rot3x3[2][1]; m[2][2] = rot3x3[2][2]; m[3][2] = 0;

	// No projection term
	m[0][3] = 0; m[1][3] = 0; m[2][3] = 0; m[3][3] = 1;
}

void Matrix4::MakeTransform(const Vector3& position, const Quaternion& orientation)
{
	// Ordering:
	//    1. Scale
	//    2. Rotate
	//    3. Translate

	Matrix3 rot3x3;
	orientation.ToRotationMatrix(rot3x3);

	// Set up final matrix with rotation and translation
	m[0][0] = rot3x3[0][0]; m[1][0] = rot3x3[0][1]; m[2][0] = rot3x3[0][2]; m[3][0] = position.x;
	m[0][1] = rot3x3[1][0]; m[1][1] = rot3x3[1][1]; m[2][1] = rot3x3[1][2]; m[3][1] = position.y;
	m[0][2] = rot3x3[2][0]; m[1][2] = rot3x3[2][1]; m[2][2] = rot3x3[2][2]; m[3][2] = position.z;

	// No projection term
	m[0][3] = 0; m[1][3] = 0; m[2][3] = 0; m[3][3] = 1;
}

void Matrix4::MakeTransform(const Vector3& position, const Vector3& scale, const Quaternion& orientation)
{
	*this = IDENTITY;
	// Ordering:
	//    1. Scale
	//    2. Rotate
	//    3. Translate

	Matrix3 rot3x3;
	orientation.ToRotationMatrix(rot3x3);

	// Set up final matrix with scale, rotation and translation
	m[0][0] = scale.x * rot3x3[0][0]; m[1][0] = scale.y * rot3x3[0][1]; m[2][0] = scale.z * rot3x3[0][2]; m[3][0] = position.x;
	m[0][1] = scale.x * rot3x3[1][0]; m[1][1] = scale.y * rot3x3[1][1]; m[2][1] = scale.z * rot3x3[1][2]; m[3][1] = position.y;
	m[0][2] = scale.x * rot3x3[2][0]; m[1][2] = scale.y * rot3x3[2][1]; m[2][2] = scale.z * rot3x3[2][2]; m[3][2] = position.z;

	// No projection term
	m[0][3] = 0; m[1][3] = 0; m[2][3] = 0; m[3][3] = 1;
}

Matrix4 Matrix4::BuildTransform(const Vector3& position, const Vector3& scale, const Quaternion& orientation)
{
	Matrix4 matrix;
	matrix.MakeTransform(position, scale, orientation);
	return matrix;
}

Matrix4 Matrix4::BuildTransform(const Vector3& position, const Vector3& scale)
{
	Matrix4 matrix;
	matrix.MakeTransform(position, scale);
	return matrix;
}

Matrix4 Matrix4::BuildTransform(const Vector3& position, const Quaternion& orientation)
{
	Matrix4 matrix;
	matrix.MakeTransform(position, orientation);
	return matrix;
}

Matrix4 Matrix4::BuildScaleTransform(const Vector3& scale)
{
	Matrix4 matrix;
	matrix.MakeScaleTransform(scale);
	return matrix;
}

Matrix4 Matrix4::BuildTransform(const Vector3& position)
{
	Matrix4 matrix = Matrix4::IDENTITY;
	matrix.SetPosition(position);
	return matrix;
}

Matrix4 Matrix4::BuildTransform(const Quaternion& orientation)
{
	Matrix4 matrix;
	matrix.MakeTransform(orientation);
	return matrix;
}

void Matrix4::MakeTransform(const Vector3& position, const Vector3& scale)
{
	*this = IDENTITY;

	this->_11 = scale.x;
	this->_22 = scale.y;
	this->_33 = scale.z;

	this->_41 = position.x;
	this->_42 = position.y;
	this->_43 = position.z;
}

void Matrix4::MakeScaleTransform(const Vector3& scale)
{
	*this = IDENTITY;
	this->_11 = scale.x;
	this->_22 = scale.y;
	this->_33 = scale.z;
}

Vector3 Matrix4::TransformNormal(const Vector3& vNormal)
{
	Vector3 vResult;
	vResult.x = ( m[0][0] * vNormal.x + m[1][0] * vNormal.y + m[2][0] * vNormal.z);
	vResult.y = ( m[0][1] * vNormal.x + m[1][1] * vNormal.y + m[2][1] * vNormal.z);
	vResult.z = ( m[0][2] * vNormal.x + m[1][2] * vNormal.y + m[2][2] * vNormal.z);

	return vResult;
}

void Matrix4::SetPosition(const Vector3& position)
{
	(*(Vector3*)&_41) = position; 
}

_COMMON_NS_END

