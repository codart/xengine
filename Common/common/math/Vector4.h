#pragma once

#include "math.h"
#include "matrix.h"

_COMMON_NS_BEGIN

class Vector3;

/** 4-dimensional homogeneous vector.
*/
class _CommonExport Vector4
{
public:
	static const Vector4 ZERO;

public:
	REAL x, y, z, w;

public:
	INLINE Vector4()
	{
	}

	INLINE Vector4( const REAL fX, const REAL fY, const REAL fZ, const REAL fW )
		: x( fX ), y( fY ), z( fZ ), w( fW)
	{
	}

	INLINE explicit Vector4( const REAL afCoordinate[4] )
		: x( afCoordinate[0] ),
		y( afCoordinate[1] ),
		z( afCoordinate[2] ),
		w( afCoordinate[3] )
	{
	}

	INLINE explicit Vector4( const int afCoordinate[4] )
	{
		x = (REAL)afCoordinate[0];
		y = (REAL)afCoordinate[1];
		z = (REAL)afCoordinate[2];
		w = (REAL)afCoordinate[3];
	}

	INLINE explicit Vector4( REAL* const r )
		: x( r[0] ), y( r[1] ), z( r[2] ), w( r[3] )
	{
	}

	INLINE explicit Vector4( const REAL scaler )
		: x( scaler )
		, y( scaler )
		, z( scaler )
		, w( scaler )
	{
	}

	explicit Vector4(const Vector3& rhs);

	/** Exchange the contents of this vector with another. 
	*/
	INLINE void swap(Vector4& other)
	{
		std::swap(x, other.x);
		std::swap(y, other.y);
		std::swap(z, other.z);
		std::swap(w, other.w);
	}

	INLINE REAL operator [] ( const size_t i ) const
	{
		assert( i < 4 );

		return *(&x+i);
	}

	INLINE REAL& operator [] ( const size_t i )
	{
		assert( i < 4 );

		return *(&x+i);
	}

	/// Pointer accessor for direct copying
	INLINE REAL* ptr()
	{
		return &x;
	}
	/// Pointer accessor for direct copying
	INLINE const REAL* ptr() const
	{
		return &x;
	}

	INLINE Vector4& operator = ( const REAL fScalar)
	{
		x = fScalar;
		y = fScalar;
		z = fScalar;
		w = fScalar;
		return *this;
	}

	INLINE bool operator == ( const Vector4& rkVector ) const
	{
		return ( x == rkVector.x &&
			y == rkVector.y &&
			z == rkVector.z &&
			w == rkVector.w );
	}

	INLINE bool operator != ( const Vector4& rkVector ) const
	{
		return ( x != rkVector.x ||
			y != rkVector.y ||
			z != rkVector.z ||
			w != rkVector.w );
	}

	Vector4& operator = (const Vector3& rhs);

	// arithmetic operations
	INLINE Vector4 operator + ( const Vector4& rkVector ) const
	{
		return Vector4(
			x + rkVector.x,
			y + rkVector.y,
			z + rkVector.z,
			w + rkVector.w);
	}

	INLINE Vector4 operator - ( const Vector4& rkVector ) const
	{
		return Vector4(
			x - rkVector.x,
			y - rkVector.y,
			z - rkVector.z,
			w - rkVector.w);
	}

	INLINE Vector4 operator * ( const REAL fScalar ) const
	{
		return Vector4(
			x * fScalar,
			y * fScalar,
			z * fScalar,
			w * fScalar);
	}

	INLINE Vector4 operator * ( const Vector4& rhs) const
	{
		return Vector4(
			rhs.x * x,
			rhs.y * y,
			rhs.z * z,
			rhs.w * w);
	}

	INLINE Vector4 operator / ( const REAL fScalar ) const
	{
		assert( fScalar != 0.0 );

		REAL fInv = 1.0f / fScalar;

		return Vector4(
			x * fInv,
			y * fInv,
			z * fInv,
			w * fInv);
	}

	INLINE Vector4 operator / ( const Vector4& rhs) const
	{
		return Vector4(
			x / rhs.x,
			y / rhs.y,
			z / rhs.z,
			w / rhs.w);
	}

	INLINE const Vector4& operator + () const
	{
		return *this;
	}

	INLINE Vector4 operator - () const
	{
		return Vector4(-x, -y, -z, -w);
	}

	INLINE friend Vector4 operator * ( const REAL fScalar, const Vector4& rkVector )
	{
		return Vector4(
			fScalar * rkVector.x,
			fScalar * rkVector.y,
			fScalar * rkVector.z,
			fScalar * rkVector.w);
	}

	INLINE friend Vector4 operator / ( const REAL fScalar, const Vector4& rkVector )
	{
		return Vector4(
			fScalar / rkVector.x,
			fScalar / rkVector.y,
			fScalar / rkVector.z,
			fScalar / rkVector.w);
	}

	INLINE friend Vector4 operator + (const Vector4& lhs, const REAL rhs)
	{
		return Vector4(
			lhs.x + rhs,
			lhs.y + rhs,
			lhs.z + rhs,
			lhs.w + rhs);
	}

	INLINE friend Vector4 operator + (const REAL lhs, const Vector4& rhs)
	{
		return Vector4(
			lhs + rhs.x,
			lhs + rhs.y,
			lhs + rhs.z,
			lhs + rhs.w);
	}

	INLINE friend Vector4 operator - (const Vector4& lhs, REAL rhs)
	{
		return Vector4(
			lhs.x - rhs,
			lhs.y - rhs,
			lhs.z - rhs,
			lhs.w - rhs);
	}

	INLINE friend Vector4 operator - (const REAL lhs, const Vector4& rhs)
	{
		return Vector4(
			lhs - rhs.x,
			lhs - rhs.y,
			lhs - rhs.z,
			lhs - rhs.w);
	}

	// arithmetic updates
	INLINE Vector4& operator += ( const Vector4& rkVector )
	{
		x += rkVector.x;
		y += rkVector.y;
		z += rkVector.z;
		w += rkVector.w;

		return *this;
	}

	INLINE Vector4& operator -= ( const Vector4& rkVector )
	{
		x -= rkVector.x;
		y -= rkVector.y;
		z -= rkVector.z;
		w -= rkVector.w;

		return *this;
	}

	INLINE Vector4& operator *= ( const REAL fScalar )
	{
		x *= fScalar;
		y *= fScalar;
		z *= fScalar;
		w *= fScalar;
		return *this;
	}

	INLINE Vector4& operator += ( const REAL fScalar )
	{
		x += fScalar;
		y += fScalar;
		z += fScalar;
		w += fScalar;
		return *this;
	}

	INLINE Vector4& operator -= ( const REAL fScalar )
	{
		x -= fScalar;
		y -= fScalar;
		z -= fScalar;
		w -= fScalar;
		return *this;
	}

	INLINE Vector4& operator *= ( const Vector4& rkVector )
	{
		x *= rkVector.x;
		y *= rkVector.y;
		z *= rkVector.z;
		w *= rkVector.w;

		return *this;
	}

	INLINE Vector4& operator /= ( const REAL fScalar )
	{
		assert( fScalar != 0.0 );

		REAL fInv = 1.0f / fScalar;

		x *= fInv;
		y *= fInv;
		z *= fInv;
		w *= fInv;

		return *this;
	}

	INLINE Vector4& operator /= ( const Vector4& rkVector )
	{
		x /= rkVector.x;
		y /= rkVector.y;
		z /= rkVector.z;
		w /= rkVector.w;

		return *this;
	}

	INLINE Vector4 operator * (const Matrix4& matrix) const
	{
		return Vector4(
			matrix.m[0][0] * x + matrix.m[1][0] * y + matrix.m[2][0] * z + matrix.m[3][0] * w, 
			matrix.m[0][1] * x + matrix.m[1][1] * y + matrix.m[2][1] * z + matrix.m[3][1] * w,
			matrix.m[0][2] * x + matrix.m[1][2] * y + matrix.m[2][2] * z + matrix.m[3][2] * w,
			matrix.m[0][3] * x + matrix.m[1][3] * y + matrix.m[2][3] * z + matrix.m[3][3] * w
			);
	}

	/** Calculates the dot (scalar) product of this vector with another.
	@param
	vec Vector with which to calculate the dot product (together
	with this one).
	@returns
	A float representing the dot product value.
	*/
	INLINE REAL dotProduct(const Vector4& vec) const
	{
		return x * vec.x + y * vec.y + z * vec.z + w * vec.w;
	}
	/// Check whether this vector contains valid values
	INLINE BOOL IsNaN() const
	{
		return Math::IsNaN(x) || Math::IsNaN(y) || Math::IsNaN(z) || Math::IsNaN(w);
	}

	/** 
		Function for writing to a stream.
	*/
	INLINE friend std::ostream& operator <<
		( std::ostream& o, const Vector4& v )
	{
		o << "Vector4(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
		return o;
	}

};


_COMMON_NS_END