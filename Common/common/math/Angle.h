#pragma once


_COMMON_NS_BEGIN


class Degree;


/** Wrapper class which indicates a given angle value is in Radians.
	@remarks
	Radian values are interchangeable with Degree values, and conversions
	will be done automatically between them.
*/
class Radian
{
	REAL mRad;

public:
	Radian ( REAL r=0 ) : mRad(r) {}
	Radian ( const Degree& d );
	Radian& operator = ( const REAL& f ) { mRad = f; return *this; }
	Radian& operator = ( const Degree& d );

	REAL ValueDegrees() const; // see bottom of this file
	REAL ValueRadians() const { return mRad; }
	REAL ValueAngleUnits() const;

	const Radian& operator + () const { return *this; }
	Radian operator + ( const Radian& r ) const { return Radian ( mRad + r.mRad ); }
	Radian operator + ( const Degree& d ) const;
	Radian& operator += ( const Radian& r ) { mRad += r.mRad; return *this; }
	Radian& operator += ( const Degree& d );
	Radian& operator += ( REAL fAngle) { mRad += fAngle; return *this; }
	Radian operator - () const { return Radian(-mRad); }
	Radian operator - ( const Radian& r ) const { return Radian ( mRad - r.mRad ); }
	Radian operator - ( const Degree& d ) const;
	Radian& operator -= ( REAL fAngle) { mRad -= fAngle; return *this; }
	Radian& operator -= ( const Radian& r ) { mRad -= r.mRad; return *this; }
	Radian& operator -= ( const Degree& d );
	Radian operator * ( REAL f ) const { return Radian ( mRad * f ); }
	Radian operator * ( const Radian& f ) const { return Radian ( mRad * f.mRad ); }
	Radian& operator *= ( REAL f ) { mRad *= f; return *this; }
	Radian operator / ( REAL f ) const { return Radian ( mRad / f ); }
	Radian& operator /= ( REAL f ) { mRad /= f; return *this; }

	bool operator <  ( const Radian& r ) const { return mRad <  r.mRad; }
	bool operator <= ( const Radian& r ) const { return mRad <= r.mRad; }
	bool operator == ( const Radian& r ) const { return mRad == r.mRad; }
	bool operator != ( const Radian& r ) const { return mRad != r.mRad; }
	bool operator >= ( const Radian& r ) const { return mRad >= r.mRad; }
	bool operator >  ( const Radian& r ) const { return mRad >  r.mRad; }


	inline _CommonExport friend std::ostream& operator <<
		( std::ostream& o, const Radian& v )
	{
		o << "Radian(" << (REAL)v.ValueRadians() << ")";
		return o;
	}
};


/** Wrapper class which indicates a given angle value is in Degrees.
	@remarks
	Degree values are interchangeable with Radian values, and conversions
	will be done automatically between them.
*/
class Degree
{
	REAL mDeg; // if you get an error here - make sure to define/typedef 'REAL' first

public:
	explicit Degree ( REAL d=0 ) : mDeg(d) {}
	Degree ( const Radian& r ) : mDeg(r.ValueDegrees()) {}
	Degree& operator = ( const REAL& f ) { mDeg = f; return *this; }
	Degree& operator = ( const Degree& d ) { mDeg = d.mDeg; return *this; }
	Degree& operator = ( const Radian& r ) { mDeg = r.ValueDegrees(); return *this; }

	REAL valueDegrees() const { return mDeg; }
	REAL valueRadians() const; // see bottom of this file
	REAL valueAngleUnits() const;

	const Degree& operator + () const { return *this; }
	Degree operator + ( const Degree& d ) const { return Degree ( mDeg + d.mDeg ); }
	Degree operator + ( const Radian& r ) const { return Degree ( mDeg + r.ValueDegrees() ); }
	Degree& operator += ( const Degree& d ) { mDeg += d.mDeg; return *this; }
	Degree& operator += ( const Radian& r ) { mDeg += r.ValueDegrees(); return *this; }
	Degree operator - () const { return Degree(-mDeg); }
	Degree operator - ( const Degree& d ) const { return Degree ( mDeg - d.mDeg ); }
	Degree operator - ( const Radian& r ) const { return Degree ( mDeg - r.ValueDegrees() ); }
	Degree& operator -= ( const Degree& d ) { mDeg -= d.mDeg; return *this; }
	Degree& operator -= ( const Radian& r ) { mDeg -= r.ValueDegrees(); return *this; }
	Degree operator * ( REAL f ) const { return Degree ( mDeg * f ); }
	Degree operator * ( const Degree& f ) const { return Degree ( mDeg * f.mDeg ); }
	Degree& operator *= ( REAL f ) { mDeg *= f; return *this; }
	Degree operator / ( REAL f ) const { return Degree ( mDeg / f ); }
	Degree& operator /= ( REAL f ) { mDeg /= f; return *this; }

	bool operator <  ( const Degree& d ) const { return mDeg <  d.mDeg; }
	bool operator <= ( const Degree& d ) const { return mDeg <= d.mDeg; }
	bool operator == ( const Degree& d ) const { return mDeg == d.mDeg; }
	bool operator != ( const Degree& d ) const { return mDeg != d.mDeg; }
	bool operator >= ( const Degree& d ) const { return mDeg >= d.mDeg; }
	bool operator >  ( const Degree& d ) const { return mDeg >  d.mDeg; }

	inline _CommonExport friend std::ostream& operator <<
		( std::ostream& o, const Degree& v )
	{
		o << "Degree(" << v.valueDegrees() << ")";
		return o;
	}
};


// these functions could not be defined within the class definition of class
// Radian because they required class Degree to be defined
inline Radian::Radian ( const Degree& d ) : mRad(d.valueRadians()) 
{
}

inline Radian& Radian::operator = ( const Degree& d ) {
	mRad = d.valueRadians(); return *this;
}

inline Radian Radian::operator + ( const Degree& d ) const 
{
	return Radian ( mRad + d.valueRadians() );
}

inline Radian& Radian::operator += ( const Degree& d ) 
{
	mRad += d.valueRadians();
	return *this;
}

inline Radian Radian::operator - ( const Degree& d ) const 
{
	return Radian ( mRad - d.valueRadians() );
}

inline Radian& Radian::operator -= ( const Degree& d ) 
{
	mRad -= d.valueRadians();
	return *this;
}

_COMMON_NS_END