#pragma once

#include "matrix.h"
#include "Vector3.h"

_COMMON_NS_BEGIN

/** A 3D box aligned with the x/y/z axes.
	@remarks
	This class represents a simple box which is aligned with the
	axes. Internally it only stores 2 points as the extremeties of
	the box, one which is the minima of all 3 axes, and the other
	which is the maxima of all 3 axes. This class is typically used
	for an axis-aligned bounding box (AABB) for collision and
	visibility determination.
*/
class _CommonExport AxisAlignedBox
{
public:
	enum Extent
	{
		EXTENT_NULL,
		EXTENT_FINITE,
		EXTENT_INFINITE
	};
protected:

	Vector3 
		_vMinimum;
	Vector3 
		_vMaximum;
	Extent 
		_extent;
	mutable Vector3* 
		_pCorners;

public:
	/*
	   1-----2
	  /|    /|
	 / |   / |
	5-----4  |
	|  0--|--3
	| /   | /
	|/    |/
	6-----7
	*/
	typedef enum {
		FAR_LEFT_BOTTOM = 0,
		FAR_LEFT_TOP = 1,
		FAR_RIGHT_TOP = 2,
		FAR_RIGHT_BOTTOM = 3,
		NEAR_RIGHT_BOTTOM = 7,
		NEAR_LEFT_BOTTOM = 6,
		NEAR_LEFT_TOP = 5,
		NEAR_RIGHT_TOP = 4
	} CornerEnum;

	inline AxisAlignedBox() : _vMinimum(Vector3::ZERO), _vMaximum(Vector3::UNIT_SCALE), _pCorners(0)
	{
		// Default to a null box 
		SetMinimum( -0.5, -0.5, -0.5 );
		SetMaximum( 0.5, 0.5, 0.5 );
		_extent = EXTENT_NULL;
	}
	inline AxisAlignedBox(Extent e) : _vMinimum(Vector3::ZERO), _vMaximum(Vector3::UNIT_SCALE), _pCorners(0)
	{
		SetMinimum( -0.5, -0.5, -0.5 );
		SetMaximum( 0.5, 0.5, 0.5 );
		_extent = e;
	}

	inline AxisAlignedBox(const AxisAlignedBox & rkBox) : _vMinimum(Vector3::ZERO), _vMaximum(Vector3::UNIT_SCALE), _pCorners(0)

	{
		if (rkBox.IsNull())
			SetNull();
		else if (rkBox.IsInfinite())
			SetInfinite();
		else
			SetExtents( rkBox._vMinimum, rkBox._vMaximum );
	}

	inline AxisAlignedBox( const Vector3& min, const Vector3& max ) : _vMinimum(Vector3::ZERO), _vMaximum(Vector3::UNIT_SCALE), _pCorners(0)
	{
		SetExtents( min, max );
	}

	inline AxisAlignedBox(
		Real mx, Real my, Real mz,
		Real Mx, Real My, Real Mz ) : _vMinimum(Vector3::ZERO), _vMaximum(Vector3::UNIT_SCALE), _pCorners(0)
	{
		SetExtents( mx, my, mz, Mx, My, Mz );
	}

	AxisAlignedBox& operator=(const AxisAlignedBox& rhs)
	{
		// Specifically override to avoid copying mpCorners
		if (rhs.IsNull())
			SetNull();
		else if (rhs.IsInfinite())
			SetInfinite();
		else
			SetExtents(rhs._vMinimum, rhs._vMaximum);

		return *this;
	}

	~AxisAlignedBox()
	{
		if (_pCorners)
			XFREE(_pCorners);
	}


public:
	/** Gets the minimum corner of the box.
	*/
	inline const Vector3& GetMinimum(void) const
	{ 
		return _vMinimum; 
	}

	/** Gets a modifiable version of the minimum
	corner of the box.
	*/
	inline Vector3& GetMinimum(void)
	{ 
		return _vMinimum; 
	}

	/** Gets the maximum corner of the box.
	*/
	inline const Vector3& GetMaximum(void) const
	{ 
		return _vMaximum;
	}

	/** Gets a modifiable version of the maximum
	corner of the box.
	*/
	inline Vector3& GetMaximum(void)
	{ 
		return _vMaximum;
	}


	/** Sets the minimum corner of the box.
	*/
	inline void SetMinimum( const Vector3& vec )
	{
		_extent = EXTENT_FINITE;
		_vMinimum = vec;
	}

	inline void SetMinimum( Real x, Real y, Real z )
	{
		_extent = EXTENT_FINITE;
		_vMinimum.x = x;
		_vMinimum.y = y;
		_vMinimum.z = z;
	}

	/** Changes one of the components of the minimum corner of the box
	used to resize only one dimension of the box
	*/
	inline void SetMinimumX(Real x)
	{
		_vMinimum.x = x;
	}

	inline void SetMinimumY(Real y)
	{
		_vMinimum.y = y;
	}

	inline void SetMinimumZ(Real z)
	{
		_vMinimum.z = z;
	}

	/** Sets the maximum corner of the box.
	*/
	inline void SetMaximum( const Vector3& vec )
	{
		_extent = EXTENT_FINITE;
		_vMaximum = vec;
	}

	inline void SetMaximum( Real x, Real y, Real z )
	{
		_extent = EXTENT_FINITE;
		_vMaximum.x = x;
		_vMaximum.y = y;
		_vMaximum.z = z;
	}

	/** Changes one of the components of the maximum corner of the box
	used to resize only one dimension of the box
	*/
	inline void SetMaximumX( Real x )
	{
		_vMaximum.x = x;
	}

	inline void SetMaximumY( Real y )
	{
		_vMaximum.y = y;
	}

	inline void SetMaximumZ( Real z )
	{
		_vMaximum.z = z;
	}

	/** Sets both minimum and maximum extents at once.
	*/
	inline void SetExtents( const Vector3& min, const Vector3& max )
	{
		assert( (min.x <= max.x && min.y <= max.y && min.z <= max.z) &&
			"The minimum corner of the box must be less than or equal to maximum corner" );

		_extent = EXTENT_FINITE;
		_vMinimum = min;
		_vMaximum = max;
	}

	inline void SetExtents(
		Real mx, Real my, Real mz,
		Real Mx, Real My, Real Mz )
	{
		assert( (mx <= Mx && my <= My && mz <= Mz) &&
			"The minimum corner of the box must be less than or equal to maximum corner" );

		_extent = EXTENT_FINITE;

		_vMinimum.x = mx;
		_vMinimum.y = my;
		_vMinimum.z = mz;

		_vMaximum.x = Mx;
		_vMaximum.y = My;
		_vMaximum.z = Mz;

	}

	/** Returns a pointer to an array of 8 corner points, useful for
	collision vs. non-aligned objects.
	@remarks
	If the order of these corners is important, they are as
	follows: The 4 points of the minimum Z face (note that
	because Ogre uses right-handed coordinates, the minimum Z is
	at the 'back' of the box) starting with the minimum point of
	all, then anticlockwise around this face (if you are looking
	onto the face from outside the box). Then the 4 points of the
	maximum Z face, starting with maximum point of all, then
	anticlockwise around this face (looking onto the face from
	outside the box). Like this:
	<pre>
	  1 -----2
	 /|     /|
	/ |    / |
	5-----4  |
	|  0--|--3
	| /   | /
	|/    |/
	6-----7
	</pre>
	@remarks as this implementation uses a static member, make sure to use your own copy !
	*/
	inline const Vector3* GetAllCorners(void) const
	{
		assert( (_extent == EXTENT_FINITE) && "Can't get corners of a null or infinite AAB" );

		// The order of these items is, using right-handed co-ordinates:
		// Minimum Z face, starting with Min(all), then anticlockwise
		//   around face (looking onto the face)
		// Maximum Z face, starting with Max(all), then anticlockwise
		//   around face (looking onto the face)
		// Only for optimization/compatibility.
		if (!_pCorners)
			_pCorners = (Vector3*)XALLOCA(sizeof(Vector3) * 8);

		_pCorners[0] = _vMinimum;
		_pCorners[1].x = _vMinimum.x; _pCorners[1].y = _vMaximum.y; _pCorners[1].z = _vMinimum.z;
		_pCorners[2].x = _vMaximum.x; _pCorners[2].y = _vMaximum.y; _pCorners[2].z = _vMinimum.z;
		_pCorners[3].x = _vMaximum.x; _pCorners[3].y = _vMinimum.y; _pCorners[3].z = _vMinimum.z;            

		_pCorners[4] = _vMaximum;
		_pCorners[5].x = _vMinimum.x; _pCorners[5].y = _vMaximum.y; _pCorners[5].z = _vMaximum.z;
		_pCorners[6].x = _vMinimum.x; _pCorners[6].y = _vMinimum.y; _pCorners[6].z = _vMaximum.z;
		_pCorners[7].x = _vMaximum.x; _pCorners[7].y = _vMinimum.y; _pCorners[7].z = _vMaximum.z;

		return _pCorners;
	}

	/** gets the position of one of the corners
	*/
	Vector3 GetCorner(CornerEnum cornerToGet) const
	{
		switch(cornerToGet)
		{
		case FAR_LEFT_BOTTOM:
			return _vMinimum;
		case FAR_LEFT_TOP:
			return Vector3(_vMinimum.x, _vMaximum.y, _vMinimum.z);
		case FAR_RIGHT_TOP:
			return Vector3(_vMaximum.x, _vMaximum.y, _vMinimum.z);
		case FAR_RIGHT_BOTTOM:
			return Vector3(_vMaximum.x, _vMinimum.y, _vMinimum.z);
		case NEAR_RIGHT_BOTTOM:
			return Vector3(_vMaximum.x, _vMinimum.y, _vMaximum.z);
		case NEAR_LEFT_BOTTOM:
			return Vector3(_vMinimum.x, _vMinimum.y, _vMaximum.z);
		case NEAR_LEFT_TOP:
			return Vector3(_vMinimum.x, _vMaximum.y, _vMaximum.z);
		case NEAR_RIGHT_TOP:
			return _vMaximum;
		default:
			return Vector3();
		}
	}

	_CommonExport friend std::ostream& operator<<( std::ostream& o, const AxisAlignedBox aab )
	{
		XASSERT(0);
	/*
	switch (aab.mExtent)
		{
		case EXTENT_NULL:
			o << "AxisAlignedBox(null)";
			return o;

		case EXTENT_FINITE:
			o << "AxisAlignedBox(min=" << aab.mMinimum << ", max=" << aab.mMaximum << ")";
			return o;

		case EXTENT_INFINITE:
			o << "AxisAlignedBox(infinite)";
			return o;

		default: // shut up compiler
			assert( false && "Never reached" );
			return o;
		}
*/
		return o;
	}

	/** Merges the passed in box into the current box. The result is the
	box which encompasses both.
	*/
	void Merge( const AxisAlignedBox& rhs )
	{
		// Do nothing if rhs null, or this is infinite
		if ((rhs._extent == EXTENT_NULL) || (_extent == EXTENT_INFINITE))
		{
			return;
		}
		// Otherwise if rhs is infinite, make this infinite, too
		else if (rhs._extent == EXTENT_INFINITE)
		{
			_extent = EXTENT_INFINITE;
		}
		// Otherwise if current null, just take rhs
		else if (_extent == EXTENT_NULL)
		{
			SetExtents(rhs._vMinimum, rhs._vMaximum);
		}
		// Otherwise Merge
		else
		{
			Vector3 min = _vMinimum;
			Vector3 max = _vMaximum;
			max.MakeCeil(rhs._vMaximum);
			min.MakeFloor(rhs._vMinimum);

			SetExtents(min, max);
		}

	}

	/** Extends the box to encompass the specified point (if needed).
	*/
	inline void Merge( const Vector3& point )
	{
		switch (_extent)
		{
		case EXTENT_NULL: // if null, use this point
			SetExtents(point, point);
			return;

		case EXTENT_FINITE:
			_vMaximum.MakeCeil(point);
			_vMinimum.MakeFloor(point);
			return;

		case EXTENT_INFINITE: // if infinite, makes no difference
			return;
		}

		assert( false && "Never reached" );
	}

	/** Transforms the box according to the matrix supplied.
	@remarks
	By calling this method you get the axis-aligned box which
	surrounds the transformed version of this box. Therefore each
	corner of the box is transformed by the matrix, then the
	extents are mapped back onto the axes to produce another
	AABB. Useful when you have a local AABB for an object which
	is then transformed.
	*/
	inline void Transform( const Matrix4& matrix )
	{
/*
		// Do nothing if current null or infinite
		if( _extent != EXTENT_FINITE )
			return;

		Vector3 oldMin, oldMax, currentCorner;

		// Getting the old values so that we can use the existing Merge method.
		oldMin = _vMinimum;
		oldMax = _vMaximum;

		// reset
		SetNull();

		// We sequentially compute the corners in the following order :
		// 0, 6, 5, 1, 2, 4 ,7 , 3
		// This sequence allows us to only change one member at a time to get at all corners.

		// For each one, we transform it using the matrix
		// Which gives the resulting point and Merge the resulting point.

		// First corner 
		// min min min
		currentCorner = oldMin;
		Merge( matrix * currentCorner );

		// min,min,max
		currentCorner.z = oldMax.z;
		Merge( matrix * currentCorner );

		// min max max
		currentCorner.y = oldMax.y;
		Merge( matrix * currentCorner );

		// min max min
		currentCorner.z = oldMin.z;
		Merge( matrix * currentCorner );

		// max max min
		currentCorner.x = oldMax.x;
		Merge( matrix * currentCorner );

		// max max max
		currentCorner.z = oldMax.z;
		Merge( matrix * currentCorner );

		// max min max
		currentCorner.y = oldMin.y;
		Merge( matrix * currentCorner );

		// max min min
		currentCorner.z = oldMin.z;
		Merge( matrix * currentCorner ); 
*/
	}

	/** Transforms the box according to the affine matrix supplied.
	@remarks
	By calling this method you get the axis-aligned box which
	surrounds the transformed version of this box. Therefore each
	corner of the box is transformed by the matrix, then the
	extents are mapped back onto the axes to produce another
	AABB. Useful when you have a local AABB for an object which
	is then transformed.
	@note
	The matrix must be an affine matrix. @see Matrix4::IsAffine.
	*/
	void TransformAffine(const Matrix4& m)
	{
		assert(m.IsAffine());

		// Do nothing if current null or infinite
		if ( _extent != EXTENT_FINITE )
			return;

		Vector3 centre = GetCenter();
		Vector3 halfSize = GetHalfSize();

		Vector3 newCentre = m.TransformAffine(centre);
		Vector3 newHalfSize(
			Math::Abs(m[0][0]) * halfSize.x + Math::Abs(m[0][1]) * halfSize.y + Math::Abs(m[0][2]) * halfSize.z, 
			Math::Abs(m[1][0]) * halfSize.x + Math::Abs(m[1][1]) * halfSize.y + Math::Abs(m[1][2]) * halfSize.z,
			Math::Abs(m[2][0]) * halfSize.x + Math::Abs(m[2][1]) * halfSize.y + Math::Abs(m[2][2]) * halfSize.z);

		SetExtents(newCentre - newHalfSize, newCentre + newHalfSize);
	}

	/** Sets the box to a 'null' value i.e. not a box.
	*/
	inline void SetNull()
	{
		_extent = EXTENT_NULL;
	}

	/** Returns true if the box is null i.e. empty.
	*/
	inline bool IsNull(void) const
	{
		return (_extent == EXTENT_NULL);
	}

	/** Returns true if the box is finite.
	*/
	bool IsFinite(void) const
	{
		return (_extent == EXTENT_FINITE);
	}

	/** Sets the box to 'infinite'
	*/
	inline void SetInfinite()
	{
		_extent = EXTENT_INFINITE;
	}

	/** Returns true if the box is infinite.
	*/
	bool IsInfinite(void) const
	{
		return (_extent == EXTENT_INFINITE);
	}

	/** Returns whether or not this box intersects another. */
	inline bool Intersects(const AxisAlignedBox& b2) const
	{
		// Early-fail for nulls
		if (this->IsNull() || b2.IsNull())
			return false;

		// Early-success for infinites
		if (this->IsInfinite() || b2.IsInfinite())
			return true;

		// Use up to 6 separating planes
		if (_vMaximum.x < b2._vMinimum.x)
			return false;
		if (_vMaximum.y < b2._vMinimum.y)
			return false;
		if (_vMaximum.z < b2._vMinimum.z)
			return false;

		if (_vMinimum.x > b2._vMaximum.x)
			return false;
		if (_vMinimum.y > b2._vMaximum.y)
			return false;
		if (_vMinimum.z > b2._vMaximum.z)
			return false;

		// otherwise, must be intersecting
		return true;

	}

	/// Calculate the area of intersection of this box and another
	inline AxisAlignedBox Intersection(const AxisAlignedBox& b2) const
	{
		if (this->IsNull() || b2.IsNull())
		{
			return AxisAlignedBox();
		}
		else if (this->IsInfinite())
		{
			return b2;
		}
		else if (b2.IsInfinite())
		{
			return *this;
		}

		Vector3 intMin = _vMinimum;
		Vector3 intMax = _vMaximum;

		intMin.MakeCeil(b2.GetMinimum());
		intMax.MakeFloor(b2.GetMaximum());

		// Check intersection isn't null
		if (intMin.x < intMax.x &&
			intMin.y < intMax.y &&
			intMin.z < intMax.z)
		{
			return AxisAlignedBox(intMin, intMax);
		}

		return AxisAlignedBox();
	}

	/// Calculate the volume of this box
	Real Volume(void) const
	{
		switch (_extent)
		{
		case EXTENT_NULL:
			return 0.0f;

		case EXTENT_FINITE:
			{
				Vector3 diff = _vMaximum - _vMinimum;
				return diff.x * diff.y * diff.z;
			}

		case EXTENT_INFINITE:
			return Math::POS_INFINITY;

		default: // shut up compiler
			assert( false && "Never reached" );
			return 0.0f;
		}
	}

	/** Scales the AABB by the vector given. */
	inline void Scale(const Vector3& s)
	{
		// Do nothing if current null or infinite
		if (_extent != EXTENT_FINITE)
			return;

		// NB assumes centered on origin
		Vector3 min = _vMinimum * s;
		Vector3 max = _vMaximum * s;
		SetExtents(min, max);
	}

/*
	//...
	/ ** Tests whether this box intersects a sphere. * /
	bool intersects(const Sphere& s) const
	{
		return Math::intersects(s, *this); 
	}
	/ ** Tests whether this box intersects a plane. * /
	bool intersects(const Plane& p) const
	{
		return Math::intersects(p, *this);
	}*/

	/** Tests whether the vector point is within this box. */
	bool Intersects(const Vector3& v) const
	{
		switch (_extent)
		{
		case EXTENT_NULL:
			return false;

		case EXTENT_FINITE:
			return(v.x >= _vMinimum.x  &&  v.x <= _vMaximum.x  && 
				v.y >= _vMinimum.y  &&  v.y <= _vMaximum.y  && 
				v.z >= _vMinimum.z  &&  v.z <= _vMaximum.z);

		case EXTENT_INFINITE:
			return true;

		default: // shut up compiler
			assert( false && "Never reached" );
			return false;
		}
	}
	/// Gets the centre of the box
	Vector3 GetCenter(void) const
	{
		assert( (_extent == EXTENT_FINITE) && "Can't get center of a null or infinite AAB" );

		return Vector3(
			(_vMaximum.x + _vMinimum.x) * 0.5f,
			(_vMaximum.y + _vMinimum.y) * 0.5f,
			(_vMaximum.z + _vMinimum.z) * 0.5f);
	}
	/// Gets the size of the box
	Vector3 GetSize(void) const
	{
		switch (_extent)
		{
		case EXTENT_NULL:
			return Vector3::ZERO;

		case EXTENT_FINITE:
			return _vMaximum - _vMinimum;

		case EXTENT_INFINITE:
			return Vector3(
				Math::POS_INFINITY,
				Math::POS_INFINITY,
				Math::POS_INFINITY);

		default: // shut up compiler
			assert( false && "Never reached" );
			return Vector3::ZERO;
		}
	}
	/// Gets the half-size of the box
	Vector3 GetHalfSize(void) const
	{
		switch (_extent)
		{
		case EXTENT_NULL:
			return Vector3::ZERO;

		case EXTENT_FINITE:
			return (_vMaximum - _vMinimum) * 0.5f;

		case EXTENT_INFINITE:
			return Vector3(
				Math::POS_INFINITY,
				Math::POS_INFINITY,
				Math::POS_INFINITY);

		default: // shut up compiler
			assert( false && "Never reached" );
			return Vector3::ZERO;
		}
	}

	/** Tests whether the given point contained by this box.
	*/
	bool Contains(const Vector3& v) const
	{
		if (IsNull())
			return false;
		if (IsInfinite())
			return true;

		return _vMinimum.x <= v.x && v.x <= _vMaximum.x &&
			_vMinimum.y <= v.y && v.y <= _vMaximum.y &&
			_vMinimum.z <= v.z && v.z <= _vMaximum.z;
	}

	/** Tests whether another box contained by this box.
	*/
	bool Contains(const AxisAlignedBox& other) const
	{
		if (other.IsNull() || this->IsInfinite())
			return true;

		if (this->IsNull() || other.IsInfinite())
			return false;

		return this->_vMinimum.x <= other._vMinimum.x &&
			this->_vMinimum.y <= other._vMinimum.y &&
			this->_vMinimum.z <= other._vMinimum.z &&
			other._vMaximum.x <= this->_vMaximum.x &&
			other._vMaximum.y <= this->_vMaximum.y &&
			other._vMaximum.z <= this->_vMaximum.z;
	}

	/** Tests 2 boxes for equality.
	*/
	bool operator== (const AxisAlignedBox& rhs) const
	{
		if (this->_extent != rhs._extent)
			return false;

		if (!this->IsFinite())
			return true;

		return this->_vMinimum == rhs._vMinimum &&
			this->_vMaximum == rhs._vMaximum;
	}

	/** Tests 2 boxes for inequality.
	*/
	bool operator!= (const AxisAlignedBox& rhs) const
	{
		return !(*this == rhs);
	}

	// special values
	static const AxisAlignedBox BOX_NULL;
	static const AxisAlignedBox BOX_INFINITE;


};



_COMMON_NS_END