#pragma once

#include "common/core/IModule.h"

_COMMON_NS_BEGIN

class _CommonExport CommonModule : implements IModule
{
public:
	CommonModule();
	virtual ~CommonModule();

public:
	virtual void
		Init(const Event* pEvent);
	virtual void 
		Tick(const Event* pEvent);
	virtual void 
		Uninit(const Event* pEvent);


protected:
	int 
		__commonLibState; // 0 : uninit, 1: inited, -1: destroyed

};

_COMMON_NS_END