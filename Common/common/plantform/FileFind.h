#pragma once

_COMMON_NS_BEGIN

class _CommonExport FileFind
{
public:
	FileFind();
	virtual ~FileFind();


public:
	virtual BOOL 
		FindFile(const String& strFileName, DWORD dwUnused = 0);
	virtual BOOL 
		FindNextFile();
	virtual void 
		Close();


	virtual const String& 
		GetFileName();

/*
	ULONGLONG GetLength() const;
	virtual String GetFilePath() const;
	virtual String GetFileTitle() const;
	virtual String GetFileURL() const;
	virtual String GetRoot() const;
*/

protected:
	String
		_strRootPath;
	String
		_strFileName;


private:
	HANDLE 
		__hFindHandle;
	WIN32_FIND_DATA
		__findData;


};


_COMMON_NS_END