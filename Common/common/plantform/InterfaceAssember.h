/*
	通过InterfaceAssember，我们可以将分布在基类、成员对象中的方法，组装成一个接口
	通过ComplexInterfaceAssember，我们可以将多个用InterfaceAssember组装的接口再次组装形成一个复合接口。

	这么做的好处是，如果一个接口的具体实现被封装了很多层，通过该方式，我们可以直接绑定接口到“实现”，
	不用一层一层的包，从而减少函数调用。另外，还可以方便的实现Facade设计模式
	坏处是，产生了平台/编译器依赖性。所以除非性能关键的地方，尽量不要用这两个类。
	最初写这两个类的目的，仅仅是试验下thunk和探索下C++接口的内存（对象）模型。

	具体示例，请参考TestInterfaceAssember.h和TestInterfaceAssember.cpp

	备注：目前接口组装只实现了Windows下的版本，并且当前的实现是依赖于VC的编译器实现的。要实现跨平台/编译器，
	必须非常清楚指定平台、C++对象模型的特性，从而重写这两个类（想必不是一件容易的事）。所以，如果有跨平台/编译器
	的需求，尽量不要用这两个类。
		
*/

#pragma once

#include "Common/plantform/Thunk.h"


_COMMON_NS_BEGIN

#define INTERFACE_BEGIN(interfaceName, methodCount)	\
	InterfaceAssember _##interfaceName##assember;			\
	virtual interfaceName* Get##interfaceName()					\
	{		\
		InterfaceAssember& assember = _##interfaceName##assember;	\
		if(*(DWORD*)assember.GetInterfacePtr()) return (interfaceName*)assember.GetInterfacePtr();	\
		assember.Init(methodCount);				\
		int iMethodIndex = 0;
//---
#define INTERFACE_OVERRIDE_BEGIN(interfaceName, bassCls)		\
	virtual interfaceName* Get##interfaceName()					\
	{		\
		InterfaceAssember& assember = _##interfaceName##assember;	\
		if(*(DWORD*)assember.GetInterfacePtr()) return (interfaceName*)assember.GetInterfacePtr();	\
		bassCls::Get##interfaceName();


//---
#define METHOD(base, methodName)		\
	assember.BindMethod(iMethodIndex++, (base*)this, Func2VoidPtr(&base::methodName)); 

//---
#define METHOD_FROM_MEMBER(pMemberPtr, memberClass, methodName)		\
	assember.BindMethod(iMethodIndex++, pMemberPtr, Func2VoidPtr(&memberClass::methodName)); 

//---
#define INTERFACE_END(interfaceName)	\
	XASSERT(assember.Check());				\
	return (interfaceName*)assember.GetInterfacePtr(); }


class _CommonExport InterfaceAssember
{
public:
	InterfaceAssember()
	{
		_nFuncThunkLength = 0;
		_pInterface = NULL;
	}

	~InterfaceAssember()
	{
		_releaseThunkData();
	}

	
public:
	void 
		Init(int nMethodCount);

	// After reset, need bind method again (no need call Init again)
	void 
		ClearBindData();
	void 
		BindMethod(int index, void* pThis, void* pMemberFunc);

	// Fetch the binded function in the designated index.
	template<typename MemberFuncPtr> void 
		GetMethodPtr(int index, MemberFuncPtr& pMemberFunc)
	{
		// Make sure Init is called and at least one method is binded with BindMethod.
		XASSERT(index < _funcThunkArray.size() - 1);
		CopyAnyPointer(pMemberFunc, _funcThunkArray[index]);
	}

	INLINE void* 
		GetInterfacePtr(){return &_pInterface;}

	INLINE DWORD 
		GetThunkFuncAddress(int index){return *(DWORD*)_funcThunkArray[index];}

	bool 
		Check();

	InterfaceAssember& 
		operator= (InterfaceAssember& src);


protected:
	int 
		_nFuncThunkLength;
	vector<BYTE*>
		_funcThunkArray; // The element number is the Interface's method count.
	vector<void*>
		_pThisPtrArray;
	vector<void*>
		_bindedFuncArray;
	void* 
		_pInterface;


protected:
	void 
		_releaseThunkData();

};

////////////////////////////////////////// ComplexInterfaceAssember ////////////////////////////////

#define COMPLEX_INTERFACE_BEGIN(interfaceName, subInterfaceCount)		\
	ComplexInterfaceAssember _##interfaceName##ComplexIAssember;			\
	virtual interfaceName* Get##interfaceName()		\
	{		\
	ComplexInterfaceAssember& complexAssember = _##interfaceName##ComplexIAssember;	\
	if(complexAssember.GetInterfacePtr()) return (interfaceName*) complexAssember.GetInterfacePtr();	\
	complexAssember.Init(subInterfaceCount);

#define BIND_INTERFACE(iIndex, interfaceName)		\
	complexAssember.BindInterface(iIndex, Get##interfaceName());

#define COMPLEX_INTERFACE_END(interfaceName)   return (interfaceName*)complexAssember.GetInterfacePtr(); }


class _CommonExport ComplexInterfaceAssember
{
public:
	ComplexInterfaceAssember()
	{
		_interface = NULL;
	}

public:
	vector<void*>
		_interfaceVTablePtrs; // The element number is the Interface count.

	void* 
		_interface;

public:
	void Init(int nInterfaceCount)
	{
		XASSERT(nInterfaceCount > 0);

		_interfaceVTablePtrs.resize(nInterfaceCount);
		_interface = &_interfaceVTablePtrs[0];
	}

	void BindInterface(int iIndex, void* pInterface)
	{
		void*& pFuncThunk = _interfaceVTablePtrs[iIndex];
		XASSERT(pFuncThunk == NULL);

		pFuncThunk = (void*)*(DWORD*)pInterface;
	}

	INLINE void* GetInterfacePtr()
	{
		return _interface;
	}

};

_COMMON_NS_END