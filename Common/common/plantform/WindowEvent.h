#pragma once

#include "common/core/Event.h"

_COMMON_NS_BEGIN

class Window;

/*
	Encapsulate operations related to Window.
*/
class _CommonExport WindowEvent : public Event
{
public:
	/*
		Window messages that can be treated as event will has the corresponding WindowEventType.
	*/
	enum WindowEventType
	{
		WINDOW_EVENT_BASE = 300,

		WINDOW_CREATE,
		WINDOW_DESTROY,

		WINDOW_SIZE,
		WINDOW_MINIMIZED,
		WINDOW_ACTIVATE,
		WINDOW_COMMAND,

		WINDOW_EVENT_END,
	};

	static bool IsValidEvent(int nType)
	{
		return nType > WINDOW_EVENT_BASE && nType < WINDOW_EVENT_END;
	}

public:
	WindowEvent(int type);
	WindowEvent() : Event(WINDOW_EVENT_BASE) {};
	virtual ~WindowEvent(void);


public:
	Window* pWindow; // Window that dispatch this event.
	DWORD wParam;
	DWORD lParam;

};

_COMMON_NS_END