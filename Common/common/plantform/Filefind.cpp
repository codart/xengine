#include "stdafx.h"
#include "FileFind.h"

_COMMON_NS_BEGIN

FileFind::FileFind()
{
	__hFindHandle = NULL;
	memset(&__findData, 0, sizeof(__findData));
}

FileFind::~FileFind()
{
	memset(&__findData, 0, sizeof(__findData));
	Close();
}

BOOL FileFind::FindFile(const String& strFileName, DWORD dwUnused)
{
	Close();

	//wcsncpy_s(__findData.cFileName, strFileName.c_str(), MAX_PATH); 
	__hFindHandle = ::FindFirstFile(strFileName.c_str(), &__findData);

	if (__hFindHandle == INVALID_HANDLE_VALUE)
	{
		DWORD dwTemp = ::GetLastError();
		Close();
		::SetLastError(dwTemp);
		return FALSE;
	}

	_strRootPath = ModuleUtil::PathFromFileName(strFileName);

	return TRUE;
}

BOOL FileFind::FindNextFile()
{
	XASSERT(__hFindHandle != NULL);

	if (__hFindHandle == NULL)
		return FALSE;

	return ::FindNextFile(__hFindHandle, &__findData);
}

void FileFind::Close()
{
	if (__hFindHandle != NULL)
	{
		if (__hFindHandle != INVALID_HANDLE_VALUE)
		{
			::FindClose(__hFindHandle);
		}
		__hFindHandle = NULL;
	}

	memset(&__findData, 0, sizeof(__findData));
}

const String& FileFind::GetFileName()
{
	_strFileName = _strRootPath + __findData.cFileName;
	return _strFileName;
}

_COMMON_NS_END




