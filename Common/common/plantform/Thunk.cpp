#include "StdAfx.h"
#include "Thunk.h"

#include "common/plantform/PlatformUtil.h"

_COMMON_NS_BEGIN

int GetFuncThunkCode(BYTE*& pFuncThunk)
{
	// Why if(0)? Because the code enclosed by this 'if' is only a template. It will not be executed!
	if(0)
	{
		__asm //__thiscall
		{
thiscall_start:	
			mov   ecx, -1;   //-1 占位符, 运行时将被替换为this指针.
			mov   eax, -2;   //-2 占位符, 运行时将被替换为成员函数地址的地址.
			jmp   eax;
thiscall_end:
		}
	}

	DWORD addressStart;
	DWORD addressEnd;

	__asm
	{
		// Move StartAddress into addre1
		mov   edx, offset thiscall_start; 
		mov   dword ptr [addressStart], edx;

		// Move EndAddress into addre1
		mov   edx, offset thiscall_end; 
		mov   dword ptr [addressEnd], edx;
	}

	int nCodeLength = addressEnd - addressStart;

	pFuncThunk =(BYTE*)PlatformUtil::MallocEXEMemory(nCodeLength);

	memset(pFuncThunk, 0, nCodeLength);
	memcpy(pFuncThunk, (void*)addressStart, nCodeLength);

	return nCodeLength;
}

void GetMemberFuncThunkCode(DWORD& addr1, DWORD& addr2, CallConvention callConvention)
{
	// Why if(0)? Because the code enclosed by this 'if' is only a template. It will not be executed!
	if(0)
	{
		__asm //__thiscall
		{
thiscall_start:	
			mov   ecx, -1;   //-1占位符, 运行时将被替换为this指针.
			mov   eax, -2;   //-2占位符,运行时将被替换为CTimer::CallBcak的地址.
			jmp   eax;
thiscall_end:
		}

		__asm //__stdcall
		{
stdcall_start:	
			push  dword ptr [esp]        ; //保存（复制）返回地址到当前栈中
			mov   dword ptr [esp+4], -1  ; //将this指针送入栈中，即原来的返回地址处
			mov   eax,  -2;
			jmp   eax                    ; //跳转至目标消息处理函数（类成员函数）
stdcall_end:
		}
	}

	if(callConvention == THIS_CALL)//this_call
	{
		__asm
		{
			// Move StartAddress into addre1
			mov   ecx, dword ptr [addr1];
			mov   edx, offset thiscall_start; 
			mov   dword ptr [ecx], edx;

			// Move EndAddress into addre1
			mov   ecx, dword ptr [addr2];
			mov   edx, offset thiscall_end; 
			mov   dword ptr [ecx], edx;
		}
	}
	else
	{
		__asm
		{
			// Move StartAddress into addre1
			mov   ecx, dword ptr [addr1];
			mov   edx, offset stdcall_start; 
			mov   dword ptr [ecx], edx;

			// Move EndAddress into addre1
			mov   ecx, dword ptr [addr2];
			mov   edx, offset stdcall_end; 
			mov   dword ptr [ecx], edx;
		}
	}
}

void ReplaceCodeBuf(BYTE *code, int len, DWORD old, DWORD x)
{
	for(int i=0; i<len-4; ++i)
	{
		if(*((DWORD *)&code[i])==old)
		{
			*((DWORD *)&code[i]) = x;
			return ;
		}
	}
}


_COMMON_NS_END