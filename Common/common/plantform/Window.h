#pragma once
#pragma warning(disable: 4312)
#pragma warning(disable: 4311)
#pragma warning(disable: 4244)

#include "Common/math/Rect.h"
#include "Common/core/EventDispatcher.h"

_COMMON_NS_BEGIN

class WindowMsgReceiver
{
public:
	virtual void 
		OnPaint(const Rect* pPaintRect) = 0;
	virtual BOOL
		OnClose() = 0;
};

/*
	Encapsulate operations related to Window.
*/
class _CommonExport Window : public EventDispatcher
{
public:
	temporary Window(DWORD_PTR hWnd) { _pMsgReceiver = NULL; __pWndProcThunk = NULL; _hWindowHandle = hWnd; }
	Window(WindowMsgReceiver* pMsgReceiver);
	virtual ~Window(void);


public:
	// Note: not thread safe now!!!
	virtual bool 
		Create(const String& strTitle, const Rect& rect);
	virtual void 
		Destroy();

	virtual void
		Attach(DWORD_PTR hWnd) { _hWindowHandle = hWnd; }
	virtual void
		ShowWindow(BOOL bShow);

	INLINE virtual int 
		GetWidth() const { return _width; }
	INLINE virtual int 
		GetHeight() const { return _height; }

	virtual DWORD_PTR
		GetWindowHandle() const {return _hWindowHandle;};


protected:
	DWORD_PTR
		_hWindowHandle;

	int
		_width;
	int
		_height;

	WindowMsgReceiver*
		_pMsgReceiver;


protected:
	ATOM 
		_registerWindowClass();
	LRESULT
		_wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


private:
	BYTE*
		__pWndProcThunk;
	int 
		__nThunkLength;


private:
	static ATOM 
		__wndClassAtom;
	static Window* 
		__pThis;
	static LRESULT CALLBACK
		__startWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

};

_COMMON_NS_END