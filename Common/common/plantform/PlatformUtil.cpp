#include "StdAfx.h"
#include "PlatformUtil.h"
#include "memory.h"
#include "common/util/Unicode.h"
#include "dbghelp.h"

_COMMON_NS_BEGIN


LPTOP_LEVEL_EXCEPTION_FILTER PlatformUtil::_pOldExceptionFilterFunc = NULL;

PlatformUtil::PlatformUtil(void)
{
}

PlatformUtil::~PlatformUtil(void)
{
}

HDYNAMICLIB PlatformUtil::LoadDynamicLib(const String& strLibName)
{
#ifdef _WIN32
	HMODULE hModule = ::LoadLibrary(strLibName.c_str());
	return (HDYNAMICLIB)hModule;
#else
	///... use dlopen() for linux
	XASSERT(0);
	HDYNAMICLIB hModule = NULL;
	return hModule;
#endif // _WIN32
}

void PlatformUtil::FreeDynamicLib(HDYNAMICLIB hLibHandle)
{
	///... use dlclose() for linux
	BOOL bRet = ::FreeLibrary((HMODULE)hLibHandle);
	if(!bRet)
	{
		XTHROWEX("Error: PlatformUtil::FreeDynamicLib failed!");
	}
}

void* PlatformUtil::GetFuncAddress(HDYNAMICLIB hLibHandle, const String& strLibName)
{
	USING_STRING_CONVERT;
	///...use dlsym() for linux
	FARPROC hFuncHandle = GetProcAddress((HMODULE)hLibHandle, StrToAStr(strLibName));
	return hFuncHandle;
}

void* PlatformUtil::MallocEXEMemory(IN size_t size)
{
	return VirtualAlloc(NULL, size, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
}

void PlatformUtil::FreeEXEMemory(IN void* pMemory, IN size_t size)
{
	VirtualFree(pMemory, size, MEM_RELEASE);
}

void PlatformUtil::EnableWriteMiniDump(BOOL bEnable)
{
	if(bEnable && !_pOldExceptionFilterFunc)
	{
		_pOldExceptionFilterFunc = SetUnhandledExceptionFilter(LPTOP_LEVEL_EXCEPTION_FILTER(UnhandledExceptionFilter));
	}
	else
	{
		if(_pOldExceptionFilterFunc)
		{
			SetUnhandledExceptionFilter(_pOldExceptionFilterFunc);
			_pOldExceptionFilterFunc = NULL;
		}
	}
}

LONG PlatformUtil::UnhandledExceptionFilter(_EXCEPTION_POINTERS *pExceptionPointers)
{
	MINIDUMP_EXCEPTION_INFORMATION  miniDumpExceptionInfo;

	miniDumpExceptionInfo.ThreadId = GetCurrentThreadId();
	miniDumpExceptionInfo.ExceptionPointers = pExceptionPointers;
	miniDumpExceptionInfo.ClientPointers = 0;

	HANDLE hDumpFile = ::CreateFileA("CrashDump.dmp", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId()
		, hDumpFile, MiniDumpNormal, &miniDumpExceptionInfo, NULL, NULL);

	CloseHandle(hDumpFile);

	return EXCEPTION_CONTINUE_SEARCH;
}

BOOL PlatformUtil::IsInHeap(const void * pObj) 
{ 
	int n; 
	if ( pObj < &n ) 
	{ 
		return TRUE; 
	} 
	return FALSE; 
} 

void* PlatformUtil::alloc16(const unsigned int bytes)
{
	byte* ptr = (byte *) malloc( bytes + 16 + 4 );
	if (!ptr) return NULL;

	byte* alignedPtr = (byte *) ( ( (int) ptr ) + 15 & ~15 );
	if ( alignedPtr - ptr < 4 ) {
		alignedPtr += 16;
	}
	*((int *)(alignedPtr - 4)) = (int) ptr;
	return (void *) alignedPtr;
}

void PlatformUtil::free16(void* ptr)
{
	free( (void *) *((int *) (( (byte *) ptr ) - 4)) );
}

void PlatformUtil::memcpyMMX(void* destination, const void* sorce, const unsigned long size_t)
{
	_asm 
	{ 
		MOV ESI, sorce 
			MOV EDI, destination 
			MOV ECX, size_t 
			MOV EDX, 64 
			SHR ECX, 6 
TOP: 
		PREFETCHNTA 64[ESI] 
		// Copy data from source(L1 cache) 
		MOVQ MM0,  0[ESI] 
		MOVQ MM1,  8[ESI] 
		MOVQ MM2, 16[ESI] 
		MOVQ MM3, 24[ESI] 
		MOVQ MM4, 32[ESI] 
		MOVQ MM5, 40[ESI] 
		MOVQ MM6, 48[ESI] 
		MOVQ MM7, 56[ESI] 


		// Save the data from MM registers to Destination 
		MOVNTQ  0[EDI], MM0 
			MOVNTQ  8[EDI], MM1 
			MOVNTQ 16[EDI], MM2 
			MOVNTQ 24[EDI], MM3 
			MOVNTQ 32[EDI], MM4 
			MOVNTQ 40[EDI], MM5 
			MOVNTQ 48[EDI], MM6 
			MOVNTQ 56[EDI], MM7 


			ADD ESI, EDX 
			ADD EDI, EDX 
			DEC ECX 
			JNZ TOP 
			EMMS 
	}
} 

void PlatformUtil::memcpy_sse2(void* dest, const void* src, const unsigned long size_t)
{

	__asm
	{
		mov esi, src;    //src pointer
		mov edi, dest;   //dest pointer

		mov ebx, size_t; //ebx is our counter 
		shr ebx, 7;      //divide by 128 (8 * 128bit registers)


loop_copy:
		prefetchnta 128[ESI]; //SSE2 prefetch
		prefetchnta 160[ESI];
		prefetchnta 192[ESI];
		prefetchnta 224[ESI];

		movdqa xmm0, 0[ESI]; //move data from src to registers
		movdqa xmm1, 16[ESI];
		movdqa xmm2, 32[ESI];
		movdqa xmm3, 48[ESI];
		movdqa xmm4, 64[ESI];
		movdqa xmm5, 80[ESI];
		movdqa xmm6, 96[ESI];
		movdqa xmm7, 112[ESI];

		movntdq 0[EDI], xmm0; //move data from registers to dest
		movntdq 16[EDI], xmm1;
		movntdq 32[EDI], xmm2;
		movntdq 48[EDI], xmm3;
		movntdq 64[EDI], xmm4;
		movntdq 80[EDI], xmm5;
		movntdq 96[EDI], xmm6;
		movntdq 112[EDI], xmm7;

		add esi, 128;
		add edi, 128;
		dec ebx;

		jnz loop_copy; //loop please
		// loop_copy_end:
	}
}

void PlatformUtil::_fast_memcpy6(void* dst, void* src, int len)
{
	_asm
	{
		push esi
			push edi
			push edx
			mov esi, [src] ; source array
			mov edi, [dst] ; destination array
			mov ecx, [len] ; number of QWORDS (8 bytes) assumes len / CACHEBLOCK is an integer
			shr ecx, 6
			mov edx, 0
			nop;lea esi, [esi+ecx*8] ; end of source
			nop;lea edi, [edi+ecx*8] ; end of destination
			neg ecx ; use a negative offset as a combo pointer-and-loop-counter

copyloop:
		movq mm0, [esi+edx*8]
		movq mm1, [esi+edx*8+8]
		movq mm2, [esi+edx*8+16]
		movq mm3, [esi+edx*8+24]
		movq mm4, [esi+edx*8+32]
		movq mm5, [esi+edx*8+40]
		movq mm6, [esi+edx*8+48]
		movq mm7, [esi+edx*8+56]
		movq [edi+edx*8], mm0
			movq [edi+edx*8+8], mm1
			movq [edi+edx*8+16], mm2
			movq [edi+edx*8+24], mm3
			movq [edi+edx*8+32], mm4
			movq [edi+edx*8+40], mm5
			movq [edi+edx*8+48], mm6
			movq [edi+edx*8+56], mm7
			add edx, 8
			add ecx, 1
			jnz copyloop
			emms

			pop edx
			pop edi
			pop esi
	}
}

int PlatformUtil::IsStack(char *pvAddress)
{

	HANDLE hProcess=GetCurrentProcess();
	// Each element contains a page protection
	// (i.e.: 0=reserved, PAGE_NOACCESS, PAGE_READWRITE, etc.)
	DWORD dwProtectBlock[4] = { 0 };
	int dwRgnBlocks=0,dwRgnGuardBlks=0,RgnSize=0;
	// Get address of region containing passed memory address.
	MEMORY_BASIC_INFORMATION mbi;
	bool fOk = (VirtualQueryEx(hProcess, pvAddress, &mbi, sizeof(mbi))
		== sizeof(mbi));

	if (!fOk)
		return(fOk);   // Bad memory address, return failure

	// Walk starting at the region's base address (which never changes)
	PVOID pvRgnBaseAddress = mbi.AllocationBase;

	// Walk starting at the first block in the region (changes in the loop)
	PVOID pvAddressBlk = pvRgnBaseAddress;

	// Save the memory type of the physical storage block.
	for (;;) {
		// Get info about the current block.
		fOk = (VirtualQueryEx(hProcess, pvAddressBlk, &mbi, sizeof(mbi))
			== sizeof(mbi));
		if (!fOk)
			break;   // Couldn't get the information, end loop.

		// Is this block in the same region?
		if (mbi.AllocationBase != pvRgnBaseAddress)
			break;   // Found a block in the next region; end loop.

		// We have a block contained in the region.

		// The following if statement is for detecting stacks in Windows 98.
		// A Windows 98 stack region's last 4 blocks look like this:
		// reserved block, no access block, read-write block, reserved block
		if (dwRgnBlocks < 4) {
			// 0th through 3rd block, remember the block's protection
			dwProtectBlock[dwRgnBlocks] =
				(mbi.State == MEM_RESERVE) ? 0 : mbi.Protect;
		} else {
			// We've seen 4 blocks in this region.
			// Shift the protection values down in the array.
			MoveMemory(&dwProtectBlock[0], &dwProtectBlock[1],
				sizeof(dwProtectBlock) - sizeof(DWORD));

			// Add the new protection value to the end of the array.
			dwProtectBlock[3] = (mbi.State == MEM_RESERVE) ? 0 : mbi.Protect;
		}

		dwRgnBlocks++;             // Add another block to the region
		RgnSize += mbi.RegionSize; // Add block's size to region size
		// If block has PAGE_GUARD attribute, add 1 to this counter
		if ((mbi.Protect & PAGE_GUARD) == PAGE_GUARD)
			dwRgnGuardBlks++;

		// Take a best guess as to the type of physical storage committed to the
		// block. This is a guess because some blocks can convert from MEM_IMAGE
		// to MEM_PRIVATE or from MEM_MAPPED to MEM_PRIVATE; MEM_PRIVATE can
		// always be overridden by MEM_IMAGE or MEM_MAPPED.

		// Get the address of the next block.
		pvAddressBlk = (PVOID) ((PBYTE) pvAddressBlk + mbi.RegionSize);
	}

	// After examining the region, check to see whether it is a thread stack
	// Windows 2000: Assume stack if region has at least 1 PAGE_GUARD block
	// Windows 9x:   Assume stack if region has at least 4 blocks with
	//               3rd block from end: reserved
	//               2nd block from end: PAGE_NOACCESS
	//               1st block from end: PAGE_READWRITE
	//               block at end: another reserved block.
	if(
		(dwRgnGuardBlks > 0)         ||
		(dwRgnBlocks >= 4)          &&
		(dwProtectBlock[0] == 0)               &&
		(dwProtectBlock[1] == PAGE_NOACCESS)  &&
		(dwProtectBlock[2] == PAGE_READWRITE) &&
		(dwProtectBlock[3] == 0))
		return TRUE; //是栈的条件好苛刻
	else
		return FALSE;
}

_COMMON_NS_END