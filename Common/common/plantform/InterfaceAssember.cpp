#include "StdAfx.h"
#include "InterfaceAssember.h"

_COMMON_NS_BEGIN

InterfaceAssember& InterfaceAssember::operator= (InterfaceAssember& src)
{
	_funcThunkArray.resize(src._funcThunkArray.size());

	for (unsigned int i=0; i< _funcThunkArray.size(); ++i)
	{
		BYTE*& pFuncThunk = _funcThunkArray[i];
		pFuncThunk = (BYTE*)PlatformUtil::MallocEXEMemory(_nFuncThunkLength);

		memcpy(pFuncThunk, src._funcThunkArray[i], _nFuncThunkLength);
	}

	_pThisPtrArray = src._pThisPtrArray;
	_bindedFuncArray = src._bindedFuncArray;

	_pInterface = &_funcThunkArray[0];
	return *this;
}

void InterfaceAssember::Init(int nMethodCount)
{
	XASSERT(nMethodCount > 0);

	_funcThunkArray.resize(nMethodCount);
	//memset(&_funcThunkArray[0], 0, sizeof(DWORD) * nMethodCount);
	_pThisPtrArray.resize(nMethodCount);
	_bindedFuncArray.resize(nMethodCount);

	_pInterface = &_funcThunkArray[0];
}

void InterfaceAssember::BindMethod(int index, void* pThis, void* pMemberFunc)
{
	BYTE*& pFuncThunk = _funcThunkArray[index];

	PlatformUtil::FreeEXEMemory(pFuncThunk, _nFuncThunkLength);

	_nFuncThunkLength = GetFuncThunkCode(pFuncThunk);

	// -1 is a placeholder for "this" pointer
	ReplaceCodeBuf(pFuncThunk, _nFuncThunkLength, -1, (DWORD)pThis);
	// -2 is a placeholder for "member-function" pointer
	ReplaceCodeBuf(pFuncThunk, _nFuncThunkLength, -2, (DWORD)pMemberFunc);
}

void InterfaceAssember::ClearBindData()
{
	_releaseThunkData();
}

bool InterfaceAssember::Check()
{
	for (unsigned int i=0; i< _funcThunkArray.size(); ++i)
	{
		BYTE*& pFuncThunk = _funcThunkArray[i];
		if(pFuncThunk == NULL)
		{
			return false;
		}
	}
	return true;
}

void InterfaceAssember::_releaseThunkData()
{
	// Release thunk data
	vector<BYTE*>::iterator it = _funcThunkArray.begin();
	vector<BYTE*>::iterator itEnd = _funcThunkArray.end();

	for ( ; it != itEnd; ++it)
	{
		BYTE*& pByte = *it;
		PlatformUtil::FreeEXEMemory(pByte, _nFuncThunkLength);
	}
}

_COMMON_NS_END