#pragma once

#include "common/core/singleton.h"

// Header files for intrinsic functions
#include <intrin.h>

_COMMON_NS_BEGIN

typedef  void*	HDYNAMICLIB;

class _CommonExport PlatformUtil //: public Singleton<PlatformUtil, AutoRelease>
{
public:
	PlatformUtil(void);
	~PlatformUtil(void);


public:
	static HDYNAMICLIB
		LoadDynamicLib(const String& strLibName);
	static void
		FreeDynamicLib(HDYNAMICLIB hLibHandle);
	static void*
		GetFuncAddress(HDYNAMICLIB hLibHandle, const String& strFuncName);


	static int 
		IsStack(char *pAddress);
	static BOOL 
		IsInHeap(const void * pObj);

	// When DEP is enabled, must use MallocEXEMemory to allocate memory for "thunk" block.
	static BOOL
		IsDEPEnable() { return ::IsProcessorFeaturePresent(PF_NX_ENABLED); }

	// Allocate memory that is executable.
	static void*
		MallocEXEMemory(IN size_t size);
	static void
		FreeEXEMemory(IN void* pMemory, IN size_t size);

	// Allocate aligned memory
	static INLINE void*
		MallocAligned(IN size_t size, IN size_t alignment) { return _mm_malloc(size, alignment); }
	static INLINE void
		FreeAligned(void* pMemory) { _mm_free(pMemory); }

	static void
		EnableWriteMiniDump(BOOL bEnable);

	static void 
		memcpyMMX(void* destination, const void* sorce, const unsigned long size_t);
	static void 
		memcpy_sse2(void* dest, const void* src, const unsigned long size_t);
	static void 
		_fast_memcpy6(void* dst, void* src, int len);


protected:
	// Allocate 16 Byte aligned memory. Only for studying.
	static void* 
		alloc16(const unsigned int bytes);
	// Deallocate memory that allocated by alloc16. Only for studying.
	static void 
		free16(void* ptr);

protected:
	static LPTOP_LEVEL_EXCEPTION_FILTER
		_pOldExceptionFilterFunc;


protected:
	static LONG 
		UnhandledExceptionFilter(_EXCEPTION_POINTERS *pExceptionPointers);

};


_COMMON_NS_END