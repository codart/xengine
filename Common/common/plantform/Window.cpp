#include "StdAfx.h"
#include "Window.h"

#include "Common/plantform/Thunk.h"
#include "Common/plantform/WindowEvent.h"

extern HINSTANCE __hInstance;

#define WND_CLS_NAME	TEXT("RenderWindow")

_COMMON_NS_BEGIN

Window* Window::__pThis = NULL;
ATOM Window::__wndClassAtom = 0;
Window::Window(WindowMsgReceiver* pMsgReceiver) : _pMsgReceiver(pMsgReceiver)
{
	_width = 0;
	_height = 0;

	_hWindowHandle = NULL;
	__pWndProcThunk = NULL;

	int nThunkCodeLen = GetFuncThunkCode(__pWndProcThunk);

	DWORD FuncAddr;
	CopyAnyPointer(FuncAddr, &Window::_wndProc);

	ReplaceCodeBuf(__pWndProcThunk, nThunkCodeLen, -1, (DWORD)((void*)this));
	ReplaceCodeBuf(__pWndProcThunk, nThunkCodeLen, -2, FuncAddr);
}

Window::~Window(void)
{
	BOOL bIsDEPEnabled = ::IsProcessorFeaturePresent(12 /*PF_NX_ENABLED*/);
	if(bIsDEPEnabled)
	{
		if(__pWndProcThunk)
		{
			::VirtualFree(__pWndProcThunk, __nThunkLength, MEM_RELEASE);
		}
	}
	else
	{
		SAFE_DELETE(__pWndProcThunk);
	}
	__pWndProcThunk = NULL;
}

void Window::ShowWindow(BOOL bShow)
{
	::ShowWindow((HWND)_hWindowHandle, bShow? SW_SHOW : SW_HIDE);
}

bool Window::Create(const String& strTitle, const Rect& rect)
{
	// Create window class
	_registerWindowClass();

	// Create window
	__pThis = this;

	AdjustWindowRect((RECT*)&rect, WS_OVERLAPPEDWINDOW, FALSE);

	HWND hWnd = ::CreateWindowEx(0, MAKEINTATOM(__wndClassAtom), strTitle.c_str(), WS_OVERLAPPEDWINDOW
				, rect.left, rect.top, rect.right - rect.left
				, rect.bottom - rect.top, NULL, NULL
				, __hInstance, 0);

	__pThis = NULL;

	return hWnd? true:false;
}

void Window::Destroy()
{
	if(_hWindowHandle)
	{
		::DestroyWindow((HWND)_hWindowHandle);
		_hWindowHandle = NULL;
	}
}


LRESULT Window::__startWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	WNDPROC pNewWndProc = (WNDPROC)__pThis->__pWndProcThunk;
	WNDPROC pOldProc = (WNDPROC)::SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)pNewWndProc);

	return pNewWndProc(hWnd, message, wParam, lParam);
}

LRESULT Window::_wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	WindowEvent e;
	e.pWindow = this;
	e.wParam = wParam;
	e.lParam = lParam;

	switch (message)
	{
	case WM_CREATE:
		{
		e.nType = WindowEvent::WINDOW_CREATE;

		Rect rect;
		GetClientRect(hWnd, rect);
		_width = rect.Width();
		_height = rect.Height();

		_hWindowHandle = (DWORD_PTR)hWnd;
		::ShowWindow(hWnd, SW_SHOW);
		UpdateWindow(hWnd);
		}
		break;

	case WM_DESTROY:
		e.nType = WindowEvent::WINDOW_DESTROY;
		break;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC tempDC = ::BeginPaint((HWND)_hWindowHandle, &ps);
			::EndPaint((HWND)_hWindowHandle, &ps);
			if(_pMsgReceiver) _pMsgReceiver->OnPaint((Rect*)&ps.rcPaint);
		}
		break;

	case WM_SIZE:
		{
			Rect clientRect;
			::GetClientRect((HWND)_hWindowHandle, clientRect);
			if(clientRect.IsRectEmpty())
			{
				// If empty, means window is minimized
				e.nType = WindowEvent::WINDOW_MINIMIZED;
			}
			else
			{
				_width = clientRect.Width();
				_height = clientRect.Height();
				e.nType = WindowEvent::WINDOW_SIZE;
			}
		}
		break;

	case WM_ACTIVATE:
		e.nType = WindowEvent::WINDOW_ACTIVATE;
		break;

	case WM_CLOSE:
		if(_pMsgReceiver) _pMsgReceiver->OnClose();
		break;

	case WM_COMMAND:
		e.nType = WindowEvent::WINDOW_COMMAND;
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	DispatchEvent(e);

	return 0;
}

ATOM Window::_registerWindowClass()
{
	if(__wndClassAtom) return __wndClassAtom;

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNCLIENT | CS_OWNDC;
	wcex.lpfnWndProc	= __startWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= __hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(0);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= WND_CLS_NAME;
	//wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	wcex.hIconSm		= NULL;

	return __wndClassAtom = RegisterClassEx(&wcex);
}

_COMMON_NS_END