#pragma once


/// Make code memory can be write.
/// The hacked memory will be restored automatically during MemoryHacker's destructor.
class MemoryHacker
{
public:
	MemoryHacker(void* pAddress, size_t nSize)
	{
		_pAddress = pAddress;
		_nSize = nSize;

		DWORD flNewProtect;
		MEMORY_BASIC_INFORMATION mbi;

		// Get the current protection attributes of the vtable                           
		VirtualQuery( (void*)pAddress, &mbi, sizeof(mbi) );

		// remove ReadOnly and ExecuteRead attributes, add on ReadWrite flag
		flNewProtect = mbi.Protect;
		flNewProtect &= ~(PAGE_READONLY | PAGE_EXECUTE_READ | PAGE_EXECUTE);
		flNewProtect |= (PAGE_READWRITE);

		///... For linux, should use mprotect() instead. See http://linux.about.com/library/cmd/blcmdl2_mprotect.htm
		if ( !VirtualProtect(   (void*)pAddress, nSize,
			flNewProtect, &_dwOldProtect) )
		{
			throw "";
		}
		_bHacked = true;
	}

	~MemoryHacker()
	{
		Restore();
	}

public:
	void Restore()
	{
		if(!_bHacked) return;
		// reset security attr
		// Put the page attributes back the way they were.
		DWORD dummy;
		VirtualProtect((void*)_pAddress, _nSize, _dwOldProtect, &dummy);
	}

protected:
	void* _pAddress;
	size_t _nSize;
	DWORD _dwOldProtect;
	BOOL _bHacked;
};

