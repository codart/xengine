#pragma once

_COMMON_NS_BEGIN

// Convert member function address to void*
template <class FromType>
void* Func2VoidPtr(FromType f)
{
	union 
	{
		FromType _f;
		void*  _t;
	}ut;

	ut._f = f;
	return ut._t;
}

// Get member function address
template <class ToType, class FromType>
void CopyAnyPointer(ToType& dest, FromType src)
{
	union 
	{
		FromType _f;
		ToType   _t;
	}ut;

	ut._f = src;

	dest = ut._t;
}

enum CallConvention
{
	THIS_CALL,
	STD_CALL,
};


int _CommonExport GetFuncThunkCode(BYTE*& pFuncThunk);

// ��������GetFuncThunkCode���档
void _CommonExport GetMemberFuncThunkCode(DWORD& addr1, DWORD& addr2, CallConvention callConvention = THIS_CALL);
void _CommonExport ReplaceCodeBuf(BYTE *code,int len, DWORD old,DWORD x);

_COMMON_NS_END