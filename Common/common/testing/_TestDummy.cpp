#include "stdafx.h"
#include "_TestDummy.h"

_COMMON_NS_BEGIN

_TestDummy::_TestDummy()
{

}

_TestDummy::~_TestDummy()
{

}

ThirdPartyObject* _TestDummy::CreateRawObject(void)
{
	return XNEW ThirdPartyObject();
}

common::SharedPtr<ThirdPartyObject> _TestDummy::CreateObjectPtr(void)
{
	return common::SharedPtr<ThirdPartyObject>(XNEW ThirdPartyObject());
}


_COMMON_NS_END