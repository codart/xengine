#pragma once

_COMMON_NS_BEGIN

class ThirdPartyObject
{
public:
	ThirdPartyObject()
	{
		a = 123;
	}

	~ThirdPartyObject()
	{
		a = 123;
	}

	virtual void
		Destroy()
	{
		XDELETE this;
	}

public:
	int a;

};

class _CommonExport _TestDummy
{
public:
	_TestDummy();
	virtual ~_TestDummy();

public:
	ThirdPartyObject* 
		CreateRawObject(void);
	common::SharedPtr<ThirdPartyObject> 
		CreateObjectPtr(void);


};


_COMMON_NS_END