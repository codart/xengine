#pragma once

#include "common/core/EventDispatcher.h"
#include "CaseParam.h"

_COMMON_NS_BEGIN

#define ADD_TEST_CASE(testCase)	\
{	\
	TestCase* pCase = new testCase;	\
	AddTestCase(#testCase, pCase, NULL);		\
}

class Tester;
class _CommonExport TestCase : public EventDispatcher
{
	friend Tester;

public:
	TestCase(void);
	virtual ~TestCase(void);

public:
	virtual void 
		Destroy();

protected:
	virtual void SetCaseParam(CaseParam* pCaseParam);
	virtual void Run();
	virtual void Update() { };
	virtual void End();


protected:
	string _dwCaseID;
	CaseParam* _pCaseParam;

};

class _CommonExport Tester
{
public:
	Tester();
	virtual ~Tester();


public:
	virtual void 
		RunCase(const string& strCaseID);
	virtual void
		Update();
	virtual void
		EndCase(const string& strCaseID);

	virtual void 
		ExecuteAllCase();
	virtual void
		DestroyAllCase();

	virtual void 
		AddTestCase(const string& strCaseID, TestCase* pCase, CaseParam* pCaseParam);


protected:
	typedef SharedPtr<TestCase> TestCasePtr;
	typedef map< string, OwnerPtr<TestCasePtr> > TestCaseMap;
	typedef TestCaseMap::iterator MapTestCaseIt;

	TestCaseMap
		_caseList;


};


_COMMON_NS_END