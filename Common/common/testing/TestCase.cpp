#include "StdAfx.h"
#include "TestCase.h"

_COMMON_NS_BEGIN

Tester::Tester()
{

}

Tester::~Tester()
{
	DestroyAllCase();
}

void Tester::DestroyAllCase()
{
	MapTestCaseIt it = _caseList.begin();
	for ( ; it != _caseList.end(); ++it)
	{
		(it->second)->End();
	}

	_caseList.clear();
}

void Tester::RunCase(const string& strCaseID)
{
	_caseList[strCaseID]->Run();
}

void Tester::EndCase(const string& strCaseID)
{
	_caseList[strCaseID]->End();
}

void Tester::ExecuteAllCase()
{
	MapTestCaseIt it = _caseList.begin();
	for ( ; it != _caseList.end(); ++it)
	{
		(it->second)->Run();
	}
}

void Tester::Update()
{
	MapTestCaseIt it = _caseList.begin();
	for ( ; it != _caseList.end(); ++it)
	{
		(it->second)->Update();
	}
}

void Tester::AddTestCase(const string& strCaseID, TestCase* pCase, CaseParam* pCaseParam)
{
	if(pCaseParam != NULL) 
	{
		pCase->SetCaseParam(pCaseParam);
	}
	_caseList[strCaseID] = pCase;
}


//////////////////////////////////////////////////////////////////////////
TestCase::TestCase(void)
{
	_pCaseParam = NULL;
}

TestCase::~TestCase(void)
{
	SAFE_DELETE(_pCaseParam);
}

void TestCase::Destroy()
{
	delete this;
}

void TestCase::Run()
{
	
}

void TestCase::End()
{

}

void TestCase::SetCaseParam(CaseParam* pCaseParam)
{
	SAFE_DELETE(_pCaseParam);
	_pCaseParam = pCaseParam;
}


_COMMON_NS_END