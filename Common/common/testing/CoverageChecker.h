#pragma once

_COMMON_NS_BEGIN

class _CommonExport CoverageChecker
{
public:
	static unsigned long long CoverMarker;
	static unsigned int CountStart;
	static unsigned int CountEnd;
};


#define COVER_START()	\
	CoverageChecker::CountStart = __COUNTER__


#define COVER_END()		\
	CoverageChecker::CountEnd = __COUNTER__

#define COVER_CHECK_BEGIN(MethodName)	\
	CoverageChecker::CoverMarker = 0;	\
	MethodName##_MarkStart();	\
	MethodName##_MarkEnd();		\
	printf("-------Check method %s's coverage...\n", #MethodName)


#define COVER_CHECK_END(MethodName)	\
	for (unsigned int i=0; i < CoverageChecker::CountEnd - CoverageChecker::CountStart - 1; ++i)	\
	{	\
		if(i >= sizeof(long long))	\
		{ printf("Error! CoverChecker support max 64 branchs in one function!\n", i+1);  break; }	\
			\
		if( (((long long)0x1 << i) & CoverageChecker::CoverMarker) == 0 )	\
		{	\
			printf("The %dth branch is not covered!\n", i+1);	\
		}	\
	}	\
	printf("----------\n");	

#define MARK_LINE()		\
	CoverageChecker::CoverMarker |= ((long long)0x1 << (__COUNTER__ - CoverageChecker::CountStart - 1))

#define COVER_REPORT()

/// For global function
#define MARK_FUNCTION_START(MethodName)	\
	void MethodName##_MarkStart()	\
	{	\
		CoverageChecker::CountStart = __COUNTER__;	\
	}


#define MARK_FUNCTION_END(MethodName)	\
	void MethodName##_MarkEnd()	\
	{	\
		CoverageChecker::CountEnd = __COUNTER__;	\
	}

/// For class method
#define COVERAGE_CHECK_DECLARE(MethodName)	\
	void MethodName##_MarkStart();		\
	void MethodName##_MarkEnd();

#define MARK_METHOD_START(ClassName, MethodName)	\
	void ClassName##::MethodName##_MarkStart()	\
	{	\
		CoverageChecker::CountStart = __COUNTER__;	\
	}

#define MARK_METHOD_END(ClassName, MethodName)	\
	void ClassName##::MethodName##_MarkEnd()	\
	{	\
		CoverageChecker::CountEnd = __COUNTER__;	\
	}


_COMMON_NS_END
