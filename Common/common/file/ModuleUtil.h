#pragma once

_COMMON_NS_BEGIN

#define BACK_SLASH				L'\\'

//...
class Archive
{
public:
protected:
private:
};

class FileArchive
{
public:
protected:
private:
};

class ZipArchive
{

};

class _CommonExport ArchiveManager
{
public:

protected:


};


class ModuleUtil
{
public:
	_CommonExport ModuleUtil(void);
	_CommonExport virtual ~ModuleUtil(void);

public:
	static void _CommonExport 
		Init(){};

	static void _CommonExport 
		Uninit();

	_CommonExport static const String& 
		GetGPUProgramScriptPath();

	_CommonExport static const String& 
		GetGPUProgramPath();

	_CommonExport static const String& 
		GetTexturePath();

	_CommonExport static const String& 
		GetMaterialPath();

	_CommonExport static const String&
		GetModelPath();

	_CommonExport static const String& 
		GetUIPath();

	_CommonExport static const String& 
		GetResourcePath();

	_CommonExport static const String& 
		GetModulePath();

	//---
	_CommonExport static const String
		PathFromFileName(const String& strFileName);

protected:
	static String 
		__strGPUProgramScriptPathBuf;
	static String
		__strGPUProgramPathBuf;
	static String 
		__strModelPathBuf;
	static String 
		_strModulePath;
	static String 
		__strTexturePathBuf;
	static String 
		__strMaterialPathBuf;
	static String 
		__strUIPathBuf;

	static String 
		__strResourcePathBuf;

};



_COMMON_NS_END