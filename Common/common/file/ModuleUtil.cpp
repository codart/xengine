#include "StdAfx.h"
#include "ModuleUtil.h"

_COMMON_NS_BEGIN

String ModuleUtil::_strModulePath = TEXT("");
String ModuleUtil::__strTexturePathBuf = TEXT("");
String ModuleUtil::__strMaterialPathBuf = TEXT("");
String ModuleUtil::__strUIPathBuf = TEXT("");
String ModuleUtil::__strModelPathBuf = TEXT("");
String ModuleUtil::__strGPUProgramScriptPathBuf = TEXT("");
String ModuleUtil::__strResourcePathBuf = TEXT("");
String ModuleUtil::__strGPUProgramPathBuf = TEXT("");


void ModuleUtil::Uninit()
{
	_strModulePath = TEXT("");
	__strTexturePathBuf = TEXT("");
	__strMaterialPathBuf = TEXT("");
	__strUIPathBuf = TEXT("");
	__strModelPathBuf = TEXT("");
	__strGPUProgramScriptPathBuf = TEXT("");
	__strResourcePathBuf = TEXT("");
	__strGPUProgramPathBuf = TEXT("");
}

ModuleUtil::ModuleUtil(void)
{
}

ModuleUtil::~ModuleUtil(void)
{
}

const String ModuleUtil::PathFromFileName(const String& strFileName)
{
	int nPos = strFileName.rfind(L'\\');

	if(nPos == -1)
	{
		nPos = strFileName.rfind(L'/');
		if(nPos == -1)
		{
			XASSERT(0 && "Incorrect file name");
		}
	}
	return strFileName.substr(0, nPos + 1);
}

//---
const String& ModuleUtil::GetGPUProgramScriptPath()
{
	if(__strGPUProgramScriptPathBuf.length()) return __strGPUProgramScriptPathBuf;

	__strGPUProgramScriptPathBuf = GetResourcePath() + TEXT("material\\scripts\\gpu_program_scripts\\");

	return __strGPUProgramScriptPathBuf;
}

const String& ModuleUtil::GetGPUProgramPath()
{
	if(__strGPUProgramPathBuf.length()) return __strGPUProgramPathBuf;

	__strGPUProgramPathBuf = GetResourcePath() + TEXT("material\\gpu_program\\");

	return __strGPUProgramPathBuf;
}

const String& ModuleUtil::GetModelPath()
{
	if(__strModelPathBuf.length()) return __strModelPathBuf;

	__strModelPathBuf = GetResourcePath() + TEXT("model\\");

	return __strModelPathBuf;
}

const String& ModuleUtil::GetTexturePath()
{
	if(__strTexturePathBuf.length() > 0) return __strTexturePathBuf;

	__strTexturePathBuf = GetResourcePath() + TEXT("material\\texture\\");

	return __strTexturePathBuf;
}

const String& ModuleUtil::GetMaterialPath()
{
	if(__strMaterialPathBuf.length() > 0) return __strMaterialPathBuf;

	__strMaterialPathBuf = GetResourcePath() + TEXT("material\\scripts\\");

	return __strMaterialPathBuf;
}

const String& ModuleUtil::GetUIPath()
{
	if(__strUIPathBuf.length() > 0) return __strUIPathBuf;

	__strUIPathBuf = GetResourcePath() + TEXT("UI\\");
		
	return __strUIPathBuf;
}

const String& ModuleUtil::GetResourcePath()
{
	if(__strResourcePathBuf.length() > 0) return __strResourcePathBuf;

	String strModulePath = GetModulePath();
	strModulePath[strModulePath.length() - 1] = ' ';
	int nPos = strModulePath.rfind('\\');
	__strResourcePathBuf = strModulePath.substr(0, nPos);
	__strResourcePathBuf += TEXT("\\resource\\");

	return __strResourcePathBuf;
}

const String& ModuleUtil::GetModulePath()
{
	TCHAR szTmpPath[MAX_PATH] = {0};

	if(_strModulePath.length() > 0) return _strModulePath;

	DWORD dwLen = GetModuleFileName(NULL, szTmpPath, MAX_PATH);
	if (dwLen > MAX_PATH)
	{
		return _strModulePath;
	}

	String sz(szTmpPath);
	int index = sz.find_last_of(BACK_SLASH);
	if (String::npos == index)
	{
		return _strModulePath;
	}

	_strModulePath = (sz.substr(0, index+1)).c_str();

	return _strModulePath;
}

_COMMON_NS_END