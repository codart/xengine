#pragma once

#include "x3d/scene/Camera.h"

_X3D_NS_BEGIN


// A camera controller controls the camera automatically by mouse/keyboard input.
// eg. first person camera/third person camera.
class CameraController
{
public:
	CameraController(void);
	virtual ~CameraController(void);

public:
	virtual void 
		Attach(CameraPtr pCamera);


protected:
	// The dest camera be controlled
	CameraPtr
		_pCamera;

};

_X3D_NS_END