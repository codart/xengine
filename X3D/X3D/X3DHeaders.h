#pragma once

#include "common/common.h"
using namespace common;

#include "common/core/ReferenceCounted.h"
#include "common/core/EventDispatcher.h"

#include "Common/general/Color.h"

#include "common/math/Vector3.h"
#include "common/math/Vector2.h"
#include "Common/math/Math.h"
#include "Common/math/matrix.h"
#include "Common/math/Quaternion.h"
#include "Common/math/Angle.h"
#include "Common/math/AxisAlignedBox.h"
#include "Common/math/Plane.h"

#include "x3d/X3DModuleEvent.h"