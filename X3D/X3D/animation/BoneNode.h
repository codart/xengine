#pragma once

#include "x3d/scene/Prefabrenderable.h"

_X3D_NS_BEGIN

class _X3DExport BoneNode : public SceneNode
{
	friend class BoneInstance;

public:
	BoneNode(const BoneNode& pBoneNode);
	BoneNode(const String& strName);
	virtual ~BoneNode();

public:
	INLINE void
		AddChild(BoneNode* pBoneNode) { Node3D::AddChild(pBoneNode); }

	INLINE BoneNode* 
		GetChildAt(UINT iIndex) { return SAFE_CAST(Node3D::GetChildAt(iIndex), BoneNode*); }
	Matrix4&
		GetOffsetTransform(Matrix4& m) const;
	void 
		SetBindingPose(void);
	INLINE const String&
		GetName() { return _strName; }


protected:
	// Make these methods protected to avoid incorrect calling.
	INLINE void
		AddChild(TreeNode* pChild) { TreeNode::AddChild(pChild); }
	INLINE TreeNode* 
		GetParent() { return TreeNode::GetParent(); }
	INLINE void 
		setParent(TreeNode* pParent) { TreeNode::setParent(pParent); }


protected:
	/// The inversed derived scale of the bone in the binding pose
	Vector3 
		_vBindDerivedInverseScale;
	/// The inversed derived orientation of the bone in the binding pose
	Quaternion 
		_vBindDerivedInverseOrientation;
	/// The inversed derived position of the bone in the binding pose
	Vector3 
		_vBindDerivedInversePosition;

};

_X3D_NS_END