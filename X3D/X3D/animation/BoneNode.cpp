#include "StdAfx.h"
#include "BoneNode.h"

_X3D_NS_BEGIN

BoneNode::BoneNode(const BoneNode& boneNode)
: SceneNode(boneNode)
{
	// Copy from boneNode
	_vBindDerivedInverseScale = boneNode._vBindDerivedInverseScale;
	_vBindDerivedInverseOrientation = boneNode._vBindDerivedInverseOrientation;
	_vBindDerivedInversePosition = boneNode._vBindDerivedInversePosition;

	_strName = boneNode._strName;
}

BoneNode::BoneNode(const String& strName) 
: _vBindDerivedInversePosition(0, 0, 0)
, _vBindDerivedInverseScale(1, 1, 1)
, SceneNode(strName)
{
	
}

BoneNode::~BoneNode(void)
{
}

void BoneNode::SetBindingPose(void)
{
	//setInitialState();

	// Save inverse derived position/scale/orientation, used for calculate offset transform later
	_vBindDerivedInversePosition = - GetDerivedPosition();
	_vBindDerivedInverseScale = Vector3::UNIT_SCALE / GetDerivedScale();
	_vBindDerivedInverseOrientation = GetDerivedOrientation().Inverse();
}


Matrix4& BoneNode::GetOffsetTransform(Matrix4& m) const
{
	// Combine scale with binding pose inverse scale,
	// NB just combine as equivalent axes, no shearing
	Vector3 locScale = GetDerivedScale() * _vBindDerivedInverseScale;

	// Combine orientation with binding pose inverse orientation
	Quaternion locRotate = _vBindDerivedInverseOrientation * GetDerivedOrientation();

	// Combine position with binding pose inverse position,
	// Note that translation is relative to scale & rotation,
	// so first reverse transform original derived position to
	// binding pose bone space, and then transform to current
	// derived bone space.
	Vector3 locTranslate = GetDerivedPosition() + (locScale * _vBindDerivedInversePosition) * locRotate;

	m.MakeTransform(locTranslate, locScale, locRotate);
	return m;
}

_X3D_NS_END

