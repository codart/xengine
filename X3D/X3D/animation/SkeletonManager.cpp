#include "StdAfx.h"
#include "SkeletonManager.h"

_X3D_NS_BEGIN

SkeletonManager::SkeletonManager(void)
{
}

SkeletonManager::~SkeletonManager(void)
{
}

SkeletonPtr SkeletonManager::AddSkeleton(const String& strSkeletonName)
{
	temporary;
	
	SkeletonPtr pNewSkeleton(XNEW Skeleton);
	_skeletonMap[strSkeletonName] = pNewSkeleton;

	pNewSkeleton->Load();

	return pNewSkeleton;
}

void SkeletonManager::unInit()
{
	_skeletonMap.clear();
}

_X3D_NS_END

