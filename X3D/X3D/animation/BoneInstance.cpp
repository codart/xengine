#include "StdAfx.h"
#include "BoneInstance.h"

#include "x3d/resource/MeshManager.h"
#include "x3d/scene/PrefabEntity.h"
#include "x3d/animation/SkeletonInstance.h"
#include "x3d/resource/FourPyramid.h"
#include "x3d/scene/SceneSpatialMgr.h"

_X3D_NS_BEGIN

BoneInstance::BoneInstance(Ptr<SkeletonInstance> pSkeletonInstance, Ptr<BoneNode> pBoneNode)
: _pSkeletonInstance(pSkeletonInstance)
, BoneNode(*pBoneNode)
{
	
}

void BoneInstance::AddChild(BoneInstance* pChildBoneNode)
{
	BoneNode::AddChild(pChildBoneNode);
}

void BoneInstance::CreateHelperEntity()
{
	ParameterList params;
	params.AddItem(TEXT("direction"), FourPyramid::DIRECTION_UP);
	params.AddItem(TEXT("width"), 1.0f);
	params.AddItem(TEXT("height"), 0.5f);

	MeshPtr pMeshPtr = MeshManager::Instance().AddPrefabMesh(TEXT("BoneJointMesh")
		, PREFAB_TYPE_FOUR_PYRAMID
		, params);

	_pBoneJointEntity = XNEW PrefabEntity(TEXT("BoneJointEntity"), pMeshPtr, _RenderSystem);

	AttachObject(_pBoneJointEntity);

	//----------
	if(this->GetChildCount() == 0) return;

	BoneInstance* pChildBoneNode = this->GetChildAt(0);

	// 因为BoneMesh 高度为0.5，所以乘以2
	float nBoneTrunkLength = pChildBoneNode->GetPosition().Length() * 2; 

	Vector3 vTemp = pChildBoneNode->GetPosition();
	vTemp.Normalise();
	Quaternion boneOrientation;
	boneOrientation.FromTwoVector(Vector3::UNIT_Y, vTemp);


	params.SetItem(TEXT("direction"), FourPyramid::DIRECTION_DOWN);

	pMeshPtr = MeshManager::Instance().AddPrefabMesh(TEXT("BoneTrunkMesh")
		, PREFAB_TYPE_FOUR_PYRAMID
		, params);

	_pBoneTrunkEntity = XNEW PrefabEntity(TEXT("BoneTrunkEntity"), pMeshPtr, _RenderSystem);

	_pBoneJointEntity->SetBaseTransform(Matrix4::BuildTransform(boneOrientation));
	_pBoneTrunkEntity->SetBaseTransform(Matrix4::BuildTransform(Vector3(0, 0.5, 0))
		* Matrix4::BuildScaleTransform(Vector3(1, nBoneTrunkLength, 1))
		* Matrix4::BuildTransform(Vector3(0, 0.5, 0)) 
		* Matrix4::BuildTransform(boneOrientation));

	AttachObject(_pBoneTrunkEntity);

	//----------------
	for (size_t i=0; i<this->GetChildCount(); ++i)
	{
		BoneInstance* pChild = this->GetChildAt(i);
		pChild->CreateHelperEntity();
	}

}

BoneInstance::~BoneInstance(void)
{
	UINT nChildCount = GetChildCount();

	for (UINT i=0; i<nChildCount; ++i)
	{
		BoneNode* pChild = GetChildAt(i);
		SAFE_DELETE(pChild);
	}

	_children.clear();
}

_X3D_NS_END


