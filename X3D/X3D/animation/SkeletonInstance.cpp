#include "StdAfx.h"
#include "SkeletonInstance.h"
#include "BoneInstance.h"
#include "x3d/scene/Entity.h"
#include "x3d/scene/PrefabEntity.h"

_X3D_NS_BEGIN

SkeletonInstance::SkeletonInstance(const SkeletonPtr& ptrSkeleton, Ptr<Entity> pEntity)
: _pMasterSkeleton(ptrSkeleton), _pParentEntity(pEntity)
, RenderObject(pEntity->GetRenderSystem())
{
	_pRootBone = NULL;

	_pMasterSkeleton = ptrSkeleton;
	_pParentEntity = pEntity;

	UpdateFromMasterSkeleton();
}

SkeletonInstance::~SkeletonInstance(void)
{
	if(_pRootBone)
	{
		SAFE_DELETE(_pRootBone);
	}
}

void SkeletonInstance::UpdateFromMasterSkeleton()
{
	// Do nothing if master skeleton is null.
	if(!_pMasterSkeleton) return;

	XASSERT(_pRootBone == NULL);
	
	// Copy bone from master skeleton

	_pRootBone = _copyBoneNode(_pMasterSkeleton->GetRootBone());
	
	// Create bone helper mesh entity for debug.
	_pRootBone->CreateHelperEntity();
}

BoneInstance* SkeletonInstance::_copyBoneNode(BoneNode* pBoneSource)
{
	BoneInstance* pBoneInstance = XNEW BoneInstance(this, pBoneSource);

	for (size_t i=0; i< pBoneSource->GetChildCount(); ++i)
	{
		BoneNode* pChildBone = pBoneSource->GetChildAt(i);
		pBoneInstance->AddChild(_copyBoneNode(pChildBone));
	}

	return pBoneInstance;
}

_X3D_NS_END
