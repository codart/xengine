#pragma once

#include "x3d/resource/Resource.h"
#include "x3d/animation/BoneNode.h"

_X3D_NS_BEGIN


class _X3DExport Skeleton : public Resource
						//... , public EventDispatcher
{
public:
	Skeleton(void);
	virtual ~Skeleton(void);


public:
	INLINE BoneNode*
		GetRootBone() { return _pRootBone; }
	virtual void
		Load();


protected:
	BoneNode*
		_pRootBone;

};

typedef SharedPtr<Skeleton> SkeletonPtr;
DECLARE_MAP_TYPE(map, String, SkeletonPtr);

typedef OwnerPtr<SkeletonPtr> SkeletonOwnerPtr;
DECLARE_MAP_TYPE(map, String, SkeletonOwnerPtr);

_X3D_NS_END