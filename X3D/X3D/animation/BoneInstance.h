#pragma once

#include "x3d/animation/BoneNode.h"

_X3D_NS_BEGIN

class SkeletonInstance;

class BoneInstance : public BoneNode
{
public:
	BoneInstance(Ptr<SkeletonInstance> pSkeletonInstance, Ptr<BoneNode> pBoneNode);
	virtual ~BoneInstance(void);


public:
	void
		AddChild(BoneInstance* pBoneNode);

	INLINE BoneInstance* 
		GetChildAt(UINT iIndex) { return SAFE_CAST(BoneNode::GetChildAt(iIndex), BoneInstance*); }

	void 
		CreateHelperEntity();


protected:
	/// The skeleton instance that owner this bone instance.
	Ptr<SkeletonInstance>
		_pSkeletonInstance;

	/// A helper entity used to visualize this bone instance.
	SharedPtr<PrefabEntity>
		_pBoneJointEntity;
	SharedPtr<PrefabEntity> 
		_pBoneTrunkEntity;

};

_X3D_NS_END

