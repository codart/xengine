#pragma once

#include "x3d/animation/Skeleton.h"

_X3D_NS_BEGIN

class Entity;
class BoneInstance;

/**
	SkeletonInstance is an instance of Skeleton. 
	It can include visible items for debug, so it inherit from RenderObject. 
*/
class _X3DExport SkeletonInstance : public RenderObject
{
	temporary friend class BoneInstance;

public:
	SkeletonInstance(const SkeletonPtr& ptrSkeleton, Ptr<Entity> pEntity);
	virtual ~SkeletonInstance(void);


public:
	/// Update this skeleton instance from the master skeleton
	void
		UpdateFromMasterSkeleton();

	INLINE BoneInstance*
		GetRootBone() const { return _pRootBone; }


protected:
	/// Master copy
	SkeletonPtr
		_pMasterSkeleton;

	Ptr<BoneInstance>
		_pRootBone;


	/// The entity that owner this skeleton instance.
	temporary Ptr<Entity>
		_pParentEntity;


protected:
	BoneInstance*
		_copyBoneNode(BoneNode* pBoneSource);

};


_X3D_NS_END

