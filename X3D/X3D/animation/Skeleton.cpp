#include "StdAfx.h"
#include "Skeleton.h"

_X3D_NS_BEGIN

Skeleton::Skeleton(void)
{

}

Skeleton::~Skeleton(void)
{
	temporary;
	BoneNode* pSecondBone = (BoneNode*)_pRootBone->GetChildAt(0);
	SAFE_DELETE(pSecondBone);

	SAFE_DELETE(_pRootBone);
}


void Skeleton::Load()
{
	temporary
	_pRootBone = XNEW BoneNode(TEXT("RootBone"));

	BoneNode* pChildBone = XNEW BoneNode(TEXT("TestSecondBone"));
	_pRootBone->AddChild(pChildBone);

	pChildBone->Up(30);
	pChildBone->SetBindingPose();

	BoneNode* pChildBone2 = XNEW BoneNode(TEXT("TestThirdBone"));
	pChildBone->AddChild(pChildBone2);

	pChildBone2->Up(10);
	//pChildBone2->Left(10);
	pChildBone2->SetBindingPose();

	BoneNode* pChildBone3 = XNEW BoneNode(TEXT("TestFourthBone"));
	pChildBone2->AddChild(pChildBone3);

	pChildBone3->Up(10);
	pChildBone3->SetBindingPose();
}

_X3D_NS_END