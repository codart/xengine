#pragma once

#include "x3d/resource/ResourceManager.h"
#include "x3d/animation/Skeleton.h"

_X3D_NS_BEGIN

class _X3DExport SkeletonManager: public Singleton<SkeletonManager, AutoRelease>
								, public ResourceManager
{
public:
	SkeletonManager(void);
	virtual ~SkeletonManager(void);


public:
	SkeletonPtr
		AddSkeleton(const String& strSkeletonName);
	void 
		unInit();

protected:
	SkeletonOwnerPtrMap
		_skeletonMap;

};

_X3D_NS_END