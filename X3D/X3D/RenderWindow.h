#pragma once

#include "Common/plantform/Window.h"
#include "Common/input/InputObject.h"
#include "Common/input/Mouse.h"
#include "Common/input/Keyboard.h"

#include "x3d/RenderArea.h"
#include "x3d/render/RenderObject.h"
#include "x3d/resource/RenderTexture.h"
#include "x3d/render/WindowBindedResource.h"


class common::ScriptBinder;

_X3D_NS_BEGIN

class WindowRenderTarget;
class Camera;
class Scene;
class RenderSystem;

class _X3DExport RenderWindow : public WindowMsgReceiver
							  , public RenderObject
							  , public EventDispatcher
{
	friend class X3DModule;
	friend class RenderArea;

	DECLARE_EVENT_RECEIVER(RenderWindow)		

public:
	static void ExportToScript(ScriptBinder* pScriptBinder);

	RenderWindow(RenderSystem* pRenderSystem, const String& strName);
	virtual ~RenderWindow(void);


public:
	virtual const String&
		GetName() { return _strName; }
	virtual void
		ShowWindow(BOOL bShow);

	virtual int
		GetWidth();
	virtual int
		GetHeight();

	virtual RenderArea*
		AddRenderArea(const CameraPtr& pAttachedCamera, const ScenePtr& pAttachedScene
						, int nZOrder = 0, Rect rect = Rect(0, 0, 0, 0));
	virtual RenderArea* 
		GetRenderArea(int nZOrder);
	virtual void
		RemoveRenderArea(int nZOrder);
	virtual void
		RemoveRenderArea(RenderArea* pRenderArea);


	// Return a reference to the internal window object.
	virtual Window*
		GetWindow(){return _pWindow;};

	/* If window is not created, return NULL */
	virtual Mouse*
		GetMouse(){return (Mouse*)_pMouse;};
	/* If window is not created, return NULL */
	virtual Keyboard*
		GetKeyboard(){ return (Keyboard*)_pKeyboard; }

	//----
	INLINE RenderTexturePtr&
		getGBuffer() { return _pWndBindedRes->GetGBuffer(); }
	INLINE RenderTexturePtr&
		getLightBuffer() { return _pWndBindedRes->GetLightBuffer(); }


package:
	WindowRenderTarget*
		getWndRenderTarget() { return _pWndRenderTarget; }
	INLINE SceneRender*
		getSceneRender() { return _pSceneRender; }


protected:
	/*
		Update all RenderArea.
	*/
	INLINE void 
		update();

	//--- Window message handlers
	override void 
		OnPaint(const Rect* pPaintRect);
	override BOOL 
		OnClose();

	/**
		CreateWnd a window. Before this RenderWindow attach to a WindowRenderTarget, it 
		cannot be used as Render Target.
	 */
	virtual void 
		CreateWnd(const String& strWindowTitle, const Rect& rect);
	virtual void
		DestroyWnd();

	// ---
	virtual DWORD_PTR
		getWindowHandle(){return _pWindow->GetWindowHandle();};
	void 
		attachWndRenderTarget(WindowRenderTarget* pWindowRenderTarget);


protected:
	String 
		_strName;
	RenderSystem*
		_pRenderSystem;
	GraphicSystem* 
		_pGraphicSystem;

	Window*
		_pWindow;

	WindowRenderTarget*
		_pWndRenderTarget;

	RenderAreaList
		_renderAreaList;

	WindowBindedResource*
		_pWndBindedRes;

	// --- Renders ---
	SceneRender*
		_pSceneRender;
	HUDRender*
		_pHUDRender;

	//-- Input Objects ---
	InputObject*
		_pMouse;
	InputObject*
		_pKeyboard;


protected:
	INLINE void 
		_deleteRenderArea(RenderArea* pRenderArea);
	INLINE void 
		_sortRenderAreaByZOrder();

	static int 
		_compareRenderArea(RenderArea* p1, RenderArea* p2);

};

typedef SharedPtr<RenderWindow> RenderWindowPtr;

_X3D_NS_END