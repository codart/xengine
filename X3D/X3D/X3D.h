#pragma once

#include "X3DMacros.h"
#include "X3DHeaders.h"
#include "X3DModule.h"

extern "C"
{
	_X3DExport common::IModule* CreateX3DModule();
	_X3DExport x3d::X3DModule* GetX3DModule();
	_X3DExport void DestroyX3DModule();
}
