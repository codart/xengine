#include "stdafx.h"
#include "SubModel.h"

#include "x3d/datadef/ModelDataDef.h"
#include "x3d/graphic/VertexLayout.h"

_X3D_NS_BEGIN

SubModel::SubModel(const String& strName)
: _strName(strName), _strMaterialName(TEXT("VertexColorMaterial"))
{
	_vertexFlag = 0;
	_primitiveType = DRAW_TRIANGLE_LIST;
	_bUpdating = FALSE;
}

SubModel::~SubModel()
{

}

void SubModel::GenerateSubMeshData(OUT SubMesh* pSubMesh)
{
	XASSERT(0);
} 

DWORD SubModel::GetVertexStride()
{
	int iStride = 0;
	if(_vertexFlag & VERTEX_FLAG_XYZ)
	{
		iStride += 3 * sizeof(float);
	}
	if(_vertexFlag & VERTEX_FLAG_NORMAL)
	{
		iStride += 3 * sizeof(float);;
	}
	if(_vertexFlag & VERTEX_FLAG_DIFFUSE)
	{
		iStride += 4 * sizeof(float);;
	}
	if(_vertexFlag & VERTEX_FLAG_UV0)
	{
		iStride += 2 * sizeof(float);;
	}

	// ---------- Bone Weights ---------
	if(_vertexFlag & VERTEX_FLAG_BONE_WEIGHT1)
	{
		XASSERT( (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0
			&& (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );
		iStride += sizeof(float);
	}

	if(_vertexFlag & VERTEX_FLAG_BONE_WEIGHT2)
	{
		XASSERT( (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
			&& (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );
		iStride += 2 * sizeof(float);
	}

	if(_vertexFlag & VERTEX_FLAG_BONE_WEIGHT3)
	{
		XASSERT( (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
			&& (_vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0 );
		iStride += 3 * sizeof(float);
	}

	if(_vertexFlag & WERTEX_FLAG_BONE_INDICES)
	{
		iStride += 4;
	}

	return iStride;
}

void SubModel::_updateVertexLayoutByVertexFlag(VertexLayout* pVertexLayout, DWORD vertexFlag)
{
	pVertexLayout->BeginUpdate();
	int iOffset = 0;
	if(vertexFlag & VERTEX_FLAG_XYZ)
	{
		pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
		iOffset += 12;
	}

	if(vertexFlag & VERTEX_FLAG_NORMAL)
	{
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT3, VES_NORMAL);
		iOffset += 12;
	}

	if(vertexFlag & VERTEX_FLAG_DIFFUSE)
	{
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT4, VES_DIFFUSE);
		iOffset += 16;
	}

	if(vertexFlag & VERTEX_FLAG_UV0)	
	{
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT2, VES_TEXTURE_COORDINATES);
		iOffset += 8;
	}

	// --------- BOne weights --------------
	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT1)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0
				&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT1, VES_BLEND_WEIGHTS);
		iOffset += sizeof(float);
	}

	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT2)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
				&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT2, VES_BLEND_WEIGHTS);
		iOffset += 2 * sizeof(float);
	}

	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT3)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
			&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0 );
		pVertexLayout->AddElement(0, iOffset, VEF_FLOAT3, VES_BLEND_WEIGHTS);
		iOffset += 3 * sizeof(float);
	}

	if(vertexFlag & WERTEX_FLAG_BONE_INDICES)
	{
		pVertexLayout->AddElement(0, iOffset, VEF_UBYTE4, VES_BLEND_INDICES);
		iOffset += 4;
	}

	pVertexLayout->EndUpdate();
}

void SubModel::_fillVertexStreamByVertexFlag(BYTE*& pVertexStream, const Vertex* pVertex, DWORD vertexFlag)
{
	int iOffset = 0;
	if(vertexFlag & VERTEX_FLAG_XYZ)
	{
		memcpy(&pVertexStream[iOffset], (void*)&pVertex->position, sizeof(pVertex->position));
		iOffset += 12;
	}

	if(vertexFlag & VERTEX_FLAG_NORMAL)
	{
		memcpy(&pVertexStream[iOffset], (void*)&pVertex->normal, sizeof(pVertex->normal));
		iOffset += 12;
	}

	if(vertexFlag & VERTEX_FLAG_DIFFUSE)
	{
		memcpy(&pVertexStream[iOffset], (void*)&pVertex->diffuse, sizeof(pVertex->diffuse));
		iOffset += 16;
	}

	if(vertexFlag & VERTEX_FLAG_UV0)	
	{
		memcpy(&pVertexStream[iOffset], (void*)&pVertex->uv0, sizeof(pVertex->uv0));
		iOffset += 8;
	}

	// ------ Bone weights ----------
	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT1)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0
			&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );

		memcpy(&pVertexStream[iOffset], (void*)&pVertex->weights[0], sizeof(float));
		iOffset += 4;
	}

	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT2)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
			&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT3) == 0 );

		memcpy(&pVertexStream[iOffset], (void*)&pVertex->weights[0], 2 * sizeof(float));
		iOffset += 2 * sizeof(float);
	}

	if(vertexFlag & VERTEX_FLAG_BONE_WEIGHT3)
	{
		XASSERT( (vertexFlag & VERTEX_FLAG_BONE_WEIGHT1) == 0
			&& (vertexFlag & VERTEX_FLAG_BONE_WEIGHT2) == 0 );

		memcpy(&pVertexStream[iOffset], (void*)&pVertex->weights[0], 3 * sizeof(float));
		iOffset += 3 * sizeof(float);
	}

	if(vertexFlag & WERTEX_FLAG_BONE_INDICES)
	{
		int n = sizeof(pVertex->indices);
		memcpy(&pVertexStream[iOffset], (void*)pVertex->indices, sizeof(pVertex->indices));
		iOffset += 4;
	}
}

void SubModel::BeginUpdate(DWORD vertexFlag, DrawPrimitiveType primitiveType)
{
	_vertexFlag = vertexFlag;
	_primitiveType = primitiveType;
	_bUpdating = TRUE;

	_onBeginUpdate();
}

void SubModel::EndUpdate()
{
	_onEndUpdate();

	XASSERT(_bUpdating);
	_bUpdating = FALSE;
}

_X3D_NS_END
