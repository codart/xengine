#include "stdafx.h"
#include "editablemesh.h"

#include "x3d/resource/VertexData.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/SubMesh.h"

#include "x3d/graphic/IndexBuffer.h"


_X3D_NS_BEGIN

EditableMesh::EditableMesh(const String& strName)
: SubModel(strName)
{

}

EditableMesh::~EditableMesh()
{
	_releaseAllVertices();
	_releaseAllFaces();
}

void EditableMesh::GenerateSubMeshData(OUT SubMesh* pSubMesh)
{
	VertexData* pVertexData = XNEW VertexData(pSubMesh->getGraphicSystem());
	IndexData* pIndexData = XNEW IndexData(pSubMesh->getGraphicSystem());

	pSubMesh->setVertexData(pVertexData);
	pSubMesh->setIndexData(pIndexData);

	pSubMesh->setDrawPrimitiveType(GetDrawPrimitiveType());
	pSubMesh->SetMaterialName(GetMaterialName());

	// Prepare vertex layout
	VertexLayout* pVertexLayout = pVertexData->GetVertexLayout();
	_updateVertexLayoutByVertexFlag(pVertexLayout, GetVertexFlag());

	// Fill vertex data
	int nStride = GetVertexStride();
	const VertexList& vertices = GetVertices();
	VertexBuffer* pVertexBuffer = pVertexData->AddVertexBufferBinding(vertices.size(), nStride);

	ConstVectorIterator<Vertex*> vertexIterator(vertices);

	BYTE* pVerticesStream = XNEW BYTE[pVertexBuffer->GetBufferSize()];
	BYTE* pVertexStream = pVerticesStream;

	while(vertexIterator.HasMore())
	{
		const Vertex* pVertex = vertexIterator.CurrentData();

		_fillVertexStreamByVertexFlag(pVertexStream, pVertex, pVertex->flag);

		pVertexStream += nStride;
		vertexIterator.Next();
	}

	pVertexBuffer->WriteData(0, pVertexBuffer->GetBufferSize(), pVerticesStream);
	SAFE_DELETE(pVerticesStream);

	// Fill index data
	const FaceList faces = GetFaces();

	IndexBuffer* pIndexBuffer = pIndexData->initIndexBuffer(faces.size() * 3, sizeof(USHORT));

	vector<FaceIndices> faceList;
	faceList.resize(faces.size());

	for (int i=0; i< (int)faces.size(); ++i)
	{
		Face* pFace = faces[i];
		FaceIndices& faceIndices = faceList[i];
		faceIndices = pFace->indices;
	}

	pIndexBuffer->WriteData(0, pIndexBuffer->GetBufferSize(), &faceList[0], true);
} 

void EditableMesh::_onBeginUpdate()
{
	_releaseAllVertices();
	_releaseAllFaces();
}

void EditableMesh::_onEndUpdate()
{
	// Update local AABB
	FASTEST_VECTOR_ITERATE(Vertex*, _vertices);
		Vertex* pVertex = GET_NEXT(_vertices);
#ifdef DEBUG
		// Check if all the updated vertex has the correct vertex element specified by _vertexFlag
		XASSERT(pVertex->flag == _vertexFlag);
#endif
		_localAABB.Merge(pVertex->position);
	FASTEST_ITERATE_END();

}

USHORT EditableMesh::AddTriangle(const Vector3& position1, const Vector3& position2, const Vector3& position3)
{
	XASSERT(_bUpdating);

	int iVertexStart = _vertices.size();

	Vertex* pVertex = XNEW Vertex;
	pVertex->position = position1;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position2; 
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position3;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	Face* pFace = XNEW Face(iVertexStart, iVertexStart+1, iVertexStart+2);
	_faces.push_back(pFace);

	return iVertexStart;
}

USHORT EditableMesh::AddQuad(const Vector3& position1, const Vector3& position2
						, const Vector3& position3, const Vector3& position4)
{
	XASSERT(_bUpdating);

	int iVertexStart = _vertices.size();

	Vertex* pVertex = XNEW Vertex;
	pVertex->position = position1;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position2; 
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position3;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position4;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	Face* pFace = XNEW Face(iVertexStart, iVertexStart+1, iVertexStart+2);
	_faces.push_back(pFace);

	pFace = XNEW Face(iVertexStart+2, iVertexStart+1, iVertexStart+3);
	_faces.push_back(pFace);

	return iVertexStart;
}

void EditableMesh::AddQuad(USHORT index1, USHORT index2, USHORT index3, USHORT index4)
{
	XASSERT(index1 < _vertices.size()
		&& index2 < _vertices.size()
		&& index3 < _vertices.size()
		&& index4 < _vertices.size() );

	Face* pFace = XNEW Face(index1, index2, index3);
	_faces.push_back(pFace);

	pFace = XNEW Face(index3, index2, index4);
	_faces.push_back(pFace);
}

void EditableMesh::SetTriangleUV(int iStartVertex, Vector2 uv1, Vector2 uv2, Vector2 uv3)
{
	XASSERT(iStartVertex + 2 <= (int)_vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->uv0 = uv1;
	pVertex->flag |= VERTEX_FLAG_UV0;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->uv0 = uv2;
	pVertex->flag |= VERTEX_FLAG_UV0;
	pVertex = _vertices[iStartVertex + 2];
	pVertex->uv0 = uv3;
	pVertex->flag |= VERTEX_FLAG_UV0;
}

void EditableMesh::SetQuadColor(int iStartVertex, Vector4 color)
{
	XASSERT(iStartVertex + 3 <= (int)_vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 2];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 3];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

void EditableMesh::SetQuadColor(int iStartVertex, Vector4 color1, Vector4 color2, Vector4 color3, Vector4 color4)
{
	XASSERT(iStartVertex + 3 <= (int)_vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->diffuse = color1;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->diffuse = color2;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 2];
	pVertex->diffuse = color3;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 3];
	pVertex->diffuse = color4;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

void EditableMesh::SetTriangleColor(int iStartVertex, Vector4 color1, Vector4 color2, Vector4 color3)
{
	XASSERT(iStartVertex + 2 <= (int)_vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->diffuse = color1;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->diffuse = color2;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 2];
	pVertex->diffuse = color3;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

UINT EditableMesh::AddVertex(const Vector3& position)
{
	XASSERT(_bUpdating);

	int iVertexIndex = _vertices.size();

	Vertex* pVertex = XNEW Vertex;
	pVertex->position = position;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	return iVertexIndex;
}

void EditableMesh::SetVertexColor(UINT iIndex, Vector4 color)
{
	XASSERT(iIndex >= 0 && iIndex < _vertices.size());

	Vertex* pVertex = _vertices[iIndex];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

void EditableMesh::SetBoneWeights(UINT iIndex, float fWeight1
									 , float fWeight2
									 , float fWeight3
									 , float fWeight4 )
{
	XASSERT(iIndex >= 0 && iIndex < _vertices.size());

	Vertex* pVertex = _vertices[iIndex];
	if(fWeight4 != -1)
	{
		pVertex->flag |= VERTEX_FLAG_BONE_WEIGHT3;
		XASSERT(Math::Equal(fWeight1, (1-fWeight2-fWeight3-fWeight4)));
	}
	else if(fWeight3 != -1)
	{
		pVertex->flag |= VERTEX_FLAG_BONE_WEIGHT2;
		XASSERT(Math::Equal(fWeight1, (1-fWeight2-fWeight3)));
	}
	else if(fWeight2 != -1)
	{
		pVertex->flag |= VERTEX_FLAG_BONE_WEIGHT1;
		XASSERT(Math::Equal(fWeight1, (1-fWeight2)));
	}

	pVertex->weights[0] = fWeight2;
	pVertex->weights[1] = fWeight3;
	pVertex->weights[2] = fWeight4;

/*
	pVertex->weights[0] = fWeight1;
	pVertex->weights[1] = fWeight2;
	pVertex->weights[2] = fWeight3;
	pVertex->weights[3] = fWeight4;*/
}

void EditableMesh::SetBoneIndices(UINT iVertexIndex
							   , int iBoneIndex1
							   , int iBoneIndex2
							   , int iBoneIndex3
							   , int iBoneIndex4)
{
	XASSERT(iVertexIndex >= 0 && iVertexIndex < _vertices.size());

	Vertex* pVertex = _vertices[iVertexIndex];

	pVertex->flag |= WERTEX_FLAG_BONE_INDICES;

	pVertex->indices[0] = iBoneIndex1;
	pVertex->indices[1] = iBoneIndex2;
	pVertex->indices[2] = iBoneIndex3;
	pVertex->indices[3] = iBoneIndex4;
}

void EditableMesh::AddTriangle(USHORT index1, USHORT index2, USHORT index3)
{
	XASSERT( index1 < _vertices.size()
			 && index2 < _vertices.size()
			 && index3 < _vertices.size() );
	Face* pFace = XNEW Face(index1, index2, index3);
	_faces.push_back(pFace);
}

void EditableMesh::_releaseAllVertices()
{
	FastVecIterator<Vertex*> vertexIterator(_vertices);

	while (vertexIterator.HasMore())
	{
		XDELETE vertexIterator.CurrentData();
		vertexIterator.Next();
	}
}

void EditableMesh::_releaseAllFaces()
{
	FastVecIterator<Face*> faceIterator(_faces);

	while (faceIterator.HasMore())
	{
		XDELETE faceIterator.CurrentData();
		faceIterator.Next();
	}

	_faces.clear();
}


_X3D_NS_END