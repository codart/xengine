#pragma once

#include "x3d/model/SubModel.h"
#include "x3d/datadef/ModelDataDef.h"

_X3D_NS_BEGIN


class _X3DExport EditableMesh : public SubModel
{
public:
	EditableMesh(const String& strName);
	virtual ~EditableMesh();

public:
	void 
		GenerateSubMeshData(OUT SubMesh* pSubMesh);

	const VertexList&
		GetVertices() { return _vertices; }
	const FaceList&
		GetFaces() { return _faces; }
	int
		GetVertexCount() { return _vertices.size(); }

	// -------- Vertex ----------
	UINT
		AddVertex(const Vector3& position);
	void
		SetVertexColor(UINT iIndex, Vector4 color);

	// At least set two weight (if only one weight, it should aways be 1, so actually no need set).
	void
		SetBoneWeights(UINT iVertexIndex, float fWeight1
						, float fWeight2
						, float fWeight3 = -1
						, float fWeight4 = -1);
	void
		SetBoneIndices(UINT iVertexIndex
						, int iBoneIndex1
						, int iBoneIndex2 = -1
						, int iBoneIndex3 = -1
						, int iBoneIndex4 = -1);


	// -------- Triangle ---------
	USHORT
		AddTriangle(const Vector3& position1, const Vector3& position2, const Vector3& position3);
	// Add a triangle with the exist vertices.
	void 
		AddTriangle(USHORT index1, USHORT index2, USHORT index3);
	void
		SetTriangleUV(int iStartVertex, Vector2 uv1, Vector2 uv2, Vector2 uv3);
	void
		SetTriangleColor(int iStartVertex, Vector4 color1, Vector4 color2, Vector4 color3);

	// --------- Quad -----------
	/* 
		Draw quad

		Vertex position:
		1---2
		| / |
		3---4

		Face:
		Trangle1 (1, 2, 3)
		Triangle2 ( 3, 4, 2)
	*/
	USHORT
		AddQuad(const Vector3& position1, const Vector3& position2
			, const Vector3& position3, const Vector3& position4);
	/* 
		Add a quad with the exist vertices.

		Face:
		Trangle1 (1, 2, 3)
		Triangle2 ( 3, 4, 2)
	*/
	void
		AddQuad(USHORT index1, USHORT index2, USHORT index3, USHORT index4);

	void
		SetQuadColor(int iStartVertex, Vector4 color1, Vector4 color2, Vector4 color3, Vector4 color4);
	void
		SetQuadColor(int iStartVertex, Vector4 color);

	void
		SetQuadUV(int iStartVertex, Vector2 uv1, Vector2 uv2, Vector2 uv3, Vector2 uv4);



protected:
	FaceList
		_faces;
	VertexList
		_vertices;


protected:
	void 
		_onBeginUpdate();
	void 
		_onEndUpdate();

	void 
		_releaseAllVertices();
	void
		_releaseAllFaces();


};

_X3D_NS_END