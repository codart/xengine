#pragma once

#include "x3d/datadef/VertexDef.h"
#include "X3D/graphic/DrawPrimitiveCommand.h"

_X3D_NS_BEGIN

enum VertexFlag
{
	VERTEX_FLAG_XYZ = 1 << 0,
	VERTEX_FLAG_NORMAL = 1 << 1,
	VERTEX_FLAG_TANGENT = 1 << 2,

	VERTEX_FLAG_UV0 = 1 << 3,
	VERTEX_FLAG_UV1 = 1 << 4,
	VERTEX_FLAG_UV2 = 1 << 5,
	VERTEX_FLAG_UV3 = 1 << 6,
	VERTEX_FLAG_UV4 = 1 << 7,
	VERTEX_FLAG_UV5 = 1 << 8,
	VERTEX_FLAG_UV6 = 1 << 9,
	VERTEX_FLAG_UV7 = 1 << 10,

	VERTEX_FLAG_DIFFUSE = 1 << 11,
	VERTEX_FLAG_BONE_WEIGHT1 = 1 << 12, // Two bone, need 1 float weight (if only one bone, no weight needed).
	VERTEX_FLAG_BONE_WEIGHT2 = 1 << 13, // Three bone, need 2 float weight 
	VERTEX_FLAG_BONE_WEIGHT3 = 1 << 14, // Four bone, need 3 float weight 

	WERTEX_FLAG_BONE_INDICES = 1 << 16, // Bone indices

	VERTEX_FLAG_XYZW = 1 << 17,
};

enum SubModelType
{
	EDITABLE_MESH,
	EDITABLE_SPLINE,
	NURBS_SPLINE,
};

class SubMesh;
class Vertex;
class VertexLayout;

class _X3DExport SubModel
{
public:
	SubModel(const String& strName);
	virtual ~SubModel();


public:
	virtual void
		GenerateSubMeshData(OUT SubMesh* pSubMesh);
	virtual void 
		BeginUpdate(DWORD vertexFlag = VERTEX_FLAG_XYZ | VERTEX_FLAG_DIFFUSE
		, DrawPrimitiveType type = DRAW_TRIANGLE_LIST);
	virtual void
		EndUpdate();

	const String&
		GetMaterialName() const { return _strMaterialName; }
	void
		SetMaterialName(const String& strMaterialName) { _strMaterialName = strMaterialName; }
	DrawPrimitiveType
		GetDrawPrimitiveType() { return _primitiveType; }
	DWORD 
		GetVertexStride();
	DWORD 
		GetVertexFlag() { return _vertexFlag; }
	const AxisAlignedBox&
		GetLocalAABB() { return _localAABB; }


protected:
	void 
		_updateVertexLayoutByVertexFlag(VertexLayout* pVertexLayout, DWORD vertexFlag);
	INLINE void 
		_fillVertexStreamByVertexFlag(BYTE*& pVertexStream, const Vertex* pVertex, DWORD vertexFlag);

//internal_event:
	virtual void 
		_onBeginUpdate() {};
	virtual void 
		_onEndUpdate() {};


protected:
	// Indicate what element the vertex include. 
	String
		_strMaterialName;
	String
		_strName;

	DWord
		_vertexFlag;
	DrawPrimitiveType
		_primitiveType;
	Boolean
		_bUpdating;
	AxisAlignedBox
		_localAABB;

};


DECLARE_LIST_TYPE_PTR(vector, SubModel);

_X3D_NS_END

