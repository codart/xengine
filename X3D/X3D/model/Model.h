#pragma once

#include "x3d/resource/Resource.h"
#include "x3d/model/SubModel.h"


_X3D_NS_BEGIN

class StaticMesh;

/*
	Use this class to model a mesh.	
*/
class _X3DExport Model : public Resource
{
public:
	Model(const String& strName);
	virtual ~Model(void);


public:
	SubModel*
		AddSubModel(const String& strName, SubModelType type);
	SubModelList&
		GetSubModelList() {return _subModelList; };

	virtual void
		AttachToMesh(StaticMesh* pMesh);
	virtual void
		UpdateAttachedMesh();


protected:
	SubModelList
		_subModelList;

	// The mesh that can real time rendering this Model.
	StaticMesh*
		_pStaticMesh;


};


DECLARE_MAP_TYPE_PTR(map, String, Model);

_X3D_NS_END

