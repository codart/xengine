#include "StdAfx.h"
#include "Model.h"

#include "x3d/resource/VertexData.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/MeshManager.h"

#include "x3d/model/editableMesh.h"
#include "x3d/model/editableSpline.h"
#include "x3d/resource/StaticMesh.h"

_X3D_NS_BEGIN

Model::Model(const String& strName)
: Resource(strName)
{
	_pStaticMesh = NULL;
}

Model::~Model(void)
{
	FastVecIterator<SubModel*> subModelIterator(_subModelList);

	while (subModelIterator.HasMore())
	{
		XDELETE subModelIterator.CurrentData();
		subModelIterator.Next();
	}
}

SubModel* Model::AddSubModel(const String& strName, SubModelType type)
{
	SubModel* pSubModel = NULL;
	
	switch (type)
	{
	case EDITABLE_MESH:
		pSubModel = XNEW EditableMesh(strName);
		break;

	case EDITABLE_SPLINE:
		pSubModel = XNEW EditableSpline(strName);
		break;

	case NURBS_SPLINE:
		XASSERT(0);
		break;

	default:
		XASSERT(0);
	}

	_subModelList.push_back(pSubModel);
	return pSubModel;
}

void Model::AttachToMesh(StaticMesh* pMesh)
{
	_pStaticMesh = pMesh;

	// Init the mesh
	//....
}

void Model::UpdateAttachedMesh()
{
	_pStaticMesh->removeAllSubMeshs();

	FastVecIterator<SubModel*> subModelIterator(_subModelList);

	while (subModelIterator.HasMore())
	{
		SubMesh* pSubMesh = _pStaticMesh->addSubMesh();
		//pSubMesh->generateBySubModel(subModelIterator.CurrentData());

		SubModel* pSubModel = subModelIterator.CurrentData();
		pSubModel->GenerateSubMeshData(pSubMesh);

		subModelIterator.Next();
	}
	
	//...return MeshManager::Instance().addMesh(pMesh);
	//return NULL;
}

_X3D_NS_END