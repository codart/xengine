#pragma once

#include "x3d/model/SubModel.h"
#include "x3d/datadef/ModelDataDef.h"

_X3D_NS_BEGIN

class _X3DExport EditableSpline : public SubModel
{
public:
	EditableSpline(const String& strName);
	virtual ~EditableSpline();


public:
	void 
		GenerateSubMeshData(OUT SubMesh* pSubMesh);

	const VertexList&
		GetVertices() { return _vertices; }
	const LineList&
		GetLines() { return _lines; }

	override void 
		BeginUpdate(DWORD vertexFlag = VERTEX_FLAG_XYZ | VERTEX_FLAG_DIFFUSE, DrawPrimitiveType type = DRAW_LINE_LIST);

	UINT 
		DrawLine(const Vector3& position1, const Vector3& position2);
	void
		SetLineUV(UINT iStartVertex, Vector2 uv1, Vector2 uv2);
	void
		SetLineColor(UINT iStartVertex, Vector4 color1, Vector4 color2);

	UINT
		AddVertex(const Vector3& position);
	void
		SetVertexColor(UINT iIndex, Vector4 color);
	void
		DrawLine(UINT iStartIndex, UINT iEndIndex);


protected:
	LineList
		_lines;
	VertexList
		_vertices;


protected:
	void 
		_onBeginUpdate();
	void 
		_onEndUpdate();
	void 
		_releaseAllVertices();
	void 
		_releaseAllLines();

};


_X3D_NS_END