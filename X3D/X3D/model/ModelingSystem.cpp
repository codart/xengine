#include "StdAfx.h"
#include "ModelingSystem.h"

_X3D_NS_BEGIN

ModelingSystem::ModelingSystem(void)
{
}

ModelingSystem::~ModelingSystem(void)
{
	MapIterator<String, Model*> modelIterator(_modelMap);

	while (modelIterator.HasMore())
	{
		XDELETE modelIterator.CurrentData();
		modelIterator.Next();
	}
	_modelMap.clear();
}

Model* ModelingSystem::AddModel(const String& strModelName) 
{
	XASSERT(strModelName.length() != 0);
	ModelMapIt it = _modelMap.find(strModelName);
	if(it != _modelMap.end()) return it->second;

	Model* pNewModel = XNEW Model(strModelName);
	_modelMap.insert(make_pair(strModelName, pNewModel));

	return pNewModel; 
}

void ModelingSystem::RemoveModel(const String& strModelName)
{
	ModelMapIt it = _modelMap.find(strModelName);
	if(it != _modelMap.end()) 
	{
		XDELETE it->second;
		_modelMap.erase(it);
	}
}



_X3D_NS_END