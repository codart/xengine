#include "stdafx.h"
#include "EditableSpline.h"

#include "x3d/resource/VertexData.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/SubMesh.h"
#include "x3d/graphic/IndexBuffer.h"

_X3D_NS_BEGIN

EditableSpline::EditableSpline(const String& strName)
: SubModel(strName)
{

}

EditableSpline::~EditableSpline()
{
	_releaseAllLines();
	_releaseAllVertices();
}

void EditableSpline::GenerateSubMeshData(OUT SubMesh* pSubMesh)
{
	VertexData* pVertexData = XNEW VertexData(pSubMesh->getGraphicSystem());
	IndexData* pIndexData = XNEW IndexData(pSubMesh->getGraphicSystem());

	pSubMesh->setVertexData(pVertexData);
	pSubMesh->setIndexData(pIndexData);

	pSubMesh->setDrawPrimitiveType(GetDrawPrimitiveType());
	pSubMesh->SetMaterialName(GetMaterialName());

	// Prepare vertex layout
	VertexLayout* pVertexLayout = pVertexData->GetVertexLayout();
	_updateVertexLayoutByVertexFlag(pVertexLayout, GetVertexFlag());

	// Fill vertex data
	int nStride = GetVertexStride();
	const VertexList& vertices = GetVertices();
	VertexBuffer* pVertexBuffer = pVertexData->AddVertexBufferBinding(vertices.size(), nStride);

	ConstVectorIterator<Vertex*> vertexIterator(vertices);

	BYTE* pVerticesStream = XNEW BYTE[pVertexBuffer->GetBufferSize()];
	BYTE* pVertexStream = pVerticesStream;

	while(vertexIterator.HasMore())
	{
		const Vertex* pVertex = vertexIterator.CurrentData();

		_fillVertexStreamByVertexFlag(pVertexStream, pVertex, pVertex->flag);

		pVertexStream += nStride;
		vertexIterator.Next();
	}

	pVertexBuffer->WriteData(0, pVertexBuffer->GetBufferSize(), pVerticesStream);
	SAFE_DELETE(pVerticesStream);

	// Fill index data
	IndexBuffer* pIndexBuffer = pIndexData->initIndexBuffer(_lines.size() * 2, sizeof(USHORT));

	vector<LineIndices> lineList;
	lineList.resize(_lines.size());

	for (int i=0; i< (int)_lines.size(); ++i)
	{
		Line* pLine = _lines[i];
		LineIndices& lineIndices = lineList[i];
		lineIndices = pLine->indices;
	}

	pIndexBuffer->WriteData(0, pIndexBuffer->GetBufferSize(), &lineList[0], true);
}

void EditableSpline::BeginUpdate(DWORD vertexFlag, DrawPrimitiveType type)
{
	SubModel::BeginUpdate(vertexFlag, type);
}

void EditableSpline::_onBeginUpdate()
{
	_releaseAllVertices();
	_releaseAllLines();
}

void EditableSpline::_onEndUpdate()
{
	// Update local AABB
	FASTEST_VECTOR_ITERATE(Vertex*, _vertices);
		Vertex* pVertex = GET_NEXT(_vertices);
#ifdef DEBUG
		// Check if all the updated vertex has the correct vertex element specified by _vertexFlag
		XASSERT(pVertex->flag == _vertexFlag);
#endif
		_localAABB.Merge(Vector3(pVertex->position.x, pVertex->position.y, pVertex->position.z));
	FASTEST_ITERATE_END();
}

UINT EditableSpline::DrawLine(const Vector3& position1, const Vector3& position2)
{
	XASSERT(_bUpdating);

	int iVertexStart = _vertices.size();

	Vertex* pVertex = XNEW Vertex;
	pVertex->position = position1;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	pVertex = XNEW Vertex;
	pVertex->position = position2; 
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	Line* pLine = XNEW Line(iVertexStart, iVertexStart+1);
	_lines.push_back(pLine);

	return iVertexStart;
}

void EditableSpline::DrawLine(UINT iStartIndex, UINT iEndIndex)
{
	XASSERT(iStartIndex >= 0 && iStartIndex < _vertices.size());
	XASSERT(iEndIndex >= 0 && iEndIndex < _vertices.size());

	Line* pLine = XNEW Line(iStartIndex, iEndIndex);
	_lines.push_back(pLine);	
}

void EditableSpline::SetLineUV(UINT iStartVertex, Vector2 uv1, Vector2 uv2)
{
	XASSERT(iStartVertex + 2 <= _vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->uv0 = uv1;
	pVertex->flag |= VERTEX_FLAG_UV0;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->uv0 = uv2;
	pVertex->flag |= VERTEX_FLAG_UV0;
}

void EditableSpline::SetLineColor(UINT iStartVertex, Vector4 color1, Vector4 color2)
{
	XASSERT(iStartVertex + 1 <= _vertices.size() - 1);

	Vertex* pVertex = _vertices[iStartVertex];
	pVertex->diffuse = color1;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
	pVertex = _vertices[iStartVertex + 1];
	pVertex->diffuse = color2;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

UINT EditableSpline::AddVertex(const Vector3& position)
{
	XASSERT(_bUpdating);

	int iVertexIndex = _vertices.size();

	Vertex* pVertex = XNEW Vertex;
	pVertex->position = position;
	pVertex->flag |= VERTEX_FLAG_XYZ;
	_vertices.push_back(pVertex);

	return iVertexIndex;
}

void EditableSpline::SetVertexColor(UINT iIndex, Vector4 color)
{
	XASSERT(iIndex >= 0 && iIndex < _vertices.size());

	Vertex* pVertex = _vertices[iIndex];
	pVertex->diffuse = color;
	pVertex->flag |= VERTEX_FLAG_DIFFUSE;
}

void EditableSpline::_releaseAllVertices()
{
	FastVecIterator<Vertex*> vertexIterator(_vertices);

	while (vertexIterator.HasMore())
	{
		XDELETE vertexIterator.CurrentData();
		vertexIterator.Next();
	}
}

void EditableSpline::_releaseAllLines()
{
	FASTEST_VECTOR_ITERATE(Line*, _lines);
		XDELETE GET_NEXT(_lines);
	FASTEST_ITERATE_END();

	_lines.clear();
}


_X3D_NS_END