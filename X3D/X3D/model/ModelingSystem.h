#pragma once

_X3D_NS_BEGIN

class ModelingSystem : public Singleton<ModelingSystem, AutoRelease>
{
public:
	ModelingSystem();
	virtual ~ModelingSystem();


public:
	Model*
		AddModel(const String& strModelName);
	void
		RemoveModel(const String& strModelName);


protected:
	ModelMap
		_modelMap;

};

_X3D_NS_END