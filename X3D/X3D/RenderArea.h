#pragma once

#include "Common/core/Event.h"
#include "Common/core/EventDispatcher.h"

#include "x3d/graphic/ViewportInfo.h"
#include "x3d/scene/Scene.h"
#include "x3d/render/IntrisincRenderOP.h"

#include "x3d/render/RenderOperation.h"
#include "x3d/hud/HUDRenderOperation.h"
#include "x3d/hud/HUDStage.h"
#include "x3d/scene/Camera.h"
#include "x3d/RenderAreaEvent.h"

_X3D_NS_BEGIN


class WindowRenderTarget;
class Scene;
class RenderWindow;

class RenderArea : public EventDispatcher
{
	friend class RenderWindow;

public:
	RenderArea(RenderSystem* pRenderSystem, RenderWindow* pRenderWindow
				, CameraPtr pAttachedCamera, ScenePtr pAttachedScene
				, const Rect& rect, int nZOrder);
	virtual ~RenderArea(void);


public:
	virtual void
		AttachHUDStage(const HUDStagePtr& pHUDStage) { _pAttachedHUDStage = pHUDStage; }
	virtual void
		DetachHUDStage() { _pAttachedHUDStage = NULL; }
	virtual HUDStagePtr
		GetAttachedHUDStage() { return _pAttachedHUDStage; }

	virtual int
		GetZOrder() const { return _nZOrder; };

	virtual int 
		GetWidth();
	virtual int
		GetHeight();

	virtual void
		SetWidth(int nWidth);
	virtual void
		SetHeight(int nHeight);
	virtual void
		SetSize(int nWidth, int nHeight);

	virtual void
		SetBackgroundColor(DWORD color){_backgroundColor = color;};
	virtual DWORD
		GetBackgroundColor(){return _backgroundColor;};

	virtual const ViewportInfo*
		GetViewportInfo() const { return &_viewportInfo; }

	/// Push a render operation to the end of the list. RenderOperation will be used for rendering scene.
	virtual void 
		AddRenderOperation(DWORD dwRenderOPType);

	/// Push a HUD render operation to the end of the list. HUDRenderOperation is used for rendering HUD Stage. 
	virtual void
		AddHUDRenderOperation(DWORD dwRenderOPType);

	virtual void
		SetAutoAspectRatio(BOOL bAutoAspectRatio);
	virtual INLINE BOOL
		GetAutoAspectRatio() { return __bAutoAspectRatio; }

	/// Return the render window this render area belongs to. 
	transient INLINE RenderWindow*
		GetRenderWindow() { return _pRenderWindow; }


protected:
	void
		update();
	void 
		_onWindowSizeChanged(const Event* pEvent);


protected:
	RenderOperationList
		_renderOPList;

	HUDRenderOperationList
		_HUDRenderOPList;

	RenderSystem* 
		_pRenderSystem;

	// The render window that this render area belongs to.
	RenderWindow*
		_pRenderWindow;

	// Camera, scene and HUDStage can be shared during scenes (They can be treated as resource).
	// Here use SharedPtr to handle this type of sharing. 
	CameraPtr 
		_pAttachedCamera;
	ScenePtr
		_pAttachedScene;
	HUDStagePtr
		_pAttachedHUDStage;

	ViewportInfo
		_viewportInfo;
	int
		_nZOrder;
	DWORD
		_backgroundColor;


private:
	//--- Viewport x,y and with,height that relative to the owner render target in scale. Range (0 ~ 1)
	float
		__fX; 
	float
		__fY;
	float
		__fWidth;
	float
		__fHeight;

	BOOL
		__bAutoAspectRatio;

};


DECLARE_LIST_TYPE_PTR(vector, RenderArea);

_X3D_NS_END