#pragma once

#if defined( X3D_EXPORTS )
#define _X3DExport __declspec(dllexport)
#else
#define _X3DExport __declspec(dllimport)
#endif

#define _X3D_NS_BEGIN namespace x3d {
#define _X3D_NS_END		}
#define _XGAME3D  ::x3d::


// --- Switchs ---
/* 
	define them in project pre-define macros
*/
/* Limit render FPS to release CPU time. */
// #define YIELD_CPU_TIME

_X3D_NS_BEGIN
_X3D_NS_END