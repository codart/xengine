#pragma once

#include "RenderStateNode.h"

_X3D_NS_BEGIN

class RenderStateMgr
{
public:
	RenderStateMgr(void);
	virtual ~RenderStateMgr(void);


public:
	void
		AddRenderable(RendableObject* pRenderable);
	void 
		RenderAll();


protected:
	RenderStateNode*
		_pRootStateNode;

};

_X3D_NS_END