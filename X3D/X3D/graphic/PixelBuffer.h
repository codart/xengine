#pragma once

#include "x3d/graphic/GraphicBuffer.h"
#include "PixelBox.h"

_X3D_NS_BEGIN

class GraphicSystem;

enum PixelBufferType
{
	INVALID_BUFFER_TYPE = -1,

	TEXTURE_PIXEL_BUFFER = 0,	// Texture's surface
	COLOR_BUFFER,			// A surface created with CreateRenderTarget in DX
	DEPTH_STENCIL_BUFFER,	// A surface created with CreateDepthStencilSurface in DX
	SWAP_BUFFER,			// A back/front buffer used for swapping. Such as IDirect3DDevice9::GetBackBuffer
};


/* 
	Wrap the IDirect3DSurface9 in DX and FrameBuffer in OGL.
	For DX, this buffer can be a Texture's surface, a RenderTarget surface or a DepthStencilSurface. 
*/
class _X3DExport PixelBuffer : public GraphicBuffer
{
protected:
	PixelBuffer(GraphicSystem* pGraphicSystem);
public:
	virtual ~PixelBuffer(void);


public:
	virtual Size
		GetBufferSize() const;

	virtual void 
		BitBlt(const PixelBuffer& srcBuffer, const Rect& srcRect, const Rect& destRect);

	virtual PixelBox
		LockRect(const common::Rect rect, LockOptions options);
	virtual void
		Unlock() { XASSERT(0); }


	virtual int
		GetWidth();
	virtual int
		GetHeight();

	PixelBufferType
		GeBufferType() { return _pixelBufferType; }


protected:
	PixelBufferType
		_pixelBufferType;


debug:
	debug_variable(BOOL, bManaged);


};

struct PixelBufferID
{
	PixelBufferID(PixelFormat pixelFormat, int nWidth, int nHeight)
	{
		this->pixelFormat = pixelFormat;
		this->nWidth = nWidth;
		this->nHeight = nHeight;
	}

	PixelFormat pixelFormat;
	Int nWidth;
	Int nHeight;
};

struct PixelBufferIDComparator
{
	bool operator()(const PixelBufferID& pixelBufferID0, const PixelBufferID& pixelBufferID1) const;
};

DECLARE_LIST_TYPE_PTR(vector, PixelBuffer);

typedef SharedPtr<PixelBuffer> PixelBufferPtr;

DECLARE_LIST_TYPE(vector, PixelBufferPtr);

typedef map<PixelBufferID, PixelBufferPtr, PixelBufferIDComparator> PixelBufferMap;
typedef PixelBufferMap::iterator PixelBufferMapIt;

_X3D_NS_END