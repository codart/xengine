#pragma once

#include "x3d/graphic/GraphicalObject.h"

_X3D_NS_BEGIN


#ifdef _DEBUG
enum GraphicBufferState
{
	GBS_UNINIT,
	GBS_INITED,
	GBS_HAS_VALID_DATA,
};
#endif

enum LockOptions
{
	/** Normal mode, allows read/write and contents are preserved. */
	LOCK_NORMAL = 0,
	/** Discards the entire</em> buffer while locking; this allows optimisation to be 
	performed because synchronisation issues are relaxed. Only allowed on buffers 
	created with the HBU_DYNAMIC flag. 
	*/
	LOCK_DISCARD = 0x00002000L,
	/** Lock the buffer for reading only. Not allowed in buffers which are created with HBU_WRITE_ONLY. 
	Mandatory on static buffers, i.e. those created without the HBU_DYNAMIC flag. 
	*/ 
	LOCK_READ_ONLY = 0x00000010L,
	/** As HBL_NORMAL, except the application guarantees not to overwrite any 
	region of the buffer which has already been used in this frame, can allow
	some optimisation on some APIs. */
	LOCK_NO_OVERWRITE = 0x00001000L,
};


class _X3DExport GraphicBuffer : public Resource
					, public GraphicalObject
{
public:
	GraphicBuffer(GraphicSystem* pGraphicSystem, BOOL bDynamic = FALSE);
	virtual ~GraphicBuffer(void);


public:
	override void
		Destroy();
	BOOL
		IsDynamic() { return _bDynamic; }


protected:
	Boolean
		_bDynamic;

debug:
	debug_variable(GraphicBufferState, bufferState);
	debug_variable(BOOL, bLocked);

};


_X3D_NS_END