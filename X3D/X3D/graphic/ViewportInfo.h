#pragma once

_X3D_NS_BEGIN

class ViewportInfo
{
public:
	DWORD       X;
	DWORD       Y;            /* Viewport Top left */
	DWORD       Width;
	DWORD       Height;       /* Viewport Dimensions */
	float       MinZ;         /* Min/max of clip Volume */
	float       MaxZ;
};


_X3D_NS_END