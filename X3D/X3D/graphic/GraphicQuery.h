#pragma once

#include "x3d/graphic/GraphicalObject.h"

_X3D_NS_BEGIN

class _X3DExport GraphicQuery : public GraphicalObject
{
public:
	GraphicQuery(GraphicSystem* pGraphicSystem);
	~GraphicQuery(void);
};

_X3D_NS_END