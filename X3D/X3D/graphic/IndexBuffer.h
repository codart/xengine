#pragma once

#include "x3d/graphic/GeometryBuffer.h"

_X3D_NS_BEGIN

class _X3DExport IndexBuffer : public GeometryBuffer
{
	need_direct_concrete

public:
	IndexBuffer(GraphicSystem* pGraphicSystem, UINT nIndexCount, UINT nIndexSize, BOOL bDynamic = FALSE);
	virtual ~IndexBuffer(void);


public:
	override void 
		WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer = false);
	override UINT
		GetBufferSize() const {return _nIndexCount * _nIndexSize;};


	INLINE UINT
		GetIndexCount() const { return _nIndexCount; };
	INLINE UINT
		GetIndexSize() const { return _nIndexSize; };


protected:
	Uint
		_nIndexCount;
	Uint
		_nIndexSize; // 16bit or 32bit

};

typedef SharedPtr<IndexBuffer> IndexBufferPtr;
DECLARE_MAP_TYPE(map, String, IndexBufferPtr);
DECLARE_LIST_TYPE(vector, IndexBufferPtr);

typedef OwnerPtr<IndexBufferPtr> IndexBufferOwnerPtr;
DECLARE_MAP_TYPE(map, String, IndexBufferOwnerPtr);


_X3D_NS_END