#pragma once

#include "RenderTarget.h"

_X3D_NS_BEGIN


class _X3DExport WindowRenderTarget : public RenderTarget
{
	need_direct_concrete

	friend class RenderWindow;
	friend class GraphicSystem;

public:
	WindowRenderTarget(GraphicSystem* pGraphicSystem, Window* pAttachedWindow);
	virtual ~WindowRenderTarget();


public:
	// Present to the window that binded to current window render target.
	virtual DWORD 
		Present();

	// Present to specified window
	virtual DWORD
		Present(transient Window* pWindow);


protected:
	virtual void 
		_onWindowSizeChanged(const Event* pEvent);
	
	virtual void
		_onWndSizeChangedImpl() = 0;


protected:
	Window* 
		_pAttachedWindow;

	// True if resources acquired.
	BOOL					
		_acquired;						

};



_X3D_NS_END