#include "StdAfx.h"
#include "GraphicSystemCap.h"

_X3D_NS_BEGIN


GraphicSystemCap::GraphicSystemCap(void)
{
	_nMaxVertexStreamCount = 4;
	_nMultiRenderTargetCount = 4;
	_nMaxTextureUnitCount = 8;
	_nMaxVertexTextureUnitCount = 4;
}

GraphicSystemCap::~GraphicSystemCap(void)
{

}

_X3D_NS_END