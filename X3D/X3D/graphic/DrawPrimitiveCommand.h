#pragma once

#include "x3d/graphic/GraphicDefs.h"

_X3D_NS_BEGIN

class VertexData;
class IndexData;


class DrawPrimitiveCommand : public Final<DrawPrimitiveCommand>
{
public:
	DrawPrimitiveCommand(void);
	~DrawPrimitiveCommand(void);


public:
	INLINE VertexData*
		GetVertexData() const { return _pVertexData; };
	INLINE IndexData*
		GetIndexData() const { return _pIndexData; };

	INLINE void
		SetVertexData(VertexData* pVertexData){ _pVertexData = pVertexData; };
	INLINE void
		SetIndexData(IndexData* pIndexData){ _pIndexData = pIndexData; };

	INLINE DrawPrimitiveType
		GetDrawPrimitiveType() const { XASSERT(_operationType != DRAW_INVALID); return _operationType; };
	INLINE void
		SetDrawPrimitiveType(DrawPrimitiveType operationType){ _operationType = operationType; };
	INLINE Int
		GetPrimitiveCount() const { XASSERT(_nCachedPrimitiveCount != -1); return _nCachedPrimitiveCount; }
	INLINE void
		SetPrimitiveCount(UINT nPrimitiveCount) { _nCachedPrimitiveCount = nPrimitiveCount; }


protected:
	/// Vertex source data
	VertexData*
		_pVertexData;
	IndexData*
		_pIndexData;

	/// The type of operation to perform
	DrawPrimitiveType 
		_operationType;

	// Note: need recalculate this count when vertex/index/operationType changed!
	Int
		_nCachedPrimitiveCount;

};


_X3D_NS_END