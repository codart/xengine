#pragma once

// Note: All this state has same value as DX9. It means a conversion is needed when using other API.


enum DrawPrimitiveType {
	DRAW_INVALID = -1, 
	/// A list of points, 1 vertex per point
	DRAW_POINT_LIST = 1,
	/// A list of lines, 2 vertices per line
	DRAW_LINE_LIST = 2,
	/// A strip of connected lines, 1 vertex per line plus 1 start vertex
	DRAW_LINE_STRIP = 3,
	/// A list of triangles, 3 vertices per triangle
	DRAW_TRIANGLE_LIST = 4,
	/// A strip of triangles, 3 vertices for the first triangle, and 1 per triangle after that 
	DRAW_TRIANGLE_STRIP = 5,
	/// A fan of triangles, 3 vertices for the first triangle, and 1 per triangle after that
	DRAW_TRIANGLE_FAN = 6,
};

// Defines the frame buffer types. GraphicSystem::ClearFrameBuffer will utilize these values to determine which 
// buffer will be cleared.
enum FrameBufferType {
	FRAME_BUFFER_COLOR  = 0x1,
	FRAME_BUFFER_DEPTH   = 0x2,
	FRAME_BUFFER_STENCIL = 0x4,

	FRAME_BUFFER_ALL = FRAME_BUFFER_COLOR | FRAME_BUFFER_DEPTH | FRAME_BUFFER_STENCIL,
};

enum FrameBlendFactor
{
	FACTOR_ZERO = 1,
	FACTOR_ONE = 2,

	FACTOR_SRC_COLOR,
	FACTOR_INV_SRC_COLOR,

	FACTOR_SRC_ALPHA,
	FACTOR_INV_SRC_ALPHA,

	FACTOR_DEST_ALPHA,
	FACTOR_INV_DEST_ALPHA,

	FACTOR_DEST_COLOR,
	FACTOR_INV_DEST_COLOR,
};

enum FrameBlendOperation
{
	FRAME_BLEND_ADD = 1,
	FRAME_BLEND_SUBTRACT,
	FRAME_BLEND_REVERSE_SUBTRACT,
	FRAME_BLEND_MIN,
	FRAME_BLEND_MAX
};

enum HardwareCullMode
{
	/// Hardware never culls triangles and renders everything it receives.
	CULL_NONE = 1,
	/// Hardware culls triangles whose vertices are listed clockwise in the view (default).
	CULL_CLOCKWISE = 2,
	/// Hardware culls triangles whose vertices are listed anticlockwise in the view.
	CULL_ANTICLOCKWISE = 3
};

enum CompareFunction
{
	COMPARE_FUNC_ALWAYS_FAIL = 1,
	COMPARE_FUNC_LESS,
	COMPARE_FUNC_EQUAL,
	COMPARE_FUNC_LESS_EQUAL,

	COMPARE_FUNC_GREATER,
	COMPARE_FUNC_NOT_EQUAL,
	COMPARE_FUNC_GREATER_EQUAL,
	COMPARE_FUNC_ALWAYS_PASS,
};

enum StencilOperation
{
	/// Leave the stencil buffer unchanged7
	STENCIL_OP_KEEP = 1,
	/// Set the stencil value to zero
	STENCIL_OP_ZERO = 2,
	/// Set the stencil value to the reference value
	STENCIL_OP_REPLACE = 3,
	/// Increase the stencil value by 1, clamping at the maximum value
	STENCIL_OP_INCREMENT = 4,
	/// Decrease the stencil value by 1, clamping at 0
	STENCIL_OP_DECREMENT = 5,
	/// Invert the bits of the stencil buffer
	STENCIL_OP_INVERT = 6,
	/// Increase the stencil value by 1, wrapping back to 0 when incrementing the maximum value
	STENCIL_OP_INCREMENT_WRAP = 7,
	/// Decrease the stencil value by 1, wrapping when decrementing 0
	STENCIL_OP_DECREMENT_WRAP = 8,

};