#pragma once

#include "x3d/graphic/GraphicalInfo.h"

_X3D_NS_BEGIN

class _X3DExport GraphicSystemCap : GraphicalInfo
{
public:
	GraphicSystemCap(void);
	~GraphicSystemCap(void);

public:
	INLINE int
		GetMaxVertexStreamCount() const { return _nMaxVertexStreamCount; }
	INLINE int 
		GetMultiRenderTargetCount() const { return _nMultiRenderTargetCount; }
	INLINE int 
		GetMaxTextureUnitCount()  const{ return _nMaxTextureUnitCount; }
	INLINE int 
		GetMaxVertexTextureUnitCount()  const{ return _nMaxVertexTextureUnitCount; }


protected:
	Int 
		_nMaxVertexStreamCount;
	// Simultaneous render target count supported.
	Int 
		_nMultiRenderTargetCount;
	Int 
		_nMaxTextureUnitCount;
	Int 
		_nMaxVertexTextureUnitCount;

};

_X3D_NS_END