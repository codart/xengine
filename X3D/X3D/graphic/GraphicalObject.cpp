#include "StdAfx.h"
#include "GraphicalObject.h"

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

GraphicalObject::GraphicalObject(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;
}

GraphicalObject::~GraphicalObject(void)
{

}

BOOL GraphicalObject::QueryInternalHandle(OUT InternalObjectHandle* ppInternalObjectHandle)
{
	SASSERT(0 && "Warning: Call any internal object's method will break the encapsulation!!!")
	
	InternalObjectHandle hInternalHandle = _getInternalObjHandle();
	if(hInternalHandle)
	{
		*ppInternalObjectHandle = hInternalHandle;
		return TRUE;
	}
	else
	{
		*ppInternalObjectHandle = NULL;
		return FALSE;
	}
}

void GraphicalObject::SetInternalHandle(IN InternalObjectHandle pInternalObjectHandle)
{
	SASSERT(0 && "Warning: Call any internal object's method will break the encapsulation!!!");

	_setInternalObjHandle(pInternalObjectHandle);
}

InternalObjectHandle GraphicalObject::_getInternalObjHandle()
{
	XASSERT(0);
	return 0;
}

void GraphicalObject::_setInternalObjHandle(InternalObjectHandle pInternalHandle)
{
	XASSERT(0);
}

_X3D_NS_END