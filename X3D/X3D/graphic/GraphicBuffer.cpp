#include "stdafx.h"
#include "GraphicBuffer.h"

#include "GraphicSystem.h"

_X3D_NS_BEGIN

GraphicBuffer::GraphicBuffer(GraphicSystem* pGraphicSystem, BOOL bDynamic)
: GraphicalObject(pGraphicSystem)
{
	_bDynamic = bDynamic;
	debug_assign(bufferState, GBS_UNINIT);
	debug_assign(bLocked, FALSE);
}

GraphicBuffer::~GraphicBuffer(void)
{

}

void GraphicBuffer::Destroy()
{
	delete this;
}


_X3D_NS_END