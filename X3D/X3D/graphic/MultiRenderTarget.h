#pragma once

#include "x3d/graphic/RenderTarget.h"
#include "x3d/graphic/PixelBuffer.h"

_X3D_NS_BEGIN

/*
	A target that has multi color buffers and one depth-stencil buffer. 
	The color buffers can be a RenderTexture's buffer(0), or a dedicated color buffer create by 
	GraphicSystem::CreatePixelBuffer().
*/
class _X3DExport MultiRenderTarget
{
public:
	MultiRenderTarget(void);
	virtual ~MultiRenderTarget(void);


public:
	void 
		BindDepthStencilBuffer(PixelBuffer* pDepthStencilBuffer);
	void
		BindColorBuffer(PixelBuffer* pColorBuffer);
	void 
		UnbindColorBuffer(PixelBuffer* pColorBuffer);

	INLINE UINT
		GetColorBufferCount(){ return _colorBufferList.size(); };
	INLINE PixelBuffer*
		GetColorBuffer(UINT iIndex) const { XASSERT(iIndex >=0 && iIndex < _colorBufferList.size()); 
											return _colorBufferList.at(iIndex); }
	INLINE const PixelBufferList&
		GetColorBuffers() { return _colorBufferList; }

	INLINE PixelBuffer*
		GetDepthStencilBuffer() { return _pDepthStencilBuffer; }


protected:
	PixelBufferList
		_colorBufferList;
	PixelBuffer*
		_pDepthStencilBuffer;

};

_X3D_NS_END