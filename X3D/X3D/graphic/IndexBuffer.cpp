#include "StdAfx.h"
#include "IndexBuffer.h"

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

IndexBuffer::IndexBuffer(GraphicSystem* pGraphicSystem, UINT nIndexCount
						, UINT nIndexSize, BOOL bDynamic)
: GeometryBuffer(pGraphicSystem)
{
	_nIndexCount = nIndexCount;
	_nIndexSize = nIndexSize;
}

IndexBuffer::~IndexBuffer(void)
{
}

void IndexBuffer::WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer)
{
	XASSERT(0);
}

_X3D_NS_END