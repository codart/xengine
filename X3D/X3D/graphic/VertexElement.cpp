#include "StdAfx.h"
#include "VertexElement.h"

_X3D_NS_BEGIN

VertexElement::VertexElement(UINT iStream, UINT nOffset, VertexElementFormat format
							, VertexElementSemantic semantic, UINT semanticIndex)
{
	this->iStream = iStream;
	this->nOffset = nOffset;
	this->format = format;
	this->semantic = semantic;
	this->semanticIndex = semanticIndex;
}

VertexElement::~VertexElement(void)
{
}


_X3D_NS_END