#include "stdafx.h"
#include "VertexBuffer.h"

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

VertexBuffer::VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
: GeometryBuffer(pGraphicSystem, bDynamic)
{
	_nVertexCount = nVertexCount;
	_nVertexSize = nVertexSize;
}

VertexBuffer::~VertexBuffer(void)
{
}

_X3D_NS_END