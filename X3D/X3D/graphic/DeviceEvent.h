#pragma once

_X3D_NS_BEGIN

class GraphicSystem;

class _X3DExport DeviceEvent : public Event
{
public:
	enum DeviceEventType
	{
		DEVICE_EVENT_START,

		DEVICE_BEFORE_INIT,
		DEVICE_BEFORE_UNINIT,

		DEVICE_AFTER_INIT,
		DEVICE_AFTER_UNINIT,

		DEVICE_LOST,
		DEVICE_RESET,

		DEVICE_EVENT_END,
	};

	DeviceEvent(int nType, GraphicSystem* pGraphicSystem_) 
		: Event(nType), pGraphicSystem(pGraphicSystem_)
	{};

public:
	GraphicSystem*
		pGraphicSystem;

};


_X3D_NS_END