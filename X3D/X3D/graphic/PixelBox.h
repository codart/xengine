#pragma once

_X3D_NS_BEGIN

/** 
	A simple wrapper for pixels in memory of a volume (3D), image (2D) or line (1D).
*/
class _X3DExport PixelBox
{
public:
	PixelBox();
	~PixelBox();


protected:
	PixelFormat 
		_pixelFormat;


};


_X3D_NS_END