#include "stdafx.h"
#include "RenderTarget.h"

#include "d3d9.h"

#include "x3d/graphic/PixelBuffer.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

RenderTarget::RenderTarget(GraphicSystem* pGraphicSystem)
: GraphicalObject(pGraphicSystem)
{
	debug_assign(_bManagedColorBuf, FALSE);
	debug_assign(_bManagedDSBuf, FALSE);
}

RenderTarget::~RenderTarget()
{
	//... Add checking code here, if managed, check the refCount == 1

#ifdef _DEBUG
	if(_bManagedColorBuf)
	{
		XASSERT(_pColorBffer.GetReferenceCount() == 1);
	}
	if(_bManagedDSBuf)
	{
		XASSERT(_pDepthStencilBuffer.GetReferenceCount() == 1);
	}
#endif
}

void RenderTarget::BindBuffers(PixelBufferPtr& pColorBuffer, PixelBufferPtr& pSharedDepthStencilBuffer)
{
	XASSERT(!_pColorBffer);
	XASSERT(!_pDepthStencilBuffer);

	_pColorBffer = pColorBuffer;
	_pDepthStencilBuffer = pSharedDepthStencilBuffer;

	debug_assign(_bManagedColorBuf, FALSE);
	debug_assign(_bManagedDSBuf, FALSE);
}

int RenderTarget::GetWidth()
{
	XASSERT(_pColorBffer && _pDepthStencilBuffer);
	return _pColorBffer->GetWidth();
}

int RenderTarget::GetHeight()
{
	XASSERT(_pColorBffer && _pDepthStencilBuffer);
	return _pColorBffer->GetHeight();
}

_X3D_NS_END