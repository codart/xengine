#include "StdAfx.h"
#include "MultiRenderTarget.h"

_X3D_NS_BEGIN

MultiRenderTarget::MultiRenderTarget(void)
{
}

MultiRenderTarget::~MultiRenderTarget(void)
{

}

void MultiRenderTarget::BindDepthStencilBuffer(PixelBuffer* pDepthStencilBuffer)
{
	_pDepthStencilBuffer = pDepthStencilBuffer;
}

void MultiRenderTarget::BindColorBuffer(PixelBuffer* pColorBuffer)
{
	_colorBufferList.push_back(pColorBuffer);
}

void MultiRenderTarget::UnbindColorBuffer(PixelBuffer* pColorBuffer)
{
	PixelBufferIterator colorBufferIterator(_colorBufferList);	
	PixelBufferListIt itFinded = colorBufferIterator.Find(pColorBuffer);

	if(itFinded != _colorBufferList.end())
	{
		_colorBufferList.erase(itFinded);
	}
	else
	{
		XASSERT(0);
	}
}

_X3D_NS_END