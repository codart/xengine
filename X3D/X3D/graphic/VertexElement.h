#pragma once

#include "x3d/graphic/GraphicalInfo.h"

_X3D_NS_BEGIN

/// Vertex element type, used to identify the base types of the vertex contents
enum VertexElementFormat
{
	VEF_FLOAT1 = 0,
	VEF_FLOAT2 = 1,
	VEF_FLOAT3 = 2,
	VEF_FLOAT4 = 3,
	/// alias to more specific colour type - use the current rendersystem's colour packing
	VEF_COLOUR = 4,
	VEF_SHORT1 = 5,
	VEF_SHORT2 = 6,
	VEF_SHORT3 = 7,
	VEF_SHORT4 = 8,
	VEF_UBYTE4 = 9,
	/// D3D style compact colour
	VEF_COLOUR_ARGB = 10,
	/// GL style compact colour
	VEF_COLOUR_ABGR = 11
};

/// Vertex element semantics, used to identify the meaning of vertex buffer contents
enum VertexElementSemantic {
	/// Position, 3 reals per vertex
	VES_POSITION = 1,
	/// Blending weights
	VES_BLEND_WEIGHTS = 2,
	/// Blending indices
	VES_BLEND_INDICES = 3,
	/// Normal, 3 reals per vertex
	VES_NORMAL = 4,
	/// Diffuse colours
	VES_DIFFUSE = 5,
	/// Specular colours
	VES_SPECULAR = 6,
	/// Texture coordinates
	VES_TEXTURE_COORDINATES = 7,
	/// Binormal (Y axis if normal is Z)
	VES_BINORMAL = 8,
	/// Tangent (X axis if normal is Z)
	VES_TANGENT = 9

};

class VertexElement : public GraphicalInfo
                    , public Final<VertexElement> 
{
public:
	VertexElement(UINT iStream, UINT nOffset, VertexElementFormat format
					, VertexElementSemantic semantic, UINT semanticIndex);

	~VertexElement(void);

public:
	INLINE UINT
		GetStream() const {return iStream;};
	INLINE UINT
		GetOffset() const {return nOffset;};
	INLINE VertexElementFormat
		GetFormat() const {return format;};
	INLINE VertexElementSemantic
		GetSemantic() const {return semantic;};
	INLINE UINT
		GetSemanticIndex() const {return semanticIndex;};


protected:
	Uint 
		iStream;
	Uint 
		nOffset;
	VertexElementFormat 
		format;
	VertexElementSemantic 
		semantic;
	Uint 
		semanticIndex;
};


DECLARE_LIST_TYPE(vector, VertexElement);


_X3D_NS_END