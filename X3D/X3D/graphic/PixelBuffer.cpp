#include "StdAfx.h"
#include "PixelBuffer.h"

_X3D_NS_BEGIN


PixelBuffer::PixelBuffer(GraphicSystem* pGraphicSystem)
: GraphicBuffer(pGraphicSystem)
{
	debug_assign(bManaged, TRUE);
	_pixelBufferType = INVALID_BUFFER_TYPE;
}

PixelBuffer::~PixelBuffer(void)
{

}

int PixelBuffer::GetWidth()
{
	XASSERT(0);
	return 0;
}

int PixelBuffer::GetHeight()
{
	XASSERT(0);
	return -1;
}

void PixelBuffer::BitBlt(const PixelBuffer& srcBuffer, const Rect& srcRect, const Rect& destRect)
{
	XASSERT(0);
}

PixelBox PixelBuffer::LockRect(const common::Rect rect, LockOptions options)
{
	PixelBox pixelBox;
	XASSERT(0);
	return pixelBox;
}

Size PixelBuffer::GetBufferSize() const
{
	XASSERT(0);
	return Size(0, 0);
}

//-------
bool PixelBufferIDComparator::operator()(const PixelBufferID& pixelBufferID0, const PixelBufferID& pixelBufferID1) const
{
	if (pixelBufferID0.pixelFormat < pixelBufferID1.pixelFormat)
		return true;

	if (pixelBufferID0.pixelFormat == pixelBufferID1.pixelFormat)
	{
		if (pixelBufferID0.nWidth < pixelBufferID1.nWidth)
			return true;

		if (pixelBufferID0.nWidth == pixelBufferID1.nWidth)
		{
			if (pixelBufferID0.nHeight < pixelBufferID1.nHeight)
				return true;
		}
	}

	return false;
}

_X3D_NS_END