#pragma once

#include "x3d/graphic/GeometryBuffer.h"

_X3D_NS_BEGIN


class _X3DExport VertexBuffer : public GeometryBuffer
{
public:
	VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic);
	virtual ~VertexBuffer(void);


public:
	INLINE UINT
		GetVertexCount() const {return _nVertexCount;};
	INLINE UINT
		GetVertexSize() const {return _nVertexSize;};
	INLINE UINT
		GetBufferSize() const { return _nVertexCount * _nVertexSize; }


protected:
	Uint
		_nVertexCount;
	Uint
		_nVertexSize;

};

typedef SharedPtr<VertexBuffer> VertexBufferPtr;
DECLARE_MAP_TYPE(map, String, VertexBufferPtr);
DECLARE_LIST_TYPE(vector, VertexBufferPtr);

typedef OwnerPtr<VertexBufferPtr> VertexBufferOwnerPtr;
DECLARE_MAP_TYPE(map, String, VertexBufferOwnerPtr);


_X3D_NS_END