#pragma once

#include "x3d/graphic/GraphicalObject.h"

struct IDirect3DSurface9;

_X3D_NS_BEGIN

class RenderTarget;

class RenderTargetEvent : public Event
{
public:
	enum RenderTargetEventType
	{
		RENDERTARGET_EVENT_START = 2100,
		
		SIZE_CHANGED,

		RENDERTARGET_EVENT_END,
	};

	RenderTargetEvent(int nType, RenderTarget* pRenderTarget_) 
					: Event(nType), pRenderTarget(pRenderTarget_)
	{}

	static BOOL IsValidEvent(int nType)
	{
		return nType > RENDERTARGET_EVENT_START && nType < RENDERTARGET_EVENT_END;
	}


public:
	RenderTarget*
		pRenderTarget;
};


class PixelBuffer;
class GraphicSystem;

/** 
	A pixel buffer which can receive the results of a rendering
    operation.

    @remarks
 */
class _X3DExport RenderTarget : public GraphicalObject
                              , public EventDispatcher
{
	no_direct_concrete

protected:
	RenderTarget(GraphicSystem* pGraphicSystem);
public:
	virtual ~RenderTarget();
	/// Ensure not allocated on stack
	static RenderTarget* Create(GraphicSystem* pGraphicSystem) { return XNEW RenderTarget(pGraphicSystem); }

public:
	int 
		GetWidth();
	int
		GetHeight();

	INLINE PixelBuffer*
		GetColorBuffer(){ return _pColorBffer; }
	INLINE PixelBuffer*
		GetDepthStencilBuffer(){ return _pDepthStencilBuffer; }

/*
	/ * 
		Bind to color and depth buffer. These buffers will be managed by this rendertarget, and will be released 
		during its destruction.
	* /
	void 
		BindBuffers(PixelBuffer* pColorBuffer, PixelBuffer* pDepthStencilBuffer);*/

	/* 
		Bind to color and depth buffer. These color buffer will be managed by this rendertarget, and will be released 
		during its destruction. The depth buffer is a shared buffer(sharing using the SharedPtr).
	*/
	void
		BindBuffers(PixelBufferPtr& pColorBuffer, PixelBufferPtr& pSharedDepthStencilBuffer);
	

protected:
	String
		_strName;

	PixelBufferPtr
		_pColorBffer;
	PixelBufferPtr
		_pDepthStencilBuffer;

	debug_variable(BOOL, _bManagedColorBuf);
	debug_variable(BOOL, _bManagedDSBuf);

};

_X3D_NS_END