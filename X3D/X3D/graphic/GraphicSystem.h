#pragma once

#include "Common/core/Event.h"
#include "Common/core/EventDispatcher.h"
#include "x3d/graphic/GraphicDefs.h"
#include "x3d/graphic/ViewportInfo.h"
#include "x3d/material/TextureUnitState.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/datadef/PixelFormat.h"
#include "x3d/graphic/GraphicDefs.h"

_X3D_NS_BEGIN

class GraphicSystemCap;

class WindowRenderTarget;
class RenderTarget;
class MultiRenderTarget;

class Texture;
class RenderTexture;
class DynamicTexture;

class Viewport;
class VertexLayout;

class VertexProgram;
class FragmentProgram;

class IndexBuffer;
class VertexBuffer;
class PixelBuffer;

class TextureUnitState;
class Shader;
class DrawPrimitiveCommand;

/*
	Provide an abstract interface to wrap the graphic API(DX, OGL or any other graphics API). Provides methods
	such as:
	1. Create graphic related resources (render target, texture, shader, etc.)
	2. Clear frame buffer, DrawPrimitive primitives
	3. Set rendering state
*/
class _X3DExport GraphicSystem : public EventDispatcher
								, public NonCopyable
{
public:
	GraphicSystem(void);
	virtual ~GraphicSystem(void);


public:
	virtual void 
		Init(DWORD_PTR dwInitData = 0);
	virtual void 
		Uninit();

	const GraphicSystemCap&
		GetGraphicSystemCap() const { XASSERT(_pGraphicSysCap); return *_pGraphicSysCap; }

	/*
		Note: IsReady method not only check the device state, but also reset the device when device is ready.
		Should call this method before call any other drawing methods in a frame. 
	*/
	virtual BOOL
		IsReady();

	// --- Factory methods for create device related resources ---
	
	virtual VertexProgram*
		CreateVertexProgram() { XASSERT(0); return NULL; };
	virtual FragmentProgram*
		CreateFragmentProgram(){ XASSERT(0); return NULL; };

	// Create a DepthStencil buffer
	virtual PixelBuffer*
		CreateDepthStencilBuffer(int nWidth, int nHeight, PixelFormat pixelFormat = PIXEL_FORMAT_D24S8);

	virtual MultiRenderTarget*
		CreateMultiRenderTarget();
	virtual WindowRenderTarget* 
		CreateWindowRenderTarget(Window* pWindow);
	virtual RenderTexture*
		CreateRenderTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat
							, PixelFormat depthStencilFormat);
	virtual DynamicTexture*
		CreateDynamicTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat);
	virtual Texture*
		CreateTexture(TextureType textureType, const String& strName);
	virtual PixelBufferPtr
		GetSharedDSBuffer(int nWidth, int nHeight, PixelFormat pixelFormat = PIXEL_FORMAT_D24S8);

	virtual VertexLayout*
		CreateVertexLayout();
	virtual VertexBuffer*
		CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic = FALSE);
	virtual IndexBuffer*
		CreateIndexBuffer(UINT nIndexCount, UINT nIndexSize, BOOL bDynamic = FALSE);


	// --- Rendering related methods --- 
	// Clear render buffer/depth stencil buffer
	virtual void 
		ClearFrameBuffer(DWORD dwBufferFlag = FRAME_BUFFER_COLOR | FRAME_BUFFER_DEPTH | FRAME_BUFFER_STENCIL
						, COLOR color = COLOR_ARGB(0, 0, 0, 0)
						, Real depth = 1.0f
						, unsigned short stencil = 0);
	
	INLINE virtual void
		SetVertexLayout(VertexLayout* pVertexDeclearation);

	virtual void
		SetViewport(const ViewportInfo* pViewportInfo);

	virtual BOOL
		SetRenderTarget(RenderTarget* pRenderTarget);
	virtual BOOL
		SetMultiRenderTarget(MultiRenderTarget* pMultiRenderTarget);
	virtual BOOL
		SetColorBuffer(int iBufferIndex, PixelBuffer* pColorBuffer);
	virtual BOOL
		SetDepthStencilBuffer(PixelBuffer* pColorBuffer);

	virtual void
		SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha);
	virtual void
		EnableColorBufferWrite(BOOL bEnable);


	INLINE virtual void
		BeginUpdateWindow(WindowRenderTarget* pWndTarget);
	INLINE virtual void
		EndUpdateWindow();

	virtual DWORD
		BeginDrawing();
	virtual DWORD
		EndDrawing();

	virtual void
		SetSceneBlending(FrameBlendFactor sourceFactor
		, FrameBlendFactor destFactor
		, FrameBlendOperation blendOp = FRAME_BLEND_ADD);

	virtual void
		SetAlphaTestParams(BOOL bEnable, CompareFunction func, BYTE value, BOOL alphaToCoverage = FALSE);

	virtual void 
		SetDepthBufferParams(BOOL bDepthTest
							, BOOL bDepthWrite
							, CompareFunction depthFunction = COMPARE_FUNC_LESS_EQUAL);

	virtual void
		SetStencilBufferParams(BOOL bEnableStencil
								, CompareFunction func = COMPARE_FUNC_ALWAYS_PASS
								, UINT refValue = 0, UINT mask = 0xFFFFFFFF
								, StencilOperation stencilFailOp = STENCIL_OP_KEEP
								, StencilOperation depthFailOp = STENCIL_OP_KEEP
								, StencilOperation stencilAndZPassOp = STENCIL_OP_KEEP
								, BOOL bTwoSidedOperation = FALSE);

	/* 
		If pTextureUnitState == NULL, will disable the texture stage. 
	*/
	virtual void
		SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	/* 
		Note: all texture unit state in the list must not be NULL!!! This 
		method should responsible for disable all the unused texture unit stage too!!!.
	*/
	virtual void
		SetTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	// Set the texture state(such as uvw address mode, filter mode) for the specified texture unit.
	virtual void 
		SetTextureState(UINT texUnitIndex, TextureState* pTextureState);
	// Set the texture for the specified texture unit.
	virtual void
		SetTexture(UINT texUnitIndex, Texture* pTexture);

	virtual void
		SetVertexTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	virtual void
		SetVertexTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	virtual void
		SetCullMode(HardwareCullMode cullMode);

	virtual void
		SetShader(Shader* pShader);

	virtual DWORD
		DrawPrimitive(const DrawPrimitiveCommand* pDrawPrimitiveCmd);

	virtual DWORD 
		SetVertexBuffer(int iStreamIndex, VertexBuffer* pVertexBuffer);
	virtual void
		SetVertexBuffers(const VertexBufferPtrList& vertexBufferList);

	virtual DWORD
		SetIndexBuffer(IndexBuffer* pIndexBuffer);

	virtual DWORD
		DrawPrimitive(DrawPrimitiveType primitiveType, int nVertexCount, int iStartIndex, int nPrimitiveCount);

	virtual void
		SetClipPlane(UINT iIndex, BOOL bEnable = TRUE, const Plane& clipPlane = Plane(0, 1, 0, 0));


package:
	// Create a pixel buffer object only. 
	virtual PixelBuffer*
		createPixelBuffer();


protected:
	GraphicSystemCap*
		_pGraphicSysCap;

	transient Shader*
		_pCurrentShader; //...????
	transient RenderTarget*
		_pCurrentRenderTarget; //...????

	// Be updated when BeginUpdateWindow, and be cleared after EndUpdateWindow
	transient WindowRenderTarget*
		_pCurWndRenderTarget;

	///------------------------------------------
	uint32_t
		_drawCount;


protected:
	PixelBufferMap
		__sharedDSBuffers;

};


_X3D_NS_END