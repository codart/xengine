#pragma once

#include "VertexElement.h"
#include "x3d/graphic/GraphicalObject.h"

_X3D_NS_BEGIN

class GraphicSystem;

class _X3DExport VertexLayout : public GraphicalObject
{
	need_direct_concrete

public:
	VertexLayout(GraphicSystem* pGraphicSystem);
	virtual ~VertexLayout(void);


public:
	virtual void
		BeginUpdate();
	virtual const VertexElement& 
		AddElement(unsigned short iStreamIndex, size_t offset, VertexElementFormat format
					, VertexElementSemantic semantic, unsigned short semanticIndex = 0);
	virtual void 
		EndUpdate() = 0;

	virtual UINT 
		GetElementCount() const {return _vertexElementList.size(); };

	
protected:
	VertexElementList
		_vertexElementList;
	
};


_X3D_NS_END