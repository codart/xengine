#pragma once

#include "GraphicBuffer.h"

_X3D_NS_BEGIN

class GeometryBuffer : public GraphicBuffer
{
public:
	GeometryBuffer(GraphicSystem* pGraphicSystem, BOOL bDynamic = FALSE)
		: GraphicBuffer(pGraphicSystem, bDynamic)
	{

	}

public:
	virtual void 
		WriteData(UINT offset, UINT nByteCount, const void* pSource, BOOL discardWholeBuffer = false) = 0;

	virtual UINT
		GetBufferSize() const = 0;

	// Returns pointer to the locked memory
	virtual void*
		Lock(UINT OffsetToLock, UINT nByteCount, LockOptions dwLockFlags) { XASSERT(0); return NULL; }
	virtual void
		Unlock() { XASSERT(0); };


protected:

};

_X3D_NS_END
