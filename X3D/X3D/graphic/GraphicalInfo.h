#pragma once

_X3D_NS_BEGIN

/* 
	Base class for all information structure.
	Note: it should not has any virtual method. A info structure may be inited by 
	memcpy/memset, virtual methods will be erased if has.
*/
class GraphicalInfo
{
public:
	GraphicalInfo(void);
	~GraphicalInfo(void);
};


_X3D_NS_END