#include "stdafx.h"
#include "VertexLayout.h"

#include "X3D/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

VertexLayout::VertexLayout(GraphicSystem* pGraphicSystem)
: GraphicalObject(pGraphicSystem)
{
}

VertexLayout::~VertexLayout(void)
{
}

void VertexLayout::BeginUpdate()
{
	_vertexElementList.clear();
}

void VertexLayout::EndUpdate()
{

}

const VertexElement& VertexLayout::AddElement(unsigned short iStreamIndex, size_t offset, VertexElementFormat format
								, VertexElementSemantic semantic, unsigned short semanticIndex)
{
	_vertexElementList.push_back(VertexElement(iStreamIndex, offset, format, semantic, semanticIndex));
	return _vertexElementList.back();
}

_X3D_NS_END