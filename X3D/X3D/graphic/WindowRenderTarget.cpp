#include "stdafx.h"
#include "WindowRenderTarget.h"

#include "x3d/graphic/PixelBuffer.h"
#include "X3D/graphic/GraphicSystem.h"
#include "Common/plantform/WindowEvent.h"

_X3D_NS_BEGIN


WindowRenderTarget::WindowRenderTarget(GraphicSystem* pGraphicSystem, Window* pAttachedWindow)
: RenderTarget(pGraphicSystem)
{
	_pAttachedWindow = pAttachedWindow;
	XASSERT(_pAttachedWindow);

	_acquired = false;
}

WindowRenderTarget::~WindowRenderTarget()
{

}

DWORD WindowRenderTarget::Present()
{
	XASSERT(0);
	return -1;
}

DWORD WindowRenderTarget::Present(Window* pWindow)
{
	XASSERT(0);
	return -1;	
}

void WindowRenderTarget::_onWindowSizeChanged(const Event* pEvent)
{
	const WindowEvent* pWindowEvent = SAFE_CAST(pEvent, const WindowEvent*);

	_onWndSizeChangedImpl();

	RenderTargetEvent e(RenderTargetEvent::SIZE_CHANGED, this);
	DispatchEvent(e);
}

_X3D_NS_END