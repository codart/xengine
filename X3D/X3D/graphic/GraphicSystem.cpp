#include "StdAfx.h"
#include "GraphicSystem.h"

#include "x3d/graphic/MultiRenderTarget.h"
#include "x3d/graphic/GraphicSystemCap.h"
#include "x3d/graphic/DeviceEvent.h"

_X3D_NS_BEGIN

GraphicSystem::GraphicSystem(void)
{
	_pCurrentShader = NULL;
	_pCurrentRenderTarget = NULL;
	_pCurWndRenderTarget = NULL;
	_pGraphicSysCap = NULL;

	_drawCount = 0;
}

GraphicSystem::~GraphicSystem(void)
{

}

void GraphicSystem::Init(DWORD_PTR dwInitData)
{
	_pGraphicSysCap = XNEW GraphicSystemCap;
}

void GraphicSystem::Uninit()
{
	XDELETE(_pGraphicSysCap);

	// Release shared buffers
#ifdef _DEBUG
	PixelBufferMapIt it = __sharedDSBuffers.begin();
	PixelBufferMapIt itEnd = __sharedDSBuffers.end();
	for ( ; it != itEnd; ++it)
	{
		XASSERT(it->second.GetReferenceCount() == 1);
	}
#endif
	__sharedDSBuffers.clear();
}

BOOL GraphicSystem::IsReady()
{
	XASSERT(0);
	return FALSE;
}

PixelBuffer* GraphicSystem::createPixelBuffer()
{
	XASSERT(0);
	return NULL;
}

VertexBuffer* GraphicSystem::CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
{
	XASSERT(0);
	return NULL;
}

Texture* GraphicSystem::CreateTexture(TextureType textureType, const String& strName)
{
	XASSERT(0);
	return NULL;
}

RenderTexture* GraphicSystem::CreateRenderTexture(const String& strName, int nWidth, int nHeight
												  , PixelFormat pixelFormat
												  , PixelFormat depthStencilFormat)
{
	XASSERT(0);
	return NULL;
}

DynamicTexture* GraphicSystem::CreateDynamicTexture(const String& strName, int nWidth, int nHeight
												 , PixelFormat pixelFormat)
{
	XASSERT(0);
	return NULL;	
}

MultiRenderTarget* GraphicSystem::CreateMultiRenderTarget()
{
	return XNEW MultiRenderTarget;
}

WindowRenderTarget* GraphicSystem::CreateWindowRenderTarget(Window* pWindow)
{
	XASSERT(0);
	return NULL;
}

IndexBuffer* GraphicSystem::CreateIndexBuffer(UINT nIndexCount, UINT nIndexSize, BOOL bDynamic)
{
	XASSERT(0);
	return NULL;
}

VertexLayout* GraphicSystem::CreateVertexLayout()
{	
	XASSERT(0);
	return NULL;
}	

PixelBufferPtr GraphicSystem::GetSharedDSBuffer(int nWidth, int nHeight
												  , PixelFormat pixelFormat)
{
	PixelBufferID dsBufferID(pixelFormat, nWidth, nHeight);
	PixelBufferMapIt it = __sharedDSBuffers.find(dsBufferID);
	if(it != __sharedDSBuffers.end()) return it->second;

	PixelBuffer* pDSBuffer = CreateDepthStencilBuffer(nWidth, nHeight, pixelFormat);
	if(!pDSBuffer)
	{
		XTHROWEX("GraphicSystem::GetSharedDSBuffer failed!");
	}
	
	PixelBufferPtr pDSBufferPtr = PixelBufferPtr(pDSBuffer);
	__sharedDSBuffers.insert(make_pair(dsBufferID, pDSBufferPtr));

	return pDSBufferPtr;
}

void GraphicSystem::ClearFrameBuffer(DWORD dwBufferFlag
									 , DWORD color
									 , Real depth
									 , unsigned short stencil)
{
	XASSERT(0);
}

void GraphicSystem::SetVertexLayout(VertexLayout* pVertexDeclearation)
{
	XASSERT(0);
}

void GraphicSystem::SetViewport(const ViewportInfo* pViewportInfo)
{
	XASSERT(0);
}

BOOL GraphicSystem::SetRenderTarget(RenderTarget* pRenderTarget)
{
	XASSERT(0);
	return FALSE;
}

BOOL GraphicSystem::SetMultiRenderTarget(MultiRenderTarget* pMultiRenderTarget)
{
	XASSERT(0);
	return FALSE;	
}

BOOL GraphicSystem::SetColorBuffer(int iBufferIndex, PixelBuffer* pColorBuffer)
{
	XASSERT(0);
	return FALSE;	
}

BOOL GraphicSystem::SetDepthStencilBuffer(PixelBuffer* pColorBuffer)
{
	XASSERT(0);
	return FALSE;	
}

void GraphicSystem::SetAlphaTestParams(BOOL bEnable, CompareFunction func, BYTE value, BOOL alphaToCoverage)
{
	XASSERT(0);
}

void GraphicSystem::SetSceneBlending(FrameBlendFactor sourceFactor
										 , FrameBlendFactor destFactor
										 , FrameBlendOperation blendOp)
{
	XASSERT(0);
}

void GraphicSystem::SetDepthBufferParams(BOOL bDepthTest, BOOL bDepthWrite, CompareFunction depthFunction)
{
	XASSERT(0);
}

void GraphicSystem::SetStencilBufferParams(BOOL bEnableStencil
										, CompareFunction func	
										, UINT refValue, UINT mask
									    , StencilOperation stencilFailOp
									    , StencilOperation depthFailOp
									    , StencilOperation passOp
									    , BOOL bTwoSidedOperation)
{
	XASSERT(0);
}


void GraphicSystem::SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha)
{
	XASSERT(0);
}

void GraphicSystem::EnableColorBufferWrite(BOOL bEnable)
{
	XASSERT(0);
}

void GraphicSystem::SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState)
{
	XASSERT(0);
}

void GraphicSystem::SetTextureUnitStates(const TextureUnitStateList& textureUnitStates)
{
	XASSERT(0);
}

void GraphicSystem::SetTextureState(UINT texUnitIndex, TextureState* pTextureState)
{
	XASSERT(0);
}

void GraphicSystem::SetTexture(UINT texUnitIndex, Texture* pTexture)
{
	XASSERT(0);
}

void GraphicSystem::SetVertexTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState)
{
	XASSERT(0);
}

void GraphicSystem::SetVertexTextureUnitStates(const TextureUnitStateList& textureUnitStates)
{
	XASSERT(0);
}

void GraphicSystem::SetShader(Shader* pShader)
{
	XASSERT(0);
}

DWORD GraphicSystem::BeginDrawing()
{
	XASSERT(0);
	return -1;
}

DWORD GraphicSystem::EndDrawing()
{
	XASSERT(0);
	return -1;
}

void GraphicSystem::BeginUpdateWindow(WindowRenderTarget* pWndTarget)
{
	_pCurWndRenderTarget = pWndTarget;
}

void GraphicSystem::EndUpdateWindow()
{
	_pCurWndRenderTarget = NULL;
}

DWORD GraphicSystem::DrawPrimitive(const DrawPrimitiveCommand* pDrawPrimitiveCmd)
{
	XASSERT(0);
	return -1;
}

DWORD GraphicSystem::SetVertexBuffer(int iStreamIndex, VertexBuffer* pVertexBuffer)
{
	XASSERT(0);
	return 0;	
}

void GraphicSystem::SetVertexBuffers(const VertexBufferPtrList& vertexBufferList)
{
	XASSERT(0);
}

DWORD GraphicSystem::SetIndexBuffer(IndexBuffer* pIndexBuffer)
{
	XASSERT(0);
	return 0;	
}

DWORD GraphicSystem::DrawPrimitive(DrawPrimitiveType primitiveType, int nVertexCount, int iStartIndex, int nPrimitiveCount)
{
	XASSERT(0);
	return 0;
}

void GraphicSystem::SetCullMode(HardwareCullMode cullMode)
{
	XASSERT(0);
}

void GraphicSystem::SetClipPlane(UINT iIndex, BOOL bEnable, const Plane& clipPlane)
{
	XASSERT(0);
}

// Create a DepthStencil buffer
PixelBuffer* GraphicSystem::CreateDepthStencilBuffer(int nWidth, int nHeight, PixelFormat pixelFormat)
{
	XASSERT(0);
	return NULL;
}

_X3D_NS_END


