#include "StdAfx.h"
#include "DrawPrimitiveCommand.h"

_X3D_NS_BEGIN

DrawPrimitiveCommand::DrawPrimitiveCommand(void) 
{
	_pVertexData = NULL;
	_pIndexData = NULL;

	_operationType = DRAW_INVALID;
	_nCachedPrimitiveCount = -1;
}

DrawPrimitiveCommand::~DrawPrimitiveCommand(void)
{
}


_X3D_NS_END