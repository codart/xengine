#pragma once

_X3D_NS_BEGIN

class GraphicSystem;

typedef DWORD_PTR InternalObjectHandle;

/*
	Pure virtual Base class for all classes that are tightly related (such as created by GraphicSystem, or 
	need graphic system to implement the internal method) with GraphicSystem. 

	Note: When write a new GraphicSystem plug-in. All the classes that inherit GraphicalObject and
		GraphicalManager should has a concrete implementation.
*/
class _X3DExport GraphicalObject : public Object
								, public NonCopyable
{
	no_direct_concrete

public:
	GraphicalObject(GraphicSystem* pGraphicSystem);
	virtual ~GraphicalObject(void);


public:
	GraphicSystem*
		GetGraphicSystem(){ return _pGraphicSystem; }
	BOOL
		QueryInternalHandle(OUT InternalObjectHandle* ppInternalObjectHandle);
	void
		SetInternalHandle(IN InternalObjectHandle pInternalObjectHandle);


protected:
	GraphicSystem*
		_pGraphicSystem;


protected:
	virtual InternalObjectHandle
		_getInternalObjHandle();
	virtual void
		_setInternalObjHandle(InternalObjectHandle pInternalHandle);

};



_X3D_NS_END