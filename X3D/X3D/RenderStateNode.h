#pragma once

#include "common/util/TreeNode.h"

_X3D_NS_BEGIN

class RenderStateNode : public TreeNode
{
public:
	RenderStateNode(void);
	virtual ~RenderStateNode(void);


public:
	virtual void 
		Render();


protected:
	void
		_renderAllRenderables();
	void
		_enterState();
	void
		_leaveState();


protected:
	RendableObjectList
		_attachedObjects;

};


_X3D_NS_END