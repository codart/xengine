#include "StdAfx.h"
#include "GPUProgram.h"

#include "common/util/XmlAttributeHandle.h"
#include "Common/util/xmlNodeHandle.h"
#include "Common/util/Unicode.h"


_X3D_NS_BEGIN

GPUProgram::GPUProgram(GraphicSystem* pGraphicSystem)
: GraphicalObject(pGraphicSystem)
{
	
}

GPUProgram::~GPUProgram(void)
{
}

void GPUProgram::Destroy()
{
	delete this;
}

void GPUProgram::LoadFromXMLNode(XmlNodeHandle& pGPUProgramNode)
{
	XASSERT(pGPUProgramNode);

	USING_STRING_CONVERT;

	SetName(AStrToStr(pGPUProgramNode.FirstAttribute("name").GetValue()));

	XmlNodeHandle pSourceNode = pGPUProgramNode["source"];
	String strSourceProgramName = AStrToStr(pSourceNode.GetValue());

	AString strEntryPoint;
	XmlNodeHandle pEntryPointNode = pGPUProgramNode["entry_point"];
	if(pEntryPointNode)
	{
		strEntryPoint = pEntryPointNode.GetValue();
	}
	else
	{
		strEntryPoint = "main";
	}

	XmlNodeHandle pProfileNode = pGPUProgramNode["profile_dx"];
	XASSERT(pProfileNode);
	AString strProfile = pProfileNode;

	LoadFromFile(GPU_PROGRAM_PATH + strSourceProgramName, strEntryPoint, strProfile);

	// Load auto parameters
	XmlNodeHandle pAutoParamsNode = pGPUProgramNode["auto_parameters"];
	XmlNodeHandle pAutoParamNode = pAutoParamsNode["auto_parameter"];

	int nXMLParamCount = 0;

	while(pAutoParamNode)
	{
		AString strConstantName = pAutoParamNode.FirstAttribute("constant_name");

		GPUProgramConstantMapIt it = _gpuConstantMap.find(strConstantName);
		if(it == _gpuConstantMap.end())
		{
			// Has param defined in script, but not find in the program constant map.
			XASSERT(0);
			pAutoParamNode = pAutoParamNode.NextSiblingNode();
			continue;
		}

		AString strType = pAutoParamNode.FirstAttribute("type");

		AutoParameter autoParameter;
		XmlAttributeHandle indexAttribute = pAutoParamNode.FirstAttribute("index");
		if(indexAttribute)
		{
			autoParameter.iIndex = indexAttribute;
		}
		else
		{
			autoParameter.iIndex = -1;
		}

		autoParameter.pGPUProgramConstant = &it->second;
		autoParameter.autoParamType = AutoParameter::AutoParamTypeFromSemantic(strType);

		_autoParameters.push_back(autoParameter);
		nXMLParamCount++;
		pAutoParamNode = pAutoParamNode.NextSiblingNode();
	}

	// Check if all params in the shader has a correct mapping.
	XASSERT(nXMLParamCount == _gpuConstantMap.size());
}

_X3D_NS_END