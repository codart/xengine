#pragma once

#include "x3d/resource/Mesh.h"
#include "x3d/resource/ResourceManager.h"
#include "x3d/resource/PrefabMesh.h"

_X3D_NS_BEGIN

class MeshManager : public Singleton<MeshManager, AutoRelease>
				  , public ResourceManager
{
public:
	MeshManager(void);
	virtual ~MeshManager(void);


public:
	void 
		unInit();

	// Add a mesh which is loaded from mesh file.
	MeshPtr&
		AddMesh(const String& strMeshFile);
	
	MeshPtr&
		AddPrefabMesh(const String& strMeshName, PrefabType type
					, const ParameterList& parameters = ParameterList());

	// ---
	// Add a mesh into MeshManager. The mesh is generate by some helper classes such as MeshGenerator.
	MeshPtr&
		AddModeledMesh(const String& strMeshName);


protected:
	MeshOwnerPtrMap
		_meshMap;


protected:
	void 
		_releaseAllMeshs();

};


_X3D_NS_END