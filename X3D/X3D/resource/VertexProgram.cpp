#include "StdAfx.h"
#include "VertexProgram.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/render/AutoParameterDataProvider.h"

_X3D_NS_BEGIN

VertexProgram::VertexProgram(GraphicSystem* pGraphicSystem)
: GPUProgram(pGraphicSystem)
{

}

VertexProgram::~VertexProgram(void)
{
}

void VertexProgram::LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile)
{
	XASSERT(0);
}

GPUProgramConstantType VertexProgram::_constantTypeFromRawType(DWORD dwRawType)
{ 
	XASSERT(0);
	return CONSTANT_TYPE_INVALID;
}

void VertexProgram::UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider)
{	
	XASSERT(0);
}

_X3D_NS_END


