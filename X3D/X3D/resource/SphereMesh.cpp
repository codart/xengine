#include "stdafx.h"
#include "SphereMesh.h"

#include "x3d/resource/VertexBufferManager.h"
#include "x3d/resource/IndexBufferManager.h"
#include "x3d/graphic/VertexLayout.h"
#include "x3d/render/RenderSystem.h"

#include "x3d/resource/VertexData.h"
#include "x3d/resource/IndexData.h"

_X3D_NS_BEGIN

SphereMesh::SphereMesh(RenderSystem* pRenderSystem, const String& strName)
: PrefabMesh(pRenderSystem, strName)
{
	_type = PREFAB_TYPE_SPHERE;

	_numSides = 14;
	_numRings = 14;
	_radius = 1;

	//... should shared sphere data
	_pVertexData = XNEW VertexData(_pGraphicSystem);
	
	_pVertexBuffer = VertexBufferManager::Instance().AddVertexBuffer(TEXT("PrefabSphereVB") + strName
											, _numSides * _numRings, sizeof(Vertex_P_N));
	_pVertexData->AddVertexBufferBinding(_pVertexBuffer, 0);

	Vertex_P_N* pVertexData = (Vertex_P_N*)_pVertexBuffer->Lock(0, _pVertexBuffer->GetBufferSize()
		, LOCK_NORMAL);

	float sideRadian = Math::PI / float(_numSides - 1);
	float ringRadian = 2.f * Math::PI / float(_numRings - 1);
	unsigned int counter = 0;

	for (int i = 0; i < _numSides; i++)
	{
		float subRadius = Math::Sin(i*sideRadian) * _radius;
		float y = Math::Cos(i*sideRadian) * _radius;
		for (int j = 0; j < _numRings; j++)
		{
			pVertexData[counter].position.x = Math::Sin(j*ringRadian) * subRadius;
			pVertexData[counter].position.y = y;
			pVertexData[counter].position.z = Math::Cos(j*ringRadian) * subRadius;

			pVertexData[counter].normal = pVertexData[counter].position / _radius;
			counter++;
		}
	}

	_pVertexBuffer->Unlock();

	// --- Create vertex layout
	_pVertexLayout = _pVertexData->GetVertexLayout();

	_pVertexLayout->BeginUpdate();
	_pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
	_pVertexLayout->AddElement(0, 12, VEF_FLOAT3, VES_NORMAL);
	_pVertexLayout->EndUpdate();


	// --- Create index buffer
	_pIndexData = XNEW IndexData(_pGraphicSystem);

	_pIndexBuffer = IndexBufferManager::Instance().AddIndexBuffer(TEXT("PointLightVolumeIB")
											, 3 * (_numSides-1) * (_numRings-1) * 2
											, sizeof(UINT));

	_pIndexData->bindIndexBuffer(_pIndexBuffer);

	USHORT* triangleData = (USHORT*)_pIndexBuffer->Lock(0, _pIndexBuffer->GetBufferSize()
		, LOCK_NORMAL);

	counter = 0;
	for (int i = 0; i < _numSides-1; i++)
	{
		unsigned int startIndexUp = i * _numRings;
		unsigned int startIndexDown = (i + 1) * _numRings;
		for (int j = 0; j < _numRings-1; j++)
		{
			triangleData[counter++] = startIndexUp + j;
			triangleData[counter++] = startIndexDown + j;
			triangleData[counter++] = startIndexDown + j + 1;

			triangleData[counter++] = startIndexUp + j;
			triangleData[counter++] = startIndexDown + j + 1;
			triangleData[counter++] = startIndexUp + j + 1;
		}
	}

	_pIndexBuffer->Unlock();
}

DrawPrimitiveCommand SphereMesh::GetDrawPrimitiveCmd()
{
	// Update draw primitive command
	DrawPrimitiveCommand drawPrimitiveCmd;
	drawPrimitiveCmd.SetVertexData(_pVertexData);
	drawPrimitiveCmd.SetIndexData(_pIndexData);
	drawPrimitiveCmd.SetDrawPrimitiveType(DRAW_TRIANGLE_LIST);

	drawPrimitiveCmd.SetPrimitiveCount(_pIndexData->GetIndexCount() / 3);
	return drawPrimitiveCmd;
}

SphereMesh::~SphereMesh()
{
	SAFE_DELETE(_pIndexData);
	SAFE_DELETE(_pVertexData);
}


_X3D_NS_END