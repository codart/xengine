#include "StdAfx.h"
#include "ResourceManager.h"

#include "x3d/render/RenderSystem.h"

_X3D_NS_BEGIN

ResourceManager::ResourceManager(void)
{
	_pRenderSystem = NULL;
	_pGraphicSystem = NULL;
}

ResourceManager::~ResourceManager(void)
{
}

void ResourceManager::init(RenderSystem* pRenderSystem)
{
	_pRenderSystem = pRenderSystem;
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
}

void ResourceManager::unInit()
{
	_pGraphicSystem = NULL;
}

_X3D_NS_END