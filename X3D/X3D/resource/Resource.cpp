#include "StdAfx.h"
#include "Resource.h"

_X3D_NS_BEGIN

Resource::Resource(const String& strName) 
: _strName(strName)
{
	debug_assign(bManaged, TRUE);
	_loadingState = LOADSTATE_UNLOADED;
}

Resource::Resource(void)
{
}

Resource::~Resource(void)
{

}

void Resource::SetName(const String& strName)
{
	_strName = strName;
}

const String& Resource::GetName() const
{
	return _strName;
}


_X3D_NS_END