#pragma once

#include "GPUProgram.h"

_X3D_NS_BEGIN

class _X3DExport FragmentProgram : public GPUProgram 
{
	need_direct_concrete

public:
	FragmentProgram(GraphicSystem* pGraphicSystem);
	~FragmentProgram(void);


public:
	override void 
		UpdateAutoParameters(transient const AutoParameterDataProvider* pAutoParamDataProvider);


protected:
	override void
		LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile);

};


typedef SharedPtr<FragmentProgram> FragmentProgramPtr;
DECLARE_MAP_TYPE(map, String, FragmentProgramPtr);

typedef OwnerPtr<FragmentProgramPtr> FragmentProgramOwnerPtr;
DECLARE_MAP_TYPE(map, String, FragmentProgramOwnerPtr);


_X3D_NS_END