#include "StdAfx.h"
#include "MeshManager.h"
#include "x3d/resource/StaticMesh.h"
#include "x3d/resource/Spheremesh.h"
#include "x3d/resource/FourPyramid.h"


_X3D_NS_BEGIN

MeshManager::MeshManager(void)
{
}

MeshManager::~MeshManager(void)
{
}

void MeshManager::unInit()
{
	_releaseAllMeshs();

	ResourceManager::unInit();
}

void MeshManager::_releaseAllMeshs()
{
	_meshMap.clear();
}

MeshPtr& MeshManager::AddMesh(const String& strMeshFile)
{
	XASSERT(_pGraphicSystem);
	XASSERT(strMeshFile.length() != 0);

	MeshOwnerPtrMapIt it = _meshMap.find(strMeshFile);
	if(it != _meshMap.end()) return it->second;

	StaticMesh* pStaticMesh = StaticMesh::Create(_pRenderSystem);
	pStaticMesh->LoadFromFile(MODEL_PATH + strMeshFile);
	pStaticMesh->SetName(strMeshFile);

	_meshMap[strMeshFile] = pStaticMesh;
	return _meshMap[strMeshFile];
}

MeshPtr& MeshManager::AddPrefabMesh(const String& strMeshName, PrefabType type, const ParameterList& parameters)
{
	XASSERT(_pGraphicSystem);
	XASSERT(strMeshName.length() != 0);

	MeshOwnerPtrMapIt it = _meshMap.find(strMeshName);
	if(it != _meshMap.end())
	{
		PrefabMeshPtr pPrefabMesh = it->second;
		XASSERT(pPrefabMesh->GetType() == type);
		return it->second;
	}

	switch (type)
	{
	case PREFAB_TYPE_SPHERE:
		_meshMap[strMeshName] = XNEW SphereMesh(_pRenderSystem, strMeshName);
		break;

	case PREFAB_TYPE_BOX:
		XASSERT(0);
		break;

	case PREFAB_TYPE_CONE:
		XASSERT(0);
		break;

	case PREFAB_TYPE_CYLINDER:
		XASSERT(0);
		break;

	case PREFAB_TYPE_FOUR_PYRAMID:
		_meshMap[strMeshName] = XNEW FourPyramid(_pRenderSystem, strMeshName, parameters);
		break;

	default:
		XASSERT(0);
	}

	return _meshMap[strMeshName];
}


MeshPtr& MeshManager::AddModeledMesh(const String& strMeshName)
{
	XASSERT(_pGraphicSystem);
	XASSERT(strMeshName.length() != 0);

	MeshOwnerPtrMapIt it = _meshMap.find(strMeshName);
	if(it != _meshMap.end())
	{
		// Make sure it is a modeled mesh.
		XASSERT(SAFE_CAST(&(it->second), StaticMesh*)->GetModel());
		return it->second;
	}

	StaticMesh* pStaticMesh = StaticMesh::Create(_pRenderSystem, strMeshName);
	pStaticMesh->CreateModel();

	_meshMap[strMeshName] = pStaticMesh;
	return _meshMap[strMeshName];
}
/*

MeshPtr MeshManager::addMesh(Mesh* pMesh)
{
	XASSERT(_pGraphicSystem);

#ifdef _DEBUG
	MeshPtrMapIt it = _meshMap.find(pMesh->GetName());
	XASSERT(it == _meshMap.end());
#endif

	MeshPtr pMeshPtr(pMesh);
	_meshMap.insert(make_pair(pMesh->GetName(), pMeshPtr));
	return pMeshPtr;	
}*/


_X3D_NS_END