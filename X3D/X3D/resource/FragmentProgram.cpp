#include "StdAfx.h"
#include "FragmentProgram.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/render/AutoParameterDataProvider.h"


_X3D_NS_BEGIN

FragmentProgram::FragmentProgram(GraphicSystem* pGraphicSystem)
: GPUProgram(pGraphicSystem)
{

}

FragmentProgram::~FragmentProgram(void)
{

}

void FragmentProgram::LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile)
{
	XASSERT(0);
}

void FragmentProgram::UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider)
{	
	XASSERT(0);
}

_X3D_NS_END