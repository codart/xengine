#pragma once

#include "x3d/resource/Resource.h"
#include "x3d/render/RenderObject.h"

_X3D_NS_BEGIN

class Mesh : public Resource
			, public RenderObject
{
public:
	Mesh(RenderSystem* pRenderSystem);
	Mesh(RenderSystem* pRenderSystem, const String& strName);
	virtual ~Mesh();

public:
	override void
		Destroy();

protected:
private:
};


typedef SharedPtr<Mesh> MeshPtr;
DECLARE_MAP_TYPE(map, String, MeshPtr);

typedef OwnerPtr<MeshPtr> MeshOwnerPtr;
DECLARE_MAP_TYPE(map, String, MeshOwnerPtr);

_X3D_NS_END