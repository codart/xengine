#pragma once

#include "x3d/graphic/IndexBuffer.h"

_X3D_NS_BEGIN


class _X3DExport IndexData : public Final<IndexData>
{
public:
	IndexData(GraphicSystem* pGraphicSystem);
	~IndexData(void);


public:
	IndexBuffer*
		GetIndexBuffer(){ return _pIndexBuffer; };
	INLINE UINT
		GetStartIndex(){ return _iIndexStart; };
	INLINE UINT
		GetIndexCount(){ return _nIndexCount; };

	// ... Consider use shared index buffer later!!!
	IndexBuffer*
		initIndexBuffer(UINT nIndexCount, UINT nIndexSize);
	void
		bindIndexBuffer(IndexBufferPtr& pIndexBuffer);


protected:
	GraphicSystem*
		_pGraphicSystem;
	IndexBufferPtr
		_pIndexBuffer;


	/// index in the buffer to start from for this operation
	Uint 
		_iIndexStart;
	/// The number of indexes to use from the buffer
	Uint 
		_nIndexCount;

};


_X3D_NS_END