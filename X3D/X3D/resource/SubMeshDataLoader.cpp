#include "stdafx.h"
#include "SubMeshDataLoader.h"

#include "common/util/XmlAttributeHandle.h"

_X3D_NS_BEGIN


SubMeshDataLoader::SubMeshDataLoader()
{
	_drawPrimitiveType = DRAW_TRIANGLE_LIST;
	_nVertexCount = 0;

	_bHasNormal = 0;
	_bHasTangent = 0;
	_bHasUV0 = 0;
	_bHasBoneIndices = FALSE;
	_nBoneWeightCount = 0;
}

SubMeshDataLoader::~SubMeshDataLoader()
{
	Unload();
}

void SubMeshDataLoader::Unload()
{
	_drawPrimitiveType = DRAW_TRIANGLE_LIST;
	_nVertexCount = 0;

	_bHasNormal = FALSE;
	_bHasTangent = FALSE;
	_bHasUV0 = FALSE;
	_bHasBoneIndices = FALSE;

	_dataStream.clear();
}

void SubMeshDataLoader::LoadFromXMLNode(const XmlNodeHandle& hGeometryNode)
{
	Unload();

	XASSERT(hGeometryNode);
	_nVertexCount = (Int)hGeometryNode.FirstAttribute("vertexcount");
	XASSERT(_nVertexCount > (Int)0);

	XmlNodeHandle hVertexBufferNode = hGeometryNode.FirstChild("vertexbuffer");
	_nVertexStride = sizeof(Vector3); // Position is vector3
	_bHasNormal = hVertexBufferNode.FirstAttribute("normals")? TRUE : FALSE;
	if(_bHasNormal)
	{
		_nVertexStride += sizeof(Vector3);
	}

	_bHasTangent = hVertexBufferNode.FirstAttribute("tangents")? TRUE : FALSE;
	if(_bHasTangent)
	{
		_nVertexStride += sizeof(Vector3);
	}

	_bHasUV0 = hVertexBufferNode.FirstAttribute("texture_coords")? TRUE : FALSE;
	if(_bHasUV0)
	{
		_nVertexStride += sizeof(Vector2);
	}

	_dataStream.resize(_nVertexCount * _nVertexStride);

	XmlNodeHandle hVertexNode = hGeometryNode.FirstChild("vertexbuffer").FirstChild("vertex");
	XASSERT(hVertexNode);
	BYTE* pCurVertexData = &_dataStream.at(0);
	for (int i = 0; hVertexNode; hVertexNode = hVertexNode.NextSiblingNode(), ++i)
	{
		XmlNodeHandle hCurXMLNode;

		// Read position
		XASSERT(hVertexNode);
		Vector3* pPosition = (Vector3*)pCurVertexData;
		hCurXMLNode = hVertexNode.FirstChild("position");
		XmlAttributeHandle hAttribute = hCurXMLNode.FirstAttribute();
		pPosition->x = hAttribute.ValueToReal();
		hAttribute = hAttribute.NextAttribute();
		pPosition->y = hAttribute.ValueToReal();
		hAttribute = hAttribute.NextAttribute();
		pPosition->z = hAttribute.ValueToReal();

		pCurVertexData += sizeof(Vector3);

		// Normal
		if(_bHasNormal)
		{
			Vector3* pNormal = (Vector3*)pCurVertexData;
			hCurXMLNode = hCurXMLNode.NextSiblingNode();
			XASSERT(strcmp(hCurXMLNode.GetName(), "normal") == 0);
			hAttribute = hCurXMLNode.FirstAttribute();
			pNormal->x = hAttribute.ValueToReal();
			hAttribute = hAttribute.NextAttribute();
			pNormal->y = hAttribute.ValueToReal();
			hAttribute = hAttribute.NextAttribute();
			pNormal->z = hAttribute.ValueToReal();

			pCurVertexData += sizeof(Vector3);
		}

		// Tangent
		if(_bHasTangent)
		{
			Vector3* pTangent = (Vector3*)pCurVertexData;
			hCurXMLNode = hCurXMLNode.NextSiblingNode();
			XASSERT(strcmp(hCurXMLNode.GetName(), "tangent") == 0);
			hAttribute = hCurXMLNode.FirstAttribute();
			pTangent->x = hAttribute.ValueToReal();
			hAttribute = hAttribute.NextAttribute();
			pTangent->y = hAttribute.ValueToReal();
			hAttribute = hAttribute.NextAttribute();
			pTangent->z = hAttribute.ValueToReal();

			pCurVertexData += sizeof(Vector3);
		}

		// First texture UV
		if(_bHasUV0)
		{
			Vector2* pUV = (Vector2*)pCurVertexData;
			hCurXMLNode = hCurXMLNode.NextSiblingNode();
			XASSERT(strcmp(hCurXMLNode.GetName(), "texcoord") == 0);
			hAttribute = hCurXMLNode.FirstAttribute();
			pUV->x = hAttribute;
			hAttribute = hAttribute.NextAttribute();
			pUV->y = hAttribute;

			pCurVertexData += sizeof(Vector2);
		}
	}
}


_X3D_NS_END