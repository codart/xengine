#include "StdAfx.h"
#include "Texture.h"

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

Texture::Texture(const String& strName, GraphicSystem* pGraphicSystem)
: GraphicalObject(pGraphicSystem), Resource(strName)
{
	_nMipmapCount = 0;
	_width = 0;
	_height = 0;

	_type = TEXTURE_NORMAL;
	_pixelFormat = PIXEL_FORMAT_UNKNOWN;
}

Texture::~Texture(void)
{

}

void Texture::Destroy()
{
	delete this;
}

void Texture::LoadImage(const String& strImageFIle)
{
	XASSERT(0);
}

void Texture::Load()
{
	XASSERT(0);
}

void Texture::Unload()
{
	XASSERT(0);
}

PixelBufferPtr Texture::GetPixelBuffer(int iFaceIndex, int iMipLevel)
{
	if(_texSurfacePixelBufs.size() == 0)
	{
		/* As GetPixelBuffer will be called rarely, so we create the PixelBuffer Wrapper objects
		the first time GetPixelBuffer be called. */
		_updatetexSurfacePixelBufs();
	}

	UINT iBufferIndex = (Int)iFaceIndex * _nMipmapCount + (Int)iMipLevel;
	XASSERT(iBufferIndex >= 0 && iBufferIndex < _texSurfacePixelBufs.size());

	return _texSurfacePixelBufs[iBufferIndex];
}

void Texture::_updatetexSurfacePixelBufs()
{
	XASSERT(0);
}

void Texture::_clearDependentedRes()
{
	_unbindPixelBuffers();
}

void Texture::_unbindPixelBuffers()
{
#ifdef _DEBUG
	for (UINT i=0; i<_texSurfacePixelBufs.size(); ++i)
	{
		XASSERT(_texSurfacePixelBufs[i].GetReferenceCount() == 1);
	}	
#endif
	_texSurfacePixelBufs.clear();
}

_X3D_NS_END