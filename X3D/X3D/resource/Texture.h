#pragma once

#include "x3d/resource/Resource.h"
#include "x3d/graphic/GraphicalObject.h"
#include "x3d/datadef/PixelFormat.h"
#include "x3d/graphic/PixelBuffer.h"


_X3D_NS_BEGIN

class PixelBuffer;


enum TextureType
{
	TEXTURE_INVALID = -1,

	TEXTURE_NORMAL = 0,
	TEXTURE_RENDER_TARGET,
	TEXTURE_DYNAMIC,
	TEXTURE_CUBE,
	TEXTURE_VOLUMN,
};

class _X3DExport Texture : public GraphicalObject
                         , public Resource
{
protected:
	Texture(const String& strName, GraphicSystem* pGraphicSystem);
public:
	virtual ~Texture(void);


public:
	/// To avoid ambiguous of Destroy().
	virtual void
		Destroy();

	virtual void 
		LoadImage(const String& strImageFIle);

	virtual void
		Load();
	virtual void
		Unload();

	virtual int
		GetWidth() const { return _width; }
	virtual int
		GetHeight() const { return _height; }


	INLINE UINT 
		GetFaceCount(void) const { return getTextureType() == TEXTURE_CUBE ? 6 : 1; }
	INLINE UINT
		GetMipmapCount() { return _nMipmapCount; }

	virtual PixelBufferPtr
		GetPixelBuffer(int iFaceIndex, int iMipLevel);

	// ---
	TextureType
		getTextureType() const { return _type; }
	virtual void
		setWidth(int nWidth){ _width = nWidth; }
	virtual void
		setHeight(int nHeight){ _height = nHeight; }


protected:
	Int 
		_width;
	Int 
		_height;
	Int
		_nMipmapCount;
	PixelFormat
		_pixelFormat;

	TextureType
		_type;

	PixelBufferPtrList
		_texSurfacePixelBufs;


protected:
	void 
		_unbindPixelBuffers();
	virtual void
		_updatetexSurfacePixelBufs();
	virtual void 
		_clearDependentedRes();


};

typedef SharedPtr<Texture> TexturePtr;
DECLARE_LIST_TYPE(vector, TexturePtr);

DECLARE_MAP_TYPE(map, String, TexturePtr);

typedef OwnerPtr<TexturePtr> TextureOwnerPtr;
DECLARE_MAP_TYPE(map, String, TextureOwnerPtr);


_X3D_NS_END