#pragma once

#include "x3d/graphic/VertexBuffer.h"
#include "x3d/resource/ResourceManager.h"

_X3D_NS_BEGIN

class VertexBufferManager : public Singleton<VertexBufferManager, AutoRelease>
						  , public ResourceManager
{
public:
	VertexBufferManager();
	virtual ~VertexBufferManager(void);


public:
	override void 
		init(RenderSystem* pRenderSystem);
	override void
		unInit();

	// Add a texture (textures that load from image file). strTextureName is the image's name.
	VertexBufferPtr&
		AddVertexBuffer(const String& strVertexBufferName
						, UINT nVertexCount
						, UINT nVertexSize
						, BOOL bDynamic = FALSE);
	

protected:
	VertexBufferOwnerPtrMap
		_vertexBufferMap;


protected:
	void 
		_releaseAllVertexBuffers();


};


_X3D_NS_END