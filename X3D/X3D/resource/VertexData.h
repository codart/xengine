#pragma once

#include "X3D/graphic/VertexLayout.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/graphic/GraphicSystemCap.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

class VertexData : public Final<VertexData>
{
public:
	VertexData(GraphicSystem* pGraphicSystem);
	~VertexData(void);


public:
	VertexBuffer*
		AddVertexBufferBinding(UINT nVertexCount, UINT nVertexSize
								, BOOL bDynamic = FALSE, int iStreamIndex = 0);
	void
		AddVertexBufferBinding(VertexBufferPtr& pVertexBuffer, int iStreamIndex = 0);

	void
		RemoveVertexBufferBinding(int iStreamIndex);

	// Note: the vertex buffer's position in the vector is just the vertex stream it binded to.
	INLINE const VertexBufferPtrList&
		GetVertexBuffers() const { return _vertexBuffers; }

	INLINE VertexBuffer*
		GetVertexBuffer(int iStreamIndex) { XASSERT(iStreamIndex >=0 && iStreamIndex < _pGraphicSystem->GetGraphicSystemCap().GetMaxVertexStreamCount());
											return _vertexBuffers[iStreamIndex];}

	INLINE int
		GetVertexCount() { return _nVertexCount; }

	INLINE VertexLayout*
		GetVertexLayout(){return _pVertexLayout; };


protected:
	GraphicSystem*
		_pGraphicSystem;

	Int
		_nVertexCount;
	VertexLayout*
		_pVertexLayout;
	
	VertexBufferPtrList
		_vertexBuffers;


protected:
	INLINE void 
		_releaseAllVertexBuffers();
	INLINE void 
		_updateVertexCount();

};


_X3D_NS_END