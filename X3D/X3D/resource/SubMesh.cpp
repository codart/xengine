#include "StdAfx.h"
#include "SubMesh.h"

#include "Common/util/Unicode.h"
#include "Common/util/XmlAttributeHandle.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/resource/VertexData.h"
#include "x3d/resource/IndexData.h"

#include "x3d/graphic/IndexBuffer.h"
#include "x3d/resource/SubMeshDataLoader.h"


_X3D_NS_BEGIN

SubMesh::SubMesh(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;

	_pVertexData = NULL;
	_pIndexData = NULL;

	_drawPrimitiveType = DRAW_TRIANGLE_LIST;
}

SubMesh::~SubMesh(void)
{
	SAFE_DELETE(_pVertexData);
	SAFE_DELETE(_pIndexData);
}

void SubMesh::setVertexData(VertexData* pVertexData) 
{
	SAFE_DELETE(_pVertexData); 
	_pVertexData = pVertexData; 
}

void SubMesh::setIndexData(IndexData* pIndexData) 
{
	SAFE_DELETE(_pIndexData); 
	_pIndexData = pIndexData; 
}

void SubMesh::_loadFromXML(const XmlNodeHandle& hSubMeshNode)
{
	XASSERT(hSubMeshNode);

	USING_STRING_CONVERT;

	_strMaterialName = AStrToStr(hSubMeshNode.FirstAttribute("material").GetValue());

	// Load vertex data
	_loadVertexData(hSubMeshNode);

	// Load index data (triangles)
	_loadIndexData(hSubMeshNode);
}

void SubMesh::_loadIndexData(const XmlNodeHandle& hSubMesh)
{
	XmlNodeHandle hTrianglesNode = hSubMesh.FirstChild("faces");
	XASSERT(hTrianglesNode);
	
	int nTriangleCount = hTrianglesNode.FirstAttribute("count");

	__indicesList.resize(nTriangleCount);

	XmlNodeHandle hInicesNode = hTrianglesNode.FirstChild("face");
	XASSERT(hInicesNode);

	for (int i = 0; hInicesNode; hInicesNode = hInicesNode.NextSiblingNode(), ++i)
	{
		XASSERT(hInicesNode);

		FaceIndices& triangleIndices = __indicesList.at(i);
	
		XmlAttributeHandle hAttribute = hInicesNode.FirstAttribute("v1");
		triangleIndices.index1 = hAttribute;
		hAttribute = hAttribute.NextAttribute();
		triangleIndices.index2 = hAttribute;
		hAttribute = hAttribute.NextAttribute();
		triangleIndices.index3 = hAttribute;
	}

	_pIndexData = XNEW IndexData(_pGraphicSystem);
	IndexBuffer* pIndexBuffer = _pIndexData->initIndexBuffer(nTriangleCount * 3, sizeof(USHORT));
	pIndexBuffer->WriteData(0, pIndexBuffer->GetBufferSize(), &__indicesList[0], true);
}

void SubMesh::_loadVertexData(const XmlNodeHandle& hSubMesh)
{
	XmlNodeHandle hGeometryNode = hSubMesh.FirstChild("geometry");
	XASSERT(hGeometryNode);
	
	_drawPrimitiveType = DRAW_TRIANGLE_LIST;

	SubMeshDataLoader subMeshDataLoader;
	subMeshDataLoader.LoadFromXMLNode(hGeometryNode);

	XASSERT(!_pVertexData);
	_pVertexData = XNEW VertexData(_pGraphicSystem);
	VertexBuffer* pVertexBuffer = _pVertexData->AddVertexBufferBinding(subMeshDataLoader.GetVertexCount()
																		, subMeshDataLoader.GetVertexStride());
	pVertexBuffer->WriteData(0, pVertexBuffer->GetBufferSize(), subMeshDataLoader.GetByteStream());

	// Update vertex layout
	VertexLayout* pVertexLayout = _pVertexData->GetVertexLayout();
	pVertexLayout->BeginUpdate();

	int nOffset = 0;
	pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
	nOffset += sizeof(Vector3);

	if (subMeshDataLoader.GetBoneWeightCount() > 0)
	{
		int nWeightCount = subMeshDataLoader.GetBoneWeightCount();
		pVertexLayout->AddElement(0, nOffset, (VertexElementFormat)(VEF_FLOAT1 + nWeightCount), VES_BLEND_WEIGHTS);
		nOffset += sizeof(float) * nWeightCount;
	}

	if (subMeshDataLoader.HasBoneIndices())
	{
		pVertexLayout->AddElement(0, nOffset, VEF_UBYTE4, VES_BLEND_INDICES);
		nOffset += sizeof(DWORD);
	}

	if(subMeshDataLoader.HasNormal()) 
	{
		pVertexLayout->AddElement(0, nOffset, VEF_FLOAT3, VES_NORMAL);
		nOffset += sizeof(Vector3);
	}
	if(subMeshDataLoader.HasTangent())
	{
		pVertexLayout->AddElement(0, nOffset, VEF_FLOAT3, VES_TANGENT);
		nOffset += sizeof(Vector3);
	}
	if(subMeshDataLoader.HasUV0())
	{
		pVertexLayout->AddElement(0, nOffset, VEF_FLOAT2, VES_TEXTURE_COORDINATES);
		nOffset += sizeof(Vector2);
	}

	pVertexLayout->EndUpdate();
}

DrawPrimitiveCommand SubMesh::GetDrawPrimitiveCmd()
{
	DrawPrimitiveCommand renderOperation;
	renderOperation.SetVertexData(_pVertexData);
	renderOperation.SetIndexData(_pIndexData);
	renderOperation.SetDrawPrimitiveType(_drawPrimitiveType);

	switch (_drawPrimitiveType)
	{
	case DRAW_TRIANGLE_LIST:
		renderOperation.SetPrimitiveCount(_pIndexData->GetIndexCount() / 3);
		break;

	case DRAW_LINE_LIST:
		renderOperation.SetPrimitiveCount(_pIndexData->GetIndexCount() / 2);
		break;

	default:
		XASSERT(0);
	}

	return renderOperation;
}

_X3D_NS_END