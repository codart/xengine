#pragma once

#include "common/util/xmlNodeHandle.h"

#include "x3d/resource/Resource.h"
#include "x3d/render/AutoParameter.h"
#include "x3d/graphic/GraphicalObject.h"

_X3D_NS_BEGIN

class AutoParameterDataProvider;

class _X3DExport GPUProgram : public GraphicalObject
                            , public Resource
{
	no_direct_concrete

public:
	GPUProgram(GraphicSystem* pGraphicSystem);
	virtual ~GPUProgram(void);


public:
	virtual void
		Destroy();
	override void
		LoadFromXMLNode(XmlNodeHandle& pGPUProgramNode);
	virtual void
		UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider){ XASSERT(0); }


protected:
	virtual void
		LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile){XASSERT(0);};


protected:
	GPUProgramConstantMap
		_gpuConstantMap;

	AutoParameterList
		_autoParameters;
	ManualParameterList
		_manualParameters;


protected:
	virtual GPUProgramConstantType
		_constantTypeFromRawType(DWORD dwRawType){ XASSERT(0); return CONSTANT_TYPE_INVALID; };


};

typedef SharedPtr<GPUProgram> GPUProgramPtr;
DECLARE_MAP_TYPE(map, String, GPUProgramPtr);

_X3D_NS_END