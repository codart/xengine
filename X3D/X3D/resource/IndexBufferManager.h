#pragma once

#include "x3d/graphic/IndexBuffer.h"
#include "x3d/resource/ResourceManager.h"

_X3D_NS_BEGIN

class IndexBufferManager : public Singleton<IndexBufferManager, AutoRelease>
						  , public ResourceManager
{
public:
	IndexBufferManager();
	virtual ~IndexBufferManager(void);


public:
	override void 
		init(RenderSystem* pRenderSystem);
	override void
		unInit();

	// Add a texture (textures that load from image file). strTextureName is the image's name.
	IndexBufferPtr&
		AddIndexBuffer(const String& strVertexBufferName
						, UINT nVertexCount
						, UINT nVertexSize
						, BOOL bDynamic = FALSE);
	

protected:
	IndexBufferOwnerPtrMap
		_indexBufferMap;


protected:
	void 
		_releaseAllIndexBuffers();


};


_X3D_NS_END