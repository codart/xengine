#include "stdafx.h"
#include "Mesh.h"


_X3D_NS_BEGIN


Mesh::Mesh(RenderSystem* pRenderSystem)
: RenderObject(pRenderSystem)
{
	
}

Mesh::Mesh(RenderSystem* pRenderSystem, const String& strName)
: RenderObject(pRenderSystem)
, Resource(strName)
{

}

Mesh::~Mesh()
{

}

void Mesh::Destroy()
{
	delete this;
}

_X3D_NS_END