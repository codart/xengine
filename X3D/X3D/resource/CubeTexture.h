#pragma once

#include "x3d/resource/Texture.h"

_X3D_NS_BEGIN

class _X3DExport CubeTexture : public Texture
{
public:
	CubeTexture(const String& strName, GraphicSystem* pGraphicSystem);
	virtual ~CubeTexture();


protected:
private:

};


_X3D_NS_END