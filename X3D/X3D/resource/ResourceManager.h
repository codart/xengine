#pragma once


_X3D_NS_BEGIN

/*
	Manager the resources that create with GraphicSystem (hardware related resource). 
*/
class ResourceManager
{
public:
	ResourceManager(void);
	virtual ~ResourceManager(void);


public:
	virtual void 
		init(RenderSystem* pRenderSystem);
	virtual void 
		unInit();


protected:
	RenderSystem* 
		_pRenderSystem;
	// The GraphicSystem that create the resource
	GraphicSystem*
		_pGraphicSystem;

};


_X3D_NS_END