#include "StdAfx.h"
#include "DynamicTexture.h"

_X3D_NS_BEGIN

DynamicTexture::DynamicTexture(const String& strName, GraphicSystem* pGraphicSystem)
: Texture(strName, pGraphicSystem)
{
	_type = TEXTURE_DYNAMIC;
}

DynamicTexture::~DynamicTexture(void)
{

}

void DynamicTexture::_clearDependentedRes()
{
	//...SAFE_DELETE(_pTextureTarget);
	Texture::_clearDependentedRes();
}

_X3D_NS_END