#include "StdAfx.h"
#include "VertexData.h"

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

VertexData::VertexData(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;

	_pVertexLayout = pGraphicSystem->CreateVertexLayout();
	_nVertexCount = -1;

	int nMaxStreamCount = _pGraphicSystem->GetGraphicSystemCap().GetMaxVertexStreamCount();

	// Init all stream with empty vertex buffer;
	for (int i=0; i<nMaxStreamCount; ++i)
	{
		VertexBufferPtr pVertexBufferPtr(_pGraphicSystem->CreateVertexBuffer(0, 0));
		_vertexBuffers.push_back(pVertexBufferPtr);
	}
}

VertexData::~VertexData(void)
{
	SAFE_DELETE(_pVertexLayout);

	_releaseAllVertexBuffers();
}

void VertexData::_releaseAllVertexBuffers()
{

}

VertexBuffer* VertexData::AddVertexBufferBinding(UINT nVertexCount
												, UINT nVertexSize, BOOL bDynamic, int iStreamIndex)
{
	XASSERT(iStreamIndex >=0 
			&& iStreamIndex < _pGraphicSystem->GetGraphicSystemCap().GetMaxVertexStreamCount());

	//...
	if(_nVertexCount == -1)
	{
		_nVertexCount = nVertexCount;
	}
	XASSERT(nVertexCount == _nVertexCount);

	VertexBufferPtr& pVertexBuffer = _vertexBuffers[iStreamIndex];

	pVertexBuffer = _pGraphicSystem->CreateVertexBuffer(nVertexCount, nVertexSize, bDynamic);

	return pVertexBuffer;
}

void VertexData::AddVertexBufferBinding(VertexBufferPtr& pVertexBuffer, int iStreamIndex)
{
	XASSERT(iStreamIndex >=0 
		&& iStreamIndex < _pGraphicSystem->GetGraphicSystemCap().GetMaxVertexStreamCount());

	if(_nVertexCount == -1)
	{
		_nVertexCount = pVertexBuffer->GetVertexCount();
	}

	VertexBufferPtr& pOldVertexBuffer = _vertexBuffers[iStreamIndex];	
	pOldVertexBuffer = pVertexBuffer;
}

void VertexData::RemoveVertexBufferBinding(int iStreamIndex)
{
	VertexBufferPtr& pVertexBuffer = _vertexBuffers[iStreamIndex];

	// Assign an empty vertex buffer
	pVertexBuffer = _pGraphicSystem->CreateVertexBuffer(0, 0);

	// If all vertex buffer is empty, set the _nVertexCount to -1
	_updateVertexCount();
}

void VertexData::_updateVertexCount()
{
	FASTEST_VECTOR_ITERATE(VertexBuffer*, _vertexBuffers);
		if(GET_NEXT(_vertexBuffers)->GetVertexCount() != 0) return;
	FASTEST_ITERATE_END();

	_nVertexCount = -1;
}

_X3D_NS_END
