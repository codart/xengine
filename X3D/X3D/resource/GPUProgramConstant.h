#pragma once

_X3D_NS_BEGIN

enum GPUProgramConstantType
{
	CONSTANT_TYPE_INVALID = -1,

	CONSTANT_TYPE_FLOAT,
	CONSTANT_TYPE_INT,
};

struct GPUProgramConstant
{
	AString 
		strConstantName;
	UINT 
		index;
	UINT 
		registerCount;
	UINT 
		sizeInBytes;
	GPUProgramConstantType 
		constantType;
};

DECLARE_MAP_TYPE(map, AString, GPUProgramConstant);


_X3D_NS_END