#pragma once

#include "x3d/resource/PrefabMesh.h"


_X3D_NS_BEGIN

class SphereMesh : public PrefabMesh
{
public:
	SphereMesh(RenderSystem* pRenderSystem, const String& strName);
	virtual ~SphereMesh();


public:
	override DrawPrimitiveCommand
		GetDrawPrimitiveCmd();


protected:
	Int
		_numSides;
	Int
		_numRings;
	Real
		_radius;

};

typedef ConcretePtr<SphereMesh, Mesh> PrefabSphereMeshPtr;

_X3D_NS_END