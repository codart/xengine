#pragma once

#include "x3d\resource\gpuprogram.h"

_X3D_NS_BEGIN

class GeneralProgram : public GPUProgram
{
public:
	GeneralProgram(GraphicSystem* pGraphicSystem);
	virtual ~GeneralProgram(void);
};

_X3D_NS_END