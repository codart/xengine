#include "StdAfx.h"
#include "RenderTexture.h"

#include "x3d/graphic/GraphicSystem.h"


_X3D_NS_BEGIN

RenderTexture::RenderTexture(const String& strName, GraphicSystem* pGraphicSystem)
: Texture(strName, pGraphicSystem)
{
	_depthStencilFormat = PIXEL_FORMAT_D24S8;
	_pTextureTarget = NULL;
	_type = TEXTURE_RENDER_TARGET;
	_bDynamicBind = FALSE;
}

RenderTexture::~RenderTexture(void)
{

}

void RenderTexture::_clearDependentedRes()
{
	SAFE_DELETE(_pTextureTarget);
	Texture::_clearDependentedRes();
}

RenderTarget* RenderTexture::GetRenderTarget()
{
	if(_pTextureTarget == NULL)
	{
		_pTextureTarget = RenderTarget::Create(_pGraphicSystem);

		PixelBufferPtr pColorBuffer = GetPixelBuffer(0, 0);	
		PixelBufferPtr pDepthStencilBufferPtr = _pGraphicSystem->GetSharedDSBuffer(pColorBuffer->GetWidth()
																			, pColorBuffer->GetHeight()
																			, _depthStencilFormat);
		_pTextureTarget->BindBuffers(pColorBuffer, pDepthStencilBufferPtr);
	}

	return _pTextureTarget;
}

void RenderTexture::Resize(int nWidth, int nHeight)
{
	XASSERT(0);
}


_X3D_NS_END