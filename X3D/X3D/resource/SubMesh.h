#pragma once

#include "Common/util/xmlNodeHandle.h"
#include "x3d/graphic/DrawPrimitiveCommand.h"
#include "x3d/datadef/VertexDef.h"
#include "x3d/datadef/ModelDataDef.h"

_X3D_NS_BEGIN

class IndexData;
class VertexData;
class GraphicSystem;

class Model;

class SubMesh : public Final<SubMesh>
{
	friend class StaticMesh;

public:
	SubMesh(GraphicSystem* pGraphicSystem);
	~SubMesh(void);


public:
	INLINE const String&
		GetMaterialName(){ return _strMaterialName; }
	INLINE void
		SetMaterialName(const String& strMaterialName){ _strMaterialName = strMaterialName; }
	
	DrawPrimitiveCommand
		GetDrawPrimitiveCmd();

	// ---
	void
		setVertexData(VertexData* pVertexData);
	void
		setIndexData(IndexData* pIndexData);
	void 
		setDrawPrimitiveType(DrawPrimitiveType drawPrimitiveType) { _drawPrimitiveType = drawPrimitiveType; }
	GraphicSystem*
		getGraphicSystem() { return _pGraphicSystem; }


protected:
	void
		_loadFromXML(const XmlNodeHandle& subMeshNode);

	void 
		_loadVertexData(const XmlNodeHandle& hSubMesh);
	void 
		_loadIndexData(const XmlNodeHandle& hSubMesh);


public:
	GraphicSystem* 
		_pGraphicSystem;

	DrawPrimitiveType 
		_drawPrimitiveType;

	VertexData*
		_pVertexData;
	IndexData*
		_pIndexData;

	// Only a material name. The SubMeshEntity will use this name to load a real Material.
	String
		_strMaterialName;


private:
	vector<FaceIndices>
		__indicesList;

};

DECLARE_LIST_TYPE_PTR(vector, SubMesh);


_X3D_NS_END