#pragma once

#include "x3d/resource/mesh.h"
#include "SubMesh.h"
#include "x3d/animation/Skeleton.h"

_X3D_NS_BEGIN

class GraphicSystem;
class RenderSystem;

class Model;

class _X3DExport StaticMesh : public Mesh
{
protected:
	StaticMesh(RenderSystem* pRenderSystem);
	StaticMesh(RenderSystem* pRenderSystem, const String& strName);
public:
	virtual ~StaticMesh(void);
	static StaticMesh* Create(RenderSystem* pRenderSystem) { return XNEW StaticMesh(pRenderSystem); }
	static StaticMesh* Create(RenderSystem* pRenderSystem, const String& strName) { return XNEW StaticMesh(pRenderSystem, strName); }

public:
	virtual void
		LoadFromFile(const String& strModelFile);
	virtual SubMeshList&
		GetSubMeshList() { return _subMeshList; }

	/** Get the axis-aligned bounding box for this mesh.
	*/
	const AxisAlignedBox& 
		GetBoundingBox(void) const { return _localAABB; }

	// --- Methods related with modeling system ---
	// Create a model object for this mesh. If the model already exist, just return the model.
	virtual Model*
		CreateModel();

	virtual Model*
		GetModel() { return _pModel; }

	// When true, the mesh will be auto updated when the model is changed.
	virtual void
		SetAutoUpdate(BOOL bAutoUpdate){XASSERT(0);};
	// Update mesh with _pModule.
	virtual void
		UpdateMeshWithModel();

	/* 
	//...
		How to update a mesh with model? Update all the submesh with related Element? Or just 
		reconstruct the mesh's all submesh?
	*/

	// ---
	virtual void 
		removeAllSubMeshs();
	virtual SubMesh*
		addSubMesh();


public:
	GraphicSystem*
		_pGraphicSystem;
	SubMeshList 
		_subMeshList;
	Model* 
		_pModel;

	/// Mesh bounding box volume
	AxisAlignedBox 
		_localAABB;

};

typedef ConcretePtr<StaticMesh, Mesh> StaticMeshPtr;

_X3D_NS_END