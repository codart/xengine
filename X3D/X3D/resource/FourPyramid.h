#pragma once

#include "x3d/resource/PrefabMesh.h"

_X3D_NS_BEGIN

class _X3DExport FourPyramid : public PrefabMesh
{
public:
	static const float DIRECTION_UP;
	static const float DIRECTION_DOWN;

public:
	FourPyramid(RenderSystem* pRenderSystem
			, const String& strName
			, const ParameterList& parameters);
	virtual ~FourPyramid();


public:
	override DrawPrimitiveCommand
		GetDrawPrimitiveCmd();


protected:
	Float
		_direction;
	Float
		_height;
	Float
		_width;
};


_X3D_NS_END

