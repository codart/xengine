#pragma once

#include "x3d/resource/GPUProgram.h"

_X3D_NS_BEGIN


class _X3DExport VertexProgram : public GPUProgram					
{
	need_direct_concrete

public:
	VertexProgram(GraphicSystem* pGraphicSystem);
	~VertexProgram(void);

public:
	override void
		UpdateAutoParameters(transient const AutoParameterDataProvider* pAutoParamDataProvider);
	override void
		LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile);


protected:
	override GPUProgramConstantType 
		_constantTypeFromRawType(DWORD dwRawType);

};


typedef SharedPtr<VertexProgram> VertexProgramPtr;
DECLARE_MAP_TYPE(map, String, VertexProgramPtr);

typedef OwnerPtr<VertexProgramPtr> VertexProgramOwnerPtr;
DECLARE_MAP_TYPE(map, String, VertexProgramOwnerPtr);


_X3D_NS_END