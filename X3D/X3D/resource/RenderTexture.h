#pragma once

#include "x3d/resource/Texture.h"
#include "x3d/graphic/RenderTarget.h"

_X3D_NS_BEGIN

class _X3DExport RenderTexture : public Texture
{
public:
	RenderTexture(const String& strName, GraphicSystem* pGraphicSystem);
	virtual ~RenderTexture(void);


public:
	virtual RenderTarget*
		GetRenderTarget();
	virtual void
		Resize(int nWidth, int nHeight);

	INLINE PixelBuffer*
		GetColorBuffer() { return GetPixelBuffer(0, 0); }

	INLINE PixelBuffer*
		GetDepthStencilBuffer() { return GetRenderTarget()->GetDepthStencilBuffer(); }


protected:
	RenderTarget*
		_pTextureTarget;

	// When depth stencil buffer is required, will use this format to create/get the depth stencil buffer.
	PixelFormat
		_depthStencilFormat;

	//...
	Boolean
		_bDynamicBind;

protected:
	override void 
		_clearDependentedRes();

};


typedef ConcretePtr<RenderTexture, Texture> RenderTexturePtr;

#define GET_TEXTURE_TARGET(pTexturePtr)	SAFE_CAST(&pTexturePtr, RenderTexture*)->GetRenderTarget()

_X3D_NS_END