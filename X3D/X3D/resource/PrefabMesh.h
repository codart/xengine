#pragma once

#include "x3d/resource/Mesh.h"
#include "x3d/resource/PrefabType.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/graphic/IndexBuffer.h"
#include "x3d/graphic/VertexLayout.h"
#include "x3d/resource/VertexData.h"


_X3D_NS_BEGIN

class PrefabMesh : public Mesh
{
public:
	PrefabMesh (RenderSystem* pRenderSystem, const String& strName);
	virtual ~PrefabMesh();


public:
	virtual DrawPrimitiveCommand
		GetDrawPrimitiveCmd() { XASSERT(0); DrawPrimitiveCommand dummy; return dummy; };
	virtual PrefabType
		GetType() { return _type; }


protected:
	PrefabType
		_type;

	GraphicSystem* 
		_pGraphicSystem;

	VertexData*
		_pVertexData;
	IndexData*
		_pIndexData;

	VertexBufferPtr
		_pVertexBuffer;
	VertexLayout*
		_pVertexLayout;

	IndexBufferPtr
		_pIndexBuffer;


};

typedef ConcretePtr<PrefabMesh, Mesh> PrefabMeshPtr;

_X3D_NS_END