#pragma once

#include "x3d/resource/resourcemanager.h"
#include "x3d/resource/GPUProgram.h"

#include "x3d/resource/VertexProgram.h"
#include "x3d/resource/FragmentProgram.h"

_X3D_NS_BEGIN

class _X3DExport GPUProgramManager : public ResourceManager
									, public Singleton<GPUProgramManager, AutoRelease>
{
public:
	GPUProgramManager(void);
	virtual ~GPUProgramManager(void);


public:
	override void 
		init(RenderSystem* pRenderSystem);
	override void 
		unInit();

	void 
		LoadAllPrograms(); 

	VertexProgramPtr
		AddVertexProgram(const String& strName);
	FragmentProgramPtr
		AddFragmentProgram(const String& strName);


protected:
	VertexProgramOwnerPtrMap
		_vertexProgramMap;
	FragmentProgramOwnerPtrMap
		_fragmentProgramMap;


protected:
	void 
		_loadProgramsFromFile(const String& strFileName);
	void 
		_releaseAllPrograms();

};


_X3D_NS_END