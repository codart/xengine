#include "StdAfx.h"
#include "StaticMesh.h"

#include "common/util/Unicode.h"
#include "common/file/ModuleUtil.h"
#include "x3d/graphic/GraphicSystem.h"
#include "x3d/model/ModelingSystem.h"

#include "Common/util/xmlDocument.h"
#include "Common/util/xmlNodeHandle.h"
#include "Common/util/XmlAttributeHandle.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/animation/SkeletonManager.h"

_X3D_NS_BEGIN

StaticMesh::StaticMesh(RenderSystem* pRenderSystem)
: Mesh(pRenderSystem)
{
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pModel = NULL;
}

StaticMesh::StaticMesh(RenderSystem* pRenderSystem, const String& strName)
: Mesh(pRenderSystem)
{
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pModel = NULL;
	SetName(strName);
}

StaticMesh::~StaticMesh(void)
{
	// Release all sub meshes
	removeAllSubMeshs();

	ModelingSystem::Instance().RemoveModel(_strName);
}

Model* StaticMesh::CreateModel()
{
	if(_pModel) return _pModel;

	_pModel = ModelingSystem::Instance().AddModel(_strName);
	_pModel->AttachToMesh(this);

	return _pModel;
}

void StaticMesh::LoadFromFile(const String& strModelFile)
{
	USING_STRING_CONVERT;
	// Check what the file type is, binary or XML
	//...

	XmlDocument xmlDoc;
	xmlDoc.LoadFromFile(StrToAStr(strModelFile));

	// Read basic info
	// Read bounds info
	XmlNodeHandle pBoundingBoxNode = xmlDoc.FirstChild("mesh").FirstChild("boundingbox");
	XASSERT(pBoundingBoxNode);
	XmlNodeHandle minNode = pBoundingBoxNode.FirstChild("min");
	XmlNodeHandle maxNode = pBoundingBoxNode.FirstChild("max");
	
	Vector3 vMin, vMax;
	vMin.x = minNode.FirstAttribute("x").ValueToReal();
	vMin.y = minNode.FirstAttribute("y").ValueToReal();
	vMin.z = minNode.FirstAttribute("z").ValueToReal();

	vMax.x = maxNode.FirstAttribute("x").ValueToReal();
	vMax.y = maxNode.FirstAttribute("y").ValueToReal();
	vMax.z = maxNode.FirstAttribute("z").ValueToReal();

	_localAABB.SetExtents(vMin, vMax);

	XmlNodeHandle pSubMeshNode = xmlDoc.FirstChild("mesh").FirstChild("submeshes").FirstChild("submesh");

	while(pSubMeshNode)
	{
		SubMesh* pSubMesh = XNEW SubMesh(_pGraphicSystem);
		_subMeshList.push_back(pSubMesh);
		pSubMesh->_loadFromXML(pSubMeshNode);

		// Get next submesh
		pSubMeshNode = pSubMeshNode.NextSiblingNode("submesh");
	}
}

void StaticMesh::removeAllSubMeshs()
{
	// Release all sub meshes
	SubMeshListIt it = _subMeshList.begin();
	SubMeshListIt itEnd = _subMeshList.end();

	for ( ; it != itEnd; ++it)
	{
		SubMesh* pSubMesh = *it;
		XASSERT(pSubMesh);
		delete pSubMesh;
	}
}

SubMesh* StaticMesh::addSubMesh()
{
	SubMesh* pSubMesh = XNEW SubMesh(_pGraphicSystem);
	_subMeshList.push_back(pSubMesh);
	return pSubMesh;
}

void StaticMesh::UpdateMeshWithModel()
{
	XASSERT(_pModel);
	_pModel->UpdateAttachedMesh();

	// Update local AABB
	_localAABB.SetNull();

	FastVecIterator<SubModel*> subModelIterator(_pModel->GetSubModelList());
	while (subModelIterator.HasMore())
	{
		_localAABB.Merge(subModelIterator.CurrentData()->GetLocalAABB());
		subModelIterator.Next();
	}

	//... Hard code bounding box
	//_localAABB.SetExtents(Vector3(-10, -10, -10), Vector3(10, 10, 10));
}


_X3D_NS_END