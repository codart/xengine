#include "StdAfx.h"
#include "IndexData.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/graphic/IndexBuffer.h"

_X3D_NS_BEGIN

IndexData::IndexData(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;
}

IndexData::~IndexData(void)
{

}

IndexBuffer* IndexData::initIndexBuffer(UINT nIndexCount, UINT nIndexSize)
{
	XASSERT(!_pIndexBuffer);
	_pIndexBuffer = _pGraphicSystem->CreateIndexBuffer(nIndexCount, nIndexSize);

	_iIndexStart = 0;
	_nIndexCount = nIndexCount;

	return _pIndexBuffer;
}

void IndexData::bindIndexBuffer(IndexBufferPtr& pIndexBuffer)
{
	XASSERT(!_pIndexBuffer);
	XASSERT(pIndexBuffer);
	_pIndexBuffer = pIndexBuffer;
	_nIndexCount = pIndexBuffer->GetIndexCount();
}

_X3D_NS_END