#pragma once

_X3D_NS_BEGIN

enum LoadingState
{
	/// Not loaded
	LOADSTATE_UNLOADED,
	/// Loading is in progress
	LOADSTATE_LOADING,
	/// Fully loaded
	LOADSTATE_LOADED,
	/// Currently unloading
	LOADSTATE_UNLOADING,
	/// Fully prepared
	LOADSTATE_PREPARED,
	/// Preparing is in progress
	LOADSTATE_PREPARING
};

class _X3DExport Resource : public Object
						, public NonCopyable
{
public:
	Resource(const String& strName);
	Resource(void);
	virtual ~Resource(void);


public:
	virtual void
		SetName(const String& strName);
	virtual const String&
		GetName() const;
	virtual BOOL
		IsLoaded(){return _loadingState == LOADSTATE_LOADED;};


protected:
	//... Should use AtomicScalar later to support background thread loading.
	LoadingState
		_loadingState;
	String
		_strName;


protected:
	virtual void
		_clearDependentedRes(){};


debug:
	debug_variable(BOOL, bManaged);

};


_X3D_NS_END