#include "stdafx.h"
#include "PrefabMesh.h"

#include "x3d/render/RenderSystem.h"

_X3D_NS_BEGIN

PrefabMesh::PrefabMesh(RenderSystem* pRenderSystem, const String& strName)
: Mesh(pRenderSystem, strName)
{
	_type = PREFAB_TYPE_INVALID;

	GraphicSystem* pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pGraphicSystem = pGraphicSystem;
}

PrefabMesh::~PrefabMesh()
{

}


_X3D_NS_END

