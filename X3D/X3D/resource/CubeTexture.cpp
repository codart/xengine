#include "stdafx.h"
#include "CubeTexture.h"


_X3D_NS_BEGIN

CubeTexture::CubeTexture(const String& strName, GraphicSystem* pGraphicSystem)
: Texture(strName, pGraphicSystem)
{
	_type = TEXTURE_CUBE;
}

CubeTexture::~CubeTexture()
{

}

_X3D_NS_END