#pragma once

#include "x3d/resource/Texture.h"
#include "x3d/resource/ResourceManager.h"

_X3D_NS_BEGIN

/*
//...
enum TextureUsage
{
	TEXTURE_STATIC,
	TEXTURE_DYNAMIC,
	TEXTURE_RENDERTARGET,

	TEXTURE_AUTOMIPMAP,

};
*/

class _X3DExport TextureManager : public Singleton<TextureManager, AutoRelease>
								, public ResourceManager
{
	DECLARE_EVENT_RECEIVER(TextureManager)

public:
	TextureManager(void);
	virtual ~TextureManager(void);


public:
	/// Add a texture (textures that load from image file). strTextureName is the image's name.
	TexturePtr&
		AddTexture(const String& strTextureName);

	/// Add an empty texture that can be used for render target.
	TexturePtr&
		AddRenderTexture(const String& strTextureName, int nWidth, int nHeight
						, PixelFormat pixelFormat = PIXEL_FORMAT_A8R8G8B8
						, PixelFormat depthStencilFormat = PIXEL_FORMAT_D24S8);

	/// Add an empty texture that can be used for dynamic drawing(optimized for lock).
	TexturePtr&
		AddDynamicTexture(const String& strTextureName, int nWidth, int nHeight
						, PixelFormat pixelFormat = PIXEL_FORMAT_A8R8G8B8);


	// Get the exist render texture
	TexturePtr&
		GetRenderTexture(const String& strTextureName);


	// --- internal public methods
	override void 
		init(RenderSystem* pRenderSystem);
	override void 
		unInit();

	//... Used for RenderOperation to get a shared render texture. If the texture not exist, return Null.
	RenderTexture*
		getRenderTexture(const String& strTextureName);
	
	void
		linkBindedTextures(RenderContext& renderContext);
	void
		unlinkBindedTextures();


protected:
	void 
		_onDeviceUninit(const Event* pEvent);
	void 
		_releaseAllTextures();
	void 
		_unLoadAllTextures();

	TextureType
		_texTypeFromD3DRESOURCETYPE(D3DRESOURCETYPE d3dResType);


protected:
	TextureOwnerPtrMap
		_textureMap;


private:
	TexturePtr
		__pNullTexture;

	// For fast access
	// The GBuffer for current rendered window
	Ptr<RenderTexture>
		__pWndGBuffer;
	// The LightBuffer for current rendered window
	Ptr<RenderTexture>
		__pWndLightBuffer;


};


_X3D_NS_END