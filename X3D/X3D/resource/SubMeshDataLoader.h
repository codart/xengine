#pragma once

_X3D_NS_BEGIN

// Base class for all loader class. A loader is an object used for loading data from file/xmlnode/network etc. 
class DataLoader
{
public:
	virtual void 
		LoadFromXMLNode(const XmlNodeHandle& hGeometryNode) { XASSERT(0); }
	virtual void
		Unload() { XASSERT(0); }
	virtual BYTE*
		GetByteStream() {XASSERT(0); return NULL; }

};

class SubMeshDataLoader : public DataLoader
{
public:
	SubMeshDataLoader();
	virtual ~SubMeshDataLoader();


public:
	INLINE int 
		GetVertexCount() { return _nVertexCount; }
	INLINE int 
		GetVertexStride() { return _nVertexStride; }
	INLINE DrawPrimitiveType
		GetDrawPrimitiveType() { return _drawPrimitiveType; }

	INLINE int
		GetBoneWeightCount() { return _nBoneWeightCount; }
	INLINE BOOL
		HasBoneIndices() { return _bHasBoneIndices; }
	INLINE BOOL 
		HasNormal() { return _bHasNormal; }
	INLINE BOOL
		HasTangent() { return _bHasTangent; }
	INLINE BOOL
		HasUV0() { return _bHasUV0; }

	override void 
		LoadFromXMLNode(const XmlNodeHandle& hGeometryNode);
	override void
		Unload();

	override BYTE*
		GetByteStream() { XASSERT(_dataStream.size() > 0);  return &_dataStream.at(0); }


protected:
	DrawPrimitiveType
		_drawPrimitiveType;

	Int 
		_nVertexCount;
	Int 
		_nVertexStride;
	vector<BYTE> 
		_dataStream;

	Boolean
		_bHasNormal;
	Boolean
		_bHasTangent;
	Boolean
		_bHasUV0;

	Boolean
		_bHasBoneIndices; // A DWORD with each byte indicate a bone index
	Int
		_nBoneWeightCount; // 0 ~ 3

};

_X3D_NS_END