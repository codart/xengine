#include "StdAfx.h"
#include "VertexBufferManager.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

VertexBufferManager::VertexBufferManager()
{
}

VertexBufferManager::~VertexBufferManager(void)
{
}

void VertexBufferManager::init(RenderSystem* pRenderSystem)
{	
	ResourceManager::init(pRenderSystem);
}

void VertexBufferManager::unInit()
{
	_releaseAllVertexBuffers();

	ResourceManager::unInit();
}


VertexBufferPtr& VertexBufferManager::AddVertexBuffer(const String& strVertexBufferName
								, UINT nVertexCount
								, UINT nVertexSize
								, BOOL bDynamic)
{
	VertexBufferOwnerPtrMapIt it = _vertexBufferMap.find(strVertexBufferName);
	if(it != _vertexBufferMap.end()) return it->second;

	VertexBuffer* pVertexBuffer(_pGraphicSystem->CreateVertexBuffer(nVertexCount, nVertexSize, bDynamic));
	pVertexBuffer->SetName(strVertexBufferName);
	_vertexBufferMap[strVertexBufferName] = pVertexBuffer;

	return _vertexBufferMap[strVertexBufferName];
}

void VertexBufferManager::_releaseAllVertexBuffers()
{
	_vertexBufferMap.clear();
}

_X3D_NS_END