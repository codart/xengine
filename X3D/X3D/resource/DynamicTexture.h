#pragma once

#include "x3d/resource/Texture.h"

_X3D_NS_BEGIN

class _X3DExport DynamicTexture : public Texture
{
public:
	DynamicTexture(const String& strName, GraphicSystem* pGraphicSystem);
	virtual ~DynamicTexture(void);


public:
	INLINE PixelBuffer*
		GetColorBuffer() { return GetPixelBuffer(0, 0); }
	

protected:


protected:
	override void 
		_clearDependentedRes();

};


typedef ConcretePtr<DynamicTexture, Texture> DynamicTexturePtr;
//...#define GET_TEXTURE_TARGET(pTexturePtr)	SAFE_CAST(&pTexturePtr, RenderTexture*)->GetRenderTarget()

_X3D_NS_END