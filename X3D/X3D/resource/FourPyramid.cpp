#include "stdafx.h"
#include "FourPyramid.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/resource/VertexBufferManager.h"
#include "x3d/resource/IndexBufferManager.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/VertexData.h"

_X3D_NS_BEGIN

const float FourPyramid::DIRECTION_UP = 1.0f;
const float FourPyramid::DIRECTION_DOWN = -1.0f;

FourPyramid::FourPyramid(RenderSystem* pRenderSystem
				   , const String& strName
				   , const ParameterList& parameters)
: PrefabMesh(pRenderSystem, strName)
{
	_type = PREFAB_TYPE_FOUR_PYRAMID; 

	_direction = parameters.GetItem(TEXT("direction"), DIRECTION_UP);
	_height = parameters.GetItem(TEXT("height"), 0.5f);
	_width = parameters.GetItem(TEXT("width"), 1.0f);

	float fHalfWidth = _width/2.0f;

	_pVertexData = XNEW VertexData(_pGraphicSystem);

	_pVertexBuffer = VertexBufferManager::Instance().AddVertexBuffer(strName + TEXT("_VB")
																	, 5
																	, sizeof(Vertex_P_N));
	_pVertexData->AddVertexBufferBinding(_pVertexBuffer, 0);

	Vertex_P_N* pVertexData = (Vertex_P_N*)_pVertexBuffer->Lock(0, _pVertexBuffer->GetBufferSize()
																, LOCK_NORMAL);

	pVertexData[0].position = Vector3::ZERO;
	pVertexData[1].position = Vector3(-fHalfWidth, _direction * _height, -fHalfWidth);
	pVertexData[2].position = Vector3(-fHalfWidth, _direction * _height, fHalfWidth);
	pVertexData[3].position = Vector3(fHalfWidth, _direction * _height, fHalfWidth);
	pVertexData[4].position = Vector3(fHalfWidth, _direction * _height, -fHalfWidth);

	pVertexData[0].normal = Vector3::UNIT_Y * _direction;
	pVertexData[1].normal = Vector3::UNIT_Y * _direction;
	pVertexData[2].normal = Vector3::UNIT_Y * _direction;
	pVertexData[3].normal = Vector3::UNIT_Y * _direction;
	pVertexData[4].normal = Vector3::UNIT_Y * _direction;

	_pVertexBuffer->Unlock();

	// --- Create vertex layout
	_pVertexLayout = _pVertexData->GetVertexLayout();

	_pVertexLayout->BeginUpdate();
	_pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
	_pVertexLayout->AddElement(0, 12, VEF_FLOAT3, VES_NORMAL);
	_pVertexLayout->EndUpdate();


	// --- Create index buffer
	_pIndexData = XNEW IndexData(_pGraphicSystem);

	_pIndexBuffer = IndexBufferManager::Instance().AddIndexBuffer(strName + TEXT("_IB")
																	, 3 * 4
																	, sizeof(UINT));

	_pIndexData->bindIndexBuffer(_pIndexBuffer);

	USHORT* triangleData = (USHORT*)_pIndexBuffer->Lock(0, _pIndexBuffer->GetBufferSize()
		, LOCK_NORMAL);

	int counter = 0;
	triangleData[counter++] = 0;
	triangleData[counter++] = 4;
	triangleData[counter++] = 1;

	triangleData[counter++] = 0;
	triangleData[counter++] = 1;
	triangleData[counter++] = 2;

	triangleData[counter++] = 0;
	triangleData[counter++] = 2;
	triangleData[counter++] = 3;

	triangleData[counter++] = 0;
	triangleData[counter++] = 3;
	triangleData[counter++] = 4;

	_pIndexBuffer->Unlock();
}

FourPyramid::~FourPyramid()
{
	SAFE_DELETE(_pIndexData);
	SAFE_DELETE(_pVertexData);
}

DrawPrimitiveCommand FourPyramid::GetDrawPrimitiveCmd()
{
	// Update draw primitive command
	DrawPrimitiveCommand drawPrimitiveCmd;
	drawPrimitiveCmd.SetVertexData(_pVertexData);
	drawPrimitiveCmd.SetIndexData(_pIndexData);
	drawPrimitiveCmd.SetDrawPrimitiveType(DRAW_TRIANGLE_LIST);
	drawPrimitiveCmd.SetPrimitiveCount(_pIndexData->GetIndexCount() / 3);

	return drawPrimitiveCmd;
}


_X3D_NS_END