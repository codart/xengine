#include "StdAfx.h"
#include "GpuProgramManager.h"

#include "common/util/xmlDocument.h"
#include "Common/util/xmlNodeHandle.h"

#include "Common/util/Unicode.h"
#include "Common/plantform/FileFind.h"

#include "x3d/resource/VertexProgram.h"
#include "x3d/resource/FragmentProgram.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

GPUProgramManager::GPUProgramManager(void)
{
}

GPUProgramManager::~GPUProgramManager(void)
{
}

void GPUProgramManager::init(RenderSystem* pRenderSystem)
{
	ResourceManager::init(pRenderSystem);
	AutoParameter::_initAutoParamMap();
}

void GPUProgramManager::unInit()
{
	AutoParameter::_uninitAutoParamMap();

	// Release all programs
	_releaseAllPrograms();

	ResourceManager::unInit();
}

void GPUProgramManager::_releaseAllPrograms()
{
	_vertexProgramMap.clear();
	_fragmentProgramMap.clear();
}

VertexProgramPtr GPUProgramManager::AddVertexProgram(const String& strName)
{
	// Test code;
	VertexProgramOwnerPtrMapIt it = _vertexProgramMap.find(strName);
	if(it != _vertexProgramMap.end()) return it->second;

	XASSERT(0);
	return VertexProgramPtr(NULL);
}

FragmentProgramPtr GPUProgramManager::AddFragmentProgram(const String& strName)
{
	// Test code;
	FragmentProgramOwnerPtrMapIt it = _fragmentProgramMap.find(strName);
	if(it != _fragmentProgramMap.end()) return it->second;

	XASSERT(0);
	return FragmentProgramPtr(NULL);
}

void GPUProgramManager::LoadAllPrograms()
{
	FileFind find;
	if(find.FindFile(GPU_PROGRAM_SCRIPT_PATH + TEXT("*.xml")))
	{
		_loadProgramsFromFile(find.GetFileName());

		while(find.FindNextFile())
		{
			_loadProgramsFromFile(find.GetFileName());
		}
	}

}

void GPUProgramManager::_loadProgramsFromFile(const String& strFileName)
{
	XASSERT(_pGraphicSystem);

	USING_STRING_CONVERT;

	XmlDocument xmlFile;
	xmlFile.LoadFromFile(StrToAStr(strFileName));

	XmlNodeHandle pGPUProgramsNode = xmlFile.FirstChild("gpu_programs");
	XmlNodeHandle pVertexProgramsNode = pGPUProgramsNode.FirstChild("vertex_program_list");

	if(pVertexProgramsNode)
	{
		XmlNodeHandle pVertexProgramNode = pVertexProgramsNode.FirstChild();

		while (pVertexProgramNode)
		{
			XASSERT(strcmp(pVertexProgramNode.GetName(), "vertex_program") == 0);

			VertexProgram* pProgram = _pGraphicSystem->CreateVertexProgram();
			pProgram->LoadFromXMLNode(pVertexProgramNode);
			_vertexProgramMap[pProgram->GetName()] = pProgram;

			pVertexProgramNode = pVertexProgramNode.NextSiblingNode();
		}
	}

	XmlNodeHandle pFragmentProgramsNode = pGPUProgramsNode.FirstChild("fragment_program_list");

	if(pFragmentProgramsNode)
	{
		XmlNodeHandle pFragmentProgramNode = pFragmentProgramsNode.FirstChild();

		while (pFragmentProgramNode)
		{
			XASSERT(strcmp(pFragmentProgramNode.GetName(), "fragment_program") == 0);

			FragmentProgram* pProgram = _pGraphicSystem->CreateFragmentProgram();
			pProgram->LoadFromXMLNode(pFragmentProgramNode);
			_fragmentProgramMap[pProgram->GetName()] = pProgram;

			pFragmentProgramNode = pFragmentProgramNode.NextSiblingNode();
		}
	}

	return;
}

_X3D_NS_END