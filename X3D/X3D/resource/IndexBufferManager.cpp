#include "StdAfx.h"
#include "IndexBufferManager.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

IndexBufferManager::IndexBufferManager()
{
}

IndexBufferManager::~IndexBufferManager(void)
{
}

void IndexBufferManager::init(RenderSystem* pRenderSystem)
{	
	ResourceManager::init(pRenderSystem);
}

void IndexBufferManager::unInit()
{
	_releaseAllIndexBuffers();

	ResourceManager::unInit();
}


IndexBufferPtr& IndexBufferManager::AddIndexBuffer(const String& strVertexBufferName
								, UINT nVertexCount
								, UINT nVertexSize
								, BOOL bDynamic)
{
	IndexBufferOwnerPtrMapIt it = _indexBufferMap.find(strVertexBufferName);
	if(it != _indexBufferMap.end()) return it->second;

	IndexBuffer* pIndexBuffer(_pGraphicSystem->CreateIndexBuffer(nVertexCount, nVertexSize, bDynamic));
	pIndexBuffer->SetName(strVertexBufferName);
	_indexBufferMap[strVertexBufferName] = pIndexBuffer;

	return _indexBufferMap[strVertexBufferName];
}

void IndexBufferManager::_releaseAllIndexBuffers()
{
	_indexBufferMap.clear();
}

_X3D_NS_END