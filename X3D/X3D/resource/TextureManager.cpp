#include "StdAfx.h"
#include "TextureManager.h"

#include "X3D/graphic/DeviceEvent.h"
#include "X3D/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

TextureManager::TextureManager(void)
{
	_pGraphicSystem = NULL;
	__pWndGBuffer = NULL;
	__pWndLightBuffer = NULL;
}

TextureManager::~TextureManager(void)
{
	_releaseAllTextures();
}

void TextureManager::init(RenderSystem* pRenderSystem)
{
	ResourceManager::init(pRenderSystem);
	_pGraphicSystem->AddListener(DeviceEvent::DEVICE_BEFORE_UNINIT, _onDeviceUninit);

	__pWndGBuffer = SAFE_CAST(_pGraphicSystem->CreateTexture(TEXTURE_RENDER_TARGET, TEXT("WindowGBuffer")), RenderTexture*);
	__pWndLightBuffer = SAFE_CAST(_pGraphicSystem->CreateTexture(TEXTURE_RENDER_TARGET, TEXT("WindowLightBuffer")), RenderTexture*);

	_textureMap[TEXT("WindowGBuffer")] = __pWndGBuffer;
	_textureMap[TEXT("WindowLightBuffer")] = __pWndLightBuffer;
}

void TextureManager::unInit()
{
	unlinkBindedTextures();

	__pWndGBuffer = NULL;
	__pWndLightBuffer = NULL;

	_releaseAllTextures();
	_pGraphicSystem->RemoveListener(DeviceEvent::DEVICE_BEFORE_UNINIT, _onDeviceUninit);

	ResourceManager::unInit();
}

void TextureManager::_onDeviceUninit(const Event* pEvent)
{
	_unLoadAllTextures();
}

void TextureManager::_unLoadAllTextures()
{
	TextureOwnerPtrMapIt it = _textureMap.begin();
	TextureOwnerPtrMapIt itEnd = _textureMap.end();

	for ( ; it != itEnd; ++it)
	{
		TexturePtr pTexturePtr = it->second;
		pTexturePtr->Unload();
	}
}

void TextureManager::_releaseAllTextures()
{
	_textureMap.clear();
}

TexturePtr& TextureManager::AddTexture(const String& strTextureName)
{
	TextureOwnerPtrMapIt  it = _textureMap.find(strTextureName);
	if(it != _textureMap.end()) return it->second;


	String strFullName = TEXTURE_PATH + strTextureName;
	D3DXIMAGE_INFO imageInfo;
	HRESULT hr = D3DXGetImageInfoFromFile(strFullName.c_str(), &imageInfo);

	TextureType textureType = _texTypeFromD3DRESOURCETYPE(imageInfo.ResourceType);
	
	// Create a new texture
	Texture* pTexture = _pGraphicSystem->CreateTexture(textureType, strTextureName);
	pTexture->LoadImage(strFullName);

	_textureMap[strTextureName] = pTexture;

	return _textureMap[strTextureName];
}

TextureType TextureManager::_texTypeFromD3DRESOURCETYPE(D3DRESOURCETYPE d3dResType)
{
	TextureType type = TEXTURE_INVALID;

	switch (d3dResType)
	{
	case D3DRTYPE_TEXTURE:
		type = TEXTURE_NORMAL;
		break;

	case D3DRTYPE_CUBETEXTURE:
		type = TEXTURE_CUBE;
		break;

	case D3DRTYPE_VOLUME:
		type = TEXTURE_VOLUMN;
		break;
	}

	XASSERT(type != TEXTURE_INVALID);

	return type;
}

TexturePtr& TextureManager::AddRenderTexture(const String& strTextureName, int nWidth, int nHeight
											, PixelFormat pixelFormat, PixelFormat depthStencilFormat)
{
	TextureOwnerPtrMapIt  it = _textureMap.find(strTextureName);
	if(it != _textureMap.end()) return it->second;

	// Create a new texture
	Texture* pTexture = (Texture*)_pGraphicSystem->CreateRenderTexture(strTextureName, nWidth, nHeight
																		, pixelFormat, depthStencilFormat);
	XASSERT(pTexture);
	_textureMap[strTextureName] = pTexture;
	return _textureMap[strTextureName];
}

TexturePtr& TextureManager::AddDynamicTexture(const String& strTextureName, int nWidth, int nHeight
									, PixelFormat pixelFormat)
{
	TextureOwnerPtrMapIt  it = _textureMap.find(strTextureName);
	if(it != _textureMap.end()) return it->second;

	// Create a new texture
	Texture* pTexture = (Texture*)_pGraphicSystem->CreateDynamicTexture(strTextureName, nWidth, nHeight, pixelFormat);
	XASSERT(pTexture);
	_textureMap[strTextureName] = pTexture;
	return _textureMap[strTextureName];
}

TexturePtr& TextureManager::GetRenderTexture(const String& strTextureName)
{
	TextureOwnerPtrMapIt  it = _textureMap.find(strTextureName);
	if(it != _textureMap.end()) return it->second;

	return __pNullTexture;
}

RenderTexture* TextureManager::getRenderTexture(const String& strTextureName)
{
	TextureOwnerPtrMapIt  it = _textureMap.find(strTextureName);
	if(it != _textureMap.end()) return (RenderTexture*)it->second.getPointer();

	return NULL;
}

void TextureManager::linkBindedTextures(RenderContext& renderContext)
{
	//__pWndGBuffer->bind(renderContext.pRenderWindow->getGBuffer());

	InternalObjectHandle hHandle;
	renderContext.pRenderWindow->getGBuffer()->QueryInternalHandle(&hHandle);
	__pWndGBuffer->SetInternalHandle(hHandle);

	renderContext.pRenderWindow->getLightBuffer()->QueryInternalHandle(&hHandle);
	__pWndLightBuffer->SetInternalHandle(hHandle);
}

void TextureManager::unlinkBindedTextures()
{
	__pWndGBuffer->SetInternalHandle(0);
	__pWndLightBuffer->SetInternalHandle(0);
}

_X3D_NS_END