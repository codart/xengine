#include "stdafx.h"
#include "PostProcessRenderOP.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/material/MaterialManager.h"
#include "x3d/resource/TextureManager.h"
#include "x3d/render/FullScreenQuad.h"

#include "x3d/scene/MovableObject.h"
#include "x3d/scene/SubMeshRenderable.h"
#include "x3d/scene/StaticMeshEntity.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

PrePostProcessOP::PrePostProcessOP(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	_pRenderTexture = TextureManager::Instance().AddRenderTexture(TEXT("SharedPostProcessTexture"), 200, 200);
	_pOutputRenderTextures[0] = SAFE_CAST(_pRenderTexture.getPointer(), RenderTexture*);
}

PrePostProcessOP::~PrePostProcessOP()
{
	
}

void PrePostProcessOP::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	RenderTarget* pRenderTarget = SAFE_CAST(_pRenderTexture.getPointer(), RenderTexture*)->GetRenderTarget();
	_pGraphicSystem->SetRenderTarget(pRenderTarget);
	_pGraphicSystem->ClearFrameBuffer(_pRenderContext->pDestRenderArea->GetBackgroundColor());

	_renderOpaqueObjsWithMtl();
}

//////////////////////////////////////////////////////////////////////////
PostProcessOP::PostProcessOP(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	_pPPSharedRT = TextureManager::Instance().AddRenderTexture(TEXT("SharedPostProcessTexture"), 200, 200);
	_pOutputRenderTextures[0] = SAFE_CAST(_pPPSharedRT.getPointer(), RenderTexture*);

	_pQuadMaterial = MaterialManager::Instance().AddMaterial(TEXT("PostEffectMtl1"));
}

PostProcessOP::~PostProcessOP()
{

}

void PostProcessOP::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	if(renderContext.pNextRenderOP)
	{
		_pGraphicSystem->SetRenderTarget(SAFE_CAST(_pPPSharedRT.getPointer(), RenderTexture*)->GetRenderTarget());
	}
	else
	{
		_pGraphicSystem->SetRenderTarget(renderContext.GetWndRenderTarget());
		_pGraphicSystem->SetViewport(renderContext.pDestRenderArea->GetViewportInfo());
	}

	// If it is not a full screen quad, need clear before render!
	//_pGraphicSystem->ClearFrameBuffer(_pRenderContext->pDestRenderArea->GetBackgroundColor());

	// Render a quad
	Technique* pTechnique = _pQuadMaterial->GetBestTechnique();

	PassIterator passeIterator(pTechnique->GetPasses());
	while(passeIterator.HasMore())
	{
		_setPass(passeIterator.CurrentData());
		_pGraphicSystem->DrawPrimitive(_pRenderSystem->GetFullScreenQuad()->GetRenderOperation());
		passeIterator.Next();
	}
}


_X3D_NS_END