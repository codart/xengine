#pragma once

#include "x3d/render/SceneRenderContext.h"

_X3D_NS_BEGIN

class RenderTarget;
class Pass;
class SceneRender;
class RenderTexture;
class RenderSystem;
class RenderArea;
class RenderWindow;
class Camera;
class GraphicSystem;

/*
Defines a process of rendering 

The general render operation includes processes list below,
	*. Set view port, render target
	*. Update AutoParameterProvider

	*. BeginScene
	*. Set Material(can use object's material, or force use another material. Take deferred shading for 
		example, we need force to render object with into Position/Normal/Depth data).
	*. Draw all primitives specified by the RENDER_OBJ_SOURCE(can be scene, baking objects, FullScreenQuad, etc.)
	*. EndScene

With one or more RenderOperation, we can define various rendering method, such as the DeferedShading/Pre-ZPass
Rendering.

*/
class RenderOperation
{
	const static int MAX_OUTPUT_RENDER_TEXTURE = 4;

public:
	RenderOperation(RenderSystem* pRenderSystem);
	virtual ~RenderOperation(void);


public:
	virtual void
		Init(RenderWindow* pRenderWindow);
	virtual void 
		Render(RenderContext& renderContext);

	INLINE RenderTexture*
		GetOutputRenderTexture(int iIndex) const
	{
		XASSERT(_pOutputRenderTextures[iIndex]); 
		return _pOutputRenderTextures[iIndex];
	}


protected:
	final INLINE void 
		_setPass(Pass* pPass);
	void 
		_renderOpaqueObjsWithMtl();
	void 
		_renderTransparentObjsWithMtl();

	INLINE void 
		_renderSingleObjectWithMtl(RendableObject* pRenderableObj);
	void 
		_renderSingleSolidWhiteObject(RendableObject* pRenderableObject);


protected:
	RenderSystem*
		_pRenderSystem;
	GraphicSystem*
		_pGraphicSystem;
	RenderWindow*
		_pRenderWindow;

	transient RenderContext* 
		_pRenderContext;


	/* 
		If RenderOP output RenderTexture, it should always output it! This is because the next RenderOP may depend
		on this RenderTexture. A variable output texture can cause the next RenderOP crashed!
	*/
	RenderTexture*
		_pOutputRenderTextures[MAX_OUTPUT_RENDER_TEXTURE];


debug:
	debug_variable(DWORD, type);

};

DECLARE_LIST_TYPE_PTR(vector, RenderOperation);

_X3D_NS_END