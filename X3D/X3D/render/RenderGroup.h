#pragma once

#include "x3d/render/RenderObject.h"
#include "x3d/scene/RendableObject.h"

_X3D_NS_BEGIN

enum DefaultRenderGroupID
{
	RENDER_GROUP_INVALID = 0xffffffff,

	RENDER_GROUP_BACKGROUND	= 1 << 0,
	RENDER_GROUP_OPAQUE		= 1 << 1,
	RENDER_GROUP_TRANSPARENT	= 1 << 2,
	RENDER_GROUP_FOREGROUND	= 1 << 3,

	RENDER_GROUP_DEPTHSORTED	= RENDER_GROUP_OPAQUE | RENDER_GROUP_TRANSPARENT,
};

enum ScenePass
{
	PASS_Unlit,
	PASS_UnlitHitTest,
	PASS_DepthSortedHitTest,
	PASS_OpaqueEmissive,
	PASS_OpaqueLighting,
	PASS_TranslucentEmissive,
	PASS_TranslucentLighting,
	PASS_UnlitTranslucency,
	PASS_ShadowDepth,
	PASS_LightFunction
};


class RenderGroup : public RenderObject
{
public:
	RenderGroup(RenderSystem* pRenderSystem, DWORD dwID);
	virtual ~RenderGroup(void);

public:
	INLINE DWORD
		GetID(){ return _dwID; }
	INLINE void 
		AddRenderableObject(RendableObject* pRenderableObj)
	{
		_RendableObjectList.push_back(pRenderableObj);
	}

	INLINE const RendableObjectList& 
		GetRendableObjectList() const { return _RendableObjectList; }

	INLINE void
		Clear(){ _RendableObjectList.clear(); }

	void
		SortByZOrder();


protected:
	DWord
		_dwID;
	
	RendableObjectList
		_RendableObjectList;


};


DECLARE_MAP_TYPE_PTR(map, DWORD, RenderGroup);

_X3D_NS_END