#include "StdAfx.h"
#include "RenderQueue.h"

#include "x3d/render/RenderSystem.h"
#include "x3d/render/SceneRender.h"


_X3D_NS_BEGIN

RenderQueue::RenderQueue(RenderSystem* pRenderSystem)
: RenderObject(pRenderSystem)
{
	__pCurRenderContext = NULL;

	_pOpaqueGroup = AddRenderGroup(RENDER_GROUP_OPAQUE);
	_pTransparentGroup = AddRenderGroup(RENDER_GROUP_TRANSPARENT);
	_pLightGroup = XNEW LightGroup(pRenderSystem);
}

RenderQueue::~RenderQueue(void)
{
	RenderGroupMapIterator renderGroupIterator(_renderGroupMap);
	while(renderGroupIterator.HasMore())
	{
		XDELETE renderGroupIterator.CurrentData();
		renderGroupIterator.Next();
	}

	SAFE_DELETE(_pLightGroup);
}

RenderGroup* RenderQueue::AddRenderGroup(DWORD dwGroupID)
{
	// Check if exist
	RenderGroupMapIt it = _renderGroupMap.find(dwGroupID);
	if(it != _renderGroupMap.end()) return it->second;

	RenderGroup* pRenderGroup = _pRenderSystem->CreateRenderGroup(dwGroupID);
	_renderGroupMap.insert(make_pair(pRenderGroup->GetID(), pRenderGroup));

	return pRenderGroup;
}

RenderGroup* RenderQueue::GetRenderGroup(DWORD dwType)
{
	return _renderGroupMap[dwType];
}

void RenderQueue::clearRenderQueue()
{
	// Clear all render groups
	RenderGroupMapIt it = _renderGroupMap.begin();
	RenderGroupMapIt itEnd = _renderGroupMap.end();

	for ( ; it != itEnd; ++it)
	{
		it->second->Clear();
	}

	// Clear light group
	_pLightGroup->Clear();
}

void RenderQueue::BeginUpdate(RenderContext* pCurRenderContex)
{
	__pCurRenderContext = pCurRenderContex;
	clearRenderQueue();

	// Fill the global lights into LightGroup
	const LightList& globalLight = __pCurRenderContext->pScene->GetGlobalLights();
	_pLightGroup->AddLights(globalLight);

}

void RenderQueue::EndUpdate()
{

}

void RenderQueue::VisitBegin()
{

}

void RenderQueue::ProcessNodes(const SceneNodeList& sceneNodes, BOOL bFullVisible)
{
// 		if ( mShowBoxes )
// 		{
// 			mBoxes.push_back( octant->getWireBoundingBox() );
// 		}

	BOOL bVisible = TRUE;

	FASTEST_VECTOR_ITERATE(SceneNode*, sceneNodes);
		SceneNode* pSceneNode = GET_NEXT(sceneNodes);

		// if this octree is partially visible, manually cull all
		// scene nodes attached directly to this level.

		if ( !bFullVisible )
			bVisible = __pCurRenderContext->pCamera->getVisibility( pSceneNode->GetWorldAABB() );

		if ( bVisible )
		{			
			// Iterate all the movable objects that be attached to the scene node.
			pSceneNode->fillRenderQueue(this);
			//..._nNumRenderedObject++;

			/*
			pSceneNode->_addToRenderQueue(camera, queue, onlyShadowCasters, visibleBounds );

			mVisible.push_back( pSceneNode );

			if ( mDisplayNodes )
				queue -> addRenderable( pSceneNode->getDebugRenderable() );
			*/

			// check if the scene manager or this node wants the bounding box shown.
			//if (pSceneNode->getShowBoundingBox() || mShowBoundingBoxes)
			//	pSceneNode->_addBoundingBoxToQueue(queue);
		}			

	FASTEST_ITERATE_END();

}

void RenderQueue::AddToOpaqueGroup(RendableObject* pRenderableObj)
{
	XASSERT(_pOpaqueGroup);
	_pOpaqueGroup->AddRenderableObject(pRenderableObj);
}

void RenderQueue::AddToTransparentGroup(RendableObject* pRenderableObj)
{
	XASSERT(_pTransparentGroup);
	_pTransparentGroup->AddRenderableObject(pRenderableObj);
}

void RenderQueue::AddToLightGroup(Light* pLight)
{
	XASSERT(_pLightGroup);
	_pLightGroup->AddLight(pLight);
}

void RenderQueue::ProcessNode(const SceneNode* pSceneNode)
{

}

void RenderQueue::VisitEnd()
{
	// Sort transparent objects by Z-Order
	_pTransparentGroup->SortByZOrder();
}




_X3D_NS_END