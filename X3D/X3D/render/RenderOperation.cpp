#include "StdAfx.h"
#include "RenderOperation.h"

#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"
#include "x3d/graphic/WindowRenderTarget.h"

#include "x3d/material/Pass.h"
#include "x3d/render/SceneRender.h"
#include "x3d/render/RenderQueue.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/resource/GpuProgramManager.h"


_X3D_NS_BEGIN

RenderOperation::RenderOperation(RenderSystem* pRenderSystem)
{
	_pRenderContext = NULL;
	_pRenderSystem = pRenderSystem;
	_pGraphicSystem = _pRenderSystem->GetGraphicSystem();

	memset(_pOutputRenderTextures, 0, sizeof(_pOutputRenderTextures));
}

RenderOperation::~RenderOperation(void)
{
}

void RenderOperation::Init(RenderWindow* pRenderWindow)
{
	_pRenderWindow = pRenderWindow;
}

void RenderOperation::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;
	TexturePtr pSharedReflectMap = TextureManager::Instance().AddRenderTexture(TEXT("SharedReflectMap"), 512, 512);

	_pGraphicSystem->SetRenderTarget(_pRenderContext->GetWndRenderTarget());
	//_pGraphicSystem->SetRenderTarget(SAFE_CAST(&pSharedReflectMap, RenderTexture*)->GetRenderTarget());
	_pGraphicSystem->SetViewport(_pRenderContext->pDestRenderArea->GetViewportInfo());

	_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_ALL, _pRenderContext->pDestRenderArea->GetBackgroundColor());

	// Render opaque objects
	_renderOpaqueObjsWithMtl();

	_renderTransparentObjsWithMtl();
}

void RenderOperation::_renderSingleObjectWithMtl(RendableObject* pRenderableObject)
{
	_pRenderContext->GetAutoParamDataProvider().SetCurrentRenderableObject(pRenderableObject);

	Technique* pTechnique = pRenderableObject->GetTechnique();

	PassList& passes = pTechnique->GetPasses();
	FASTEST_VECTOR_ITERATE(Pass*, passes);
		_setPass(GET_NEXT(passes));
		_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());
	FASTEST_ITERATE_END();
}

void RenderOperation::_renderSingleSolidWhiteObject(RendableObject* pRenderableObject)
{
	AutoParameterDataProvider& autoParamProvider = _pRenderContext->GetAutoParamDataProvider();

	autoParamProvider.SetCurrentRenderableObject(pRenderableObject);

	VertexProgramPtr pVertexProgram = GPUProgramManager::Instance().AddVertexProgram(TEXT("SolidWhiteVS"));
	FragmentProgramPtr pFragmentProgram = GPUProgramManager::Instance().AddFragmentProgram(TEXT("SolidWhiteFS"));

	pVertexProgram->UpdateAutoParameters(&autoParamProvider);
	pFragmentProgram->UpdateAutoParameters(&autoParamProvider);

	_pGraphicSystem->SetShader(&Shader(pVertexProgram, pFragmentProgram));

	_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());
}

void RenderOperation::_renderTransparentObjsWithMtl()
{
	XASSERT(_pRenderContext && _pRenderContext->GetSceneRender());
	SceneRender* pSceneRender = _pRenderContext->GetSceneRender();

	RenderQueue* pRenderQueue = pSceneRender->GetRenderQueue();
	RenderGroup* pTransparentGroup = pRenderQueue->GetTransparentGroup();

	const RendableObjectList& renderableObjList = pTransparentGroup->GetRendableObjectList();

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);

		RendableObject* pRenderableObject = GET_NEXT(renderableObjList);
		_renderSingleObjectWithMtl(pRenderableObject);

	FASTEST_ITERATE_END();
}

void RenderOperation::_renderOpaqueObjsWithMtl()
{
	// Start rendering
	XASSERT(_pRenderContext && _pRenderContext->GetSceneRender());
	SceneRender* pSceneRender = _pRenderContext->GetSceneRender();
	Scene* pScene = _pRenderContext->pScene;

	RenderQueue* pRenderQueue = pSceneRender->GetRenderQueue();
	RenderGroup* pOpaqueGroup = pRenderQueue->GetOpaqueGroup();
	
	pSceneRender->GetAutoParamProvider().SetCurrentLightList(&pRenderQueue->GetLightGroup()->GetLightList());
	const RendableObjectList& renderableObjList = pOpaqueGroup->GetRendableObjectList();

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);

		RendableObject* pRenderableObject = GET_NEXT(renderableObjList);
		_renderSingleObjectWithMtl(pRenderableObject);

	FASTEST_ITERATE_END();
}

void RenderOperation::_setPass(Pass* pPass)
{
	// Set pass basic states
	_pGraphicSystem->SetSceneBlending( pPass->GetSrcBlendFactor()
									, pPass->GetDestBlendFactor()
									, pPass->GetFrameBlendOperation() );

	_pGraphicSystem->SetCullMode(pPass->GetHardwareCullMode());
	_pGraphicSystem->SetDepthBufferParams(pPass->GetDepthEnable(), pPass->GetDepthWrite()
											, pPass->GetDepthCompareFunction());
	_pGraphicSystem->SetStencilBufferParams(pPass->GetStencilEnable()
											, pPass->GetStencilCompareFunc(), pPass->GetStencilRefValue()
											, pPass->GetStencilMask(), pPass->GetStencilFailOp()
											, pPass->GetStencilPassZFailOp(), pPass->GetStencilPassOp()
											, pPass->GetTwoSizeStencilEnable());

	_pGraphicSystem->SetAlphaTestParams(pPass->GetAlphaTestEnable()
										, pPass->GetAlphaCompareFunc()
										, pPass->GetAlphaRefValue()
										, pPass->GetAlphaToCoverage());

	_pGraphicSystem->EnableColorBufferWrite(pPass->GetColorBufferWrite());

	_pGraphicSystem->SetTextureUnitStates(pPass->GetTextureUnitStates());

	_pGraphicSystem->SetVertexTextureUnitStates(pPass->GetVertexTextureUnitStates());

	//... Set shader constant
	XASSERT(_pRenderContext && _pRenderContext->GetSceneRender());
	Shader* pShader = pPass->GetShader();
	pShader->UpdateAutoParameters(&_pRenderContext->GetAutoParamDataProvider());

	_pGraphicSystem->SetShader(pShader);
}


/*  testing code for rendering mirror effect

// Get mirror object (Hardcode)
RenderQueue* pRenderQueue = _pRenderContext->pSceneRender->GetRenderQueue();
RenderGroup* pTransparentGroup = pRenderQueue->GetTransparentGroup();

const RendableObjectList& transparentObjs = pTransparentGroup->GetRendableObjectList();
RendableObject* pMirrorObject = transparentObjs[0];

// Prepare the reflect map(shared, need clear each time using)
_pGraphicSystem->SetRenderTarget(SAFE_CAST(&pSharedReflectMap, RenderTexture*)->GetRenderTarget());
_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_COLOR | FRAME_BUFFER_DEPTH
, _pRenderContext->pDestRenderArea->GetBackgroundColor());

// Prepare a reflect camera
Camera reflectedCamera(_pRenderContext->pCamera);
reflectedCamera.ReflectByPlane(Plane(0, 1, 0, 0));
_pRenderContext->pSceneRender->GetAutoParamProvider().SetCurrentCamera(&reflectedCamera);

// Set clip plane, only render the reflected scene objects that after the plane
Plane projClipPlane = Plane(0, 1, 0, 0);

projClipPlane = projClipPlane * reflectedCamera.getViewProjMatrix();

_pGraphicSystem->SetClipPlane(0, TRUE, projClipPlane);
// Render Reflected scene objects
_renderOpaqueObjsWithMtl();
_pGraphicSystem->SetClipPlane(0, FALSE);

// Render mirror (sampler the reflect map) to dest render target
_pRenderContext->pSceneRender->GetAutoParamProvider().SetCurrentCamera(_pRenderContext->pCamera);
_pGraphicSystem->SetRenderTarget(_pRenderContext->pDestRenderTarget);
_renderSingleObjectWithMtl(pMirrorObject);
*/

_X3D_NS_END