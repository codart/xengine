#include "stdafx.h"
#include "RenderShadowDepthMap.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/resource/RenderTexture.h"
#include "x3d/render/SceneRender.h"

#include "x3d/material/MaterialManager.h"
#include "x3d/render/RenderQueue.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"


_X3D_NS_BEGIN

RenderShadowDepthMap::RenderShadowDepthMap(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	_pShadowDepthMap = pRenderSystem->GetSharedShadowDepthMap(0);
	_pRenderShadowMapMtl = MaterialManager::Instance().AddMaterial(TEXT("RenderShadowMapMtl"));

	_pOutputRenderTextures[0] = SAFE_CAST(_pShadowDepthMap.getPointer(), RenderTexture*);
}

RenderShadowDepthMap::~RenderShadowDepthMap()
{

}

void RenderShadowDepthMap::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	RenderTarget* pRenderTarget = SAFE_CAST(_pShadowDepthMap.getPointer(), RenderTexture*)->GetRenderTarget();
	_pGraphicSystem->SetRenderTarget(pRenderTarget);
	_pGraphicSystem->ClearFrameBuffer(COLOR_ARGB(0, 0, 1, 0));

	_renderSceneShadowDepthMap();
}

void RenderShadowDepthMap::_renderSceneShadowDepthMap()
{
	// Traverse all scene objects, render the objects that has CAST_SHADOW flag with RenderShadowMapMaterial
	XASSERT(_pRenderContext && _pRenderContext->GetSceneRender());
	SceneRender* pSceneRender = _pRenderContext->GetSceneRender();
	Scene* pScene = _pRenderContext->pScene;
	const Light* pLight0 = pScene->GetGlobalLight(0);

	if(!pLight0->IsShadowCastingEnabled()) return;

	const Camera* pLightCamera = pLight0->getShadowMapCamera();
	_pRenderContext->GetAutoParamDataProvider().SetShadowMapCamera(pLightCamera);

	RenderQueue* pRenderQueue = pSceneRender->GetRenderQueue();
	RenderGroup* pOpaqueGroup = pRenderQueue->GetRenderGroup(RENDER_GROUP_OPAQUE);

	const RendableObjectList& renderableObjList = pOpaqueGroup->GetRendableObjectList();

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);
		RendableObject* pRenderableObject = GET_NEXT(renderableObjList);

		pSceneRender->GetAutoParamProvider().SetCurrentRenderableObject(pRenderableObject);

		pSceneRender->GetAutoParamProvider().SetCurrentLightList(&pScene->GetGlobalLights());

		Technique* pTechnique = _pRenderShadowMapMtl->GetBestTechnique();

		PassList& passes = pTechnique->GetPasses();
		FASTEST_VECTOR_ITERATE(Pass*, passes);
			_setPass(GET_NEXT(passes));
			_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());
		FASTEST_ITERATE_END();

	FASTEST_ITERATE_END();

}

_X3D_NS_END