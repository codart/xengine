#include "StdAfx.h"
#include "FullScreenQuad.h"

#include "x3d/resource/IndexData.h"
#include "x3d/resource/VertexData.h"
#include "x3d/graphic/IndexBuffer.h"
#include "x3d/graphic/VertexBuffer.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/graphic/DrawPrimitiveCommand.h"

_X3D_NS_BEGIN

FullScreenQuad::FullScreenQuad(GraphicSystem* pGraphicSystem)
{
	_pDrawQuadPrimitiveCmd = XNEW DrawPrimitiveCommand;
	_pDrawQuadPrimitiveCmd->SetPrimitiveCount(2);
	_pDrawQuadPrimitiveCmd->SetDrawPrimitiveType(DRAW_TRIANGLE_LIST);

	_pQuadIndexData = XNEW IndexData(pGraphicSystem);
	_pQuadVertexData = XNEW VertexData(pGraphicSystem);

	_pDrawQuadPrimitiveCmd->SetIndexData(_pQuadIndexData);
	_pDrawQuadPrimitiveCmd->SetVertexData(_pQuadVertexData);

	IndexBuffer* pIndexBuffer = _pQuadIndexData->initIndexBuffer(6, sizeof(USHORT));

/*
	0 --- 1
	|\	  |
	|  \  |
	|	\ |
	3 --- 2	
*/

	USHORT indices[6];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 2;
	indices[4] = 3;
	indices[5] = 0;

	pIndexBuffer->WriteData(0, 6 * sizeof(USHORT), indices);

	VertexBuffer* pVertexBuffer = _pQuadVertexData->AddVertexBufferBinding(4, sizeof(QuadVertex2D));

	QuadVertex2D vertices[4];
	vertices[0].position.SetVector(-1, 1, 0);
	vertices[0].uv1.x = 0;
	vertices[0].uv1.y = 0;

	vertices[1].position.SetVector(1, 1, 0);
	vertices[1].uv1.x = 1;
	vertices[1].uv1.y = 0;

	vertices[2].position.SetVector(1, -1, 0);
	vertices[2].uv1.x = 1;
	vertices[2].uv1.y = 1;

	vertices[3].position.SetVector(-1, -1, 0);
	vertices[3].uv1.x = 0;
	vertices[3].uv1.y = 1;

	pVertexBuffer->WriteData(0, 4 * sizeof(QuadVertex2D), vertices);

	VertexLayout* pVertexLayout = _pQuadVertexData->GetVertexLayout();
	pVertexLayout->BeginUpdate();
	pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
	pVertexLayout->AddElement(0, 12, VEF_FLOAT2, VES_TEXTURE_COORDINATES);
	pVertexLayout->EndUpdate();
}

FullScreenQuad::~FullScreenQuad(void)
{
	SAFE_DELETE(_pQuadVertexData);
	SAFE_DELETE(_pQuadIndexData);
	SAFE_DELETE(_pDrawQuadPrimitiveCmd);
}


_X3D_NS_END