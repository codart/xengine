#pragma once

#include "x3d/render/RenderObject.h"
#include "common/core/Factory.h"

_X3D_NS_BEGIN

class IntrinsicRenderableFactory : public Factory
								, public RenderObject
{
public:
	IntrinsicRenderableFactory(RenderSystem* pRenderSystem);
	virtual ~IntrinsicRenderableFactory();


public:
	override void
		Init(DWORD dwInstallData) ;
	override void
		Destroy();

	override const RenderableTypeList&
		GetSupporttedTypes() { return _supporttedTypes; }

	override Object*
		CreateObject(DWORD dwCreationData);


protected:
	RenderableTypeList
		_supporttedTypes;

};


_X3D_NS_END