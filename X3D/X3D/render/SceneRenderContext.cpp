#include "stdafx.h"
#include "SceneRenderContext.h"

#include "x3d/render/SceneRender.h"
#include "x3d/RenderArea.h"
#include "x3d/graphic/WindowRenderTarget.h"

_X3D_NS_BEGIN

SceneRender* RenderContext::GetSceneRender() 
{
	return pRenderWindow->getSceneRender(); 
}

RenderTarget* RenderContext::GetWndRenderTarget()
{
	return pRenderWindow->getWndRenderTarget();
}

AutoParameterDataProvider& RenderContext::GetAutoParamDataProvider()
{
	return GetSceneRender()->GetAutoParamProvider(); 
}

RenderWindow* RenderContext::GetRenderWindow()
{
	return pDestRenderArea->GetRenderWindow(); 
}

_X3D_NS_END