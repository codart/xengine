#include "StdAfx.h"
#include "AutoParameterDataProvider.h"

_X3D_NS_BEGIN

const UINT MATRIX4_FLOAT4_COUNT = 4;
const UINT MATRIX3X4_FLOAT4_COUNT = 3;
const UINT VECTOR2_FLOAT4_COUNT = 1;
const UINT VECTOR3_FLOAT4_COUNT = 1;
const UINT VECTOR4_FLOAT4_COUNT = 1;


AutoParameterDataProvider::AutoParameterDataProvider(void)
{
	_dataEntries.resize(APT_COUNT, DataEntry(0, 0, TRUE));

	_dataEntries[APT_world_matrix].pDataPointer = (DataPointer)&_worldMatrixTranspose;
	_dataEntries[APT_world_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_invert_transpose_world].pDataPointer = (DataPointer)&_invTranspWorldTranspose;
	_dataEntries[APT_invert_transpose_world].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_world_view_proj_matrix].pDataPointer = (DataPointer)&_worldViewProjTranspose;
	_dataEntries[APT_world_view_proj_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_invert_transpose_world_view].pDataPointer = (DataPointer)&_invTranspWorldView;
	_dataEntries[APT_invert_transpose_world_view].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_invert_view_matrix].pDataPointer = (DataPointer)&_invViewTranspose;
	_dataEntries[APT_invert_view_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_view_proj_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_light_space_wvp_matrix].pDataPointer = (DataPointer)&_lightWVPTranspose;
	_dataEntries[APT_light_space_wvp_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;

	_dataEntries[APT_world_view_matrix].pDataPointer = (DataPointer)&_worldViewMatrixTranspose;
	_dataEntries[APT_world_view_matrix].nFloat4Count = MATRIX4_FLOAT4_COUNT;
	_dataEntries[APT_camera_position].pDataPointer = (DataPointer)&_cameraPosition;
	_dataEntries[APT_camera_position].nFloat4Count = VECTOR3_FLOAT4_COUNT;

	_dataEntries[APT_ambient_light_color].pDataPointer = (DataPointer)&_ambientLightColor;
	_dataEntries[APT_ambient_light_color].nFloat4Count = VECTOR4_FLOAT4_COUNT;
	_dataEntries[APT_viewport_size].pDataPointer = (DataPointer)&_vViewportSize;
	_dataEntries[APT_viewport_size].nFloat4Count = VECTOR2_FLOAT4_COUNT;

#ifndef _DEBUG
	_dataEntries[APT_ambient_light_color].bDirty = FALSE;
#endif

	_pCurrentCamera = NULL;
	_pCurrentRenderableObj = NULL;
	_pCurrentLightList = NULL;
	_pCurrentRenderArea = NULL;
	_pShadowMapCamera = NULL;

	//----
	__bWorldViewMatrixDirty = TRUE;
}

AutoParameterDataProvider::~AutoParameterDataProvider(void)
{

}

const DataEntry& AutoParameterDataProvider::GetAutoParameterData(const AutoParameter& autoParameter) const
{
	AutoParameterType paramType = autoParameter.autoParamType;
	DataEntry& dataEntry = _dataEntries.at(paramType);

	switch(paramType)
	{
	case APT_bone_matrix:
		_fillBoneMatrixEntry(dataEntry);
		break;

	case APT_light_direction:
		dataEntry.pDataPointer = _getLightDirection(autoParameter.iIndex);
		break;

	case APT_light_dir_view_space:
		dataEntry.pDataPointer = _getLightDirectionViewSpace(autoParameter.iIndex);
		break;

	case APT_light_position:
		dataEntry.pDataPointer = _getLightPosition(autoParameter.iIndex);
		break;

	case APT_light_color:
		dataEntry.pDataPointer = _getLightColor(autoParameter.iIndex);
		break;

	case APT_light_pos_view_space:
		dataEntry.pDataPointer = _getLightPosViewSpace(autoParameter.iIndex);
		break;

	case APT_surface_shininess:
		dataEntry.pDataPointer = (DataPointer)_pCurrentRenderableObj->GetMaterial()->GetBestTechnique()->GetShininessPointer();
		break;

	case APT_view_proj_matrix:
		dataEntry.pDataPointer = (DataPointer)&_pCurrentCamera->getViewProjTranspose();
		break;

	default:
		if(dataEntry.bDirty)
		{
			_updateDataCache(paramType);
			dataEntry.bDirty = false;
		}
	}

	return dataEntry;
}

Matrix4& AutoParameterDataProvider::_getWorldViewMatrix() const
{
	if(__bWorldViewMatrixDirty == TRUE)
	{
		__bWorldViewMatrixDirty = FALSE;
		Matrix4 worldMatrix;
		_pCurrentRenderableObj->GetWorldTransforms(&worldMatrix);
		__worldViewMatrix = worldMatrix * _pCurrentCamera->getViewMatrix();
	}
	return __worldViewMatrix;
}

void AutoParameterDataProvider::_updateDataCache(AutoParameterType paramType) const
{
	switch (paramType)
	{
	case APT_world_matrix:
		_pCurrentRenderableObj->GetWorldTransforms(&_worldMatrixTranspose);
		_worldMatrixTranspose = _worldMatrixTranspose.Transpose();
		break;

	case APT_invert_transpose_world:
		_pCurrentRenderableObj->GetWorldTransforms(&_invTranspWorldTranspose);
		_invTranspWorldTranspose = _invTranspWorldTranspose.InverseAffine();
		break;

	case APT_world_view_matrix:
		_worldViewMatrixTranspose = _getWorldViewMatrix().Transpose();
		break;

	case APT_world_view_proj_matrix:
		_worldViewProjTranspose = (_getWorldViewMatrix() * _pCurrentCamera->getProjectMatrix()).Transpose();
		break;

	case APT_invert_transpose_world_view:
		_invTranspWorldView = _getWorldViewMatrix().InverseAffine();
		break;

	case APT_invert_view_matrix:
		_invViewTranspose = _pCurrentCamera->getInverseViewMatrix().Transpose();
		break;

	case APT_light_space_wvp_matrix:
		{
			XASSERT(_pShadowMapCamera);
			_pCurrentRenderableObj->GetWorldTransforms(&_lightWVPTranspose);
			_lightWVPTranspose *= _pShadowMapCamera->getViewProjMatrix();
			_lightWVPTranspose = _lightWVPTranspose.Transpose();			
		}

		break;

	case APT_camera_position:
		_cameraPosition = _pCurrentCamera->getCameraPosition();
		break;

	case APT_ambient_light_color:
		// Ambient light is from scene, will be set at the beginning of SceneRender::RenderScene()
		XASSERT(_dataEntries[APT_ambient_light_color].bDirty == FALSE);
		break;

	case APT_viewport_size:
		{
			const ViewportInfo* pViewportInfo =  _pCurrentRenderArea->GetViewportInfo();
			_vViewportSize.x = pViewportInfo->Width;
			_vViewportSize.y = pViewportInfo->Height;
		}
		break;

	default:
		XASSERT(0);
	}

	XASSERT(_dataEntries.at(paramType).pDataPointer);
}

void AutoParameterDataProvider::_fillBoneMatrixEntry(DataEntry& dataEntry) const
{
//	XASSERT(0);
	dataEntry.pDataPointer = _pCurrentRenderableObj->GetBoneMatrixBuffer();
	dataEntry.nFloat4Count = _pCurrentRenderableObj->GetBoneCount() * 3;
}

DataPointer AutoParameterDataProvider::_getLightDirection(UINT index) const
{
	XASSERT(_pCurrentLightList);
	XASSERT(index >=0 && index < _pCurrentLightList->size());
	Light* pLight = (*_pCurrentLightList)[index];
	return (DataPointer)&pLight->GetDerivedDirection();
}

DataPointer AutoParameterDataProvider::_getLightDirectionViewSpace(UINT index) const
{
	XASSERT(_pCurrentLightList);
	XASSERT(index >=0 && index < _pCurrentLightList->size());
	Light* pLight = (*_pCurrentLightList)[index];
	return (DataPointer)&pLight->GetDerivedDirViewSpace(_pCurrentCamera);
}	

DataPointer AutoParameterDataProvider::_getLightPosition(UINT index) const
{
	XASSERT(_pCurrentLightList);
	XASSERT(index >=0 && index < _pCurrentLightList->size());
	Light* pLight = (*_pCurrentLightList)[index];
	return (DataPointer)&pLight->GetDerivedPosition();
}

DataPointer AutoParameterDataProvider::_getLightColor(UINT index) const
{
	XASSERT(_pCurrentLightList);
	XASSERT(index >=0 && index < _pCurrentLightList->size());
	Light* pLight = (*_pCurrentLightList)[index];
	return (DataPointer)&pLight->GetColor();
}

DataPointer AutoParameterDataProvider::_getLightPosViewSpace(UINT index) const
{
	XASSERT(_pCurrentLightList && _pCurrentCamera);
	XASSERT(index >=0 && index < _pCurrentLightList->size());
	Light* pLight = (*_pCurrentLightList)[index];
	return (DataPointer)&pLight->GetDerivedPosViewSpace(_pCurrentCamera);
}


_X3D_NS_END