#pragma once

_X3D_NS_BEGIN

class RenderConfigEvent :  public Event
{
public:
	enum RenderConfigEventType
	{
		RENDER_CONFIG_EVENT_BASE = 1000,
		TEX_FILTER_TYPE_CHANGED,

		RENDER_CONFIG_EVENT_END,
	};
};

// Manager the global rendering setting. Such as the shadow level, 
// texture filtering, lighting level, geometry level and so on
class RenderConfig : public EventDispatcher
{
public:
	RenderConfig(RenderSystem* pRenderSystem);
	virtual ~RenderConfig();


protected:
	TextureFilterType
		_minFilterType;
	TextureFilterType
		_magFilterType;
	TextureFilterType
		_mipFilterType;

	float
		_shadowMapSize;

private:


};


_X3D_NS_END