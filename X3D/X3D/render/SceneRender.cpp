#include "StdAfx.h"
#include "SceneRender.h"

#include "X3D/scene/Scene.h"
#include "x3d/graphic/graphicsystem.h"

#include "x3d/graphic/WindowRenderTarget.h"

#include "x3d/render/RenderSystem.h"
#include "x3d/render/RenderQueue.h"
#include "x3d/scene/SceneSpatialMgr.h"
#include "x3d/resource/TextureManager.h"

_X3D_NS_BEGIN

SceneRender::SceneRender(RenderSystem* pRenderSystem)
: RenderObject(pRenderSystem)
{
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pRenderQueue = pRenderSystem->CreateRenderQueue();

	_pCurrentRenderArea = NULL;
	_pCurrentCamera = NULL;
	_pCurrentRenderTarget = NULL;
}

SceneRender::~SceneRender(void)
{
	//...
	SAFE_DELETE(_pRenderQueue);
}

void SceneRender::_getAffectedLightList(SceneNode* pNode)
{
	XASSERT(0);
}

void SceneRender::RenderScene(RenderOperationList& renderOPList
							, RenderArea* pRenderArea, Camera* pCamera
							, RenderWindow* pRenderWindow, Scene* pScene)
{
	XASSERT(_pGraphicSystem && pRenderArea && pCamera && pRenderWindow);

	_pCurrentRenderArea = pRenderArea;
	_pCurrentCamera = pCamera;
	_pCurrentRenderTarget = pRenderWindow->getWndRenderTarget();

	RenderContext renderContext(pRenderWindow, pRenderArea, pCamera, pScene);

	TextureManager::Instance().linkBindedTextures(renderContext);

	// Update scene graph. This will update the spatial node too.
	pScene->updateSceneGraph();
	_pRenderQueue->BeginUpdate(&renderContext);

	SceneSpatialMgr* pSpatialMgr = pScene->getSpatialMgr();

	/* 
		Fill render queue with visible renderable objects. RenderOperation will use RenderQueue as a 
		renderable object source.
	 */
	pSpatialMgr->VisitVisibleSceneNodes(pCamera, _pRenderQueue);

	// Update per-frame data of _autoParamDataProvider
	_autoParamDataProvider.SetCurrentCamera(_pCurrentCamera);
	_autoParamDataProvider.SetAmbientLightColor(pScene->GetAmbientLightColor());
	_autoParamDataProvider.SetCurrentRenderArea(_pCurrentRenderArea);


	FASTEST_VECTOR_ITERATE(RenderOperation*, renderOPList);
		renderContext.pNextRenderOP = PEEK_NEXT(renderOPList);
		GET_NEXT(renderOPList)->Render(renderContext);

	FASTEST_ITERATE_END();


	_pRenderQueue->EndUpdate();
	TextureManager::Instance().unlinkBindedTextures();
}

_X3D_NS_END


/*


struct QuadVertex
{
enum FVF
{
FVF_QUADVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE //| D3DFVF_TEX1
};

float	x, y;		// screen position    
float	z;			// Z-buffer depth 0..1

Vector3 normal;
//DWORD	color;		// color of D3DRGBA(r, g, b, a)

// texture coordinates
UVCoordinate uv1;
//UVCoordinate uv2;
};

QuadVertex g_quad[3];

// For test
float length = 150;

g_quad[0].x = 0;
g_quad[0].y = 0;
g_quad[0].z = 0;
g_quad[0].uv1.u = 0;
g_quad[0].uv1.v = 0;
//g_quad[0].color = 0xffffffff;

g_quad[1].x = 0;
g_quad[1].y = length;
g_quad[1].z = 0;
g_quad[1].uv1.u = 0;
g_quad[1].uv1.v = 1;
//g_quad[1].color = 0xffffffff;

g_quad[2].x = length;
g_quad[2].y = length;
g_quad[2].z = 0;
g_quad[2].uv1.u = 1;
g_quad[2].uv1.v = 1;
//g_quad[2].color = 0xffffffff;*/
