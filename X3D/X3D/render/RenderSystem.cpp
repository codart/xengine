#include "StdAfx.h"
#include "RenderSystem.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/render/RenderOperation.h"
#include "x3d/resource/MeshManager.h"
#include "x3d/render/IntrisincRenderOP.h"
#include "x3d/render/RenderQueue.h"
#include "x3d/render/RenderGroup.h"

#include "x3d/render/LightPrePassRenderOp.h"
#include "x3d/render/PostProcessRenderOP.h"
#include "x3d/render/UnitTestRenderOP.h"
#include "x3d/render/FullScreenQuad.h"

#include "x3d/render/RenderGBufferOP.h"
#include "x3d/render/RenderShadowDepthMap.h"
#include "x3d/resource/TextureManager.h"
#include "x3d/render/IntrinsicRenderableFactory.h"

#include "x3d/render/RenderConfig.h"
#include "x3d/render/IntrinsicRenderableFactory.h"

_X3D_NS_BEGIN

RenderSystem::RenderSystem(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;
	_pFullScreenQuad = NULL;

	_pRenderConfig = XNEW RenderConfig(this);
}

RenderSystem::~RenderSystem(void)
{
	SAFE_DELETE(_pFullScreenQuad);
	SAFE_DELETE(_pRenderConfig);
}

void RenderSystem::Init()
{
	XASSERT(!_pFullScreenQuad);
	_pFullScreenQuad = XNEW FullScreenQuad(_pGraphicSystem);

	Factory* pIntrisicObjFactory = XNEW IntrinsicRenderableFactory(this);
	pIntrisicObjFactory->Init(0);
	
	RegisterRenderableObjFactory(pIntrisicObjFactory, pIntrisicObjFactory->GetSupporttedTypes());
}

void RenderSystem::Destroy()
{	
	FactorySetIt it = _renderableFactorySet.begin();
	FactorySetIt itEnd = _renderableFactorySet.end();

	for ( ; it != itEnd; ++it)
	{
		(*it)->Destroy();
	}

	_renderableFactorySet.clear();

	XDELETE this;
}

BOOL RenderSystem::RegisterRenderableObjFactory(Factory* pFactory, const RenderableTypeList& supporttedTyps)
{
	FactorySetIt it = _renderableFactorySet.find(pFactory);
	if(it == _renderableFactorySet.end())
	{
		_renderableFactorySet.insert(pFactory);
	}

	XASSERT(pFactory);
	ConstVectorIterator<DWORD> supporttedTypeIterator(supporttedTyps);

	while (supporttedTypeIterator.HasMore())
	{
		_renderableToFactoryMap.insert(make_pair(supporttedTypeIterator.CurrentData(), pFactory));
		supporttedTypeIterator.Next();
	}

	return TRUE;
}

RendableObject* RenderSystem::CreateRendableObject(DWORD dwType)
{
	FactoryMapIt it = _renderableToFactoryMap.find(dwType);
	if(it == _renderableToFactoryMap.end()) return NULL;

	Factory* pFactory = it->second;
	Object* pObject = pFactory->CreateObject(dwType);
	RendableObject* pRenderableObj = SAFE_CAST(pObject, RendableObject*);
	XASSERT(pRenderableObj);
	return pRenderableObj;
}

RenderQueue* RenderSystem::CreateRenderQueue()
{
	return XNEW RenderQueue(this);
}

RenderGroup* RenderSystem::CreateRenderGroup(DWORD dwGroupID)
{
	return XNEW RenderGroup(this, dwGroupID);
}

TexturePtr RenderSystem::GetSharedShadowDepthMap(int iIndex)
{
	XASSERT(iIndex >= 0);
	if(iIndex >= (int)_sharedShadowDepthMaps.size())
	{
		_sharedShadowDepthMaps.resize(iIndex + 1);
	}

	if(!_sharedShadowDepthMaps[iIndex])
	{
		StringStream ss;
		ss << L"SharedShadowMap" << iIndex;
		_sharedShadowDepthMaps[iIndex] = TextureManager::Instance().AddRenderTexture(ss.str()
													, 256, 256/*, PIXEL_FORMAT_FLOAT32_R*/);
	}

	return _sharedShadowDepthMaps[iIndex];
}

HUDRenderOperation* RenderSystem::CreateHUDRenderOperation(DWORD dwHUDRenderOPType)
{
	switch (dwHUDRenderOPType)
	{
	case HUD_ROP_DEFAULT:
		return XNEW HUDRenderOperation(this);
		break;

	default:
		XASSERT(0);
	}

	return NULL;
}

RenderOperation* RenderSystem::CreateRenderOperation(DWORD dwRenderOPType)
{
	switch (dwRenderOPType)
	{
	case ROP_DEFAULT:
		return XNEW RenderOperation(this);
		break;

	case ROP_PRE_POSTPROCESS:
		return XNEW PrePostProcessOP(this);
		break;

	case ROP_POSTPROCESS:
		return XNEW PostProcessOP(this);
		break;

	case ROP_PRE_Z_PASS:
		return XNEW LightPrePassRenderOP(this);
		break;

	case ROP_RENDER_GBUFFER:
		return XNEW RenderGBufferOP(this);
		break;

	case ROP_RENDER_WITH_GBUFFER:
		return XNEW RenderWithGBufferOP(this);
		break;

	case ROP_RENDER_SHADOW_MAP:
		return XNEW RenderShadowDepthMap(this);
		break;

	case ROP_UNIT_TEST:
		return XNEW UnitTestRenderOP(this);
		break;

	default:
		XASSERT(0);
	}

	return NULL;
}

_X3D_NS_END