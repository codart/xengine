#pragma once

#include "x3d/resource/RenderTexture.h"
#include "common/core/Factory.h"
#include "x3d/scene/PrefabRenderable.h"
#include <set>

_X3D_NS_BEGIN

class RenderOperation;
class GraphicSystem;
class RenderGroup;
class RenderQueue;
class FullScreenQuad;
class RenderConfig;

class _X3DExport RenderSystem
{
public:
	RenderSystem(GraphicSystem* pGraphicSystem);
	virtual ~RenderSystem(void);


public:
	virtual void
		Init();
	virtual void
		Destroy();

	virtual RenderOperation*
		CreateRenderOperation(DWORD dwRenderOPType);
	virtual HUDRenderOperation*
		CreateHUDRenderOperation(DWORD dwHUDRenderOPType);
	virtual RenderQueue*
		CreateRenderQueue();
	virtual RenderGroup*
		CreateRenderGroup(DWORD dwGroupID);

	virtual RendableObject*
		CreateRendableObject(DWORD dwType);
	virtual TexturePtr
		GetSharedShadowDepthMap(int iIndex);

	virtual BOOL
		RegisterRenderableObjFactory(Factory* pFactory, const RenderableTypeList& supporttedTyps);

	GraphicSystem*
		GetGraphicSystem(){ return _pGraphicSystem; };
	INLINE FullScreenQuad*
		GetFullScreenQuad() const { XASSERT(_pFullScreenQuad); return _pFullScreenQuad; }


protected:
	GraphicSystem*
		_pGraphicSystem;
	RenderConfig*
		_pRenderConfig;

	TexturePtrList
		_sharedShadowDepthMaps;
		
	// --- Object factory ---
	FactoryMap
		_renderableToFactoryMap;

	typedef std::set<Factory*> FactorySet;
	typedef FactorySet::iterator FactorySetIt;

	FactorySet
		_renderableFactorySet;

	FullScreenQuad*
		_pFullScreenQuad;

};

_X3D_NS_END