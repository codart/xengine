#pragma once

#include "x3d/render/RenderOperation.h"
#include "x3d/resource/RenderTexture.h"

_X3D_NS_BEGIN

class UnitTestRenderOP : public RenderOperation
{
public:
	UnitTestRenderOP(RenderSystem* pRenderSystem);
	virtual ~UnitTestRenderOP(void);

public:
	virtual void 
		Render(RenderContext& renderContext);


protected:
	MaterialPtr
		_pRenderGBufferMtl;

	MaterialPtr
		_pLPPDiffuseMtl;

};


_X3D_NS_END