#pragma once

#include "AutoParameter.h"
#include "Common/general/Color.h"
#include "x3d/scene/Light.h"
#include "x3d/scene/RendableObject.h"
#include "x3d/scene/Camera.h"
#include "x3d/RenderArea.h"

_X3D_NS_BEGIN

class DataEntry 
{
public:
	DataPointer		pDataPointer;
	UINT			nFloat4Count;
	BOOL			bDirty;

	DataEntry(DataPointer dataSlot, UINT nFloat4Count, BOOL bDirty)
	{
		this->pDataPointer = dataSlot;
		this->nFloat4Count = nFloat4Count;
		this->bDirty = bDirty;
	}
};


class _X3DExport AutoParameterDataProvider : public Final<AutoParameterDataProvider>
{
public:
	AutoParameterDataProvider(void);
	~AutoParameterDataProvider(void);


public:
	INLINE const DataEntry& 
		GetAutoParameterData(const AutoParameter& autoParameter) const;

	// --- Global parameters, this parameters will be update always before rendering scene ---
	INLINE void 
		SetAmbientLightColor(const Color& ambimentLightColor)
	
	{
#ifdef _DEBUG
		_dataEntries[APT_ambient_light_color].bDirty = FALSE;
#endif
		_ambientLightColor = ambimentLightColor; 
	};

	INLINE void SetCurrentCamera(const Camera* pCamera)
	{
		_pCurrentCamera = pCamera;

		_dataEntries[APT_world_view_proj_matrix].bDirty = TRUE;
		_dataEntries[APT_camera_position].bDirty = TRUE;
		_dataEntries[APT_invert_transpose_world_view].bDirty = TRUE;
		_dataEntries[APT_invert_view_matrix].bDirty = TRUE;
		_dataEntries[APT_view_proj_matrix].bDirty = TRUE;

		__bWorldViewMatrixDirty = TRUE;
	}

	INLINE void SetShadowMapCamera(const Camera* pCamera)
	{
		_pShadowMapCamera = pCamera;
		_dataEntries[APT_light_space_wvp_matrix].bDirty = TRUE;
	}

	INLINE void SetCurrentRenderableObject(const RendableObject* pRenderableObj)
	{
		_pCurrentRenderableObj = pRenderableObj;

		_dataEntries[APT_world_matrix].bDirty = TRUE;
		_dataEntries[APT_invert_transpose_world].bDirty = TRUE;
		_dataEntries[APT_world_view_proj_matrix].bDirty = TRUE;
		_dataEntries[APT_light_space_wvp_matrix].bDirty = TRUE;
		_dataEntries[APT_world_view_matrix].bDirty = TRUE;
		_dataEntries[APT_invert_transpose_world_view].bDirty = TRUE;

		__bWorldViewMatrixDirty = TRUE;
	}

	INLINE void SetCurrentLightList(const LightList* pLightList)
	{
		_pCurrentLightList = pLightList; 
	}

	INLINE void SetCurrentRenderArea(const RenderArea* pRenderArea)
	{
		_pCurrentRenderArea = pRenderArea;
		_dataEntries[APT_viewport_size].bDirty = TRUE;
	}


protected:
	DECLARE_LIST_TYPE(vector, DataEntry);

	mutable DataEntryList
		_dataEntries;

	/* Cached parameters

		Note: for data which cannot be fetched directly(or not cached by any other object), we need cache them 
		to reduce unnecessary calculation. For example, the world-view-proj matrix need to be cached and the light
		direction no need cached(as it already cached by light object).
	*/
	mutable Matrix4 
		_worldMatrixTranspose;
	mutable Matrix4 
		_invTranspWorldTranspose;

	mutable Matrix4 
		_worldViewProjTranspose;
	mutable Matrix4 
		_invTranspWorldView;

	mutable Matrix4
		_invViewTranspose;

	// World view projection matrix for the current light (used for render shadow depth map)
	mutable Matrix4 
		_lightWVPTranspose;

	mutable Matrix4 
		_worldViewMatrixTranspose;
	mutable Matrix4
		__worldViewMatrix;
	mutable BOOL
		__bWorldViewMatrixDirty;

	mutable Vector3 
		_cameraPosition;

	mutable Color 
		_ambientLightColor;
	mutable Vector2
		_vViewportSize;


	// The actually data providers
	const RenderArea*
		_pCurrentRenderArea;
	const Camera*
		_pCurrentCamera;

	// The current light's camera for rendering shadow map
	const Camera*
		_pShadowMapCamera;

	const RendableObject*
		_pCurrentRenderableObj;

	const LightList*
		_pCurrentLightList;


protected:
	INLINE Matrix4& 
		_getWorldViewMatrix() const;

	INLINE void 
		_updateDataCache(AutoParameterType paramType) const;

	INLINE void
		_fillBoneMatrixEntry(DataEntry& dataEntry) const;

	INLINE DataPointer 
		_getLightDirection(UINT index) const;

	INLINE DataPointer 
		_getLightDirectionViewSpace(UINT index) const;	
	
	INLINE DataPointer 
		_getLightPosition(UINT index) const;

	INLINE DataPointer 
		_getLightColor(UINT index) const;

	INLINE DataPointer 
		_getLightPosViewSpace(UINT index) const;

};

_X3D_NS_END