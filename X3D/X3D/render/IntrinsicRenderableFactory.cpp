#include "stdafx.h"
#include "IntrinsicRenderableFactory.h"
#include "IntrinsicRenderableTypes.h"

#include "x3d/scene/SubMeshRenderable.h"
#include "x3d/hud/RenderableQuad.h"
#include "x3d/lighting/PointLightVolume.h"
#include "x3d/scene/PrefabRenderable.h"

_X3D_NS_BEGIN

IntrinsicRenderableFactory::IntrinsicRenderableFactory(RenderSystem* pRenderSystem)
: RenderObject(pRenderSystem)
{

}

IntrinsicRenderableFactory::~IntrinsicRenderableFactory()
{

}

void IntrinsicRenderableFactory::Init(DWORD dwInstallData)
{
	_supporttedTypes.push_back(SUB_MESH_ENTITY);
	_supporttedTypes.push_back(HUD_QUAD);
	_supporttedTypes.push_back(POINT_LIGHT_VOLUME);
	_supporttedTypes.push_back(PREFAB_RENDABLE);
}

void IntrinsicRenderableFactory::Destroy()
{
	XDELETE this;
}

Object* IntrinsicRenderableFactory::CreateObject(DWORD dwCreationData)
{
	IntrinsicObjectType type = (IntrinsicObjectType)dwCreationData;
	RenderObject* pRenderableObj = NULL;

	switch (type)
	{
	case SUB_MESH_ENTITY:
		pRenderableObj = XNEW SubMeshRendable(_pRenderSystem);
		break;

	case HUD_QUAD:
		pRenderableObj = XNEW RenderableQuad(_pRenderSystem);
		break;

	case POINT_LIGHT_VOLUME:
		pRenderableObj = XNEW PointLightVolume(_pRenderSystem);
		break;

	case PREFAB_RENDABLE:
		pRenderableObj = XNEW PrefabRenderable(_pRenderSystem);
		break;

	default:
		XASSERT(0);
	}

	return pRenderableObj;
}


_X3D_NS_END