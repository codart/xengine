#pragma once

#include "x3d/resource/RenderTexture.h"
#include "x3d/material/Material.h"
#include "x3d/render/RenderOperation.h"

_X3D_NS_BEGIN

class SceneRender;

/*
	A rendering with Post-Process support should has RenderOP like below, 
	1. PrePostProcessOP
	2. PostProcessOP-1
	3. PostProcessOP-2
	.....
	n. PostProcessOP-n

*/
class PrePostProcessOP : public RenderOperation
{
public:
	PrePostProcessOP(RenderSystem* pRenderSystem);
	virtual ~PrePostProcessOP();

public:
	virtual void 
		Render(RenderContext& renderContext);


protected:
	TexturePtr
		_pRenderTexture;

};

class _X3DExport PostProcessOP : public RenderOperation
{
public:
	PostProcessOP(RenderSystem* pRenderSystem);
	virtual ~PostProcessOP();


public:
	virtual void 
		Render(RenderContext& renderContext);
	Technique*
		GetTechnique(){ return _pQuadMaterial->GetBestTechnique(); }


protected:
	MaterialPtr
		_pQuadMaterial;

	//  Shared render texture for all post process OP.
	TexturePtr
		_pPPSharedRT;

};


_X3D_NS_END