#pragma once

_X3D_NS_BEGIN

class _X3DExport RenderShadowDepthMap : public RenderOperation
{
public:
	RenderShadowDepthMap(RenderSystem* pRenderSystem);
	virtual ~RenderShadowDepthMap();


public:
	virtual void 
		Render(RenderContext& renderContext);


protected:
	TexturePtr
		_pShadowDepthMap;
	MaterialPtr
		_pRenderShadowMapMtl;


protected:
	void 
		_renderSceneShadowDepthMap();


};


_X3D_NS_END