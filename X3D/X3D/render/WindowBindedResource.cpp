#include "stdafx.h"
#include "WindowBindedResource.h"

#include "Common/plantform/WindowEvent.h"
#include "x3d/resource/TextureManager.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

WindowBindedResource::WindowBindedResource(RenderSystem* pRenderSystem, RenderWindow* pBindedWindow)
: RenderObject(pRenderSystem)
{
	_pRenderWindow = pBindedWindow;

	XASSERT(_pRenderWindow);

	_pRenderWindow->GetWindow()->AddListener(WindowEvent::WINDOW_CREATE, _onWindowCreate);
	_pRenderWindow->GetWindow()->AddListener(WindowEvent::WINDOW_DESTROY, _onWindowDestroy);
	_pRenderWindow->GetWindow()->AddListener(WindowEvent::WINDOW_SIZE, _onWindowSized);
}

WindowBindedResource::~WindowBindedResource()
{
	XASSERT(_pRenderWindow);
	_pRenderWindow->GetWindow()->RemoveListener(WindowEvent::WINDOW_CREATE, _onWindowCreate);
	_pRenderWindow->GetWindow()->RemoveListener(WindowEvent::WINDOW_DESTROY, _onWindowDestroy);
	_pRenderWindow->GetWindow()->RemoveListener(WindowEvent::WINDOW_SIZE, _onWindowSized);
}

void WindowBindedResource::_onWindowCreate(const Event* pEvent)
{
	_createWindowBuffers();
}

void WindowBindedResource::_onWindowDestroy(const Event* pEvent)
{
	_pGBuffer = NULL;
	_pLightBuffer = NULL;
}

void WindowBindedResource::_onWindowSized(const Event* pEvent)
{
	_createWindowBuffers();
}

void WindowBindedResource::_createWindowBuffers()
{
	GraphicSystem* pGraphicSystem = _pRenderSystem->GetGraphicSystem();
	Window* pWindow = _pRenderWindow->GetWindow();
	XASSERT(pWindow);

	int nAdjustedWidth = (0 == pWindow->GetWidth())? 1:pWindow->GetWidth();
	int nAdjustedHeight =  (0 == pWindow->GetHeight())? 1:pWindow->GetHeight();

	if(_pGBuffer)
	{
		XASSERT(_pLightBuffer);

		_pGBuffer->Resize(nAdjustedWidth, nAdjustedHeight);
		_pLightBuffer->Resize(nAdjustedWidth, nAdjustedHeight);
	}
	else
	{
		StringStream ss;

		ss <<_pRenderWindow->GetName() << "-GBuffer";

		_pGBuffer = pGraphicSystem->CreateRenderTexture(ss.str(), nAdjustedWidth, nAdjustedHeight
														, PIXEL_FORMAT_A16B16G16R16F, PIXEL_FORMAT_D24S8);

		ss.str(TEXT(""));
		ss <<_pRenderWindow->GetName() << "-LightBuffer";
		_pLightBuffer = pGraphicSystem->CreateRenderTexture(ss.str(), nAdjustedWidth, nAdjustedHeight
														, PIXEL_FORMAT_A16B16G16R16F, PIXEL_FORMAT_D24S8);
	}
}


_X3D_NS_END