#pragma once

_X3D_NS_BEGIN

class RenderTarget;
class SceneRender;
class RenderOperation;
class RenderArea;
class RenderWindow;
class Camera;

class RenderContext
{
public:
	RenderContext(RenderWindow* pRenderWindow, RenderArea* pRenderArea
				, Camera* pCamera, Scene* pScene)
	{
		this->pScene = pScene;
		this->pRenderWindow = pRenderWindow;
		this->pCamera = pCamera;
		this->pDestRenderArea = pRenderArea;
		this->pLastRenderOP = NULL;
		this->pNextRenderOP = NULL;
	}


public:
	transient AutoParameterDataProvider& 
		GetAutoParamDataProvider();
	transient RenderWindow*
		GetRenderWindow();
	SceneRender*
		GetSceneRender();
	RenderTarget*
		GetWndRenderTarget();


public:
	Scene*
		pScene;
	Camera*
		pCamera;
	RenderArea*
		pDestRenderArea;

	RenderWindow*
		pRenderWindow;

	// ---
	mutable RenderOperation*
		pLastRenderOP;
	mutable RenderOperation*
		pNextRenderOP;

};

_X3D_NS_END