#pragma once

#include "x3d/render/RenderOperation.h"
#include "x3d/material/Material.h"

_X3D_NS_BEGIN

class MultiRenderTarget;

class RenderGBufferOP : public RenderOperation
{
public:
	RenderGBufferOP(RenderSystem* pRenderSystem);
	~RenderGBufferOP(void);


public:
	virtual void 
		Render(RenderContext& renderContext);


protected:
	MultiRenderTarget*
		_pMultiRenderTarget;

	RenderTexture*
		_pSharedNormalBuffer;
	RenderTexture*
		_pSharedPositionBuffer;
	RenderTexture*
		_pSharedDiffuseAndZBuffer;

	MaterialPtr
		_pRenderGBufferMtl;

};

class RenderWithGBufferOP : public RenderOperation
{
public:
	RenderWithGBufferOP(RenderSystem* pRenderSystem);
	~RenderWithGBufferOP(void);


public:
	virtual void 
		Render(RenderContext& renderContext);


protected:
	MaterialPtr
		_pRenderQuadByGBufferMtl;

};


_X3D_NS_END