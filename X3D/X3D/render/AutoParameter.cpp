#include "StdAfx.h"
#include "AutoParameter.h"

_X3D_NS_BEGIN

#define ADD_AUTO_PARAM_SEMANTIC(semantic)	\
{ #semantic, APT##_##semantic }

AutoParameter::AutoParamSemantic AutoParameter::s_autoParamSemantics[] = 
{
	// Bone matrix
	ADD_AUTO_PARAM_SEMANTIC(bone_matrix),

	ADD_AUTO_PARAM_SEMANTIC(world_matrix),
	ADD_AUTO_PARAM_SEMANTIC(invert_transpose_world),
	ADD_AUTO_PARAM_SEMANTIC(world_view_proj_matrix),
	ADD_AUTO_PARAM_SEMANTIC(world_view_matrix),
	ADD_AUTO_PARAM_SEMANTIC(view_proj_matrix),
	ADD_AUTO_PARAM_SEMANTIC(invert_transpose_world_view),
	ADD_AUTO_PARAM_SEMANTIC(invert_view_matrix),

	ADD_AUTO_PARAM_SEMANTIC(camera_position),

	ADD_AUTO_PARAM_SEMANTIC(light_space_wvp_matrix),	
	ADD_AUTO_PARAM_SEMANTIC(light_direction),
	ADD_AUTO_PARAM_SEMANTIC(light_dir_view_space),
	ADD_AUTO_PARAM_SEMANTIC(light_position),
	ADD_AUTO_PARAM_SEMANTIC(light_color),
	ADD_AUTO_PARAM_SEMANTIC(light_pos_view_space),
	ADD_AUTO_PARAM_SEMANTIC(ambient_light_color),

	//--- Material related
	ADD_AUTO_PARAM_SEMANTIC(surface_shininess),


	ADD_AUTO_PARAM_SEMANTIC(viewport_size),

	//batch params
	ADD_AUTO_PARAM_SEMANTIC(batch_wvp_w_v),
};

AutoParameterTypeMap AutoParameter::s_autoParamMap;

AutoParameterType AutoParameter::AutoParamTypeFromSemantic(const AString& strSemantic)
{
	AutoParameterTypeMapIt it = s_autoParamMap.find(strSemantic);
	XASSERT(it != s_autoParamMap.end());

	return it->second;
}

void AutoParameter::_initAutoParamMap()
{
	for ( int i=0; i<APT_COUNT; ++i)
	{
		s_autoParamMap.insert(make_pair(s_autoParamSemantics[i].pszSemantic, s_autoParamSemantics[i].type));
	}
}

void AutoParameter::_uninitAutoParamMap()
{
	s_autoParamMap.clear();
}

_X3D_NS_END