#pragma once

#include "x3d/render/RenderObject.h"
#include "x3d/render/RenderGroup.h"
#include "x3d/scene/SceneSpatialMgr.h"

_X3D_NS_BEGIN

class LightGroup : public RenderObject
{
public:
	LightGroup(RenderSystem* pRenderSystem)
		: RenderObject(pRenderSystem)
	{
		_vLightList.reserve(10);
	}

	~LightGroup()
	{
		Clear();
	}


public:
	INLINE void 
		AddLight(Light* pLight) {_vLightList.push_back(pLight);}

	INLINE void
		AddLights(const LightList& lightList)
	{
		_vLightList.insert(_vLightList.end(), lightList.begin(), lightList.end());
	}

	INLINE const LightList& 
		GetLightList() const { return _vLightList; }

	INLINE void
		Clear(){ _vLightList.clear(); }


protected:
	LightList
		_vLightList;

};

class _X3DExport RenderQueue : public RenderObject
                             , implements ISpatialVisitor
{
public:
	RenderQueue(RenderSystem* pRenderSystem);
	~RenderQueue(void);

public:
	virtual void 
		BeginUpdate(RenderContext* pCurRenderContex);
	virtual void
		EndUpdate();
	void
		AddToOpaqueGroup(RendableObject* pRenderableObj);
	void
		AddToTransparentGroup(RendableObject* pRenderableObj);
	void 
		AddToLightGroup(Light* pLight);

	INLINE RenderGroup*
		GetOpaqueGroup() { return _pOpaqueGroup; }
	INLINE RenderGroup*
		GetTransparentGroup() { return _pTransparentGroup; }
	INLINE LightGroup*
		GetLightGroup() { return _pLightGroup; }

	RenderGroup*
		AddRenderGroup(DWORD dwGroupID);
	RenderGroup* 
		GetRenderGroup(DWORD dwGroupID);

	//---
	void 
		clearRenderQueue();


protected:
	// implement ISpatialVisitor
	virtual void 
		VisitBegin();
	virtual void 
		ProcessNodes(const SceneNodeList& sceneNodes, BOOL bFullVisible);
	virtual void
		ProcessNode(const SceneNode* pSceneNode);
	virtual void 
		VisitEnd();


protected:
	RenderGroupMap
		_renderGroupMap;

	//-- Store some render group for fast access ---
	RenderGroup*
		_pOpaqueGroup;
	RenderGroup*
		_pTransparentGroup;
	
	// All visible light objects will be filled into this group
	LightGroup*
		_pLightGroup;


private:
	// The render context for the current render process(such as render a scene)
	mutable RenderContext*
		__pCurRenderContext;

};


_X3D_NS_END