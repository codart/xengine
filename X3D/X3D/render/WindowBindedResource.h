#pragma once

#include "RenderObject.h"
#include "x3d/resource/RenderTexture.h"

_X3D_NS_BEGIN

class RenderWindow;


class WindowBindedResource : public RenderObject
{
	DECLARE_EVENT_RECEIVER(WindowBindedResource);

public:
	WindowBindedResource(RenderSystem* pRenderSystem, RenderWindow* pBindedWindow);
	virtual ~WindowBindedResource();


public:
	INLINE RenderTexturePtr&
		GetGBuffer() { return _pGBuffer.GetSmartPtr(); } /// Note: RenderTexturePtr is ConcretePtr, we need spatial specialization of OwnerPtr for this type!
	INLINE RenderTexturePtr&
		GetLightBuffer() { return _pLightBuffer; }


protected:
	RenderWindow*
		_pRenderWindow;

	OwnerPtr<RenderTexturePtr>
		_pGBuffer;

	OwnerPtr<RenderTexturePtr>
		_pTestTexPtr;

	RenderTexturePtr
		_pLightBuffer;


protected:
	void 
		_onWindowCreate(const Event* pEvent);
	void 
		_onWindowDestroy(const Event* pEvent);

	void 
		_onWindowSized(const Event* pEvent);
	void 
		_createWindowBuffers();

};


_X3D_NS_END