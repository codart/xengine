#pragma once

#include "GPUProgramConstant.h"

_X3D_NS_BEGIN

typedef void* DataPointer;

#define ADD_AUTO_PARAM_TYPE(semantic) APT##_##semantic

enum AutoParameterType
{
	APT_INVALID = -1,

	// Bone matrix
	ADD_AUTO_PARAM_TYPE(bone_matrix),

	ADD_AUTO_PARAM_TYPE(world_matrix),
	ADD_AUTO_PARAM_TYPE(invert_transpose_world),
	ADD_AUTO_PARAM_TYPE(world_view_proj_matrix),
	ADD_AUTO_PARAM_TYPE(world_view_matrix),
	ADD_AUTO_PARAM_TYPE(view_proj_matrix),
	ADD_AUTO_PARAM_TYPE(invert_transpose_world_view),
	ADD_AUTO_PARAM_TYPE(invert_view_matrix),

	ADD_AUTO_PARAM_TYPE(camera_position),

	// World view projection matrix in the spot light's space
	ADD_AUTO_PARAM_TYPE(light_space_wvp_matrix),	
	ADD_AUTO_PARAM_TYPE(light_direction),
	ADD_AUTO_PARAM_TYPE(light_dir_view_space),
	ADD_AUTO_PARAM_TYPE(light_position),
	ADD_AUTO_PARAM_TYPE(light_color),

	/* 
	With xyz the light pos, and w the light radius.

	Note: the light position int view space is dependent on current camera. So make sure the 
	current camera is correctly set with AutoParameterDataProvider::SetCurrentCamera!
	*/
	ADD_AUTO_PARAM_TYPE(light_pos_view_space),

	ADD_AUTO_PARAM_TYPE(ambient_light_color),

	//--- Material related
	ADD_AUTO_PARAM_TYPE(surface_shininess),

	ADD_AUTO_PARAM_TYPE(viewport_size),

	// batch params
	ADD_AUTO_PARAM_TYPE(batch_wvp_w_v),
	//ADD_AUTO_PARAM_TYPE(batch_wvp_xxxx),

	APT_COUNT,

};

DECLARE_MAP_TYPE(map, AString, AutoParameterType);


struct AutoParameter 
{
	friend class GPUProgramManager;

public:
	AutoParameterType 
		autoParamType;

	GPUProgramConstant* 
		pGPUProgramConstant;

	union
	{
	/* Zero based index.

	Note: Some param is not a single param but a list of params, such as light related params. So we need a index 
		to identify which item to be used. For example, to use light3's direction, this index should be 2. 
		*/
	int	
		iIndex;
	};


public:
	static AutoParameterType 
		AutoParamTypeFromSemantic(const AString& strSemantic);


protected:
	struct AutoParamSemantic
	{
		char* pszSemantic;
		AutoParameterType type;
	};

	static AutoParamSemantic
		s_autoParamSemantics[];
	static AutoParameterTypeMap
		s_autoParamMap;


protected:
	static void 
		_initAutoParamMap();
	static void
		_uninitAutoParamMap();

};

DECLARE_LIST_TYPE(vector, AutoParameter);

struct ManualParameter
{

};

DECLARE_LIST_TYPE(vector, ManualParameter);

_X3D_NS_END