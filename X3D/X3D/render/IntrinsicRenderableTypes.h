#pragma once

enum IntrinsicObjectType
{
	SUB_MESH_ENTITY = 0x01000001,
	HUD_QUAD,
	POINT_LIGHT_VOLUME,
	PREFAB_RENDABLE,
};
