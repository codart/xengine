#pragma once

_X3D_NS_BEGIN

// Default render operation types
enum DefaultRenderOPType
{
	ROP_DEFAULT,
	ROP_PRE_POSTPROCESS,
	ROP_POSTPROCESS,
	ROP_PRE_Z_PASS,

	ROP_RENDER_SHADOW_MAP,

	// Deferred shading OPs.
	ROP_RENDER_GBUFFER,
	ROP_RENDER_WITH_GBUFFER,

	ROP_UNIT_TEST,
};

enum DefaultHUDRenderOPType
{
	HUD_ROP_DEFAULT,
};

_X3D_NS_END