#pragma once

#include "x3d/datadef/VertexDef.h"

_X3D_NS_BEGIN

class GraphicSystem;
class DrawPrimitiveCommand;
class IndexData;
class VertexData;

/*
	Utility structure used to store data for render a full screen quad (post effect rendering). 
*/
class FullScreenQuad : public Final<FullScreenQuad>
{
public:
	FullScreenQuad(GraphicSystem* pGraphicSystem);
	~FullScreenQuad(void);

public:
	INLINE DrawPrimitiveCommand* 
		GetRenderOperation(){ return _pDrawQuadPrimitiveCmd; }


protected:
	// The cached quad used to do post effect rendering
	DrawPrimitiveCommand*
		_pDrawQuadPrimitiveCmd;

	IndexData*
		_pQuadIndexData;
	VertexData*
		_pQuadVertexData;
	
};

_X3D_NS_END