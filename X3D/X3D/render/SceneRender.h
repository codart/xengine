#pragma once

#include "x3d/scene/Light.h"
#include "x3d/render/AutoParameterDataProvider.h"
#include "x3d/render/RenderOperation.h"
#include "x3d/render/RenderObject.h"

_X3D_NS_BEGIN


class Viewport;
class Camera;
class RenderTarget;
class Scene;
class Material;
class Pass;
class SceneNode;
class PostProcessOP;
class FullScreenQuad;
class RenderQueue;

/*
	Responsible for the rendering of a scene. 
	A scene can have multi scene render, each scene render to render a scene with specified camera/viewport.

*/
class SceneRender : public RenderObject
{
public:
	SceneRender(RenderSystem* pRenderSystem);
	virtual ~SceneRender(void);


public:
	INLINE AutoParameterDataProvider&
		GetAutoParamProvider() { return _autoParamDataProvider; }

	virtual void 
		RenderScene(RenderOperationList& renderOPList, RenderArea* pRenderArea
					, Camera* pCamera, RenderWindow* pRenderWindow, Scene* pScene);

	virtual void 
		OnMovableObjectAdded(MovableObject* pMovableObject) { XASSERT(0); };
	virtual void 
		OnMovableObjectRemoved(MovableObject* pMovableObject) { XASSERT(0); };

	final INLINE RenderQueue*
		GetRenderQueue() { return _pRenderQueue; }


protected:
	final void
		_getAffectedLightList(SceneNode* pNode);

	// When a scene is finalized, all the objects will be stored into linear space(vector) to 
	// optimize iterator performance. 
	final INLINE void
		_finalizeScene();


protected:
	RenderQueue*
		_pRenderQueue;

	// ---
	GraphicSystem* 
		_pGraphicSystem;

	// ---
	AutoParameterDataProvider
		_autoParamDataProvider;

	// --- 
	mutable RenderArea*
		_pCurrentRenderArea;
	mutable Camera*
		_pCurrentCamera;
	mutable RenderTarget*
		_pCurrentRenderTarget;

};


_X3D_NS_END