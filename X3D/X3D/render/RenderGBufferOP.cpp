#include "StdAfx.h"
#include "RenderGBufferOP.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/material/MaterialManager.h"
#include "x3d/render/FullScreenQuad.h"

#include "x3d/graphic/MultiRenderTarget.h"

#include "x3d/resource/RenderTexture.h"
#include "x3d/render/RenderQueue.h"
#include "x3d/render/SceneRender.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

RenderGBufferOP::RenderGBufferOP(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	_pSharedNormalBuffer = TextureManager::Instance().getRenderTexture(TEXT("SharedNormalBuffer"));
	_pSharedPositionBuffer = TextureManager::Instance().getRenderTexture(TEXT("SharedPositionBuffer"));
	_pSharedDiffuseAndZBuffer = TextureManager::Instance().getRenderTexture(TEXT("SharedDiffuseAndZBuffer"));

	_pMultiRenderTarget = _pGraphicSystem->CreateMultiRenderTarget();
	_pMultiRenderTarget->BindDepthStencilBuffer(_pGraphicSystem->GetSharedDSBuffer(512, 512));
	_pMultiRenderTarget->BindColorBuffer(_pSharedPositionBuffer->GetColorBuffer());
	_pMultiRenderTarget->BindColorBuffer(_pSharedNormalBuffer->GetColorBuffer());
	_pMultiRenderTarget->BindColorBuffer(_pSharedDiffuseAndZBuffer->GetColorBuffer());

	_pRenderGBufferMtl = MaterialManager::Instance().AddMaterial(TEXT("RenderGBuffer"));
}

RenderGBufferOP::~RenderGBufferOP(void)
{
	SAFE_DELETE(_pMultiRenderTarget);
}

void RenderGBufferOP::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	_pGraphicSystem->SetMultiRenderTarget(_pMultiRenderTarget);
	_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_ALL, COLOR_ARGB(0, 0, 0, 0));

	_pGraphicSystem->SetDepthBufferParams(TRUE, TRUE, COMPARE_FUNC_LESS_EQUAL);

	// Start rendering
	RenderQueue* pRenderQueue = renderContext.GetSceneRender()->GetRenderQueue();
	RenderGroup* pOpaqueGroup = pRenderQueue->GetRenderGroup(RENDER_GROUP_OPAQUE);

	const RendableObjectList& renderableObjList = pOpaqueGroup->GetRendableObjectList();

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);

		RendableObject* pRenderableObject = GET_NEXT(renderableObjList);

		renderContext.GetAutoParamDataProvider().SetCurrentRenderableObject(pRenderableObject);

		Technique* pTechnique = _pRenderGBufferMtl->GetBestTechnique();

		PassList& passes = pTechnique->GetPasses();
		FASTEST_VECTOR_ITERATE(Pass*, passes);
			_setPass(GET_NEXT(passes));
			_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());
		FASTEST_ITERATE_END();

	FASTEST_ITERATE_END();

}

//////////////////////////////////////////////////////////////////////////
RenderWithGBufferOP::RenderWithGBufferOP(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	_pRenderQuadByGBufferMtl = MaterialManager::Instance().AddMaterial(TEXT("RenderQuadWithGBuffer"));
}

RenderWithGBufferOP::~RenderWithGBufferOP(void)
{
}

void RenderWithGBufferOP::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	_pGraphicSystem->SetRenderTarget(renderContext.GetWndRenderTarget());
	_pGraphicSystem->SetViewport(renderContext.pDestRenderArea->GetViewportInfo());

	// Switch off depth Write
	_pGraphicSystem->SetDepthBufferParams(FALSE, FALSE);

	// Render a quad
	Technique* pTechnique = _pRenderQuadByGBufferMtl->GetBestTechnique();

	LightList tempLightList;
	Light _defaultLight1(TEXT("light1"), _pRenderSystem);
	Light _defaultLight2(TEXT("light1"), _pRenderSystem);
	//..._defaultLight2.SetDirection(Vector3(0.5f, 0.7f, 0.6f));
	SASSERT(0);

	tempLightList.push_back(&_defaultLight1);
	tempLightList.push_back(&_defaultLight2);
	renderContext.GetAutoParamDataProvider().SetCurrentLightList(&tempLightList);

	PassList& passes = pTechnique->GetPasses();
	FASTEST_VECTOR_ITERATE(Pass*, passes);
		_setPass(GET_NEXT(passes));
		_pGraphicSystem->DrawPrimitive(_pRenderSystem->GetFullScreenQuad()->GetRenderOperation());
	FASTEST_ITERATE_END()
}

_X3D_NS_END