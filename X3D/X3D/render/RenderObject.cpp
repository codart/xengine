#include "StdAfx.h"
#include "RenderObject.h"

#include "x3d/render/RenderSystem.h"

_X3D_NS_BEGIN

RenderObject::RenderObject(const RenderObject& renderObj)
{
	_pRenderSystem = renderObj._pRenderSystem;
}

RenderObject::RenderObject(RenderSystem* pRenderSystem)
{
	_pRenderSystem = pRenderSystem;
}

RenderObject::~RenderObject(void)
{

}

GraphicSystem* RenderObject::GetGraphicSystem()
{
	return _pRenderSystem->GetGraphicSystem();
}


_X3D_NS_END