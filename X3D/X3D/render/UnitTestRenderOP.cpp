#include "StdAfx.h"
#include "UnitTestRenderOP.h"

#include "x3d/render/RenderSystem.h"
#include "x3d/resource/TextureManager.h"
#include "x3d/material/MaterialManager.h"
#include "x3d/resource/GpuProgramManager.h"
#include "x3d/resource/VertexBufferManager.h"

#include "x3d/render/FullScreenQuad.h"
#include "x3d/resource/RenderTexture.h"
#include "x3d/render/RenderQueue.h"

#include "x3d/render/SceneRender.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/graphic/IndexBuffer.h"
#include "x3d/graphic/VertexLayout.h"

#include "x3d/lighting/LightVolume.h"
#include "x3d/lighting/PointLightVolume.h"
#include "X3D/graphic/GraphicSystem.h"

_X3D_NS_BEGIN


UnitTestRenderOP::UnitTestRenderOP(RenderSystem* pRenderSystem)
: RenderOperation(pRenderSystem)
{
	//_pRenderGBufferMtl = MaterialManager::Instance().AddMaterial(L"RenderGBuffer1");
	//_pLPPDiffuseMtl = MaterialManager::Instance().AddMaterial(L"LPP_Diffuse");

	_pRenderGBufferMtl = MaterialManager::Instance().AddMaterial(TEXT("RenderNormaledGBuffer"));
	_pLPPDiffuseMtl = MaterialManager::Instance().AddMaterial(TEXT("LPP_NormaledDiffuse"));
}

UnitTestRenderOP::~UnitTestRenderOP(void)
{

}

void UnitTestRenderOP::Render(RenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	RenderQueue* pRenderQueue = renderContext.GetSceneRender()->GetRenderQueue();
	RenderGroup* pOpaqueGroup = pRenderQueue->GetRenderGroup(RENDER_GROUP_OPAQUE);

	const RendableObjectList& renderableObjList = pOpaqueGroup->GetRendableObjectList();

	// --------- Rednering G-Buffer -----------
	//_pGraphicSystem->SetRenderTarget(renderContext.pRenderWindow->getGBuffer()->GetRenderTarget());
	_pGraphicSystem->SetColorBuffer(0, renderContext.pRenderWindow->getGBuffer()->GetColorBuffer());
	_pGraphicSystem->SetDepthStencilBuffer(renderContext.GetWndRenderTarget()->GetDepthStencilBuffer());
	_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_ALL, COLOR_ARGB(255, 0, 0, 0));

	_pGraphicSystem->SetDepthBufferParams(TRUE, TRUE);

	Pass* pPassRenderGBuffer = _pRenderGBufferMtl->GetBestTechnique()->GetPasses()[0];

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);
		RendableObject* pRenderableObject = CURRENT_DATA(renderableObjList);

		const String& strName = pRenderableObject->GetMaterial()->GetName();
		if(strName == TEXT("VertexColorMaterial")) CONTINUE(renderableObjList);

		renderContext.GetAutoParamDataProvider().SetCurrentRenderableObject(pRenderableObject);

		_setPass(pPassRenderGBuffer);
		_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());

		GET_NEXT(renderableObjList);
	FASTEST_ITERATE_END();


	// ------------ Rendering Light buffer -----------
	_pGraphicSystem->SetColorBuffer(0, renderContext.pRenderWindow->getLightBuffer()->GetColorBuffer());
	_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_COLOR, COLOR_ARGB(1, 1, 1, 1));

	// Set Render state
	_pGraphicSystem->SetSceneBlending(FACTOR_ONE, FACTOR_ONE, FRAME_BLEND_ADD);
	_pGraphicSystem->SetDepthBufferParams(FALSE, FALSE);
	_pGraphicSystem->SetCullMode(CULL_CLOCKWISE);

	const LightList& lightList = pRenderQueue->GetLightGroup()->GetLightList();
	PointLightVolume* pLightVolume = (PointLightVolume*)lightList[1]->getLightVolume();
	pLightVolume->BeginRenderLightVolumes(&renderContext);

	FASTEST_VECTOR_ITERATE(Light*, lightList);
		Light* pLight = CURRENT_DATA(lightList);	
		if(pLight->GetLightType() != LIGHT_POINT) CONTINUE(lightList);

		LightList tmpLightList;
		tmpLightList.push_back(pLight);
		renderContext.GetAutoParamDataProvider().SetCurrentLightList(&tmpLightList);

		LightVolume* pLightVolume = pLight->getLightVolume();
		XASSERT(pLightVolume);

		pLightVolume->RenderLightBuffer(&renderContext);
		GET_NEXT(lightList);
	FASTEST_ITERATE_END();

	// Restore render state
	_pGraphicSystem->SetCullMode(CULL_ANTICLOCKWISE);
	_pGraphicSystem->SetSceneBlending(FACTOR_ONE, FACTOR_ZERO, FRAME_BLEND_ADD);


	//----- Forward rending opaque scene objects -----
	_pGraphicSystem->SetColorBuffer(0, renderContext.GetWndRenderTarget()->GetColorBuffer());
	_pGraphicSystem->ClearFrameBuffer(FRAME_BUFFER_COLOR, renderContext.pDestRenderArea->GetBackgroundColor());

	Technique* pTechnique = _pLPPDiffuseMtl->GetBestTechnique();
	renderContext.GetAutoParamDataProvider().SetCurrentLightList(&lightList);

	PassList& passes = pTechnique->GetPasses();
	Pass* pPass = passes[0];
	_pGraphicSystem->SetSceneBlending( pPass->GetSrcBlendFactor()
								, pPass->GetDestBlendFactor()
								, pPass->GetFrameBlendOperation() );

	_pGraphicSystem->SetCullMode(pPass->GetHardwareCullMode());
	_pGraphicSystem->SetDepthBufferParams(pPass->GetDepthEnable(), pPass->GetDepthWrite()
											, pPass->GetDepthCompareFunction());
	_pGraphicSystem->SetTextureUnitStates(pPass->GetTextureUnitStates());

	Shader* pShader = pPass->GetShader();
	_pGraphicSystem->SetShader(pShader);

	FASTEST_VECTOR_REITERATE(RendableObject*, renderableObjList);
		RendableObject* pRenderableObject = CURRENT_DATA(renderableObjList);

		const String& strName = pRenderableObject->GetMaterial()->GetName();
		if(strName == TEXT("VertexColorMaterial")) CONTINUE(renderableObjList);

		renderContext.GetAutoParamDataProvider().SetCurrentRenderableObject(pRenderableObject);	

		pShader->UpdateAutoParameters(&_pRenderContext->GetAutoParamDataProvider());

		_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());

		GET_NEXT(renderableObjList);

	FASTEST_ITERATE_END();
}

_X3D_NS_END