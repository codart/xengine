#pragma once

_X3D_NS_BEGIN

class RenderSystem;

/*
	Abstract base class for all objects that related with rendering. 
*/
class _X3DExport RenderObject : public Object
							, public NonCopyable
{
public:
	RenderObject(RenderSystem* pRenderSystem);
	RenderObject(const RenderObject& renderObj);

	virtual ~RenderObject(void);


public:
	INLINE RenderSystem*
		GetRenderSystem() { return _pRenderSystem; }
	GraphicSystem*
		GetGraphicSystem();


protected:
	RenderSystem*
		_pRenderSystem;

};


_X3D_NS_END