#include "StdAfx.h"
#include "X3DModule.h"

#include "x3d/graphic/GraphicSystem.h"
#include "x3d/render/RenderSystem.h"
#include "Common/input/_InputManager.h"
#include "Common/time/TimerMgr.h"

#include "Common/plantform/FileFind.h"
#include "x3d/PlugableModule.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/material/MaterialManager.h"
#include "x3d/resource/MeshManager.h"
#include "x3d/resource/GpuProgramManager.h"
#include "x3d/resource/VertexBufferManager.h"
#include "x3d/resource/IndexBufferManager.h"
#include "x3d/animation/SkeletonManager.h"

#include "X3D/graphic/WindowRenderTarget.h"
#include "x3d/X3DModuleEvent.h"

#include "common/script/ScriptBinder.h"
#include "Common/script/EventDelegator.h"
#include "Common/util/Unicode.h"

#include "common/util/INIDocument.h"
#include "common/debug/Logger.h"

_X3D_NS_BEGIN

typedef PlugableModule* FuncGetPlugableModule();

X3DModule::X3DModule(DWORD dwUserData)
{
	_pGraphicSystem = NULL;
	_pRenderSystem = NULL;

	_hModuleHandle = dwUserData;
}

X3DModule::~X3DModule(void)
{

}

void X3DModule::_loadGraphicSystemModule()
{
	INIDoc configINI;
	String strFileName = MODULE_PATH + TEXT("x3d.ini");
	SI_Error error = configINI.LoadFile(strFileName.c_str());
	if(error == SI_FILE)
	{
		ERRORLOG << "INI file " << strFileName.c_str() << "doesn't exist!";
	}

	String strGraphicSysModuleName = configINI.GetValue(TEXT(""), TEXT("GraphicSystem"), TEXT("D3D9GraphicSystem.dll"));
	String strModuleFile = MODULE_PATH + strGraphicSysModuleName;
	HMODULE hGraphicSysModule = ::LoadLibrary(strModuleFile.c_str());
	XASSERT(hGraphicSysModule);
	
	FuncGetPlugableModule* pGetGraphicSysModuleFunc = (FuncGetPlugableModule*)::GetProcAddress(hGraphicSysModule
																					, "GetPlugableModule");
	PlugableModule* pModule = pGetGraphicSysModuleFunc();
	pModule->Install();

	_plugableModules.push_back(pModule);
}

void X3DModule::_unloadPlugableModules()
{
	PlugableModuleListIt it = _plugableModules.begin();
	PlugableModuleListIt itEnd = _plugableModules.end();

	for ( ; it != itEnd; ++it)
	{
		PlugableModule* pPlugableModule = *it;
		pPlugableModule->Uninstall();
		pPlugableModule->Destroy();
	}

}

void X3DModule::installGraphicSystem(GraphicSystem* pGraphicSystem)
{
	_pGraphicSystem = pGraphicSystem;
}

GraphicSystem* X3DModule::uninstallGraphicSystem()
{
	GraphicSystem* pGraphicSystem = _pGraphicSystem;
	_pGraphicSystem = NULL;
	return pGraphicSystem;
}

void X3DModule::Init(const Event* pEvent)
{
	X3DModuleEvent event(X3DModuleEvent::ENGINE_INIT);
	DispatchEvent(event);

	GetCore()->EnableConsole(TRUE);

	_loadGraphicSystemModule();	

	// Load RenderSystem
	//... should make renderSystem separate module later!
	_pRenderSystem = XNEW RenderSystem(_pGraphicSystem);

	XASSERT(_pGraphicSystem);
	_pGraphicSystem->Init(0);
	_pRenderSystem->Init();

	//InputManager::Instance().Init(GetModuleHandle());

	GPUProgramManager::Instance().init(_pRenderSystem);
	TextureManager::Instance().init(_pRenderSystem);
	MaterialManager::Instance().init(_pRenderSystem);
	VertexBufferManager::Instance().init(_pRenderSystem);
	IndexBufferManager::Instance().init(_pRenderSystem);
	MeshManager::Instance().init(_pRenderSystem);


	_pScriptBinder = XNEW ScriptBinder();

	// Export render window
	RenderWindow::ExportToScript(_pScriptBinder);

	// Export event
	_pScriptBinder->BindClass<Event>("Event");
	_pScriptBinder->BindProperty<Event>("nType", &Event::nType);

	// Export X3DManager
	_pScriptBinder->BindClass<X3DModule>("X3DManager");
	//..._pScriptBinder->BindMethod<X3DManager>("SetMaxFPS", &X3DManager::SetMaxFPS);

	typedef RenderWindow* ( X3DModule::*FuncGetRenderWindow)(const char* );
	FuncGetRenderWindow funcGetLuaWindow = &X3DModule::GetRenderWindow;
	_pScriptBinder->BindMethod<X3DModule>("GetRenderWindow", funcGetLuaWindow);
	//..._pScriptBinder->BindMethod<X3DModule>("EnableConsole", &X3DModule::EnableConsole);

	typedef int ( X3DModule::*FuncAddEventListenerEx)(int, const char*);
	FuncAddEventListenerEx funcAddEventListenerEx = &X3DModule::AddEventListener;
	_pScriptBinder->BindMethod<X3DModule>("AddEventListener", funcAddEventListenerEx);

	EventDispatcher::EnableScriptSupport(TRUE, _pScriptBinder);

	_pScriptBinder->BindObject("X3DManager", this);

	USING_STRING_CONVERT;
	AString strLuaLibName = WStrToAStr(RESOURCE_PATH.c_str());
	strLuaLibName += "script\\lualib.lua";
	_pScriptBinder->RunFile(strLuaLibName);

	event.nType = X3DModuleEvent::ENGINE_INITED;
	DispatchEvent(event);
}

void X3DModule::Tick(const Event* pEvent)
{
	// Render all StageWindow
	_updateAllRenderWindow();
}

void X3DModule::Uninit(const Event* pEvent)
{
	X3DModuleEvent event(X3DModuleEvent::ENGINE_DESTROY);
	DispatchEvent(event);

	SAFE_DELETE(_pScriptBinder);

	ClearAllHUDStage();
	ClearAllScene();

	RemoveAllRenderWindow();

	RemnoveAllHUDStage();
	RemoveAllScene();

	_pRenderSystem->Destroy();
	_pRenderSystem = NULL;

	MeshManager::Instance().unInit();
	IndexBufferManager::Instance().unInit();
	VertexBufferManager::Instance().unInit();
	MaterialManager::Instance().unInit();
	TextureManager::Instance().unInit();
	GPUProgramManager::Instance().unInit();
	SkeletonManager::Instance().unInit();

	XASSERT(_pGraphicSystem);
	_pGraphicSystem->Uninit();

	InputManager::Instance().UnInit();

	_unloadPlugableModules();
}

void X3DModule::RunScriptString(const AString& strScript)
{
	XASSERT(_pScriptBinder);
	_pScriptBinder->RunString(strScript);

	fflush(stdout);
}

/*
void X3DModule::Quit()
{
	::PostQuitMessage(0);
}*/


void X3DModule::_updateAllRenderWindow()
{
	RenderWindowOwnerMapIter it = _renderWindowMap.begin();
	RenderWindowOwnerMapIter itEnd = _renderWindowMap.end();
	
	for ( ; it != itEnd; ++it)
	{
		OwnerPtr<RenderWindowPtr>& pRenderWindow = it->second;
		pRenderWindow->update();
	}
}

RenderWindow* X3DModule::GetRenderWindow(const String& strWindowName)
{
	RenderWindowOwnerMapIter it = _renderWindowMap.find(strWindowName);
	if(it == _renderWindowMap.end()) return NULL;
	else return it->second;
}

RenderWindow* X3DModule::GetRenderWindow(const char* pszWindowName)
{
	USING_STRING_CONVERT;
	return X3DModule::GetRenderWindow(String(AStrToStr(pszWindowName)));
}

RenderWindow* X3DModule::AddRenderWindow(const String& strWindowName, const Rect& rect)
{
	RenderWindow* pRenderWindow = GetRenderWindow(strWindowName);
	if(pRenderWindow)
	{
		USING_STRING_CONVERT;

		AStringStream ss;
		ss << "Cannot create RenderWindow because a RenderWindow with name " << WStrToAStr(strWindowName.c_str())
			<< "alreay exist!";
		XTHROWEX(ss.str().c_str());
	}

	pRenderWindow = XNEW RenderWindow(_pRenderSystem, strWindowName);
	pRenderWindow->CreateWnd(strWindowName, rect);
	
	WindowRenderTarget* pWindowRenderTarget = _pGraphicSystem->CreateWindowRenderTarget(pRenderWindow->GetWindow());
	pRenderWindow->attachWndRenderTarget(pWindowRenderTarget);
	_renderWindowMap[strWindowName] = pRenderWindow;

	return pRenderWindow;
}

void X3DModule::RemoveRenderWindow(const String& strWindowName)
{
	RenderWindowOwnerMapIter it = _renderWindowMap.find(strWindowName);
	if(it == _renderWindowMap.end()) return;

	RenderWindow* pRenderWindow = it->second;
	pRenderWindow->Destroy();
	XDELETE pRenderWindow;

	_renderWindowMap.erase(it);
}

void X3DModule::RemnoveAllHUDStage()
{
	_HUDStageMap.clear();
}

void X3DModule::ClearAllHUDStage()
{
	MapIterator< String, OwnerPtr<HUDStagePtr> > iterator(_HUDStageMap);
	while (iterator.HasMore())
	{
		iterator.CurrentData()->Clear();
		iterator.Next();
	}
}

void X3DModule::ClearAllScene()
{
	MapIterator< String, SceneOwnerPtr > iterator(_sceneMap);
	while (iterator.HasMore())
	{
		iterator.CurrentData()->Clear();
		iterator.Next();
	}
}

void X3DModule::RemoveAllRenderWindow()
{
	MapIterator<String, OwnerPtr<RenderWindowPtr> > _renderWindowIterator(_renderWindowMap);

	while(_renderWindowIterator.HasMore())
	{
		RenderWindow* pRenderWindow = _renderWindowIterator.CurrentData();
		pRenderWindow->DestroyWnd();
		_renderWindowIterator.Next();
	}
	_renderWindowMap.clear();
}

void X3DModule::RemoveAllScene()
{
	_sceneMap.clear();
}

CameraPtr X3DModule::AddCamera(const String& strCameraName)
{
	CameraPtr pCamera = GetCamera(strCameraName);
	if(pCamera)
	{
		USING_STRING_CONVERT;

		AStringStream ss;
		ss << "Cannot create Camera because a Camera with name " << WStrToAStr(strCameraName.c_str())
			<< "alreay exist!";
		XTHROWEX(ss.str().c_str());
	}

	
	CameraPtr pCameraPtr(XNEW Camera);
	_cameraMap[strCameraName] = pCameraPtr;

	return pCameraPtr;
}

void X3DModule::RemoveCamera(const String& strCameraName)
{
	CameraOwnerMapIter it = _cameraMap.find(strCameraName);
	if(it == _cameraMap.end()) return;

	CameraPtr& pCamera = it->second;
	int nRefCount = pCamera.GetReferenceCount();
	pCamera.Release();

	if(nRefCount != 1)
	{
		XTHROWEX("Camera be removed when is still be referenced by other object!");
	}

	_cameraMap.erase(it);
}

CameraPtr X3DModule::GetCamera(const String& strCameraName)
{
	CameraOwnerMapIter it = _cameraMap.find(strCameraName);
	if(it == _cameraMap.end()) return CameraPtr(NULL);
	else return it->second;
}

HUDStagePtr X3DModule::GetHUDStage(const String& strStageName)
{
	HUDStageOwnerMapIter it = _HUDStageMap.find(strStageName);
	if(it == _HUDStageMap.end()) return HUDStagePtr(NULL);
	else return it->second;
}

HUDStagePtr X3DModule::AddHUDStage(const String& strStageName)
{
	HUDStage* pHUDStage = GetHUDStage(strStageName);
	if(pHUDStage)
	{
		USING_STRING_CONVERT;

		AStringStream ss;
		ss << "Cannot create HUD Stage because a Stage with name " << WStrToAStr(strStageName.c_str())
			<< "alreay exist!";
		XTHROWEX(ss.str().c_str());
	}

	HUDStagePtr pHUDStagePtr(XNEW HUDStage(_pRenderSystem));
	_HUDStageMap[strStageName] = pHUDStagePtr;

	return pHUDStagePtr;
}

ScenePtr X3DModule::AddScene(const String& strSceneName)
{
	Scene* pScene = GetScene(strSceneName);
	if(pScene)
	{
		USING_STRING_CONVERT;

		AStringStream ss;
		ss << "Cannot create Scene because a Scene with name " << WStrToAStr(strSceneName.c_str())
			<< "alreay exist!";
		XTHROWEX(ss.str().c_str());
	}

	ScenePtr pScenePtr(XNEW Scene(_pRenderSystem));
	_sceneMap[strSceneName] = pScenePtr;

	return pScenePtr;
}

void X3DModule::RemoveScene(const String& strSceneName)
{
	SceneOwnerMapIter it = _sceneMap.find(strSceneName);
	if(it == _sceneMap.end()) return;

	ScenePtr& pScenePtr = it->second;
	int nRefCount = pScenePtr.GetReferenceCount();
	pScenePtr.Release();

	if(nRefCount != 1)
	{
		XTHROWEX("Scene still be referenced by other object, remove scene failed!");
	}

	_sceneMap.erase(it);
}

ScenePtr X3DModule::GetScene(const String& strSceneName)
{
	SceneOwnerMapIter it = _sceneMap.find(strSceneName);
	if(it == _sceneMap.end()) return ScenePtr(NULL);
	else return it->second;
}



_X3D_NS_END