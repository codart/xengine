#pragma once

_X3D_NS_BEGIN

class _X3DExport PlugableModule
{
public:
	PlugableModule(void);
	virtual ~PlugableModule(void);

public:
	virtual void
		Install() { XASSERT(0); }
	virtual void
		Uninstall() { XASSERT(0); }

	// Destroy itself.
	virtual void
		Destroy() { XDELETE this; }

};

DECLARE_LIST_TYPE_PTR(vector, PlugableModule);

_X3D_NS_END