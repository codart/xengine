#include "stdafx.h"
#include "HUDCanvas.h"
#include "RenderableQuad.h"

_X3D_NS_BEGIN

HUDCanvas::HUDCanvas(const String& strName, Scene* pScene
					, RenderableQuad* pRenderableQuad, TexturePtr& ptrDynamicTexture)
: MovableObject2D(strName, pScene), _ptrDynamicTexture(ptrDynamicTexture)
{
	XASSERT(pRenderableQuad);
	_pRenderableQuad = pRenderableQuad;
	_pRenderableQuad->setParentHUDObject(this);

	_ptrDynamicTexture = ptrDynamicTexture;
}

HUDCanvas::~HUDCanvas(void)
{
	SAFE_DELETE(_pRenderableQuad);
}

void HUDCanvas::fillRenderableObjList(RendableObjectList& renderableObjList)
{
	renderableObjList.push_back(_pRenderableQuad);
}

void HUDCanvas::setParentNode(Node2D* pParentNode)
{
	MovableObject2D::setParentNode(pParentNode);

	// Update bounding rect ( or add a flag _transformDirty = true, then update the bounding rect the first time
	// getBoundingRect be called???)

}

_X3D_NS_END