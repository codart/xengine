#pragma once

#include "x3d/scene/RendableObject.h"
#include "x3d/datadef/VertexDef.h"

_X3D_NS_BEGIN

class MovableObject2D;

// Use HUDQuad to render a quad.
class _X3DExport RenderableQuad : public RendableObject
{
public:
	RenderableQuad(RenderSystem* pRenderSystem);
	~RenderableQuad(void);


public:
	void 
		Init(const MaterialPtr& pMaterial, float fWidth, float fHeight);

	override transient const Material* 
		GetMaterial(void) const { return _pMaterial; };
	override const DrawPrimitiveCommand*
		GetDrawPrimitiveCmd() const;
	void 
		SetSize(float fWidth, float fHeight);
	float 
		GetWidth() const { return _fWidth; }
	float 
		GetHeight() const { return _fHeight; }

	// Update quad's vertex data.
	override void
		UpdateTransform(const ViewportInfo& viewportInfo);

	temporary void
		setParentHUDObject(MovableObject2D* pParentHUDObject);

protected:
	Real 
		_fWidth;
	Real
		_fHeight;

	Quad2DST
		_quad;

	DrawPrimitiveCommand*
		_pDrawPrimitiveCmd;
	MaterialPtr 
		_pMaterial;

	MovableObject2D*
		_pParentHUDObject;

	temporary IndexData*
		_pQuadIndexData;
	temporary VertexData*
		_pQuadVertexData;

};


_X3D_NS_END