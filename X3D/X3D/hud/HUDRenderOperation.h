#pragma once

_X3D_NS_BEGIN

class HUDRender;
class RenderArea;
class RenderTarget;
class HUDRenderOperation;

class HUDRenderContext
{
public:
	HUDRenderContext(HUDRender* pHUDRender, RenderArea* pRenderArea
						, RenderTarget* pDestRenderTarget)
	{
		this->pHUDRender = pHUDRender;
		this->pDestRenderTarget = pDestRenderTarget;
		this->pDestRenderArea = pRenderArea;
		this->pLastRenderOP = NULL;
		this->pNextRenderOP = NULL;
	}

public:
	HUDRender*
		pHUDRender;

	RenderArea*
		pDestRenderArea;
	RenderTarget*
		pDestRenderTarget;

	// ---
	HUDRenderOperation*
		pLastRenderOP;
	HUDRenderOperation*
		pNextRenderOP;

};


class HUDRenderOperation
{
public:
	HUDRenderOperation(RenderSystem* pRenderSystem);
	virtual ~HUDRenderOperation(void);


public:
	void 
		Render(HUDRenderContext& renderContext);


protected:
	RenderSystem*
		_pRenderSystem;
	GraphicSystem*
		_pGraphicSystem;

	HUDRenderContext* 
		_pRenderContext;


protected:
	INLINE void 
		_setPass(Pass* pPass);

};


DECLARE_LIST_TYPE_PTR(vector, HUDRenderOperation);

_X3D_NS_END