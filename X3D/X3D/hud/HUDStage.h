#pragma once

#include "x3d/scene/RendableObject.h"
#include "x3d/hud/Node2D.h"
#include "x3d/hud/MovableObject2D.h"

_X3D_NS_BEGIN

class HUDRender;
class HUDBitmap;
class HUDCanvas;

class _X3DExport HUDStage
{
public:
	HUDStage(RenderSystem* pRenderSystem);
	virtual ~HUDStage(void);


public:
	virtual void 
		Destroy();

	INLINE HUDRender*
		GetHUDRender() { return _pHUDRender; }
	
	/// Add a bitmap HUD from a bitmap file.
	HUDBitmap*
		AddHUDBitmap(const String& strName, const String& strBitmapName);
	/// Add a bitmap HUD from an exist texture.
	HUDBitmap*
		AddHUDBitmap(const String& strName, TexturePtr& pTexture);

	// Add a empty canvas HUD.
	HUDCanvas*
		AddHUDCanvas(const String& strName, Size size);


	// Remove all nodes and all movable objects
	void 
		Clear();

	// Now just return a renderable obj list without using any SpatialMgr.
	temporary void
		getRendableObjectList(RendableObjectList& renderableObjList);

	virtual Node2D*
		GetRootNode() { return _pRootNode; }
	virtual Node2D*
		CreateHUDNode(const String& strNodeName);
	virtual Node2D*
		GetHUDNode(const String& strNodeName);

	virtual void 
		DestroyHUDNode(const String& strNodeName);

	// Add a node as the child of the root node.
	virtual void
		AddChild(Node2D* pHUDNode);

	//--- Hit test ---
	// Return a object that hit the point(x, y)
	virtual MovableObject2D*
		HitTest(float x, float y);


protected:
	RenderSystem* 
		_pRenderSystem;
	HUDRender*
		_pHUDRender;

	Node2D*
		_pRootNode;

	/* 
		The HUDStage is not only a factory to create HUD node but also management the 
		lifetime of these objects. This map stores all nodes created by HUDStage. 
	*/
	Node2DMap
		_allHUDNodes;

	MovableObject2DMap
		_movableObjectMap;

	
protected:
	void 
		_traverseHUDNode(Node2D* pHUDNode, RendableObjectList& renderableObjList);

};


DECLARE_MAP_TYPE_PTR(map, String, HUDStage);

typedef SharedPtr<HUDStage>	HUDStagePtr;
DECLARE_MAP_TYPE(map, String, HUDStagePtr);


_X3D_NS_END