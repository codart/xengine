#include "StdAfx.h"
#include "Node2D.h"
#include "x3d/hud/RenderableQuad.h"

_X3D_NS_BEGIN

MovableObject2D::MovableObject2D(Scene* pScene)
{

}

MovableObject2D::MovableObject2D(const String& strName, Scene* pScene)
: _strName(strName)
{

}

MovableObject2D::~MovableObject2D(void)
{

}

Vector2 MovableObject2D::GetPosition() const
{
	XASSERT(SAFE_CAST(_pParentNode, Node2D*));

	if(_pParentNode)
	{
		return ((Node2D*)_pParentNode)->GetPosition();
	}
	else
	{
		XASSERT(0);
		return Vector2(0, 0);
	}
}

RectF MovableObject2D::GetBoundingRect() const
{
	XASSERT(0);
	return RectF(0, 0, 0, 0);
}

const AxisAlignedBox& MovableObject2D::GetBoundingBox(void) const
{
	XASSERT(0);
	return *((AxisAlignedBox*)NULL);
}

void MovableObject2D::fillRenderableObjList(RendableObjectList& renderableObjList)
{
	XASSERT(0);
}

_X3D_NS_END