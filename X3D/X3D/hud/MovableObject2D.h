#pragma once


_X3D_NS_BEGIN

class RenderableQuad;
class Node2D;

class _X3DExport MovableObject2D 
{
public:
	MovableObject2D(Scene* pScene);
	MovableObject2D(const String& strName, Scene* pScene);
	virtual ~MovableObject2D(void);


public:
	virtual const AxisAlignedBox& 
		GetBoundingBox(void) const;

	virtual void
		setParentNode(Node2D* pParentNode) { _pParentNode = pParentNode; }

	virtual void
		fillRenderableObjList(RendableObjectList& renderableObjList);


	// Get the object's bounding rect (relative to the renderArea).
	temporary virtual RectF
		GetBoundingRect() const;
	
	//... Get position only now
	temporary virtual Vector2
		GetPosition() const;


protected:
	/// Node to which this object is attached
	Node2D*
		_pParentNode;
	String
		_strName;

};

DECLARE_LIST_TYPE_PTR(vector, MovableObject2D);
DECLARE_MAP_TYPE_PTR(map, String, MovableObject2D);

_X3D_NS_END