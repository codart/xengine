#pragma once

#include "MovableObject2D.h"

_X3D_NS_BEGIN

class InteractiveObject : public MovableObject2D
{
public:
	InteractiveObject(Scene* pScene);
	virtual ~InteractiveObject(void);
};


_X3D_NS_END