#include "StdAfx.h"
#include "RenderableQuad.h"
#include "x3d/material/MaterialManager.h"

#include "x3d/graphic/IndexBuffer.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/VertexData.h"
#include "x3d/hud/MovableObject2D.h"

_X3D_NS_BEGIN

RenderableQuad::RenderableQuad(RenderSystem* pRenderSystem)
: RendableObject(pRenderSystem)
{
	_fWidth = 0;
	_fHeight = 0;
	_pParentHUDObject = NULL;
}

void RenderableQuad::Init(const MaterialPtr& pMaterial, float fWidth, float fHeight)
{
	_fWidth = fWidth;
	_fHeight = fHeight;

	_pParentHUDObject = NULL;
	_pMaterial = pMaterial;

	_pDrawPrimitiveCmd = XNEW DrawPrimitiveCommand;
	_pDrawPrimitiveCmd->SetPrimitiveCount(2);
	_pDrawPrimitiveCmd->SetDrawPrimitiveType(DRAW_TRIANGLE_LIST);

	_pQuadIndexData = XNEW IndexData(GetGraphicSystem());
	_pQuadVertexData = XNEW VertexData(GetGraphicSystem());

	_pDrawPrimitiveCmd->SetIndexData(_pQuadIndexData);
	_pDrawPrimitiveCmd->SetVertexData(_pQuadVertexData);

	IndexBuffer* pIndexBuffer = _pQuadIndexData->initIndexBuffer(6, sizeof(USHORT));

	USHORT indices[6];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 2;
	indices[4] = 3;
	indices[5] = 0;

	pIndexBuffer->WriteData(0, 6 * sizeof(USHORT), indices);

	VertexBuffer* pVertexBuffer = _pQuadVertexData->AddVertexBufferBinding(4, sizeof(QuadVertex2D), TRUE);

	QuadVertex2D vertices[4];
	vertices[0].position.SetVector(-0.2f, 0.2f, 0);
	vertices[0].uv1.x = 0;
	vertices[0].uv1.y = 0;

	vertices[1].position.SetVector(0.2f, 0.2f, 0);
	vertices[1].uv1.x = 1;
	vertices[1].uv1.y = 0;

	vertices[2].position.SetVector(0.2f, -0.2f, 0);
	vertices[2].uv1.x = 1;
	vertices[2].uv1.y = 1;

	vertices[3].position.SetVector(-0.2f, -0.2f, 0);
	vertices[3].uv1.x = 0;
	vertices[3].uv1.y = 1;

	pVertexBuffer->WriteData(0, 4 * sizeof(QuadVertex2D), vertices);

	VertexLayout* pVertexLayout = _pQuadVertexData->GetVertexLayout();
	pVertexLayout->BeginUpdate();
	pVertexLayout->AddElement(0, 0, VEF_FLOAT3, VES_POSITION);
	pVertexLayout->AddElement(0, 12, VEF_FLOAT2, VES_TEXTURE_COORDINATES);
	pVertexLayout->EndUpdate();
}

RenderableQuad::~RenderableQuad(void)
{
	SAFE_DELETE(_pQuadVertexData);
	SAFE_DELETE(_pQuadIndexData);
	SAFE_DELETE(_pDrawPrimitiveCmd);
}

void RenderableQuad::SetSize(float fWidth, float fHeight)
{
	//... _bSizeChanged = TURE;
	_fWidth = fWidth;
	_fHeight = fHeight;
}

void RenderableQuad::UpdateTransform(const ViewportInfo& viewportInfo)
{
	Vector2 vPosition = _pParentHUDObject->GetPosition();

	REAL width = (REAL)2.0 * _fWidth / (REAL)(viewportInfo.Width);
	REAL height = (REAL)2.0 * _fHeight / (REAL)(viewportInfo.Height);

	REAL fLeft = -1 + (REAL)2.0 * vPosition.x / viewportInfo.Width;
	REAL fRight = fLeft + width;
	REAL fTop = 1 - (REAL)2.0 * vPosition.y / viewportInfo.Height;
	REAL fBottom = fTop - height;

	// Update quad vertex data
	VertexBuffer* pVertexBuffer = _pQuadVertexData->GetVertexBuffer(0);
	XASSERT(pVertexBuffer);

/*
	0 --- 1
	|\	  |
	|  \  |
	|	\ |
	3 --- 2	
*/

	QuadVertex2D vertices[4];
	vertices[0].position.SetVector(fLeft, fTop, 0);
	vertices[0].uv1.x = 0;
	vertices[0].uv1.y = 0;

	vertices[1].position.SetVector(fRight, fTop, 0);
	vertices[1].uv1.x = 1;
	vertices[1].uv1.y = 0;

	vertices[2].position.SetVector(fRight, fBottom, 0);
	vertices[2].uv1.x = 1;
	vertices[2].uv1.y = 1;

	vertices[3].position.SetVector(fLeft, fBottom, 0);
	vertices[3].uv1.x = 0;
	vertices[3].uv1.y = 1;

	pVertexBuffer->WriteData(0, 4 * sizeof(QuadVertex2D), vertices, TRUE);
}

void RenderableQuad::setParentHUDObject(MovableObject2D* pParentHUDObject)
{
	_pParentHUDObject = pParentHUDObject;
}

const DrawPrimitiveCommand* RenderableQuad::GetDrawPrimitiveCmd() const
{
	return _pDrawPrimitiveCmd;
}

_X3D_NS_END