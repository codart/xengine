#pragma once

#include "MovableObject2D.h"
#include "x3d/resource/DynamicTexture.h"

_X3D_NS_BEGIN

/*
	Pixel buffer lock 后，包含Pitch。为了能够快速绘制，应当开辟一个没有Pitch的buffer，
	参考egg里的方法。绘制完毕后，按Row拷贝到Pixel Buffer 里。
*/
class _X3DExport HUDCanvas : public MovableObject2D
{
public:
	HUDCanvas(const String& strName, Scene* pScene
			, RenderableQuad* pHUDQuad, TexturePtr& ptrDynamicTexture);
	virtual ~HUDCanvas(void);


public:
	override void
		fillRenderableObjList(RendableObjectList& renderableObjList);
	// Add code to update bounding rect
	override void
		setParentNode(Node2D* pParentNode);


protected:
	RenderableQuad*
		_pRenderableQuad;

	DynamicTexturePtr
		_ptrDynamicTexture;

};

_X3D_NS_END