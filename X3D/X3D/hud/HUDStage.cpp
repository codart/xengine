#include "StdAfx.h"
#include "HUDStage.h"

#include "x3d/material/MaterialManager.h"
#include "x3d/resource/TextureManager.h"
#include "HUDRender.h"
#include "x3d/hud/Node2D.h"
#include "x3d/hud/RenderableQuad.h"
#include "x3d/hud/HUDBitmap.h"
#include "x3d/hud/HUDCanvas.h"

#include "x3d/render/RenderSystem.h"

_X3D_NS_BEGIN

HUDStage::HUDStage(RenderSystem* pRenderSystem)
{
	_pRenderSystem = pRenderSystem;
	_pHUDRender = XNEW HUDRender(_pRenderSystem, this);

	_pRootNode = XNEW Node2D(this, TEXT("ROOT Node2D"));
}

HUDStage::~HUDStage(void)
{
	Clear();

	SAFE_DELETE(_pHUDRender);
	SAFE_DELETE(_pRootNode);
}

void HUDStage::Destroy()
{
	delete this;
}

MovableObject2D* HUDStage::HitTest(float x, float y)
{
	//... Currently has no SpatialMgr for HUDStage, just traverse all nodes
	TreeNodeList nodes = _pRootNode->GetChildNodes();

	Node2D* pNode0 = (Node2D*)nodes[0];
	
	MovableObject2DList objects = pNode0->GetMovableObject2DList();
	MovableObject2D* pHUDObject = (MovableObject2D*)objects[0];
	RectF rect = pHUDObject->GetBoundingRect();

	if(rect.PtInRect(x, y))
	{
		return pHUDObject;
	}
	else 
	{
		return NULL;
	}
}

void HUDStage::AddChild(Node2D* pHUDNode)
{
	_pRootNode->AddChild(pHUDNode);
}

void HUDStage::getRendableObjectList(RendableObjectList& renderableObjList)
{
	// Traverse all nodes and fill the list with renderable objects.
	_traverseHUDNode(_pRootNode, renderableObjList);
}

void HUDStage::_traverseHUDNode(Node2D* pHUDNode, RendableObjectList& renderableObjList)
{
	const MovableObject2DList& movableObjList = pHUDNode->GetMovableObject2DList();
	FASTEST_VECTOR_ITERATE(MovableObject2D*, movableObjList);
		MovableObject2D* pMovableObj = GET_NEXT(movableObjList);
		pMovableObj->fillRenderableObjList(renderableObjList);
	FASTEST_ITERATE_END();

	// Traverse all children
	const TreeNodeList& children = pHUDNode->GetChildNodes();

	FASTEST_VECTOR_ITERATE(Node2D*, children);
		Node2D* pHUDNode = GET_NEXT(children);
		_traverseHUDNode(pHUDNode, renderableObjList);
	FASTEST_ITERATE_END();
}

void HUDStage::Clear()
{
	// Release all movable object
	MovableObject2DMapIt it = _movableObjectMap.begin();
	MovableObject2DMapIt itEnd = _movableObjectMap.end();

	for ( ; it != itEnd; ++it)
	{
		MovableObject2D* pMovableObject = it->second;
		XDELETE pMovableObject;
	}

	_movableObjectMap.clear();

	// Release all HUD nodes
	//...
	Node2DMapIt it2 = _allHUDNodes.begin();
	Node2DMapIt itEnd2 = _allHUDNodes.end();

	for ( ; it2 != itEnd2; ++it2)
	{
		XDELETE it2->second;
	}

	_allHUDNodes.clear();
}

HUDCanvas* HUDStage::AddHUDCanvas(const String& strName, Size size)
{
	MovableObject2DMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end())
	{
		return SAFE_CAST(it->second, HUDCanvas*);
	}

	String strCanvasTexture = TEXT("HUDCanvasTex: ");
	strCanvasTexture += strName;
	TexturePtr ptrTexture = TextureManager::Instance().AddDynamicTexture(strCanvasTexture, size.x, size.y);
	MaterialPtr pMaterial = MaterialManager::Instance().CreateMaterialFromTexture(strCanvasTexture, ptrTexture);

	RenderableQuad* pHUDQuad = XNEW RenderableQuad(_pRenderSystem);
	pHUDQuad->Init(pMaterial, ptrTexture->GetWidth(), ptrTexture->GetHeight());

	HUDCanvas* pHUDCanvas = XNEW HUDCanvas(strName, NULL, pHUDQuad, ptrTexture);
	_movableObjectMap.insert(make_pair(strName, pHUDCanvas));
	return pHUDCanvas;
}

HUDBitmap* HUDStage::AddHUDBitmap(const String& strName, const String& strBitmapName)
{
	MovableObject2DMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end())
	{
		SAFE_CAST(it->second, HUDBitmap*);
		return (HUDBitmap*)it->second;
	}

	MaterialPtr pMaterial = MaterialManager::Instance().CreateMaterialFromBitmap(strBitmapName);

	if(!pMaterial)
	{
		XTHROWEX("HUDStage::AddHUDBitmap, CreateMaterialFromBitmap failed!");
	}

	TextureUnitState* pTexUnitState = pMaterial->GetTechnique(0)->GetPass(0)->GetTextureUnitState(0);
	TexturePtr pTexture = pTexUnitState->GetTexturePtr();

	RenderableQuad* pHUDQuad = XNEW RenderableQuad(_pRenderSystem);
	pHUDQuad->Init(pMaterial, pTexture->GetWidth(), pTexture->GetHeight());
	HUDBitmap* pHUDObject = XNEW HUDBitmap(strName, NULL, pHUDQuad);
	_movableObjectMap.insert(make_pair(strName, pHUDObject));
	return pHUDObject;
}

HUDBitmap* HUDStage::AddHUDBitmap(const String& strName, TexturePtr& pTexture)
{
	MovableObject2DMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end())
	{
		SAFE_CAST(it->second, HUDBitmap*);
		return (HUDBitmap*)it->second;
	}

	MaterialPtr pNewTexMtl(Material::Create());
	StringStream ss;
	ss << TEXT("HUDTexMtl:") << pTexture->GetName();
	pNewTexMtl->SetName(ss.str());
	BOOL bRet = pNewTexMtl->intMtlFromTexture(pTexture);

	if(!bRet)
	{
		XTHROWEX("HUDStage::AddHUDBitmap, CreateMaterialFromBitmap failed!");
	}

	RenderableQuad* pHUDQuad = XNEW RenderableQuad(_pRenderSystem);
	pHUDQuad->Init(pNewTexMtl, pTexture->GetWidth(), pTexture->GetHeight());
	HUDBitmap* pHUDObject = XNEW HUDBitmap(strName, NULL, pHUDQuad);
	_movableObjectMap.insert(make_pair(strName, pHUDObject));
	return pHUDObject;
}

Node2D* HUDStage::CreateHUDNode(const String& strNodeName)
{
	XASSERT(_allHUDNodes.find(strNodeName) == _allHUDNodes.end());
	Node2D* pHUDNode = XNEW Node2D(this, strNodeName);
	_allHUDNodes[strNodeName] = pHUDNode;

	return pHUDNode;
}

Node2D* HUDStage::GetHUDNode(const String& strNodeName)
{
	Node2DMapIt it = _allHUDNodes.find(strNodeName);
	if(it == _allHUDNodes.end()) return NULL;
	else return it->second;
}

void HUDStage::DestroyHUDNode(const String& strNodeName)
{
	Node2DMapIt it = _allHUDNodes.find(strNodeName);
	XASSERT(it != _allHUDNodes.end());

	// Make sure this node is not any other node' parent
#ifdef DEBUG
	Node2DMapIterator HUDNodeIterator(_allHUDNodes);
	while (HUDNodeIterator.HasMore())
	{
		XASSERT(HUDNodeIterator.CurrentData()->TreeNode::GetParent() != it->second);
	}

#endif		

	XDELETE it->second;
	_allHUDNodes.erase(it);
}

_X3D_NS_END