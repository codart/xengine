#pragma once

#include "Node2D.h"
#include "RenderableQuad.h"

_X3D_NS_BEGIN

class _X3DExport HUDBitmap : public MovableObject2D
{
public:
	HUDBitmap(const String& strName, Scene* pScene, RenderableQuad* pHUDQuad);	
	virtual ~HUDBitmap(void);


public:
	RectF 
		GetBoundingRect() const;

	void 
		SetSize(float fWidth, float fHeight);

	// ---
	override void
		fillRenderableObjList(RendableObjectList& renderableObjList);
	// Add code to update bounding rect
	override void
		setParentNode(Node2D* pParentNode);


protected:
	RenderableQuad*
		_pHUDQuad;

};


_X3D_NS_END