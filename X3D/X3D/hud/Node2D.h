#pragma once

#include "common/util/TreeNode.h"
#include "x3d/hud/MovableObject2D.h"

_X3D_NS_BEGIN

class HUDStage;

class _X3DExport Node2D : public TreeNode 
{
public:
	Node2D(HUDStage* pHUDStage);
	Node2D(HUDStage* pHUDStage, const String& strName);
	virtual ~Node2D(void);


public:
	virtual void
		AddChild(Node2D* pChild);
	virtual Node2D*
		GetParentNode() { return SAFE_CAST(_pParent, Node2D*); }

	virtual void
		SetPosition(const Vector2 vPosition);
	Vector2 
		GetPosition();

	virtual Vector2
		GetWorldPosition();

	//---
	final void 
		AttachObject(MovableObject2D* pMovableObject);
	final BOOL
		DetachObject(MovableObject2D* pMovableObject);
	final INLINE const MovableObject2DList&
		GetHUDObjectList(){ return _attachedObjList; }

	final const String&
		GetName() const { return _strName; }
	final void 
		SetName(const String& strName) { _strName = strName; }

	final MovableObject2DList& 
		GetMovableObject2DList() { return _attachedObjList; }

	// ---
	INLINE void 
		dirtyTransform();


protected:
	//----
	String 
		_strName;

	MovableObject2DList
		_attachedObjList;

	//-------
	HUDStage*
		_pHUDStage;

	Vector2
		_vPosition;
	Vector2
		_vScale;	
	Radian
		_angle;
	Boolean
		_bTransformDirty;

	Vector2 
		_cachedPosition;

protected:
	// Make these methods protected to avoid incorrect calling.
	INLINE void
		AddChild(TreeNode* pChild) { TreeNode::AddChild(pChild); }
	INLINE TreeNode* 
		GetParent() { return TreeNode::GetParent(); }
	INLINE void 
		setParent(TreeNode* pParent) { TreeNode::setParent(pParent); }


};

DECLARE_MAP_TYPE_PTR(map, String, Node2D);

_X3D_NS_END