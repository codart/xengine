#include "StdAfx.h"
#include "Node2D.h"

_X3D_NS_BEGIN

Node2D::Node2D(HUDStage* pHUDStage)
: _vPosition(0, 0)
, _vScale(1, 1)
, _angle(0)
{
	_pHUDStage = pHUDStage;
	_bTransformDirty = TRUE;
}

Node2D::Node2D(HUDStage* pHUDStage, const String& strName)
: _strName(strName)
, _vPosition(0, 0)
, _vScale(1, 1)
, _angle(0)
{
	_pHUDStage = pHUDStage;
	_bTransformDirty = TRUE;
}

Node2D::~Node2D(void)
{
}

void Node2D::AttachObject(MovableObject2D* pMovableObject)
{
	XASSERT(find(_attachedObjList.begin(), _attachedObjList.end(), pMovableObject) == _attachedObjList.end());
	pMovableObject->setParentNode(this);
	_attachedObjList.push_back(pMovableObject);
}

BOOL Node2D::DetachObject(MovableObject2D* pMovableObject)
{
	MovableObject2DListIt it = _attachedObjList.begin();
	MovableObject2DListIt itEnd = _attachedObjList.end();

	BOOL bFind = FALSE;
	for ( ; it != itEnd; ++it)
	{
		if(*it == pMovableObject)
		{
			bFind = TRUE;
			break;
		}
	}

	if(bFind) _attachedObjList.erase(it);

	return bFind;
}

void Node2D::AddChild(Node2D* pChild)
{
	TreeNode::AddChild(pChild);
}

void Node2D::SetPosition(const Vector2 vPosition)
{
	_vPosition = vPosition;
	dirtyTransform();
}

void Node2D::dirtyTransform()
{
	_bTransformDirty = TRUE;

	// Dirty all children
	FASTEST_VECTOR_ITERATE(TreeNode*, _children);
		Node2D* pHUDNode = SAFE_CAST(GET_NEXT(_children), Node2D*);
		pHUDNode->dirtyTransform();
	FASTEST_ITERATE_END();
}

Vector2 Node2D::GetPosition()
{
	return _vPosition;
}

Vector2 Node2D::GetWorldPosition()
{
	if(_bTransformDirty)
	{
		_bTransformDirty = FALSE;

		if(_pParent)
		{
			_cachedPosition = SAFE_CAST(_pParent, Node2D*)->GetPosition() + _vPosition;
			return _cachedPosition;
		}
		else
		{
			_cachedPosition = _vPosition;
			return _vPosition;
		}
	}
	else
	{
		return _cachedPosition;
	}
}


_X3D_NS_END