#include "StdAfx.h"
#include "HUDRender.h"

#include "x3d/render/RenderSystem.h"

_X3D_NS_BEGIN

HUDRender::HUDRender(RenderSystem* pRenderSystem, HUDStage* pHUDStage)
{
	_pRenderSystem = pRenderSystem;
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pHUDStage = pHUDStage;
}

HUDRender::~HUDRender(void)
{
}

void HUDRender::RenderHUDStage(HUDRenderOperationList& renderOPList
								   , RenderArea* pRenderArea
								   , RenderTarget* pRenderTarget)
{
	XASSERT(_pGraphicSystem && pRenderArea && pRenderTarget);

	_pCurrentRenderArea = pRenderArea;
	_pCurrentRenderTarget = pRenderTarget;

	HUDRenderContext renderContext(this, pRenderArea, pRenderTarget);

	// Update per-frame data of _autoParamDataProvider
	_autoParamDataProvider.SetCurrentRenderArea(_pCurrentRenderArea);

	FASTEST_VECTOR_ITERATE(HUDRenderOperation*, renderOPList);
		renderContext.pNextRenderOP = PEEK_NEXT(renderOPList);
		GET_NEXT(renderOPList)->Render(renderContext);
	FASTEST_ITERATE_END();

}

_X3D_NS_END