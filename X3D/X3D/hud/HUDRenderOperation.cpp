#include "StdAfx.h"
#include "HUDRenderOperation.h"

#include "x3d/hud/hudRender.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

HUDRenderOperation::HUDRenderOperation(RenderSystem* pRenderSystem)
{
	_pRenderSystem = pRenderSystem;
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();

}

HUDRenderOperation::~HUDRenderOperation(void)
{
}

void HUDRenderOperation::Render(HUDRenderContext& renderContext)
{
	_pRenderContext = &renderContext;

	_pGraphicSystem->SetRenderTarget(_pRenderContext->pDestRenderTarget);
	_pGraphicSystem->SetViewport(_pRenderContext->pDestRenderArea->GetViewportInfo());

	//_pGraphicSystem->ClearFrameBuffer(_pRenderContext->pDestRenderArea->GetBackgroundColor());

	XASSERT(_pRenderContext && _pRenderContext->pHUDRender);
	HUDStage* pHUDStage = renderContext.pHUDRender->GetHUDStage();

	RendableObjectList renderableObjList;
	pHUDStage->getRendableObjectList(renderableObjList);

	FASTEST_VECTOR_ITERATE(RendableObject*, renderableObjList);

		RendableObject* pRenderableObject = GET_NEXT(renderableObjList);

		_pRenderContext->pHUDRender->GetAutoParamProvider().SetCurrentRenderableObject(pRenderableObject);

		Technique* pTechnique = pRenderableObject->GetTechnique();

		PassList& passes = pTechnique->GetPasses();
		FASTEST_VECTOR_ITERATE(Pass*, passes);
			_setPass(GET_NEXT(passes));
			pRenderableObject->UpdateTransform(*_pRenderContext->pDestRenderArea->GetViewportInfo());
			_pGraphicSystem->DrawPrimitive(pRenderableObject->GetDrawPrimitiveCmd());
		FASTEST_ITERATE_END();

	FASTEST_ITERATE_END();

}

void HUDRenderOperation::_setPass(Pass* pPass)
{
	// Set pass basic states
	_pGraphicSystem->SetSceneBlending( pPass->GetSrcBlendFactor()
		, pPass->GetDestBlendFactor()
		, pPass->GetFrameBlendOperation() );

	_pGraphicSystem->SetCullMode(pPass->GetHardwareCullMode());
	_pGraphicSystem->SetDepthBufferParams(pPass->GetDepthEnable(), pPass->GetDepthWrite()
		, pPass->GetDepthCompareFunction());

	_pGraphicSystem->EnableColorBufferWrite(pPass->GetColorBufferWrite());

	_pGraphicSystem->SetTextureUnitStates(pPass->GetTextureUnitStates());

	_pGraphicSystem->SetVertexTextureUnitStates(pPass->GetVertexTextureUnitStates());

	//... Set shader constant
	XASSERT(_pRenderContext && _pRenderContext->pHUDRender);
	Shader* pShader = pPass->GetShader();
	XASSERT(pShader->GetVertexProgram());
	AutoParameterDataProvider& autoParamProvider = _pRenderContext->pHUDRender->GetAutoParamProvider();
	pShader->GetVertexProgram()->UpdateAutoParameters(&autoParamProvider);
	XASSERT(pShader->GetFragmentProgram());
	pShader->GetFragmentProgram()->UpdateAutoParameters(&autoParamProvider);

	_pGraphicSystem->SetShader(pShader);
}


_X3D_NS_END