#pragma once

#include "MovableObject2D.h"

_X3D_NS_BEGIN

class TextAreaElement : public MovableObject2D
{
public:
	TextAreaElement(Scene* pScene);
	virtual ~TextAreaElement(void);
};

_X3D_NS_END