#pragma once

#include "x3d/render/AutoParameterDataProvider.h"
#include "x3d/hud/HUDRenderOperation.h"

_X3D_NS_BEGIN

class HUDRender
{
public:
	HUDRender(RenderSystem* pRenderSystem, HUDStage* pHUDStage);
	virtual ~HUDRender(void);


public:
	virtual void 
		RenderHUDStage(HUDRenderOperationList& renderOPList
						, RenderArea* pRenderArea
						, RenderTarget* pRenderTarget);

	INLINE HUDStage*
		GetHUDStage() {return _pHUDStage; }
	INLINE AutoParameterDataProvider&
		GetAutoParamProvider() { return _autoParamDataProvider; }


protected:
	RenderSystem*
		_pRenderSystem;
	GraphicSystem*
		_pGraphicSystem;

	// The stage that this render belongs to.
	HUDStage*
		_pHUDStage;

	AutoParameterDataProvider
		_autoParamDataProvider;

	//---
	mutable RenderArea*
		_pCurrentRenderArea;
	mutable RenderTarget*
		_pCurrentRenderTarget;


};

_X3D_NS_END