#include "StdAfx.h"
#include "HUDBitmap.h"

_X3D_NS_BEGIN

HUDBitmap::HUDBitmap(const String& strName, Scene* pScene, RenderableQuad* pHUDQuad)
: MovableObject2D(strName, pScene)
{
	XASSERT(pHUDQuad);
	_pHUDQuad = pHUDQuad;

	_pHUDQuad->setParentHUDObject(this);
}

HUDBitmap::~HUDBitmap(void)
{
	//...
	SAFE_DELETE(_pHUDQuad);
}

void HUDBitmap::SetSize(float fWidth, float fHeight)
{
	XASSERT(_pHUDQuad);
	_pHUDQuad->SetSize(fWidth, fHeight);
}

void HUDBitmap::fillRenderableObjList(RendableObjectList& renderableObjList)
{
	renderableObjList.push_back(_pHUDQuad);
}

void HUDBitmap::setParentNode(Node2D* pParentNode)
{
	MovableObject2D::setParentNode(pParentNode);

	// Update bounding rect ( or add a flag _transformDirty = true, then update the bounding rect the first time
	// getBoundingRect be called???)

}

RectF HUDBitmap::GetBoundingRect() const
{
	RectF boundingRect;
	if(_pParentNode)
	{
		Vector2 vPos = GetPosition();
		boundingRect.SetRect(vPos.x, vPos.y, vPos.x + _pHUDQuad->GetWidth(), vPos.y + _pHUDQuad->GetHeight());
	}
	else
	{
		boundingRect.SetRect(0, 0, 0, 0);
	}
	return boundingRect;
}

_X3D_NS_END