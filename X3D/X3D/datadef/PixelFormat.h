#pragma once


enum PixelFormat
{
	/// Unknown pixel format.
	PIXEL_FORMAT_UNKNOWN = 0,

	/// 24-bit pixel format, 8 bits for blue, green and red.
	PIXEL_FORMAT_B8G8R8 = 20,
	/// 32-bit pixel format, 8 bits for alpha, red, green and blue.
	PIXEL_FORMAT_A8R8G8B8 = 21,
	/// 32-bit pixel format, 8 bits for red, 8 bits for green, 8 bits for blue
	/// like PIXEL_FORMAT_A8R8G8B8, but alpha will get discarded
	PIXEL_FORMAT_X8R8G8B8 = 22,

	// 32-bit pixel format, 32 bits (float) for red
	PIXEL_FORMAT_FLOAT32_R = 114,

	// Depth buffer format, with 24 bit for depth 8 bit for stencil
	PIXEL_FORMAT_D24S8 = 75,

	PIXEL_FORMAT_D16 = 80,

	PIXEL_FORMAT_A16B16G16R16F = 113,

/*

	/// 8-bit pixel format, all bits luminace.
	PIXEL_FORMAT_L8 = 1,
	PIXEL_FORMAT_BYTE_L = PIXEL_FORMAT_L8,
	/// 16-bit pixel format, all bits luminace.
	PIXEL_FORMAT_L16 = 2,
	PIXEL_FORMAT_SHORT_L = PIXEL_FORMAT_L16,
	/// 8-bit pixel format, all bits alpha.
	PIXEL_FORMAT_A8 = 3,
	PIXEL_FORMAT_BYTE_A = PIXEL_FORMAT_A8,
	/// 8-bit pixel format, 4 bits alpha, 4 bits luminance.
	PIXEL_FORMAT_A4L4 = 4,
	/// 2 byte pixel format, 1 byte luminance, 1 byte alpha
	PIXEL_FORMAT_BYTE_LA = 5,
	/// 16-bit pixel format, 5 bits red, 6 bits green, 5 bits blue.
	PIXEL_FORMAT_R5G6B5 = 6,
	/// 16-bit pixel format, 5 bits red, 6 bits green, 5 bits blue.
	PIXEL_FORMAT_B5G6R5 = 7,
	/// 8-bit pixel format, 2 bits blue, 3 bits green, 3 bits red.
	PIXEL_FORMAT_R3G3B2 = 31,
	/// 16-bit pixel format, 4 bits for alpha, red, green and blue.
	PIXEL_FORMAT_A4R4G4B4 = 8,
	/// 16-bit pixel format, 5 bits for blue, green, red and 1 for alpha.
	PIXEL_FORMAT_A1R5G5B5 = 9,
	/// 32-bit pixel format, 8 bits for blue, green, red and alpha.
	PIXEL_FORMAT_A8B8G8R8 = 13,
	/// 32-bit pixel format, 8 bits for blue, green, red and alpha.
	PIXEL_FORMAT_B8G8R8A8 = 14,
	/// 32-bit pixel format, 8 bits for red, green, blue and alpha.
	PIXEL_FORMAT_R8G8B8A8 = 28,
	/// 32-bit pixel format, 8 bits for blue, 8 bits for green, 8 bits for red
	/// like PIXEL_FORMAT_A8B8G8R8, but alpha will get discarded
	PIXEL_FORMAT_X8B8G8R8 = 27,

//#if XPLANTFORM_ENDIAN == XPLANTFORM_ENDIAN_BIG
	/// 3 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue
	PIXEL_FORMAT_BYTE_RGB = PIXEL_FORMAT_R8G8B8,
	/// 3 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red
	PIXEL_FORMAT_BYTE_BGR = PIXEL_FORMAT_B8G8R8,
	/// 4 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red and one byte for alpha
	PIXEL_FORMAT_BYTE_BGRA = PIXEL_FORMAT_B8G8R8A8,
	/// 4 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue, and one byte for alpha
	PIXEL_FORMAT_BYTE_RGBA = PIXEL_FORMAT_R8G8B8A8,
/ *
#else
	/// 3 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue
	PIXEL_FORMAT_BYTE_RGB = PIXEL_FORMAT_B8G8R8,
	/// 3 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red
	PIXEL_FORMAT_BYTE_BGR = PIXEL_FORMAT_R8G8B8,
	/// 4 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red and one byte for alpha
	PIXEL_FORMAT_BYTE_BGRA = PIXEL_FORMAT_A8R8G8B8,
	/// 4 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue, and one byte for alpha
	PIXEL_FORMAT_BYTE_RGBA = PIXEL_FORMAT_A8B8G8R8,
#endif        
* /
	/// 32-bit pixel format, 2 bits for alpha, 10 bits for red, green and blue.
	PIXEL_FORMAT_A2R10G10B10 = 15,
	/// 32-bit pixel format, 10 bits for blue, green and red, 2 bits for alpha.
	PIXEL_FORMAT_A2B10G10R10 = 16,
	/// DDS (DirectDraw Surface) DXT1 format
	PIXEL_FORMAT_DXT1 = 17,
	/// DDS (DirectDraw Surface) DXT2 format
	PIXEL_FORMAT_DXT2 = 18,
	/// DDS (DirectDraw Surface) DXT3 format
	PIXEL_FORMAT_DXT3 = 19,
	/// DDS (DirectDraw Surface) DXT4 format
	PIXEL_FORMAT_DXT4 = 20,
	/// DDS (DirectDraw Surface) DXT5 format
	PIXEL_FORMAT_DXT5 = 21,
	// 16-bit pixel format, 16 bits (float) for red
	PIXEL_FORMAT_FLOAT16_R = 32,
	// 48-bit pixel format, 16 bits (float) for red, 16 bits (float) for green, 16 bits (float) for blue
	PIXEL_FORMAT_FLOAT16_RGB = 22,
	// 64-bit pixel format, 16 bits (float) for red, 16 bits (float) for green, 16 bits (float) for blue, 16 bits (float) for alpha
	PIXEL_FORMAT_FLOAT16_RGBA = 23,
	// 96-bit pixel format, 32 bits (float) for red, 32 bits (float) for green, 32 bits (float) for blue
	PIXEL_FORMAT_FLOAT32_RGB = 24,
	// 128-bit pixel format, 32 bits (float) for red, 32 bits (float) for green, 32 bits (float) for blue, 32 bits (float) for alpha
	PIXEL_FORMAT_FLOAT32_RGBA = 25,
	// 32-bit, 2-channel s10e5 floating point pixel format, 16-bit green, 16-bit red
	PIXEL_FORMAT_FLOAT16_GR = 35,
	// 64-bit, 2-channel floating point pixel format, 32-bit green, 32-bit red
	PIXEL_FORMAT_FLOAT32_GR = 36,
	// Depth texture format
	PIXEL_FORMAT_DEPTH = 29,
	// 64-bit pixel format, 16 bits for red, green, blue and alpha
	PIXEL_FORMAT_SHORT_RGBA = 30,
	// 32-bit pixel format, 16-bit green, 16-bit red
	PIXEL_FORMAT_SHORT_GR = 34,
	// 48-bit pixel format, 16 bits for red, green and blue
	PIXEL_FORMAT_SHORT_RGB = 37,
	/// PVRTC (PowerVR) RGB 2 bpp
	PIXEL_FORMAT_PVRTC_RGB2 = 38,
	/// PVRTC (PowerVR) RGBA 2 bpp
	PIXEL_FORMAT_PVRTC_RGBA2 = 39,
	/// PVRTC (PowerVR) RGB 4 bpp
	PIXEL_FORMAT_PVRTC_RGB4 = 40,
	/// PVRTC (PowerVR) RGBA 4 bpp
	PIXEL_FORMAT_PVRTC_RGBA4 = 41,
	// Number of pixel formats currently defined
	PIXEL_FORMAT_COUNT = 42
*/
};