#pragma once

_X3D_NS_BEGIN

struct FaceIndices
{
	USHORT index1;
	USHORT index2;
	USHORT index3;
};

class Face
{
public:
	Face(DWORD index1, DWORD index2, DWORD index3)
	{
		indices.index1 = index1;
		indices.index2 = index2;
		indices.index3 = index3;
	}

public:
	FaceIndices
		indices;

};

DECLARE_LIST_TYPE_PTR(vector, Face);

//////////////////////////////////////////////////////////////////////////

struct LineIndices
{
	USHORT index1;
	USHORT index2;
};


class Line
{
public:
	Line(DWORD index1, DWORD index2)
	{
		indices.index1 = index1;
		indices.index2 = index2;
	}

public:
	LineIndices
		indices;
};

DECLARE_LIST_TYPE_PTR(vector, Line);

//////////////////////////////////////////////////////////////////////////
class Vertex
{
public:
	Vertex()
	{
		memset(this, 0, sizeof(Vertex));
	}

public:
	Vector3 position;
	Vector3 normal;
	Vector2 uv0;
	Vector4 diffuse;
	float weights[4];
	BYTE indices[4];

	DWORD flag;
};

DECLARE_LIST_TYPE_PTR(vector, Vertex);

_X3D_NS_END