#pragma once

_X3D_NS_BEGIN

// Self transformed vertex
struct Vertex2DST
{
	Vector3 position; 

	// Below is a 3x3 matrix (separate stored into three vector3)
	Vector3 row1;
	Vector3 row2;
	Vector3 row3;

	Vector4 color;

};

struct QuadRect
{
	Vertex2DST		topLeft;
	Vertex2DST		topRight;
	Vertex2DST		bottomRight;
	Vertex2DST		bottomLeft;
};

struct Quad2DST
{
	QuadRect rect;
};

struct QuadVertex2D
{
	Vector3 position;
	Vector2 uv1;
};

//////////////////////////////////////////////////////////////////////////
struct Vertex_P_N
{
	Vector3
		position;
	Vector4
		normal;
};

// Position, normal and UV0
struct Vertex_P_N_UV
{
	Vector3
		position;
	Vector3
		normal;
	Vector2
		uv;
};

struct Vertex_P_N_UV_T
{
public:
	Vector3
		position;
	Vector3
		normal;
	Vector2
		uv;

	Vector3
		tangent;

};

// Position and diffuse color
struct Vertex_P_D
{
	Vector3
		position;
	Vector4
		color;
};

_X3D_NS_END
