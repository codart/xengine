#pragma once


_X3D_NS_BEGIN

class RenderAreaEvent : public Event
{
public:
	enum RenderAreaEventType
	{
		RENDER_AREA_EVENT_START = 2000,
		SIZE_CHANGED,
		RENDER_AREA_EVENT_END,
	};

	RenderAreaEvent(int nType, const ViewportInfo& viewportRegion) : Event(nType), region(viewportRegion)
	{}

	static BOOL IsValidEvent(int nType)
	{
		return nType > RENDER_AREA_EVENT_START && nType < RENDER_AREA_EVENT_END;
	}

public:
	const ViewportInfo& 
		region;

};

_X3D_NS_END