#include "StdAfx.h"
#include "Node3D.h"

_X3D_NS_BEGIN

Node3D::Node3D(const Node3D& node3D)
{
	_vPosition = node3D._vPosition;
	_vScale = node3D._vScale;
	_qOrientation = node3D._qOrientation;

	_bTransformDirty = true;
	_bPositionDirty = true;
	_bOrientationDirty = true;
	_bScaleDirty = true;
}

Node3D::Node3D(const String& strName)
: _strName(strName)
, _vPosition(Vector3::ZERO)
, _vScale(Vector3::UNIT_SCALE)
, _qOrientation(Quaternion::IDENTITY)
{
	_bTransformDirty = TRUE;
	_bOrientationDirty = TRUE;
	_bPositionDirty = TRUE;
	_bScaleDirty = TRUE;

}

Node3D::~Node3D(void)
{
}

void Node3D::AttachObject(MovableObject* pMovableObject)
{
	XASSERT(find(_attachedObjList.begin(), _attachedObjList.end(), pMovableObject) == _attachedObjList.end());
	pMovableObject->setParentNode(this);
	_attachedObjList.push_back(pMovableObject);
}

BOOL Node3D::DetachObject(MovableObject* pMovableObject)
{
	MovableObjectListIt it = _attachedObjList.begin();
	MovableObjectListIt itEnd = _attachedObjList.end();

	BOOL bFind = FALSE;
	for ( ; it != itEnd; ++it)
	{
		if(*it == pMovableObject)
		{
			bFind = TRUE;
			break;
		}
	}

	if(bFind) _attachedObjList.erase(it);

	return bFind;
}

void Node3D::SetPosition(const Vector3 vPosition)
{
	_vPosition = vPosition;

	_handlePositionChange();
}

void Node3D::SetOrientation(const Quaternion& qRotation)
{
	_qOrientation = qRotation;
	_qOrientation.Normalise();

	_handleOrientationChange();
}

void Node3D::Pitch(Radian rAngle)
{
	Quaternion quaternion;
	quaternion.FromAngleAxis(rAngle, Vector3::UNIT_X);

	Rotate(quaternion);
}

void Node3D::Yaw(Radian rAngle)
{
	Quaternion quaternion;
	quaternion.FromAngleAxis(rAngle, Vector3::UNIT_Y);

	Rotate(quaternion);
}

void Node3D::Roll(Radian rAngle)
{
	Quaternion quaternion;
	quaternion.FromAngleAxis(rAngle, Vector3::UNIT_Z);

	Rotate(quaternion);
}

void Node3D::Rotate(const Quaternion& qDeltaRotation)
{
	Quaternion qNormalized = qDeltaRotation;
	qNormalized.Normalise();

	_qOrientation *= qNormalized;

	_handleOrientationChange();
}

void Node3D::SetScale(const Vector3& scale)
{
	XASSERT(!scale.IsNaN() && "Invalid vector supplied as parameter");

	_vScale = scale;

	_handleScaleChange();
}

void Node3D::Translate(const Vector3& vDistance)
{
	_vPosition += vDistance;

	_handlePositionChange();
}

void Node3D::Up(float fDistance)
{
	_vPosition += Vector3(0, fDistance, 0);
	_handlePositionChange();
}

void Node3D::Down(float fDistance)
{
	_vPosition += Vector3(0, -fDistance, 0);
	_handlePositionChange();
}

void Node3D::Left(float fDistance)
{
	_vPosition += Vector3(fDistance, 0, 0);
	_handlePositionChange();
}

void Node3D::Right(float fDistance)
{
	_vPosition += Vector3(-fDistance, 0, 0);
	_handlePositionChange();
}

void Node3D::Forward(float fDistance)
{
	_vPosition += Vector3(0, 0, fDistance);
	_handlePositionChange();	
}

void Node3D::Backward(float fDistance)
{
	_vPosition += Vector3(0, 0, -fDistance);
	_handlePositionChange();	
}

void Node3D::fillRenderQueueR(RenderQueue* pRenderQueue)
{
	FASTEST_VECTOR_ITERATE(MovableObject*, _attachedObjList);
		MovableObject* pMovableObj = GET_NEXT(_attachedObjList);
		pMovableObj->fillRenderQueue(pRenderQueue);
	FASTEST_ITERATE_END();

	// Traverse children
	FASTEST_VECTOR_ITERATE(Node3D*, _children);
		GET_NEXT(_children)->fillRenderQueueR(pRenderQueue);
	FASTEST_ITERATE_END();
}

void Node3D::_handleTransformChange()
{
	_bTransformDirty = TRUE;
	_bPositionDirty = TRUE;
	_bOrientationDirty = TRUE;

	FASTEST_VECTOR_ITERATE(Node3D*, _children);
		Node3D* pHierarchyNode = GET_NEXT(_children);
		pHierarchyNode->_handleTransformChange();
	FASTEST_ITERATE_END();
}

void Node3D::_handlePositionChange()
{
	_bTransformDirty = TRUE;
	_bPositionDirty = TRUE;

	FASTEST_VECTOR_ITERATE(Node3D*, _children);
		Node3D* pHierarchyNode = GET_NEXT(_children);
		pHierarchyNode->_handlePositionChange();
	FASTEST_ITERATE_END();
}

void Node3D::_handleScaleChange()
{
	_bTransformDirty = TRUE;
	_bScaleDirty = TRUE;
	_bPositionDirty = TRUE;

	FASTEST_VECTOR_ITERATE(Node3D*, _children);
		Node3D* pHierarchyNode = GET_NEXT(_children);
		pHierarchyNode->_handleScaleChange();
	FASTEST_ITERATE_END();
}

void Node3D::_handleOrientationChange()
{
	_bTransformDirty = TRUE;
	_bOrientationDirty = TRUE;
	_bPositionDirty = TRUE;

	FASTEST_VECTOR_ITERATE(Node3D*,  _children);
		Node3D* pHierarchyNode = GET_NEXT(_children);
		pHierarchyNode->_handleOrientationChange();
	FASTEST_ITERATE_END();
}

const Matrix4* Node3D::GetDerivedTransform(void) const
{
	if(_bTransformDirty)
	{
		_cachedDerivedTransform.MakeTransform(_vPosition, _vScale, _qOrientation);
		if(_pParent)
		{
			const Matrix4* pParentTransform = SAFE_CAST(_pParent, Node3D*)->GetDerivedTransform();
			_cachedDerivedTransform = _cachedDerivedTransform * (*pParentTransform);
		}
		_bTransformDirty = FALSE;
	}
	return &_cachedDerivedTransform;	
}

const Quaternion& Node3D::GetDerivedOrientation() const
{
	if(_bOrientationDirty)
	{	
		if(_pParent)
		{
			Node3D* pParent = SAFE_CAST(_pParent, Node3D*);
			_cachedDerivedOrientation = _qOrientation * pParent->GetDerivedOrientation();
		}
		else
		{
			_cachedDerivedOrientation = _qOrientation;
		}
		_bOrientationDirty = FALSE;
	}	
	return _cachedDerivedOrientation;	
}

const Vector3& Node3D::GetDerivedPosition() const
{
	if(_bPositionDirty)
	{
		if(_pParent)
		{
			Node3D* pParent = SAFE_CAST(_pParent, Node3D*);
			_cachedDerivedPosition = pParent->GetDerivedPosition() +
									(_vPosition * pParent->GetDerivedScale()) * pParent->GetDerivedOrientation();
		}
		else
		{
			_cachedDerivedPosition = _vPosition;
		}
		_bPositionDirty = FALSE;
	}

	return _cachedDerivedPosition;	
}

const Vector3& Node3D::GetDerivedScale() const
{
	if(_bScaleDirty)
	{
		if(_pParent)
		{
			Node3D* pParent = SAFE_CAST(_pParent, Node3D*);
			_cachedDerivedScale = _vScale * pParent->GetDerivedScale();
		}
		else
		{
			_cachedDerivedScale = _vScale;
		}
		_bScaleDirty = FALSE;
	}

	return _cachedDerivedScale;
}


_X3D_NS_END