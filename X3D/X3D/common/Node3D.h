#pragma once

#include "common/util/TreeNode.h"
#include "x3d/scene/Node3DVisitor.h"

_X3D_NS_BEGIN

/// Node3D represent a 3D space node to which MovableObject can be attached.
class _X3DExport Node3D : public TreeNode
{
public:
	Node3D(const Node3D& node3D);
	Node3D(const String& strName);
	virtual ~Node3D(void);


public:
	final void 
		SetPosition(const Vector3 vPosition);
	final INLINE const Vector3&
		GetPosition() { return _vPosition; }
	final void 
		SetScale(const Vector3& scale);
	final INLINE Vector3&
		GetScale() { return _vScale; }
	final void
		SetOrientation(const Quaternion& qRotation);
	final INLINE const Quaternion&
		GetOrientation() { return _qOrientation; }

	final void 
		Translate(const Vector3& vDistance);
	final void
		Up(float fDistance);
	final void
		Down(float fDistance);
	final void
		Left(float fDistance);
	final void
		Right(float fDistance);
	final void
		Forward(float fDistance);
	final void
		Backward(float fDistance);

	final void 
		Pitch(Radian angle); // rotate on x
	final void 
		Yaw(Radian angle);   // rotate on y
	final void 
		Roll(Radian angle);  // rotate on z

	final void
		Rotate(const Quaternion& qDeltaRotation);

	final const Matrix4*
		GetDerivedTransform(void) const;
	final const Quaternion&
		GetDerivedOrientation() const;
	final const Vector3&
		GetDerivedPosition() const;
	final const Vector3&
		GetDerivedScale() const;

	//---
	final void 
		AttachObject(MovableObject* pMovableObject);
	final BOOL
		DetachObject(MovableObject* pMovableObject);
	final INLINE const MovableObjectList&
		GetMovableObjectList(){ return _attachedObjList; }

	final const String&
		GetName() const { return _strName; }
	final void 
		SetName(const String& strName) { _strName = strName; }

	/// Accept a visitor, let it to visit this node recursively.
	final INLINE void 
		AcceptVisitor(Node3DVisitor* pVisitor)
	{
		FASTEST_VECTOR_ITERATE(Node3D*, _children);
			GET_NEXT(_children)->AcceptVisitor(pVisitor);
		FASTEST_ITERATE_END();

		pVisitor->Visit(this);
	}	

	//-------------
	/// Fill attached movable objects into render queue.
	final INLINE void 
		fillRenderQueue(RenderQueue* pRenderQueue)
	{
		FASTEST_VECTOR_ITERATE(MovableObject*, _attachedObjList);
			MovableObject* pMovableObj = GET_NEXT(_attachedObjList);
			pMovableObj->fillRenderQueue(pRenderQueue);
		FASTEST_ITERATE_END();
	}

	/// Fill attached movable objects into render queue recursively.
	final void 
		fillRenderQueueR(RenderQueue* pRenderQueue);


protected:
	//----
	String 
		_strName;

	MovableObjectList
		_attachedObjList;

	// --- For transformation ---
	Vector3
		_vPosition;
	Vector3
		_vScale;
	Quaternion
		_qOrientation;

	mutable Boolean  
		_bTransformDirty;
	mutable Boolean
		_bPositionDirty;
	mutable Boolean
		_bOrientationDirty;
	mutable Boolean
		_bScaleDirty;

	// --- Cached transformation data ---
	mutable Matrix4
		_cachedDerivedTransform;
	mutable Quaternion
		_cachedDerivedOrientation;
	mutable Vector3
		_cachedDerivedPosition;
	mutable Vector3
		_cachedDerivedScale;


protected:
	// Override this method to do custom AcceptVisitor behavior.
	virtual void 
		_onUpdate() { }

	void
		_updateChildren();
	void 
		_handleTransformChange();
	void 
		_handlePositionChange();
	void 
		_handleScaleChange();
	void 
		_handleOrientationChange();


};


_X3D_NS_END