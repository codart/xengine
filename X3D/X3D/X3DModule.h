#pragma once

#include "Common/core/EventDispatcher.h"
#include "Common/core/IModule.h"
#include "x3d/RenderWindow.h"
#include "x3d/scene/Camera.h"
#include "x3d/scene/Scene.h"
#include "x3d/PlugableModule.h"
#include "x3d/model/Model.h"
#include "x3d/hud/HUDStage.h"

_COMMON_NS_BEGIN

class ScriptBinder;

_COMMON_NS_END

_X3D_NS_BEGIN

enum PlugableModuleType
{
	PLUGIN_GRAPHIC_SYS,
	PLUGIN_SCENE_RENDER,

	PLUGIN_BAKING_SYS,
	PLUGIN_PHYSICS_SYS,
};

/// Utility macros ///
#define _GraphicSystem	(GetX3DModule()->getGraphicSystem())
#define _RenderSystem	(GetX3DModule()->getRenderSystem())

class GraphicSystem;
class RenderSystem;

class X3DModule : implements IModule
				 , public EventDispatcher
{
public:
	X3DModule(DWORD dwUserData);
	virtual ~X3DModule(void);


public:
	virtual void
		RunScriptString(const AString& strScript);
	virtual ScriptBinder*
		GetScriptBinder() { return _pScriptBinder; }


	// Implements IModule
	override void 
		Init(const Event* pEvent);
	override void 
		Tick(const Event* pEvent);
	override void 
		Uninit(const Event* pEvent);

	//virtual void
	//	Quit();


	virtual DWORD_PTR
		GetModuleHandle(){return _hModuleHandle;}

	//---
	virtual ScenePtr
		AddScene(const String& strSceneName);
	virtual void
		RemoveScene(const String& strSceneName);
	virtual ScenePtr
		GetScene(const String& strSceneName);
	virtual void 
		ClearAllScene();
	virtual void 
		RemoveAllScene();

	//---
	virtual HUDStagePtr
		AddHUDStage(const String& strHUDStageName);
	virtual HUDStagePtr
		GetHUDStage(const String& strStageName);
	void
		ClearAllHUDStage();
	void 
		RemnoveAllHUDStage();

	//---
	virtual RenderWindow*
		AddRenderWindow(const String& strWindowName, const Rect& rect = Rect(200, 200, 700, 580));
	virtual void
		RemoveRenderWindow(const String& strWindowName);
	virtual void 
		RemoveAllRenderWindow();
	virtual RenderWindow* 
		GetRenderWindow(const String& strWindowName);

	//---
	virtual CameraPtr
		AddCamera(const String& strCameraName);
	virtual void
		RemoveCamera(const String& strCameraName);
	virtual CameraPtr
		GetCamera(const String& strCameraName);

	//----------------
	INLINE GraphicSystem*
		getGraphicSystem() { XASSERT(_pGraphicSystem); return _pGraphicSystem; }
	virtual void
		installGraphicSystem(GraphicSystem* pGraphicSystem);
	virtual GraphicSystem*
		uninstallGraphicSystem();

	INLINE RenderSystem*
		getRenderSystem() { XASSERT(_pRenderSystem); return _pRenderSystem; }
	

protected:
	typedef OwnerPtr<ScenePtr> SceneOwnerPtr;
	typedef map< String, SceneOwnerPtr> SceneOwnerMap;
	typedef SceneOwnerMap::iterator SceneOwnerMapIter;

	typedef OwnerPtr<HUDStagePtr> HUDStageOwnerPtr;
	typedef map< String, HUDStageOwnerPtr > HUDStageOwnerMap;
	typedef HUDStageOwnerMap::iterator HUDStageOwnerMapIter;

	typedef OwnerPtr<CameraPtr> CameraOwnerPtr;
	typedef map< String, CameraOwnerPtr > CameraOwnerMap;
	typedef CameraOwnerMap::iterator CameraOwnerMapIter;

	typedef OwnerPtr<RenderWindowPtr> RenderWindowOwnerPtr;
	typedef map< String, RenderWindowOwnerPtr > RenderWindowOwnerMap;
	typedef RenderWindowOwnerMap::iterator RenderWindowOwnerMapIter;

	RenderWindowOwnerMap
		_renderWindowMap;
	SceneOwnerMap
		_sceneMap;
	HUDStageOwnerMap
		_HUDStageMap;
	CameraOwnerMap
		_cameraMap;

	PlugableModuleList
		_plugableModules;

	//EventDispatcher*
	//	_pEventDispatcher;
	DWORD_PTR
		_hModuleHandle;

	Ptr<GraphicSystem>
		_pGraphicSystem;
	Ptr<RenderSystem>
		_pRenderSystem;

	ScriptBinder*
		_pScriptBinder;


protected:
	INLINE void 
		_updateAllRenderWindow();

	INLINE void 
		_loadGraphicSystemModule();
	INLINE void 
		_unloadPlugableModules();

	// Script support
	virtual RenderWindow*
		GetRenderWindow(const char* pszWindowName);


};


_X3D_NS_END