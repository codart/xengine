#pragma once
#include "common/core/event.h"

_X3D_NS_BEGIN

class _X3DExport X3DModuleEvent : public Event
{
public:
	enum FrameEventType
	{
		FRAME_EVENT_BASE = 300,
		
		ENGINE_INIT,
		ENGINE_INITED,
		ENGINE_DESTROY,

		//..FRAME_START, // Use CoreEvent::CORE_FRAME_TCIK instead
		//..FRAME_END, //// Use CoreEvent::CORE_FRAME_TCIK instead
	};

	X3DModuleEvent(int nType):Event(nType)
	{

	}
};

_X3D_NS_END