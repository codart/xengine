#include "StdAfx.h"
#include "RenderStateNode.h"

_X3D_NS_BEGIN

RenderStateNode::RenderStateNode(void)
{
}

RenderStateNode::~RenderStateNode(void)
{
}

void RenderStateNode::Render()
{
	_enterState();
	
	_renderAllRenderables();

	FASTEST_VECTOR_ITERATE(TreeNode*, _children);
		RenderStateNode* pRenderStateNode = SAFE_CAST(GET_NEXT(_children), RenderStateNode*);
		pRenderStateNode->Render();
	FASTEST_ITERATE_END();

	_leaveState();
}

void RenderStateNode::_renderAllRenderables()
{

}

void RenderStateNode::_enterState()
{

}

void RenderStateNode::_leaveState()
{

}

_X3D_NS_END