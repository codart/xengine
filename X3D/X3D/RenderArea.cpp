#include "StdAfx.h"
#include "RenderArea.h"

#include "Common/plantform/WindowEvent.h"

#include "x3d/scene/Camera.h"
#include "x3d/graphic/WindowRenderTarget.h"
#include "x3d/render/RenderOperation.h"
#include "x3d/render/SceneRender.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/HUD/HUDStage.h"
#include "x3d/HUD/HUDRender.h"

_X3D_NS_BEGIN

RenderArea::RenderArea(RenderSystem* pRenderSystem, RenderWindow* pRenderWindow
						, CameraPtr pAttachedCamera, ScenePtr pAttachedScene
						, const Rect& rect, int nZOrder)
{
	_pRenderSystem = pRenderSystem;

	_pRenderWindow = pRenderWindow;
	_pAttachedCamera = pAttachedCamera;
	_pAttachedScene = pAttachedScene;
	_pAttachedHUDStage = NULL;

	_nZOrder = nZOrder;
	_backgroundColor = D3DCOLOR_XRGB(255, 255, 255);

	_pAttachedCamera->SetAspectRatio((float)rect.Width()/(float)rect.Height());

	__bAutoAspectRatio = TRUE;
	AddEventListener(RenderAreaEvent::SIZE_CHANGED
						, MFUNC(&Camera::_onRenderAreaSizeChanged, _pAttachedCamera));

	_viewportInfo.X = rect.left;
	_viewportInfo.Y = rect.top;
	_viewportInfo.Width = rect.Width();
	_viewportInfo.Height = rect.Height();
	_viewportInfo.MinZ = 0;
	_viewportInfo.MaxZ = 1;

	//---
	__fX = (float)_viewportInfo.X / (float)_pRenderWindow->GetWidth();
	__fY = (float)_viewportInfo.Y / (float)_pRenderWindow->GetHeight();
	__fWidth = (float)_viewportInfo.Width / (float)_pRenderWindow->GetWidth();
	__fHeight = (float)_viewportInfo.Height / (float)_pRenderWindow->GetHeight();

}

RenderArea::~RenderArea(void)
{
	RemoveEventListener(RenderAreaEvent::SIZE_CHANGED
							, MFUNC(&Camera::_onRenderAreaSizeChanged, _pAttachedCamera));


	// Release all render operations
	RenderOperationIterator renderOPIterator(_renderOPList);
	while (renderOPIterator.HasMore())
	{
		XDELETE renderOPIterator.CurrentData();
		renderOPIterator.Next();
	}

	// Release all HUD render operations
	HUDRenderOperationIterator HUDRenderOPIterator(_HUDRenderOPList);
	while (HUDRenderOPIterator.HasMore())
	{
		XDELETE HUDRenderOPIterator.CurrentData();
		HUDRenderOPIterator.Next();
	}
}

void RenderArea::SetAutoAspectRatio(BOOL bAutoAspectRatio) 
{
	if(__bAutoAspectRatio == bAutoAspectRatio) return;

	__bAutoAspectRatio = bAutoAspectRatio; 
	if(!__bAutoAspectRatio)
	{
		RemoveEventListener(RenderAreaEvent::SIZE_CHANGED
							, MFUNC(&Camera::_onRenderAreaSizeChanged, _pAttachedCamera));
	}
	else
	{
		AddEventListener(RenderAreaEvent::SIZE_CHANGED
							, MFUNC(&Camera::_onRenderAreaSizeChanged, _pAttachedCamera));
	}
}

void RenderArea::update()
{
	SceneRender* pSceneRender = _pRenderWindow->getSceneRender();
	pSceneRender->RenderScene(_renderOPList, this, _pAttachedCamera
							, _pRenderWindow, _pAttachedScene);

	// Render HUD stage if exist.
	if(_pAttachedHUDStage)
	{
		HUDRender* pHUDRender = _pAttachedHUDStage->GetHUDRender();
		pHUDRender->RenderHUDStage(_HUDRenderOPList, this, _pRenderWindow->getWndRenderTarget());
	}
}

void RenderArea::AddRenderOperation(DWORD dwRenderOPType)
{
	RenderOperation* pRenderOperation = _pRenderSystem->CreateRenderOperation(dwRenderOPType);
	pRenderOperation->Init(_pRenderWindow);
	_renderOPList.push_back(pRenderOperation);
}

void RenderArea::AddHUDRenderOperation(DWORD dwRenderOPType)
{
	HUDRenderOperation* pHUDRenderOperation = _pRenderSystem->CreateHUDRenderOperation(dwRenderOPType);
	_HUDRenderOPList.push_back(pHUDRenderOperation);
}

int RenderArea::GetWidth()
{
	return _viewportInfo.Width;
}

int RenderArea::GetHeight()
{
	return _viewportInfo.Height;
}

void RenderArea::SetWidth(int nWidth)
{
	_viewportInfo.Width = nWidth;

	RenderAreaEvent e(RenderAreaEvent::SIZE_CHANGED, _viewportInfo);
	DispatchEvent(e);
}

void RenderArea::SetHeight(int nHeight)
{
	_viewportInfo.Height = nHeight;

	RenderAreaEvent e(RenderAreaEvent::SIZE_CHANGED, _viewportInfo);
	DispatchEvent(e);
}

void RenderArea::SetSize(int nWidth, int nHeight)
{
	_viewportInfo.Width = nWidth;
	_viewportInfo.Height = nHeight;

	RenderAreaEvent e(RenderAreaEvent::SIZE_CHANGED, _viewportInfo);
	DispatchEvent(e);
}

void RenderArea::_onWindowSizeChanged(const Event* pEvent)
{
	const WindowEvent* pWindowEvent = SAFE_CAST(pEvent, const WindowEvent*);
	Window* pWindow = pWindowEvent->pWindow;

	_viewportInfo.X = pWindow->GetWidth() * __fX;
	_viewportInfo.Y = pWindow->GetHeight() * __fY;
	_viewportInfo.Width = pWindow->GetWidth() * __fWidth;
	_viewportInfo.Height = pWindow->GetHeight() * __fHeight;

	if(_viewportInfo.Width == 0) _viewportInfo.Width = 1;
	if(_viewportInfo.Height == 0) _viewportInfo.Height = 1;

	RenderAreaEvent e(RenderAreaEvent::SIZE_CHANGED, _viewportInfo);
	DispatchEvent(e);
}


_X3D_NS_END