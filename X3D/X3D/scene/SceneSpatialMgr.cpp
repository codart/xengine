#include "stdafx.h"
#include "SceneSpatialMgr.h"

_X3D_NS_BEGIN

SceneSpatialMgr::SceneSpatialMgr(void)
: _rootOctreeNode(NULL)
{
	float fLength = 1000;
	Vector3 vExtent = Vector3(fLength, fLength, fLength);
	_rootOctreeNode.setWorldAABB(-vExtent, vExtent);

	_nMaxDepth = 8;
}

SceneSpatialMgr::~SceneSpatialMgr(void)
{
	//... Release all spatial nodes
	_releaseNode(&_rootOctreeNode);

}

void SceneSpatialMgr::_releaseNode(OctreeNode* pSpatialNode)
{
	OctreeNode* pOctreeNode = (OctreeNode*)pSpatialNode;
	OctreeNode* pChildOctreeNode;
	if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 0 ][ 0 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 0 ][ 0 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 1 ][ 0 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 1 ][ 0 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 0 ][ 1 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 0 ][ 1 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 1 ][ 1 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 1 ][ 1 ]) != 0 )
		_releaseNode(pChildOctreeNode);

	if(pSpatialNode != &_rootOctreeNode)
	{	
		SAFE_DELETE(pSpatialNode);
	}
}

void SceneSpatialMgr::VisitVisibleSceneNodes(Camera* pCamera, ISpatialVisitor* pVisitor)
{
	pVisitor->VisitBegin();

	_walkOctree(pCamera, pVisitor, &_rootOctreeNode, FALSE);

	pVisitor->VisitEnd();
}

void SceneSpatialMgr::_walkOctree(Camera* pCamera, ISpatialVisitor* pSpatialVisitor, OctreeNode* pOctreeNode, BOOL bFullVisible)
{
	XASSERT(pOctreeNode);

	if(pOctreeNode->GetSceneNodeCount() == 0) return;

	Camera::Visibility visibility = Camera::NOT_VISIBLE;

	if ( bFullVisible)
	{
		visibility = Camera::FULL_VISIBLE;
	}
	else if ( pOctreeNode == &_rootOctreeNode)
	{
		visibility = Camera::PARTIAL_VISIBLE;
	}
	else
	{
		AxisAlignedBox box;
		pOctreeNode->getCullBounds(&box);
		visibility = pCamera->getVisibility(box);
	}

	if ( visibility != Camera::NOT_VISIBLE)
	{
		//Add stuff to be rendered;
		const SceneNodeList& sceneNodeList = pOctreeNode->GetSceneNodeList();

		pSpatialVisitor->ProcessNodes(sceneNodeList, visibility == Camera::FULL_VISIBLE);

		OctreeNode* pChildOctreeNode;
		BOOL bChildFoundVisible = (visibility == Camera::FULL_VISIBLE);
		if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 0 ][ 0 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 0 ][ 0 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 1 ][ 0 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 1 ][ 0 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 0 ][ 1 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 0 ][ 1 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 0 ][ 1 ][ 1 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

		if ( (pChildOctreeNode = pOctreeNode->_children[ 1 ][ 1 ][ 1 ]) != 0 )
			_walkOctree( pCamera, pSpatialVisitor, pChildOctreeNode, bChildFoundVisible );

	}
}

void SceneSpatialMgr::UpdateSpatialNode(SceneNode* pSceneNode)
{
	XASSERT(pSceneNode);

	const AxisAlignedBox& box = pSceneNode ->GetWorldAABB();

	if ( pSceneNode->getSpatialNode() == NULL)
	{
		//if outside the octree, force into the root node.
		if ( ! pSceneNode->isInAABB( _rootOctreeNode.GetWorldAABB()))
			_rootOctreeNode.AttachSceneNode(pSceneNode);
		else
			_addOctreeNode( pSceneNode, &_rootOctreeNode );
		return ;
	}

	if ( ! pSceneNode -> isInAABB( pSceneNode->getSpatialNode()->GetWorldAABB()) )
	{
		_removeOctreeNode( pSceneNode );

		//if outside the octree, force into the root node.
		if ( ! pSceneNode->isInAABB( _rootOctreeNode.GetWorldAABB()) )
			_rootOctreeNode.AttachSceneNode( pSceneNode );
		else
			_addOctreeNode( pSceneNode, &_rootOctreeNode );
	}
}

void SceneSpatialMgr::_addOctreeNode( SceneNode* pSceneNode, OctreeNode* pOctreeNode, int nDepth)
{
	const AxisAlignedBox& box = pSceneNode->GetWorldAABB();

	//if the octree is twice as big as the scene node,
	//we will add it to a child.
	if ( ( nDepth < _nMaxDepth ) && pOctreeNode->isTwiceSize( box ) )
	{
		int x, y, z;
		pOctreeNode -> _getChildIndicesByAABB( box, &x, &y, &z );

		if ( pOctreeNode->_children[x][y][z] == 0 )
		{
			pOctreeNode -> _children[x][y][z] = XNEW OctreeNode(pOctreeNode);
			const Vector3& octantMin = pOctreeNode->GetWorldAABB().GetMinimum();
			const Vector3& octantMax = pOctreeNode->GetWorldAABB().GetMaximum();
			Vector3 min, max;

			if ( x == 0 )
			{
				min.x = octantMin.x;
				max.x = ( octantMin.x + octantMax.x ) / 2.0f;
			}
			else
			{
				min.x = ( octantMin.x + octantMax.x ) / 2.0f;
				max.x = octantMax.x;
			}

			if ( y == 0 )
			{
				min.y = octantMin.y;
				max.y = ( octantMin.y + octantMax.y ) / 2.0f;
			}
			else
			{
				min.y = ( octantMin.y + octantMax.y ) / 2.0f;
				max.y = octantMax.y;
			}

			if ( z == 0 )
			{
				min.z = octantMin.z;
				max.z = ( octantMin.z + octantMax.z ) / 2.0f;
			}
			else
			{
				min.z = ( octantMin.z + octantMax.z ) / 2.0f;
				max.z = octantMax.z;
			}

			pOctreeNode->_children[x][y][z]->setWorldAABB(min, max);
			pOctreeNode -> _children[x][y][z]->_vHalfSize = ( max - min ) / 2;
		}

		_addOctreeNode( pSceneNode, pOctreeNode->_children[x][y][z], ++nDepth );

	}
	else
	{
		pOctreeNode->AttachSceneNode(pSceneNode);
	}

}

void SceneSpatialMgr::_removeOctreeNode(SceneNode* pSceneNode)
{
	OctreeNode* pOctreeNode = (OctreeNode*)pSceneNode->getSpatialNode();

	if (pOctreeNode)
	{
		pOctreeNode->DetachSceneNode(pSceneNode);
	}
}

_X3D_NS_END