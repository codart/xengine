#pragma once

#include "x3d/scene/RendableObject.h"

_X3D_NS_BEGIN

class RenderQueue;
class Node3D;
class Scene;

typedef void* UserData;

class _X3DExport MovableObject : public NonCopyable
{
public:
	MovableObject();
	MovableObject(const String& strName);
	virtual ~MovableObject(void);

public:
	INLINE const String&
		GetName() const { return _strName; }

	INLINE UserData
		GetUserData() { return _pUserData; }
	INLINE void
		SetUserData(const UserData* userData) { _pUserData = userData; }

	const Matrix4*
		GetWorldTransform(void) const;

	virtual const AxisAlignedBox& 
		GetBoundingBox(void) const = 0;

	/** Retrieves the axis-aligned bounding box for this object in world coordinates. */
	virtual const AxisAlignedBox& 
		GetWorldBoundingBox(BOOL bDerive = false) const;

	// ------
	/// If the movable object is not attached to the scene graph, it doesn't have a world transform and 
	/// any call to this method will cause an assert(debug) or crash(release);
	const Matrix4*
		getWorldTransform(void) const;
	const Vector3*
		getDerivedPosition() const;
	const Quaternion*
		getDerivedOrientation() const;

	// ---
	virtual void
		fillRenderQueue(RenderQueue* pRenderQueue);
	virtual void
		fillRenderableObjList(RendableObjectList& renderableObjList);
	virtual void
		setParentNode(Node3D* pParentNode){ _pParentNode = pParentNode; }


protected:
	String
		_strName;

	Ptr<UserData>
		_pUserData;

	/// Node to which this object is attached
	Node3D*
		_pParentNode;

	mutable AxisAlignedBox 
		_localAABB;
    mutable AxisAlignedBox 
		_worldAABB;

	mutable Boolean
		_bLocalAABBDirty;

};


DECLARE_LIST_TYPE_PTR(vector, MovableObject);
DECLARE_MAP_TYPE_PTR(map, String, MovableObject);

_X3D_NS_END