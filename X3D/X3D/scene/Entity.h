#pragma once

#include "x3d/scene/MovableObject.h"
#include "x3d/scene/Light.h"
#include "x3d/render/RenderObject.h"

_X3D_NS_BEGIN

/*
	Defines an instance of a discrete, movable object with rich render effect. 
*/
class _X3DExport Entity : public MovableObject
						, public RenderObject
{
public:
	Entity(const String& strName, RenderSystem* pRenderSystem);
	virtual ~Entity();


protected:
	//..LightList
	//	_localLightList;

};


_X3D_NS_END