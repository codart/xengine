#include "StdAfx.h"
#include "SceneNode.h"

#include "x3d/scene/SceneSpatialMgr.h"

_X3D_NS_BEGIN

SceneNode::SceneNode(const SceneNode& sceneNode)
: Node3D(sceneNode)
//.... 
{
	_pSpatialNode = NULL;
}
SceneNode::SceneNode(Scene* pScene, const String& strName)
: Node3D(strName)
{
	_pSpatialNode = NULL; 
}

SceneNode::~SceneNode(void)
{
}

SceneNode::SceneNode(const String& strName)
: Node3D(strName)
{
	
}

void SceneNode::AddChild(SceneNode* pChild)
{
	TreeNode::AddChild(pChild);
}

void SceneNode::updateAABB()
{
	// Update bounding box
	_worldAABB.SetNull();
	_localAABB.SetNull();	

	FASTEST_VECTOR_ITERATE(MovableObject*, _attachedObjList);
		MovableObject* pMovableObject = GET_NEXT(_attachedObjList);
		const AxisAlignedBox& box = pMovableObject->GetBoundingBox();
		_localAABB.Merge(box);
		_worldAABB.Merge(pMovableObject->GetWorldBoundingBox(TRUE));
	FASTEST_ITERATE_END();
}

BOOL SceneNode::isInAABB(const AxisAlignedBox& box)
{
	if (box.IsNull()) return false;

	// Always succeed if AABB is infinite
	if (box.IsInfinite())
		return true;

	Vector3 center = _worldAABB.GetMaximum().MidPoint( _worldAABB.GetMinimum() );

	Vector3 bmin = box.GetMinimum();
	Vector3 bmax = box.GetMaximum();

	bool centre = ( bmax > center && bmin < center );
	if (!centre)
		return false;

	// Even if covering the centre line, need to make sure this BB is not large
	// enough to require being moved up into parent. When added, bboxes would
	// end up in parent due to cascade but when updating need to deal with
	// bbox growing too large for this child
	Vector3 octreeSize = bmax - bmin;
	Vector3 nodeSize = _worldAABB.GetMaximum() - _worldAABB.GetMinimum();
	return nodeSize < octreeSize;
}

_X3D_NS_END