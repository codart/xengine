#include "StdAfx.h"
#include "Light.h"

#include "x3d/render/RenderQueue.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/render/IntrinsicRenderableTypes.h"
#include "x3d/lighting/PointLightVolume.h"

_X3D_NS_BEGIN

Light::Light(const String& strName, RenderSystem* pRenderSystem)
: MovableObject(strName)
, RenderObject(pRenderSystem)
{
	//_vPosition = Vector3(100, 100, -100);
	//_vDirection = Vector3(0.5f, 1.2f, -0.6f);
	//_vDirection.Normalise();
	_pShadowMapCamera = NULL;

	_lightType = LIGHT_POINT;
	_vDirection = Vector3::NEGATIVE_UNIT_Y;

	__bShadowMapCameraDirty = TRUE;

	_fRadius = 10.0f;
	_bEnableShadowCasting = FALSE;

	_bLocalAABBDirty = TRUE;
	_pLightVolume = SAFE_CAST(_pRenderSystem->CreateRendableObject(POINT_LIGHT_VOLUME), LightVolume*);
	_pLightVolume->Init(this);

	//__bLightTransformDirty = TRUE;
}

Light::~Light(void)
{
	SAFE_DELETE(_pLightVolume);
	SAFE_DELETE(_pShadowMapCamera);
}

void Light::EnableShadowCasting(BOOL bEnable)
{
	_bEnableShadowCasting = bEnable;
	if(bEnable)
	{
		if(!_pShadowMapCamera)
		{
			_pShadowMapCamera = XNEW Camera;
		}
	}
	else
	{
		SAFE_DELETE(_pShadowMapCamera);
	}
}

const Camera* Light::getShadowMapCamera() const
{
	if(__bShadowMapCameraDirty)
	{
		_updateShadowMapCamera();
	}
	return _pShadowMapCamera;
}

void Light::_updateShadowMapCamera() const
{
	XASSERT(_pShadowMapCamera);
	
	_pShadowMapCamera->setCameraPosition(GetDerivedPosition());
	_pShadowMapCamera->setCameraDirection(GetDerivedDirection());
	//...pShadowMapCamera->lookAt(_vPosition + _vDirection);
	//... test code
	SASSERT(0);
	_pShadowMapCamera->lookAt(Vector3::ZERO);
}

const Vector3& Light::GetDerivedPosition() const
{
	SceneNode* pSceneNode = SAFE_CAST(_pParentNode, SceneNode*);
	return pSceneNode->GetDerivedPosition();
}

const Vector4& Light::GetDerivedPosViewSpace(const Camera* pCamera) const
{
	__derivedPosViewSpaceBuf = GetDerivedPosition() * pCamera->getViewMatrix();
	__derivedPosViewSpaceBuf.w = _fRadius;
	return __derivedPosViewSpaceBuf;
}

const Vector4& Light::GetDerivedDirViewSpace(const Camera* pCamera) const
{
	Vector3 vDerivedDir = pCamera->getInverseViewMatrix().Transpose().TransformNormal(GetDerivedDirection());
	//vDerivedDir = GetDerivedDirection() * pCamera->getViewMatrix();
	//vDerivedDir.Normalise();
	__derivedDirViewSpaceBuf = Vector4(vDerivedDir.x, vDerivedDir.y, vDerivedDir.z, 1);
	__derivedDirViewSpaceBuf.w = _fRadius;
	return __derivedDirViewSpaceBuf;
}

const Vector3& Light::GetDerivedDirection() const
{
	if(_lightType == LIGHT_DIRECTIONAL)
	{
		// Need update from parent?????
		return _vDirection;
	}
	else if(_lightType == LIGHT_SPOTLIGHT)
	{
		// Treat the default direction is downward
		SceneNode* pSceneNode = SAFE_CAST(_pParentNode, SceneNode*);
		_vDirection = Vector3::NEGATIVE_UNIT_Y * (pSceneNode->GetDerivedOrientation());
		return _vDirection;
	}
	else
	{
		XASSERT(0);
		return _vDirection;
	}
}

const Matrix4* Light::getWorldTransform(void) const
{
	if(_lightType == LIGHT_POINT)
	{
		// For point light, its scale is depend on it radius but not derived scale
		__pTempTransform.MakeScaleTransform(Vector3(_fRadius, _fRadius, _fRadius));
		SceneNode* pParentNode = SAFE_CAST(_pParentNode, SceneNode*);
		Matrix4 parentPosAndDirMatrix;
		parentPosAndDirMatrix.MakeTransform(pParentNode->GetDerivedPosition()
										, pParentNode->GetDerivedOrientation());
		__pTempTransform *= parentPosAndDirMatrix;
		return &__pTempTransform;
	}
	else
	{
		return SAFE_CAST(_pParentNode, SceneNode*)->GetDerivedTransform();
	}
}

void Light::fillRenderQueue(RenderQueue* pRenderQueue)
{
	pRenderQueue->AddToLightGroup(this);
}

void Light::SetRadius(Real fRadius)
{
	_bLocalAABBDirty = TRUE;
	_fRadius = fRadius;
}

const AxisAlignedBox& Light::GetBoundingBox() const 
{
	SASSERT(0);

	if(_bLocalAABBDirty)
	{
		switch (_lightType)
		{
		case LIGHT_POINT:
			_localAABB.SetExtents(Vector3(-_fRadius, -_fRadius, -_fRadius)
				, Vector3(_fRadius, _fRadius, _fRadius));
			break;

		case LIGHT_SPOTLIGHT:
			XASSERT(0);
			break;

		case LIGHT_DIRECTIONAL:
			XASSERT(0);
			break;
		}

		_bLocalAABBDirty = FALSE;
	}

	return _localAABB; 
}


_X3D_NS_END