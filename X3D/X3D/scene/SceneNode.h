#pragma once

#include "Common/math/AxisAlignedBox.h"
#include "x3d/common/Node3D.h"


_X3D_NS_BEGIN

class SceneNode;
class SpatialNode;
class Scene;
class SceneSpatialMgr;

DECLARE_MAP_TYPE_PTR(map, String, SceneNode);
DECLARE_LIST_TYPE_PTR(vector, SceneNode);

//...SceneNode 适合于构建全场景中的有层级关系的运动的对象（类似骨骼动画）。如果场景中还包含大量静态物体
// SceneNode机制会导致消耗大量内存（Node3D会缓存各种变换），并且损失相当的效率（用来检查SceneNode是否被OctreeNode
// 包含）。
// 重构方案：保留SceneNode，同时增加一个StaticObject对象（与MovableObject对立）用来渲染静态物体，StaticObject
// 只包含一个worldTransform（相对于世界远点），并且它可以直接被OctreeNode管理。

class _X3DExport SceneNode : public Node3D
{
public:
	SceneNode(const SceneNode& sceneNode);
	SceneNode(Scene* pScene, const String& strName);
	SceneNode(const String& strName);
	virtual ~SceneNode(void);


public:
	virtual void
		AddChild(SceneNode* pChild);
	virtual SceneNode*
		GetParentNode() { return SAFE_CAST(_pParent, SceneNode*); }

    INLINE const AxisAlignedBox& 
		GetWorldAABB(void) const { return _worldAABB; }

	// ---
	final INLINE void
		setSpatialNode(SpatialNode* pSpatialNode){ _pSpatialNode = pSpatialNode; }
	final INLINE SpatialNode*
		getSpatialNode() { return _pSpatialNode; }

	BOOL 
		isInAABB(const AxisAlignedBox& box);
	BOOL
		isTransformDirty() { return _bTransformDirty; }
	INLINE void 
		updateAABB();


protected:
	Ptr<SpatialNode>
		_pSpatialNode;

	//----
	AxisAlignedBox
		_worldAABB;
	AxisAlignedBox
		_localAABB;


protected:
	// Make these methods protected to avoid incorrect calling.
	INLINE void
		AddChild(TreeNode* pChild) { TreeNode::AddChild(pChild); }
	INLINE TreeNode* 
		GetParent() { return TreeNode::GetParent(); }
	INLINE void 
		setParent(TreeNode* pParent) { TreeNode::setParent(pParent); }


};

_X3D_NS_END