#include "stdafx.h"
#include "Entity.h"


_X3D_NS_BEGIN


Entity::Entity(const String& strName, RenderSystem* pRenderSystem)
: MovableObject(strName)
, RenderObject(pRenderSystem)
{

}

Entity::~Entity()
{

}


_X3D_NS_END
