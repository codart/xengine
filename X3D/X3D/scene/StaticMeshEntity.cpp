#include "StdAfx.h"
#include "StaticMeshEntity.h"

#include "x3d/render/RenderQueue.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/render/IntrinsicRenderableTypes.h"
#include "x3d/animation/SkeletonManager.h"
#include "x3d/animation/BoneInstance.h"
#include "x3d/scene/PrefabEntity.h"

_X3D_NS_BEGIN


float* StaticMeshEntity::GetBoneMatrixBuffer() const
{
	BoneNode* pRootBone = _skeletonInstance->GetRootBone();
	BoneNode* pSecondBone = pRootBone->GetChildAt(0);
	BoneNode* pThirdBone = pSecondBone->GetChildAt(0);

	if(!_pCachedBoneMatrixs)
	{
		_pCachedBoneMatrixs = XNEW float[12 * 3]; // 12 * BoneCount
	}

	Matrix4 tempMatrix;

	// 骨骼矩阵，是一个4x3矩阵。也就是只有旋转和偏移，忽略投影（第四列）的矩阵。
	// GPU矩阵是右手系的，所以有先做一次转置。
	memcpy(_pCachedBoneMatrixs, &pRootBone->GetOffsetTransform(tempMatrix).Transpose(), sizeof(float) * 12);
	memcpy(_pCachedBoneMatrixs+12, &pSecondBone->GetOffsetTransform(tempMatrix).Transpose(), sizeof(float) * 12);
	memcpy(_pCachedBoneMatrixs+24, &pThirdBone->GetOffsetTransform(tempMatrix).Transpose(), sizeof(float) * 12);

	return _pCachedBoneMatrixs; 
}

StaticMeshEntity::StaticMeshEntity(const String& strName, const MeshPtr& pMesh, RenderSystem* pRenderSystem)
: Entity(strName, pRenderSystem)
, _pMesh(pMesh)
{
	temporary;
	_skeletonInstance = NULL;
	_pBoneMatrixs = NULL;
	_pCachedBoneMatrixs = NULL;
	_bShowBoneHelperEntity = false;

	_createSubEntitys();
}

StaticMeshEntity::~StaticMeshEntity(void)
{
	//SAFE_CAST(_pParentNode, SceneNode*)
	SAFE_DELETE(_skeletonInstance);
	_skeletonInstance = NULL;

	_releaseAllSubEntites();
}

void StaticMeshEntity::LoadSkeleton()
{
	temporary;
	if(!_skeletonInstance)
	{
		_skeletonInstance = XNEW SkeletonInstance(SkeletonManager::Instance().AddSkeleton(TEXT("test_skeleton"))
											, this);
	}

	_bShowBoneHelperEntity = TRUE;

	if(_pParentNode)
	{
		_pParentNode->AddChild(_skeletonInstance->GetRootBone());
	}
}

void StaticMeshEntity::fillRenderQueue(RenderQueue* pRenderQueue)
{
	FASTEST_VECTOR_ITERATE(RendableObject*, _subMeshEntityList);
		RendableObject* pRenderableObj = GET_NEXT(_subMeshEntityList);

		switch (pRenderableObj->GetMaterial()->GetRenderStyle())
		{
		case RENDER_STYLE_OPAQUE:
			pRenderQueue->AddToOpaqueGroup(pRenderableObj);
			break;

		case RENDER_STYLE_TRANSPARENT:
			pRenderQueue->AddToTransparentGroup(pRenderableObj);
			break;
		}
	FASTEST_ITERATE_END();

	if(_bShowBoneHelperEntity && _skeletonInstance->GetRootBone())
	{
		BoneInstance* pRootBone = _skeletonInstance->GetRootBone();
		pRootBone->fillRenderQueueR(pRenderQueue);
	}
}

void StaticMeshEntity::_createSubEntitys()
{
	XASSERT(_pMesh);

	SubMeshList& subMeshList = _pMesh->GetSubMeshList();

	SubMeshListIt it = subMeshList.begin();
	SubMeshListIt itEnd = subMeshList.end();

	for ( ; it != itEnd; ++it)
	{
		RenderSystem* pRenderSystem = _pMesh->GetRenderSystem();
		SubMeshRendable* pNewSubMeshEntity = SAFE_CAST(pRenderSystem->CreateRendableObject(SUB_MESH_ENTITY), SubMeshRendable*);
		pNewSubMeshEntity->Init(*it, this);
		_subMeshEntityList.push_back(pNewSubMeshEntity);
	}

}

const AxisAlignedBox& StaticMeshEntity::GetBoundingBox(void) const
{
	//XASSERT(0);
	// Get from Mesh
	//if (_pMesh->isLoaded())
	{
		_boundingBox = _pMesh->GetBoundingBox();
		//..._boundingBox.merge(getChildObjectsBoundingBox());

		// Don't scale here, this is taken into account when world BBox calculation is done
	}
	//else
	//	_boundingBox.SetNull();

	return _boundingBox;
}

void StaticMeshEntity::UpdateEntity()
{
	_releaseAllSubEntites();

	_pMesh->UpdateMeshWithModel();

	_createSubEntitys();
}

void StaticMeshEntity::_releaseAllSubEntites()
{
	SubMeshRendableListIt it = _subMeshEntityList.begin();
	SubMeshRendableListIt itEnd = _subMeshEntityList.end();

	for ( ; it != itEnd; ++it)
	{
		delete *it;
	}
}

_X3D_NS_END