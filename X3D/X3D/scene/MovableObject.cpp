#include "StdAfx.h"
#include "MovableObject.h"
#include "x3d/common/Node3D.h"

_X3D_NS_BEGIN

MovableObject::MovableObject()
: _strName(TEXT("Unnamed"))
{
	_pParentNode = NULL;
	_bLocalAABBDirty = TRUE;
	_pUserData = NULL;
}

MovableObject::MovableObject(const String& strName)
: _strName(strName)
{
	_pParentNode = NULL;
	_bLocalAABBDirty = TRUE;
}

MovableObject::~MovableObject(void)
{
}

void MovableObject::fillRenderQueue(RenderQueue* pRenderQueue)
{
	XASSERT(0);
}

void MovableObject::fillRenderableObjList(RendableObjectList& renderableObjList)
{
	XASSERT(0);
}

const Matrix4* MovableObject::getWorldTransform(void) const
{
	return _pParentNode->GetDerivedTransform();
}

const Matrix4* MovableObject::GetWorldTransform(void) const
{
	if(_pParentNode)
	{
		return _pParentNode->GetDerivedTransform();
	}
	else
	{
		return NULL;
	}
}

const AxisAlignedBox& MovableObject::GetWorldBoundingBox(BOOL bDerive) const
{
	if (bDerive)
	{
		_worldAABB = this->GetBoundingBox();
		_worldAABB.TransformAffine(*getWorldTransform());
	}

	return _worldAABB;
}

const Vector3* MovableObject::getDerivedPosition() const
{
	return &_pParentNode->GetDerivedPosition();
}

const Quaternion* MovableObject::getDerivedOrientation() const
{
	return &_pParentNode->GetDerivedOrientation();
}


_X3D_NS_END