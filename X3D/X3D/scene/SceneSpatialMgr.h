#pragma once

#include "x3d/scene/OctreeNode.h"
#include "x3d/scene/SceneQuerys.h"

_X3D_NS_BEGIN

/*
	For visiting spatial nodes during spatial query.
*/
class ISpatialVisitor
{
public:
	virtual void 
		VisitBegin() { XASSERT(0); }
	virtual void 
		ProcessNodes(const SceneNodeList& sceneNodes, BOOL bFullVisible) { XASSERT(0); }
	virtual void
		ProcessNode(const SceneNode* pSceneNode, BOOL bFullVisible) { XASSERT(0); }
	virtual void 
		VisitEnd() { XASSERT(0); }
};

/*
	Provide spatial queries against the movable objects in a scene. 
*/
class _X3DExport SceneSpatialMgr
{
public:
	SceneSpatialMgr(void);
	virtual ~SceneSpatialMgr(void);

public:
	// Methods to receive scene graph events from Scene.
	virtual void 
		OnAddToSceneGraph(MovableObject* pMovableObject) {};
	virtual void 
		OnRemoveFromSceneGraph(MovableObject* pMovableObject) {};

	// Scene query method
	virtual SceneRayQuery* 
		CreateRayQuery(){ XASSERT(0); return NULL; };
	virtual ScenePointQuery* 
		CreatePointQuery(){ XASSERT(0); return NULL; };
	virtual SceneSphereQuery* 
		CreateSphereQuery(){ XASSERT(0); return NULL; };
	virtual SceneAABBQuery* 
		CreateAABBQuery(){ XASSERT(0); return NULL; };

	//virtual SceneQuery* CreateEncroachmentQuery();
	//virtual SceneQuery* CreateOverlapQuery();

	/* 
		Update scene node's corresponding spatial node. Will be called this during 
		Scene::UpdateSceneGraph() by SceneNode::Update().
	 */
	virtual void
		UpdateSpatialNode(SceneNode* pSceneNode);


	/* 
		Traverse all visible scene nodes in specified camera and use a ISpatialVisitor to 
		visit these nodes.
		eg. The RenderQueue will utilize this method to visit all scene node and fill the 
		renderable objects into RenderQueue.
	*/
	virtual void 
		VisitVisibleSceneNodes(Camera* pCamera, ISpatialVisitor* pVisitor);


protected:
	OctreeNode
		_rootOctreeNode;
	/// Max depth for the tree
	Int 
		_nMaxDepth;

	Int 
		_nNumRenderedObject;


protected:
	virtual void 
		_releaseNode(OctreeNode* pSpatialNode);


    /** Adds the Octree Node, starting at the given octree, and recursing at max to the specified depth.
    */
	void 
		_addOctreeNode( SceneNode* pSceneNode, OctreeNode* pOctreeNode, int nDepth = 0);
	/** Removes the given octree node */
	void 
		_removeOctreeNode( SceneNode* );
	void 
		_walkOctree(Camera* pCamera, ISpatialVisitor* pSpatialVisitor, OctreeNode* pOctreeNode, BOOL bFullVisible);

};


_X3D_NS_END