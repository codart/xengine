#pragma once

#include "x3d/scene/MovableObject.h"

_X3D_NS_BEGIN

class Camera;
class LightVolume;

enum LightType
{
	/// Point light sources give off light equally in all directions, so require only position not direction
	LIGHT_POINT = 0,
	/// Directional lights simulate parallel light beams from a distant source, hence have direction but no position
	LIGHT_DIRECTIONAL = 1,
	/// Spotlights simulate a cone of light from a source so require position and direction, plus extra values for falloff
	LIGHT_SPOTLIGHT = 2
};

class _X3DExport Light : public MovableObject
						, public RenderObject
{
public:
	Light(const String& strName, RenderSystem* pRenderSystem);
	virtual ~Light(void);

public:
	override const AxisAlignedBox& 
		GetBoundingBox() const;

	void 
		SetLightType(LightType lightType) { _lightType = lightType; }
	const LightType
		GetLightType() const { return _lightType; }

	void 
		SetRadius(Real fRadius);
	float
		GetRadius() { return _fRadius; }

	INLINE void
		SetColor(Color color) { _color = color; }
	INLINE const Color&
		GetColor() const { return _color; }

	INLINE void
		SetDirection(const Vector3& vDirection) {_vDirection = vDirection; }

	void 
		EnableShadowCasting(BOOL bEnable);
	BOOL
		IsShadowCastingEnabled() const { return _bEnableShadowCasting; }

	const Vector3&
		GetDerivedPosition() const;
	const Vector4&
		GetDerivedPosViewSpace(const Camera* pCamera) const;
	const Vector4&
		GetDerivedDirViewSpace(const Camera* pCamera) const;

	const Vector3&
		GetDerivedDirection() const;

	// ---
	override const Matrix4* 
		getWorldTransform(void) const;
	override void 
		fillRenderQueue(RenderQueue* pRenderQueue);

	const Camera* 
		getShadowMapCamera() const;
	INLINE LightVolume*
		getLightVolume() { return _pLightVolume; }


protected:
	LightType
		_lightType;
	mutable Vector3 
		_vDirection;

	Real
		_fRadius;
	Color 
		_color;
	Color
		_specular;
	
	mutable Camera*
		_pShadowMapCamera;
	Boolean
		_bEnableShadowCasting;

	LightVolume*
		_pLightVolume;


protected:
	void 
		_updateShadowMapCamera() const;


private:
	BOOL
		__bShadowMapCameraDirty;

	mutable Matrix4
		__pTempTransform;
	//mutable Matrix4
	//	__pLightScaleTransform;
	//mutable BOOL
	//	__bLightRadiusDirty;

	mutable Vector4
		__derivedPosViewSpaceBuf;
	mutable Vector4
		__derivedDirViewSpaceBuf;

};

DECLARE_LIST_TYPE_PTR(vector, Light);

_X3D_NS_END