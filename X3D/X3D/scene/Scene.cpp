#include "StdAfx.h"
#include "Scene.h"

#include "x3d/render/SceneRender.h"
#include "x3d/scene/SceneSpatialMgr.h"
#include "x3d/scene/StaticMeshEntity.h"

#include "x3d/resource/MeshManager.h"
#include "x3d/scene/SceneNode.h"
#include "x3d/render/RenderSystem.h"

#include "common/util/xmlDocument.h"
#include "Common/util/XmlAttributeHandle.h"
#include "Common/util/Unicode.h"
#include "x3d/scene/PrefabEntity.h"

_X3D_NS_BEGIN


Scene::Scene(RenderSystem* pRenderSystem)
: _ambientLightColor(0, 0, 0, 0)
{
	_pRenderSystem = pRenderSystem;

	_pSceneSpatialMgr = XNEW SceneSpatialMgr;
	_pRootNode = XNEW SceneNode(this, TEXT("RootSceneNode"));

	// Create a global light
	Light* pGlobalLight = AddLight(TEXT("SunLight"));
	pGlobalLight->SetLightType(LIGHT_DIRECTIONAL);
	Vector3 vDir = Vector3(-1.0f, -1.0f, 1.0f);
	vDir.Normalise();
	pGlobalLight->SetDirection(vDir);
	pGlobalLight->SetColor(Color(0.4f, 0.4f, 0.4f));
	_globalLights.push_back(pGlobalLight);
}

Scene::~Scene(void)
{
	Clear();
}

void Scene::Destroy()
{
	delete this;
}

void Scene::Clear()
{
/*
	// Release all light
	LightListIt itLight = _globalLights.begin();
	LightListIt itLightEnd = _globalLights.end();

	for ( ; itLight != itLightEnd; ++itLight )
	{
		XDELETE *itLight;
	}
*/

	// Release all movable object
	MovableObjectMapIt it = _movableObjectMap.begin();
	MovableObjectMapIt itEnd = _movableObjectMap.end();

	for ( ; it != itEnd; ++it)
	{
		MovableObject* pMovableObject = it->second;
		XDELETE pMovableObject;
	}
	_movableObjectMap.clear();

	// Release all scene nodes
	//...
	SceneNodeMapIt it2 = _allSceneNodes.begin();
	SceneNodeMapIt itEnd2 = _allSceneNodes.end();

	for ( ; it2 != itEnd2; ++it2)
	{
		delete it2->second;
	}

	_allSceneNodes.clear();

	SAFE_DELETE(_pRootNode);
	SAFE_DELETE(_pSceneSpatialMgr);
}

void Scene::SaveSceneGraph(const String& strFileName)
{
	XASSERT(_pRootNode);

	USING_STRING_CONVERT;

	XmlDocument xmlDoc;

	XmlNodeHandle rootNode = xmlDoc.AppendNode("Node");
	xmlDoc.AppendAttribute(rootNode, "name", "root");

	const TreeNodeList& children = _pRootNode->GetChildNodes();

	TreeNodeConstIterator nodeIterator(children);
	XmlNodeHandle currentNode = rootNode;

	while(nodeIterator.HasMore())
	{
		SceneNode* pSceneNode = SAFE_CAST(nodeIterator.CurrentData(), SceneNode*);
		XmlNodeHandle childNodeHandle = xmlDoc.AppendNode(currentNode, "node");
		xmlDoc.AppendAttribute(childNodeHandle, "name", WStrToAStr(pSceneNode->GetName().c_str()));

		// Traverse all the attached movable objects

		nodeIterator.Next();
	}

	xmlDoc.SaveToFile(WStrToAStr( (RESOURCE_PATH + TEXT("test.xml")).c_str() ));
}

SceneNode* Scene::GetRootNode()
{
	XASSERT(_pRootNode);
	return _pRootNode;
}

void Scene::AddChild(SceneNode* pSceneNode)
{
	_pRootNode->AddChild(pSceneNode);
}

SceneNode* Scene::CreateSceneNode(const String& strNodeName)
{
	XASSERT(_allSceneNodes.find(strNodeName) == _allSceneNodes.end());
	SceneNode* pSceneNode = XNEW SceneNode(this, strNodeName);
	_allSceneNodes[strNodeName] = pSceneNode;

	return pSceneNode;
}

SceneNode* Scene::GetSceneNode(const String& strNodeName)
{
	SceneNodeMapIt it = _allSceneNodes.find(strNodeName);
	if(it == _allSceneNodes.end()) return NULL;
	else return it->second;
}

void Scene::DestroySceneNode(const String& strNodeName)
{
	SceneNodeMapIt it = _allSceneNodes.find(strNodeName);
	XASSERT(it != _allSceneNodes.end());

	// Make sure this node is not any other node' parent
#ifdef DEBUG
	SceneNodeMapIterator sceneNodeIterator(_allSceneNodes);
	while (sceneNodeIterator.HasMore())
	{
		XASSERT(sceneNodeIterator.CurrentData()->TreeNode::GetParent() != it->second);
	}

#endif		

	XDELETE it->second;
	_allSceneNodes.erase(it);
}

StaticMeshEntity* Scene::AddStaticMeshEntity(const String& strName, const String& strMeshName, BOOL bGeneratedByModel)
{
	MovableObjectMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end()) return SAFE_CAST(it->second, StaticMeshEntity*);

	MeshPtr pMeshPtr;
	if(bGeneratedByModel)
	{
		pMeshPtr = MeshManager::Instance().AddModeledMesh(strMeshName);
	}
	else
	{
		pMeshPtr = MeshManager::Instance().AddMesh(strMeshName);
	}

	StaticMeshEntity* pMeshEntity = StaticMeshEntity::Create(strName, pMeshPtr, _pRenderSystem);
	_movableObjectMap.insert(make_pair(strName, pMeshEntity));
	return pMeshEntity;
}

PrefabEntity* Scene::AddPrefabEntity(const String& strName, const String& strPrefabMeshName, PrefabType type)
{
	MovableObjectMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end()) return SAFE_CAST(it->second, PrefabEntity*);

	MeshPtr pMeshPtr = MeshManager::Instance().AddPrefabMesh(strPrefabMeshName, type);

	PrefabEntity* pPrefabEntity = XNEW PrefabEntity(strName, pMeshPtr, _pRenderSystem);
	_movableObjectMap.insert(make_pair(strName, pPrefabEntity));

	return pPrefabEntity;
}

Light* Scene::AddLight(const String& strName)
{
	MovableObjectMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end()) return SAFE_CAST(it->second, Light*);

	Light* pLight = XNEW Light(strName, _pRenderSystem);
	_movableObjectMap.insert(make_pair(strName, pLight));

	return pLight;
}

MovableObject* Scene::GetMovableObject(const String& strName)
{
	MovableObjectMapIt it = _movableObjectMap.find(strName);
	if(it != _movableObjectMap.end()) return (StaticMeshEntity*)it->second;
	else return NULL;
}

void Scene::updateSceneGraph()
{
	XASSERT(_pRootNode);
	_pRootNode->AcceptVisitor(this);
}

void Scene::Visit(Node3D* pNode3D)
{
	SceneNode* pSceneNode = SAFE_CAST(pNode3D, SceneNode*);
	pSceneNode->updateAABB();

	// update spatial node
	_pSceneSpatialMgr->UpdateSpatialNode(pSceneNode);
}

_X3D_NS_END