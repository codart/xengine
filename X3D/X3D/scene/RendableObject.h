#pragma once

#include "x3d/material/Material.h"
#include "x3d/render/RenderObject.h"

_X3D_NS_BEGIN

typedef DWORD RenderableType;
DECLARE_LIST_TYPE(vector, RenderableType);

class DrawPrimitiveCommand;
class RenderSystem;
class RenderContext;
class Entity;

class RendableObject : public RenderObject
{
public:
	RendableObject(RenderSystem* pRenderSystem);
	virtual ~RendableObject(void);


public:
	virtual transient const Material*
		GetMaterial(void) const { XASSERT(0); return NULL; }
	virtual const DrawPrimitiveCommand*
		GetDrawPrimitiveCmd() const { XASSERT(0); return NULL; }
    virtual void
		GetWorldTransforms(Matrix4* pMatrixBuffer) const;
	virtual void
		UpdateTransform(const ViewportInfo& viewportInfo);

	virtual Technique* 
		GetTechnique(void) const { XASSERT(GetMaterial()); return GetMaterial()->GetBestTechnique(); }

	virtual void
		Render(transient RenderContext* pRenderContext) { XASSERT(0); }

	Entity*
		GetParentEntity() { return _pParentEntity; }
	temporary float*
		GetBoneMatrixBuffer() const;
	temporary UINT
		GetBoneCount() const { return 3; }

protected:
	Entity*
		_pParentEntity;

};

DECLARE_LIST_TYPE_PTR(vector, RendableObject);

_X3D_NS_END