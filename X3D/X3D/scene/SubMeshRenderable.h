#pragma once

#include "X3D/material/Material.h"
#include "x3d/scene/RendableObject.h"
#include "x3d/graphic/DrawPrimitiveCommand.h"

_X3D_NS_BEGIN

class Entity;
class SubMesh;

class SubMeshRendable : public RendableObject
					, public Final<SubMeshRendable>
{
public:
	SubMeshRendable(RenderSystem* pRenderSystem);
	virtual ~SubMeshRendable(void);

public:	
	void 
		Init(SubMesh* pSubMesh, Entity* pParentEntity);

	override transient const Material* 
		GetMaterial(void) const {return _pMaterial;};
	override const DrawPrimitiveCommand*
		GetDrawPrimitiveCmd() const;
	override void 
		GetWorldTransforms(Matrix4* pMatrixBuffer) const;

	INLINE SubMesh*
		GetSubMesh(){return _pSubMesh;};


private:
	DrawPrimitiveCommand
		_drawPrimitiveCmd;

	MaterialPtr 
		_pMaterial;

	SubMesh*
		_pSubMesh;

};


DECLARE_LIST_TYPE_PTR(vector, SubMeshRendable);


_X3D_NS_END