#include "stdafx.h"
#include "Camera.h"

#include "x3d/scene/Scene.h"
#include "common/math/Angle.h"

_X3D_NS_BEGIN

Camera::Camera()
{
	_cameraType = AIRCRAFT;

	_position   = Vector3(0.0f, 100.0f, -100.0f);
	_right = Vector3(1.0f, 0.0f, 0.0f);
	_up    = Vector3(0.0f, 1.0f, 0.0f);
	_look  = Vector3(0.0f, 0.0f, 1.0f);

	_fFieldOfViewY = Math::PI/4;
	_fFarClipDist = 10000;
	_fNearClipDist =1;
	_fAspect = 1.3333f;

	__bViewChanged = TRUE;
	__bProjectionChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bFrustumChanged = TRUE;
	__bViewProjTranspChanged = TRUE;

	__cachedViewMatrix.Identify();
	__cachedProjMatrix.Identify();
	__cachedViewProjMatrix.Identify();
	__cachedViewProjTransp.Identify();

	lookAt(Vector3(0, 0, 0));
}

Camera::~Camera()
{
}

void Camera::Destroy()
{
	delete this;
}

void Camera::reset(transient const Camera* pCamera)
{
	__bViewChanged = TRUE;
	__bProjectionChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bFrustumChanged = TRUE;

	_position = pCamera->_position;
	_up = pCamera->_up;
	_right = pCamera->_right;
	_look = pCamera->_look;

	_fAspect = pCamera->_fAspect;
	_fFieldOfViewY = pCamera->_fFieldOfViewY;
	_fNearClipDist = pCamera->_fNearClipDist;
	_fFarClipDist = pCamera->_fFarClipDist;
}

void Camera::ReflectByPlane(const Plane& plane)
{
	Matrix4 reflect = Math::BuildReflectionMatrix(plane);
	
	_position *= reflect;
	_up *= reflect;
	_look *= reflect;
	_right = _up.CrossProduct(_look);

	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;
}

void Camera::SetAspectRatio(float fAspectRatio)
{
	__bProjectionChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;

	_fAspect = fAspectRatio;
}

const Matrix4& Camera::getViewMatrix() const
{
	if(!__bViewChanged)
	{
		return __cachedViewMatrix;
	}

	__bViewChanged = FALSE;
	_updateCachedViewMatrix();

	return __cachedViewMatrix;
}

const Matrix4& Camera::getProjectMatrix() const 
{
	if(!__bProjectionChanged)
	{
		return __cachedProjMatrix;
	}

	__bProjectionChanged = FALSE;
	_updateCachedProjMatirx();

	return __cachedProjMatrix;
}

const Matrix4& Camera::getViewProjMatrix() const
{
	if(!__bViewProjChanged)
	{
		return __cachedViewProjMatrix;
	}

	__bViewProjChanged = FALSE;
	_updateCachedViewProjMatrix();

	return __cachedViewProjMatrix;
}

const Matrix4& Camera::getViewProjTranspose() const
{
	if(!__bViewProjTranspChanged)
	{
		return __cachedViewProjTransp;
	}

	__bViewProjTranspChanged = FALSE;
	_updateCachedViewProjTranspMatrix();

	return __cachedViewProjTransp;
}

void Camera::_updateCachedViewProjMatrix() const
{
	__cachedViewProjMatrix = getViewMatrix() * getProjectMatrix();
}

void Camera::_updateCachedViewProjTranspMatrix() const
{
	__cachedViewProjTransp = (getViewMatrix() * getProjectMatrix()).Transpose();
}

Camera::Visibility Camera::getVisibility(const AxisAlignedBox& bound)
{
	if(bound.IsNull()) return NOT_VISIBLE;
	if(bound.IsInfinite()) return FULL_VISIBLE;

	//... Update per frame????
	_updateCachedFrustumPlanes();

	// Get centre of the box
	Vector3 centre = bound.GetCenter();
	// Get the half-size of the box
	Vector3 halfSize = bound.GetHalfSize();

	bool all_inside = true;

	for ( int plane = 0; plane < 6; ++plane )
	{

		// Skip far plane if infinite view frustum
		if (plane == FRUSTUM_PLANE_FAR && _fFarClipDist == 0.0f)
			continue;

		// This updates frustum planes and deals with cull frustum
		Plane::Side side = getFrustumPlane((FrustumPlaneIndex)plane).GetSide(centre, halfSize);
		if(side == Plane::NEGATIVE_SIDE) return NOT_VISIBLE;

		// We can't return now as the box could be later on the negative side of a plane.
		if(side == Plane::BOTH_SIDE) 
			all_inside = false;
	}

	if ( all_inside )
		return FULL_VISIBLE;
	else
		return PARTIAL_VISIBLE;
}

void Camera::_updateCachedViewMatrix() const
{
	// Keep camera's axes orthogonal to each other
	_look.Normalise();

	_up = _look.CrossProduct(_right);
	_up.Normalise();

	_right = _up.CrossProduct(_look);
	_right.Normalise();

	// Build the view matrix:
	float x = -_right.DotProduct(_position);
	float y = -_up.DotProduct(_position);
	float z = -_look.DotProduct(_position);

	(__cachedViewMatrix)(0, 0) = _right.x; 
	(__cachedViewMatrix)(0, 1) = _up.x; 
	(__cachedViewMatrix)(0, 2) = _look.x; 
	(__cachedViewMatrix)(0, 3) = 0.0f;

	(__cachedViewMatrix)(1, 0) = _right.y; 
	(__cachedViewMatrix)(1, 1) = _up.y; 
	(__cachedViewMatrix)(1, 2) = _look.y; 
	(__cachedViewMatrix)(1, 3) = 0.0f;

	(__cachedViewMatrix)(2, 0) = _right.z; 
	(__cachedViewMatrix)(2, 1) = _up.z; 
	(__cachedViewMatrix)(2, 2) = _look.z; 
	(__cachedViewMatrix)(2, 3) = 0.0f;

	(__cachedViewMatrix)(3, 0) = x;        
	(__cachedViewMatrix)(3, 1) = y;     
	(__cachedViewMatrix)(3, 2) = z;       
	(__cachedViewMatrix)(3, 3) = 1.0f;
}

void Camera::_updateCachedProjMatirx() const
{
	Degree d;
	d.valueRadians();
	D3DXMatrixPerspectiveFovLH((D3DXMATRIX*)&__cachedProjMatrix
									, _fFieldOfViewY.ValueRadians()
									, _fAspect, _fNearClipDist, _fFarClipDist);
}

void Camera::_updateCachedFrustumPlanes()
{
	if(!__bFrustumChanged) return;

	__bFrustumChanged = FALSE;
	_calculateFrustumPlane(__cachedFrustumPlanes, TRUE);
}

void Camera::setCameraPosition(const Vector3& vPosition)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	_position = vPosition;
}

void Camera::setCameraDirection(const Vector3& vDirection)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	_look = vDirection;
	_right = Vector3::UNIT_Y.CrossProduct(_look);	
	_up = _look.CrossProduct(_right);	
}

void Camera::lookAt(const Vector3& vLookAtPt)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	_look = vLookAtPt - _position;
	_look.Normalise();

	_right = Vector3::UNIT_Y.CrossProduct(_look);	
	_up = _look.CrossProduct(_right);
}

void Camera::Walk(float units)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	// move only on xz plane for land object
	if( _cameraType == LANDOBJECT )
		_position += Vector3(_look.x, 0.0f, _look.z) * units;

	if( _cameraType == AIRCRAFT )
		_position += _look * units;
}

void Camera::Strafe(float units)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	// move only on xz plane for land object
	if( _cameraType == LANDOBJECT )
		_position += Vector3(_right.x, 0.0f, _right.z) * units;

	if( _cameraType == AIRCRAFT )
		_position += _right * units;
}

void Camera::Fly(float units)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	// move only on y-axis for land object
	if( _cameraType == LANDOBJECT )
		_position.y += units;

	if( _cameraType == AIRCRAFT )
		_position += _up * units;
}

void Camera::Pitch(float angle)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	Matrix4 tempMatrix;
	D3DXMatrixRotationAxis((D3DXMATRIX*)&tempMatrix, (D3DXVECTOR3*)&_right, angle);

	// rotate _up and _look around _right vector
	_up *= tempMatrix;
	_look *= tempMatrix;
}

void Camera::Yaw(float angle)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	Matrix4 tempMatrix;

	// rotate around world y (0, 1, 0) always for land object
	if( _cameraType == LANDOBJECT )
		D3DXMatrixRotationY((D3DXMATRIX*)&tempMatrix, angle);

	// rotate around own up vector for aircraft
	if( _cameraType == AIRCRAFT )
		D3DXMatrixRotationAxis((D3DXMATRIX*)&tempMatrix, (D3DXVECTOR3*)&_up, angle);

	// rotate _right and _look around _up or y-axis
	_right *= tempMatrix;
	_look *= tempMatrix;
}

void Camera::Roll(float angle)
{
	// only roll for aircraft type
	if( _cameraType == AIRCRAFT )
	{
		__bViewChanged = TRUE;
		__bViewProjChanged = TRUE;
		__bViewProjTranspChanged = TRUE;
		__bFrustumChanged = TRUE;

		Matrix4 rotateMatrix;
		D3DXMatrixRotationAxis((D3DXMATRIX*)&rotateMatrix, (D3DXVECTOR3*)&_look, angle);

		// rotate _up and _right around _look vector
		_right *= rotateMatrix;
		_up *= rotateMatrix;
	}
}

void Camera::Move(const Vector3& vec)
{
	__bViewChanged = TRUE;
	__bViewProjChanged = TRUE;
	__bViewProjTranspChanged = TRUE;
	__bFrustumChanged = TRUE;

	_position += vec;
}

void Camera::MoveRelative(const Vector3& vec)
{

}

void Camera::_onRenderAreaSizeChanged(const Event* pEvent)
{
	const RenderAreaEvent* pRenderAreaEvent = SAFE_CAST(pEvent, const RenderAreaEvent*);
	SetAspectRatio((float)pRenderAreaEvent->region.Width/(float)pRenderAreaEvent->region.Height);
}

void Camera::_calculateFrustumPlane(Plane pFrustumPlanes[6], BOOL normalize)
{
	const Matrix4& viewProjMatrix = getViewProjMatrix();

	// Near clipping plane
	pFrustumPlanes[0].normal.x = viewProjMatrix._13;
	pFrustumPlanes[0].normal.y = viewProjMatrix._23;
	pFrustumPlanes[0].normal.z = viewProjMatrix._33;
	pFrustumPlanes[0].distance = viewProjMatrix._43;
	// Far clipping plane
	pFrustumPlanes[1].normal.x = viewProjMatrix._14 - viewProjMatrix._13;
	pFrustumPlanes[1].normal.y = viewProjMatrix._24 - viewProjMatrix._23;
	pFrustumPlanes[1].normal.z = viewProjMatrix._34 - viewProjMatrix._33;
	pFrustumPlanes[1].distance = viewProjMatrix._44 - viewProjMatrix._43;
	// Left clipping plane
	pFrustumPlanes[2].normal.x = viewProjMatrix._14 + viewProjMatrix._11;
	pFrustumPlanes[2].normal.y = viewProjMatrix._24 + viewProjMatrix._21;
	pFrustumPlanes[2].normal.z = viewProjMatrix._34 + viewProjMatrix._31;
	pFrustumPlanes[2].distance = viewProjMatrix._44 + viewProjMatrix._41;
	// Right clipping plane
	pFrustumPlanes[3].normal.x = viewProjMatrix._14 - viewProjMatrix._11;
	pFrustumPlanes[3].normal.y = viewProjMatrix._24 - viewProjMatrix._21;
	pFrustumPlanes[3].normal.z = viewProjMatrix._34 - viewProjMatrix._31;
	pFrustumPlanes[3].distance = viewProjMatrix._44 - viewProjMatrix._41;
	// Top clipping plane
	pFrustumPlanes[4].normal.x = viewProjMatrix._14 - viewProjMatrix._12;
	pFrustumPlanes[4].normal.y = viewProjMatrix._24 - viewProjMatrix._22;
	pFrustumPlanes[4].normal.z = viewProjMatrix._34 - viewProjMatrix._32;
	pFrustumPlanes[4].distance = viewProjMatrix._44 - viewProjMatrix._42;
	// Bottom clipping plane
	pFrustumPlanes[5].normal.x = viewProjMatrix._14 + viewProjMatrix._12;
	pFrustumPlanes[5].normal.y = viewProjMatrix._24 + viewProjMatrix._22;
	pFrustumPlanes[5].normal.z = viewProjMatrix._34 + viewProjMatrix._32;
	pFrustumPlanes[5].distance = viewProjMatrix._44 + viewProjMatrix._42;

	// Normalize the plane equations, if requested
	if (normalize == TRUE)
	{
		pFrustumPlanes[0].Normalise();
		pFrustumPlanes[1].Normalise();
		pFrustumPlanes[2].Normalise();
		pFrustumPlanes[3].Normalise();
		pFrustumPlanes[4].Normalise();
		pFrustumPlanes[5].Normalise();
	}
}


_X3D_NS_END