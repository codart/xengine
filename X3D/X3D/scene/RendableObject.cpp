#include "StdAfx.h"
#include "RendableObject.h"
#include "x3d/graphic/ViewportInfo.h"

temporary 
#include "x3d/scene/SubMeshRenderable.h"
#include "x3d/scene/StaticMeshEntity.h"

_X3D_NS_BEGIN

float* RendableObject::GetBoneMatrixBuffer() const
{
	// 仅仅当RenderableObject是一个SubMeshRendable时才有可能有骨骼
	SubMeshRendable* pSubMeshRenderable = SAFE_CAST((RendableObject*)this, SubMeshRendable*);

	// 目前Skeleton被hardcode写进了StaticMeshEntity.
	StaticMeshEntity* pStaticMeshEntity = SAFE_CAST(pSubMeshRenderable->GetParentEntity(), StaticMeshEntity*);

	return pStaticMeshEntity->GetBoneMatrixBuffer();
}

RendableObject::RendableObject(RenderSystem* pRenderSystem)
: RenderObject(pRenderSystem)
{
	_pParentEntity = NULL;

}

RendableObject::~RendableObject(void)
{
}

void RendableObject::GetWorldTransforms(Matrix4* pMatrixBuffer) const
{
	XASSERT(0);
}

void RendableObject::UpdateTransform(const ViewportInfo& viewportInfo)
{
	XASSERT(0);
}



_X3D_NS_END