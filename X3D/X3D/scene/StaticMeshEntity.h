#pragma once

#include "x3d/scene/Entity.h"
#include "x3d/scene/SubMeshRenderable.h"
#include "x3d/resource/StaticMesh.h"

#include "x3d/animation/SkeletonInstance.h"


_X3D_NS_BEGIN

class _X3DExport StaticMeshEntity : public Entity
							, public Final<StaticMeshEntity>
{
protected:
	StaticMeshEntity(const String& strName, const MeshPtr& pMesh, RenderSystem* pRenderSystem);
public:
	virtual ~StaticMeshEntity(void);
	static StaticMeshEntity* Create(const String& strName, const MeshPtr& pMesh, RenderSystem* pRenderSystem)
									{ return XNEW StaticMeshEntity(strName, pMesh, pRenderSystem); }

public:
	const SubMeshRendableList&
		GetSubMeshEntityList(){return _subMeshEntityList;}
	StaticMeshPtr&
		GetStaticMesh(){return _pMesh;}

	virtual const AxisAlignedBox& 
		GetBoundingBox(void) const;

	//---
	temporary override void
		fillRenderQueue(RenderQueue* pRenderQueue);

	temporary SkeletonInstance&
		GetSkeleton() { return *_skeletonInstance; }
	temporary float* 
		GetBoneMatrixBuffer() const;

	temporary void
		LoadSkeleton();

	void
		UpdateEntity();


protected:
	void 
		_createSubEntitys();
	void 
		_releaseAllSubEntites();


protected:
	temporary Ptr<SkeletonInstance>
		_skeletonInstance;
	temporary Boolean
		_bShowBoneHelperEntity;

	StaticMeshPtr
		_pMesh;

	// Bounding box that contains all the mesh of each child entity
	mutable AxisAlignedBox 
		_boundingBox;

	SubMeshRendableList
		_subMeshEntityList;


	temporary mutable Matrix4*
		_pBoneMatrixs;

	temporary mutable float*
		_pCachedBoneMatrixs;


};



_X3D_NS_END