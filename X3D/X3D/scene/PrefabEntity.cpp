#include "stdafx.h"
#include "PrefabEntity.h"

#include "x3d/render/RenderQueue.h"
#include "x3d/resource/MeshManager.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/scene/PrefabRenderable.h"
#include "x3d/render/IntrinsicRenderableTypes.h"

_X3D_NS_BEGIN

PrefabEntity::PrefabEntity(const String& strName, MeshPtr& pPrefabMesh, RenderSystem* pRenderSystem)
: Entity(strName, pRenderSystem)
{
	_boundingBox.SetExtents(Vector3(-1, -1, -1), Vector3(1, 1, 1));

	_pPrefabRenderable= SAFE_CAST(GetRenderSystem()->CreateRendableObject(PREFAB_RENDABLE)
									, PrefabRenderable*); 
	_pPrefabRenderable->Init(pPrefabMesh, this);
}

PrefabEntity::~PrefabEntity(void)
{
	SAFE_DELETE(_pPrefabRenderable);
}

const AxisAlignedBox& PrefabEntity::GetBoundingBox(void) const
{
	return _boundingBox;
}

void PrefabEntity::fillRenderQueue(RenderQueue* pRenderQueue)
{
	switch (_pPrefabRenderable->GetMaterial()->GetRenderStyle())
	{
	case RENDER_STYLE_OPAQUE:
		pRenderQueue->AddToOpaqueGroup(_pPrefabRenderable);
		break;

	case RENDER_STYLE_TRANSPARENT:
		pRenderQueue->AddToTransparentGroup(_pPrefabRenderable);
		break;
	}
}

_X3D_NS_END