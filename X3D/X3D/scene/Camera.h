#pragma once

#include "Common/math/matrix.h"
#include "common/math/AxisAlignedBox.h"
 
_X3D_NS_BEGIN


enum CameraType 
{ 
	LANDOBJECT, AIRCRAFT 
};

class Scene;

enum FrustumPlaneIndex
{
	FRUSTUM_PLANE_NEAR   = 0,
	FRUSTUM_PLANE_FAR    = 1,
	FRUSTUM_PLANE_LEFT   = 2,
	FRUSTUM_PLANE_RIGHT  = 3,
	FRUSTUM_PLANE_TOP    = 4,
	FRUSTUM_PLANE_BOTTOM = 5
};


/** 
	A viewpoint from which the scene will be rendered. Each camera need to attach to a scene to render.

    @remarks
 */
class _X3DExport Camera
{
	friend class RenderArea;
	friend class Viewport;

public:
	Camera(Camera* pSrcCamera){ reset(pSrcCamera); }
	Camera();
	virtual ~Camera();


public:
	enum Visibility
	{
		NOT_VISIBLE = 0,
		PARTIAL_VISIBLE = 1,
		FULL_VISIBLE = 2,
	};

public:
	virtual void
		Destroy();
	virtual Vector3
		GetDirection() { return _look; }
	virtual void
		SetAspectRatio(float fAspectRatio);

	virtual void 
		Strafe(float units); // left/right

	virtual void 
		Fly(float units);    // up/down
	virtual void 
		Walk(float units);   // forward/backward

	virtual void 
		Move(const Vector3& vec);
	virtual void 
		MoveRelative(const Vector3& vec);

	virtual void 
		Pitch(float angle); // rotate on right vector
	virtual void 
		Yaw(float angle);   // rotate on up vector
	virtual void 
		Roll(float angle);  // rotate on look vector

	const Matrix4&
		getViewMatrix() const; 
	Matrix4
		getInverseViewMatrix() const { return Matrix4(_right, _up, _look, _position); }
	const Matrix4&
		getViewProjMatrix() const;
	const Matrix4& 
		getViewProjTranspose() const;
	const Matrix4&
		getProjectMatrix() const;
	Vector3
		getCameraPosition() const { return _position; }; 

	void 
		setCameraPosition(const Vector3& vPosition);

	// Note: Treat the camera is always horizontal
	void 
		setCameraDirection(const Vector3& vDirection);

	void 
		lookAt(const Vector3& vLookAtPt);
	
	// SceneSpatialMgr will use this method to determine whether a space is visible. 
	Visibility 
		getVisibility(const AxisAlignedBox& bound);
	const Plane&
		getFrustumPlane(FrustumPlaneIndex planeIndex) { return __cachedFrustumPlanes[planeIndex]; };

	// Reset camera's stage by pCamera
	void
		reset(transient const Camera* pCamera);

	void
		ReflectByPlane(const Plane& plane);


protected:
	CameraType  
		_cameraType;

	// View params
	mutable Vector3
		_position;
	mutable Vector3 
		_right;
	mutable Vector3 
		_up;
	mutable Vector3
		_look;

	// Projection params
	/// y-direction field-of-view (default 45)
	Radian 
		_fFieldOfViewY;
	/// Near clip distance - default 1
	Real 
		_fNearClipDist;
	/// Far clip distance - default 10000
	Real 
		_fFarClipDist;
	/// x/y viewport ratio - default 1.3333
	Real 
		_fAspect;


protected:
	virtual void 
		_onRenderAreaSizeChanged(const Event* pEvent);
	
	INLINE void
		_calculateFrustumPlane(Plane pFrustumPlanes[6], BOOL normalize);

	INLINE void 
		_updateCachedViewMatrix() const;
	INLINE void
		_updateCachedProjMatirx() const;
	INLINE void 
		_updateCachedViewProjMatrix() const;
	INLINE void 
		_updateCachedViewProjTranspMatrix() const;
	INLINE void 
		_updateCachedFrustumPlanes();


private:
	mutable Boolean 
		__bViewChanged;
	mutable Boolean
		__bProjectionChanged;
	mutable Boolean
		__bViewProjChanged;
	mutable Boolean
		__bViewProjTranspChanged;
	mutable Boolean
		__bFrustumChanged;

	mutable Plane 
		__cachedFrustumPlanes[6];

	mutable Matrix4 
		__cachedViewMatrix;
	Matrix4
		__cachedProjMatrix;
	mutable Matrix4
		__cachedViewProjMatrix;

	mutable Matrix4
		__cachedViewProjTransp;

};

//DECLARE_MAP_TYPE_PTR(map, String, Camera)
typedef SharedPtr<Camera>	CameraPtr;
DECLARE_MAP_TYPE(map, String, CameraPtr);

_X3D_NS_END