#pragma once

#include "x3d/render/RenderObject.h"
#include "x3d/graphic/VertexBuffer.h"
#include "x3d/graphic/IndexBuffer.h"

#include "x3d/resource/PrefabMesh.h"

_X3D_NS_BEGIN

class Entity;

class PrefabRenderable : public RendableObject
					, public Final<PrefabRenderable>
{
public:
	PrefabRenderable(RenderSystem* pRenderSystem);
	~PrefabRenderable();


public:
	virtual transient const Material*
		GetMaterial(void) const { return _pMaterial; }

	void 
		Init(MeshPtr& pSphereMesh, Entity* pParentEntity);

	// Use this property to apply a base transformation to a PrefabRenderable conveniently.
	void
		SetBaseTransform(const Matrix4& baseTransform) { _baseTransform = baseTransform; }
	const Matrix4&
		GetBaseTransform() { return _baseTransform; }

	virtual void
		GetWorldTransforms(Matrix4* pMatrixBuffer) const;
	virtual const DrawPrimitiveCommand*
		GetDrawPrimitiveCmd() const;


protected:
	GraphicSystem*
		_pGraphicSystem;

	PrefabMeshPtr
		_pPrefabMeshPtr;

	Entity*
		_pParentEntity;

	MaterialPtr
		_pMaterial;
	DrawPrimitiveCommand
		_drawPrimitiveCmd;

	Matrix4 
		_baseTransform;
};

typedef SharedPtr<PrefabRenderable> PrefabRendablePtr;

_X3D_NS_END