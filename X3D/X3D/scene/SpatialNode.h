#pragma once

_X3D_NS_BEGIN

/*
	Abstract base class for all spatial node(such as OctreeNode, BSPNode etc).
*/
class _X3DExport SpatialNode
{
public:
	SpatialNode();
	virtual ~SpatialNode();


public:
	INLINE AxisAlignedBox&
		GetWorldAABB(){ return _worldAABB; }


	// ---
	INLINE void
		setWorldAABB(const Vector3& minimun, const Vector3& maximun)
	{
		_worldAABB.SetMinimum(minimun.x, minimun.y, minimun.z);
		_worldAABB.SetMaximum(maximun.x, maximun.y, maximun.z);
	}

	BOOL isTwiceSize(const AxisAlignedBox &box) const
	{
		// infinite boxes never fit in a child - always root node
		XASSERT(!box.IsInfinite());

		Vector3 halfMBoxSize = _worldAABB.GetHalfSize();
		Vector3 boxSize = box.GetSize();
		return ((boxSize.x <= halfMBoxSize.x) && (boxSize.y <= halfMBoxSize.y) && (boxSize.z <= halfMBoxSize.z));
	}


protected:
	// The bounding box of this spatial node. Use this box to determine whether a scene node
	// is in this spatial node.
	AxisAlignedBox 
		_worldAABB;

};

_X3D_NS_END