#include "StdAfx.h"
#include "SubMeshRenderable.h"

#include "X3D/material/MaterialManager.h"
#include "x3d/scene/Entity.h"
#include "x3d/resource/SubMesh.h"

_X3D_NS_BEGIN

SubMeshRendable::SubMeshRendable(RenderSystem* pRenderSystem)
: RendableObject(pRenderSystem)
{
	_pSubMesh = NULL;
}

SubMeshRendable::~SubMeshRendable(void)
{
	_pSubMesh = NULL;
}

void SubMeshRendable::Init(SubMesh* pSubMesh, Entity* pParentEntity)
{
	_pSubMesh = pSubMesh;
	_pParentEntity = pParentEntity;
	_pMaterial = MaterialManager::Instance().AddMaterial(_pSubMesh->GetMaterialName());

	_drawPrimitiveCmd = pSubMesh->GetDrawPrimitiveCmd();
}

const DrawPrimitiveCommand* SubMeshRendable::GetDrawPrimitiveCmd() const
{
	return &_drawPrimitiveCmd;
}

void SubMeshRendable::GetWorldTransforms(Matrix4* pMatrixBuffer) const
{
/*
	Matrix4 rotateMatrix;
	D3DXMatrixRotationY((D3DXMATRIX*)&rotateMatrix, PI);

	D3DXQUATERNION d3dxQuaternion;
	D3DXQuaternionRotationMatrix(&d3dxQuaternion, (D3DXMATRIX*)&rotateMatrix);

	Quaternion quaternion;
	quaternion.FromRotationMatrix(rotateMatrix);
*/
/*
	float fSqrt = Math::Sqrt(0.5f);

	Matrix4 matrix = Matrix4::IDENTITY;
	matrix._11 = fSqrt;
	matrix._12 = 0;
	matrix._13 = fSqrt;

	matrix._31 = -fSqrt;
	matrix._32 = 0;
	matrix._33 = fSqrt;

	*pMatrixBuffer = matrix; 

*/
	*pMatrixBuffer = *_pParentEntity->getWorldTransform();
}

_X3D_NS_END