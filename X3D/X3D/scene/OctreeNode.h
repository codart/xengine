#include "x3d/scene/SpatialNode.h"

_X3D_NS_BEGIN

no_v_base class OctreeNode : public SpatialNode
{
	friend class SceneSpatialMgr;

public:
	OctreeNode(OctreeNode* pParent);
	~OctreeNode();


public:
	INLINE const SceneNodeList&
		GetSceneNodeList() { return _sceneNodeList; }

	///number of SceneNodes in this octree and all its children.
	INLINE int 
		GetSceneNodeCount() const { return _nNumSceneNode; } 

	INLINE void 
		AttachSceneNode(SceneNode* pSceneNode)
	{
		pSceneNode->setSpatialNode(this);
		_sceneNodeList.push_back(pSceneNode);

		_ref();
	}

	INLINE void
		DetachSceneNode(SceneNode* pSceneNode)
	{
		pSceneNode->setSpatialNode(NULL);
		_sceneNodeList.erase( std::find(_sceneNodeList.begin(), _sceneNodeList.end(), pSceneNode));
		
		_unref();
	}

	//---
	INLINE void getCullBounds( AxisAlignedBox *b ) const
	{
		b ->SetExtents( _worldAABB.GetMinimum() - _vHalfSize, _worldAABB.GetMaximum() + _vHalfSize );
	}


protected:
	OctreeNode* 
		_children[2][2][2];

	// Scene nodes that attached to this octree node.
	SceneNodeList
		_sceneNodeList;

	OctreeNode*
		_pParentNode;

	///number of SceneNodes in this octree and all its children.
	int 
		_nNumSceneNode;

    /** Vector containing the dimensions of this octree / 2
    */
    Vector3 
		_vHalfSize;


public:
   /** Increments the overall node count of this octree and all its parents
    */
    INLINE void _ref()
    {
        _nNumSceneNode++;
        if ( _pParentNode != 0 ) _pParentNode -> _ref();
    };

    /** Decrements the overall node count of this octree and all its parents
    */
    INLINE void _unref()
    {
        _nNumSceneNode--;
        if ( _pParentNode != 0 ) _pParentNode -> _unref();
    };

	void 
		_getChildIndicesByAABB( const AxisAlignedBox &box, int *x, int *y, int *z ) const;

};


_X3D_NS_END