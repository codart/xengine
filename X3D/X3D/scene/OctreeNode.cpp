#include "stdafx.h"
#include "OctreeNode.h"

_X3D_NS_BEGIN

OctreeNode::OctreeNode(OctreeNode* pParentNode)
: _vHalfSize(0, 0, 0)
{
	_worldAABB = AxisAlignedBox::BOX_NULL;
	_pParentNode = pParentNode;
	_nNumSceneNode = 0;

	memset(_children, 0, sizeof(OctreeNode*) * 8);
}

OctreeNode::~OctreeNode()
{
	
}

void OctreeNode::_getChildIndicesByAABB( const AxisAlignedBox &box, int *x, int *y, int *z ) const
{
	Vector3 max = _worldAABB.GetMaximum();
	Vector3 min = box.GetMinimum();

	Vector3 center = _worldAABB.GetMaximum().MidPoint( _worldAABB.GetMinimum() );

	Vector3 ncenter = box.GetMaximum().MidPoint( box.GetMinimum() );

	if ( ncenter.x > center.x )
		* x = 1;
	else
		*x = 0;

	if ( ncenter.y > center.y )
		* y = 1;
	else
		*y = 0;

	if ( ncenter.z > center.z )
		* z = 1;
	else
		*z = 0;

}

_X3D_NS_END