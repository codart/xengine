#include "stdafx.h"
#include "PrefabRenderable.h"

#include "x3d/render/RenderSystem.h"
#include "x3d/resource/MeshManager.h"
#include "x3d/material/MaterialManager.h"
#include "x3d/scene/Entity.h"

_X3D_NS_BEGIN
PrefabRenderable::PrefabRenderable(RenderSystem* pRenderSystem)
: RendableObject(pRenderSystem)
, _baseTransform(Matrix4::IDENTITY)
{
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();
	_pMaterial = MaterialManager::Instance().AddMaterial(TEXT("SolidWhite")); 
}

PrefabRenderable::~PrefabRenderable()
{

}

void PrefabRenderable::Init(MeshPtr& pSphereMesh, Entity* pParentEntity)
{
	_pPrefabMeshPtr = pSphereMesh;
	_pParentEntity = pParentEntity;

	_drawPrimitiveCmd = _pPrefabMeshPtr->GetDrawPrimitiveCmd();
}

const DrawPrimitiveCommand* PrefabRenderable::GetDrawPrimitiveCmd() const
{
	return &_drawPrimitiveCmd;
}

void PrefabRenderable::GetWorldTransforms(Matrix4* pMatrixBuffer) const
{
	*pMatrixBuffer = _baseTransform * (*_pParentEntity->getWorldTransform());
}

_X3D_NS_END