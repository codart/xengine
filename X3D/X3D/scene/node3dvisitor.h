#pragma once

_X3D_NS_BEGIN

class Node3D;

class Node3DVisitor
{
public:
	virtual void Visit(Node3D* pNode3D) = 0;

};

_X3D_NS_END