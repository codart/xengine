#pragma once

#include "x3d/scene/Entity.h"
#include "x3d/scene/PrefabRenderable.h"

_X3D_NS_BEGIN

/// Note: PrefabEntity can have a Base Transform applied to the PrefabRendable. Use this feature we can
/// easily compose a complex entity with many PrefabRendable.
class _X3DExport PrefabEntity : public Entity
{
public:
	PrefabEntity(const String& strName, MeshPtr& pPrefabMesh, RenderSystem* pRenderSystem);
	virtual ~PrefabEntity(void);


public:
	virtual PrefabType
		GetType() { return _prefabType; }
	virtual const AxisAlignedBox& 
		GetBoundingBox(void) const;
	void
		SetBaseTransform(const Matrix4& baseTransform) { _pPrefabRenderable->SetBaseTransform(baseTransform); }
	const Matrix4&
		GetBaseTransform() { return _pPrefabRenderable->GetBaseTransform(); }

	//---
	temporary override void
		fillRenderQueue(RenderQueue* pRenderQueue);


public:
	// Bounding box that contains all the mesh of each child entity
	mutable AxisAlignedBox 
		_boundingBox;

	PrefabRenderable*
		_pPrefabRenderable;

	PrefabType
		_prefabType;

	//ParameterList
	//	_parameterList;

};



_X3D_NS_END