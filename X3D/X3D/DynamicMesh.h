#pragma once

class DynamicMeshEntity
{
public:
protected:
private:
};

class DynamicSubMesh
{
public:

protected:

};

/*
	A mesh can be modify on the fly. Based on the dynamic vertex buffer internally.
*/
class DynamicMesh
{
public:
	DynamicMesh(void);
	~DynamicMesh(void);
};
