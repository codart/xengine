#pragma once


_X3D_NS_BEGIN

class RenderWindowEvent : public Event
{
public:
	enum RenderWindowEventType
	{
		RENDER_WINDOW_EVENT_START = 3100,
		WINDOW_FRAME_TICK,
		RENDER_WINDOW_EVENT_END,
	};

	RenderWindowEvent(int nType) : Event(nType)
	{}

	static BOOL IsValidEvent(int nType)
	{
		return nType > RENDER_WINDOW_EVENT_START && nType < RENDER_WINDOW_EVENT_END;
	}

public:


};

_X3D_NS_END


