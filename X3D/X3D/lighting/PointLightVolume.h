#pragma once

#include "LightVolume.h"
#include "x3d/scene/PrefabRenderable.h"
#include "x3d/resource/RenderTexture.h"

_X3D_NS_BEGIN


class PointLightVolume : public LightVolume
{
public:
	PointLightVolume(RenderSystem* pRenderSystem);
	virtual ~PointLightVolume(void);

public:
	override void 
		Init(Light* pLight);

	override void
		RenderLightBuffer(transient RenderContext* pRenderContext);

	temporary void
		BeginRenderLightVolumes(transient RenderContext* pRenderContext);


protected:
	INLINE void 
		_drawLightSphere();


protected:
	GraphicSystem*
		_pGraphicSystem;

	DrawPrimitiveCommand
		_drawPrimitiveCmd;

	TextureState 
		_GBufferTexState;
	RenderTexturePtr
		_pLightProjectTexture;
	TextureUnitState
		_pLightProjectTexUnitState;

	// Use a sphere to indicate point light's area.
	PrefabMeshPtr
		_lightSphere;

	VertexBuffer*
		_pLightSphereVB;
	IndexBuffer*
		_pLightSphereIB;
	VertexLayout*
		_pVertexLayout;

	Shader*
		_pEvalPointLightBufShader;

};

_X3D_NS_END