#pragma once

#include "x3d\scene\rendableobject.h"

_X3D_NS_BEGIN

class Light;

/*
	Light Volume is used for light-pre-pass light buffer calculation.
*/
class LightVolume : public RendableObject
{
public:
	LightVolume(RenderSystem* pRenderSystem);
	~LightVolume(void);


public:
	virtual void 
		Init(Light* pLight);
	virtual void
		GetWorldTransforms(Matrix4* pMatrixBuffer) const;

	virtual void
		RenderLightBuffer(transient RenderContext* pRenderContext) { XASSERT(0); }


protected:
	Light*
		_pLight;

};

_X3D_NS_END