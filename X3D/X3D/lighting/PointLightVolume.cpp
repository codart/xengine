#include "StdAfx.h"
#include "PointLightVolume.h"

#include "x3d/render/RenderSystem.h"

#include "x3d/resource/GpuProgramManager.h"
#include "x3d/resource/MeshManager.h"

#include "x3d/graphic/VertexBuffer.h"
#include "x3d/graphic/IndexBuffer.h"
#include "x3d/render/AutoParameterDataProvider.h"
#include "x3d/resource/TextureManager.h"
#include "x3d/graphic/GraphicSystem.h"
#include "x3d/resource/IndexData.h"
#include "x3d/resource/VertexData.h"

_X3D_NS_BEGIN

PointLightVolume::PointLightVolume(RenderSystem* pRenderSystem)
: LightVolume(pRenderSystem)
{
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();

	_pLightProjectTexture = TextureManager::Instance().AddTexture(TEXT("lightProjectTexture.dds"));
	_pLightProjectTexUnitState.AddTextureFrame(_pLightProjectTexture);

	_pLightSphereVB = NULL;
	_pLightSphereIB = NULL;
	_pVertexLayout = NULL;
}

PointLightVolume::~PointLightVolume(void)
{
	SAFE_DELETE(_pEvalPointLightBufShader);
}

void PointLightVolume::RenderLightBuffer(transient RenderContext* pRenderContext)
{
	AutoParameterDataProvider& autoParamsProvider = pRenderContext->GetAutoParamDataProvider();
	autoParamsProvider.SetCurrentRenderableObject(this);
	_pEvalPointLightBufShader->UpdateAutoParameters(&autoParamsProvider);
	_drawLightSphere();
}

void PointLightVolume::BeginRenderLightVolumes(transient RenderContext* pRenderContext)
{
	GraphicSystem* pGraphicSystem = _pRenderSystem->GetGraphicSystem();

	pGraphicSystem->SetShader(_pEvalPointLightBufShader);

	pGraphicSystem->SetTexture(0, &pRenderContext->pRenderWindow->getGBuffer());
	pGraphicSystem->SetTextureState(0, &_GBufferTexState);
	pGraphicSystem->SetTextureUnitState(1, &_pLightProjectTexUnitState);

	_pGraphicSystem->SetVertexBuffer(0, _pLightSphereVB);
	_pGraphicSystem->SetIndexBuffer(_pLightSphereIB);
	_pGraphicSystem->SetVertexLayout(_pVertexLayout);
}

void PointLightVolume::_drawLightSphere()
{
	// Not use DrawPrimitive(DrawPrimitiveCommand) because it is lower performance than 
	// DrawPrimitive(DrawPrimitiveType primitiveType, int nVertexCount, int iStartIndex, int nPrimitiveCount)
	_pGraphicSystem->DrawPrimitive(_drawPrimitiveCmd.GetDrawPrimitiveType()
									, _drawPrimitiveCmd.GetVertexData()->GetVertexCount()
									, 0, _drawPrimitiveCmd.GetPrimitiveCount());
}

void PointLightVolume::Init(Light* pLight)
{
	LightVolume::Init(pLight);

	_lightSphere = MeshManager::Instance().AddPrefabMesh(TEXT("SharedLightSphere"), PREFAB_TYPE_SPHERE);
	_drawPrimitiveCmd = _lightSphere->GetDrawPrimitiveCmd();
	_pLightSphereVB = _drawPrimitiveCmd.GetVertexData()->GetVertexBuffer(0);
	_pLightSphereIB = _drawPrimitiveCmd.GetIndexData()->GetIndexBuffer();
	_pVertexLayout = _drawPrimitiveCmd.GetVertexData()->GetVertexLayout();

	GraphicSystem* pGraphicSystem = _pRenderSystem->GetGraphicSystem();

	// --- Create shader
	VertexProgramPtr pVS = GPUProgramManager::Instance().AddVertexProgram(TEXT("EvalPointLightVS"));
	FragmentProgramPtr pPS = GPUProgramManager::Instance().AddFragmentProgram(TEXT("EvalPointLightFS"));

	_pEvalPointLightBufShader = XNEW Shader(pVS, pPS);
}

_X3D_NS_END