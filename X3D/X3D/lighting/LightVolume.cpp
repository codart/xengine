#include "StdAfx.h"
#include "LightVolume.h"

_X3D_NS_BEGIN

LightVolume::LightVolume(RenderSystem* pRenderSystem)
: RendableObject(pRenderSystem)
{
	_pLight = NULL;
}

LightVolume::~LightVolume(void)
{
}

void LightVolume::Init(Light* pLight)
{
	_pLight = pLight;
}

void LightVolume::GetWorldTransforms(Matrix4* pMatrixBuffer) const
{
	XASSERT(_pLight);
	*pMatrixBuffer = *_pLight->getWorldTransform();
}


_X3D_NS_END