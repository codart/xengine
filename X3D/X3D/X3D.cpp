// dllmain.cpp : Implementation of DllMain.
#include <stdlib.h>
#include <crtdbg.h>

#include "stdafx.h"
#include "X3DModule.h"


// DLL Entry Point
x3d::X3DModule* __pX3DModule = NULL;
int __bX3DModuleState = 0;	// 0 : uninit, 1: inited, -1: destroyed
HINSTANCE __hInstance = NULL;

extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	HRESULT hRes = 0;

	//_CrtSetBreakAlloc(146);

	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		GetCore()->RegisterModule(CreateX3DModule, DestroyX3DModule, TRUE);
		__hInstance = hInstance;
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		//SAFE_DELETE(__pX3DModule);
		//_CrtDumpMemoryLeaks();
		break;
	}
	return TRUE;
}

extern "C"
{
	_X3DExport common::IModule* CreateX3DModule()
	{
		XASSERT(!__pX3DModule);
		__pX3DModule = XNEW x3d::X3DModule(DWORD_PTR(__hInstance));
		return __pX3DModule;
	}

	_X3DExport x3d::X3DModule* GetX3DModule()
	{
		return __pX3DModule;
	}

	_X3DExport void DestroyX3DModule()
	{
		SAFE_DELETE(__pX3DModule);
	}
}
