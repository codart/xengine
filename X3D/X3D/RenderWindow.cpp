#include "StdAfx.h"
#include "RenderWindow.h"

#include "Common/plantform/WindowEvent.h"
#include "Common/input/_InputManager.h"
#include "x3d/graphic/WindowRenderTarget.h"
#include "x3d/render/RenderSystem.h"
#include "x3d/render/SceneRender.h"
#include "x3d/render/WindowBindedResource.h"
#include "x3d/graphic/GraphicSystem.h"
#include "x3d/RenderWindowEvent.h"
#include "Common/script/ScriptBinder.h"

_X3D_NS_BEGIN

void RenderWindow::ExportToScript(ScriptBinder* pScriptBinder)
{
	pScriptBinder->BindClass<Mouse>("Mouse");
	typedef int (Mouse::*FuncAddEventListenerEx)(int, const char*);
	FuncAddEventListenerEx funcAddEventListenerEx = &Mouse::AddEventListener;
	pScriptBinder->BindMethod<Mouse>("AddEventListener", funcAddEventListenerEx);

	pScriptBinder->BindClass<RenderWindow>("RenderWindow");
	pScriptBinder->BindMethod<RenderWindow>("ShowWindow", &RenderWindow::ShowWindow);
	pScriptBinder->BindMethod<RenderWindow>("GetMouse", &RenderWindow::GetMouse);
}

RenderWindow::RenderWindow(RenderSystem* pRenderSystem, const String& strName)
: RenderObject(pRenderSystem)
, _strName(strName)
{
	_pRenderSystem = pRenderSystem;
	_pGraphicSystem = pRenderSystem->GetGraphicSystem();

	//... Use factory pattern later
	_pWindow = XNEW Window(this);
	_pWndRenderTarget = NULL;

	_pMouse = NULL;
	_pKeyboard = NULL;

	_pSceneRender = XNEW SceneRender(pRenderSystem);
	_pWndBindedRes = XNEW WindowBindedResource(pRenderSystem, this);
}

RenderWindow::~RenderWindow(void)
{
	RenderAreaIterator renderAreaIt(_renderAreaList);

	while (renderAreaIt.HasMore())
	{
		_deleteRenderArea(renderAreaIt.CurrentData());
		renderAreaIt.Next();
	}

	SAFE_DELETE(_pWndRenderTarget);
	SAFE_DELETE(_pWndBindedRes);
	SAFE_DELETE(_pSceneRender);
	SAFE_DELETE(_pWindow);
}

void RenderWindow::update()
{
	XASSERT(_pWndRenderTarget && _pGraphicSystem);

	if(!_pGraphicSystem->IsReady()) return;

	//_pGraphicSystem->BeginUpdateWindow(_pWndRenderTarget);
	_pGraphicSystem->BeginDrawing();

	FASTEST_VECTOR_ITERATE(RenderArea*, _renderAreaList);
		GET_NEXT(_renderAreaList)->update();
	FASTEST_ITERATE_END();

	_pGraphicSystem->EndDrawing();
	//_pGraphicSystem->EndUpdateWindow();

	// Tick here can make CPU be parallelized with GPU maximized!!!
	// EndDrawing will trigger GPU to do rendering asynchronous!!
	RenderWindowEvent e(RenderWindowEvent::WINDOW_FRAME_TICK);
	DispatchEvent(e);

	// Present will make CPU sync with GPU!!!
	DWORD dwRet = _pWndRenderTarget->Present();
	XASSERT(dwRet == S_OK || dwRet == D3DERR_DEVICELOST);
}

int RenderWindow::GetWidth()
{
	XASSERT(_pWindow);
	return _pWindow->GetWidth();
}

int RenderWindow::GetHeight()
{
	XASSERT(_pWindow);
	return _pWindow->GetHeight();
}

void RenderWindow::OnPaint(const Rect* pPaintRect)
{
	if(!_pWndRenderTarget) return;
	update();
}

BOOL RenderWindow::OnClose()
{
	 _pWindow->ShowWindow(false);
	return TRUE;
}

void RenderWindow::CreateWnd(const String& strWindowTitle, const Rect& rect)
{
	XASSERT(_pWindow);
	BOOL bRet = _pWindow->Create(strWindowTitle, rect);
	if(!bRet)
	{
		XTHROWEX("CreateWnd Window failed!!!");
	}

	_pMouse = InputManager::Instance().AddInputObject(INPUT_OBJ_MOSUE, _pWindow->GetWindowHandle());
	_pKeyboard = InputManager::Instance().AddInputObject(INPUT_OBJ_KEYBOARD, _pWindow->GetWindowHandle());

	_pMouse->EnableScriptSupport(TRUE, GetX3DModule()->GetScriptBinder());
	_pKeyboard->EnableScriptSupport(TRUE, GetX3DModule()->GetScriptBinder());
}

void RenderWindow::DestroyWnd()
{
	 InputManager::Instance().RemoveInputObject(_pMouse);
	 InputManager::Instance().RemoveInputObject(_pKeyboard);
	
	 _pMouse = NULL;
	 _pKeyboard = NULL;

	XASSERT(_pWindow);
	_pWindow->Destroy();
}

void RenderWindow::ShowWindow(BOOL bShow)
{
	XASSERT(_pWindow);
	_pWindow->ShowWindow(bShow);
}

RenderArea* RenderWindow::AddRenderArea(const CameraPtr& pAttachedCamera, const ScenePtr& pAttachedScene
										, int nZOrder, Rect rect)
{
	XASSERT(_pWindow);

	RenderArea* pRenderArea = GetRenderArea(nZOrder);
	if(pRenderArea) return pRenderArea;

	XASSERT(rect.left >= 0 && rect.Width() <= GetWidth());
	XASSERT(rect.top >= 0 && rect.Height() <= GetHeight());

	if(rect.IsRectEmpty())
	{
		rect.SetRect(0, 0, GetWidth(), GetHeight());
	}

	RenderArea* pNewRenderArea = new RenderArea(_pRenderSystem, this
												, pAttachedCamera, pAttachedScene
												, rect, nZOrder);
	_renderAreaList.push_back(pNewRenderArea);

	_pWindow->AddEventListener(WindowEvent::WINDOW_SIZE, MFUNC(&RenderArea::_onWindowSizeChanged, pNewRenderArea));

	_sortRenderAreaByZOrder();

	return pNewRenderArea;
}

int RenderWindow::_compareRenderArea(RenderArea* p1, RenderArea* p2)
{
	XASSERT(p1 && p2);
	if(p1->GetZOrder() > p2->GetZOrder()) return 1;
	return 0;
}

void RenderWindow::_sortRenderAreaByZOrder()
{
	std::sort(_renderAreaList.begin(), _renderAreaList.end(), _compareRenderArea);
}

RenderArea* RenderWindow::GetRenderArea(int nZOrder)
{
	RenderAreaIterator iterator(_renderAreaList); 

	while(iterator.HasMore())
	{
		RenderArea* pRenderArea = iterator.CurrentData();
		if(pRenderArea->GetZOrder() == nZOrder)
		{
			return pRenderArea;
		}
		iterator.Next();
	}
	return NULL;
}

void RenderWindow::RemoveRenderArea(int nZOrder)
{
	RenderAreaIterator iterator(_renderAreaList); 
	RenderAreaListIt delRenderAreaIt;
	while(iterator.HasMore())
	{
		RenderArea* pRenderArea = iterator.CurrentData();
		if(pRenderArea->GetZOrder() == nZOrder)
		{
			delRenderAreaIt = iterator.CurrentIt();
			break;
		}
		iterator.Next();
	}

	if(delRenderAreaIt != _renderAreaList.end())
	{
		_deleteRenderArea(*delRenderAreaIt);
		_renderAreaList.erase(delRenderAreaIt);
	}

	_sortRenderAreaByZOrder();
}

void RenderWindow::RemoveRenderArea(RenderArea* pDestRenderArea)
{
	RenderAreaIterator iterator(_renderAreaList); 
	RenderAreaListIt delRenderAreaIt;
	while(iterator.HasMore())
	{
		RenderArea* pRenderArea = iterator.CurrentData();
		if(pRenderArea == pDestRenderArea)
		{
			delRenderAreaIt = iterator.CurrentIt();
			break;
		}
		iterator.Next();
	}

	if(delRenderAreaIt != _renderAreaList.end())
	{
		_deleteRenderArea(*delRenderAreaIt);
		_renderAreaList.erase(delRenderAreaIt);
	}

	_sortRenderAreaByZOrder();
}

void RenderWindow::_deleteRenderArea(RenderArea* pRenderArea)
{
	XASSERT(_pWindow);
	_pWindow->RemoveEventListener(WindowEvent::WINDOW_SIZE
					, MFUNC(&RenderArea::_onWindowSizeChanged, pRenderArea));

	XDELETE pRenderArea;
}

void RenderWindow::attachWndRenderTarget(WindowRenderTarget* pWindowRenderTarget)
{
	XASSERT(_pWndRenderTarget == 0);
	_pWndRenderTarget = pWindowRenderTarget;

	_pWindow->AddEventListener(WindowEvent::WINDOW_SIZE
								, MFUNC(&WindowRenderTarget::_onWindowSizeChanged, _pWndRenderTarget));
}

_X3D_NS_END