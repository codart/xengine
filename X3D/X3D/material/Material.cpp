#include "StdAfx.h"
#include "Material.h"

#include "Common/util/Unicode.h"
#include "Common/util/xmlDocument.h"
#include "Common/util/xmlNodeHandle.h"
#include "Common/util/XmlAttributeHandle.h"

#include "x3d/resource/TextureManager.h"
#include "x3d/resource/GpuProgramManager.h"

_X3D_NS_BEGIN


Material::Material(void)
{
	_renderStyle = RENDER_STYLE_OPAQUE;
}

Material::~Material(void)
{
	_releaseAllTechniques();
}

Technique* Material::GetBestTechnique() const
{
	XASSERT(_techniqueList.size());
	return _techniqueList[0];
}

void Material::_releaseAllTechniques()
{
	// Release all technique
	TechniqueListIt it = _techniqueList.begin();
	TechniqueListIt itEnd = _techniqueList.end();

	for ( ; it != itEnd; ++it)
	{
		Technique* pTechnique = *it;
		XASSERT(pTechnique);
		delete pTechnique;
	}
}

BOOL Material::intMtlFromBitmap(const String& strBitmapFile)
{
	XASSERT(_techniqueList.size() == 0);

	TexturePtr pTexture = TextureManager::Instance().AddTexture(strBitmapFile);
	if(!pTexture)
	{
		return FALSE;
	}

	Technique* pTechnique = XNEW Technique;
	_techniqueList.push_back(pTechnique);

	Pass* pPass = pTechnique->AddPass();
	pPass->SetDepthEnable(FALSE);
	pPass->SetDepthWrite(FALSE);

	TextureUnitState* pTexUnitState = XNEW TextureUnitState;
	pTexUnitState->AddTextureFrame(pTexture);
	pPass->AddTextureUnitState(pTexUnitState);

	VertexProgramPtr pVS = GPUProgramManager::Instance().AddVertexProgram(TEXT("DefaultHUDQuadVS"));
	FragmentProgramPtr pFS = GPUProgramManager::Instance().AddFragmentProgram(TEXT("DefaultHUDQuadFS"));
	pPass->SetShader(pVS, pFS);

	return TRUE;
}

BOOL Material::intMtlFromTexture(const TexturePtr& pTexture)
{
	XASSERT(_techniqueList.size() == 0);

	Technique* pTechnique = XNEW Technique;
	_techniqueList.push_back(pTechnique);

	Pass* pPass = pTechnique->AddPass();
	pPass->SetDepthEnable(FALSE);
	pPass->SetDepthWrite(FALSE);

	TextureUnitState* pTexUnitState = XNEW TextureUnitState;
	pTexUnitState->AddTextureFrame(pTexture);
	pPass->AddTextureUnitState(pTexUnitState);

	VertexProgramPtr pVS = GPUProgramManager::Instance().AddVertexProgram(TEXT("DefaultHUDQuadVS"));
	FragmentProgramPtr pFS = GPUProgramManager::Instance().AddFragmentProgram(TEXT("DefaultHUDQuadFS"));
	pPass->SetShader(pVS, pFS);

	return TRUE;
}

BOOL Material::LoadFromXmlNode(const XmlNodeHandle& materialNode)
{
	_releaseAllTechniques();

	USING_STRING_CONVERT;

	SetName(ConstAStrToStr(materialNode.FirstAttribute("name")));
	XmlAttributeHandle renderStyleNode = materialNode.FirstAttribute("render_style");
	if(renderStyleNode)
	{
		_renderStyle = __renderStyleFromString(renderStyleNode.GetValue());
	}

	GPUProgramManager& gpuProgramMgr = GPUProgramManager::Instance();

	// technique
	XmlNodeHandle techniqueNode = materialNode.FirstChild();
	XASSERT(strcmp(techniqueNode.GetName(), "technique") == 0);

	Technique* pTechnique = new Technique;
	_techniqueList.push_back(pTechnique);
	pTechnique->SetName(AStrToStr(techniqueNode.FirstAttribute().GetValue()));
	XASSERT(strcmp(techniqueNode.FirstAttribute().GetName(), "name") == 0);

	// Load passes
	XmlNodeHandle passNode = techniqueNode.FirstChild();
	XASSERT(strcmp(passNode.GetName(), "pass") == 0);

	while (passNode)
	{
		Pass* pPass = pTechnique->AddPass();
		pPass->SetName(ConstAStrToStr(passNode.FirstAttribute()));
		XASSERT(strcmp(passNode.FirstAttribute().GetName(), "name") == 0);

		__loadPassBasicParameters(pPass, passNode);

		// Read all texture unit state
		{
			XmlNodeHandle textureUnitNode = passNode.FirstChild("texture_unit");
			while(textureUnitNode)
			{
				TextureUnitState* pTexUnitState = new TextureUnitState;

				XmlAttributeHandle textureUnitType = textureUnitNode.FirstAttribute("type");
				if(!textureUnitType)
				{
					// By default, treat it as normal texture unit(texture unit for fragment program)
					pPass->AddTextureUnitState(pTexUnitState);
				}
				else 
				{
					if(strcmp(textureUnitType.GetValue(), "VertexTextureUnit") == 0)
					{
						pPass->AddVertexTextureUnitState(pTexUnitState);
					}
				}

				XmlNodeHandle textureNode = textureUnitNode.FirstChild("texture");
				XmlAttributeHandle textureType = textureNode.FirstAttribute("type");

				if(!textureType)
				{
					// If no texture type property, treat it is a simple texture file.
					TexturePtr& pTexturePtr = TextureManager::Instance().AddTexture(ConstAStrToStr(textureNode));
					pTexUnitState->AddTextureFrame(pTexturePtr);					
				}
				else if(strcmp(textureType.GetValue(), "RenderTexture") == 0)
				{
					PixelFormat format = PIXEL_FORMAT_A8R8G8B8;
					PixelFormat depthStencilFormat = PIXEL_FORMAT_D24S8;
					XmlAttributeHandle formatAttribute = textureNode.FirstAttribute("format");
					if(formatAttribute)
					{
						format = __texFormatFromString(formatAttribute.GetValue());
					}
					XmlAttributeHandle dsFormatAttribute = textureNode.FirstAttribute("depth_stencil_format");
					if(dsFormatAttribute)
					{
						depthStencilFormat = __texFormatFromString(dsFormatAttribute.GetValue());
					}


					TexturePtr pTexturePtr;

					String strTextureName = ConstAStrToStr(textureNode);
					if(strTextureName == TEXT("WindowGBuffer") || strTextureName == TEXT("WindowLightBuffer"))
					{
						pTexturePtr = TextureManager::Instance().GetRenderTexture(strTextureName);
					}
					else
					{
						pTexturePtr = TextureManager::Instance().AddRenderTexture( strTextureName
							, textureNode.FirstAttribute("width")
							, textureNode.FirstAttribute("height") 
							, format
							, depthStencilFormat);
					}
					pTexUnitState->AddTextureFrame(pTexturePtr);										
				}
				else if(strcmp(textureType.GetValue(), "SharedReflectMap") == 0)
				{
					// Not defined texture type, the material script is incorrect!!!
					XASSERT(0);
				}

				// Texture address mode
				XmlNodeHandle addressModeNode = textureUnitNode.FirstChild("tex_address_mode");
				if(addressModeNode)
				{
					TextureAddressMode addressMode = __textureModeFromString(addressModeNode.GetValue());
					//...
					pTexUnitState->SetAddressMode(addressMode, addressMode, addressMode);
				}
				// Texture border color
				XmlNodeHandle borderColor = textureUnitNode.FirstChild("tex_border_color");
				if(borderColor)
				{
					pTexUnitState->SetBorderColor(borderColor.ToHexInt());					
				}

				// Texture min filter
				XmlNodeHandle minFilterNode = textureUnitNode.FirstChild("tex_min_filter");
				if(minFilterNode)
				{
					TextureFilterType minFilterType = __textureFilterFromString(minFilterNode.GetValue());
					pTexUnitState->SetMinFilterType(minFilterType);
				}
				// Texture mag filter
				XmlNodeHandle magFilterNode = textureUnitNode.FirstChild("tex_mag_filter");
				if(magFilterNode)
				{
					TextureFilterType magFilterType = __textureFilterFromString(magFilterNode.GetValue());
					pTexUnitState->SetMagFilterType(magFilterType);
				}
				// Texture mip filter
				XmlNodeHandle mipFilterNode = textureUnitNode.FirstChild("tex_mip_filter");
				if(mipFilterNode)
				{
					TextureFilterType mipFilterType = __textureFilterFromString(mipFilterNode.GetValue());
					pTexUnitState->SetMipFilterType(mipFilterType);
				}

				textureUnitNode = textureUnitNode.NextSiblingNode("texture_unit");
			}
		}

		// Read shader definition
		{
			XmlNodeHandle shaderNode = passNode.FirstChild("shader");
			if(shaderNode)
			{
				// Read the new format first( VS/FS stored in shader node's attributes)
				XmlAttributeHandle vertexProgramAttrib = shaderNode.FirstAttribute("vertex_program");
				XASSERT(vertexProgramAttrib);
				XmlAttributeHandle fragmentProgramAttrib = shaderNode.FirstAttribute("fragment_program");
				XASSERT(fragmentProgramAttrib);
				pPass->SetShader(gpuProgramMgr.AddVertexProgram(ConstAStrToStr(vertexProgramAttrib))
								, gpuProgramMgr.AddFragmentProgram(ConstAStrToStr(fragmentProgramAttrib)) );

			}
			else
			{
				// Use default shader
				XASSERT(0);				
			}
		}

		passNode = passNode.NextSiblingNode();
	}

	return FALSE;
}

void Material::LoadFromFile(const String& strMaterialFile, const String& strMaterialName)
{
	USING_STRING_CONVERT;

	// Check what the file type is, binary or XML
	//...

	// LoadSingleMaterial XML file

	XmlDocument xmlDoc;
	xmlDoc.LoadFromFile(StrToAStr(strMaterialFile));

	XmlNodeHandle materialNode = xmlDoc.FirstChild("materials").FirstChild("material");
	while ( materialNode && strMaterialName != ConstAStrToStr(materialNode.FirstAttribute("name")) )
	{
		materialNode = materialNode.NextSiblingNode();
		XASSERT(strcmp(materialNode.GetName(), "material") == 0);
	}

	if(!materialNode)
	{
		AStringStream ss;
		ss << "Material::LoadSingleMaterial : No material with name " 
			<<WStrToAStr(strMaterialName.c_str())<< " found in" << WStrToAStr(strMaterialFile.c_str());
		XTHROWEX(ss.str().c_str());
	}

	LoadFromXmlNode(materialNode);
}

void Material::__loadPassBasicParameters(Pass* pPass, const XmlNodeHandle& passNode)
{
	XmlNodeHandle blendOpNode = passNode.FirstChild("frame_blend_op");
	if(blendOpNode)
	{
		const char* pszText = blendOpNode.GetValue();
		pPass->SetFrameBlendOperation(__blendOperationFromString(pszText));
	}

	XmlNodeHandle srcBlendFactorNode = passNode.FirstChild("src_blend");
	XmlNodeHandle destBlendFactorNode = passNode.FirstChild("dest_blend");

	FrameBlendFactor srcBlend = FACTOR_ONE;
	FrameBlendFactor destBlend = FACTOR_ONE;

	if(srcBlendFactorNode)
	{
		srcBlend = __blendFactorFromString(srcBlendFactorNode.GetValue());
	}
	if(destBlendFactorNode)
	{
		destBlend = __blendFactorFromString(destBlendFactorNode.GetValue());
	}

	pPass->SetFrameBlendFactor(srcBlend, destBlend);

	// --- Cull mode
	XmlNodeHandle cullNode = passNode.FirstChild("cull_hardware");
	pPass->SetHardwareCullMode(cullNode? __hardwareCullFromString(cullNode.GetValue()) : CULL_ANTICLOCKWISE);

	// --- Depth buffer params
	XmlNodeHandle depthEnable = passNode.FirstChild("depth_enable");
	pPass->SetDepthEnable(depthEnable? __BOOLFromString(depthEnable.GetValue()) : TRUE);	

	XmlNodeHandle depthWriteEnable = passNode.FirstChild("depth_write");
	pPass->SetDepthWrite(depthWriteEnable? __BOOLFromString(depthWriteEnable.GetValue()) : TRUE);	

	XmlNodeHandle depthCompareFunc = passNode.FirstChild("depth_compare_function");
	pPass->SetDepthCompareFunction(depthCompareFunc? __cmpFuncFromString(depthCompareFunc.GetValue()) : COMPARE_FUNC_LESS_EQUAL);	

	// --- Color buffer params
	XmlNodeHandle colorWriteEnable = passNode.FirstChild("color_write");
	pPass->SetColorBufferWrite(colorWriteEnable? __BOOLFromString(colorWriteEnable.GetValue()) : TRUE);	

	// --- Alpha test params
	XmlNodeHandle alphaTest = passNode.FirstChild("alpha_test");
	if(alphaTest)
	{
		pPass->SetAlphaTestEnable(__BOOLFromString(alphaTest.GetValue()));
		XmlNodeHandle alphaCmpareFunc = passNode.FirstChild("alpha_cmpare_func");
		XASSERT(alphaCmpareFunc);
		pPass->SetAlphaCompareFunc(__cmpFuncFromString(alphaCmpareFunc.GetValue()));
		XmlNodeHandle alphaRefValue = passNode.FirstChild("alpha_ref_value");
		XASSERT(alphaRefValue);
		pPass->SetAlphaRefValue(alphaRefValue);
	}
}

CompareFunction Material::__cmpFuncFromString(const char* pszCmpFunc)
{
	if( 0 == strcmp(pszCmpFunc, "always_fail")) return COMPARE_FUNC_ALWAYS_FAIL;
	else if( 0 == strcmp(pszCmpFunc, "always_pass")) return COMPARE_FUNC_ALWAYS_PASS;
	else if( 0 == strcmp(pszCmpFunc, "less")) return COMPARE_FUNC_LESS;
	else if( 0 == strcmp(pszCmpFunc, "less_equal")) return COMPARE_FUNC_LESS_EQUAL;
	else if( 0 == strcmp(pszCmpFunc, "equal")) return COMPARE_FUNC_EQUAL;
	else if( 0 == strcmp(pszCmpFunc, "not_equal")) return COMPARE_FUNC_NOT_EQUAL;
	else if( 0 == strcmp(pszCmpFunc, "greater_equal")) return COMPARE_FUNC_GREATER_EQUAL;
	else if( 0 == strcmp(pszCmpFunc, "greater")) return COMPARE_FUNC_GREATER;
	else XASSERT(0);

	return COMPARE_FUNC_ALWAYS_PASS;
}

BOOL Material::__BOOLFromString(const char* pszDepthWrite)
{
	if( 0 == strcmp(pszDepthWrite, "true")) return TRUE;
	else if( 0 == strcmp(pszDepthWrite, "false")) return FALSE;
	else { XASSERT(0); }

	return FALSE;
}

TextureFilterType Material::__textureFilterFromString(const char* pszFilterType)
{
	TextureFilterType type = TEX_FILTER_INVALID;

	if(0 == strcmp(pszFilterType, "none"))
	{
		type = TEX_FILTER_NONE;
	}
	else if(0 == strcmp(pszFilterType, "point"))
	{
		type = TEX_FILTER_POINT;
	}
	else if(0 == strcmp(pszFilterType, "linear"))
	{
		type = TEX_FILTER_LINEAR;
	}
	else if(0 == strcmp(pszFilterType, "anisotropic"))
	{
		type = TEX_FILTER_ANISOTROPIC;
	}
	else if(0 == strcmp(pszFilterType, "flatcubic"))
	{
		type = TEX_FILTER_FLATCUBIC;
	}
	else if(0 == strcmp(pszFilterType, "gaussiancubic"))
	{
		type = TEX_FILTER_GAUSSIANCUBIC;
	}

	XASSERT(type != TEX_FILTER_INVALID);
	return type;
}


TextureAddressMode Material::__textureModeFromString(const char* pszAddressMode)
{
	TextureAddressMode mode = TEX_ADDRESS_INVALID;
	if(0 == strcmp(pszAddressMode, "wrap"))
	{
		mode = TEX_ADDRESS_WRAP;
	}
	else if(0 == strcmp(pszAddressMode, "mirror"))
	{
		mode = TEX_ADDRESS_MIRROR;
	}
	else if(0 == strcmp(pszAddressMode, "clamp"))
	{
		mode = TEX_ADDRESS_CLAMP;
	}
	else if(0 == strcmp(pszAddressMode, "border"))
	{
		mode = TEX_ADDRESS_BORDER;
	}

	XASSERT(mode != TEX_ADDRESS_INVALID);
	return mode;
}

HardwareCullMode Material::__hardwareCullFromString(const char* pszString)
{
	HardwareCullMode cullMode = CULL_NONE;

	if(0 == strcmp(pszString, "none"))
	{
		cullMode = CULL_NONE;
	}
	else if(0 == strcmp(pszString, "clockwise"))
	{
		cullMode =  CULL_CLOCKWISE;
	}
	else if(0 == strcmp(pszString, "anticlockwise"))
	{
		cullMode =  CULL_ANTICLOCKWISE;
	}

	return cullMode;
}

FrameBlendFactor Material::__blendFactorFromString(const char* pszString)
{
	FrameBlendFactor blendFactor = FACTOR_ONE;

	if(0 == strcmp(pszString, "one"))
	{
		blendFactor = FACTOR_ONE;
	}
	else if(0 == strcmp(pszString, "zero"))
	{
		blendFactor =  FACTOR_ZERO;
	}
	else if(0 == strcmp(pszString, "dest_color"))
	{
		blendFactor =  FACTOR_DEST_COLOR;
	}
	else if(0 == strcmp(pszString, "src_color"))
	{
		blendFactor =  FACTOR_SRC_COLOR;
	}
	else if(0 == strcmp(pszString, "inv_dest_color"))
	{
		blendFactor =  FACTOR_INV_DEST_COLOR;
	}
	else if(0 == strcmp(pszString, "inv_src_color"))
	{
		blendFactor =  FACTOR_INV_SRC_COLOR;
	}
	else if(0 == strcmp(pszString, "dest_alpha"))
	{
		blendFactor =  FACTOR_DEST_ALPHA;
	}
	else if(0 == strcmp(pszString, "src_alpha"))
	{
		blendFactor =  FACTOR_SRC_ALPHA;
	}
	else if(0 == strcmp(pszString, "inv_dest_alpha"))
	{
		blendFactor =  FACTOR_INV_DEST_ALPHA;
	}
	else if(0 == strcmp(pszString, "inv_src_alpha"))
	{
		blendFactor =  FACTOR_INV_SRC_ALPHA;
	}

	return blendFactor;
}

FrameBlendOperation Material::__blendOperationFromString(const char* pszString)
{
	FrameBlendOperation blendOperation = FRAME_BLEND_ADD;

	if(0 == strcmp(pszString, "add"))
	{
		blendOperation = FRAME_BLEND_ADD;
	}
	else if(0 == strcmp(pszString, "subtract"))
	{
		blendOperation =  FRAME_BLEND_SUBTRACT;
	}
	else if(0 == strcmp(pszString, "reverse subtract"))
	{
		blendOperation =  FRAME_BLEND_REVERSE_SUBTRACT;
	}
	else if(0 == strcmp(pszString, "min"))
	{
		blendOperation =  FRAME_BLEND_MIN;
	}
	else if(0 == strcmp(pszString, "max"))
	{
		blendOperation =  FRAME_BLEND_MAX;
	}

	return blendOperation;
}

RenderStyle Material::__renderStyleFromString(const char* pszRenderStyle)
{
	RenderStyle renderStyle = RENDER_STYLE_INVALID;

	if(0 == strcmp(pszRenderStyle, "add"))
	{
		renderStyle = RENDER_STYLE_OPAQUE;
	}
	else if(0 == strcmp(pszRenderStyle, "transparent"))
	{
		renderStyle = RENDER_STYLE_TRANSPARENT;
	}

	XASSERT(renderStyle != RENDER_STYLE_INVALID);
	return renderStyle;
}

PixelFormat Material::__texFormatFromString(const char* pszFormat)
{
	PixelFormat format = PIXEL_FORMAT_UNKNOWN;

	if(0 == strcmp(pszFormat, "B8G8R8"))
	{
		format = PIXEL_FORMAT_B8G8R8;
	}
	else if(0 == strcmp(pszFormat, "A8R8G8B8"))
	{
		format = PIXEL_FORMAT_A8R8G8B8;
	}
	else if(0 == strcmp(pszFormat, "X8R8G8B8"))
	{
		format = PIXEL_FORMAT_X8R8G8B8;
	}
	else if(0 == strcmp(pszFormat, "FLOAT32_R"))
	{
		format = PIXEL_FORMAT_FLOAT32_R;
	}
	else if(0 == strcmp(pszFormat, "D24S8"))
	{
		format = PIXEL_FORMAT_D24S8;
	}
	else if(0 == strcmp(pszFormat, "D16"))
	{
		format = PIXEL_FORMAT_D16;
	}
	else if(0 == strcmp(pszFormat, "A16B16G16R16"))
	{
		format = PIXEL_FORMAT_A16B16G16R16F;
	}


	XASSERT(format != PIXEL_FORMAT_UNKNOWN);
	return format;
}


_X3D_NS_END