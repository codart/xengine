#pragma once

#include "X3D/material/Material.h"
#include "x3d/resource/ResourceManager.h"

_X3D_NS_BEGIN

/*
	Material is composed of Texture/GPUProgram resource. It means that material is related with 
	GraphicSystem indirectly but not directly. So MaterialManager is a GS loose related resource manager.

	GS tight related resource	--- Texture/GraphicBuffer(VertexBuffer/PixelBuffer)/RenderTarget/GPUProgram
	GS loose related resource	--- Material 

*/
class _X3DExport MaterialManager : public Singleton<MaterialManager, AutoRelease>
								, public ResourceManager
{
public:
	MaterialManager(void);
	virtual ~MaterialManager(void);

public:
	void
		LoadAllMaterials();

	MaterialPtr
		AddMaterial(const String& strMaterialName);
	MaterialPtr
		GetMaterial(const String& strMaterialName);

	/// For create HUD bitmap material
	MaterialPtr
		CreateMaterialFromBitmap(const String& strBitmapName);
	/// For create HUD canvas material
	MaterialPtr
		CreateMaterialFromTexture(const String& strMtlName, TexturePtr& ptrTexture);

	// ---
	void
		unInit();


protected:
	MaterialOwnerPtrMap
		_materialMap;


protected:
	void 
		_loadMaterialsFromFile(const String& strFileName);

};


_X3D_NS_END