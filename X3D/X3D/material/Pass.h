#pragma once

#include "X3D/material/TextureUnitState.h"
#include "x3d/material/Shader.h"
#include "x3d/graphic/GraphicDefs.h"

_X3D_NS_BEGIN

class Pass : public Final<Pass>
{
public:
	Pass(void);
	~Pass(void);


public:
	void 
		AddTextureUnitState(TextureUnitState* pState);
	TextureUnitState*
		GetTextureUnitState(UINT iIndex);
	INLINE const TextureUnitStateList&
		GetTextureUnitStates() { return _textureUnitStates; };

	void 
		AddVertexTextureUnitState(TextureUnitState* pState);
	TextureUnitState*
		GetVertexTextureUnitState(UINT iIndex);
	INLINE const TextureUnitStateList&
		GetVertexTextureUnitStates() { return _vertexTextureUnitStates; }

	UINT
		GetTextureUnitStateCount(){ return _textureUnitStates.size(); }
	UINT
		GetVertexTextureUnitStateCount() { return _vertexTextureUnitStates.size(); }

	INLINE void 
		SetName(const String& strName){_strName = strName;};
	INLINE const String&
		GetName();

	INLINE void
		SetFrameBlendFactor(FrameBlendFactor srcFactor, FrameBlendFactor destFactor)
					{_sourceBlendFactor = srcFactor; _destBlendFactor = destFactor;}
	INLINE void
		SetFrameBlendOperation(FrameBlendOperation blendOperation)
					{_frameBlendOperation = blendOperation;}

	INLINE FrameBlendOperation
		GetFrameBlendOperation(){return _frameBlendOperation;};
	INLINE FrameBlendFactor
		GetSrcBlendFactor(){return _sourceBlendFactor;};
	INLINE FrameBlendFactor
		GetDestBlendFactor(){return _destBlendFactor;};

	INLINE void
		SetHardwareCullMode(HardwareCullMode cullMode){_hardwareCullMode = cullMode;};
	INLINE HardwareCullMode
		GetHardwareCullMode(){return _hardwareCullMode;}

	INLINE void
		SetShader(VertexProgramPtr pVertexProgramPtr, FragmentProgramPtr pFragmentProgramPtr)
				{_pShader->SetVertexProgram(pVertexProgramPtr); _pShader->SetFragmentProgram(pFragmentProgramPtr);}
	INLINE Shader*
		GetShader(){return _pShader;};

	// Color buffer params
	INLINE BOOL
		GetColorBufferWrite() const { return _bColorWrite; }
	INLINE void
		SetColorBufferWrite(BOOL bEnable) { _bColorWrite = bEnable; }

	// Depth buffer params
	INLINE BOOL
		GetDepthEnable() const { return _bDepthEnable; }
	INLINE void
		SetDepthEnable(BOOL bEnable) { _bDepthEnable = bEnable; }
	INLINE BOOL
		GetDepthWrite() const  { return _bDepthWrite; }
	INLINE void
		SetDepthWrite(BOOL bEnable) { _bDepthWrite = bEnable; }
	INLINE CompareFunction
		GetDepthCompareFunction() {return _depthCmpareFunc; }
	INLINE void
		SetDepthCompareFunction(CompareFunction func) {_depthCmpareFunc = func; }

	// Stencil buffer params
	INLINE BOOL
		GetStencilEnable() { return _bEnableStencilTest; }
	INLINE CompareFunction
		GetStencilCompareFunc() { return _stencilCompareFunc; }
	INLINE UINT
		GetStencilRefValue() { return _stencilRefValue; }
	INLINE UINT
		GetStencilMask() { return _stencilMask; }
	INLINE StencilOperation
		GetStencilFailOp() { return _stencilFailOp; }
	INLINE StencilOperation
		GetStencilPassZFailOp() { return _stencilPassZFailOp; }
	INLINE StencilOperation
		GetStencilPassOp() { return _stencilPassOp; }
	INLINE BOOL
		GetTwoSizeStencilEnable() { return _bTwoSizeStencil; }

	// Alpha testing params
	INLINE void
		SetAlphaTestEnable(BOOL bEnable) { _bAlphaTest = bEnable; }
	INLINE void
		SetAlphaCompareFunc(CompareFunction func) { _alphaCompFunc = func; }
	INLINE void
		SetAlphaRefValue(BYTE alphaRefValue) { _alphaRefValue = alphaRefValue; }
	INLINE BOOL
		SetAlphaToCoverage(BOOL bAlphaToCoverage) { _bAlphaToCoverage = bAlphaToCoverage; }

	INLINE BOOL
		GetAlphaTestEnable() { return _bAlphaTest; }
	INLINE CompareFunction
		GetAlphaCompareFunc() { return _alphaCompFunc; }
	INLINE BYTE
		GetAlphaRefValue() { return _alphaRefValue; }
	INLINE BOOL
		GetAlphaToCoverage() { return _bAlphaToCoverage; }


protected:
	// --- Basic members ---------------------------------------------

	// Frame blending params
	FrameBlendOperation
		_frameBlendOperation;

	FrameBlendFactor 
		_sourceBlendFactor;
	FrameBlendFactor 
		_destBlendFactor;
	FrameBlendFactor 
		_sourceBlendFactorAlpha;
	FrameBlendFactor 
		_destBlendFactorAlpha;

	// Hardware cull mode
	HardwareCullMode
		_hardwareCullMode;

	// Color buffer params
	Boolean
		_bColorWrite;

	// Depth buffer params
	Boolean
		_bDepthEnable;
	Boolean 
		_bDepthWrite;
	CompareFunction
		_depthCmpareFunc;

	// Stencil buffer params
	Boolean
		_bEnableStencilTest;
	CompareFunction
		_stencilCompareFunc;
	Uint
		_stencilRefValue;
	Uint
		_stencilMask;
	StencilOperation
		_stencilFailOp;
	StencilOperation
		_stencilPassZFailOp;
	StencilOperation
		_stencilPassOp;
	Boolean
		_bTwoSizeStencil;

	// Alpha testing params
	Boolean
		_bAlphaTest;
	CompareFunction 
		_alphaCompFunc;
	Byte
		_alphaRefValue;
	Boolean
		_bAlphaToCoverage;

	// --- Complex members begin -------------------------------------------
	TextureUnitStateList
		_textureUnitStates;

	// Store the texture unit states for Vertex Texture Fetch.
	TextureUnitStateList
		_vertexTextureUnitStates;

	Shader*
		_pShader;

	String 
		_strName;


};

DECLARE_LIST_TYPE_PTR(vector, Pass);

_X3D_NS_END