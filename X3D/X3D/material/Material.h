#pragma once

#include "X3D/resource/Resource.h"
#include "Common/core/Final.h"
#include "x3d/material/Technique.h"


_X3D_NS_BEGIN

enum RenderStyle
{
	RENDER_STYLE_INVALID = -1,

	RENDER_STYLE_OPAQUE = 0,
	RENDER_STYLE_TRANSPARENT,
};


class _X3DExport Material : public Resource
				, public Final<Material>
{
protected:
	Material(void);

public:
	virtual ~Material(void);
	/// Ensure not allocated on stack
	static Material* Create() { return XNEW Material(); }

public:
	Technique*
		GetBestTechnique() const;

	INLINE Technique*
		GetTechnique(int iIndex) { return _techniqueList[iIndex]; };
	INLINE RenderStyle
		GetRenderStyle() const { return _renderStyle; }

	// ---
	// Load a material from a material file.
	// Note: One material file contains list of materials.
	void
		LoadFromFile(const String& strMaterialFile, const String& strMaterialName);

	BOOL
		LoadFromXmlNode(const XmlNodeHandle& materialNode);

	// ---
	BOOL 
		intMtlFromBitmap(const String& strBitmapFile);
	BOOL 
		intMtlFromTexture(const TexturePtr& pTexture);


protected:
	TechniqueList
		_techniqueList;
	RenderStyle
		_renderStyle;


protected:
	void 
		_releaseAllTechniques();


private:
	void 
		__loadPassBasicParameters(Pass* pPass, const XmlNodeHandle& passNode);

	CompareFunction 
		__cmpFuncFromString(const char* pszCmpFunc);
	BOOL
		__BOOLFromString(const char* pszDepthWrite);
	FrameBlendOperation 
		__blendOperationFromString(const char* pszString);
	FrameBlendFactor 
		__blendFactorFromString(const char* pszString);
	HardwareCullMode 
		__hardwareCullFromString(const char* pszString);
	TextureAddressMode
		__textureModeFromString(const char* pszAddressMode);
	TextureFilterType
		__textureFilterFromString(const char* pszFilterType);
	RenderStyle 
		__renderStyleFromString(const char* pszRenderStyle);
	PixelFormat 
		__texFormatFromString(const char* pszFormat);

};


typedef SharedPtr<Material> MaterialPtr;
DECLARE_MAP_TYPE(map, String, MaterialPtr);

typedef OwnerPtr<MaterialPtr> MaterialOwnerPtr;
DECLARE_MAP_TYPE(map, String, MaterialOwnerPtr);

_X3D_NS_END