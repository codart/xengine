#include "StdAfx.h"
#include "MaterialManager.h"

#include "Common/util/xmlDocument.h"
#include "Common/util/xmlNodeHandle.h"
#include "Common/util/XmlAttributeHandle.h"

#include "Common/plantform/FileFind.h"
#include "Common/util/Unicode.h"

_X3D_NS_BEGIN

MaterialManager::MaterialManager(void)
{

}

MaterialManager::~MaterialManager(void)
{

}

void MaterialManager::unInit()
{
	_materialMap.clear();
}

void MaterialManager::LoadAllMaterials()
{
	FileFind find;
	if(find.FindFile(MATERIAL_PATH + TEXT("*.xml")))
	{
		_loadMaterialsFromFile(find.GetFileName());

		while(find.FindNextFile())
		{
			_loadMaterialsFromFile(find.GetFileName());
		}
	}

}

void MaterialManager::_loadMaterialsFromFile(const String& strFileName)
{
	USING_STRING_CONVERT;
	
	XmlDocument xmlDoc;
	xmlDoc.LoadFromFile(StrToAStr(strFileName));

	XmlNodeHandle materialNode = xmlDoc.FirstChild("materials").FirstChild("material");

	while(materialNode)
	{
		Material* pMaterial = Material::Create();
		pMaterial->LoadFromXmlNode(materialNode);
		_materialMap[pMaterial->GetName()] = pMaterial;

		materialNode = materialNode.NextSiblingNode();
	}

	return;
}

MaterialPtr MaterialManager::AddMaterial(const String& strMaterialName)
{
	MaterialOwnerPtrMapIt it = _materialMap.find(strMaterialName);
	if(it != _materialMap.end()) return it->second;

	//...
	XASSERT(0);
	return MaterialPtr(NULL);
}

MaterialPtr MaterialManager::GetMaterial(const String& strMaterialName)
{
	MaterialOwnerPtrMapIt it = _materialMap.find(strMaterialName);
	if(it != _materialMap.end()) return it->second;

	//...
	XASSERT(0);
	return MaterialPtr(NULL);
}

MaterialPtr MaterialManager::CreateMaterialFromTexture(const String& strMtlName, TexturePtr& ptrTexture)
{
	MaterialOwnerPtrMapIt it = _materialMap.find(strMtlName);
	if(it != _materialMap.end()) return it->second;

	MaterialPtr pNewTexMtl(Material::Create());
	pNewTexMtl->SetName(strMtlName);
	BOOL bRet = pNewTexMtl->intMtlFromTexture(ptrTexture);

	if(bRet)
	{
		_materialMap[strMtlName] = pNewTexMtl;		
		return pNewTexMtl;
	}
	else
	{
		return MaterialPtr(NULL);
	}	
}

MaterialPtr MaterialManager::CreateMaterialFromBitmap(const String& strBitmapName)
{
	String strBmpMtlName = TEXT("HUDTexMtl: ");
	strBmpMtlName += strBitmapName;

	MaterialOwnerPtrMapIt it = _materialMap.find(strBmpMtlName);
	if(it != _materialMap.end()) return it->second;

	MaterialPtr pNewBmpMtl(Material::Create());
	pNewBmpMtl->SetName(strBmpMtlName);
	BOOL bRet = pNewBmpMtl->intMtlFromBitmap(strBitmapName);
	
	if(bRet)
	{
		_materialMap[strBmpMtlName] = pNewBmpMtl;
		return pNewBmpMtl;
	}
	else
	{
		return MaterialPtr(NULL);
	}
}

_X3D_NS_END