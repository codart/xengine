#pragma once

#include "x3d/resource/VertexProgram.h"
#include "x3d/resource/FragmentProgram.h"

_X3D_NS_BEGIN

no_v_base class Shader
{
public:
	Shader();
	Shader(VertexProgramPtr pVertexProgramPtr, FragmentProgramPtr pFragmentProgramPtr);
	~Shader(void);

public:
	INLINE void
		SetVertexProgram(VertexProgramPtr pVertexProgramPtr){_pVertexProgramPtr = pVertexProgramPtr;};
	INLINE void
		SetFragmentProgram(FragmentProgramPtr pFragmentProgramPtr){_pFragmentProgramPtr = pFragmentProgramPtr;};

	INLINE VertexProgram* 
		GetVertexProgram(){return _pVertexProgramPtr;};
	INLINE FragmentProgram* 
		GetFragmentProgram(){return _pFragmentProgramPtr;};

	INLINE void
		UpdateAutoParameters(transient const AutoParameterDataProvider* pAutoParamProvider)
	{
		_pVertexProgramPtr->UpdateAutoParameters(pAutoParamProvider);
		_pFragmentProgramPtr->UpdateAutoParameters(pAutoParamProvider);
	}


protected:
	VertexProgramPtr
		_pVertexProgramPtr;
	FragmentProgramPtr
		_pFragmentProgramPtr;

};

typedef SharedPtr<Shader> ShaderPtr;
DECLARE_MAP_TYPE(map, String, ShaderPtr);

_X3D_NS_END