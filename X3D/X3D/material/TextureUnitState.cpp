#include "StdAfx.h"
#include "TextureUnitState.h"

_X3D_NS_BEGIN

TextureUnitState::TextureUnitState(void)
{
	_iCurrentFrame = -1;	
}

TextureUnitState::~TextureUnitState(void)
{
}

void TextureUnitState::AddTextureFrame(const TexturePtr& pTexture)
{
	_textures.push_back(pTexture);
	_iCurrentFrame = _textures.size() - 1;
}

void TextureUnitState::SetAddressMode(TextureAddressMode uAddressMode
									  , TextureAddressMode vAddressMode
									  , TextureAddressMode wAddressMode)
{
	_textureState._uAddressMode = uAddressMode;
	_textureState._vAddressMode = vAddressMode;
	_textureState._wAddressMode = wAddressMode;
}

void TextureUnitState::SetTextureFilterType(TextureFilterType minFilter
											, TextureFilterType magFilter
											, TextureFilterType mipFilter)
{
	_textureState._minFilter = minFilter;
	_textureState._magFilter = magFilter;
	_textureState._mipFilter = mipFilter;
}

_X3D_NS_END