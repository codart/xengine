#pragma once

#include "x3d/resource/Texture.h"

_X3D_NS_BEGIN

enum TextureAddressMode
{
	TEX_ADDRESS_INVALID = -1,
	/// Texture wraps at values over 1.0
	TEX_ADDRESS_WRAP = 1,
	/// Texture mirrors (flips) at joins over 1.0
	TEX_ADDRESS_MIRROR,
	/// Texture clamps at 1.0
	TEX_ADDRESS_CLAMP,
	/// Texture coordinates outside the range [0.0, 1.0] are set to the border colour
	TEX_ADDRESS_BORDER
};

enum TextureFilterType
{
	TEX_FILTER_INVALID		   = -1,
	TEX_FILTER_NONE            = 0,    // filtering disabled (valid for mip filter only)
	TEX_FILTER_POINT           = 1,    // nearest
	TEX_FILTER_LINEAR          = 2,    // linear interpolation
	TEX_FILTER_ANISOTROPIC     = 3,    // anisotropic
	TEX_FILTER_FLATCUBIC       = 4,    // cubic
	TEX_FILTER_GAUSSIANCUBIC   = 5,    // different cubic kernel
};

class TextureState
{
public:
	TextureState()
	{
		_uAddressMode = TEX_ADDRESS_WRAP;
		_vAddressMode = TEX_ADDRESS_WRAP;
		_wAddressMode = TEX_ADDRESS_WRAP;

		_minFilter = TEX_FILTER_POINT;
		_magFilter = TEX_FILTER_LINEAR;
		_mipFilter = TEX_FILTER_NONE;

		_borderColor = 0x0;
	}


public:
	TextureAddressMode
		_uAddressMode;
	TextureAddressMode
		_vAddressMode;
	TextureAddressMode
		_wAddressMode;

	TextureFilterType
		_minFilter;
	TextureFilterType
		_magFilter;
	TextureFilterType
		_mipFilter;

	// Specify the border color when address mode is border.
	COLOR
		_borderColor;
};

class _X3DExport TextureUnitState : public Final<TextureUnitState>
{
public:
	TextureUnitState(void);
	~TextureUnitState(void);


public:
	void 
		AddTextureFrame(const TexturePtr& pTexture);
	void
		RemoveAllTextureFrame() { _textures.clear(); }


	// Get current frame's texture. 
	INLINE TexturePtr& GetTexturePtr()
	{
		if(-1 == _iCurrentFrame) return _nullTexture;
		else
		{
			XASSERT(_iCurrentFrame >= 0 && _iCurrentFrame < (int)_textures.size());
			return _textures[_iCurrentFrame];
		}
	}

	void
		SetAddressMode(TextureAddressMode u, TextureAddressMode v, TextureAddressMode w);

	INLINE TextureAddressMode
		GetAddressModeU(){ return _textureState._uAddressMode; }
	INLINE TextureAddressMode
		GetAddressModeV(){ return _textureState._vAddressMode; }
	INLINE TextureAddressMode
		GetAddressModeW(){ return _textureState._wAddressMode; }

	INLINE void
		SetBorderColor(COLOR borderColor){ _textureState._borderColor = borderColor; }
	INLINE COLOR
		GetBorderColor() {return _textureState._borderColor; }
		

	// ---
	void
		SetTextureFilterType(TextureFilterType minFilter, TextureFilterType magFilter, TextureFilterType mipFilter);
	void 
		SetMinFilterType(TextureFilterType minFilter) { _textureState._minFilter = minFilter; }
	void 
		SetMagFilterType(TextureFilterType magFilter) { _textureState._magFilter = magFilter; }
	void 
		SetMipFilterType(TextureFilterType mipFilter) { _textureState._mipFilter = mipFilter; }

	INLINE TextureFilterType
		GetMinFilterType() { return _textureState._minFilter; }
	INLINE TextureFilterType
		GetMagFilterType() { return _textureState._magFilter; }
	INLINE TextureFilterType
		GetMipFilterType() { return _textureState._mipFilter; }

	
protected:
	TexturePtrList
		_textures;
	Int 
		_iCurrentFrame;

	TexturePtr
		_nullTexture;

	TextureState
		_textureState;

};


DECLARE_LIST_TYPE_PTR(vector, TextureUnitState);

_X3D_NS_END