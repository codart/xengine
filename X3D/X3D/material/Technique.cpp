#include "StdAfx.h"
#include "Technique.h"

_X3D_NS_BEGIN

Technique::Technique(void)
{
	_fShininess = 8;
}

Technique::~Technique(void)
{
	// Release all pass
	PassListIt it = _passes.begin();
	PassListIt itEnd = _passes.end();

	for ( ; it != _passes.end(); ++it)
	{
		Pass* pPass = *it;
		XASSERT(pPass);
		delete pPass;
	}

}

Pass* Technique::AddPass()
{
	Pass* pPass = new Pass;
	_passes.push_back(pPass);

	return pPass;
}

Pass* Technique::GetPass(UINT iIndex)
{
	XASSERT(iIndex >=0 && iIndex < _passes.size());
	return _passes[0];
}

_X3D_NS_END