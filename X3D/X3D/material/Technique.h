#pragma once

#include "x3d/material/Pass.h"


_X3D_NS_BEGIN

class Technique : public Final<Technique>
{
public:
	Technique(void);
	~Technique(void);

public:
	Pass*
		AddPass();
	Pass* 
		GetPass(UINT iIndex = 0);

	PassList&
		GetPasses(){return _passes;};

	void 
		SetName(const String& strName){ _strName = strName; };
	const String&
		GetName() const { return _strName; };

	/* Update the resources that are binded to render context.
	 e.g.  The refection map (full window/screen) is binded to the WindowRenderTaget, should update form WindowRenderTaget
	 when the texture is referenced to a refelection map.
	*/
	void 
		UpdateContextBindedTextures();

	const Real
		GetShininess() const 
	{
		return _fShininess; 
	}
	const REAL*
		GetShininessPointer() 
	{ 
		return &_fShininess; 
	}


protected:
	String 
		_strName;

	PassList
		_passes;

	Real 
		_fShininess;

};

DECLARE_LIST_TYPE_PTR(vector, Technique);


_X3D_NS_END