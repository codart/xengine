#include "StdAfx.h"
#include "Pass.h"

_X3D_NS_BEGIN

Pass::Pass(void)
{
	memset(this, 0, (BYTE*)&_textureUnitStates - (BYTE*)this);
	_pShader = XNEW Shader();

	_sourceBlendFactor = FACTOR_ONE;
	_destBlendFactor = FACTOR_ZERO;

	_frameBlendOperation = FRAME_BLEND_ADD;

	_sourceBlendFactorAlpha = FACTOR_ONE;
	_destBlendFactorAlpha = FACTOR_ZERO;

	_hardwareCullMode = CULL_ANTICLOCKWISE;
	_bDepthEnable = TRUE;
	_bDepthWrite = TRUE;
	_depthCmpareFunc = COMPARE_FUNC_LESS_EQUAL;
	_bColorWrite = TRUE;

	_bAlphaTest = FALSE;
	_alphaCompFunc = COMPARE_FUNC_ALWAYS_PASS;
	_alphaRefValue = 0;
	_bAlphaToCoverage = FALSE;
}

Pass::~Pass(void)
{
	// Release all texture unit states
	FastVecIterator<TextureUnitState*> texIterator(_textureUnitStates);
	while(texIterator.HasMore())
	{
		XASSERT(texIterator.CurrentData());
		XDELETE(texIterator.CurrentData());
		texIterator.Next();
	}

	// Release all vertex texture unit states
	FastVecIterator<TextureUnitState*> vertexTexIterator(_vertexTextureUnitStates);
	while(vertexTexIterator.HasMore())
	{
		XASSERT(vertexTexIterator.CurrentData());
		XDELETE(vertexTexIterator.CurrentData());
		vertexTexIterator.Next();
	}

	SAFE_DELETE(_pShader);
}

void Pass::AddTextureUnitState(TextureUnitState* pState)
{
	_textureUnitStates.push_back(pState);
}

TextureUnitState* Pass::GetTextureUnitState(UINT iIndex)
{
	if(_textureUnitStates.size() == 0) return NULL;
	else
	{
		XASSERT(iIndex >= 0 && iIndex < _textureUnitStates.size());
		return _textureUnitStates.at(iIndex);
	}
}

void Pass::AddVertexTextureUnitState(TextureUnitState* pState)
{
	_vertexTextureUnitStates.push_back(pState);
}

TextureUnitState* Pass::GetVertexTextureUnitState(UINT iIndex)
{
	if(_vertexTextureUnitStates.size() == 0) return NULL;
	else
	{
		XASSERT(iIndex >= 0 && iIndex < _vertexTextureUnitStates.size());
		return _vertexTextureUnitStates.at(iIndex);
	}
}

const String& Pass::GetName()
{
	return _strName;
}


_X3D_NS_END