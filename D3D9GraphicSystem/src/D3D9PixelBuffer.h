#pragma once

#include "x3d/graphic/PixelBuffer.h"

_X3D_NS_BEGIN

class D3D9PixelBuffer : public PixelBuffer
{
public:
	D3D9PixelBuffer(GraphicSystem* pGraphicSystem);
	~D3D9PixelBuffer(void);

public:
	override Size
		GetBufferSize() const;

	virtual void 
		BitBlt(const PixelBuffer& srcBuffer, const Rect& srcRect, const Rect& destRect);

	virtual int
		GetWidth();
	virtual int
		GetHeight();


package:
	// ---
	void
		internalInit(IDirect3DSurface9* pDirect3DSurface9);
	void 
		internalRelease();

	INLINE IDirect3DSurface9* 
		getD3DSurface9() const {return _pDirect3DSurface9; }


protected:
	IDirect3DSurface9* 
		_pDirect3DSurface9;

	D3DSURFACE_DESC*
		_pSurfaceDesc;

protected:
	override InternalObjectHandle
		_getInternalObjHandle();


};


_X3D_NS_END