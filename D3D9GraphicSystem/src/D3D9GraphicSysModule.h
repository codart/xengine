#pragma once

#include "x3d/PlugableModule.h"

_X3D_NS_BEGIN

class D3D9GraphicSystem;

class D3D9GraphicSysModule : public PlugableModule
{
public:
	D3D9GraphicSysModule(void);
	virtual ~D3D9GraphicSysModule(void);

public:
	override void
		Install();
	override void
		Uninstall();


protected:
	D3D9GraphicSystem*
		_pGraphicSystem;

};

_X3D_NS_END