#pragma once

#include "x3d/graphic/VertexLayout.h"

struct IDirect3DVertexDeclaration9;

_X3D_NS_BEGIN

class D3D9VertexLayout : public VertexLayout
{
public:
	D3D9VertexLayout(GraphicSystem* pGraphicSystem);
	~D3D9VertexLayout(void);


public:
	override void
		BeginUpdate();
	override void 
		EndUpdate();


package:
	// ---
	LPDIRECT3DVERTEXDECLARATION9
		getD3DVertexDeclaration9(){ XASSERT(_pD3DVertexDeclaration9); return _pD3DVertexDeclaration9; };


protected:
	LPDIRECT3DVERTEXDECLARATION9
		_pD3DVertexDeclaration9;


protected:
	override InternalObjectHandle
		_getInternalObjHandle();
	INLINE void 
		_releaseD3DVertexDeclaration9();

	INLINE D3DDECLTYPE 
		_D3DTypeFromFormat(VertexElementFormat format);
	INLINE D3DDECLUSAGE 
		_D3DUsageFromSematic(VertexElementSemantic sematic);


};

_X3D_NS_END