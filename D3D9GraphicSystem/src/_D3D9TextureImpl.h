#pragma once

#include "x3d/resource/RenderTexture.h"
#include "x3d/resource/DynamicTexture.h"
#include "x3d/resource/CubeTexture.h"
#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN


template<class ConctreateTexture, class Base>
class _D3D9TextureImpl : public ConctreateTexture
                      , public Base
{
public:
	_D3D9TextureImpl(const String& strName, GraphicSystem* pGraphicSystem)
		: ConctreateTexture(strName, pGraphicSystem)
	{
		__pD3DTexture9 = NULL;
	}

	virtual ~_D3D9TextureImpl()
	{
		_clearDependentedRes();

		RELEASE_CHECK_EX(SAFE_CAST(this, ConctreateTexture*)->bManaged, __pD3DTexture9, 0);
		SAFE_RELEASE(__pD3DTexture9);
	}

public:
	void 
		LoadImage(const String& strImageFIle)
	{
		LPDIRECT3DTEXTURE9 pTexture = NULL;

		LPDIRECT3DDEVICE9 pD3DDevice = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

		if( FAILED( D3DXCreateTextureFromFileEx( pD3DDevice, strImageFIle.c_str(), 
			D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, 
			0, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
			D3DX_DEFAULT, D3DX_DEFAULT, 
			NULL, NULL, NULL, &pTexture) ) )
		{
			//XTHROW(LoadResFailedException); 
			XASSERT(0);
		}

		SAFE_RELEASE(__pD3DTexture9);

		__pD3DTexture9 = pTexture;

		D3DSURFACE_DESC TDesc;
		pTexture->GetLevelDesc(0, &TDesc);

		_width = TDesc.Width;
		_height = TDesc.Height;
		
		ConctreateTexture::_nMipmapCount = __pD3DTexture9->GetLevelCount();
		XASSERT(ConctreateTexture::_nMipmapCount >= 1);
	}

	void 
		LoadCubeTexImage(const String& strImageFIle)
	{
		IDirect3DCubeTexture9* pTexture = NULL;

		LPDIRECT3DDEVICE9 pD3DDevice = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

		if( FAILED( D3DXCreateCubeTextureFromFileEx( pD3DDevice, strImageFIle.c_str(), 
			D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
			D3DX_DEFAULT, D3DX_DEFAULT, 
			NULL, NULL, NULL, &pTexture) ) )
		{
			//XTHROW(LoadResFailedException); 
			XASSERT(0);
		}

		SAFE_RELEASE(__pD3DTexture9);

		__pD3DTexture9 = pTexture;

		D3DSURFACE_DESC TDesc;
		pTexture->GetLevelDesc(0, &TDesc);

		_width = TDesc.Width;
		_height = TDesc.Height;

		ConctreateTexture::_nMipmapCount = __pD3DTexture9->GetLevelCount();
		XASSERT(ConctreateTexture::_nMipmapCount >= 1);
	}

	override InternalObjectHandle _getInternalObjHandle()
	{
		return InternalObjectHandle(__pD3DTexture9);
	}

	override void _setInternalObjHandle(InternalObjectHandle pInternalHandle)
	{
		__pD3DTexture9 = (IDirect3DBaseTexture9*)pInternalHandle;
	}


protected:
	override void _updatetexSurfacePixelBufs()
	{
		XASSERT(getD3D9Texture() && ConctreateTexture::_texSurfacePixelBufs.size() == 0);
		
		for (int i=0; i <ConctreateTexture::_nMipmapCount; ++i)
		{
			IDirect3DSurface9* pD3DSurface9 = NULL;

			switch(ConctreateTexture::getTextureType())
			{
			case TEXTURE_NORMAL:
			case TEXTURE_RENDER_TARGET:
				((IDirect3DTexture9*)__pD3DTexture9)->GetSurfaceLevel(i, &pD3DSurface9);
				break;

			case TEXTURE_CUBE:
				//SAFE_CAST(__pD3DTexture9, IDirect3DCubeTexture9*)->GetSurfaceLevel(i, &pD3DSurface9);
				break;
			}
			D3D9PixelBuffer* pPixelBuffer = SAFE_CAST(ConctreateTexture::_pGraphicSystem->createPixelBuffer(), D3D9PixelBuffer*);
			pPixelBuffer->internalInit(pD3DSurface9);
			debug_ptr_assign(pPixelBuffer, bManaged, FALSE);
			ConctreateTexture::_texSurfacePixelBufs.push_back(PixelBufferPtr(pPixelBuffer));
		}
	}

};

_X3D_NS_END