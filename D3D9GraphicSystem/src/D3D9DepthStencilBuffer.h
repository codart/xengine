#pragma once

#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN

// This wrap the depth stencil buffer that create with IDirect3DDevice9::CreateDepthStencilSurface
class D3D9DepthStencilBuffer : public D3D9PixelBuffer
{
	friend class D3D9GraphicSystem;

public:
	D3D9DepthStencilBuffer(GraphicSystem* pGraphicSystem);
	virtual ~D3D9DepthStencilBuffer(void);


package:
	void 
		internalInit(int nWidth, int nHeight, PixelFormat pixelFormat);


protected:
	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);


protected:
	Int 
		_nWidth;
	Int 
		_nHeight;

	PixelFormat
		_pixelFormat;

};

_X3D_NS_END