#pragma once

#include "x3d/resource/Texture.h"
#include "_D3D9TextureBaseImpl.h"

_X3D_NS_BEGIN

class D3D9Texture : public D3D9TextureBase
{
	typedef D3D9TextureBase BaseClass;

public:
	D3D9Texture(const String& strName, GraphicSystem* pGraphicSystem);
	~D3D9Texture(void);


public:
	override void 
		LoadImage(const String& strImageFIle);

};

_X3D_NS_END