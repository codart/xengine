#include "StdAfx.h"
#include "D3D9WindowRenderTarget.h"

#include "x3d/graphic/DeviceEvent.h"
#include "D3D9GraphicSystem.h"
#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN

D3D9WindowRenderTarget::D3D9WindowRenderTarget(GraphicSystem* pGraphicSystem, Window* pAttachedWindow)
: WindowRenderTarget(pGraphicSystem, pAttachedWindow)
{
	__pSwapChain = NULL;
	memset(&__presentParameters, 0, sizeof(__presentParameters));

	_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9WindowRenderTarget::_onDeviceLost, this));
	_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9WindowRenderTarget::_onDeviceReset, this));
}

D3D9WindowRenderTarget::~D3D9WindowRenderTarget(void)
{
	SAFE_CHECKED_RELEASE(__pSwapChain, 0);
}

void D3D9WindowRenderTarget::_onWndSizeChangedImpl()
{
	/*
		Note: Need check the device state before to recreate window render target. The IsReady not
		only check the device state, but also reset the device when device is ready.
	*/
	if(!_pGraphicSystem->IsReady()) return;

	internalReleaseAll();
	internalInit();
}

void D3D9WindowRenderTarget::internalReleaseAll()
{	
	XASSERT(_pColorBffer.GetReferenceCount() == 1);
	XASSERT(_pDepthStencilBuffer.GetReferenceCount() == 1);

	if(_pColorBffer)
	{
		SAFE_CAST(_pColorBffer.getPointer(), D3D9PixelBuffer*)->internalRelease();
	}
	if(_pDepthStencilBuffer)
	{
		SAFE_CAST(_pDepthStencilBuffer.getPointer(), D3D9PixelBuffer*)->internalRelease();
	}

	SAFE_CHECKED_RELEASE(__pSwapChain, 0);
}

DWORD D3D9WindowRenderTarget::Present()
{
	HRESULT hRet = __pSwapChain->Present(NULL, NULL, NULL, NULL, D3DPRESENT_INTERVAL_IMMEDIATE);

	SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->handlePresentResult(hRet);
	return hRet;
}

DWORD D3D9WindowRenderTarget::Present(Window* pWindow)
{
	XASSERT(pWindow);
	HRESULT hRet = __pSwapChain->Present(NULL, NULL, (HWND)pWindow->GetWindowHandle(), NULL, D3DPRESENT_INTERVAL_IMMEDIATE);

	SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->handlePresentResult(hRet);
	return hRet;
}

void D3D9WindowRenderTarget::internalInit()
{
	XASSERT(_pGraphicSystem->IsReady());
	XASSERT(_pAttachedWindow && _pAttachedWindow->GetWindowHandle());
	XASSERT(::IsWindow((HWND)_pAttachedWindow->GetWindowHandle()));

	IDirect3DDevice9* pD3DDevice = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

	D3DPRESENT_PARAMETERS& d3dPresentParams = __presentParameters;
	d3dPresentParams.Windowed = TRUE;
	d3dPresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dPresentParams.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dPresentParams.EnableAutoDepthStencil = FALSE;
	d3dPresentParams.AutoDepthStencilFormat = D3DFMT_D24S8;
	d3dPresentParams.BackBufferWidth = (0 == _pAttachedWindow->GetWidth())? 1:_pAttachedWindow->GetWidth();
	d3dPresentParams.BackBufferHeight =  (0 == _pAttachedWindow->GetHeight())? 1:_pAttachedWindow->GetHeight();
	d3dPresentParams.hDeviceWindow = (HWND)_pAttachedWindow->GetWindowHandle();
	d3dPresentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	IDirect3DSwapChain9* pSwapChain = NULL;
	HRESULT hResult = pD3DDevice->CreateAdditionalSwapChain(&d3dPresentParams, &pSwapChain);
	if(hResult == D3DERR_DEVICELOST)
	{
		XTHROWEX("CreateAdditionalSwapChain failed");
	}

	// Create depth stencil buffer
	IDirect3DSurface9* pDepthStencilSurface = NULL;
	hResult = pD3DDevice->CreateDepthStencilSurface( 
		// Depth stencil buffer should has the same size as back buffer
		d3dPresentParams.BackBufferWidth, 
		d3dPresentParams.BackBufferHeight, 
		D3DFMT_D24S8, 
		D3DMULTISAMPLE_NONE, 
		0, 
		TRUE,  // discard true or false?
		&pDepthStencilSurface, 
		NULL);


	if(hResult != D3D_OK)
	{
		XTHROWEX("CreateDepthStencilSurface failed");
	}

	hResult = D3DERR_NOTAVAILABLE;

	__pSwapChain = pSwapChain;

	IDirect3DSurface9* pRenderSurface = NULL;
	__pSwapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &pRenderSurface);

	if(!_pColorBffer)
	{
		_pColorBffer = (D3D9PixelBuffer*)_pGraphicSystem->createPixelBuffer();
	}
	if(!_pDepthStencilBuffer)
	{
		_pDepthStencilBuffer = (D3D9PixelBuffer*)_pGraphicSystem->createPixelBuffer();
	}

	SAFE_CAST(_pColorBffer.getPointer(), D3D9PixelBuffer*) ->internalInit(pRenderSurface);
	SAFE_CAST(_pDepthStencilBuffer.getPointer(), D3D9PixelBuffer*)->internalInit(pDepthStencilSurface);

	debug_ptr_assign(_pColorBffer, bManaged, TRUE);
	debug_ptr_assign(_pDepthStencilBuffer, bManaged, TRUE);

	debug_assign(_bManagedColorBuf, TRUE);
	debug_assign(_bManagedDSBuf, TRUE);
}

void D3D9WindowRenderTarget::_onDeviceLost(const Event* pEvent)
{
	internalReleaseAll();
}

void D3D9WindowRenderTarget::_onDeviceReset(const Event* pEvent)
{
	XASSERT(_pAttachedWindow);
	internalInit();
}

_X3D_NS_END