#pragma once

#include "x3d/graphic/WindowRenderTarget.h"

_X3D_NS_BEGIN

class D3D9WindowRenderTarget : public WindowRenderTarget
{
	friend class D3D9GraphicSystem;

public:
	D3D9WindowRenderTarget(GraphicSystem* pGraphicSystem, Window* pAttachedWindow);
	~D3D9WindowRenderTarget(void);


public:
	override DWORD 
		Present();
	virtual DWORD
		Present(transient Window* pWindow);


package:
	final void
		internalInit();
	final void 
		internalReleaseAll();


protected:
	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);

	override void 
		_onWndSizeChangedImpl();


protected:
	IDirect3DSwapChain9* 	
		__pSwapChain;
	D3DPRESENT_PARAMETERS	
		__presentParameters;

};

_X3D_NS_END