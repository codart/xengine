#include "StdAfx.h"
#include "D3D9VertexLayout.h"

#include "D3D9GraphicSystem.h"


_X3D_NS_BEGIN

D3D9VertexLayout::D3D9VertexLayout(GraphicSystem* pGraphicSystem)
: VertexLayout(pGraphicSystem)
{
	_pD3DVertexDeclaration9 = NULL;
}

D3D9VertexLayout::~D3D9VertexLayout(void)
{
	_releaseD3DVertexDeclaration9();
}

void D3D9VertexLayout::_releaseD3DVertexDeclaration9()
{
	SAFE_CHECKED_RELEASE(_pD3DVertexDeclaration9, 0);
}

InternalObjectHandle D3D9VertexLayout::_getInternalObjHandle()
{
	return (InternalObjectHandle)_pD3DVertexDeclaration9;
}

void D3D9VertexLayout::BeginUpdate()
{
	SAFE_CHECKED_RELEASE(_pD3DVertexDeclaration9, 0);
	VertexLayout::BeginUpdate();
}

void D3D9VertexLayout::EndUpdate()
{
	VertexLayout::EndUpdate();

	XASSERT(!_pD3DVertexDeclaration9);

	// Create vertex D3D9 vertex declaration
	D3DVERTEXELEMENT9* pD3DElements = new D3DVERTEXELEMENT9[_vertexElementList.size() + 1];

	VertexElementListConstIt it = _vertexElementList.begin();
	VertexElementListConstIt itEnd = _vertexElementList.end();

	unsigned int iIndex;
	for (iIndex = 0; it != itEnd; ++it, ++iIndex)
	{
		const VertexElement& vertexElement = *it;
		pD3DElements[iIndex].Method = D3DDECLMETHOD_DEFAULT;
		pD3DElements[iIndex].Offset = static_cast<WORD>(vertexElement.GetOffset());
		pD3DElements[iIndex].Stream = vertexElement.GetStream();
		pD3DElements[iIndex].Type = _D3DTypeFromFormat(vertexElement.GetFormat());
		pD3DElements[iIndex].Usage = _D3DUsageFromSematic(vertexElement.GetSemantic());

		// NB force index if colours since D3D uses the same usage for 
		// diffuse & specular
		if (it->GetSemantic() == VES_SPECULAR)
		{
			pD3DElements[iIndex].UsageIndex = 1;
		}
		else if (it->GetSemantic() == VES_DIFFUSE)
		{
			pD3DElements[iIndex].UsageIndex = 0;
		}
		else
		{
			pD3DElements[iIndex].UsageIndex = static_cast<BYTE>(it->GetSemanticIndex());
		}
	}
	// Add terminator
	pD3DElements[iIndex].Stream = 0xff;
	pD3DElements[iIndex].Offset = 0;
	pD3DElements[iIndex].Type = D3DDECLTYPE_UNUSED;
	pD3DElements[iIndex].Method = 0;
	pD3DElements[iIndex].Usage = 0;
	pD3DElements[iIndex].UsageIndex = 0;

	IDirect3DDevice9* pD3DDevice9 = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();
	HRESULT hr = pD3DDevice9->CreateVertexDeclaration(pD3DElements, &_pD3DVertexDeclaration9);

	if (FAILED(hr))
	{
		XTHROWEX("VertexDeclaration::getVertexDeclarationD3D9:	Cannot create D3D9 vertex \
				  declaration!");
	}

	delete [] pD3DElements;
}

D3DDECLTYPE D3D9VertexLayout::_D3DTypeFromFormat(VertexElementFormat format)
{
	switch (format)
	{
	case VEF_COLOUR:
	case VEF_COLOUR_ABGR:
	case VEF_COLOUR_ARGB:
		return D3DDECLTYPE_D3DCOLOR;
		break;
	case VEF_FLOAT1:
		return D3DDECLTYPE_FLOAT1;
		break;
	case VEF_FLOAT2:
		return D3DDECLTYPE_FLOAT2;
		break;
	case VEF_FLOAT3:
		return D3DDECLTYPE_FLOAT3;
		break;
	case VEF_FLOAT4:
		return D3DDECLTYPE_FLOAT4;
		break;
	case VEF_SHORT2:
		return D3DDECLTYPE_SHORT2;
		break;
	case VEF_SHORT4:
		return D3DDECLTYPE_SHORT4;
		break;
	case VEF_UBYTE4:
		return D3DDECLTYPE_UBYTE4;
		break;

	default:
		XASSERT(0);
	}

	return D3DDECLTYPE_FLOAT3;
}

D3DDECLUSAGE D3D9VertexLayout::_D3DUsageFromSematic(VertexElementSemantic sematic)
{
	switch (sematic)
	{
	case VES_BLEND_INDICES:
		return D3DDECLUSAGE_BLENDINDICES;
		break;
	case VES_BLEND_WEIGHTS:
		return D3DDECLUSAGE_BLENDWEIGHT;
		break;
	case VES_DIFFUSE:
		return D3DDECLUSAGE_COLOR; // NB index will differentiate
		break;
	case VES_SPECULAR:
		return D3DDECLUSAGE_COLOR; // NB index will differentiate
		break;
	case VES_NORMAL:
		return D3DDECLUSAGE_NORMAL;
		break;
	case VES_POSITION:
		return D3DDECLUSAGE_POSITION;
		break;
	case VES_TEXTURE_COORDINATES:
		return D3DDECLUSAGE_TEXCOORD;
		break;
	case VES_BINORMAL:
		return D3DDECLUSAGE_BINORMAL;
		break;
	case VES_TANGENT:
		return D3DDECLUSAGE_TANGENT;
		break;

	default:
		XASSERT(0);
	}

	return D3DDECLUSAGE_POSITION;
}


_X3D_NS_END