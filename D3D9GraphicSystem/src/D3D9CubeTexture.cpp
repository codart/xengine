#include "stdafx.h"
#include "D3D9CubeTexture.h"

#include "D3D9GraphicSystem.h"

_X3D_NS_BEGIN

D3D9CubeTexture::D3D9CubeTexture(const String& strName, GraphicSystem* pGraphicSystem)
: BaseClass(strName, pGraphicSystem)
{
}

D3D9CubeTexture::~D3D9CubeTexture(void)
{
}

void D3D9CubeTexture::LoadImage(const String& strImageFIle)
{
	IDirect3DCubeTexture9* pTexture = NULL;

	LPDIRECT3DDEVICE9 pD3DDevice = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

	if( FAILED( D3DXCreateCubeTextureFromFileEx( pD3DDevice, strImageFIle.c_str(), 
		D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
		D3DX_DEFAULT, D3DX_DEFAULT, 
		NULL, NULL, NULL, &pTexture) ) )
	{
		//XTHROW(LoadResFailedException); 
		XASSERT(0);
	}

	SAFE_RELEASE(__pD3DTexture9);

	__pD3DTexture9 = pTexture;

	D3DSURFACE_DESC TDesc;
	pTexture->GetLevelDesc(0, &TDesc);

	_width = TDesc.Width;
	_height = TDesc.Height;

	_nMipmapCount = __pD3DTexture9->GetLevelCount();
	XASSERT(_nMipmapCount >= 1);
}


_X3D_NS_END