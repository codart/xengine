#pragma once

#include "d3d9texture.h"
#include "x3d/resource/RenderTexture.h"

_X3D_NS_BEGIN

class D3D9RenderTexture : public D3D9RenderTextureBase
{
	friend class D3D9GraphicSystem;
	typedef D3D9RenderTextureBase BaseClass;

public:
	D3D9RenderTexture(const String& strName, GraphicSystem* pGraphicSystem);
	~D3D9RenderTexture();


public:
	virtual void
		Resize(int nWidth, int nHeight);

	INLINE void 
		internalInit(int nWidth, int nHeight, PixelFormat pixelFormat, PixelFormat depthStencilFormat);
	INLINE void 
		internalReleaseAll();


protected:
	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);


private:
	Bool
		__bDeviceEventListened;

};

_X3D_NS_END