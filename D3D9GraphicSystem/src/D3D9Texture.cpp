#include "StdAfx.h"
#include "D3D9Texture.h"

#include "D3D9GraphicSystem.h"

_X3D_NS_BEGIN

D3D9Texture::D3D9Texture(const String& strName, GraphicSystem* pGraphicSystem)
: BaseClass(strName, pGraphicSystem)
{
}

D3D9Texture::~D3D9Texture(void)
{
}

void D3D9Texture::LoadImage(const String& strImageFIle)
{
	BaseClass::LoadImage(strImageFIle);
}


_X3D_NS_END