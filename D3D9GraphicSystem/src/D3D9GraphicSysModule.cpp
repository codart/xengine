#include "StdAfx.h"
#include "D3D9GraphicSysModule.h"

#include "D3D9GraphicSystem.h"

_X3D_NS_BEGIN

D3D9GraphicSysModule::D3D9GraphicSysModule(void)
{
	_pGraphicSystem = NULL;
}

D3D9GraphicSysModule::~D3D9GraphicSysModule(void)
{
}

void D3D9GraphicSysModule::Install()
{
	_pGraphicSystem = XNEW D3D9GraphicSystem;
	GetX3DModule()->installGraphicSystem(_pGraphicSystem);
}

void D3D9GraphicSysModule::Uninstall()
{
	GraphicSystem* pGraphicSystem = GetX3DModule()->uninstallGraphicSystem();
	XASSERT(_pGraphicSystem == pGraphicSystem);
	SAFE_CAST(pGraphicSystem, D3D9GraphicSystem*);
	SAFE_DELETE(pGraphicSystem);
}

_X3D_NS_END