// D3D9GraphicSystem.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "D3D9System.h"

#include "D3D9GraphicSysModule.h"


_XD3D9GSExport x3d::PlugableModule* GetPlugableModule()
{
	return XNEW D3D9GraphicSysModule;
}

