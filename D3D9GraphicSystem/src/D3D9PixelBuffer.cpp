#include "StdAfx.h"
#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN

D3D9PixelBuffer::D3D9PixelBuffer(GraphicSystem* pGraphicSystem)
: PixelBuffer(pGraphicSystem)
{
	_pDirect3DSurface9 = NULL;

	_pSurfaceDesc = new D3DSURFACE_DESC;
	memset(_pSurfaceDesc, 0, sizeof(D3DSURFACE_DESC));
}

D3D9PixelBuffer::~D3D9PixelBuffer(void)
{
	SAFE_DELETE(_pSurfaceDesc);

	RELEASE_CHECK_EX(bManaged, _pDirect3DSurface9, 0);
	SAFE_RELEASE(_pDirect3DSurface9);
}

InternalObjectHandle D3D9PixelBuffer::_getInternalObjHandle()
{
	return (InternalObjectHandle)_pDirect3DSurface9;
}

int D3D9PixelBuffer::GetWidth()
{
	return _pSurfaceDesc->Width;
}

int D3D9PixelBuffer::GetHeight()
{
	return _pSurfaceDesc->Height;
}

void D3D9PixelBuffer::BitBlt(const PixelBuffer& srcBuffer, const Rect& srcRect, const Rect& destRect)
{
	// Remove const modifier
	PixelBuffer* pSrcBuffer = (PixelBuffer*)&srcBuffer; 

	if(D3DXLoadSurfaceFromSurface(
		this->getD3DSurface9(), NULL, (RECT*)&destRect, 
		SAFE_CAST(pSrcBuffer, D3D9PixelBuffer*)->getD3DSurface9(), NULL, (RECT*)&srcRect,
		D3DX_DEFAULT, 0) != D3D_OK)
	{
		XASSERT(0);
	}
}

Size D3D9PixelBuffer::GetBufferSize() const
{	
	return Size(_pSurfaceDesc->Width, _pSurfaceDesc->Height);
}

void D3D9PixelBuffer::internalInit(IDirect3DSurface9* pDirect3DSurface9)
{
	_pDirect3DSurface9 = pDirect3DSurface9;
	_pDirect3DSurface9->GetDesc(_pSurfaceDesc);

	debug_assign(bufferState, GBS_INITED);
}

void D3D9PixelBuffer::internalRelease()
{
	memset(_pSurfaceDesc, 0, sizeof(*_pSurfaceDesc));

	RELEASE_CHECK_EX(bManaged, _pDirect3DSurface9, 0);
	SAFE_RELEASE(_pDirect3DSurface9);

	debug_assign(bufferState, GBS_UNINIT);
}

_X3D_NS_END