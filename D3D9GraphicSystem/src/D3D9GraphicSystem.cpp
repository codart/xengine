#include "StdAfx.h"
#include "D3D9GraphicSystem.h"

#include "x3d/graphic/GraphicSystemCap.h"

#include "X3D/graphic/DeviceEvent.h"
#include "x3d/resource/VertexData.h"
#include "x3d/graphic/MultiRenderTarget.h"

#include "x3d/resource/IndexData.h"
#include "x3d/material/TextureUnitState.h"
#include "x3d/material/Shader.h"
#include "x3d/graphic/DrawPrimitiveCommand.h"

#include "D3D9VertexBuffer.h"
#include "D3D9IndexBuffer.h"
#include "D3D9PixelBuffer.h"
#include "D3D9RenderTexture.h"
#include "D3D9DynamicTexture.h"
#include "D3D9VertexLayout.h"
#include "D3D9VertexProgram.h"
#include "D3D9FragmentProgram.h"
#include "D3D9WindowRenderTarget.h"
#include "D3D9DepthStencilBuffer.h"
#include "D3D9CubeTexture.h"

#include <d3d9.h>
#include <d3dx9.h>


_X3D_NS_BEGIN

D3D9GraphicSystem::D3D9GraphicSystem(void)
{
	__pD3D9 = NULL;
	__pD3D9Device = NULL;
	__pD3DPresentParam = NULL;

	__bDeviceLost = FALSE;

	__iLastTexDisableFrom = -1;
	__iLastVertexTexDisableFrom = -1;
	__iLastRTDisableFrom = -1;

	__fullWndViewportInfo.X = 0;
	__fullWndViewportInfo.Y = 0;
	__fullWndViewportInfo.Width = 1024;
	__fullWndViewportInfo.Height = 1024;		
	__fullWndViewportInfo.MinZ = 0;
	__fullWndViewportInfo.MaxZ = 1;

	// By default no clip plane enabled, so set all bits to 0
	__dwClipPlaneFlag = 0; 

}

D3D9GraphicSystem::~D3D9GraphicSystem(void)
{
	SAFE_DELETE(__pD3DPresentParam);
}

void D3D9GraphicSystem::Init(DWORD_PTR dwInitData)
{
	GraphicSystem::Init(dwInitData);

	DeviceEvent e(DeviceEvent::DEVICE_BEFORE_INIT, this);
	DispatchEvent(e);

	HWND hWnd = HWND(dwInitData);

	BOOL bCreateTempWindow = false;
	if(!hWnd)
	{
		bCreateTempWindow = true;
		hWnd = ::CreateWindowEx(0, TEXT("Button"), NULL, 0, 0, 0, 10, 10, NULL, 0
								, (HINSTANCE)GetX3DModule()->GetModuleHandle(), 0);
	}

	// Create the D3D object.
	if(NULL == __pD3D9 && !(__pD3D9 = Direct3DCreate9(D3D_SDK_VERSION))) 
	{ XTHROWEX("Direct3DCreate9 failed"); }

	__initWndPresentParameters(hWnd);

	// Create the D3DDevice
	if( FAILED( __pD3D9->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		__pD3DPresentParam, &__pD3D9Device ) ) )
	{ XTHROWEX("Create D3D9 Device failed"); }

	// Init GraphicSystemCap
	// ....

	__iLastTexDisableFrom = 0;
	__iLastRTDisableFrom = 0;

	if(bCreateTempWindow)
	{
		::DestroyWindow(hWnd);
		hWnd = NULL;
	}

	_onDeviceReset();

	e.nType = DeviceEvent::DEVICE_AFTER_INIT;
	DispatchEvent(e);
}

void D3D9GraphicSystem::Uninit()
{
	DeviceEvent e(DeviceEvent::DEVICE_BEFORE_UNINIT, this);
	DispatchEvent(e);

	GraphicSystem::Uninit();

	SAFE_CHECKED_RELEASE(__pD3D9Device, 0);
	SAFE_DELETE(__pD3DPresentParam);
	SAFE_RELEASE(__pD3D9);

	e.nType = DeviceEvent::DEVICE_BEFORE_UNINIT;
	DispatchEvent(e);
}

BOOL D3D9GraphicSystem::IsReady()
{
	XASSERT(__pD3D9Device);

	HRESULT hRet = __pD3D9Device->TestCooperativeLevel();	

	if(hRet == S_OK) return TRUE;

	if(hRet == D3DERR_DEVICELOST)
	{
		_onDeviceLost();

		DeviceEvent e(DeviceEvent::DEVICE_LOST, this);
		DispatchEvent(e);

		__bDeviceLost = TRUE;
		return FALSE;

	}
	else if(__bDeviceLost && hRet == D3DERR_DEVICENOTRESET)
	{
		XASSERT(__bDeviceLost == TRUE);

		if(!__bDeviceLost)
		{
			_onDeviceLost();

			DeviceEvent e(DeviceEvent::DEVICE_LOST, this);
			DispatchEvent(e);

			__bDeviceLost = TRUE;
		}

		if(__tryResetDevice() != D3D_OK)
		{
			return FALSE;
		}
		else
		{
			__bDeviceLost = FALSE;
			return TRUE;
		}
	}

	return FALSE;
}

void D3D9GraphicSystem::ClearFrameBuffer(DWORD dwBufferFlag
									 , DWORD color
									 , Real depth
									 , unsigned short stencil)
{
	HRESULT hr = 0;

	if( FAILED(hr = __pD3D9Device->Clear(0, NULL, dwBufferFlag, color, depth, stencil)) )
	{
		XASSERT(0);
	}
}

void D3D9GraphicSystem::SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha)
{
	DWORD val = 0;
	if (bWriteRed) 
		val |= D3DCOLORWRITEENABLE_RED;
	if (bWriteGreen)
		val |= D3DCOLORWRITEENABLE_GREEN;
	if (bWriteBlue)
		val |= D3DCOLORWRITEENABLE_BLUE;
	if (bWriteAlpha)
		val |= D3DCOLORWRITEENABLE_ALPHA;
	HRESULT hRet = __setRenderState(D3DRS_COLORWRITEENABLE, val); 
	XASSERT(hRet == D3D_OK);
}

void D3D9GraphicSystem::EnableColorBufferWrite(BOOL bEnable)
{
	HRESULT hRet = __setRenderState(D3DRS_COLORWRITEENABLE, bEnable? 0xffffffff : 0);
	XASSERT(hRet == D3D_OK);
}

void D3D9GraphicSystem::SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState)
{
	if(pTextureUnitState)
	{
		_setTextureUnitState(texUnitIndex, pTextureUnitState);
	}
	else 
	{
		// Disable texture stage
		_disableTextureUnit(texUnitIndex);
	}
}

void D3D9GraphicSystem::SetTextureUnitStates(const TextureUnitStateList& textureUnitStates)
{
	FASTEST_VECTOR_ITERATE(TextureUnitState*, textureUnitStates);
		_setTextureUnitState(CURRENT_INDEX(textureUnitStates), CURRENT_DATA(textureUnitStates));
		GET_NEXT(textureUnitStates);
	FASTEST_ITERATE_END();

	// Disable unused texture stages.
	int iDisableFrom = textureUnitStates.size();
	if(iDisableFrom < __iLastTexDisableFrom)
	{
		int iDisableEnd = __iLastTexDisableFrom;
		__iLastTexDisableFrom = iDisableFrom;

		// Disable texture unit state
		for (int j=iDisableFrom; j<iDisableEnd; j++)
		{
			_disableTextureUnit(j);
		}
	}
	else
	{
		__iLastTexDisableFrom = iDisableFrom;
	}	
}

void D3D9GraphicSystem::SetTexture(UINT texUnitIndex, Texture* pTexture)
{
	_D3D9TextureBaseImpl* pTextureBaseImpl = _D3D9TextureBaseImpl::CastToTextureBase(pTexture);
	XASSERT(pTextureBaseImpl);
	__setTexture(texUnitIndex, pTextureBaseImpl->getD3D9Texture());
}

void D3D9GraphicSystem::SetTextureState(UINT texUnitIndex, TextureState* pTextureState)
{
	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSU, pTextureState->_uAddressMode);
	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSV, pTextureState->_vAddressMode);
	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSW, pTextureState->_wAddressMode);

	__setSamplerState(texUnitIndex, D3DSAMP_MAGFILTER, pTextureState->_magFilter);
	__setSamplerState(texUnitIndex, D3DSAMP_MINFILTER, pTextureState->_minFilter);
	__setSamplerState(texUnitIndex, D3DSAMP_MIPFILTER, pTextureState->_mipFilter);


	if(pTextureState->_uAddressMode == TEX_ADDRESS_BORDER
		|| pTextureState->_vAddressMode == TEX_ADDRESS_BORDER
		|| pTextureState->_wAddressMode == TEX_ADDRESS_BORDER)
	{
		__setSamplerState(texUnitIndex, D3DSAMP_BORDERCOLOR, pTextureState->_borderColor);
	}
}

void D3D9GraphicSystem::SetVertexTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState)
{
	int vertexTextureIndex = D3DVERTEXTEXTURESAMPLER0 + texUnitIndex;
	SetTextureUnitState(vertexTextureIndex, pTextureUnitState);
}

void D3D9GraphicSystem::SetVertexTextureUnitStates(const TextureUnitStateList& textureUnitStates)
{
	FASTEST_VECTOR_ITERATE(TextureUnitState*, textureUnitStates);
		_setTextureUnitState(D3DVERTEXTEXTURESAMPLER0 + CURRENT_INDEX(textureUnitStates)
							, CURRENT_DATA(textureUnitStates));
		GET_NEXT(textureUnitStates);
	FASTEST_ITERATE_END();

	// Disable unused texture stages.
	int iDisableFrom = textureUnitStates.size();
	if(iDisableFrom < __iLastVertexTexDisableFrom)
	{
		int iDisableEnd = __iLastVertexTexDisableFrom;
		__iLastVertexTexDisableFrom = iDisableFrom;

		// Disable texture unit state
		for (int j=iDisableFrom; j<iDisableEnd; j++)
		{
			_disableTextureUnit(D3DVERTEXTEXTURESAMPLER0 + j);
		}
	}
	else
	{
		__iLastVertexTexDisableFrom = iDisableFrom;
	}
}

void D3D9GraphicSystem::SetVertexLayout(VertexLayout* pVertexLayout)
{
	XASSERT(pVertexLayout);

	int n = pVertexLayout->GetElementCount();

	HRESULT hRet = __pD3D9Device->SetVertexDeclaration( SAFE_CAST(pVertexLayout, D3D9VertexLayout*)->getD3DVertexDeclaration9() );
	XASSERT(hRet == D3D_OK);
}

void D3D9GraphicSystem::SetViewport(const ViewportInfo* pViewportInfo)
{
	HRESULT hRet;
	if(!pViewportInfo)
	{
		hRet = __pD3D9Device->SetViewport((D3DVIEWPORT9*)&__fullWndViewportInfo);
	}
	else
	{
		hRet = __pD3D9Device->SetViewport((D3DVIEWPORT9*)pViewportInfo);
	}
	XASSERT(hRet == D3D_OK);
}

BOOL D3D9GraphicSystem::SetColorBuffer(int iBufferIndex, PixelBuffer* pColorBuffer)
{
	D3D9PixelBuffer* pD3D9ColorBuffer = SAFE_CAST(pColorBuffer, D3D9PixelBuffer*);
	HRESULT hRet = __pD3D9Device->SetRenderTarget(iBufferIndex, pD3D9ColorBuffer->getD3DSurface9());
	XASSERT(hRet == D3D_OK);
	return hRet == D3D_OK;	
}

BOOL D3D9GraphicSystem::SetDepthStencilBuffer(PixelBuffer* pColorBuffer)
{
	HRESULT hRet = __pD3D9Device->SetDepthStencilSurface(SAFE_CAST(pColorBuffer, D3D9PixelBuffer*)->getD3DSurface9());
	XASSERT(hRet == D3D_OK);
	return hRet == D3D_OK;	
}

BOOL D3D9GraphicSystem::SetRenderTarget(RenderTarget* pRenderTarget)
{
	if(__bDeviceLost)
	{
		if(__tryResetDevice() != D3D_OK) return FALSE;
	}

	XASSERT(pRenderTarget);
	if(_pCurrentRenderTarget == pRenderTarget) return TRUE;

	D3D9PixelBuffer* pColorBuffer = SAFE_CAST(pRenderTarget->GetColorBuffer(), D3D9PixelBuffer*);
	HRESULT hRet = __pD3D9Device->SetRenderTarget(0, pColorBuffer->getD3DSurface9());
	XASSERT(hRet == D3D_OK);

	_disableUnusedColorBuffer(1);

	D3D9PixelBuffer* pDepthStencilBuffer = SAFE_CAST(pRenderTarget->GetDepthStencilBuffer(), D3D9PixelBuffer*);
	hRet = __pD3D9Device->SetDepthStencilSurface(pDepthStencilBuffer->getD3DSurface9());
	XASSERT(hRet == D3D_OK);

	// Store the full screen viewport info
	__fullWndViewportInfo.Width = pColorBuffer->GetWidth();
	__fullWndViewportInfo.Height = pColorBuffer->GetHeight();

	return TRUE;
}

BOOL D3D9GraphicSystem::SetMultiRenderTarget(MultiRenderTarget* pMultiRenderTarget)
{
	if(__bDeviceLost)
	{
		if(__tryResetDevice() != D3D_OK) return FALSE;
	}

	// Set color buffers.
	HRESULT hRet;
	const PixelBufferList& pixelBuffers = pMultiRenderTarget->GetColorBuffers();
	FASTEST_VECTOR_ITERATE(PixelBuffer*, pixelBuffers);
		D3D9PixelBuffer* pColorBuffer = SAFE_CAST(CURRENT_DATA(pixelBuffers), D3D9PixelBuffer*);
		hRet = __pD3D9Device->SetRenderTarget(CURRENT_INDEX(pixelBuffers), pColorBuffer->getD3DSurface9());
		XASSERT(hRet == D3D_OK);
		GET_NEXT(pixelBuffers);
	FASTEST_ITERATE_END();

	_disableUnusedColorBuffer(pMultiRenderTarget->GetColorBufferCount());

	PixelBuffer* pDepthStencilBuffer = pMultiRenderTarget->GetDepthStencilBuffer();
	hRet = __pD3D9Device->SetDepthStencilSurface(SAFE_CAST(pDepthStencilBuffer, D3D9PixelBuffer*)->getD3DSurface9());
	XASSERT(hRet == D3D_OK);

	return TRUE;
}

void D3D9GraphicSystem::_disableUnusedColorBuffer(int iDisableFrom)
{
	HRESULT hRet;
	if(iDisableFrom <= __iLastRTDisableFrom)
	{
		int iDisableEnd = __iLastRTDisableFrom;
		__iLastRTDisableFrom = iDisableFrom;

		for (int j=iDisableFrom; j<iDisableEnd; j++)
		{
			hRet = __pD3D9Device->SetRenderTarget(j, NULL);
			XASSERT(hRet == D3D_OK);			
		}
	}
	else
	{
		__iLastRTDisableFrom = iDisableFrom;
	}
}

void D3D9GraphicSystem::SetSceneBlending(FrameBlendFactor sourceFactor
									 , FrameBlendFactor destFactor
									 , FrameBlendOperation blendOp)
{
	if(sourceFactor == FACTOR_ONE && destFactor == FACTOR_ZERO)
	{
		__setRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	}
	else
	{
		__setRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		__setRenderState(D3DRS_BLENDOP, blendOp);
		__setRenderState(D3DRS_SRCBLEND, sourceFactor);
		__setRenderState(D3DRS_DESTBLEND, destFactor);
	}
}

void D3D9GraphicSystem::SetAlphaTestParams(BOOL bEnable, CompareFunction func, BYTE refValue, BOOL bAlphaToCoverage)
{
	__setRenderState(D3DRS_ALPHATESTENABLE, bEnable);

	if(bEnable)
	{
		// Set always just be sure
		__setRenderState(D3DRS_ALPHAFUNC, (D3DCMPFUNC)func);
		__setRenderState(D3DRS_ALPHAREF, refValue );
	}

	//... Implement bAlphaToCoverage later
	XASSERT(!bAlphaToCoverage);
}

void D3D9GraphicSystem::SetDepthBufferParams(BOOL bDepthTest
											 , BOOL bDepthWrite
											 , CompareFunction depthFunction)
{

	__setRenderState(D3DRS_ZENABLE, bDepthTest);
	__setRenderState(D3DRS_ZWRITEENABLE, bDepthWrite);

	__setRenderState(D3DRS_ZFUNC, depthFunction);
}

void D3D9GraphicSystem::SetStencilBufferParams(BOOL bEnableStencil
											, CompareFunction func
											, UINT refValue, UINT mask
											, StencilOperation stencilFailOp
											, StencilOperation depthFailOp
											, StencilOperation stencilAndZPassOp 
											, BOOL bTwoSidedOperation)
{
	//... write two side stencil later
	SASSERT(0);

	__setRenderState(D3DRS_STENCILENABLE, bEnableStencil);

	// func
	__setRenderState(D3DRS_STENCILFUNC, func);

	// reference value
	__setRenderState(D3DRS_STENCILREF, refValue);

	// mask
	__setRenderState(D3DRS_STENCILMASK, mask);

	// fail op
	__setRenderState(D3DRS_STENCILFAIL, stencilFailOp);

	// depth fail op
	__setRenderState(D3DRS_STENCILZFAIL, depthFailOp);

	// pass op
	__setRenderState(D3DRS_STENCILPASS, stencilAndZPassOp);	
}

void D3D9GraphicSystem::SetCullMode(HardwareCullMode cullMode)
{
	__setRenderState(D3DRS_CULLMODE, cullMode);
}

void D3D9GraphicSystem::SetShader(Shader* pShader)
{
	// ... Need do this check????
	if(pShader == _pCurrentShader) return;

	_pCurrentShader = pShader;

	// Not support fixed function pipeline, the shader cannot be null!
	XASSERT(pShader); 

	D3D9VertexProgram* pVertexProgram = SAFE_CAST(pShader->GetVertexProgram(), D3D9VertexProgram*);
	D3D9FragmentProgram* pFragmentProgram = SAFE_CAST(pShader->GetFragmentProgram(), D3D9FragmentProgram*);

	HRESULT hRet = D3D_OK;
	//.. Need implement auto param first
	hRet = __pD3D9Device->SetVertexShader(pVertexProgram->getD3DVertexShader9());

	XASSERT(hRet == D3D_OK);

	hRet = __pD3D9Device->SetPixelShader(pFragmentProgram->getD3DPixelShader9());

	XASSERT(hRet == D3D_OK);
}

HRESULT D3D9GraphicSystem::__setSamplerState(UINT texUnitIndex, D3DSAMPLERSTATETYPE sampleStateType, DWORD value)
{
#ifdef _DEBUG
	DWORD dwOldState;
	__pD3D9Device->GetSamplerState(texUnitIndex, sampleStateType, &dwOldState);
	if(value == dwOldState) return D3D_OK;
#endif

	HRESULT hRet = __pD3D9Device->SetSamplerState(texUnitIndex, sampleStateType, value);
	XASSERT(hRet == D3D_OK);
	return hRet;
}

HRESULT D3D9GraphicSystem::__setRenderState(D3DRENDERSTATETYPE state, DWORD value)
{
#ifdef _DEBUG
	DWORD dwOldState;
	__pD3D9Device->GetRenderState(state, &dwOldState);
	if(value == dwOldState) return D3D_OK;
#endif

	HRESULT hRet = __pD3D9Device->SetRenderState(state, value);
	XASSERT(hRet == D3D_OK);
	return hRet;
}

HRESULT D3D9GraphicSystem::__setTexture(UINT texUnitIndex, IDirect3DBaseTexture9* pD3D9BaseTexture)
{
	HRESULT hRet = __pD3D9Device->SetTexture(texUnitIndex, pD3D9BaseTexture);
	XASSERT(hRet == D3D_OK);
	return hRet;
}

DWORD D3D9GraphicSystem::BeginDrawing()
{
	if( !SUCCEEDED( __pD3D9Device->BeginScene() ) )
	{
		XTHROWEX("D3D BeginScene failed!!!");
	}

	_drawCount = 0;

	return 0;
}

DWORD D3D9GraphicSystem::EndDrawing()
{
	HRESULT hRet = __pD3D9Device->EndScene();
	return hRet;
}

void D3D9GraphicSystem::BeginUpdateWindow(WindowRenderTarget* pWndTarget)
{
	GraphicSystem::BeginUpdateWindow(pWndTarget);
}

void D3D9GraphicSystem::EndUpdateWindow()
{
	GraphicSystem::EndUpdateWindow();
}

DWORD D3D9GraphicSystem::SetVertexBuffer(int iStreamIndex, VertexBuffer* pVertexBuffer)
{
	XASSERT(pVertexBuffer);
	D3D9VertexBuffer* pD3D9VertexBuffer = SAFE_CAST(pVertexBuffer, D3D9VertexBuffer*);

	// Make sure the vertex buffer is NULL(GBS_UNINIT) or has valid vertex data(GBS_HAS_VALID_DATA)
	XASSERT(pD3D9VertexBuffer->bufferState == GBS_UNINIT || pD3D9VertexBuffer->bufferState == GBS_HAS_VALID_DATA);

	HRESULT hRet = __pD3D9Device->SetStreamSource(iStreamIndex
												, pD3D9VertexBuffer->getD3D9VertexBuffer()
												, 0, pD3D9VertexBuffer->GetVertexSize());
	XASSERT(hRet == D3D_OK);
	return hRet;
}

DWORD D3D9GraphicSystem::SetIndexBuffer(IndexBuffer* pIndexBuffer)
{
	D3D9IndexBuffer* pD3D9IndexBuffer = SAFE_CAST(pIndexBuffer, D3D9IndexBuffer*);
	HRESULT hRet = __pD3D9Device->SetIndices(pD3D9IndexBuffer->getD3DIndexBuffer9());
	XASSERT(hRet == D3D_OK);

	return hRet;
}

DWORD D3D9GraphicSystem::DrawPrimitive(DrawPrimitiveType primitiveType
									, int nVertexCount
									, int iStartIndex
									, int nPrimitiveCount)
{
	HRESULT hRet = __pD3D9Device->DrawIndexedPrimitive(D3DPRIMITIVETYPE(primitiveType)
														, 0, 0, nVertexCount, 0
														, nPrimitiveCount);
	XASSERT(hRet == D3D_OK);

	_drawCount++;

	return 0;
}

DWORD D3D9GraphicSystem::DrawPrimitive(const DrawPrimitiveCommand* pDrawPrimitiveCmd)
{
	VertexData* pVertexData = pDrawPrimitiveCmd->GetVertexData();
	IndexData* pIndexData = pDrawPrimitiveCmd->GetIndexData();
	XASSERT(pVertexData && pIndexData);

	XASSERT(pVertexData->GetVertexLayout() && pVertexData->GetVertexLayout()->GetElementCount() > 0);
	SetVertexLayout(pVertexData->GetVertexLayout());

	SetVertexBuffers(pVertexData->GetVertexBuffers());

	// Set index buffer
	D3D9GraphicSystem::SetIndexBuffer(pIndexData->GetIndexBuffer());

	HRESULT hRet = __pD3D9Device->DrawIndexedPrimitive(D3DPRIMITIVETYPE(pDrawPrimitiveCmd->GetDrawPrimitiveType())
									, 0, 0, pVertexData->GetVertexCount(), 0
									, pDrawPrimitiveCmd->GetPrimitiveCount());
	XASSERT(hRet == D3D_OK);

	_drawCount++;

	return 0;
}

void D3D9GraphicSystem::SetVertexBuffers(const VertexBufferPtrList& vertexBuffers)
{
	FASTEST_VECTOR_ITERATE(VertexBufferPtr, vertexBuffers);
		VertexBufferPtr pVertexBuffer = CURRENT_DATA(vertexBuffers);
		D3D9GraphicSystem::SetVertexBuffer(CURRENT_INDEX(vertexBuffers), pVertexBuffer);
		GET_NEXT(vertexBuffers);
	FASTEST_ITERATE_END();
}

void D3D9GraphicSystem::handlePresentResult(DWORD hRet)
{
	if(!__bDeviceLost && hRet == D3DERR_DEVICELOST)
	{
		_onDeviceLost();

		DeviceEvent e(DeviceEvent::DEVICE_LOST, this);
		DispatchEvent(e);

		__bDeviceLost = TRUE;
	}
}

VertexProgram* D3D9GraphicSystem::CreateVertexProgram()
{
	return XNEW D3D9VertexProgram(this);
}

FragmentProgram* D3D9GraphicSystem::CreateFragmentProgram()
{
	return XNEW D3D9FragmentProgram(this);
}

PixelBuffer* D3D9GraphicSystem::createPixelBuffer()
{
	return XNEW D3D9PixelBuffer(this);
}

PixelBuffer* D3D9GraphicSystem::CreateDepthStencilBuffer(int nWidth, int nHeight, PixelFormat pixelFormat)
{
	D3D9DepthStencilBuffer* pDepthStencilBuffer = XNEW D3D9DepthStencilBuffer(this);
	
	pDepthStencilBuffer->internalInit(nWidth, nHeight, pixelFormat);
	debug_ptr_assign(pDepthStencilBuffer, bManaged, TRUE);

	return pDepthStencilBuffer;
}

RenderTexture* D3D9GraphicSystem::CreateRenderTexture(const String& strName, int nWidth, int nHeight
														, PixelFormat pixelFormat
														, PixelFormat depthStencilFormat)
{
	D3D9RenderTexture* pRenderTexture = XNEW D3D9RenderTexture(strName, this);
	pRenderTexture->internalInit(nWidth, nHeight, pixelFormat, depthStencilFormat);

	return pRenderTexture;
}

DynamicTexture* D3D9GraphicSystem::CreateDynamicTexture(const String& strName, int nWidth, int nHeight
												   , PixelFormat pixelFormat)
{
	D3D9DynamicTexture* pDynamicTexture = XNEW D3D9DynamicTexture(strName, this);
	pDynamicTexture->internalInit(nWidth, nHeight, pixelFormat);

	return pDynamicTexture;
}

VertexLayout* D3D9GraphicSystem::CreateVertexLayout()
{
	D3D9VertexLayout* pVertexLayout = XNEW D3D9VertexLayout(this);
	return pVertexLayout;
}	

WindowRenderTarget* D3D9GraphicSystem::CreateWindowRenderTarget(Window* pWindow)
{
	D3D9WindowRenderTarget* pWindowRenderTarget = XNEW D3D9WindowRenderTarget(this, pWindow);
	pWindowRenderTarget->internalInit();

	return pWindowRenderTarget;
}

IndexBuffer* D3D9GraphicSystem::CreateIndexBuffer(UINT nIndexCount, UINT nIndexSize, BOOL bDynamic)
{
	D3D9IndexBuffer* pIndexBuffer = XNEW D3D9IndexBuffer(this, nIndexCount, nIndexSize);
	pIndexBuffer->internalInit();

	return pIndexBuffer;
}

VertexBuffer* D3D9GraphicSystem::CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
{
	D3D9VertexBuffer* pVertexBuffer = XNEW D3D9VertexBuffer(this, nVertexCount, nVertexSize, bDynamic);
	pVertexBuffer->internalInit();

	// If in default pool, should listen DEVICE_LOST/DEVICE_RESET event.
	if(bDynamic)
	{
		AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9VertexBuffer::_onDeviceLost, pVertexBuffer));
		AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9VertexBuffer::_onDeviceReset, pVertexBuffer));
	}

	return pVertexBuffer;
}

Texture* D3D9GraphicSystem::CreateTexture(TextureType textureType, const String& strName)
{
	Texture* pTexture = NULL;

	switch (textureType)
	{
	case TEXTURE_RENDER_TARGET:
		pTexture = XNEW D3D9RenderTexture(strName, this);
		break;

	case TEXTURE_NORMAL:
		pTexture = XNEW D3D9Texture(strName, this);
		break;

	case TEXTURE_CUBE:
		pTexture = XNEW D3D9CubeTexture(strName, this);
		break;

	default:
		XASSERT(0);
	}

	return pTexture;
}

void D3D9GraphicSystem::_disableTextureUnit(int iStageIndex)
{
	HRESULT hRet;
	hRet = __pD3D9Device->SetTexture(iStageIndex, NULL);
	XASSERT(hRet == D3D_OK);

	// Disable vertex texture
	//...
}

void D3D9GraphicSystem::_setTextureUnitState(int texUnitIndex, TextureUnitState* pTextureUnitState)
{
	TexturePtr& pTexture = pTextureUnitState->GetTexturePtr();

	_D3D9TextureBaseImpl* pTextureBaseImpl = _D3D9TextureBaseImpl::CastToTextureBase(pTexture);

	XASSERT(pTextureBaseImpl);

	__setTexture(texUnitIndex, pTextureBaseImpl->getD3D9Texture());

	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSU, pTextureUnitState->GetAddressModeU());
	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSV, pTextureUnitState->GetAddressModeV());
	__setSamplerState(texUnitIndex, D3DSAMP_ADDRESSW, pTextureUnitState->GetAddressModeW());

	__setSamplerState(texUnitIndex, D3DSAMP_MAGFILTER, pTextureUnitState->GetMagFilterType());
	__setSamplerState(texUnitIndex, D3DSAMP_MINFILTER, pTextureUnitState->GetMinFilterType());
	__setSamplerState(texUnitIndex, D3DSAMP_MIPFILTER, pTextureUnitState->GetMipFilterType());


	if(pTextureUnitState->GetAddressModeU() == TEX_ADDRESS_BORDER
		|| pTextureUnitState->GetAddressModeV() == TEX_ADDRESS_BORDER
		|| pTextureUnitState->GetAddressModeW() == TEX_ADDRESS_BORDER)
	{
		__setSamplerState(texUnitIndex, D3DSAMP_BORDERCOLOR, pTextureUnitState->GetBorderColor());
	}
}

void D3D9GraphicSystem::_onDeviceLost()
{
	_pCurrentShader = NULL;
	_pCurrentRenderTarget = NULL;

	__pD3D9Device->SetIndices(NULL);
	__pD3D9Device->SetStreamSource(0, NULL, 0, 0);
}

void D3D9GraphicSystem::_onDeviceReset()
{
	__iLastTexDisableFrom = _pGraphicSysCap->GetMaxTextureUnitCount();
	__iLastVertexTexDisableFrom = _pGraphicSysCap->GetMaxVertexTextureUnitCount();
	__iLastRTDisableFrom = _pGraphicSysCap->GetMultiRenderTargetCount();

	// We will use a shader based lighting, so disable D3D Lighting
	__setRenderState(D3DRS_LIGHTING, FALSE);
}

void D3D9GraphicSystem::__initWndPresentParameters(HWND hWnd)
{
	if(!__pD3DPresentParam) 
	{
		__pD3DPresentParam = new D3DPRESENT_PARAMETERS;
		memset(__pD3DPresentParam, 0, sizeof(D3DPRESENT_PARAMETERS));
	}

	// Use the current display mode. 
	D3DDISPLAYMODE mode;
	if(FAILED(__pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT , &mode))) 
	{
		XTHROWEX("GetAdapterDisplayMode failed!!!");	
	}

	__pD3DPresentParam->Windowed = TRUE;
	__pD3DPresentParam->SwapEffect = D3DSWAPEFFECT_COPY;
	__pD3DPresentParam->BackBufferFormat = D3DFMT_UNKNOWN;
	__pD3DPresentParam->EnableAutoDepthStencil = TRUE;
	__pD3DPresentParam->AutoDepthStencilFormat = D3DFMT_D24S8;
	__pD3DPresentParam->BackBufferWidth = 16;
	__pD3DPresentParam->BackBufferHeight = 16;
	__pD3DPresentParam->hDeviceWindow = hWnd;
	__pD3DPresentParam->BackBufferCount = 1;
	__pD3DPresentParam->PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
}

DWORD D3D9GraphicSystem::__tryResetDevice()
{
	HRESULT hRet = S_FALSE;
	try
	{
		hRet = __pD3D9Device->Reset(__pD3DPresentParam);
		if(hRet == D3D_OK)
		{
			_onDeviceReset();

			DeviceEvent e(DeviceEvent::DEVICE_RESET, this);
			DispatchEvent(e);
		}
	}
	catch(Exception& e)
	{
		e.what();
		XASSERT(0);
	}

	return hRet;
}

void D3D9GraphicSystem::SetClipPlane(UINT iIndex, BOOL bEnable, const Plane& clipPlane)
{
	if(bEnable)
	{
		__dwClipPlaneFlag |= 1 << iIndex;
		__pD3D9Device->SetClipPlane(iIndex, (float*)&clipPlane);
	}
	else
	{
		__dwClipPlaneFlag &= !(1 << iIndex);
	}
	
	__setRenderState(D3DRS_CLIPPLANEENABLE, __dwClipPlaneFlag);
}

_X3D_NS_END