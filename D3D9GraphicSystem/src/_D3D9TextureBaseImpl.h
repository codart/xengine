#pragma once

#include "_D3D9TextureImpl.h"

_X3D_NS_BEGIN

class _D3D9TextureBaseImpl;

typedef _D3D9TextureImpl<Texture, _D3D9TextureBaseImpl> D3D9TextureBase;
typedef _D3D9TextureImpl<RenderTexture, _D3D9TextureBaseImpl> D3D9RenderTextureBase;
typedef _D3D9TextureImpl<DynamicTexture, _D3D9TextureBaseImpl> D3D9DynamicTextureBase;
typedef _D3D9TextureImpl<CubeTexture, _D3D9TextureBaseImpl> D3D9CubeTextureBase;

class _D3D9TextureBaseImpl
{
package:
	// --- concrete class public methods 
	void 
		internalInit(IDirect3DTexture9* pTexture){ __pD3DTexture9 = pTexture; }

	INLINE IDirect3DBaseTexture9*
		getD3D9Texture(){return __pD3DTexture9;}


	INLINE static _D3D9TextureBaseImpl* CastToTextureBase(Texture* pTexture)
	{
		_D3D9TextureBaseImpl* pTextureBase;
		switch (pTexture->getTextureType())
		{
		case TEXTURE_NORMAL:
			{
				D3D9TextureBase* pTTexture = (D3D9TextureBase*)pTexture;
				pTextureBase = (_D3D9TextureBaseImpl*)pTTexture;
			}
			break;

		case TEXTURE_RENDER_TARGET:
			{
				D3D9RenderTextureBase* pTTexture = (D3D9RenderTextureBase*)pTexture;
				pTextureBase = (_D3D9TextureBaseImpl*)pTTexture;
			}
			break;

		case TEXTURE_CUBE:
			{
				D3D9CubeTextureBase* pTTexture = (D3D9CubeTextureBase*)pTexture;
				pTextureBase = (_D3D9TextureBaseImpl*)pTTexture;
			}
			break;

		case TEXTURE_DYNAMIC:
			{
				D3D9DynamicTextureBase* pTTexture = (D3D9DynamicTextureBase*)pTexture;
				pTextureBase = (_D3D9TextureBaseImpl*)pTTexture;
			}
			break;
		default:
			XASSERT(0);
		}
		return pTextureBase;
	}


protected:
	IDirect3DBaseTexture9* 
		__pD3DTexture9;


};


_X3D_NS_END