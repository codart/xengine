#pragma once

#include "d3d9texture.h"
#include "x3d/resource/CubeTexture.h"

_X3D_NS_BEGIN

class D3D9CubeTexture : public D3D9CubeTextureBase
{
	typedef D3D9CubeTextureBase BaseClass;

public:
	D3D9CubeTexture(const String& strName, GraphicSystem* pGraphicSystem);
	~D3D9CubeTexture();


public:
	override void 
		LoadImage(const String& strImageFIle);

package:
	INLINE IDirect3DCubeTexture9*
		getDirect3DCubeTexture9() {return (IDirect3DCubeTexture9*)__pD3DTexture9; }

};

_X3D_NS_END