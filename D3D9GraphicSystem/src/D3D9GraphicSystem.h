#pragma once

#include "x3d/graphic/GraphicSystem.h"

_X3D_NS_BEGIN

class _D3D9TextureBaseImpl;
class D3D9Texture;
class D3D9DepthStencilBuffer;

class D3D9GraphicSystem :  public GraphicSystem
{
public:
	D3D9GraphicSystem(void);
	~D3D9GraphicSystem(void);


public:
	override void 
		Init(DWORD_PTR dwInitData);
	override void 
		Uninit();
	
	override BOOL
		IsReady();

	// Factory methods
	override VertexProgram*
		CreateVertexProgram();
	override FragmentProgram*
		CreateFragmentProgram();

	override VertexLayout* 
		CreateVertexLayout();
	override WindowRenderTarget* 
		CreateWindowRenderTarget(Window* pWindow);
	override VertexBuffer* 
		CreateVertexBuffer(UINT nVertexCount, UINT nVertexSize, BOOL bDynamic = FALSE);
	override IndexBuffer* 
		CreateIndexBuffer(UINT nIndexCount, UINT nIndexSize, BOOL bDynamic = FALSE);

	override RenderTexture*
		CreateRenderTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat
							, PixelFormat depthStencilFormat);
	override DynamicTexture* 
		CreateDynamicTexture(const String& strName, int nWidth, int nHeight
							, PixelFormat pixelFormat);
	virtual PixelBuffer*
		CreateDepthStencilBuffer(int nWidth, int nHeight, PixelFormat pixelFormat = PIXEL_FORMAT_D24S8);
	
	override INLINE void 
		SetTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	override void
		SetTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	override void
		SetTextureState(UINT texUnitIndex, TextureState* pTextureState);
	override void 
		SetTexture(UINT texUnitIndex, Texture* pTexture);

	override void 
		SetVertexTextureUnitState(UINT texUnitIndex, TextureUnitState* pTextureUnitState);
	override void 
		SetVertexTextureUnitStates(const TextureUnitStateList& textureUnitStates);

	override Texture* 
		CreateTexture(TextureType textureType, const String& strName);

	// ---
	override void 
		ClearFrameBuffer(DWORD dwBufferFlag = FRAME_BUFFER_COLOR | FRAME_BUFFER_DEPTH | FRAME_BUFFER_STENCIL
						, DWORD color = COLOR_ARGB(0, 0, 0, 0)
						, Real depth = 1.0f
						, unsigned short stencil = 0);


	INLINE override void
		SetVertexLayout(VertexLayout* pVertexDeclearation);

	override void
		SetViewport(const ViewportInfo* pViewportInfo);


	override BOOL
		SetRenderTarget(RenderTarget* pRenderTarget);
	override BOOL
		SetMultiRenderTarget(MultiRenderTarget* pMultiRenderTarget);
	virtual BOOL
		SetColorBuffer(int iBufferIndex, PixelBuffer* pColorBuffer);
	virtual BOOL
		SetDepthStencilBuffer(PixelBuffer* pColorBuffer);


	override DWORD
		BeginDrawing();
	override DWORD
		EndDrawing();

	override void
		BeginUpdateWindow(WindowRenderTarget* pWndTarget);
	override void
		EndUpdateWindow();

	override DWORD
		DrawPrimitive(const DrawPrimitiveCommand* pRenderOperation);

	INLINE override DWORD 
		SetVertexBuffer(int iStreamIndex, VertexBuffer* pVertexBuffer);

	INLINE override void
		SetVertexBuffers(const VertexBufferPtrList& vertexBuffers);
	INLINE override DWORD
		SetIndexBuffer(IndexBuffer* pIndexBuffer);

	override DWORD
		DrawPrimitive(DrawPrimitiveType primitiveType, int nVertexCount, int iStartIndex, int nPrimitiveCount);

	override void
		SetColorBufferParams(BOOL bWriteRed, BOOL bWriteGreen, BOOL bWriteBlue, BOOL bWriteAlpha);
	override void
		SetDepthBufferParams(BOOL bDepthTest, BOOL bDepthWrite
							, CompareFunction depthFunction = COMPARE_FUNC_LESS_EQUAL);
	override void
		SetStencilBufferParams(BOOL bEnableStencil = TRUE, CompareFunction func = COMPARE_FUNC_ALWAYS_PASS, 
								UINT refValue = 0, UINT mask = 0xFFFFFFFF, 
								StencilOperation stencilFailOp = STENCIL_OP_KEEP, 
								StencilOperation depthFailOp = STENCIL_OP_KEEP,
								StencilOperation stencilAndZPassOp = STENCIL_OP_KEEP, 
								BOOL bTwoSidedOperation = FALSE);

	override void 
		EnableColorBufferWrite(BOOL bEnable);

	override void 
		SetSceneBlending(FrameBlendFactor sourceFactor
						, FrameBlendFactor destFactor
						, FrameBlendOperation blendOp);

	override void
		SetAlphaTestParams(BOOL bEnable, CompareFunction func, BYTE value, BOOL alphaToCoverage = FALSE);

	override void 
		SetShader(Shader* pShader);
	override void 
		SetCullMode(HardwareCullMode cullMode);
	override void 
		SetClipPlane(UINT iIndex, BOOL bEnable = TRUE, const Plane& clipPlane = Plane(0, 1, 0, 0));


package:
	override PixelBuffer*
		createPixelBuffer();
	INLINE LPDIRECT3DDEVICE9
		getD3D9Device(){return __pD3D9Device;};
	INLINE BOOL
		isDeviceLost(){return __bDeviceLost;};
	void 
		handlePresentResult(DWORD hRet);


protected:
	// Note: pTexture should not be null. To clear a texture stage, use DisableTexture.
	INLINE void 
		_setTextureUnitState(int texUnitIndex, TextureUnitState* pTextureUnitState);
	INLINE void
		_disableTextureUnit(int iStageIndex);

	INLINE void 
		_disableUnusedColorBuffer(int iDisableFrom);

	INLINE void 
		_onDeviceReset();
	INLINE void
		_onDeviceLost();


protected:
	LPDIRECT3D9             
		__pD3D9; // Used to create the D3DDevice
	LPDIRECT3DDEVICE9
		__pD3D9Device; // Our rendering device

	D3DPRESENT_PARAMETERS*
		__pD3DPresentParam; 

	BOOL
		__bDeviceLost;

	// For disable texture unit stage.
	Int 
		__iLastTexDisableFrom;

	// For disable render target.
	Int
		__iLastRTDisableFrom;

	// For disable vertex texture unit stage.
	Int 
		__iLastVertexTexDisableFrom;

	// Cached viewport info which used to set the viewport to full window(screen).
	ViewportInfo 
		__fullWndViewportInfo;

	DWORD
		__dwClipPlaneFlag;


private:
	INLINE HRESULT 
		__setSamplerState(UINT texUnitIndex, D3DSAMPLERSTATETYPE sampleStateType, DWORD value);
	INLINE HRESULT
		__setRenderState(D3DRENDERSTATETYPE state, DWORD value);
	INLINE HRESULT
		__setTexture(UINT texUnitIndex, IDirect3DBaseTexture9* pD3D9BaseTexture);

	void 
		__initWndPresentParameters(HWND hWnd);
	DWORD 
		__tryResetDevice();


};


_X3D_NS_END