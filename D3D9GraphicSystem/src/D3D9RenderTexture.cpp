#include "StdAfx.h"
#include "D3D9RenderTexture.h"

#include "x3d/graphic/RenderTarget.h"
#include "x3d/graphic/DeviceEvent.h"
#include "D3D9GraphicSystem.h"
#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN

D3D9RenderTexture::D3D9RenderTexture(const String& strName, GraphicSystem* pGraphicSystem)
: BaseClass(strName, pGraphicSystem)
{
	__bDeviceEventListened = false;
}

D3D9RenderTexture::~D3D9RenderTexture()
{
	if(__bDeviceEventListened)
	{
		_pGraphicSystem->RemoveEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9RenderTexture::_onDeviceLost, this));
		_pGraphicSystem->RemoveEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9RenderTexture::_onDeviceReset, this));
	}
}

void D3D9RenderTexture::Resize(int nWidth, int nHeight)
{
	internalReleaseAll();
	internalInit(nWidth, nHeight, _pixelFormat, _depthStencilFormat);
}

void D3D9RenderTexture::internalInit(int nWidth, int nHeight, PixelFormat pixelFormat, PixelFormat depthStencilFormat)
{
	XASSERT(__pD3DTexture9 == NULL);

	LPDIRECT3DDEVICE9 pDirect3DDevice9 = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();
 
	LPDIRECT3DTEXTURE9 pD3D9Texture = NULL;
	HRESULT hResult = pDirect3DDevice9->CreateTexture(nWidth, nHeight
									, 1 , D3DUSAGE_RENDERTARGET
									, (D3DFORMAT)pixelFormat, D3DPOOL_DEFAULT
									, &pD3D9Texture, NULL);

	if(hResult !=  D3D_OK)
	{
		XASSERT(0);
	}

	_pixelFormat = pixelFormat;
	_depthStencilFormat = depthStencilFormat;
	_width = nWidth;
	_height = nHeight;
	_nMipmapCount = 1;

	_D3D9TextureImpl::internalInit(pD3D9Texture);

	if(!__bDeviceEventListened)
	{
		_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9RenderTexture::_onDeviceLost, this));
		_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9RenderTexture::_onDeviceReset, this));

		__bDeviceEventListened = true;
	}
}

void D3D9RenderTexture::_onDeviceLost(const Event* pEvent)
{
	internalReleaseAll();
}

void D3D9RenderTexture::internalReleaseAll()
{
	SAFE_DELETE(_pTextureTarget);

	// Release texture pixel buffers
	_unbindPixelBuffers();

	SAFE_CHECKED_RELEASE(__pD3DTexture9, 0);
}

void D3D9RenderTexture::_onDeviceReset(const Event* pEvent)
{
	XASSERT(_width && _height && _pixelFormat != PIXEL_FORMAT_UNKNOWN && _depthStencilFormat != PIXEL_FORMAT_UNKNOWN);
	internalInit(_width, _height, _pixelFormat, _depthStencilFormat);
}

_X3D_NS_END