#include "StdAfx.h"
#include "D3D9IndexBuffer.h"

#include "D3D9GraphicSystem.h"

D3D9IndexBuffer::D3D9IndexBuffer(GraphicSystem* pGraphicSystem, UINT nIndexCount
								, UINT nIndexSize, BOOL bDynamic)
: IndexBuffer(pGraphicSystem, nIndexCount, nIndexSize)
{
	__pD3DIndexBuffer9 = NULL;
}

D3D9IndexBuffer::~D3D9IndexBuffer(void)
{
	SAFE_CHECKED_RELEASE(__pD3DIndexBuffer9, 0);
}

InternalObjectHandle D3D9IndexBuffer::_getInternalObjHandle()
{
	return InternalObjectHandle(__pD3DIndexBuffer9);
}

void D3D9IndexBuffer::internalInit()
{
	IDirect3DDevice9* pD3D9Device = ((D3D9GraphicSystem*)_pGraphicSystem)->getD3D9Device();

	UINT nSizeInBytes = _nIndexCount * _nIndexSize;

	D3DFORMAT indexFormat = D3DFMT_INDEX16;
	if(_nIndexSize == 32) indexFormat = D3DFMT_INDEX32;

	DWORD dwUsage = _bDynamic? D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY : 0;
	D3DPOOL poolType = _bDynamic? D3DPOOL_DEFAULT : D3DPOOL_MANAGED;

	if( FAILED (pD3D9Device->CreateIndexBuffer(nSizeInBytes, dwUsage, indexFormat
		, poolType, &__pD3DIndexBuffer9, NULL)))
	{
		XTHROWEX("IndexBuffer::internalInit: pD3D9Device->CreateIndexBuffer() failed!!!");
	}

	debug_assign(bufferState, GBS_INITED);
}

void D3D9IndexBuffer::WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer)
{
	debug_check(bufferState == GBS_INITED);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	XASSERT(__pD3DIndexBuffer9);

	BYTE* pBuffer = NULL;
	HRESULT hRet = __pD3DIndexBuffer9->Lock(offset, length, (void**)&pBuffer, 0);
	XASSERT(hRet == D3D_OK);

	//BYTE* pByte = BYTE[length];
	memcpy(pBuffer, pSource, length);

	hRet = __pD3DIndexBuffer9->Unlock();
	XASSERT(hRet == D3D_OK);
}

void* D3D9IndexBuffer::Lock(UINT offsetToLock, UINT nByteCount, LockOptions dwLockOptions)
{
	debug_check(bufferState == GBS_INITED || bufferState == GBS_HAS_VALID_DATA);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	debug_check(bLocked == FALSE);
	debug_assign(bLocked, TRUE);

	XASSERT(__pD3DIndexBuffer9);

	BYTE* pBuffer = NULL;
	HRESULT hRet = __pD3DIndexBuffer9->Lock(offsetToLock, nByteCount, (void**)&pBuffer, dwLockOptions);
	XASSERT(hRet == D3D_OK);

	return pBuffer;
}

void D3D9IndexBuffer::Unlock()
{
	debug_check(bLocked == TRUE);

	HRESULT hRet = __pD3DIndexBuffer9->Unlock();
	XASSERT(hRet == D3D_OK);

	debug_assign(bLocked, FALSE);
}
