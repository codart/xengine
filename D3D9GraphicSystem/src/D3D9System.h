#include "x3d/PlugableModule.h"

#if defined(XD3D9_EXPORT)
#define _XD3D9GSExport __declspec(dllexport)
#else
#define _XD3D9GSExport __declspec(dllimport)
#endif


extern "C"
{
	 _XD3D9GSExport x3d::PlugableModule* GetPlugableModule();
}
