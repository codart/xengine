#include "StdAfx.h"
#include "D3D9FragmentProgram.h"

#include "x3d/render/AutoParameterDataProvider.h"
#include "D3D9GraphicSystem.h"

_X3D_NS_BEGIN

D3D9FragmentProgram::D3D9FragmentProgram(GraphicSystem* pGraphicSystem)
: FragmentProgram(pGraphicSystem)
{
	__pD3DPixelShader9 = NULL;
}

D3D9FragmentProgram::~D3D9FragmentProgram(void)
{
	SAFE_CHECKED_RELEASE(__pD3DPixelShader9, 0);
}

void D3D9FragmentProgram::LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile)
{
	XASSERT(!__pD3DPixelShader9);

	LPD3DXBUFFER pShaderBuffer = NULL;
	LPD3DXBUFFER pErrorBuffer = NULL;

	// Compile the HLSL function
	HRESULT hRet = D3DXCompileShaderFromFile(strFileName.c_str(), NULL, NULL
		, strEntryPoint.c_str()
		, strProfile.c_str(), D3DXSHADER_USE_LEGACY_D3DX9_31_DLL
		, &pShaderBuffer, &pErrorBuffer, &__pConstantTable);

	if( pErrorBuffer )
	{
		::MessageBoxA(0, (char*)pErrorBuffer->GetBufferPointer(), 0, 0);
		pErrorBuffer->Release();	
	}

	if(hRet != D3D_OK)
	{
		XASSERT(0);
	}

	// Create fragment shader
	IDirect3DDevice9* pD3D9Device = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();
	hRet = pD3D9Device->CreatePixelShader((DWORD*)pShaderBuffer->GetBufferPointer(), &__pD3DPixelShader9);
	SAFE_RELEASE(pShaderBuffer);
	if(hRet != D3D_OK)
	{
		XASSERT(0);
	}

	// Construct the constant map
	D3DXCONSTANTTABLE_DESC constantTableDesc;
	__pConstantTable->GetDesc(&constantTableDesc);

	for (UINT i = 0; i< constantTableDesc.Constants; ++i)
	{
		D3DXHANDLE hConstant = __pConstantTable->GetConstant(NULL, i);
		XASSERT(hConstant);

		D3DXCONSTANT_DESC constantDesc;
		UINT nCount = 1;
		__pConstantTable->GetConstantDesc(hConstant, &constantDesc, &nCount);

		// Texture is set by texture unit state but not shader constant.
		if(constantDesc.Type >= D3DXPT_SAMPLER && constantDesc.Type <= D3DXPT_SAMPLERCUBE) continue;

		GPUProgramConstant constant;
		LPCSTR pConstantName = constantDesc.Name[0] == '$' ? &constantDesc.Name[1] : constantDesc.Name; 
		constant.strConstantName = pConstantName;
		constant.index = constantDesc.RegisterIndex;
		constant.registerCount = constantDesc.RegisterCount;
		constant.sizeInBytes = constantDesc.Bytes;
		constant.constantType = _constantTypeFromRawType(constantDesc.Type);

		_gpuConstantMap.insert(make_pair(constant.strConstantName, constant));		
	}

}

void D3D9FragmentProgram::UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider)
{		
	FASTEST_VECTOR_ITERATE(AutoParameter, _autoParameters);
		AutoParameter& autoParameter = GET_NEXT(_autoParameters);
		XASSERT(autoParameter.pGPUProgramConstant);

		const DataEntry& dataEntry = pAutoParamDataProvider->GetAutoParameterData(autoParameter);

		switch (autoParameter.pGPUProgramConstant->constantType)
		{
		case CONSTANT_TYPE_FLOAT:
			_setFloatConstant(autoParameter.pGPUProgramConstant, dataEntry);
			break;

		case CONSTANT_TYPE_INT:
			_setIntConstant(autoParameter.pGPUProgramConstant, dataEntry);
			break;

		default:
			XASSERT(0);
		}
	
	FASTEST_ITERATE_END();
}

GPUProgramConstantType D3D9FragmentProgram::_constantTypeFromRawType(DWORD dwRawType)
{ 
	switch (dwRawType)
	{
	case D3DXPT_FLOAT:
		return CONSTANT_TYPE_FLOAT;
		break;

	case D3DXPT_INT:
		return CONSTANT_TYPE_INT;
		break;

	default:
		XASSERT(0);
	}

	return CONSTANT_TYPE_INVALID;
}

void D3D9FragmentProgram::_setFloatConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry)
{
	HRESULT hRet = 0 ;

	//...XASSERT(dataEntry.nFloat4Count <= pGPUConstant->registerCount && dataEntry.nFloat4Count != 0);

	IDirect3DDevice9* pD3D9Device = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();
	hRet = pD3D9Device->SetPixelShaderConstantF(pGPUConstant->index
						, (float*)dataEntry.pDataPointer, pGPUConstant->registerCount/*, dataEntry.nFloat4Count*/);
	XASSERT(hRet == D3D_OK);

}

void D3D9FragmentProgram::_setIntConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry)
{
	XASSERT(0);
}

_X3D_NS_END