#pragma once

#include "x3d/resource/VertexProgram.h"

_X3D_NS_BEGIN

class DataEntry;

class D3D9VertexProgram : public VertexProgram
                        , public Final<D3D9VertexProgram>
{
public:
	D3D9VertexProgram(GraphicSystem* pGraphicSystem);
	~D3D9VertexProgram(void);

public:
	override void 
		LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile);
	override void 
		UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider);


package:
	// ---
	INLINE IDirect3DVertexShader9*
		getD3DVertexShader9(){return __pD3DVertexShader9;};


protected:
	override GPUProgramConstantType 
		_constantTypeFromRawType(DWORD dwRawType);

	INLINE void 
		_setFloatConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry);
	INLINE void 
		_setIntConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry);


protected:
	IDirect3DVertexShader9*
		__pD3DVertexShader9;

	LPD3DXBUFFER 
		__pMicroCode;
	LPD3DXCONSTANTTABLE 
		__pConstantTable;

};

_X3D_NS_END