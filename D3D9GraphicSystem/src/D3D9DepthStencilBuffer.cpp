#include "StdAfx.h"
#include "D3D9DepthStencilBuffer.h"

#include "D3D9GraphicSystem.h"
#include "x3d/graphic/DeviceEvent.h"

_X3D_NS_BEGIN

D3D9DepthStencilBuffer::D3D9DepthStencilBuffer(GraphicSystem* pGraphicSystem)
: D3D9PixelBuffer(pGraphicSystem)
{
	_pixelFormat = PIXEL_FORMAT_UNKNOWN;
	_nWidth = 0;
	_nHeight = 0;

	pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9DepthStencilBuffer::_onDeviceLost, this));
	pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9DepthStencilBuffer::_onDeviceReset, this));
}

D3D9DepthStencilBuffer::~D3D9DepthStencilBuffer(void)
{
}

void D3D9DepthStencilBuffer::internalInit(int nWidth, int nHeight, PixelFormat pixelFormat)
{
	XASSERT(_pDirect3DSurface9 == NULL);

	IDirect3DSurface9* pTempSurface = NULL;
	IDirect3DDevice9* pD3DDevice9 = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

	HRESULT hRet = pD3DDevice9->CreateDepthStencilSurface(nWidth, nHeight
		, D3DFORMAT(pixelFormat)
		, D3DMULTISAMPLE_NONE
		, 0 
		, FALSE
		, &pTempSurface
		, NULL);
	XASSERT(hRet == D3D_OK);

	_pixelFormat = pixelFormat;
	_nWidth = nWidth;
	_nHeight = nHeight;

	D3D9PixelBuffer::internalInit(pTempSurface);
}

void D3D9DepthStencilBuffer::_onDeviceLost(const Event* pEvent)
{
	internalRelease();
}

void D3D9DepthStencilBuffer::_onDeviceReset(const Event* pEvent)
{
	XASSERT(_pixelFormat != PIXEL_FORMAT_UNKNOWN);
	XASSERT(_nWidth !=0 );
	XASSERT(_nHeight !=0 );

	internalInit(_nWidth, _nHeight, _pixelFormat);
}

_X3D_NS_END