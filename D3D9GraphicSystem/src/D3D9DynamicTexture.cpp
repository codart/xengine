#include "StdAfx.h"
#include "D3D9DynamicTexture.h"

#include "x3d/graphic/RenderTarget.h"
#include "x3d/graphic/DeviceEvent.h"
#include "D3D9GraphicSystem.h"
#include "D3D9PixelBuffer.h"

_X3D_NS_BEGIN

D3D9DynamicTexture::D3D9DynamicTexture(const String& strName, GraphicSystem* pGraphicSystem)
: BaseClass(strName, pGraphicSystem)
{
	_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9DynamicTexture::_onDeviceLost, this));
	_pGraphicSystem->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9DynamicTexture::_onDeviceReset, this));
}

D3D9DynamicTexture::~D3D9DynamicTexture()
{
	_pGraphicSystem->RemoveEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&D3D9DynamicTexture::_onDeviceLost, this));
	_pGraphicSystem->RemoveEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&D3D9DynamicTexture::_onDeviceReset, this));
}

void D3D9DynamicTexture::internalInit(int nWidth, int nHeight, PixelFormat pixelFormat)
{
	XASSERT(__pD3DTexture9 == NULL);

	LPDIRECT3DDEVICE9 pDirect3DDevice9 = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();
 
	LPDIRECT3DTEXTURE9 pD3D9Texture = NULL;
	HRESULT hResult = pDirect3DDevice9->CreateTexture(nWidth, nHeight
									, 1 , D3DUSAGE_DYNAMIC
									, (D3DFORMAT)pixelFormat, D3DPOOL_DEFAULT
									, &pD3D9Texture, NULL);

	if(hResult !=  D3D_OK)
	{
		XASSERT(0);
	}

	_pixelFormat = pixelFormat;
	_width = nWidth;
	_height = nHeight;
	_nMipmapCount = 1;

	_D3D9TextureImpl::internalInit(pD3D9Texture);
}

void D3D9DynamicTexture::_onDeviceLost(const Event* pEvent)
{
	internalReleaseAll();
}

void D3D9DynamicTexture::internalReleaseAll()
{
	//...SAFE_DELETE(_pTextureTarget);

	// Release texture pixel buffers
	_unbindPixelBuffers();

	SAFE_CHECKED_RELEASE(__pD3DTexture9, 0);
}

void D3D9DynamicTexture::_onDeviceReset(const Event* pEvent)
{
	XASSERT(_width && _height && _pixelFormat != PIXEL_FORMAT_UNKNOWN);
	internalInit(_width, _height, _pixelFormat);
}

_X3D_NS_END