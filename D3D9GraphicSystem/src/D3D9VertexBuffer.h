#pragma once

#include "x3d/graphic/VertexBuffer.h"

_X3D_NS_BEGIN

class D3D9VertexBuffer : public VertexBuffer
{
	friend class D3D9GraphicSystem;

public:
	D3D9VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic = FALSE);
	~D3D9VertexBuffer(void);

public:
	override void 
		WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer = false);

	override void*
		Lock(UINT OffsetToLock, UINT nByteCount, LockOptions dwLockOptions);
	override void 
		Unlock();


package:
	void 
		internalInit();
	void 
		internalReleaseAll();
	INLINE IDirect3DVertexBuffer9*
		getD3D9VertexBuffer() { return __pD3DVertexBuffer9; };


protected:
	override InternalObjectHandle
		_getInternalObjHandle();

	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);


private:
	IDirect3DVertexBuffer9*
		__pD3DVertexBuffer9;


};

_X3D_NS_END