#pragma once

#include "x3d/resource/FragmentProgram.h"

_X3D_NS_BEGIN

class DataEntry;

class D3D9FragmentProgram : public FragmentProgram
                          , public Final<D3D9FragmentProgram>
{
public:
	D3D9FragmentProgram(GraphicSystem* pGraphicSystem);
	~D3D9FragmentProgram(void);

public:
	override void 
		UpdateAutoParameters(const AutoParameterDataProvider* pAutoParamDataProvider);
	override void 
		LoadFromFile(const String& strFileName, const AString& strEntryPoint, const AString& strProfile);

	// ---
	INLINE IDirect3DPixelShader9*
		getD3DPixelShader9(){ return __pD3DPixelShader9; }


protected:
	override GPUProgramConstantType
		_constantTypeFromRawType(DWORD dwRawType);
	INLINE void 
		_setFloatConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry);
	INLINE void 
		_setIntConstant(const GPUProgramConstant* pGPUConstant, const DataEntry& dataEntry);


protected:
	IDirect3DPixelShader9*
		__pD3DPixelShader9;
	LPD3DXBUFFER 
		__pMicroCode;
	LPD3DXCONSTANTTABLE 
		__pConstantTable;

};


_X3D_NS_END