#pragma once

#include "d3d9texture.h"
#include "x3d/resource/RenderTexture.h"

_X3D_NS_BEGIN

class D3D9DynamicTexture : public D3D9DynamicTextureBase
{
	friend class D3D9GraphicSystem;
	typedef D3D9DynamicTextureBase BaseClass;

public:
	D3D9DynamicTexture (const String& strName, GraphicSystem* pGraphicSystem);
	~D3D9DynamicTexture ();


public:
	INLINE void 
		internalInit(int nWidth, int nHeight, PixelFormat pixelFormat);
	INLINE void 
		internalReleaseAll();


protected:
	virtual void
		_onDeviceLost(const Event* pEvent);
	virtual void
		_onDeviceReset(const Event* pEvent);

};

_X3D_NS_END