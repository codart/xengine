#include "StdAfx.h"
#include "D3D9VertexBuffer.h"

#include "D3D9GraphicSystem.h"

_X3D_NS_BEGIN

D3D9VertexBuffer::D3D9VertexBuffer(GraphicSystem* pGraphicSystem, UINT nVertexCount, UINT nVertexSize, BOOL bDynamic)
: VertexBuffer(pGraphicSystem, nVertexCount, nVertexSize, bDynamic)
{
	XASSERT(pGraphicSystem && nVertexCount >= 0 && nVertexSize >= 0);
	__pD3DVertexBuffer9 = NULL;
}

D3D9VertexBuffer::~D3D9VertexBuffer(void)
{
	internalReleaseAll();
}

void D3D9VertexBuffer::internalInit()
{
	if(_nVertexCount == 0) return;

	IDirect3DDevice9* pD3D9Device = SAFE_CAST(_pGraphicSystem, D3D9GraphicSystem*)->getD3D9Device();

	UINT nSizeInBytes = _nVertexCount * _nVertexSize;

	DWORD dwUsage = _bDynamic? D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY : 0;
	D3DPOOL poolType = _bDynamic? D3DPOOL_DEFAULT : D3DPOOL_MANAGED;
	if( FAILED (pD3D9Device->CreateVertexBuffer(nSizeInBytes, dwUsage, 0
		, poolType, &__pD3DVertexBuffer9
		, NULL)))
	{
		XTHROWEX("D3D9VertexBuffer::internalInit: pD3D9Device->CreateVertexBuffer() failed!!!");
	}

	debug_check(bufferState == GBS_UNINIT);
	debug_assign(bufferState, GBS_INITED);
}

void D3D9VertexBuffer::internalReleaseAll()
{
	SAFE_CHECKED_RELEASE(__pD3DVertexBuffer9, 0);

	debug_assign(bufferState, GBS_UNINIT);
}

void D3D9VertexBuffer::_onDeviceLost(const Event* pEvent)
{
	internalReleaseAll();
}

void D3D9VertexBuffer::_onDeviceReset(const Event* pEvent)
{
	internalInit();
}

void D3D9VertexBuffer::WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer)
{
	debug_check(bufferState == GBS_INITED || bufferState == GBS_HAS_VALID_DATA);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	BYTE* pBuffer = NULL;
	DWORD dwLockFlag = discardWholeBuffer? D3DLOCK_DISCARD : 0;
	HRESULT hRet = __pD3DVertexBuffer9->Lock(offset, length, (void**)&pBuffer, dwLockFlag);
	XASSERT(hRet == D3D_OK);

	memcpy(pBuffer, pSource, length);

	hRet = __pD3DVertexBuffer9->Unlock();
	XASSERT(hRet == D3D_OK);
}

void* D3D9VertexBuffer::Lock(UINT offsetToLock, UINT nByteCount, LockOptions dwLockOptions)
{
	debug_check(bufferState == GBS_INITED || bufferState == GBS_HAS_VALID_DATA);
	debug_assign(bufferState, GBS_HAS_VALID_DATA);

	debug_check(bLocked == FALSE);
	debug_assign(bLocked, TRUE);

	BYTE* pBuffer = NULL;
	HRESULT hRet = __pD3DVertexBuffer9->Lock(offsetToLock, nByteCount, (void**)&pBuffer, dwLockOptions);
	XASSERT(hRet == D3D_OK);
	
	return pBuffer;
}

void D3D9VertexBuffer::Unlock()
{
	debug_check(bLocked == TRUE);

	HRESULT hRet = __pD3DVertexBuffer9->Unlock();
	XASSERT(hRet == D3D_OK);

	debug_assign(bLocked, FALSE);
}

InternalObjectHandle D3D9VertexBuffer::_getInternalObjHandle()
{
	return (InternalObjectHandle)__pD3DVertexBuffer9;
}


_X3D_NS_END