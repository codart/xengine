#pragma once

#include "x3d/graphic/IndexBuffer.h"

_X3D_NS_BEGIN

class D3D9IndexBuffer : public IndexBuffer
{
public:
	D3D9IndexBuffer(GraphicSystem* pGraphicSystem, UINT nIndexCount, UINT nIndexSize, BOOL bDynamic = FALSE);
	~D3D9IndexBuffer(void);

public:
	override void 
		WriteData(UINT offset, UINT length, const void* pSource, BOOL discardWholeBuffer = false);

	override void*
		Lock(UINT OffsetToLock, UINT nByteCount, LockOptions dwLockOptions);
	override void 
		Unlock();


package:
	//---
	void
		internalInit();
	INLINE IDirect3DIndexBuffer9*
		getD3DIndexBuffer9(){return __pD3DIndexBuffer9;}


protected:
	IDirect3DIndexBuffer9*
		__pD3DIndexBuffer9;


protected:
	override InternalObjectHandle
		_getInternalObjHandle();

};

_X3D_NS_END