#pragma once

class NetMessage
{
public:
	enum { header_length = 4 };
	enum { max_body_length = 512 };

public:
	NetMessage();
	virtual ~NetMessage();

public:
	const char* data() const
	{
		return _pData;
	}

	char* data()
	{
		return _pData;
	}

	size_t length() const
	{
		return header_length + _nBodyLength;
	}

	const char* body() const
	{
		return _pData + header_length;
	}

	char* body()
	{
		return _pData + header_length;
	}

	size_t body_length() const
	{
		return _nBodyLength;
	}

	void body_length(size_t new_length)
	{
		_nBodyLength = new_length;
		if (_nBodyLength > max_body_length)
			_nBodyLength = max_body_length;
	}

	bool decode_header()
	{
		using namespace std; // For strncat and atoi.
		char header[header_length + 1] = "";
		strncat(header, _pData, header_length);
		_nBodyLength = atoi(header);
		if (_nBodyLength > max_body_length)
		{
			_nBodyLength = 0;
			return false;
		}
		return true;
	}

	void encode_header()
	{
		using namespace std; // For sprintf and memcpy.
		char header[header_length + 1] = "";
		sprintf(header, "%4d", _nBodyLength);
		memcpy(_pData, header, header_length);
	}

public:
	char 
		_pData[header_length + max_body_length];
	size_t 
		_nBodyLength;
};

typedef std::deque<NetMessage> NetMsgQueue;
