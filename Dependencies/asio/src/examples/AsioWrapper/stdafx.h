// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "asio.hpp"


using namespace asio;
using asio::ip::tcp;

#define SAFE_DELETE(p) if(p) delete p; p = NULL
#define XASSERT(expression) assert(expression)
#define INLINE __forceinline
#define XNEW new
typedef std::wstring String;
typedef std::string AString;

// TODO: reference additional headers your program requires here
