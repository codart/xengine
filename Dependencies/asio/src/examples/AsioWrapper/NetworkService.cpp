#include "StdAfx.h"
#include "NetworkService.h"
#include <assert.h>

NetworkService::NetworkService(void)
{
	__pIOService = new io_service;
	__pNetThread = NULL;
}

NetworkService::~NetworkService(void)
{
	SAFE_DELETE(__pIOService);
}

void NetworkService::Start()
{
	XASSERT(__pIOService);
	XASSERT(!__pNetThread);
	__pNetThread = new asio::thread(boost::bind(&asio::io_service::run, __pIOService));
}

void NetworkService::Tick()
{
	if(_connections.size() == 0 ) return;

	_connections[0]->tick();
}

void NetworkService::End()
{
	XASSERT(__pNetThread);

	// Close all client connection
	//....
	__pNetThread->join();
}

void NetworkService::Post(Connection* pConnection, const NetMessage& msg)
{
	XASSERT(__pIOService);
	__pIOService->post(boost::bind(&Connection::do_write, pConnection, msg));
}

void NetworkService::Close(Connection* pConnection)
{
	XASSERT(__pIOService);
	__pIOService->post(boost::bind(&Connection::do_close, pConnection));
}

Connection* NetworkService::AddConnection(const String& strConnectionName)
{
	Connection* pConnection = GetConnection(strConnectionName);
	if(pConnection)
	{
		XASSERT(0);
	}

	Connection* pNewConnection = XNEW Connection(this, strConnectionName);
	_connections.push_back(pNewConnection);

	return pNewConnection;
}

Connection* NetworkService::GetConnection(const String& strConnectionName)
{

	return NULL;
}

void NetworkService::RemoveConnection(const String& strConnectionName)
{
	
}


