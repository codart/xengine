#pragma once

#include "Connection.h"

class NetworkService
{
public:
	NetworkService(void);
	virtual ~NetworkService(void);

public:
	virtual void
		Start();
	virtual void
		Tick();
	virtual void
		End();

	virtual Connection*
		AddConnection(const String& strConnectionName);
	virtual void
		RemoveConnection(const String& strConnectionName);
	virtual Connection*
		GetConnection(const String& strConnectionName);

	virtual void
		Post(Connection* pConnection, const NetMessage& msg);
	virtual void
		Close(Connection* pConnection);

	INLINE io_service*
		_getIOService() { return __pIOService; }


protected:
	std::vector<Connection*>
		_connections;

private:
	io_service*
		__pIOService;

	asio::thread*
		__pNetThread;
};
