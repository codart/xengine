// AsioWrapper.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Connection.h"
#include "NetworkService.h"

int _tmain(int argc, _TCHAR* argv[])
{
	NetworkService networkService;

	Connection* pConnection1 = networkService.AddConnection(L"testConnection");
	pConnection1->Connect("127.0.0.1", "100");

	//asio::thread netThread(boost::bind(&asio::io_service::run, &ioService));
	//ioService.run();
	networkService.Start();

	BOOL bLoop = TRUE;
	while(bLoop)
	{
		Sleep(1);
		networkService.Tick();
	}

	pConnection1->Close();
	networkService.RemoveConnection(L"testConnection");
	networkService.End();

	//connection.Close();
	//netThread.join();

	return 0;
}

