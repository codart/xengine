#include "StdAfx.h"
#include "Connection.h"

#include "NetworkService.h"

Connection::Connection(NetworkService* pNetworkService, const String& strConnectionName)
: _socket(*pNetworkService->_getIOService())
, _endPointResolver(*pNetworkService->_getIOService())
, _strName(strConnectionName)
{
	_pNetworkService = pNetworkService;
	::InitializeCriticalSection(&_readMsgQueueCriticalSec);
}

Connection::~Connection(void)
{
}

void Connection::Connect(const std::string& strIP, const std::string& strPort)
{
	tcp::resolver::query query(strIP, strPort);
	tcp::resolver::iterator endPointIterator = _endPointResolver.resolve(query);
	
	_socket.async_connect(*endPointIterator
		, boost::bind(&Connection::_handleConnect, this, asio::placeholders::error));
}

void Connection::tick()
{
	::EnterCriticalSection(&_readMsgQueueCriticalSec);

	while(_readMsgQueue.size() > 0)
	{
		NetMessage& msg = _readMsgQueue.front();

		OnMessage(msg);

		_readMsgQueue.pop_front();
	}

	::LeaveCriticalSection(&_readMsgQueueCriticalSec);
}

void Connection::OnMessage(const NetMessage& msg)
{
	std::cout.write(msg.body(), msg.body_length());
	std::cout << "\n";
}

void Connection::Write(const NetMessage& msg)
{
	XASSERT(_pNetworkService);
	//..._ioService.post(boost::bind(&Connection::do_write, this, msg));
	_pNetworkService->Post(this, msg);
}

void Connection::Close()
{
	XASSERT(_pNetworkService);
	//..._ioService.post(boost::bind(&Connection::do_close, this));
	_pNetworkService->Close(this);
}

void Connection::do_write(NetMessage msg)
{
	bool bWriteInProgress = !_writeMsgQueue.empty();
	_writeMsgQueue.push_back(msg);
	if (!bWriteInProgress)
	{
		asio::async_write(_socket,
			asio::buffer(_writeMsgQueue.front().data(),
			_writeMsgQueue.front().length()),
			boost::bind(&Connection::_handleWrite, this,
			asio::placeholders::error));
	}
}

void Connection::_handleWrite(const asio::error_code& error)
{
	if (!error)
	{
		_writeMsgQueue.pop_front();
		if (!_writeMsgQueue.empty())
		{
			asio::async_write(_socket,
				asio::buffer(_writeMsgQueue.front().data(),
				_writeMsgQueue.front().length()),
				boost::bind(&Connection::_handleWrite, this,
				asio::placeholders::error));
		}
	}
	else
	{
		do_close();
	}
}

void Connection::_handleConnect(const asio::error_code& error)
{
	if (!error)
	{
		asio::async_read(_socket,
			asio::buffer(_readMsgBuf.data(), NetMessage::header_length),
			boost::bind(&Connection::_handleReadHeader, this,
			asio::placeholders::error));


/*
		_socket.async_read_some(asio::buffer(_readMsgBuf.data(), NetMessage::max_body_length),
								boost::bind(&Connection::_handler_readsome, this,
								asio::placeholders::error, asio::placeholders::bytes_transferred));
*/
	}
	else
	{
		assert(0);
	}
}

void Connection::_handler_readsome(
					   const asio::error_code& ec,
					   std::size_t bytes_transferred)
{
	std::cout.write(_readMsgBuf._pData, bytes_transferred);
	std::cout << "\n";

	_socket.async_read_some(asio::buffer(_readMsgBuf.data(), NetMessage::max_body_length),
		boost::bind(&Connection::_handler_readsome, this,
		asio::placeholders::error, asio::placeholders::bytes_transferred));

	return;
}

void Connection::_handleReadHeader(const asio::error_code& error)
{
	if (!error && _readMsgBuf.decode_header())
	{
		asio::async_read(_socket,
			asio::buffer(_readMsgBuf.body(), _readMsgBuf.body_length()),
			boost::bind(&Connection::_handleReadBody, this,
			asio::placeholders::error));
	}
	else
	{
		do_close();
	}
}

void Connection::_handleReadBody(const asio::error_code& error)
{
	::EnterCriticalSection(&_readMsgQueueCriticalSec);

	if (!error)
	{
		_readMsgQueue.push_back(_readMsgBuf);

		// try to read next message
		asio::async_read(_socket,
			asio::buffer(_readMsgBuf.data(), NetMessage::header_length),
			boost::bind(&Connection::_handleReadHeader, this,
			asio::placeholders::error));
	}
	else
	{
		do_close();
	}

	::LeaveCriticalSection(&_readMsgQueueCriticalSec);
}

void Connection::do_close()
{
	_socket.close();
}