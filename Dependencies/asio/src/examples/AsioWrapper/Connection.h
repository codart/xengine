#pragma once

#include "NetMessage.h"

class NetworkService;

class Connection
{
public:
	Connection(NetworkService* pNetworkService, const String& strConnectionName);
	~Connection(void);


public:
	void 
		Connect(const std::string& strIP, const std::string& strPort);
	void 
		Write(const NetMessage& msg);
	void 
		Close();
	void
		tick();


protected:
	void 
		_handleConnect(const asio::error_code& error);
	void 
		_handleReadHeader(const asio::error_code& error); 
	void 
		_handleReadBody(const asio::error_code& error);
	void 
		_handleWrite(const asio::error_code& error);

	void _handler_readsome(
		const asio::error_code& ec,
		std::size_t bytes_transferred);

	// ------
	virtual void 
		OnMessage(const NetMessage& msg);


public:
	void 
		do_write(NetMessage msg);
	void 
		do_close();


protected:
	String
		_strName;
	NetworkService*
		_pNetworkService;
	tcp::socket 
		_socket;

	tcp::resolver 
		_endPointResolver;

	NetMessage
		_readMsgBuf;
	NetMsgQueue 
		_writeMsgQueue;

	//----
	NetMsgQueue
		_readMsgQueue;

	CRITICAL_SECTION
		_readMsgQueueCriticalSec;

};
