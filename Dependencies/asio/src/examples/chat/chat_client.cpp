//
// chat_client.cpp
// ~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//



//
// time_t_timer.cpp
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
/*

#include "stdafx.h"
#include <iostream>
#include "boost/lambda/lambda.hpp"
#include "boost/function.hpp"
#include "iostream"

int TestFunc(int a)
{
	return a + 4;	
}

int main() {
	using namespace boost::lambda;

	//(std::cout << boost::lambda::_1 << " " << boost::lambda::_3 << " " << boost::lambda::_2 << "!\n")
	//	("Hello","friend","my");

	(std::cout << boost::lambda::_1)(10);
	int b = (1 << 2);
	//	(t1, t1, t2);

	return 1;

	boost::function<void(int,int,int)> f=
		std::cout << boost::lambda::_1 << "*" << boost::lambda::_2 << "+" << boost::lambda::_3
		<< "=" <<boost::lambda::_1*boost::lambda::_2+boost::lambda::_3 << "\n";

	f(1,2,3);
	f(3,2,1);
}
*/

/*
#include "stdafx.h"
#include <asio.hpp>
#include <ctime>
#include <iostream>

struct time_t_traits
{
	// The time type.
	typedef std::time_t time_type;

	// The duration type.
	struct duration_type
	{
		duration_type() : value(0) {}
		duration_type(std::time_t v) : value(v) {}
		std::time_t value;
	};

	// Get the current time.
	static time_type now()
	{
		return std::time(0);
	}

	// Add a duration to a time.
	static time_type add(const time_type& t, const duration_type& d)
	{
		return t + d.value;
	}

	// Subtract one time from another.
	static duration_type subtract(const time_type& t1, const time_type& t2)
	{
		return duration_type(t1 - t2);
	}

	// Test whether one time is less than another.
	static bool less_than(const time_type& t1, const time_type& t2)
	{
		return t1 < t2;
	}

	// Convert to POSIX duration type.
	static boost::posix_time::time_duration to_posix_duration(
		const duration_type& d)
	{
		return boost::posix_time::seconds(d.value);
	}
};

typedef asio::basic_deadline_timer<
std::time_t, time_t_traits> time_t_timer;

void handle_timeout(const asio::error_code&)
{
	std::cout << "handle_timeout\n";
}

int main()
{
	try
	{
		asio::io_service io_service;

		time_t_timer timer(io_service);

		timer.expires_from_now(5);
		std::cout << "Starting synchronous wait\n";
		timer.wait();
		std::cout << "Finished synchronous wait\n";

		timer.expires_from_now(5);
		std::cout << "Starting asynchronous wait\n";
		timer.async_wait(handle_timeout);
		io_service.run();
		std::cout << "Finished asynchronous wait\n";
	}
	catch (std::exception& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
	}

	return 0;
}

*/


#include "stdafx.h"
#include "chat_message.hpp"

using asio::ip::tcp;

typedef std::deque<chat_message> chat_message_queue;

class chat_client
{
public:
  chat_client(asio::io_service& io_service,
      tcp::resolver::iterator endpoint_iterator)
    : io_service_(io_service),
      socket_(io_service)
  {
    asio::async_connect(socket_, endpoint_iterator,
        boost::bind(&chat_client::handle_connect, this,
          asio::placeholders::error));
  }

  void write(const chat_message& msg)
  {
    io_service_.post(boost::bind(&chat_client::do_write, this, &msg));
  }

  void close()
  {
    io_service_.post(boost::bind(&chat_client::do_close, this));
  }

private:

  void handle_connect(const asio::error_code& error)
  {
    if (!error)
    {
      asio::async_read(socket_,
          asio::buffer(read_msg_.data(), chat_message::header_length),
          boost::bind(&chat_client::handle_read_header, this,
            asio::placeholders::error));
    }
  }

  void handle_read_header(const asio::error_code& error)
  {
    if (!error && read_msg_.decode_header())
    {
      asio::async_read(socket_,
          asio::buffer(read_msg_.body(), read_msg_.body_length()),
          boost::bind(&chat_client::handle_read_body, this,
            asio::placeholders::error));
    }
    else
    {
      do_close();
    }
  }

  void handle_read_body(const asio::error_code& error)
  {
    if (!error)
    {
      std::cout.write(read_msg_.body(), read_msg_.body_length());
      std::cout << "\n";
      asio::async_read(socket_,
          asio::buffer(read_msg_.data(), chat_message::header_length),
          boost::bind(&chat_client::handle_read_header, this,
            asio::placeholders::error));
    }
    else
    {
      do_close();
    }
  }

  void do_write(const chat_message* msg)
  {
    bool write_in_progress = !write_msgs_.empty();
    write_msgs_.push_back(*msg);
    if (!write_in_progress)
    {
      asio::async_write(socket_,
          asio::buffer(write_msgs_.front().data(),
            write_msgs_.front().length()),
          boost::bind(&chat_client::handle_write, this,
            asio::placeholders::error));
    }
  }

  void handle_write(const asio::error_code& error)
  {
    if (!error)
    {
      write_msgs_.pop_front();
      if (!write_msgs_.empty())
      {
        asio::async_write(socket_,
            asio::buffer(write_msgs_.front().data(),
              write_msgs_.front().length()),
            boost::bind(&chat_client::handle_write, this,
              asio::placeholders::error));
      }
    }
    else
    {
      do_close();
    }
  }

  void do_close()
  {
    socket_.close();
  }

private:
  asio::io_service& io_service_;
  tcp::socket socket_;
  chat_message read_msg_;
  chat_message_queue write_msgs_;
};

#define LINE_TO_STR_HELPER0(line) #line
#define LINE_TO_STR_HELPER1(_LINE_) LINE_TO_STR_HELPER0(_LINE_)
#define LINE_TO_STRING() LINE_TO_STR_HELPER1(__LINE__)

#define XALERT(msg) message(__FILE__ "(" LINE_TO_STRING() ") : alert: "msg)

int main(int argc, char* argv[])
{

#pragma XALERT("only a test program, never used in production!!!")

  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: chat_client <host> <port>\n";
      return 1;
    }

    asio::io_service io_service;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(argv[1], argv[2]);
    tcp::resolver::iterator iterator = resolver.resolve(query);

    chat_client chatClient(io_service, iterator);

    asio::thread netThread(boost::bind(&asio::io_service::run, &io_service));

    char line[chat_message::max_body_length + 1];
    while (std::cin.getline(line, chat_message::max_body_length + 1))
    {
      using namespace std; // For strlen and memcpy.
      chat_message msg;
      msg.body_length(strlen(line));
      memcpy(msg.body(), line, msg.body_length());
      msg.encode_header();
      chatClient.write(msg);
    }

    chatClient.close();
    netThread.join();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
