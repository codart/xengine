/**********************************************************************
 *<
	FILE: TestIGame1.h

	DESCRIPTION:	Includes for Plugins

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 2003, All Rights Reserved.
 **********************************************************************/

#ifndef __TestIGame1__H
#define __TestIGame1__H

#include "resource.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define TestIGame1_CLASS_ID	Class_ID(0x785a98e4, 0xc2e635b4)

class MaxExporter : public SceneExport 
{
public:
	MaxExporter();
	virtual ~MaxExporter();

public:
	int				ExtCount();					// Number of extensions supported
	const TCHAR *	Ext(int n);					// Extension #n (i.e. "3DS")
	const TCHAR *	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
	const TCHAR *	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
	const TCHAR *	AuthorName();				// ASCII Author name
	const TCHAR *	CopyrightMessage();			// ASCII Copyright message
	const TCHAR *	OtherMessage1();			// Other message #1
	const TCHAR *	OtherMessage2();			// Other message #2
	unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
	void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box

	BOOL	SupportsOptions(int ext, DWORD options);
	int		DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);
	void	ExportChildNodeInfo(IGameNode* Node,FILE* pfile);
	void	ExportMaterial(int maxcount,FILE* pfile);
	void	DumpProperty(IGameProperty * prop,FILE* pfile);
	//Constructor/Destructor


private:
	TiXmlDocument*
		_pXMLDocument;
	IGameScene * 
		_pIGame ;
	FILE* 
		_pBinaryFile;


protected:
	static HWND 
		s_hParams;

};



class TestIGame1ClassDesc : public ClassDesc2 {
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new MaxExporter(); }
	const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
	SClass_ID		SuperClassID() { return SCENE_EXPORT_CLASS_ID; }
	Class_ID		ClassID() { return TestIGame1_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("TestIGame1"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }					// returns owning module handle


};


#endif
