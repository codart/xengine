#pragma once

#include "Max.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

#include "igame/IGame.h"
#include "igame/IGameObject.h"
#include "igame/IGameProperty.h"
#include "igame/IGameControl.h"
#include "igame/IGameModifier.h"
#include "igame/IConversionManager.h"
#include "igame/IGameError.h"
#include "igame/IGameFX.h"

#include "tinyxml.h"