#include "stdafx.h"
#include "MaxExporter.h"
#include <string>

using namespace std;

static TestIGame1ClassDesc TestIGame1Desc;
ClassDesc2* GetTestIGame1Desc() { return &TestIGame1Desc; }

INT_PTR CALLBACK TestIGame1OptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam) {
	static MaxExporter *imp = NULL;

	switch(message) {
		case WM_INITDIALOG:
			imp = (MaxExporter *)lParam;
			CenterWindow(hWnd,GetParent(hWnd));
			return TRUE;

		case WM_CLOSE:
			EndDialog(hWnd, 0);
			return 1;
	}
	return 0;
}


//--- TestIGame1 -------------------------------------------------------
MaxExporter::MaxExporter()
{

}

MaxExporter::~MaxExporter() 
{
	
	
}

int MaxExporter::ExtCount()
{
	//TODO: Returns the number of file name extensions supported by the plug-in.
	return 1;
}

const TCHAR *MaxExporter::Ext(int n)
{		
	//TODO: Return the 'i-th' file name extension (i.e. "3DS").
	return _T("IGT");
}

const TCHAR *MaxExporter::LongDesc()
{
	//TODO: Return long ASCII description (i.e. "Targa 2.0 Image File")
	return _T("");
}
	
const TCHAR *MaxExporter::ShortDesc() 
{			
	//TODO: Return short ASCII description (i.e. "Targa")
	return _T("i game test");
}

const TCHAR *MaxExporter::AuthorName()
{			
	//TODO: Return ASCII Author name
	return _T("sssa2000");
}

const TCHAR *MaxExporter::CopyrightMessage() 
{	
	// Return ASCII Copyright message
	return _T("");
}

const TCHAR *MaxExporter::OtherMessage1() 
{		
	//TODO: Return Other message #1 if any
	return _T("");
}

const TCHAR *MaxExporter::OtherMessage2() 
{		
	//TODO: Return other message #2 in any
	return _T("");
}

unsigned int MaxExporter::Version()
{				
	//TODO: Return Version number * 100 (i.e. v3.01 = 301)
	return 100;
}

void MaxExporter::ShowAbout(HWND hWnd)
{			
	// Optional
}

BOOL MaxExporter::SupportsOptions(int ext, DWORD options)
{
	// TODO Decide which options to support.  Simply return
	// true for each option supported by each Extension 
	// the exporter supports.

	return TRUE;
}

int	MaxExporter::DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	string strXMLFileName = name;
	strXMLFileName += ".xml";
	_pXMLDocument = new TiXmlDocument(strXMLFileName.c_str());

	//_pXMLDocument->

	_pBinaryFile=fopen(name,"w");

	_pIGame = GetIGameInterface();
	IGameConversionManager * cm = GetConversionManager();
	cm->SetCoordSystem(IGameConversionManager::IGAME_D3D);
	_pIGame->InitialiseIGame(true); 
	_pIGame->SetStaticFrame(0);
	//////////////////////////////////////////////////////////////////////////
	fprintf(_pBinaryFile,"Test IGame Export\n");
	fprintf(_pBinaryFile,"****************************************************************\n");
	fprintf(_pBinaryFile,"\n");
	//////////////////////////////////////////////////////////////////////////
	int mat_num=_pIGame->GetRootMaterialCount();
	int node_num=_pIGame->GetTotalNodeCount();
	fprintf(_pBinaryFile,"Total Node Number:%d\n",node_num);
	fprintf(_pBinaryFile,"Total Material Number:%d\n",mat_num);
	ExportMaterial(mat_num,_pBinaryFile);
	//////////////////////////////////////////////////////////////////////////
	for(int i = 0; i <_pIGame->GetTopLevelNodeCount();i++)
	{
		IGameNode * pGameNode = _pIGame->GetTopLevelNode(i);
		if(pGameNode->IsTarget())
			continue;
		ExportChildNodeInfo(pGameNode,_pBinaryFile);
		fprintf(_pBinaryFile,"****************************************************************\n");
	}
	fprintf(_pBinaryFile,"File End\n");
	//////////////////////////////////////////////////////////////////////////
	_pIGame->ReleaseIGame();
	//////////////////////////////////////////////////////////////////////////
	//end 

	if(_pBinaryFile)
	{
		fclose(_pBinaryFile);
	}
	return TRUE;
}


void MaxExporter::ExportChildNodeInfo(IGameNode* Node,FILE* pfile)
{
	//name
	//tm
	//material
	//vertex
	//face
	fprintf(pfile,"Node Name:%s\n",Node->GetName());
	GMatrix matrix=Node->GetLocalTM(0);
	for(int i=0;i<4;++i)
	{
		fprintf(pfile,"Node TM Row%d is: %f,%f,%f,%f\n",i,matrix[i].x,matrix[i].y,matrix[i].z,matrix[i].w);
	}
	fprintf(_pBinaryFile,"  \n");

	if(Node->GetNodeMaterial())
	{
		fprintf(pfile, "Material Name:%s\n", Node->GetNodeMaterial()->GetMaterialName());
	}

	IGameObject * obj = Node->GetIGameObject();
	IGameObject::MaxType T = obj->GetMaxType();
	switch(obj->GetIGameType())
	{
		case IGameObject::IGAME_MESH:
			{
				IGameMesh * gM = (IGameMesh*)obj;
				gM->SetCreateOptimizedNormalList();
				if(gM->InitializeData()) //Extract the 3ds Max data into IGame data.
				{
					int numFaces = gM->GetNumberOfFaces();
					int numVert=gM->GetNumberOfVerts();
					int numNormal=gM->GetNumberOfNormals();
					//////////////////////////////////////////////////////////////////////////
					//vertex
					fprintf(pfile,"Total Vertex Number:%d\n",numVert);
					for(int i=0;i<numVert;++i)
					{
						Point3 ver;
						gM->GetVertex(i,ver);
						fprintf(pfile,"VertexID:%d :%f,%f,%f\n",i,ver.x,ver.y,ver.z);
						
					}
					fprintf(_pBinaryFile,"  \n");
					//////////////////////////////////////////////////////////////////////////
					//normal
					fprintf(pfile,"Total Normals:%d\n",numNormal);
					for(int k=0;k<numNormal;++k)
					{
						Point3 _normal;
						if(gM->GetNormal(k,_normal))
						{
							fprintf(pfile,"Normal %d: %f,%f,%f\n",k,_normal.x,_normal.y,_normal.z);

						}
					}

					fprintf(_pBinaryFile,"  \n");
					//////////////////////////////////////////////////////////////////////////
					//face
					fprintf(pfile,"Total Number of Face:%d\n",numFaces);
					for(int j=0;j<numFaces;++j)
					{
						FaceEx* f = gM->GetFace(j);
						fprintf(pfile,"Face Vert:%d,%d,%d\n",f->vert[0], f->vert[1], f->vert[2]);
						fprintf(pfile,"Face Normal:%d %d %d\n", f->norm[0], f->norm[1], f->norm[2]);
						fprintf(pfile,"Face Group:%u\n",(unsigned int)f->smGrp);
						fprintf(pfile,"Material ID:%d\n", f->matID);
						fprintf(pfile,"Edge Visible:%d,%d,%d\n",f->edgeVis[0],f->edgeVis[1],f->edgeVis[2]);	
						fprintf(_pBinaryFile,"  \n");
					}

					

				}
			}
		default:
			{
				return;
			}
			
	}


	
}

void MaxExporter::ExportMaterial(int maxcount,FILE* pfile)
{
	IGameProperty *prop;

	for(int j =0;j<maxcount;j++)
	{
		IGameMaterial * mat = _pIGame->GetRootMaterial(j);
		if(mat)
		{
			prop = mat->GetDiffuseData();
			DumpProperty(prop,pfile);

			prop = mat->GetAmbientData();
			DumpProperty(prop,pfile);

			prop = mat->GetSpecularData();
			DumpProperty(prop,pfile);

			prop = mat->GetOpacityData();
			DumpProperty(prop,pfile);

			prop = mat->GetGlossinessData();
			DumpProperty(prop,pfile);

			prop = mat->GetSpecularLevelData();
			DumpProperty(prop,pfile);
		}
		fprintf(_pBinaryFile,"  \n");
	}
}


void MaxExporter::DumpProperty(IGameProperty * prop,FILE* pfile)
{
	TSTR Buf;
	if(!prop)	//fix me NH...
		return;

	if(prop->GetType() == IGAME_POINT3_PROP)
	{
		Point3 p; 
		prop->GetPropertyValue(p);
		fprintf(pfile,"%s:%f,%f,%f\n",prop->GetName(),p.x,p.y,p.z);
	}
	else if( prop->GetType() == IGAME_FLOAT_PROP)
	{
		float f;
		prop->GetPropertyValue(f);
		fprintf(pfile,"%s:%f\n",prop->GetName(),f);
	}
	else if(prop->GetType()==IGAME_STRING_PROP)
	{
		TCHAR * b;
		prop->GetPropertyValue(b);
		fprintf(pfile,"%s:%s\n",prop->GetName(),b);
	}
	else
	{
		int i;
		prop->GetPropertyValue(i);
		fprintf(pfile,"%s:%d\n",prop->GetName(),i);

	}
	
}
