#pragma once

#include "x2d/render/IExtensionRender.h"

_X2DADV_NS_BEGIN

/*
	RenderToTexture, MicroTexture, Baking shadows, baking pre/post effect.
*/
class _X2DAdvExport BakingSystem : IExtensionRender
{
public:
	BakingSystem(void);
	virtual ~BakingSystem(void);

	virtual void
		Init(IStageRender* pStageRender) = 0;
	virtual void
		Destroy() = 0;
	virtual String
		GetRenderName() = 0;

};


_X2DADV_NS_END