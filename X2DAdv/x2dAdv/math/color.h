#pragma once

//#pragma pack(push)
//#pragma pack(2)

_X2DADV_NS_BEGIN

template<typename T>
class ColorT
{
public:
	ColorT(){}

	ColorT(T a, T r, T g, T b)
	{
		SetColor(a, r, g, b);
	}

	ColorT(const ColorT<T>& color)
	{
		a = color.a;
		r = color.r;
		g = color.g;
		b = color.b;
	}

	~ColorT(void){}

	INLINE void SetColor(T a, T r, T g, T b)
	{
		this->a = a;
		this->r = r;
		this->g = g;
		this->b = b;
	}
	
	ColorT<T> operator- (const ColorT<T>& color)
	{
		return ColorT<T>(a-color.a, r-color.r, g-color.g, b-color.b);
	}

	ColorT<T> operator+ (const ColorT<T>& color)
	{
		return ColorT<T>(a+color.a, r+color.r, g+color.g, b+color.b);
	}

	ColorT<T> operator/ (const ColorT<T>& color)
	{
		return ColorT<T>(a/color.a, r/color.r, g/color.g, b/color.b);
	}

	ColorT<T> operator/ (T value)
	{
		return ColorT<T>(a/value, r/value, g/value, b/value);
	}

	ColorT<T> operator* (T value)
	{
		return ColorT<T>(a*value, r*value, g*value, b*value);
	}


public:

#ifdef _WIN32
	// Note: Don't change the member order!!
	T	b;
	T	g;
	T	r;
	T	a;
#endif

};

class _X2DAdvExport Color : public ColorT<BYTE>
{
public:
	Color(){}
	Color(BYTE a, BYTE r, BYTE g, BYTE b) : ColorT<BYTE>(a, r, g, b) 
	{}

	INLINE DWORD ToDWORD()
	{
		return *(DWORD*)this;
	}

	INLINE void FromDWORD(DWORD dwColor)
	{
		*this = *(Color*)&dwColor;
	}

	template<typename T>
	Color& operator= (ColorT<T>& color)
	{
		this->a = color.a;
		this->r = color.r;
		this->g = color.g;
		this->b = color.b;
		return *this;
	}
};

class _X2DAdvExport ColorF : public ColorT<float>
{
public:
	ColorF(){}
	ColorF(float a, float r, float g, float b) : ColorT<float>(a, r, g, b) 
	{}

	INLINE DWORD ToDWORD()
	{
		Color color(BYTE(this->a), BYTE(this->r), BYTE(this->g), BYTE(this->b));
		return color.ToDWORD();
	}

	INLINE void FromDWORD(DWORD dwColor)
	{
		Color color;
		color.FromDWORD(dwColor);

		a = (float)color.a;
		r = (float)color.r;
		g = (float)color.g;
		b = (float)color.b;
	}

	template<typename T>
	ColorF& operator= (ColorT<T>& color)
	{
		this->a = color.a;
		this->r = color.r;
		this->g = color.g;
		this->b = color.b;
		return *this;
	}
};

_X2DADV_NS_END

//#pragma pack(pop)