// dllmain.cpp : Implementation of DllMain.
#include <stdlib.h>
#include <crtdbg.h>

#include "stdafx.h"

static x2dAdv::X2DAdvManager* __pSingletonInstance = NULL;

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	HRESULT hRes = 0;

	//_CrtSetBreakAlloc(139);

	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		__pSingletonInstance = new x2dAdv::X2DAdvManager;
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		delete __pSingletonInstance;
		//_Module.Term();
		_CrtDumpMemoryLeaks();
		break;
	}
	return TRUE;
}

extern "C"
{
_X2DADV_NS_BEGIN
	
	_X2DAdvExport X2DAdvManager* GetX2DAdvManager()
	{
		XASSERT(__pSingletonInstance);
		return __pSingletonInstance;
	}

_X2DADV_NS_END
}
