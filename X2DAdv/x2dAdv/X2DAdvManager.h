#pragma once

#include "d3d9.h"
#include "x2dadv/render/GraphicInterface.h"

_X2DADV_NS_BEGIN


class RenderWindow : public Object
					, public ReferenceCounted
{
public:

protected:
	RefCountPtr<WindowRenderTarget>
		_pWindowRenderTarget;

private:
};

typedef map<String, RefCountPtr<RenderWindow>> RenderWindowMap;
typedef RenderWindowMap::iterator RenderWindowMapIt;


/** Abstract class representing a Texture that can be used as render target.
    @remarks
 */
class X2DAdvManager
{
public:
	X2DAdvManager();
	virtual ~X2DAdvManager();


public:
	virtual void 
		Init();
	virtual void
		Uninit();

	virtual GraphicSystem*
		GetGraphicInterface();

	virtual RenderWindow*
		AddRenderWindow(const String& strWindowName);


protected:
	GraphicSystem*
		_GraphicInterface;

	RenderWindowMap
		_renderWindowMap;

};

_X2DADV_NS_END