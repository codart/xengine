#pragma once
#include "common/core/event.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport DeviceEvent : public Event
{
public:
	enum DeviceEventType
	{
		// Note: Only _D3DDevice will dispatch DEVICE_LOST/RESET event
		DEVICE_LOST = 1100,
		DEVICE_RESET,

		DEVICE_DESTROYED,
	};

public:
	DeviceEvent(int nType) : Event(nType){}
	virtual ~DeviceEvent(void){};
};


_X2DADV_NS_END