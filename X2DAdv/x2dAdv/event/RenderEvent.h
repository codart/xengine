#pragma once
#include "common/core/event.h"

_X2DADV_NS_BEGIN

class RenderEvent : public Event
{
public:
	enum RenderEventType
	{
		RENDER_EVENT_BASE = 600,

		PRE_RENDER,
		RENDERING,	// Rendering event will be dispatched at least once
		POST_RENDER,
	};

	RenderEvent(int nType):Event(nType)
	{

	}
};

_X2DADV_NS_END