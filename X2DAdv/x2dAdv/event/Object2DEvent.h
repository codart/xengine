#pragma once
#include "common/core/event.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport Object2DEvent : public Event
{
public:
	enum Object2DEventType
	{
		HIT_LEFT_BOUND = 400,
		HIT_RIGHT_BOUND,
		HIT_TOP_BOUND,
		HIT_BOTTOM_BOUND,
	};

public:
	Object2DEvent(int nType) : Event(nType){}
	virtual ~Object2DEvent(void){};
};


_X2DADV_NS_END