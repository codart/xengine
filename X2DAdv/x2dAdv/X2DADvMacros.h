#pragma once

#if defined( XGAME2D_EXPORTS )
#define _X2DAdvExport __declspec(dllexport)
#else
#define _X2DAdvExport __declspec(dllimport)
#endif

#define _X2DADV_NS_BEGIN namespace x2dAdv {
#define _X2DADV_NS_END		}

