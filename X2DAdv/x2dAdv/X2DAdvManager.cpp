#include "stdafx.h"
#include "X2DAdvManager.h"

#include "x2dAdv/render/DXRenderAdv.h"
#include "x2d/display/Bitmap.h"
#include "common/file/ModuleUtil.h"
#include "x2d/Library.h"

// Resource managers
#include "x2dAdv/resource/TextureManager.h"

_X2DADV_NS_BEGIN



X2DAdvManager::X2DAdvManager()
{
	_GraphicInterface = NULL;
}

X2DAdvManager::~X2DAdvManager()
{
	
}

void X2DAdvManager::Init()
{
	DXRenderAdv::_$InitRenderEngine();

	_GraphicInterface = new GraphicSystem;
	_GraphicInterface->internalInit(DXRenderAdv::_$GetD3DDevice());

#ifdef _DEBUG
	RefCountPtr<Bitmap> obj2DHotRect = new Bitmap;
	obj2DHotRect->LoadBitmap(TEXTURE_PATH + L"hotRect.png");
	Library::Instance().AddToLibrary(obj2DHotRect, L"HotRectIndicator", FALSE);
#endif // _DEBUG

	TextureManager::Instance().Init();

}

void X2DAdvManager::Uninit()
{
	TextureManager::Instance().UnInit();

	_GraphicInterface->internalUninit();
	DXRenderAdv::_$UninitRenderEngine();
}

GraphicSystem* X2DAdvManager::GetGraphicInterface()
{
	XASSERT(_GraphicInterface);
	return _GraphicInterface;
}

RenderWindow* X2DAdvManager::AddRenderWindow(const String& strWindowName)
{
	//RenderWindow* pRenderWindow = _GraphicInterface->CreateRenderWindow();
	
	return NULL;
}

_X2DADV_NS_END