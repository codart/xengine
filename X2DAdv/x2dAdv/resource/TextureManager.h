#pragma once

#include "d3d9.h"
#include "x2dAdv/render/Texture.h"

_X2DADV_NS_BEGIN

typedef map<String, RefCountPtr<Texture>> TextureMap;
typedef TextureMap::iterator TextureMapIt;

/** Abstract class representing a Texture that can be used as render target.
    @remarks
 */
class _X2DAdvExport TextureManager : public Singleton<TextureManager, AutoRelease>
{
public:
	TextureManager();
	virtual ~TextureManager();


public:
	virtual void
		Init();
	virtual void
		UnInit();

	virtual Texture*
		LoadTexture();

	virtual Texture*
		CreateTexture(const String& strTextureName);
	virtual RenderTexture*
		CreateRenderTexture(const String& strTextureName, int nWidth, int nHeight);

	virtual void
		DestroyTexture(const String& strTexture);


protected:
	GraphicSystem*
		_pGraphicInterface;

	TextureMap
		_textureMap;

};

_X2DADV_NS_END