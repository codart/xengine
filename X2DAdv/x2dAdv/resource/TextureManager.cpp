#include "stdafx.h"
#include "TextureManager.h"

_X2DADV_NS_BEGIN


TextureManager::TextureManager()
{
	_pGraphicInterface = NULL;

}

TextureManager::~TextureManager()
{
	
}


void TextureManager::Init()
{
	X2DAdvManager* pX2DAdvMgr = GetX2DAdvManager();
	_pGraphicInterface = pX2DAdvMgr->GetGraphicInterface();
}

void TextureManager::UnInit()
{

}

Texture* TextureManager::LoadTexture()
{
	return NULL;
}

Texture* TextureManager::CreateTexture(const String& strTextureName)
{
	Texture* pTexture = _pGraphicInterface->CreateNormalTexture(strTextureName);
	pTexture->_strName = strTextureName;

	_textureMap[strTextureName] = pTexture;

	return pTexture;
}

RenderTexture* TextureManager::CreateRenderTexture(const String& strTextureName, int nWidth, int nHeight)
{
	RenderTexture* pRenderTexture = _pGraphicInterface->CreateRenderTexture(nWidth, nHeight);
	pRenderTexture->_strName = strTextureName;

	_textureMap[strTextureName] = pRenderTexture;
	return pRenderTexture;
}

void TextureManager::DestroyTexture(const String& strTexture)
{
	TextureMapIt it = _textureMap.find(strTexture);
	if(it == _textureMap.end()) return;

	_textureMap.erase(it);
}

_X2DADV_NS_END