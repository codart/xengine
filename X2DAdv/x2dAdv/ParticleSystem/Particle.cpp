#include "StdAfx.h"
#include "Particle.h"

_X2DADV_NS_BEGIN

Particle::Particle(void)
{
	bInitted = FALSE;
}

Particle::~Particle(void)
{
}

void Particle::InitFromTemplate(Particle* pTemplate)
{
	if(bInitted) return;

	pTemplate->DisplayObject::_CopyData(this);

	// Copy basic data
	objType = pTemplate->objType;
	__SetWidth(pTemplate->width);
	__SetHeight(pTemplate->height);
	SetZOffset(pTemplate->zOffset);

	_bManagedRenderData = FALSE;
	blendMode = pTemplate->blendMode;
	SetAlpha(pTemplate->GetAlpha());

	_bitmapArray = pTemplate->_bitmapArray;

	_goFirstActionFirstFrame();

	bInitted = TRUE;
}

_X2DADV_NS_END