#pragma once

#include "stdafx.h"
#include "EmitArea.h"
#include "common/math/Math.h"

_X2DADV_NS_BEGIN

//////////////////////////////////////////////////////////////////////////
// LineEmitArea
LineEmitArea::LineEmitArea()
{
	_ptStart.Reset();
	_ptEnd.Reset();
	__bUseVerticle = FALSE;

	__k = 0;
	__b = 0;
}

void LineEmitArea::SetStartPoint(PointF point)
{
	_ptStart = point;

	__updateParams();
}

void LineEmitArea::SetEndPoint(PointF point)
{
	_ptEnd = point;

	__updateParams();
}

PointF LineEmitArea::GetRandomPoint()
{
	if(__bUseVerticle)
	{
		float fY = Math::Rand(_ptStart.y, _ptEnd.y);
		float fX = __b + __k * fY;
		return PointF(fX, fY);

	}
	else
	{
		float fX = Math::Rand(_ptStart.x, _ptEnd.x);
		float fY = __b + __k * fX;
		return PointF(fX, fY);

	}
}

void LineEmitArea::__updateParams()
{
	if(abs(_ptStart.x - _ptEnd.x) > abs(_ptStart.y - _ptEnd.y))
	{
		__k = (_ptEnd.y - _ptStart.y)/(_ptEnd.x - _ptStart.x);
		__b = _ptEnd.y -__k * _ptEnd.x;
		__bUseVerticle = FALSE;
	}
	else
	{
		__k = (_ptEnd.x - _ptStart.x)/(_ptEnd.y - _ptStart.y);
		__b = _ptEnd.x -__k * _ptEnd.y;
		__bUseVerticle = TRUE;
	}
}
//////////////////////////////////////////////////////////////////////////

/*
PointF SquareEmitArea::GetRandomPoint()
{
	return PointF(Random::Rand(_ptTopLeft.x, _ptTopLeft.x)
		, Random::Rand(_ptBottomRight.y, _ptBottomRight.y));
}
*/


//////////////////////////////////////////////////////////////////////////
// 
PointF CircleEmitArea::GetRandomPoint()
{
	return PointF(0, 0);
}

//////////////////////////////////////////////////////////////////////////
// RingArea
RingEmitArea::RingEmitArea()
{
	_fRadius = 100;
}

RingEmitArea::RingEmitArea(float fRadius)
{
	_fRadius = fRadius;
}

void RingEmitArea::SetRadius(float fRadius)
{
	_fRadius = fRadius;
}

PointF RingEmitArea::GetRandomPoint()
{
	PointF pt;
	float fAngle = Math::Rand(0.0f, Math::PI/2);

	switch ((++__iRange - (__iRange >> 2 << 2)))
	{
	case 0:
		pt.x = _fRadius * Math::LowCos(fAngle);
		pt.y = _fRadius * Math::LowSin(fAngle);
		break;

	case 1:
		pt.x = -_fRadius * Math::LowCos(fAngle);
		pt.y = _fRadius * Math::LowSin(fAngle);
		break;

	case 2:
		pt.x = _fRadius * Math::LowCos(fAngle);
		pt.y = -_fRadius * Math::LowSin(fAngle);
		break;

	case 3:
		pt.x = -_fRadius * Math::LowCos(fAngle);
		pt.y = -_fRadius * Math::LowSin(fAngle);
		break;

	default:
		XASSERT(0);
	}
	return pt;
}

_X2DADV_NS_END