#pragma once

#include "x2dAdv/render/RenderDataAdv.h"
#include "x2d/render/DXRenderBase.h"
#include "common/algorithm/ObjectPool.h"
#include "x2dAdv/ParticleSystem/Particle.h"

_X2DADV_NS_BEGIN

#define ParticleContainerType list

typedef ParticleContainerType<Particle*>::iterator ListParticleIt;

/*
	Ideal: there are two methods to update particles into vertex buffer
		1. Store particles in a list. Traverse all list members and update the required particle into buffer.
		2. Store particles in a vector(linear memory), use memcpy to copy all list into buffer. All the Vertex
		has a flag which will be used by shader to determine whether need render the particle. 
*/
class ParticleArray : public RenderData
					, public ParticleContainerType<Particle*>
{
public:
	static const int MAX_PARTICLES = 4000;

public:
	ParticleArray(void);
	~ParticleArray(void);

public:
	BOOL 
		LoadParticle(LPCWSTR pszTextureFile
						, int nActionCount = 1, int nBmpsPerAction = 1
						, COLORREF _keyColor = NO_KEY_COLOR, BMPS_ALIGN bmpAlign = H_BMPS);

	void 
		Dispose();

	Particle*
		AddParticle();
	void 
		RemoveParticle(ListParticleIt& it);
	void 
		Reserve(int nReservedCount);

	INLINE float 
		GetParticleWidth()
	{
		return _pParticleTemplate->width;
	}

	INLINE float 
		GetParticleHeight()
	{
		return _pParticleTemplate->height;
	}


public:
	// Implement RenderData's interface
	override void
		Swap(RenderData* pRenderData){XTHROW(NoImplementationException);};


public:
	RefCountPtr<Particle>
		_pParticleTemplate;

protected:

	ObjectPool<Particle*>
		_particlePool;
};


_X2DADV_NS_END