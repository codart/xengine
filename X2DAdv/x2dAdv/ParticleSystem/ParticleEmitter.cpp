#include "StdAfx.h"
#include "ParticleEmitter.h"

#include "x2d/error/LoadResFailedException.h"
#include "x2d/event/FrameEvent.h"

_X2DADV_NS_BEGIN

ParticleEmitter::ParticleEmitter(void) : InteractiveObject(this)
{
	_pEmitArea = NULL;

	status = EMMITER_STOPPED;

	_blendMode = BLEND_ALPHA;
	_nEmissionRate = 150;
	_fTimeToAlive = DEFAULT_TIME_TO_ALIVE;
	
	_fMinSpeed = 0.2f;
	_fMaxSpeed = 1.0f;

	_vGravity = Vector3(0, 0, 0);

	_direction.x = 0;
	_direction.y = -1;
	_direction.z = 0;

	width = 0.0f;
	height = 0.0f;

	_fSpreadHalfAngle = 0;
	_fBaseAngle = 0;

	_bUniformScale = TRUE;

	_vInitMinScale.x = 1;
	_vInitMinScale.y = 1;

	_vInitMaxScale.x = 1;
	_vInitMaxScale.y = 1;

	_vFinalMinScale.x = 1;
	_vFinalMinScale.y = 1;

	_vFinalMaxScale.x = 1;
	_vFinalMaxScale.y = 1;

	_fMinRotateSpeed = 0;
	_fMaxRotateSpeed = 0;
	_fRotateVariation = 0;
	_fInitRotation = 0;

	_initColor.FromDWORD(DEFAULT_INIT_COLOR);
	_finalColor.FromDWORD(DEFAULT_FINAL_COLOR);

	_bRandomAction = _bRandomFrame = TRUE;
	_iActionIndex = _iFrameIndex = 0;

	__fNewParticleCount = 0.0f;
}

void ParticleEmitter::SetActionIndex(int iActionIndex)
{
	_iActionIndex = iActionIndex;
}

void ParticleEmitter::SetFrameIndex(int iFrameIndex)
{
	_iFrameIndex = iFrameIndex;
}

ParticleEmitter::~ParticleEmitter(void)
{
}

void ParticleEmitter::SetSpreadAngle(float fSpreadAngle)
{
	_fSpreadHalfAngle = fSpreadAngle;
}

void ParticleEmitter::SetBlendMode(BLEND_MODE blendMode)
{
	_blendMode = blendMode;
}

void ParticleEmitter::SetTimeToLive(float fTimeToLive)
{
	_fTimeToAlive = fTimeToLive;
}

void ParticleEmitter::SetDirection(const Vector3& direction)
{
	if(direction.x == 0)
	{
		if(direction.y >0) _fBaseAngle = 0;
		else if(direction.y <= 0) _fBaseAngle = Math::PI;
	}
	else if(direction.y == 0)
	{
		if(direction.x > 0) _fBaseAngle = Math::PI/2;
		else if(direction.x <= 0) _fBaseAngle = - Math::PI/2;
	}
	else
	{
		_fBaseAngle = atan(direction.y/direction.x);
		if(direction.y<0)
		{
			_fBaseAngle = Math::PI +_fBaseAngle;
		}
	}
}

void ParticleEmitter::SetInitScaleRange(Vector2 fMinInitScale, Vector2 fMaxInitScale)
{
	_vInitMinScale = fMinInitScale;
	_vInitMaxScale = fMaxInitScale;
}

void ParticleEmitter::SetFinalScaleRange(Vector2 fFinalMinScale, Vector2 fFinalMaxScale)
{
	_vFinalMinScale = fFinalMinScale;
	_vFinalMaxScale = fFinalMaxScale;
}

void ParticleEmitter::SetDirection(float fAngle)
{
	_fBaseAngle = fAngle;
}

void ParticleEmitter::SetEmitterArea(EmitArea* pArea)
{
	_pEmitArea = pArea;
}

void ParticleEmitter::SetCenterPosition(float x, float y)
{
	Vector2 vAverageScale = (_vInitMinScale + _vInitMaxScale)/2;

	float centerX = x - _aliveParticleList->GetParticleWidth() * vAverageScale.x/2;
	float centerY = y - _aliveParticleList->GetParticleHeight() * vAverageScale.y/2;

	//SetPosition(centerX, centerY);

	SetPosition(x, y);
}

float ParticleEmitter::GetEmitterWidth()
{
	return width;
}

float ParticleEmitter::GetEmitterHeight()
{
	return height;
}

void ParticleEmitter::SetInitColor(ColorF initColor)
{
	_initColor = initColor;
}

void ParticleEmitter::SetFinalColor(ColorF finalColor)
{
	_finalColor = finalColor;
}

int ParticleEmitter::LoadParticle(const String& strTextureFileName, int nActionCount, int nBmpsPerAction, 
								  COLORREF _keyColor, BMPS_ALIGN bmpAlign)
{
	if(!_aliveParticleList) _aliveParticleList = new ParticleArray;

	if(!_aliveParticleList->LoadParticle(strTextureFileName.c_str(), nActionCount
										, nBmpsPerAction, _keyColor, bmpAlign))
	{
		XTHROW(LoadResFailedException);
	}

	pRenderData = _aliveParticleList;
	return 0;
}

void ParticleEmitter::Reserve(int nCount)
{
	if(!_aliveParticleList)
	{
		XTHROWEX("Cannot reserve particles if the _pParticleTemplate is NULL. Use ParticleArray::LoadParticle to init the _pParticleTemplate");
	}

	_aliveParticleList->Reserve(nCount);
	__deadParticleList.reserve(nCount/2);
}

float ParticleEmitter::GetParticleWidth()
{
	XASSERT(_aliveParticleList);
	return _aliveParticleList->GetParticleWidth();
}

float ParticleEmitter::GetParticleHeight()
{
	XASSERT(_aliveParticleList);
	return _aliveParticleList->GetParticleHeight();
}

void ParticleEmitter::Fire(BOOL bAutoUpdate)
{
	if(status == EMMITER_SPRAYING) return;

	status = EMMITER_SPRAYING;
	__fNewParticleCount = 0.0f;

	if(bAutoUpdate)
	{
		AddEventListenerEx(FrameEvent::FRAME_START
						, MFUNC(&ParticleEmitter::_onEnterFrame, this));
	}

}

void ParticleEmitter::Stop()
{
	status = EMMITER_STOPPED;
}

int ParticleEmitter::GetAliveParticleCount()
{
	return _aliveParticleList->size();
}

void ParticleEmitter::_onEnterFrame(const Event* pEvent)
{
	Update();
}

void ParticleEmitter::Update()
{
	switch (status)
	{
	case EMMITER_SPRAYING:
		_sprayParticle();
		break;
	}
}

void ParticleEmitter::SetEmissionRate(int fEmissionRate)
{
	_nEmissionRate = fEmissionRate;
}

void ParticleEmitter::SetSpeedRange(float fMinSpeed, float fMaxSpeed)
{
	_fMinSpeed = fMinSpeed;
	_fMaxSpeed = fMaxSpeed;
}

void ParticleEmitter::SetGravity(const Vector3& vGravity)
{
	_vGravity = vGravity;
}

void ParticleEmitter::SetRotateSpeedRange(float fMinRotateSpeed, float fMaxRotateSpeed)
{
	_fMinRotateSpeed = fMinRotateSpeed;
	_fMaxRotateSpeed = fMaxRotateSpeed;
}

void ParticleEmitter::SetRotateVariation(float fRotateVariation)
{
	_fRotateVariation = fRotateVariation;
}

void ParticleEmitter::_sprayParticle()
{
	_updateAliveParticles();

	_removeDeadParticles();

	_emitNewParticle();
}

void ParticleEmitter::_updateAliveParticles()
{
	float fDeltaTime = GetDeltaTime();

	ListParticleIt it = _aliveParticleList->begin();
	for ( ; it != _aliveParticleList->end(); ++it)
	{
		Particle* pParticle = *it;

		// Alpha and color
		pParticle->currentColor = pParticle->currentColor + pParticle->deltaColor * fDeltaTime;
		//???pParticle->currentColor = pParticle->currentColor;
		pParticle->SetARGB(pParticle->currentColor.ToDWORD());

		// Scale
		float fDeltaScaleX = pParticle->vDeltaScale.x * fDeltaTime;
		float fDeltaScaleY = pParticle->vDeltaScale.y * fDeltaTime;

		pParticle->SetScale(pParticle->GetScaleX() + fDeltaScaleX
							, pParticle->GetScaleY() + fDeltaScaleY);

		// Position
		pParticle->vSpeed += fDeltaTime * pParticle->vGravity;
		pParticle->SetAbsolutePos(pParticle->GetAbsoluteX() + pParticle->vSpeed.x * fDeltaTime
								, pParticle->GetAbsoluteY() + pParticle->vSpeed.y * fDeltaTime);

		// Rotation
		float fRotateSpeed = Math::Rand(pParticle->fRotateSpeed - _fRotateVariation
										, pParticle->fRotateSpeed + _fRotateVariation);
		pParticle->SetRotation(pParticle->GetRotation() + fRotateSpeed * fDeltaTime);

		// Alive test
		pParticle->timeToLive -= fDeltaTime;

		if(pParticle->timeToLive <= 0)
		{
			__deadParticleList.push_back(it);
		}
	}

}

void ParticleEmitter::_removeDeadParticles()
{
	// Remove dead particles
	VecListParticleIt deadIt = __deadParticleList.begin();
	for ( ; deadIt != __deadParticleList.end(); ++deadIt)
	{
		_aliveParticleList->RemoveParticle(*deadIt);
	}
	__deadParticleList.clear();
}

void ParticleEmitter::_emitNewParticle()
{
	float fDeltaCount = GetDeltaTime() * _nEmissionRate;
	__fNewParticleCount += fDeltaCount;

	if(__fNewParticleCount < 1.0f) return;

	
	Math::SeedByTime();
	for (int i = 0; i<int(__fNewParticleCount); i++)
	{
		if(_aliveParticleList->size() > 1000) break;

		Particle* pParticle = _aliveParticleList->AddParticle();

		// Set particle init state
		pParticle->blendMode = _blendMode;
		pParticle->SetAlpha(255);
		
		float fTimeToAlive = Math::Rand(_fTimeToAlive - 0.5f, _fTimeToAlive + 0.5f);
		pParticle->timeToLive = fTimeToAlive;
		pParticle->totalTimeToLive = fTimeToAlive;
		pParticle->vSpeed.z = 0.0f;

		int iActionIndex;
		if(_bRandomAction)
		{
			iActionIndex = Math::Rand(0, pParticle->GetActionCount() - 1);
			pParticle->SetActionIndex(iActionIndex);
		}
		else
		{
			pParticle->SetActionIndex(_iActionIndex);
		}

		int iFrameIndex;
		if(_bRandomFrame)
		{
			iFrameIndex = Math::Rand(0, pParticle->GetFrameCount(iActionIndex) - 1);
			pParticle->GotoFrame(iFrameIndex);
		}
		else
		{
			pParticle->GotoFrame(_iFrameIndex);
		}

		float fAngle = Math::Rand(_fBaseAngle - _fSpreadHalfAngle, _fBaseAngle + _fSpreadHalfAngle);
		Vector3 vAngle;
		vAngle.z = 0;
		vAngle.x = sin(fAngle);
		vAngle.y = cos(fAngle);
		pParticle->vSpeed = vAngle;
		pParticle->vSpeed.Normalise();		

		float fSpeed = Math::Rand(_fMinSpeed, _fMaxSpeed);
		pParticle->vSpeed *= fSpeed;
		//pParticle->vDeltaSpeed = pParticle->vSpeed / -2;
		pParticle->vGravity = _vGravity;

		// Scale variation
		if(_bUniformScale)
		{
			float fInitScale = Math::Rand(_vInitMinScale.x, _vInitMaxScale.x);
			pParticle->SetScale(fInitScale, fInitScale);

			float fFinalScale = Math::Rand(_vFinalMinScale.x, _vFinalMaxScale.x);

			float fDeltaScale = (fFinalScale - fInitScale)/fTimeToAlive;
			pParticle->vDeltaScale = Vector2(fDeltaScale, fDeltaScale);
		}
		else
		{
			XASSERT(0);
			//... Implement later
			pParticle->SetScaleX(Math::Rand(_vInitMinScale.x, _vInitMaxScale.x));
			pParticle->SetScaleY(Math::Rand(_vInitMinScale.y, _vInitMaxScale.y));

			pParticle->vDeltaScale = Vector2(Math::Rand(_vFinalMinScale.x, _vFinalMaxScale.x)
											, Math::Rand(_vFinalMinScale.y, _vFinalMaxScale.y));
		}

		// Color
		pParticle->SetARGB(_initColor.ToDWORD());
		pParticle->currentColor = _initColor;
		pParticle->deltaColor = (_finalColor - _initColor)/fTimeToAlive;

		// Position
		PointF spawnPoint = _pEmitArea? _pEmitArea->GetRandomPoint() : PointF(0, 0);

		//pParticle->SetPosition(fX, fY);
		pParticle->SetAbsolutePos(__GetX() + spawnPoint.x - _aliveParticleList->GetParticleWidth()/2
								, __GetY() + spawnPoint.y - _aliveParticleList->GetParticleHeight()/2);

		// Rotation 
		pParticle->fRotateSpeed = Math::Rand(_fMinRotateSpeed, _fMaxRotateSpeed);
		pParticle->SetRotation(_fInitRotation);

	}

	__fNewParticleCount = 0.0f;
}


_X2DADV_NS_END