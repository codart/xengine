#include "StdAfx.h"
#include "ParticleArray.h"

_X2DADV_NS_BEGIN

ParticleArray::ParticleArray(void)
{
	//reserve(MAX_PARTICLES);
	//_$pTexture = NULL;
	renderType = RENDER_PARTICLE_SYS;
}

ParticleArray::~ParticleArray(void)
{
	ListParticleIt it = begin();
	for ( ; it != end(); ++it)
	{
		_particlePool.FreeInstance(*it);
	}

}

BOOL ParticleArray::LoadParticle(LPCWSTR pszTextureFile
								 , int nActionCount, int nBmpsPerAction, 
								 COLORREF _keyColor,  BMPS_ALIGN bmpAlign)
{
	Dispose();

	_pParticleTemplate = new Particle;
	_pParticleTemplate->LoadActions(pszTextureFile, nActionCount, nBmpsPerAction, _keyColor, bmpAlign);
	_pParticleTemplate->blendMode = BLEND_ALPHA;
	_pParticleTemplate->SetHotX(_pParticleTemplate->width/2);
	_pParticleTemplate->SetHotY(_pParticleTemplate->height/2);

	return TRUE;
}

void ParticleArray::Reserve(int nReservedCount)
{
	XASSERT(_pParticleTemplate);

	int nNewToSpawn = nReservedCount - _particlePool.GetPoolSize();
	if(nNewToSpawn <= 0) return;

	vector<Particle*> vReservedParticles;
	for (int i=0; i<nNewToSpawn; ++i)
	{
		Particle* pNewParticle = _particlePool.GetInstance();
		pNewParticle->InitFromTemplate(_pParticleTemplate);
		vReservedParticles.push_back(pNewParticle);
	}

	for (int i=0; i<nNewToSpawn; ++i)
	{
		_particlePool.FreeInstance(vReservedParticles[i]);
	}
}

// Use splice to add particle
Particle* ParticleArray::AddParticle()
{
	Particle* pNewParticle = _particlePool.GetInstance();
	//Particle* pNewParticle = _particlePool.GetInstance();

	if(!pNewParticle->bInitted)
	{
		pNewParticle->InitFromTemplate(_pParticleTemplate);
	}

	push_back(pNewParticle);
	return pNewParticle;
}

void ParticleArray::RemoveParticle(ListParticleIt& it)
{
	_particlePool.FreeInstance(*it);
	//_particlePool.FreeInstance(*it);
	erase(it);
}

void ParticleArray::Dispose()
{
	clear();
}


_X2DADV_NS_END