#include "StdAfx.h"
#include "ParticleSystem.h"

#include "x2d/event/FrameEvent.h"

_X2DADV_NS_BEGIN

ParticleSystem::ParticleSystem(void) : DisplayObjectContainer(this)
{


}

ParticleSystem::~ParticleSystem(void)
{

}

int ParticleSystem::AddEmitter(ParticleEmitter* pEmitter)
{
	DisplayObjectContainer::AddChild(pEmitter);
	pEmitter->SetCenterPosition(0, 0);
	return 0;
}

_X2DADV_NS_END