#pragma once

#include "x2dAdv\ParticleSystem\ParticleEmitter.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport ParticleSystem : public DisplayObject
									, public DisplayObjectContainer
{
	
public:
	ParticleSystem(void);
	virtual ~ParticleSystem(void);

public:
	virtual int 
		AddEmitter(ParticleEmitter* pEmitter);

};


_X2DADV_NS_END