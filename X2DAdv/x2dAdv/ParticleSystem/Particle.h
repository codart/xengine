#pragma once

#include "d3d9types.h"
#include "d3dx9math.h"
#include "common/math/Vector2.h"
#include "common/math/Vector3.h"
#include "x2dAdv/math/color.h"

_X2DADV_NS_BEGIN

struct ParticleVertex
{
	enum FVF
	{
		FVF_PARTICLE_VERTEX = D3DFVF_XYZ|D3DFVF_PSIZE|D3DFVF_DIFFUSE
	};

	D3DVECTOR   positon;
	float		size;
	D3DCOLOR	color;
};


/*
	Particles not use a relative position. So must use SetAbsolutePos and GetAbsolutePos!!!
*/
class Particle : public Sprite
{
public:
	Particle(void);
	~Particle(void);

public:
	void InitFromTemplate(Particle* pTemplate);

	void SetAbsolutePos(float x, float y)
	{
		__x = x;
		__y = y;
	}

	float GetAbsoluteX()
	{
		return __x;
	}

	float GetAbsoluteY()
	{
		return __y;
	}

public:
	BOOL		bInitted;

	float		fRadialAccel;
	float		fTangentialAccel;

	float		fSize;
	float		fSizeDelta;

	D3DCOLOR	colColor;		// + alpha
	D3DCOLOR	colColorDelta;

	/// Time to live, number of seconds left of particles natural life
	float		timeToLive;
	/// Total Time to live, number of seconds of particles natural life
	float		totalTimeToLive;

	Vector3		vSpeed;
	// Delta speed is much like the gravity.
	Vector3		vGravity;

	Vector2		vDeltaScale;

	ColorF		currentColor;
	ColorF		deltaColor;

	// Angle rotate per-second
	float		fRotateSpeed; 
};


_X2DADV_NS_END