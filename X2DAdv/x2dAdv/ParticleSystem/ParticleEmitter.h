#pragma once

#include "x2d\display\displayobjectcontainer.h"
#include "x2dAdv\ParticleSystem\ParticleArray.h"
#include "common\math\Point.h"
#include "x2dAdv\ParticleSystem\EmitArea.h"


_X2DADV_NS_BEGIN

/*
	*. When the emitter is moving, the particles should be affected???? The more younger, the more be affected.
		or when the emitter is moving, reduce the original particle count and spawn some dead particles.
*/
class _X2DAdvExport ParticleEmitter : public DisplayObject
									, public InteractiveObject
{
	const static DWORD DEFAULT_INIT_COLOR = 0xffffffff;
	const static DWORD DEFAULT_FINAL_COLOR = 0x00ffffff;

	const static int DEFAULT_TIME_TO_ALIVE = 2;

public:
	ParticleEmitter(void);
	virtual ~ParticleEmitter(void);

public:
	void 
		Fire(BOOL bAutoUpdate = FALSE);

	void 
		Stop();

	int
		LoadParticle(const String& strTextureFileName
					, int nActionCount = 1, int nBmpsPerAction = 1
					, COLORREF _keyColor = NO_KEY_COLOR,  BMPS_ALIGN bmpAlign = H_BMPS);

	void
		Reserve(int nCount);
	//...
	void
		CreateFromSprite(Sprite* pSprite);

	void
		Update();

	void
		SetBlendMode(BLEND_MODE blendMode);

	void 
		SetTimeToLive(float fTimeToLive);

	void 
		SetEmitterArea(EmitArea* pArea);

	float 
		GetEmitterWidth();

	float
		GetEmitterHeight();

	void
		SetCenterPosition(float x, float y);

	int 
		GetAliveParticleCount();

	float
		GetParticleWidth();

	float
		GetParticleHeight();

	void 
		SetEmissionRate(int fEmissionRate);


	//---
	void 
		SetDirection(const Vector3& direction);
	void 
		SetDirection(float fAngle);


	void 
		SetSpreadAngle(float fSpreadAngle);
	void 
		SetSpeedRange(float fMinSpeed, float fMaxSpeed);
	void 
		SetGravity(const Vector3& vGravity);

	
	void 
		SetRotateSpeedRange(float fMinRotateSpeed, float fMaxRotateSpeed);
	void
		SetRotateVariation(float fRotateVariation);

	void 
		SetInitScaleRange(Vector2 fMinInitScale, Vector2 fMaxInitScale);
	void 
		SetFinalScaleRange(Vector2 fMinFinalScale, Vector2 fMaxFinalScale);


	void 
		SetInitColor(ColorF initColor);
	void
		SetFinalColor(ColorF finalColor);

	void 
		SetActionIndex(int iActionIndex);
	void 
		SetFrameIndex(int iFrameIndex);


protected:
	SharedPtr<ParticleArray>
		_aliveParticleList;

	// Emitter info
	enum EMMITER_STATUS
	{
		EMMITER_SPRAYING,
		EMMITER_PAUSED,
		EMMITER_STOPPED,
	};

	EMMITER_STATUS
		status;


	RefCountPtr<EmitArea>
		_pEmitArea;

	BLEND_MODE
		_blendMode;

	BOOL
		_bRandomAction;
	BOOL
		_bRandomFrame;
	int 
		_iActionIndex;
	int 
		_iFrameIndex;


	// Number of particles be emitted per-seconds
	int 
		_nEmissionRate;

	float 
		_fTimeToAlive;

	float
		_fMinSpeed; 

	float
		_fMaxSpeed;


	// Spray direction related
	Vector3
		_direction;
	Vector3
		_vGravity;

	float 
		_fSpreadHalfAngle;
	float 
		_fBaseAngle;


	// Scale related
	BOOL 
		_bUniformScale;
	Vector2
		_vInitMinScale;
	Vector2
		_vInitMaxScale;

	Vector2
		_vFinalMinScale;
	Vector2
		_vFinalMaxScale;


	// Rotation 
	float 
		_fInitRotation;

	float 
		_fMinRotateSpeed;	// Angle rotate per-second.

	float
		_fMaxRotateSpeed; 

	float
		_fRotateVariation;


	// Color relate
	ColorF
		_initColor;

	ColorF
		_finalColor;


protected:
	void 
		_onEnterFrame(const Event* pEvent);

	INLINE void
		_sprayParticle();

	INLINE void
		_updateAliveParticles();
	INLINE void 
		_removeDeadParticles();
	INLINE void 
		_emitNewParticle();


private:
	typedef vector<ListParticleIt>::iterator VecListParticleIt;

	vector<ListParticleIt> 
		__deadParticleList;

	float 
		__fNewParticleCount;


};


_X2DADV_NS_END