#pragma once

#include "common/math/Point.h"


_X2DADV_NS_BEGIN

class _X2DAdvExport EmitArea : public Object
							, public ReferenceCounted
{
public:
	virtual PointF GetRandomPoint() = 0;
};

class _X2DAdvExport LineEmitArea : public EmitArea
{
public:
	LineEmitArea();


public:
	void 
		SetStartPoint(PointF point);

	void 
		SetEndPoint(PointF point);

	override PointF 
		GetRandomPoint();


protected:
	PointF
		_ptStart;
	PointF
		_ptEnd;


private:
	void 
		__updateParams();


private:
	float 
		__b;
	float
		__k;

	BOOL 
		__bUseVerticle;

};

class _X2DAdvExport CircleEmitArea : public EmitArea
{
public:
	override PointF 
		GetRandomPoint();
};

class _X2DAdvExport RingEmitArea : public EmitArea
{
public:
	RingEmitArea();
	explicit RingEmitArea(float fRadius);


public:
	void 
		SetRadius(float fRadius);

	override PointF 
		GetRandomPoint();


protected:
	float _fRadius;


private:
	UINT __iRange;


};

_X2DADV_NS_END