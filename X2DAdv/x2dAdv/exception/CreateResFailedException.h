#pragma once
#include "common/core/exception.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport CreateResFailedException : public Exception
{
public:
	CreateResFailedException(void)
		: Exception("Create resource failed!!!")
	{
		
	}
};


_X2DADV_NS_END