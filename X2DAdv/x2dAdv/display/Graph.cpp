#include "stdafx.h"
#include "Graph.h"
#include "x2dAdv/render/GraphData.h"
#include "x2d/display/stage.h"
#include "x2d/event/DisplayListEvent.h"

_X2DADV_NS_BEGIN

Graph::Graph()
{
	_pGraphData = NULL;
	AddEventListener(DisplayListEvent::ADD_TO_STAGE, MFUNC(&Graph::_onAddToStage, this));

	//...??? need listen REMOVE_FROM_STAGE event????
	_createRenderData();

}

Graph::~Graph()
{
	RemoveEventListener(DisplayListEvent::ADD_TO_STAGE, MFUNC(&Graph::_onAddToStage, this));
}

void Graph::_onAddToStage(const Event* pEvent)
{
	XASSERT(_pStage != EMPTY_STAGE);

	_createRenderData();

	IExtensionRender* pExtRender = _pStage->GetStageRender()->GetExtensionRender(L"IGraphRender");

	if(!pExtRender)
		XTHROWEX("Cannot find a IGraphRender for Graph rendering!!!");

	_pGraphData->_SetGraphRender((IGraphRender*)pExtRender);

}

void Graph::Clear()
{
	_pGraphData->Clear();
}

void Graph::MoveTo(const PointF& ptTo)
{
	_pGraphData->MoveTo(Vector3(ptTo.x, ptTo.y, 0));
}

void Graph::LineTo(const PointF& ptTo)
{
	_pGraphData->LineTo(Vector3(ptTo.x, ptTo.y, 0));
}

void Graph::MoveTo(const Vector2& ptTo)
{
	_pGraphData->MoveTo(Vector3(ptTo.x, ptTo.y, 0));
}

void Graph::LineTo(const Vector2& ptTo)
{
	_pGraphData->LineTo(Vector3(ptTo.x, ptTo.y, 0));
}

void Graph::_createRenderData()
{
	if(!_bManagedRenderData) 
	{
		_bManagedRenderData = TRUE;
		_pGraphData = NULL;
		_renderDataAdv = NULL;
		pRenderData = NULL;
	}

	if(	!_pGraphData)
	{
		_pGraphData = new GraphData;

		pRenderData = _pGraphData;
		_renderDataAdv = _pGraphData;	
	}

}

_X2DADV_NS_END