#pragma once

#include "x2d/display/DisplayObject.h"
#include "x2dAdv/render/RenderDataAdv.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport DisplayObjectAdv : public DisplayObject
{
public:
	DisplayObjectAdv()
	{
		_pStage = NULL;
	}

	virtual ~DisplayObjectAdv()
	{

	}

public:
	virtual void 
		Init(IStageRenderAdv* pStageRenderAdv){XASSERT(0);};

	virtual INLINE void
		SetX(float newRelativeX)
	{
		float changeX = newRelativeX - _x;
		__SetX(__GetX() + changeX);
		_x = newRelativeX;

		Matrix4 newMatrix;
		newMatrix.Identify();
		D3DXMatrixTranslation((D3DXMATRIX*)&newMatrix, __GetX(), __GetY(), __GetZ());

		_renderDataAdv->SetTransform(newMatrix);

		TRAVERSE_CHILD_NODE_HEAD
			((DisplayObjectAdv*)pCurNode)->SetX(((DisplayObjectAdv*)pCurNode)->_x + changeX);
		TRAVERSE_CHILD_NODE_TAIL
	}

	/**
		Set the relative y position
	*/
	virtual INLINE void
		SetY(float newRelativeY)
	{
		float changeY = newRelativeY - _y;
		__SetY(__y + changeY);
		_y = newRelativeY;

		Matrix4 newMatrix;
		newMatrix.Identify();
		D3DXMatrixTranslation((D3DXMATRIX*)&newMatrix, __GetX(), __GetY(), __GetZ());

		_renderDataAdv->SetTransform(newMatrix);

		TRAVERSE_CHILD_NODE_HEAD
			((DisplayObjectAdv*)pCurNode)->SetY(((DisplayObjectAdv*)pCurNode)->_y + changeY);
		TRAVERSE_CHILD_NODE_TAIL
	}


protected:
	SharedPtr<RenderDataAdv>
		_renderDataAdv;


};

_X2DADV_NS_END
