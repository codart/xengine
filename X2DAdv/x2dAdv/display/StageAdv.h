#pragma once

#include "x2d/display/Stage.h"
#include "x2dAdv/render/DXRenderAdv.h"
#include "x2dAdv/camera/Camera.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport StageAdv : public Stage
{
public:
	StageAdv(void);
	virtual ~StageAdv(void);

public:
	virtual void
		AddCompositor(const String& strName);
	final Camera2D*
		GetCamera();
	final IStageRenderAdv*
		GetGame2DRender();


protected:
	override void 
		_onStageWindowCreate(HWND hWnd);
	override void 
		_onStageWindowDestroy();

	virtual IStageRenderAdv* 
		createGame2DStageRender();
	virtual Camera2D*
		createCamera();


protected:
	IStageRenderAdv*
		_pGame2DRender;
	Camera2D* 
		_pCurrentCamera;

};


_X2DADV_NS_END