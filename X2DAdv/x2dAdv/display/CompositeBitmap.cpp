#include "StdAfx.h"
#include "CompositeBitmap.h"

_X2DADV_NS_BEGIN

CompositeBitmap::CompositeBitmap(void)
{
	_rowCount = _columnCount = 1;
}

CompositeBitmap::~CompositeBitmap(void)
{
}

void CompositeBitmap::CreateBitmap(int cellWidth, int cellHeight
								   , int rowCount, int columnCount)
{
	_createRenderData();

	int tempWidth = cellWidth * columnCount;
	int tempHeight = cellHeight * rowCount;

	BitmapData tempBitmap;
	tempBitmap.CreateBitmap(tempWidth, tempHeight, XRGB_32);

	bitmapData->Swap(&tempBitmap);

	_updateInternalData(NO_KEY_COLOR);

	_rowCount = rowCount;
	_columnCount = columnCount;
}

void CompositeBitmap::CopyToCell(Bitmap* srcBitmap, int cellX, int cellY
								 , Rect* pSrcClipRect)
{
	XASSERT(bitmapData);

	int nCellWidth = (int)width / _columnCount;
	int nCellHeight = (int)height / _rowCount;

	int srcX = 0;
	int srcY = 0;
	int rectWidth = nCellWidth;
	int rectHeight = nCellHeight;

	if(pSrcClipRect)
	{
		XASSERT(pSrcClipRect->left <= srcBitmap->width);
		XASSERT(pSrcClipRect->top <= srcBitmap->height);
		XASSERT(pSrcClipRect->left + pSrcClipRect->Width() <= srcBitmap->width);
		XASSERT(pSrcClipRect->top + pSrcClipRect->Height() <= srcBitmap->height);

		if(pSrcClipRect->Width() <= nCellWidth) 
			rectWidth = pSrcClipRect->Width();
		if(pSrcClipRect->Height() <= nCellHeight) 
			rectHeight = pSrcClipRect->Height();
		srcX = pSrcClipRect->left;
		srcY = pSrcClipRect->top;
	}

	Rect srcRect(srcX, srcY
				, srcX + rectWidth, srcY + rectHeight);
	
	Point destPoint(cellX * nCellWidth, cellY * nCellHeight);
	bitmapData->CopyPixels(srcBitmap->bitmapData, srcRect, destPoint);
}

_X2DADV_NS_END