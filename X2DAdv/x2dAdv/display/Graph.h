#pragma once

#include "x2dAdv/display/DisplayObjectAdv.h"
#include "common/math/vector3.h"
#include "common/math/vector2.h"
#include "common/math/Point.h"

_X2DADV_NS_BEGIN

class GraphData;
class IGraphRender;

class _X2DAdvExport Graph : public DisplayObjectAdv
{
public:
	Graph();
	virtual ~Graph();

public:
	void 
		LoadGraphFromFile(LPCWSTR pszFileName);

	void 
		SaveGraphToFile(LPCWSTR pszFileName);

	void 
		Clear();
	
	void
		MoveTo(const PointF& ptTo);

	void 
		LineTo(const PointF& ptTo);

	void 
		MoveTo(const Vector2& ptTo);
	void 
		LineTo(const Vector2& ptTo);


protected:
	override void 
		_createRenderData();
	void 
		_onAddToStage(const Event* pEvent);


protected:
	GraphData*
		_pGraphData;


};

_X2DADV_NS_END