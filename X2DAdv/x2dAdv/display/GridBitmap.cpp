#include "StdAfx.h"
#include "GridBitmap.h"

#include "x2dAdv/display/StageAdv.h"
#include "x2dAdv/render/GridBitmapData.h"


_X2DADV_NS_BEGIN

GridBitmap::GridBitmap(void)
{
	_pGridBitmapData = NULL;
}

GridBitmap::~GridBitmap(void)
{
}

void GridBitmap::LoadBitmap(const String& strFileName
				, int nRowCount, int nColumnCount
				, COLORREF keyColor)
{

	if(!_pStage) 
		XTHROWEX("GridBitmap::LoadBitmap failed, Null _pStage.	\
						The GridBitmap must be added to stage!!!!");

	_createRenderData();

	XASSERT(_pGridBitmapData);
	_pGridBitmapData->LoadFromBmpFile(((StageAdv*)_pStage)->GetGame2DRender()
									, strFileName.c_str(), nRowCount
									, nColumnCount, keyColor);
}


void GridBitmap::SetPointPos(int nRow, int nColumn, PointF fPosition)
{
	XASSERT(_pGridBitmapData);
	_pGridBitmapData->SetPointPos(nRow, nColumn, fPosition);
}

void GridBitmap::SetPointColor(int nRow, int nColumn, COLOR color)
{
	XASSERT(_pGridBitmapData);
	_pGridBitmapData->SetPointColor(nRow, nColumn, color);
}

void GridBitmap::_createRenderData()
{
	if(!_bManagedRenderData) 
	{
		_bManagedRenderData = TRUE;
		_pGridBitmapData = NULL;
		_renderDataAdv = NULL;
		pRenderData = NULL;
	}

	if(	!_pGridBitmapData) _pGridBitmapData = new GridBitmapData;

	pRenderData = _pGridBitmapData;
	_renderDataAdv = _pGridBitmapData;
}

_X2DADV_NS_END