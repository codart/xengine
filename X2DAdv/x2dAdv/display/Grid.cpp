#include "StdAfx.h"
#include "Grid.h"
#include "x2dAdv/render/GridData.h"
#include "x2dAdv/display/StageAdv.h"

_X2DADV_NS_BEGIN

Grid::Grid(void)
{
	_pGridData = NULL;
	pRenderData = _pGridData;
	_renderDataAdv = _pGridData;
}

Grid::~Grid(void)
{

}

void Grid::SetPointPos(int nRow, int nColumn, PointF position)
{
	XASSERT(_pGridData);
	_pGridData->SetPointPos(nRow, nColumn, position);
}

void Grid::SetPointColor(int nRow, int nColumn, COLOR color)
{
	XASSERT(_pGridData);
	_pGridData->SetPointColor(nRow, nColumn, color);
}


void Grid::CreateGrid(int width, int height
					, int nRowCount, int nColumnCount)
{
	if(!_pStage) 
		XTHROWEX("Grid::CreateGrid failed, Null _pStage. The GridData must be added to stage!!!!");

	try
	{
		GridData tempGridData;
		tempGridData.CreateGrid(((StageAdv*)_pStage)->GetGame2DRender()
								, width, height, nRowCount, nColumnCount);

		_createRenderData();
		_renderDataAdv->Swap(&tempGridData);
		//_updateInternalData(_keyColor);
	}
	catch(Exception& e)
	{
		DbgMsgBoxA(e.what());
	}
}

void Grid::_createRenderData()
{
	if(!_bManagedRenderData) 
	{
		_bManagedRenderData = TRUE;
		_pGridData = NULL;
		_renderDataAdv = NULL;
		pRenderData = NULL;
	}

	if(	!_pGridData) _pGridData = new GridData;

	pRenderData = _pGridData;
	_renderDataAdv = _pGridData;
}


_X2DADV_NS_END