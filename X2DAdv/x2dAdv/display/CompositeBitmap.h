#pragma once

#include "x2d/display/Bitmap.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport CompositeBitmap : public Bitmap
{
public:
	CompositeBitmap(void);
	virtual ~CompositeBitmap(void);

public:
	virtual void 
		CreateBitmap(int cellWidth, int cellHeight, int rowCount, int columnCount);

	virtual void 
		CopyToCell(Bitmap* srcBitmap, int cellX, int cellY, Rect* pSrcClipRect = NULL);


protected:
	int
		_rowCount;
	int
		_columnCount;
};


_X2DADV_NS_END
