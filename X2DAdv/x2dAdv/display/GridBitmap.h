#pragma once

#include "x2dAdv/display/DisplayObjectAdv.h"
#include "common/math/Point.h"

_X2DADV_NS_BEGIN

class GridBitmapData;

class _X2DAdvExport GridBitmap : public DisplayObjectAdv
{
public:
	GridBitmap(void);
	virtual ~GridBitmap(void);

public:
	void LoadBitmap(const String& strFileName
					, int nRowCount, int nColumnCount
					, COLORREF keyColor = NO_KEY_COLOR);

	
	void SetPointPos(int nRow, int nColumn, PointF fPosition);
	void SetPointColor(int nRow, int nColumn, COLOR color);


protected:
	override void 
		_createRenderData();


protected:
	GridBitmapData*
		_pGridBitmapData;
};


_X2DADV_NS_END
