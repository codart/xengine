#pragma once
#include "x2d/display/sprite.h"
#include "common/math/Rect.h"
#include "x2d/display/Bitmap.h"
#include "x2d/display/InteractiveObject.h"

/*
	*. Can we utilize hardware Z-Buffer and than use batch render to optimize rendering?

*/

_X2DADV_NS_BEGIN

class _X2DAdvExport Object2D : public Sprite
							 , public DisplayObjectContainer
							 , public InteractiveObject
{
public:
	Object2D(void);
	virtual ~Object2D(void);

public:
	void
		UniformZOffset();

	override ReferenceCounted*
		Clone(BOOL bUseReference = TRUE);


public:
	void 
		UpdateObjectPos();

	INLINE void 
		SetObjX(float objX){SetX(objX - hotX);}

	INLINE void 
		SetObjY(float objY)
	{
		float _yNew = objY - hotY;
		_setAbsoluteY(_yNew - _y);
		_y = _yNew;
	}

	final INLINE void
		SetObjPosition(float objX, float objY)
	{
		float _xNew = objX - hotX;
		float _yNew = objY - hotY;
		_setAbsoluteXY(_xNew - _x, _yNew - _y);
		_x = _xNew;
		_y = _yNew;
	}

	final INLINE void
		UseBottomAsZPos(BOOL bVal, float zOffset = 0);

	final INLINE float
		GetObjX()
	{
		return _x + hotX;
	}

	final INLINE float
		GetObjY()
	{
		return _y + hotY;
	}

	final void 
		SetBoundRect(float left, float top
					, float right, float bottom);

	final void 
		SetHotRect(float hotX, float hotY
				, float hotWidth, float hotHeight);

protected:
	override void 
		_updateInternalData(COLORREF _keyColor);

protected:
	void 
		_CopyData(Object2D* pDestObject2D, BOOL bReferenceToResource);

protected:
	BOOL 
		_bBottomAsZPos;
	RectF
		boundRect;

public:
	float 
		hotX;
	float 
		hotY;

	float 
		hotWidth;
	float 
		hotHeight;

	float 
		speedx;
	float 
		speedy;

#ifdef _DEBUG
	RefCountPtr<Bitmap>
		hotRectIndicator;
#endif // _DEBUG

};


_X2DADV_NS_END