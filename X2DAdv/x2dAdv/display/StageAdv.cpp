#include "StdAfx.h"
#include "StageAdv.h"
#include "x2d/display/_StageMgr.h"
#include "x2d/event/StageEvent.h"

_X2DADV_NS_BEGIN

StageAdv::StageAdv(void)
{
	_pCurrentCamera = NULL;
	_pGame2DRender = NULL;
}

StageAdv::~StageAdv(void)
{

}

void StageAdv::AddCompositor(const String& strName)
{
	_pGame2DRender->AddCompositor(strName);
}

void StageAdv::_onStageWindowCreate(HWND hWnd)
{
	if(_$pStageRender == EMPTY_RENDER)
	{
		XASSERT(!_pGame2DRender);
		_pGame2DRender = createGame2DStageRender();
		_$pStageRender = _pGame2DRender;
		_$pStageRender->SetHostWindow(hWnd);
		_StageMgr::Instance().AddStage(this);

		XASSERT(!_pCurrentCamera);
		_pCurrentCamera = createCamera();
		_pGame2DRender->SetCamera(_pCurrentCamera);
	}

	StageEvent e(StageEvent::STAGE_CREATE);
	e.pSourceObj = this;
	DispatchEvent(e);

	onInit();
}

void StageAdv::_onStageWindowDestroy()
{
	onUninit();

	Stage::_onStageWindowDestroy();

	SAFE_DELETE(_pCurrentCamera);
}

IStageRenderAdv* StageAdv::createGame2DStageRender()
{
	IStageRenderAdv* pGame2DRender = new DXRenderAdv;
	return pGame2DRender; 
}

Camera2D* StageAdv::createCamera()
{
	Camera2D* pCamera = new Camera2D;
	return pCamera;
}

Camera2D* StageAdv::GetCamera()
{
	return _pCurrentCamera;
}

IStageRenderAdv* StageAdv::GetGame2DRender()
{
	return _pGame2DRender;
}


_X2DADV_NS_END