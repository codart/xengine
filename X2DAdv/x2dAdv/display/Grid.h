#pragma once

#include "x2dAdv/display/DisplayObjectAdv.h"
#include "common/math/Point.h"

_X2DADV_NS_BEGIN

class GridData;

class _X2DAdvExport Grid : public DisplayObjectAdv
{
	static const int DEFAULT_ROW_COUNT = 3;
	static const int DEFAULT_COLUMN_COUNT = 3;

public:
	Grid(void);
	virtual ~Grid(void);


public:
	void CreateGrid(int width, int height
					, int nRowCount = DEFAULT_ROW_COUNT
					, int nColumnCount = DEFAULT_COLUMN_COUNT);

	void
		SetPointPos(int nRow, int nColumn, PointF position);

	void 
		SetPointColor(int nRow, int nColumn, COLOR color);


protected:
	override void 
		_createRenderData();


protected:
	GridData*
		_pGridData;

};

_X2DADV_NS_END
