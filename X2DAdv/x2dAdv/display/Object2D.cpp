#include "StdAfx.h"
#include "Object2D.h"

#include "x2dAdv/event/Object2DEvent.h"
#include "x2d/Library.h"

_X2DADV_NS_BEGIN

Object2D::Object2D(void) : DisplayObjectContainer(this), InteractiveObject(this)
{
	_bBindZToY = TRUE;
	speedx = speedy = 0;
	hotX = hotY = 0;
	hotWidth = hotHeight = 32;

	_bBottomAsZPos = TRUE;

	objType |= OBJ_OBJECT2D;
}

Object2D::~Object2D(void)
{
}

void Object2D::UniformZOffset()
{
	//Update children's x
	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->SetZOffset(zOffset - ((DisplayObject*)pCurNode)->GetY());
	TRAVERSE_CHILD_NODE_TAIL
}

void Object2D::SetBoundRect(float left, float top, float right, float bottom)
{
	boundRect.SetRect(left, top, right, bottom);
}

void Object2D::SetHotRect(float hotX, float hotY, float hotWidth, float hotHeight)
{
	this->hotX = hotX;
	this->hotY = hotY;
	this->hotWidth = hotWidth;
	this->hotHeight = hotHeight;

#ifdef _DEBUG
	if(!hotRectIndicator)
	{
		hotRectIndicator = (Bitmap*)Library::Instance().CreateObject(L"HotRectIndicator");
		hotRectIndicator->SetZ(TOP_MOST);
		hotRectIndicator->__alpha = 166;
		AddChild(hotRectIndicator);
	}

	hotRectIndicator->SetX(hotX);
	hotRectIndicator->SetY(hotY);
	hotRectIndicator->width = hotWidth;
	hotRectIndicator->height = hotHeight;

#endif // _DEBUG
}

ReferenceCounted* Object2D::Clone(BOOL bUseReference)
{
	Object2D* pObject2D = new Object2D; 
	_CopyData(pObject2D, bUseReference);

	return pObject2D;
}

void Object2D::_CopyData(Object2D* pDestObject2D, BOOL bReferenceToResource)
{
	pDestObject2D->speedx = speedx;
	pDestObject2D->speedy = speedy;

	pDestObject2D->_bBottomAsZPos = _bBottomAsZPos;

	pDestObject2D->SetHotRect(hotX, hotY, hotWidth, hotHeight);

	Sprite::_CopyData(pDestObject2D, bReferenceToResource);

}

void Object2D::UpdateObjectPos()
{
	float nDelta = GetDeltaTime() * 1000;

	float newObjX = _x + hotX + nDelta * speedx;

	if(newObjX <= boundRect.left || newObjX >= boundRect.right) 
	{
		newObjX = float(newObjX <= boundRect.left? boundRect.left:boundRect.right); 

		Object2DEvent event(newObjX < boundRect.left? 
							Object2DEvent::HIT_LEFT_BOUND 
							: Object2DEvent::HIT_RIGHT_BOUND);
		event.pSourceObj = this;
		DispatchEvent(event);
	}

	//----
	float newObjY = _y + hotY + nDelta * speedy;

	if(newObjY <= boundRect.top || newObjY >= boundRect.bottom) 
	{
		newObjY = float(newObjY <= boundRect.top? boundRect.top:boundRect.bottom);

		Object2DEvent event(newObjY< boundRect.top? 
							Object2DEvent::HIT_TOP_BOUND
							:Object2DEvent::HIT_BOTTOM_BOUND);
		event.pSourceObj = this;
		DispatchEvent(event);
	}

	SetObjPosition(newObjX, newObjY);
}

void Object2D::UseBottomAsZPos(BOOL bVal, float newZ)
{
	if(_bBottomAsZPos) SetZOffset(height);
	else SetZ(newZ);
}

void Object2D::_updateInternalData(COLORREF _keyColor)
{
	Sprite::_updateInternalData(_keyColor);
	if(_bBottomAsZPos) SetZOffset(height);
}


_X2DADV_NS_END