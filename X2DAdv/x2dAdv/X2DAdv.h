#pragma once

using namespace x2d;

#include "X2dAdvMacros.h"
#include "X2DAdvHeaders.h"
#include "X2DAdvManager.h"

extern "C"
{
_X2DADV_NS_BEGIN

	_X2DAdvExport X2DAdvManager* GetX2DAdvManager();

_X2DADV_NS_END
}
