#pragma once

_X2DADV_NS_BEGIN

enum CameraType 
{ 
	LANDOBJECT, AIRCRAFT 
};

class _X2DAdvExport Camera2D
{
public:
	Camera2D(void);
	Camera2D(CameraType cameraType);
	virtual ~Camera2D(void);

public:
	void Strafe(float units); // left/right
	void Fly(float units);    // up/down
	void Walk(float units);   // forward/backward

	void move(const D3DXVECTOR3& vec);
	void moveRelative(const D3DXVECTOR3& vec);

	void Pitch(float angle); // rotate on right vector
	void Yaw(float angle);   // rotate on up vector
	void Roll(float angle);  // rotate on look vector

	void getViewMatrix(D3DXMATRIX* viewMatrix); 


protected:
	CameraType  
		_cameraType;

	D3DXVECTOR3 
		_right;
	D3DXVECTOR3 
		_up;
	D3DXVECTOR3 
		_look;
	D3DXVECTOR3 
		_pos;


private:
	BOOL 
		__bChanged;

	D3DXMATRIX 
		__cachedViewMatrix;

};


_X2DADV_NS_END
