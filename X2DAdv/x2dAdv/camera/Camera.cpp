#include "StdAfx.h"
#include "Camera.h"

_X2DADV_NS_BEGIN

Camera2D::Camera2D()
{
	_cameraType = AIRCRAFT;
	__bChanged = TRUE;

	_pos   = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	_right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	_up    = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	_look  = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
}

Camera2D::Camera2D(CameraType cameraType)
{
	_cameraType = cameraType;

	_pos   = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	_right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	_up    = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	_look  = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
}

Camera2D::~Camera2D()
{

}

void Camera2D::getViewMatrix(D3DXMATRIX* viewMatrix)
{
	if(!__bChanged)
	{
		*viewMatrix = __cachedViewMatrix;
		return;
	}

	__bChanged = FALSE;

	// Keep camera's axes orthogonal to eachother
	D3DXVec3Normalize(&_look, &_look);

	D3DXVec3Cross(&_up, &_look, &_right);
	D3DXVec3Normalize(&_up, &_up);

	D3DXVec3Cross(&_right, &_up, &_look);
	D3DXVec3Normalize(&_right, &_right);

	// Build the view matrix:
	float x = -D3DXVec3Dot(&_right, &_pos);
	float y = -D3DXVec3Dot(&_up, &_pos);
	float z = -D3DXVec3Dot(&_look, &_pos);

	(*viewMatrix)(0, 0) = _right.x; 
	(*viewMatrix)(0, 1) = _up.x; 
	(*viewMatrix)(0, 2) = _look.x; 
	(*viewMatrix)(0, 3) = 0.0f;

	(*viewMatrix)(1, 0) = _right.y; 
	(*viewMatrix)(1, 1) = _up.y; 
	(*viewMatrix)(1, 2) = _look.y; 
	(*viewMatrix)(1, 3) = 0.0f;

	(*viewMatrix)(2, 0) = _right.z; 
	(*viewMatrix)(2, 1) = _up.z; 
	(*viewMatrix)(2, 2) = _look.z; 
	(*viewMatrix)(2, 3) = 0.0f;

	(*viewMatrix)(3, 0) = x;        
	(*viewMatrix)(3, 1) = y;     
	(*viewMatrix)(3, 2) = z;       
	(*viewMatrix)(3, 3) = 1.0f;

	__cachedViewMatrix = *viewMatrix;
}

void Camera2D::Walk(float units)
{
	__bChanged = TRUE;

	// move only on xz plane for land object
	if( _cameraType == LANDOBJECT )
		_pos += D3DXVECTOR3(_look.x, 0.0f, _look.z) * units;

	if( _cameraType == AIRCRAFT )
		_pos += _look * units;
}

void Camera2D::Strafe(float units)
{
	__bChanged = TRUE;

	// move only on xz plane for land object
	if( _cameraType == LANDOBJECT )
		_pos += D3DXVECTOR3(_right.x, 0.0f, _right.z) * units;

	if( _cameraType == AIRCRAFT )
		_pos += _right * units;
}

void Camera2D::Fly(float units)
{
	__bChanged = TRUE;

	// move only on y-axis for land object
	if( _cameraType == LANDOBJECT )
		_pos.y += units;

	if( _cameraType == AIRCRAFT )
		_pos += _up * units;
}

void Camera2D::Pitch(float angle)
{
	__bChanged = TRUE;

	D3DXMATRIX T;
	D3DXMatrixRotationAxis(&T, &_right, angle);

	// rotate _up and _look around _right vector
	D3DXVec3TransformCoord(& _up, & _up, &T);
	D3DXVec3TransformCoord(&_look, &_look, &T);
}

void Camera2D::Yaw(float angle)
{
	__bChanged = TRUE;

	D3DXMATRIX T;

	// rotate around world y (0, 1, 0) always for land object
	if( _cameraType == LANDOBJECT )
		D3DXMatrixRotationY(&T, angle);

	// rotate around own up vector for aircraft
	if( _cameraType == AIRCRAFT )
		D3DXMatrixRotationAxis(&T, &_up, angle);

	// rotate _right and _look around _up or y-axis
	D3DXVec3TransformCoord(&_right, &_right, &T);
	D3DXVec3TransformCoord(&_look, &_look, &T);
}

void Camera2D::Roll(float angle)
{
	// only roll for aircraft type
	if( _cameraType == AIRCRAFT )
	{
		__bChanged = TRUE;

		D3DXMATRIX rotateMatrix;
		D3DXMatrixRotationAxis(&rotateMatrix, &_look, angle);

		// rotate _up and _right around _look vector
		D3DXVec3TransformCoord(&_right, &_right, &rotateMatrix);
		D3DXVec3TransformCoord(&_up, &_up, &rotateMatrix);
	}
}

_X2DADV_NS_END