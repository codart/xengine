#pragma once

#include <Cg/cg.h> 
#include <Cg/cgD3D9.h>

#include "x2dAdv/render/IStageRenderAdv.h"
#include "x2dAdv/shader/IGPUProgram.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport FragmentProgram : public IGPUProgram
{
public:
	FragmentProgram(void);
	virtual ~FragmentProgram(void);

public:
	void 
		SetStageRender(IStageRender* pStageRender);
	IStageRender* 
		GetStageRender();

	override void 
		LoadProgram(LPCSTR pszFileName);
	override void 
		DestroyProgram();

	void SetWorldViewProjMatrix(float* pMatrix);
	void SetCurrentTime(float time);

	void Bind();
	void UnBind();

protected:
	void onDeviceLost(Event* pEvent);
	void onDeviceReset(Event* pEvent);

protected:
	CGparameter 
		_cgParamModelViewProj;
	CGparameter
		_cgParamTime;

	IStageRender*
		_pGame2DRender;

	CGcontext 
		_cgContext;
	CGprofile 
		_cgVertexProfile;
	CGprogram 
		_cgVertexProgram;

	//--------
	CGprogram 
		_cgFragmentProgram;

	CGprofile 
		_cgFragmentProfile;


private:
	void _checkForCgError(const char *situation);


};


_X2DADV_NS_END