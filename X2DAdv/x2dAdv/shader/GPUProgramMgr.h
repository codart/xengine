#pragma once

#include "GPUProgram.h"
#include <Cg/cg.h> 
#include <Cg/cgD3D9.h>

_X2DADV_NS_BEGIN


class _X2DAdvExport GPUProgramManager //: public 
{
public:
	GPUProgramManager(void);
	virtual ~GPUProgramManager(void);

public:
	virtual GPUProgram* CreateProgram(GpuProgramType type, string strFileName);

public:
	IStageRender*
		_pStageRender;
	
};

class _X2DAdvExport D3DGPUProgramManager : public GPUProgramManager
{
public:
	D3DGPUProgramManager(LPDIRECT3DDEVICE9 device);

protected:
	CGcontext
		_cgContext;

};


_X2DADV_NS_END