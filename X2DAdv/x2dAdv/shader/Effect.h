#pragma once

#include <Cg/cg.h> 
#include <Cg/cgD3D9.h>

#include "x2dAdv/render/IStageRenderAdv.h"

_X2DADV_NS_BEGIN


class _X2DAdvExport Effect
{
public:
	Effect(IStageRender* pRender);
	virtual Effect(void);

public:
	void Begin();
	void End();

protected:
	void 
		_setStageRender(IStageRender* pStageRender);


protected:
	IStageRender*
		_pStageRender;


private:
	void _checkForCgError(const char *situation);


};


_X2DADV_NS_END