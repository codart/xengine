#pragma once

#include <Cg/cg.h> 
#include <Cg/cgD3D9.h>

#include "x2dAdv/render/IStageRenderAdv.h"
#include "x2dAdv/shader/GPUProgram.h"

_X2DADV_NS_BEGIN


class _X2DAdvExport VertexProgram : public GPUProgram
{
public:
	VertexProgram(IStageRender* pRender);
	virtual ~VertexProgram(void);

public:
	IStageRender* 
		GetStageRender();

	override void 
		LoadProgram(LPCSTR pszFileName);
	virtual void 
		UnloadProgram();

	override void 
		Destroy();

	void SetWorldViewProjMatrix(float* pMatrix);
	void SetCurrentTime(float time);

	void Bind();
	void UnBind();

protected:
	void onDeviceLost(const Event* pEvent);
	void onDeviceReset(const Event* pEvent);

	void 
		_setStageRender(IStageRender* pStageRender);


protected:
	IStageRender*
		_pStageRender;

	CGcontext 
		_cgContext;

	// Vertex shader
	CGprofile 
		_cgVertexProfile;
	CGprogram 
		_cgVertexProgram;

	// Fragment shader
	CGprogram 
		_cgFragmentProgram;

	CGprofile 
		_cgFragmentProfile;

	// Parameters
	CGparameter 
		_cgParamModelViewProj;
	CGparameter
		_cgParamTime;


private:
	void _checkForCgError(const char *situation);


};


_X2DADV_NS_END