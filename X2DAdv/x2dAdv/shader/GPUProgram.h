#pragma once

_X2DADV_NS_BEGIN

class Resource
{
public:
protected:
private:
};

enum GpuProgramType
{
	PROGRAM_VERTEX,
	PROGRAM_FRAGMENT,
	PROGRAM_GEOMETRY,
};


class _X2DAdvExport GPUProgram : public Resource
{
public:
	virtual void LoadProgram(LPCSTR pszFileName) = 0;
	virtual void UnloadProgram() = 0;
	virtual void Destroy() = 0;

protected:
	GpuProgramType
		_type;
};

_X2DADV_NS_END