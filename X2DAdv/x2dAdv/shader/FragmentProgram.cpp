#include "StdAfx.h"
#include "FragmentProgram.h"

#include "x2d/error/LoadResFailedException.h"
#include "x2dAdv/render/DXRenderAdv.h"
#include "x2dAdv/event/DeviceEvent.h"

_X2DADV_NS_BEGIN

FragmentProgram::FragmentProgram(void)
{
	_cgParamModelViewProj = NULL;

	_pGame2DRender = NULL;
	_cgContext = NULL;
	_cgVertexProfile = CG_PROFILE_UNKNOWN;
	_cgVertexProgram = NULL;
	_cgParamTime = NULL;

	_cgFragmentProgram = NULL;
	_cgFragmentProfile = CG_PROFILE_UNKNOWN;
}

FragmentProgram::~FragmentProgram(void)
{

}

void FragmentProgram::onDeviceLost(Event* pEvent)
{
	//cgD3D9SetDevice(NULL);
}

void FragmentProgram::onDeviceReset(Event* pEvent)
{
	
}

void FragmentProgram::DestroyProgram()
{
	if(_cgVertexProgram)
	{
		cgDestroyProgram(_cgVertexProgram);
		_checkForCgError("destroying vertex program");
		_cgVertexProgram = NULL;
	}

	if(_cgFragmentProgram) 
	{
		_checkForCgError("destroying fragment program");
		cgDestroyProgram(_cgFragmentProgram);
		_cgFragmentProgram = NULL;
	}

	cgDestroyContext(_cgContext);

	cgD3D9SetDevice(NULL);
}

void FragmentProgram::SetStageRender(IStageRender* pStageRender)
{
	XASSERT(pStageRender);

	_pGame2DRender = pStageRender;
	cgD3D9SetDevice((LPDIRECT3DDEVICE9)pStageRender->GetDevice());
	_checkForCgError("setting Direct3D device");

	pStageRender->AddEventListener(DeviceEvent::DEVICE_LOST, this
									, MFunc(&FragmentProgram::onDeviceLost));
	pStageRender->AddEventListener(DeviceEvent::DEVICE_RESET, this
									, MFunc(&FragmentProgram::onDeviceReset));
}

IStageRender* FragmentProgram::GetStageRender()
{
	return _pGame2DRender;
}

void FragmentProgram::Bind()
{
	cgD3D9BindProgram(_cgVertexProgram);
	_checkForCgError("binding vertex program");

	cgD3D9BindProgram(_cgFragmentProgram);
	_checkForCgError("binding fragment program");
}

void FragmentProgram::UnBind()
{
	GetD3DDevice()->SetVertexShader(NULL);
	GetD3DDevice()->SetPixelShader(NULL);
}

void FragmentProgram::SetWorldViewProjMatrix(float* pMatrix)
{
	XASSERT(_cgParamModelViewProj);

	cgSetMatrixParameterfr(_cgParamModelViewProj, pMatrix);
	_checkForCgError("binding vertex program");
}

void FragmentProgram::SetCurrentTime(float time)
{
	XASSERT(_cgParamModelViewProj);

	cgSetParameter1f(_cgParamTime, time);
	_checkForCgError("binding vertex program");
	
}

void FragmentProgram::LoadProgram(LPCSTR pszFileName)
{
	_cgContext = cgCreateContext();
	_checkForCgError("creating context");
	cgSetParameterSettingMode(_cgContext, CG_DEFERRED_PARAMETER_SETTING);

	/* Determine the best profile once a device to be set. */
	_cgVertexProfile = cgD3D9GetLatestVertexProfile();
	_checkForCgError("getting latest profile");

	const char **profileOpts;
	profileOpts = cgD3D9GetOptimalOptions(_cgVertexProfile);
	_checkForCgError("getting latest profile options");

	_cgVertexProgram =
		cgCreateProgramFromFile(
		_cgContext,					/* Cg runtime context */
		CG_SOURCE,					/* Program in human-readable form */
		pszFileName,				/* Name of file containing program */
		_cgVertexProfile,			/* Profile: OpenGL ARB vertex program */
		"main",						/* Entry function name */
		profileOpts);				/* Pass optimal compiler options */
	_checkForCgError("creating vertex program from file");

	_cgParamModelViewProj = cgGetNamedParameter(_cgVertexProgram, "modelViewProj");
	_checkForCgError("could not get modelViewProj parameter");

	_cgParamTime = cgGetNamedParameter(_cgVertexProgram, "currentTime");
	_checkForCgError("could not get currentTime parameter");

	cgD3D9LoadProgram(_cgVertexProgram, false, 0);
	_checkForCgError("loading vertex program");

	_cgFragmentProfile = cgD3D9GetLatestPixelProfile();
	profileOpts = cgD3D9GetOptimalOptions(_cgFragmentProfile);
	_checkForCgError("getting latest pixel profile options");

	/* Specify fragment program with a string. */
	_cgFragmentProgram =
		cgCreateProgramFromFile(
		_cgContext,					/* Cg runtime context */
		CG_SOURCE,					/* Program in human-readable form */
		"f:/testFS.cg",
		_cgFragmentProfile,			/* Profile: latest fragment profile */
		"main",						/* Entry function name */
		profileOpts);				/* No extra compiler options */
	_checkForCgError("creating fragment program from string");

	cgD3D9LoadProgram(_cgFragmentProgram, false, 0);
	_checkForCgError("loading fragment program");

}


void FragmentProgram::_checkForCgError(const char *situation)
{
	char buffer[4096];
	CGerror error;
	const char *string = cgGetLastErrorString(&error);

	if (error != CG_NO_ERROR) {
		if (error == CG_COMPILER_ERROR) {
			sprintf_s(buffer,
				"Program: %s\n"
				"Situation: %s\n"
				"Error: %s\n\n"
				"Cg compiler output...\n",
				_cgVertexProgram, situation, string);
			OutputDebugStringA(buffer);
			OutputDebugStringA(cgGetLastListing(_cgContext));
			sprintf_s(buffer,
				"Program: %s\n"
				"Situation: %s\n"
				"Error: %s\n\n"
				"Check debug output for Cg compiler output...",
				_cgVertexProgram, situation, string);
			MessageBoxA(0, buffer,
				"Cg compilation error", MB_OK | MB_ICONSTOP | MB_TASKMODAL);
		} else 
		{
			sprintf_s(buffer,
				"Program: %s\n"
				"Situation: %s\n"
				"Error: %s",
				_cgVertexProgram, situation, string);
			MessageBoxA(0, buffer,
				"Cg runtime error", MB_OK | MB_ICONSTOP | MB_TASKMODAL);
		}
	}
}

_X2DADV_NS_END