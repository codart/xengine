#include "StdAfx.h"
#include "x2dAdv/render/RenderDataAdv.h"

_X2DADV_NS_BEGIN

RenderDataAdv::RenderDataAdv(void)
{
	_pStageRender = NULL;
	_worldMatrix.Identify();
}

RenderDataAdv::~RenderDataAdv(void)
{
	
}

void RenderDataAdv::Swap(RenderData* pRenderData)
{
	XASSERT(pRenderData->renderType > RENDER_TYPE_ADV_START);
	
	RenderDataAdv* pRenderDataAdv = (RenderDataAdv*)pRenderData;
	
	Matrix4 pTmpWorldMatrix = pRenderDataAdv->_worldMatrix;
	IStageRenderAdv* pTmpStageRender = pRenderDataAdv->_pStageRender;

	pRenderDataAdv->_worldMatrix = _worldMatrix;
	pRenderDataAdv->_pStageRender = _pStageRender;

	_worldMatrix = pTmpWorldMatrix;
	_pStageRender = pTmpStageRender;
}

_X2DADV_NS_END