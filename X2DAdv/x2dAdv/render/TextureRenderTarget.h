#pragma once

#include "d3d9.h"
#include "x2d/render/RenderTarget.h"


_X2DADV_NS_BEGIN


class DXRenderAdv;
class Texture;

/** 
	Take texture as render target. 
 */
class _X2DAdvExport TextureRenderTarget : public RenderTarget
{
	friend class DXRenderAdv;

public:
	TextureRenderTarget(Texture* pOwnerTexture);
	virtual ~TextureRenderTarget();


public:
	Texture*
		GetOwnerTexture(){return _pOwnerTexture;};

public:
	final void 
		internalInit(IDirect3DTexture9* pD3DTexture9);


protected:
	Texture*
		_pOwnerTexture;

};

_X2DADV_NS_END