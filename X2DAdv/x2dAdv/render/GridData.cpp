#include "stdafx.h"
#include "x2dAdv/render/GridBitmapData.h"

#include "x2dAdv/exception/CreateResFailedException.h"
#include "x2dAdv/event/DeviceEvent.h"

_X2DADV_NS_BEGIN

GridData::GridData()
{
	renderType = RENDER_GRID;

	nRowCount = 0;
	nColumnCount = 0;

	width = 0;
	height = 0;

	_pGridVertexBuffer = NULL;
	_pGridIndexBuffer = NULL;

	_worldMatrix.Identify();
}

GridData::~GridData()
{
	SAFE_RELEASE(_pGridVertexBuffer);
	SAFE_RELEASE(_pGridIndexBuffer);
}

// Implement RenderData's interface
void GridData::Swap(RenderData* pRenderData)
{
	XASSERT(pRenderData && pRenderData->renderType == RENDER_GRID);

	GridData* pGridData = (GridData*)pRenderData;

	LPDIRECT3DVERTEXBUFFER9 pTempVertexBuf = pGridData->_pGridVertexBuffer;
	LPDIRECT3DINDEXBUFFER9 pTempIndexBuf = pGridData->_pGridIndexBuffer;
	int tmpWidth = pGridData->width;
	int tmpHeight = pGridData->height;
	int tmpRowCount = pGridData->nRowCount;
	int tmpColumnCount = pGridData->nColumnCount;

	pGridData->_pGridVertexBuffer = _pGridVertexBuffer;
	pGridData->_pGridIndexBuffer = _pGridIndexBuffer;

	pGridData->width = this->width;
	pGridData->height = this->height;
	pGridData->nRowCount = this->nRowCount;
	pGridData->nColumnCount = this->nColumnCount;

	this->_pGridVertexBuffer = pTempVertexBuf;
	this->_pGridIndexBuffer = pTempIndexBuf;

	// ...???? it is nasty ???
	if(_pGridVertexBuffer || _pGridIndexBuffer)
	{
		pGridData->_removeDeviceListener();
	}

	this->width = tmpWidth;
	this->height = tmpHeight;
	this->nRowCount = tmpRowCount;
	this->nColumnCount = tmpColumnCount;

	RenderDataAdv::Swap(pRenderData);

	if(_pGridVertexBuffer || _pGridIndexBuffer)
	{
		//.... DEVICE_LOST/RESET event should be dispatched by the _Device object. Check the Design in DXRenderBase.h.
		// Should remove event listener when D
		_pStageRender->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&GridData::_onDeviceLost, this));
		_pStageRender->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&GridData::_onDeviceReset, this));
	}

}

int GridData::Render(RenderState& renderState)
{
	XASSERT(_pStageRender);

	LPDIRECT3DDEVICE9 pD3DDevice = (LPDIRECT3DDEVICE9)_pStageRender->GetDevice();

	Matrix4 modelViewProjMatrix;
	_pStageRender->GetViewProjMatrix((float*)&modelViewProjMatrix);

	const Matrix4* pWorldMatrix = GetTransform();
	modelViewProjMatrix = (*pWorldMatrix) * modelViewProjMatrix;

	pD3DDevice->SetStreamSource(0, _pGridVertexBuffer, 0, sizeof(QuadVertex));
	pD3DDevice->SetIndices(_pGridIndexBuffer);

	pD3DDevice->SetTransform(D3DTS_WORLD, (D3DXMATRIX*)pWorldMatrix);

	pD3DDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0
										, GetVertexCount(), 0, GetCellCount()<<1);

	return 0;
}

int GridData::GetVertexCount()
{
	return nRowCount * nColumnCount;
}

int GridData::GetCellCount()
{
	return (nRowCount-1) * (nColumnCount-1);
}

void GridData::SetPointPos(int nRow, int nColumn, PointF position)
{
	if(!_pGridVertexBuffer) return;

	QuadVertex* _pGridVertexData = NULL;

	if(FAILED(_pGridVertexBuffer->Lock((nRow * nColumnCount + nColumn)*sizeof(QuadVertex)
									, sizeof(QuadVertex), (void**)&_pGridVertexData
									, D3DLOCK_NOOVERWRITE)))
	{
		XTHROWEX("_pGridVertexBuffer->Lock() failed");
	}

	_pGridVertexData->x = position.x;
	_pGridVertexData->y = position.y;

	_pGridVertexBuffer->Unlock();
}


void GridData::SetPointColor(int nRow, int nColumn, COLOR color)
{
	if(!_pGridVertexBuffer) return;

	QuadVertex* _pGridVertexData = NULL;

	// lock vertex buffer
	if(FAILED(_pGridVertexBuffer->Lock((nRow * nColumnCount + nColumn)*sizeof(QuadVertex)
									, sizeof(QuadVertex), (void**)&_pGridVertexData
									, D3DLOCK_NOOVERWRITE)))
	{
		XTHROWEX("_pGridVertexBuffer->Lock() failed");
	}

	_pGridVertexData->color = color;

	_pGridVertexBuffer->Unlock();
}

void GridData::CreateGrid(IStageRenderAdv* pRender, int width, int height
						  , int nRowCount, int nColumnCount)
{
	XASSERT(pRender && pRender->GetDevice());

	_pStageRender = pRender;

	this->width = width;
	this->height = height;
	this->nRowCount = nRowCount;
	this->nColumnCount = nColumnCount;

	__createDefaultPoolData();

	__constructGridData();
}

void GridData::__createDefaultPoolData()
{
	LPDIRECT3DDEVICE9 pDevice = (LPDIRECT3DDEVICE9)_pStageRender->GetDevice();
	//Create vertex buffer
	if( FAILED (pDevice->CreateVertexBuffer(GetVertexCount() * sizeof(QuadVertex),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		QuadVertex::FVF_QUADVERTEX,
		D3DPOOL_DEFAULT, &_pGridVertexBuffer
		, NULL)))
	{
		XTHROW(CreateResFailedException);
	}

	// Create index buffer
	if( FAILED( pDevice->CreateIndexBuffer(GetCellCount()*6*sizeof(WORD),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY
		, D3DFMT_INDEX16, D3DPOOL_DEFAULT
		, &_pGridIndexBuffer, NULL ) ) )
	{
		XTHROW(CreateResFailedException);
	}

	// Default pool data should be release/reset when device lost/reset
	_pStageRender->AddEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&GridData::_onDeviceLost, this));
	_pStageRender->AddEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&GridData::_onDeviceReset, this));

}

void GridData::_onDeviceLost(const Event* pEvent)
{
	SAFE_RELEASE(_pGridVertexBuffer);
	SAFE_RELEASE(_pGridIndexBuffer);
}

void GridData::_onDeviceReset(const Event* pEvent)
{
	__createDefaultPoolData();
	__constructGridData();
}

void GridData::_removeDeviceListener()
{
	_pStageRender->RemoveEventListener(DeviceEvent::DEVICE_LOST, MFUNC(&GridData::_onDeviceLost, this));
	_pStageRender->RemoveEventListener(DeviceEvent::DEVICE_RESET, MFUNC(&GridData::_onDeviceReset, this));
}	

void GridData::__constructGridData()
{
	QuadVertex* pGridVertexData = NULL;
	WORD *pIndicesData;

	if(FAILED(_pGridVertexBuffer->Lock(0, 0, (void**)&pGridVertexData
		, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE)))
	{
		XTHROWEX("_pGridVertexBuffer->Lock() failed");
	}

	if(FAILED(_pGridIndexBuffer->Lock( 0, 0, (void**)&pIndicesData, 0 )))
	{
		XTHROWEX("_pGridIndexBuffer->Lock() failed");
	};

	int nCellWidth = width/(nColumnCount-1);
	int nCellHeight = height/(nRowCount-1);

	int nQuadCount = GetCellCount();
	int nRowQuadCount = nColumnCount-1;
	int nCellRow = 0;
	int nCellColumn = 0;

	QuadVertex* pVertexDataCursor = pGridVertexData;
	WORD* pIndiceDataCursor = pIndicesData;

	for(int quadIndex=0; quadIndex<nQuadCount; ++quadIndex)
	{
		nCellRow = quadIndex / nRowQuadCount;
		nCellColumn = quadIndex % nRowQuadCount;

		// Get four vertex of the cell(Quad), calculate vertexes' position
		int row =0;
		int column=0;

		int nVertexTopLeft;
		int nVertexTopRight;
		int nVertexBottomRight;
		int nVertexBottomLeft;

		for (int vertexIndex=0; vertexIndex<4; ++vertexIndex)
		{
			switch (vertexIndex)
			{
			case 0:
				row = nCellRow;
				column = nCellColumn;
				nVertexTopLeft = row*nColumnCount + column;
				break;
			case 1:
				row = nCellRow;
				column = nCellColumn+1;
				nVertexTopRight = row*nColumnCount + column;
				break;
			case 2:
				row = nCellRow+1;
				column = nCellColumn+1;
				nVertexBottomRight = row*nColumnCount + column;
				break;
			case 3:
				row = nCellRow+1;
				column = nCellColumn;
				nVertexBottomLeft = row*nColumnCount + column;
				break;
			}

			QuadVertex* pVertex = &pGridVertexData[row*nColumnCount + column];

			pVertex->x = float(column * nCellWidth);
			pVertex->y = float(row * nCellHeight);
			pVertex->z = 0;
			pVertex->color = 0xffffffff;

			pVertex->uv1.u = pVertex->x/width;
			pVertex->uv1.v = pVertex->y/height;
		}

		// Calculate vertex indicies
		// When use triangle list, two triangle need 6 indices
		*pIndiceDataCursor++=nVertexTopLeft;		*pIndiceDataCursor++=nVertexTopRight;	*pIndiceDataCursor++=nVertexBottomRight;
		*pIndiceDataCursor++=nVertexBottomRight;	*pIndiceDataCursor++=nVertexBottomLeft;	*pIndiceDataCursor++=nVertexTopLeft;

	}

	_pGridVertexBuffer->Unlock();
	_pGridIndexBuffer->Unlock();
}

int GridData::__getRowByVertexOrder(int vertexOrder)
{
	int nCellNumber = vertexOrder/4;

	return 0;
}

int GridData::__getColumnByVertexOrder(int vertexOrder)
{
	int nCellNumber = vertexOrder/4;

	return 0;
}


_X2DADV_NS_END