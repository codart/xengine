#pragma once

#include "x2dAdv/render/TextureRenderTarget.h"


_X2DADV_NS_BEGIN


/** 
	A filter effect that can be apply to a viewport. Such as bloom, HDR, blur etc.

    @remarks
 */
class _X2DAdvExport ViewportEffect : public Object
									, public ReferenceCounted
{
public:
	ViewportEffect(RenderTarget* pDestRenderTarget);
	virtual ~ViewportEffect();


public:
	virtual void
		Init();

	virtual TextureRenderTarget*
		GetTextureRenderTarget();

	virtual void
		SetDestRenderTarget(RenderTarget* pRenderTarget);

	/* 
	1. Add ViewportEffect to Stage's CompositorChain.
	2. Traverse Stage's CompositorChain, process each compositor(just Render a 
		FullScreen Quad with ViewportEffect's texture).
	*/
	virtual void
		RenderToDestTarget();


protected:
	RefCountPtr<RenderTexture>
		_pRenderTexture;
	RenderTarget*
		_pDestRenderTarget;

};

_X2DADV_NS_END