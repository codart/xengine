#include "stdafx.h"
#include "x2dAdv/render/GridBitmapData.h"


_X2DADV_NS_BEGIN

GridBitmapData::GridBitmapData(void)
{
	renderType = RENDER_GRID_BITMAP;
};

 GridBitmapData::~GridBitmapData(void)
 {

 }

void GridBitmapData::LoadFromBmpFile(IStageRenderAdv* pRender, LPCWSTR pszBmpFileName
								 , int nRowCount, int nColumnCount, COLORREF keyColor_)
{
	_pStageRender = pRender;

	if(!_pGridData) _pGridData = new GridData;
	if(!_pBitmapData) _pBitmapData = new BitmapData;

	_pBitmapData->LoadFromBmpFile(pszBmpFileName, keyColor_);

	_pGridData->CreateGrid(pRender, _pBitmapData->width, _pBitmapData->height, nRowCount, nColumnCount);

}

_X2DADV_NS_END