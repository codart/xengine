#pragma once

#include "x2d/render/RenderState.h"
#include "x2d/render/DXRenderBase.h"
#include "x2dAdv/render/IStageRenderAdv.h"
#include "x2dAdv/ParticleSystem/Particle.h"
#include "x2dAdv/ParticleSystem/ParticleArray.h"
#include "x2dAdv/camera/Camera.h"
#include "x2dAdv/shader/VertexProgram.h"
#include "x2dAdv/render/ViewportEffect.h"

_X2DADV_NS_BEGIN


/*
	*. Add shader fx support. Add frame effect for stage with shader.
	*. Should use alpha testing instead alpha blend?
*/

class X2DAdvManager;

class DXRenderAdv : public DXStageRenderBase
					, implements IStageRenderAdv
{
	DECLARE_NON_COPYABLE(DXRenderAdv)

	friend VertexProgram;
	friend X2DAdvManager;

public:
	DXRenderAdv(void);
	virtual ~DXRenderAdv(void);

public:
	//--- IStageRender
	override void
		Destroy();
	INLINE override DWORD_PTR 
		GetDevice();

	override int
		SetHostWindow(HWND hWnd);
	override int 
		SetRenderRectSize(int width, int height);

	override BOOL
		BeginRender();
	// Note: should call it between BeginRender()/EndRender().
	override void 
		ClearBackground(COLORREF bgColor);
	//..override void 
	//..	RenderDisplayList(DisplayObject* pRenderData);
	override void
		RenderStage(const RenderGroup* pRenderGroup);
	override int
		EndRender();
	IExtensionRender* 
		GetExtensionRender(const String& strRenderName);


	//--- IGame2DRender
	override void 
		SetCamera(Camera2D* pCamera);
	override float* 
		GetViewProjMatrix(float pMatrix[16]);
	override void
		AddCompositor(const String& strName);



protected:
	override void
		_onDeviceLost();
	override void
		_onDeviceReset();


protected:
	INLINE IExtensionRender* 
		_createExtensionRender(String strRenderName);
	INLINE void 
		_setRenderTarget(RenderTarget* pRenderTarget);

	INLINE void 
		_insertTreeIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_insertObjIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_renderZOrderedObjList();
	INLINE void
		_drawPrimitive(DisplayData* pRenderData);
	INLINE void 
		_drawSinglePrimitive(DisplayData* pDisplayData);

	INLINE void 
		_drawBitmap(DisplayData* pRenderMetaData);

	INLINE void 
		_drawGrid(DisplayData* pDisplayData);
	// Draw simple bitmap with clip rect
	INLINE void 
		_drawGridBitmap(DisplayData* pDisplayData);

	INLINE void 
		_drawParticleSys(DisplayData* pRenderMetaData);


	// Batch render support
	INLINE void
		_beginBatchRender();
	INLINE void 
		_renderBatch(BOOL bEndRender = FALSE);
	INLINE void
		_endBatchRender();
	

	//---
	INLINE void 
		_setBlendMode(BLEND_MODE blendMode);

	INLINE void 
		_restoreBatchState();

private:
	// Implement IEventDispatcher
	override void 
		DispatchEvent(Event& event);
	override int 
		AddEventListener(int nEventType, MemberFunctor functor);

	override int 
		RemoveEventListener(int nEventType, MemberFunctor functor);
	override void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = FALSE);
	override BOOL
		IsEventListener(int nEventType, MemberFunctor functor);

protected:
	typedef map<String, IExtensionRender*>::iterator MapExtensionRenderIt;

	map<String, IExtensionRender*>
		_extensionRenders;

	Camera2D*
		_pCurrentCamera;

	int
		_nQuadCount;

	// Batch render support
	RenderState
		_renderState;

	RefCountPtr<ViewportEffect>
		_viewportEffect;


protected:
	static int 
		_$InitRenderEngine();
	static int 
		_$UninitRenderEngine();


protected:
	static LPDIRECT3DVERTEXBUFFER9
		s_pBatchVertexBuffer;

	static QuadVertex* 
		_pBatchVertexArray;

private:
	Matrix4 
		__identifyMatrix;


private:
	INLINE void
		__blendOnFrameBuffer(DisplayData* pDisplayData);
	INLINE void 
		__drawBitmapWithLightMap(DisplayData* pDisplayData, DisplayData* pLightMap);

	INLINE void
		__beginStageMask(const RenderGroup* pRenderGroup);
	INLINE void
		__endStageMask(const RenderGroup* pRenderGroup);

	INLINE static void 
		__createDefaultPoolData();
	INLINE static void 
		__setDefaultStencilState();

};


_X2DADV_NS_END