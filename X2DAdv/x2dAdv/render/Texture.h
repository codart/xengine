#pragma once

#include "d3d9.h"


_X2DADV_NS_BEGIN


/** Abstract class representing a Texture resource.
    @remarks
 */
class _X2DAdvExport Texture : public Object
							, public ReferenceCounted
{
	friend class DXRenderAdv;
	friend class GraphicSystem;
	friend class TextureManager;

public:
	Texture();
	virtual ~Texture();

public:
	virtual int 
		GetWidth();
	virtual int
		GetHeight();
	virtual String&
		GetName(){return _strName;};

protected:
	String
		_strName;

public:
	final void
		internalInit(IDirect3DTexture9*);

protected:
	IDirect3DTexture9*
		_pDirect3DTexture9;

};

_X2DADV_NS_END