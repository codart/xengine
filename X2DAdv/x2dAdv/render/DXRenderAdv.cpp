#include "StdAfx.h"
#include "DXRenderAdv.h"

#include "x2dAdv/event/DeviceEvent.h"
#include "x2dAdv/event/RenderEvent.h"

#include "x2dAdv/render/GridBitmapData.h"
#include "x2dAdv/render/RenderDataAdv.h"
#include "x2dAdv/render/DXGraphRender.h"
#include "x2d/render/WindowRenderTarget.h"

#include "x2d/display/MaskObject.h"



_X2DADV_NS_BEGIN

LPDIRECT3DVERTEXBUFFER9 DXRenderAdv::s_pBatchVertexBuffer = NULL;
QuadVertex* DXRenderAdv::_pBatchVertexArray = NULL;

DXRenderAdv::DXRenderAdv(void)
{
	_renderState.Reset();

	_pBatchVertexArray = NULL;

	_pCurrentCamera = NULL;
	_nQuadCount = 0;	
	_bZSort = TRUE;

	__identifyMatrix.Identify();

	_viewportEffect = NULL;
}

DXRenderAdv::~DXRenderAdv(void)
{

}

int DXRenderAdv::_$InitRenderEngine()
{
	XASSERT(s_pd3dDevice);

	__createDefaultPoolData();
	__setDefaultStencilState();

	return 0;
}

int DXRenderAdv::_$UninitRenderEngine()
{
	SAFE_RELEASE(s_pBatchVertexBuffer);
	return 0;
}

void DXRenderAdv::DispatchEvent(Event& event)
{
	EventDispatcher::DispatchEvent(event);
}

int DXRenderAdv::AddEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::AddEventListener(nEventType, functor);
}

int DXRenderAdv::RemoveEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::RemoveEventListener(nEventType, functor);
}

void DXRenderAdv::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	EventDispatcher::RemoveEvent(nEventType, bLazyRelease);
}

BOOL DXRenderAdv::IsEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::IsEventListener(nEventType, functor);
}

float* DXRenderAdv::GetViewProjMatrix(float pMatrix[16])
{	
	if(!_pCurrentCamera) 
	{
		D3DXMatrixMultiply((D3DXMATRIX*)pMatrix, __pProjectionMatrix, __pViewMatrix);
	}
	else
	{
		// Use current camera's view matrix
		D3DXMATRIX viewMatrix;
		_pCurrentCamera->getViewMatrix(&viewMatrix);
		D3DXMatrixMultiply((D3DXMATRIX*)pMatrix, __pProjectionMatrix, &viewMatrix);
	}
	return pMatrix;
}

void DXRenderAdv::Destroy()
{
	// Release all extension renders
	MapExtensionRenderIt it = _extensionRenders.begin();

	for ( ; it != _extensionRenders.end(); ++it)
	{
		IExtensionRender* pRender = it->second;
		pRender->Destroy();
	}

	DXStageRenderBase::Destroy();
}

IExtensionRender* DXRenderAdv::_createExtensionRender(String strRenderName)
{
	if(strRenderName == L"IGraphRender")
	{
		return new DXGraphRender;
	}
	else
	{
		return NULL;
	}
}

IExtensionRender* DXRenderAdv::GetExtensionRender(const String& strRenderName)
{
	IExtensionRender* pExtensionRender = _extensionRenders[strRenderName];
	if(!pExtensionRender)
	{
		pExtensionRender = _createExtensionRender(strRenderName);
		pExtensionRender->Init(((IStageRenderAdv*)this));
		_extensionRenders[strRenderName] = pExtensionRender;
	}

	return pExtensionRender;
}

DWORD_PTR DXRenderAdv::GetDevice()
{
	return DXStageRenderBase::GetDevice();
}

int DXRenderAdv::SetRenderRectSize(int _width, int _height)
{
	DXStageRenderBase::SetRenderRectSize(_width, _height);
	return 0;
}

int DXRenderAdv::SetHostWindow(HWND hWnd)
{
	DXStageRenderBase::SetHostWindow(hWnd);
	return 0;
}

void DXRenderAdv::SetCamera(Camera2D* pCamera)
{
	_pCurrentCamera = pCamera;
}

void DXRenderAdv::AddCompositor(const String& strName)
{
	XASSERT(!_viewportEffect);
	_viewportEffect = new ViewportEffect(_pWindowRenderTarget);
}

void DXRenderAdv::_setRenderTarget(RenderTarget* pRenderTarget)
{
	s_pd3dDevice->SetRenderTarget(0, pRenderTarget->getRenderSurface());
}

BOOL DXRenderAdv::BeginRender()
{
	if(!__bDeviceReady)
	{
		if(__tryRestoreDevice() != D3D_OK) return FALSE;
	}

	RenderEvent event(RenderEvent::PRE_RENDER);
	event.pSourceObj = this;
	DispatchEvent(event);

	if(_viewportEffect)
	{
		// If has Compositor, render to Compositor's renderTarget.
		_setRenderTarget(_viewportEffect->GetTextureRenderTarget());
	}
	else
	{
		XASSERT(_pWindowRenderTarget);
		s_pd3dDevice->SetRenderTarget(0, _pWindowRenderTarget->getRenderSurface());
	}

	s_pd3dDevice->SetDepthStencilSurface(_pWindowRenderTarget->getDepthStencilSurface());

	// Begin the scene
	if( !SUCCEEDED( s_pd3dDevice->BeginScene() ) )
	{
		XTHROWEX("D3D BeginScene failed!!!");
	}

	// Set view and projection matrix
	s_pd3dDevice->SetTransform(D3DTS_PROJECTION, __pProjectionMatrix);
	s_pd3dDevice->SetTransform(D3DTS_VIEW, __pViewMatrix);

	// Set view matrix
	if(_pCurrentCamera) 
	{
		// Use current camera's view matrix
		D3DXMATRIX viewMatrix;
		_pCurrentCamera->getViewMatrix(&viewMatrix);
		s_pd3dDevice->SetTransform(D3DTS_VIEW, &viewMatrix);
	}

	// Prepare for batch render
	//.._beginBatchRender();

	return TRUE;
}

void DXRenderAdv::ClearBackground(COLORREF bgColor)
{
	HRESULT hRet = s_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL
										, bgColor, 1.0f, 0 );
}

void DXRenderAdv::RenderStage(const RenderGroup* pRenderGroup)
{
	// Set compositor as renderTarget
	// .....

	__drawBitmapWithLightMap(pRenderGroup->_pLightedBmp, pRenderGroup->_pLightMap);

	// Render mask (stencil)
	__beginStageMask(pRenderGroup);

	// Prepare for batch render
	_beginBatchRender();

	_insertTreeIntoZOrderedList(pRenderGroup->_pOpaqueDisplayTree);

	// Render ordered dirty obj list
	_renderZOrderedObjList();

	_endBatchRender();

	__blendOnFrameBuffer(pRenderGroup->_pLightMap);
	_renderState._pCurTexture = NULL;

	RenderEvent event(RenderEvent::RENDERING);
	event.pSourceObj = this;
	DispatchEvent(event);

	// Restore stencil state to default
	__endStageMask(pRenderGroup);
}

int DXRenderAdv::EndRender()
{
	RenderEvent event(RenderEvent::POST_RENDER);
	event.pSourceObj = this;
	DispatchEvent(event);

	// End the scene
	s_pd3dDevice->EndScene();

	if(_viewportEffect)
	{
		GetX2DAdvManager()->GetGraphicInterface()->TextureToTarget(_viewportEffect->GetTextureRenderTarget()
			, _pWindowRenderTarget);
	}


	HRESULT hResult = _pWindowRenderTarget->PresentToWindow();

	if(hResult == D3DERR_DEVICELOST)
	{
		_onDeviceLost();
		s_bDeviceLost = TRUE;
		__bDeviceReady = FALSE;
	}

	return 0;
}

void DXRenderAdv::_onDeviceLost()
{
	DXStageRenderBase::_onDeviceLost();

	SAFE_RELEASE(s_pBatchVertexBuffer);

	_renderState.Reset();

	//... Temp code 
	GetX2DAdvManager()->GetGraphicInterface()->_onDeviceLost();
	

	DeviceEvent event(DeviceEvent::DEVICE_LOST);
	event.pSourceObj = this;
	DispatchEvent(event);
}

void DXRenderAdv::_onDeviceReset()
{
	DXStageRenderBase::_onDeviceReset();

	__createDefaultPoolData();

	//... Temp code 
	GetX2DAdvManager()->GetGraphicInterface()->_onDeviceReset();

	// Dispatch event
	DeviceEvent event(DeviceEvent::DEVICE_RESET);
	event.pSourceObj = this;
	DispatchEvent(event);
}

void DXRenderAdv::_insertTreeIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(	!pDisplayData->_visible) return;

	if(pDisplayData->pRenderData) _insertObjIntoZOrderedList(pDisplayData);

	NodeTypePtr pCurNode = pDisplayData->pFirstChild;	
	while (pCurNode)					
	{
		_insertTreeIntoZOrderedList((DisplayObject*)pCurNode);
		pCurNode = pCurNode->pNextSibling;
	}	
}

void DXRenderAdv::_insertObjIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(_bZSort)
	{
		_orderedList.push_back(pDisplayData);
	}
	else _drawPrimitive(pDisplayData);
}

void DXRenderAdv::_renderZOrderedObjList()
{
	if(_bZSort)
	{
		sort(_orderedList.begin(), _orderedList.end(), CompareZ);

		VecDisplayDataIt it = _orderedList.begin();
		for ( ; it != _orderedList.end(); ++it)
		{
			_drawPrimitive(*it);
		}

		_orderedList.clear();
	}
}

/*
void DXRenderAdv::_saveRenderState()
{
	
}
*/

void DXRenderAdv::_restoreBatchState()
{
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	// Texture stage0 alpha blend option
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	// Frame blend option
	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Restore old vertex\index buffer
	s_pd3dDevice->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));
	s_pd3dDevice->SetIndices(s_pQuadIndexBuffer);

	s_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&__identifyMatrix);

	//... Sould interuput the batch render????
	s_pd3dDevice->SetTexture(0, (LPDIRECT3DTEXTURE9)_renderState._pCurTexture);

	s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX);
}

void DXRenderAdv::__blendOnFrameBuffer(DisplayData* pDisplayData)
{
	if(!pDisplayData) return;

	DWORD bAlphaEnabled = FALSE;

	s_pd3dDevice->GetRenderState(D3DRS_ALPHABLENDENABLE, &bAlphaEnabled);

	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
	s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

	s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX); // Need restore FVF?
	s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

	DXStageRenderBase::_drawSimpleBmp(pDisplayData);

	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, bAlphaEnabled);
}

void DXRenderAdv::__drawBitmapWithLightMap(DisplayData* pDisplayData, DisplayData* pLightMap)
{
	if(!pDisplayData || !pLightMap) return;

	BitmapData* pBitmapData = (BitmapData*)pDisplayData->pRenderData;
	BitmapData* pLightMapData = (BitmapData*)pLightMap->pRenderData;

	s_pd3dDevice->SetTexture(0, pBitmapData->_$pTexture);
	s_pd3dDevice->SetTexture(1, pLightMapData->_$pTexture);


	// set color blend operation and texture coordinate index for texture stage 1
	s_pd3dDevice->SetTextureStageState(1, D3DTSS_COLOROP,		D3DTOP_MODULATE2X);
	s_pd3dDevice->SetTextureStageState(1, D3DTSS_COLORARG1,	D3DTA_TEXTURE);
	s_pd3dDevice->SetTextureStageState(1, D3DTSS_COLORARG2,	D3DTA_CURRENT);
	s_pd3dDevice->SetTextureStageState(1, D3DTSS_ALPHAOP,		D3DTOP_DISABLE);
	s_pd3dDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX,	1);

	s_pd3dDevice->SetSamplerState(1, D3DSAMP_BORDERCOLOR, D3DCOLOR_RGBA(102, 102, 102, 102));
	s_pd3dDevice->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
	s_pd3dDevice->SetSamplerState(1, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);


	s_pQuadVertexBuffer->Lock( 0, 0, (void**)&s_pVertexArray
		, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE);

	// Copy one Quad's vertexes to s_pVertexArray
	Quad* pQuad = pDisplayData->RetrieveQuad(pBitmapData->_textureRect, pBitmapData->_lightMapUVRect);
	memcpy(s_pVertexArray, pQuad->vertex, sizeof(QuadVertex)*Quad::VETEXT_COUNT);

	s_pQuadVertexBuffer->Unlock();

	s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX); // Need restore FVF?
	s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

	int ret = s_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
	XASSERT(SUCCEEDED(ret));
}

void DXRenderAdv::_drawSinglePrimitive(DisplayData* pDisplayData)
{
	XASSERT(pDisplayData->pRenderData);

	switch (pDisplayData->pRenderData->renderType)
	{
	case RENDER_BITMAP:
		_drawSimpleBmp(pDisplayData);
		break;

	case RENDER_PARTICLE_SYS:
		_beginBatchRender();
		_drawParticleSys(pDisplayData);
		_endBatchRender();
		break;

	case RENDER_TEXT:
		_endBatchRender();
		_drawText(pDisplayData);
		_renderState._pCurTexture = NULL;
		_beginBatchRender();
		break;


	case RENDER_GRAPH:
		_endBatchRender();
		pDisplayData->pRenderData->Render(_renderState);
		_restoreBatchState();
		_beginBatchRender();
		break;

	case RENDER_GRID_BITMAP:
		pDisplayData->pRenderData->Render(_renderState);

		// Restore old vertex\index buffer
		GetD3DDevice()->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));
		GetD3DDevice()->SetIndices(s_pQuadIndexBuffer);

		GetD3DDevice()->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&__identifyMatrix);

		//... Sould interuput the batch render????
		GetD3DDevice()->SetTexture(0, (LPDIRECT3DTEXTURE9)_renderState._pCurTexture);
		break;

	case RENDER_GRID:
		GetD3DDevice()->SetTexture(0, NULL);
		pDisplayData->pRenderData->Render(_renderState);

		// Restore old vertex\index buffer
		GetD3DDevice()->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));
		GetD3DDevice()->SetIndices(s_pQuadIndexBuffer);

		GetD3DDevice()->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&__identifyMatrix);
		GetD3DDevice()->SetTexture(0, (LPDIRECT3DTEXTURE9)_renderState._pCurTexture);
		break;
	default:
		XASSERT(0);

	}		
}

void DXRenderAdv::_drawPrimitive(DisplayData* pDisplayData)
{
	XASSERT(pDisplayData->pRenderData);

	switch (pDisplayData->pRenderData->renderType)
	{
	case RENDER_BITMAP:
		_drawBitmap(pDisplayData);
		break;

	case RENDER_PARTICLE_SYS:
		_drawParticleSys(pDisplayData);
		break;

	case RENDER_TEXT:
		_endBatchRender();
		_drawText(pDisplayData);
		_renderState._pCurTexture = NULL;
		_beginBatchRender();
		break;

	case RENDER_GRAPH:
		_endBatchRender();
		pDisplayData->pRenderData->Render(_renderState);
		_restoreBatchState();
		_beginBatchRender();
		break;

	case RENDER_GRID_BITMAP:
		pDisplayData->pRenderData->Render(_renderState);

		// Restore old vertex\index buffer
		GetD3DDevice()->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));
		GetD3DDevice()->SetIndices(s_pQuadIndexBuffer);

		GetD3DDevice()->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&__identifyMatrix);

		//... Sould interuput the batch render????
		GetD3DDevice()->SetTexture(0, (LPDIRECT3DTEXTURE9)_renderState._pCurTexture);
		break;

	case RENDER_GRID:
		GetD3DDevice()->SetTexture(0, NULL);
		pDisplayData->pRenderData->Render(_renderState);

		// Restore old vertex\index buffer
		GetD3DDevice()->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));
		GetD3DDevice()->SetIndices(s_pQuadIndexBuffer);

		GetD3DDevice()->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&__identifyMatrix);
		GetD3DDevice()->SetTexture(0, (LPDIRECT3DTEXTURE9)_renderState._pCurTexture);
		break;

	default:
		XASSERT(0);
		
	}		
}

void DXRenderAdv::_drawGrid(DisplayData* pDisplayData)
{


}

void DXRenderAdv::_drawGridBitmap(DisplayData* pDisplayData)
{

}

void DXRenderAdv::_drawBitmap(DisplayData* pDisplayData)
{
	XASSERT(_pBatchVertexArray);

	BitmapData* pBitmapData = (BitmapData*) pDisplayData->pRenderData;
	XASSERT(pBitmapData);

	if(pBitmapData->_$pTexture != (LPDIRECT3DTEXTURE9)_renderState._pCurTexture 
		|| pDisplayData->blendMode != _renderState._curBlendMode
		|| _nQuadCount >(QUAD_COUNT_PER_DRAW -1)
		)
	{
		_renderBatch();

		if(pDisplayData->blendMode != _renderState._curBlendMode)
		{
			_setBlendMode(pDisplayData->blendMode);			
		}

		_renderState._curRenderType = RENDER_BITMAP;
		if(pBitmapData->_$pTexture != (LPDIRECT3DTEXTURE9)_renderState._pCurTexture)
		{
			s_pd3dDevice->SetTexture( 0, pBitmapData->_$pTexture);
			_renderState._pCurTexture = (DWORD_PTR)pBitmapData->_$pTexture;
		}
	}

	// Copy vertex to _pBatchVertexArray (Vertex buffer)
	Quad* pQuad = pDisplayData->RetrieveQuad(pBitmapData->_textureRect, pBitmapData->_lightMapUVRect);

	memcpy(&_pBatchVertexArray[_nQuadCount * Quad::VETEXT_COUNT], pQuad->vertex, SIZE_OF_QUAD);

	++_nQuadCount;
}

void DXRenderAdv::_setBlendMode(BLEND_MODE blendMode)
{
	_renderState._curBlendMode = blendMode;

	switch (blendMode)
	{
	case BLEND_NONE:
		s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		break;

	case BLEND_ALPHA:
		s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;

	case BLEND_ADD:
		s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		break;

	default:
		XASSERT(0);
	}

}

void DXRenderAdv::_beginBatchRender()
{
	s_pd3dDevice->SetStreamSource(0, s_pBatchVertexBuffer, 0, sizeof(QuadVertex));

	_renderState._curRenderType = RENDER_BITMAP;
	s_pBatchVertexBuffer->Lock( 0, 0, (void**)&_pBatchVertexArray
							, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE);
}

void DXRenderAdv::_endBatchRender()
{
	_renderBatch(TRUE);
}

void DXRenderAdv::_renderBatch(BOOL bEndRender)
{
	if(!_pBatchVertexArray) return;

	s_pBatchVertexBuffer->Unlock();
	
	if(_nQuadCount)
	{
		switch(_renderState._curRenderType)
		{
		case RENDER_BITMAP:
			// Vertex count is _nQuadCount*4, primitive count is _nQuadCount*2
			s_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0
											, _nQuadCount<<2, 0, _nQuadCount<<1);

			if(_nQuadCount >(QUAD_COUNT_PER_DRAW -1))
			{
				/* 
					Do CPU calculation when rendering(Listener RENDERING event), 
					can make the GPU parallel running with CPU. Should dispatch this event only when there
					will be a big batch of triangles be rendered. 
				*/
				RenderEvent event(RenderEvent::RENDERING);
				event.pSourceObj = this;
				DispatchEvent(event);
			}

			break;

		case RENDER_EMPTY:
			break;

		case RENDER_PARTICLE_SYS:
			break;

		default:
			XASSERT(0);
		}

	}

	_nQuadCount = 0;

	if(bEndRender) _pBatchVertexArray = NULL;
	else
	{
		s_pBatchVertexBuffer->Lock( 0, 0, (void**)&_pBatchVertexArray
									, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE);
	}
	
}

void DXRenderAdv::_drawParticleSys(DisplayData* pRenderMetaData)
{
	ParticleArray* pParticleArray = (ParticleArray*)pRenderMetaData->pRenderData;
	int nParticleCount = (int)pParticleArray->size();
	if(!nParticleCount) return;

	DisplayObject* pParticle = NULL;
	ListParticleIt it = pParticleArray->begin();
	ListParticleIt itEnd = pParticleArray->end();

	while(it != itEnd)
	{
		pParticle = *it;
		_drawBitmap(pParticle);
		++it;
	}
}

void DXRenderAdv::__createDefaultPoolData()
{
	//Create vertex buffer
	if( FAILED (s_pd3dDevice->CreateVertexBuffer(QUAD_VERTEX_BUF_SIZE*sizeof(QuadVertex),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		QuadVertex::FVF_QUADVERTEX,
		D3DPOOL_DEFAULT, &s_pBatchVertexBuffer
		, NULL)))
	{
		XTHROWEX("CreateVertexBuffer failed!!!");
	}
}

void DXRenderAdv::__beginStageMask(const RenderGroup* pRenderGroup)
{
	if(pRenderGroup->_pMaskObject)
	{
		// Set stencil state
		_renderState._pCurTexture = NULL;

		s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX); // Need restore FVF?
		s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

		s_pd3dDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0);
		s_pd3dDevice->SetRenderState(D3DRS_STENCILENABLE, true);
		s_pd3dDevice->SetRenderState(D3DRS_STENCILREF, 0x1);
		s_pd3dDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
		s_pd3dDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);

		//s_pd3dDevice->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x0000005);
		//s_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE); 
		//s_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

		// Render mask
		_drawSinglePrimitive(pRenderGroup->_pMaskObject);

		//s_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE); 

		s_pd3dDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0xf);

		s_pd3dDevice->SetRenderState(D3DRS_STENCILFUNC,  D3DCMP_EQUAL);
		s_pd3dDevice->SetRenderState(D3DRS_STENCILPASS,  D3DSTENCILOP_KEEP);

	}
	else
	{
		s_pd3dDevice->SetRenderState(D3DRS_STENCILENABLE, false);
	}
}

void DXRenderAdv::__endStageMask(const RenderGroup* pRenderGroup)
{
	if(pRenderGroup->_pMaskObject)
	{
		s_pd3dDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	}
}

void DXRenderAdv::__setDefaultStencilState()
{
	s_pd3dDevice->SetRenderState(D3DRS_STENCILENABLE,    false);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILREF,       0x1);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILMASK,      0xffffffff);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILZFAIL,     D3DSTENCILOP_KEEP);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILFAIL,      D3DSTENCILOP_KEEP);

	s_pd3dDevice->SetRenderState(D3DRS_STENCILFUNC,      D3DCMP_ALWAYS);
	s_pd3dDevice->SetRenderState(D3DRS_STENCILPASS,      D3DSTENCILOP_REPLACE);
}

_X2DADV_NS_END