#pragma once

#include "d3d9.h"
#include "x2dAdv/render/Texture.h"
#include "x2dAdv/render/RenderTexture.h"

_X2DADV_NS_BEGIN

/*
	This class encapsulate any device/driver related code. For example, D3D's device lost event will be dispatched
	by this class.
*/
class GraphicSystem
{
public:
	//virtual IStageRender*
	virtual Texture*
		CreateNormalTexture(const String& strTextureName);
	virtual RenderTexture*
		CreateRenderTexture(int nWidth, int nHeight);

	// Render a TextureRenderTarget to a RenderTarget with a Full Quad.
	virtual void
		TextureToTarget(TextureRenderTarget* pSrcRenderTarget, RenderTarget* pDestTarget);
	
	virtual void 
		SetTexture(int iTextureStage, Texture* pTexture);

	//---
	void
		_onDeviceLost();
	void
		_onDeviceReset();


public:
	void 
		internalInit(IDirect3DDevice9* pD3DDevice);
	void
		internalUninit();
	IDirect3DDevice9*
		getD3D9Device();

protected:
	void 
		_createInternalResource();


protected:
	IDirect3DDevice9*
		_pDirect3DDevice9;

	// Use for render Quad to RenderTarget(implement ViewportEffect). 
	// May be can use GridQuad to achieve some effect with VertexShader????
	LPDIRECT3DVERTEXBUFFER9
		_pQuadVertexBuffer;
	LPDIRECT3DINDEXBUFFER9
		_pQuadIndexBuffer; 
	QuadVertex*
		_pQuadVertexArray;

};

/*
class D3D9Interface : public GraphicInterface
{
public:
protected:
private:
};
*/

_X2DADV_NS_END