#include "stdafx.h"
#include "simpleDraw.h"
#include "math.h"
#include "d3d9.h"

#pragma warning(disable : 4996)

_X2DADV_NS_BEGIN

/*****************************************************************************
Debug Routine
*/
#define 	__SMALL_MODEL_WORK_MODE_DEBUG__
#ifdef 	__SMALL_MODEL_WORK_MODE_DEBUG__
#define 	__BREAK_		{__asm INT 03H}
void 	LOGOUT(const char* pszFormat, ...)
{
	static HANDLE	hLogout = NULL;
	char szLogMessage[1024];
	va_list vlist;
	DWORD dwWrited;

	if (hLogout == NULL)
	{
		AllocConsole();
		hLogout	= GetStdHandle(STD_OUTPUT_HANDLE);
	}

	va_start(vlist, pszFormat);
	vsprintf(szLogMessage, pszFormat, vlist);
	va_end(vlist);

	WriteConsole(hLogout, szLogMessage, (DWORD)strlen(szLogMessage), &dwWrited, NULL);

	FILE* pFile = fopen("vmelog.txt", "a+");

	if (pFile == NULL)
		__asm INT 03H;

	fwrite(szLogMessage, strlen(szLogMessage), 1, pFile);
	fwrite("\n", 1, 1, pFile);

	if (pFile != NULL)
		fclose(pFile);
}
#else
#define 	__BREAK_
#define 	LOGOUT()
#endif
//////////////////////////////////////////////////////////////////////////////


struct TVERTEX
{
	float x;
	float y;
	float z;
	DWORD c;
};

struct TSTREAM
{
	TVERTEX* pVB;
	int 		nLength;
	int 		nUsed;
	D3DPRIMITIVETYPE	drawType;
};

static TSTREAM s_stream		= {NULL, 0, 0, D3DPT_LINELIST};

inline void 		PushVertex(LPDIRECT3DDEVICE9 pRender, const Vector3& vec, 
							   D3DCOLOR col, D3DPRIMITIVETYPE type)
{
	if (s_stream.nLength == s_stream.nUsed)
	{
		TVERTEX* pTemp = s_stream.pVB;
		s_stream.pVB = new TVERTEX[s_stream.nLength * 2 + 1];

		if (s_stream.nUsed != 0)
		{
			memcpy(s_stream.pVB, pTemp, sizeof(TVERTEX) * s_stream.nUsed);
		}

		s_stream.nLength = s_stream.nLength * 2 + 1;
		delete[] pTemp;
	}

	if (type != s_stream.drawType && s_stream.nUsed != 0)
	{
		FlushDrawBuffer(pRender);
		s_stream.nUsed = 0;
		s_stream.drawType = type;
	}
	else if (s_stream.nUsed == 0)
	{
		s_stream.drawType = type;
	}

	s_stream.pVB[s_stream.nUsed].x = vec.x;
	s_stream.pVB[s_stream.nUsed].y = vec.y;
	s_stream.pVB[s_stream.nUsed].z = vec.z;
	s_stream.pVB[s_stream.nUsed].c = col;

	s_stream.nUsed++;
}

inline static float ATR/*AngleToRadian*/(float fAngle)
{
	fAngle = fAngle - ((int)(fAngle / 360.0F)) * 360.0F;
	return (fAngle / 180.0F) * VWPI;
}

inline static float RTA/*RadianToAngle*/(float fRadian)
{
	return fRadian / VWPI * 180.0F;
}

inline static float SqrtAdd(float num1, float num2)
{
	return num1 * num1 + num2 * num2;
}

inline static float _Dec(float val)
{
	return val - ((int)val);
}

inline static int _4D5C(float val)
{
	if (val - ((int)val) > 0.5F)
		return ((int)val) + 1;
	return (int)val;
}

inline static int _Car(float val)
{
	if (val - ((int)val) > 0.0F)
		return ((int)val) + 1;
	return (int)val;
}

inline static float _Distance(float x, float y)
{
	return (float)sqrt(x*x + y*y);
}

inline static float _Distance(float x1, float y1, float x2, float y2)
{
	return (float)sqrt(((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)));
}

inline static float _Min(float val1, float val2)
{
	if (val1 < val2)
		return val1;
	return val2;
}

inline static float _Max(float val1, float val2)
{
	if (val1 > val2)
		return val1;
	return val2;
}

static inline bool DrawQuadrangle(LPDIRECT3DDEVICE9 pRender,
								  const TVERTEX& ver1, const TVERTEX& ver2, 
								  const TVERTEX& ver3, const TVERTEX& ver4)
{
	TVERTEX	vec[4];

	if (pRender == NULL)
		return false;

	vec[0] = ver1;
	vec[1] = ver2;
	vec[2] = ver3;
	vec[3] = ver4;

	pRender->SetVertexShader(NULL);
	pRender->SetPixelShader(NULL);
	pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	pRender->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 4, &vec, sizeof(TVERTEX));

	return true;
};

bool	SimpleDrawRelease()
{
	if (s_stream.pVB != NULL)
	{
		delete[] s_stream.pVB;
		s_stream.pVB = NULL;
	}
	return true;
}

bool	VSmallModel_DrawPoint
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		pos,
 D3DCOLOR col,
 int 		nSize
 )
{
	TVERTEX vec;

	vec.x		= pos.x;
	vec.y		= pos.y;
	vec.z		= pos.z;
	vec.c		= col;

	pRender->SetVertexShader(NULL);
	pRender->SetPixelShader(NULL);
	pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	pRender->DrawPrimitiveUP(D3DPT_POINTLIST, 1, &vec, sizeof(TVERTEX));

	return true;
}

bool	DrawLine
(
 LPDIRECT3DDEVICE9 pRender, 
 const Vector3& vec1, const Vector3& vec2, 
 D3DCOLOR col1, D3DCOLOR col2
 )
{
	if (pRender == NULL)
		return false;

	PushVertex(pRender, vec1, col1, D3DPT_LINELIST);
	PushVertex(pRender, vec2, col2, D3DPT_LINELIST);

	return true;
}

bool	VSmallModel_DrawPlane
(
 LPDIRECT3DDEVICE9 pRender,
 float 	fWidth,
 float 	fHeight,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	float hx = fWidth / 2.0F;
	float hz = fHeight / 2.0F;

	TVERTEX vec[4];
	vec[0].x = hx;
	vec[0].y = 0.0F;
	vec[0].z = hz;
	vec[0].c = col;

	vec[1].x = hx;
	vec[1].y = 0.0F;
	vec[1].z = -hz;
	vec[1].c = col;

	vec[2].x = -hx;
	vec[2].y = 0.0F;
	vec[2].z = hz;
	vec[2].c = col;

	vec[3].x = -hx;
	vec[3].y = 0.0F;
	vec[3].z = -hz;
	vec[3].c = col;

	pRender->SetVertexShader(NULL);
	pRender->SetPixelShader(NULL);
	pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	pRender->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vec, sizeof(TVERTEX));

	return true;
}

bool	VSmallModel_DrawRound
(
 LPDIRECT3DDEVICE9 pRender, 
 float fHeight, float fRadius, int nSteps, 
 D3DCOLOR colTop, D3DCOLOR colBottom, float fBaseHeight, 
 ROUNDDIR eDirType, bool is2DCirCle
 )
{
	if (pRender == NULL)
		return false;

	TVERTEX* pTemp = new TVERTEX[nSteps+2];

	if (is2DCirCle)
	{
		switch (eDirType)
		{
		case ERD_VERTICAL:

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2;
				pTemp[x].c = colBottom;
				pTemp[x].x = fBaseHeight;
				pTemp[x].y = cos(delta) * fRadius;
				pTemp[x].z = sin(delta) * fRadius;
			}

			pTemp[nSteps] = pTemp[0];

			for (int x = 0; x < nSteps; x++)
			{
				DrawLine(pRender, Vector3(pTemp[x].x, pTemp[x].y, pTemp[x].z), 
					Vector3(pTemp[x + 1].x, pTemp[x + 1].y, pTemp[x + 1].z),
					pTemp[x].c, pTemp[x + 1].c);
			}

			break;

		case ERD_HORIZONTAL:

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2;
				pTemp[x].c = colBottom;
				pTemp[x].x = cos(delta) * fRadius;
				pTemp[x].y = fBaseHeight;
				pTemp[x].z = sin(delta) * fRadius;
			}

			pTemp[nSteps] = pTemp[0];

			for (int x = 0; x < nSteps; x++)
			{
				DrawLine(pRender, Vector3(pTemp[x].x, pTemp[x].y, pTemp[x].z), 
					Vector3(pTemp[x + 1].x, pTemp[x + 1].y, pTemp[x + 1].z),
					pTemp[x].c, pTemp[x + 1].c);
			}

			break;

		case ERD_SCREENDIR:

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2;
				pTemp[x].c = colBottom;
				pTemp[x].x = cos(delta) * fRadius;
				pTemp[x].y = sin(delta) * fRadius;
				pTemp[x].z = fBaseHeight;
			}

			pTemp[nSteps] = pTemp[0];

			for (int x = 0; x < nSteps; x++)
			{
				DrawLine(pRender, Vector3(pTemp[x].x, pTemp[x].y, pTemp[x].z), 
					Vector3(pTemp[x + 1].x, pTemp[x + 1].y, pTemp[x + 1].z),
					pTemp[x].c, pTemp[x + 1].c);
			}

			break;
		}
	}
	else
	{
		switch (eDirType)
		{
		case ERD_VERTICAL:

			pTemp[0].c = colTop;
			pTemp[0].x = 0.0F;
			pTemp[0].y = fHeight + fBaseHeight;
			pTemp[0].z = 0.0F;

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2;
				pTemp[x+1].c = colBottom;
				pTemp[x+1].x = cos(delta) * fRadius;
				pTemp[x+1].y = fBaseHeight;
				pTemp[x+1].z = sin(delta) * fRadius;
			}

			pTemp[nSteps + 1].c = colBottom;
			pTemp[nSteps + 1].x = fRadius;
			pTemp[nSteps + 1].y = fBaseHeight;
			pTemp[nSteps + 1].z = 0.0F;

			break;

		case ERD_HORIZONTAL:

			pTemp[0].c = colTop;
			pTemp[0].x = fHeight + fBaseHeight;
			pTemp[0].y = 0.0f;
			pTemp[0].z = 0.0f;

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2.0f;
				pTemp[x+1].c = colBottom;
				pTemp[x+1].x = fBaseHeight;
				pTemp[x+1].y = cos(delta) * fRadius;
				pTemp[x+1].z = sin(delta) * fRadius;
			}

			pTemp[nSteps + 1].c = colBottom;
			pTemp[nSteps + 1].x = fBaseHeight;
			pTemp[nSteps + 1].y = fRadius;
			pTemp[nSteps + 1].z = 0.0F;

			break;

		case ERD_SCREENDIR:

			pTemp[0].c = colTop;
			pTemp[0].x = 0.0f;
			pTemp[0].y = 0.0f;
			pTemp[0].z = fHeight + fBaseHeight;

			for (int x = 0; x < nSteps; x++)
			{
				float delta = (float)x / nSteps * VWPI * 2.0f;
				pTemp[x+1].c = colBottom;
				pTemp[x+1].x = cos(delta) * fRadius;
				pTemp[x+1].y = sin(delta) * fRadius;
				pTemp[x+1].z = fBaseHeight;
			}

			pTemp[nSteps + 1].c = colBottom;
			pTemp[nSteps + 1].x = fRadius;
			pTemp[nSteps + 1].y = 0.0f;
			pTemp[nSteps + 1].z = fBaseHeight;

			break;
		}

		pRender->SetVertexShader(NULL);
		pRender->SetPixelShader(NULL);
		pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

		pRender->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, nSteps, pTemp, sizeof(TVERTEX));
	}

	delete[] pTemp;

	return true;
}

bool	VSmallModel_DrawCone
(
 LPDIRECT3DDEVICE9 pRender,
 float fHeight, float fRadius, int nSteps,
 D3DCOLOR colTop, D3DCOLOR colBottom, float fBaseHeight, ROUNDDIR eDirType
 )
{
	if (pRender == NULL)
		return false;

	if (!VSmallModel_DrawRound(pRender, 0.0f, fRadius, nSteps, colTop, colBottom, fBaseHeight, eDirType))
		return false;

	if (!VSmallModel_DrawRound(pRender, fHeight, fRadius, nSteps, colTop, colBottom, fBaseHeight, eDirType))
		return false;

	return true;
}

bool	VSmallModel_DrawBox
(
 LPDIRECT3DDEVICE9 pRender,
 float 	fWidth,
 float 	fHeight,
 float 	fDepth,
 D3DCOLOR col
 )
{
	return false;
}

bool	VSmallModel_DrawSurroundBox
(
 LPDIRECT3DDEVICE9 pRender,
 float 	fWidth,
 float 	fHeight,
 float 	fDepth,
 D3DCOLOR col1,
 D3DCOLOR col2
 )
{
	if (pRender == NULL)
		return false;

	float x = fWidth / 2.0F;
	float y = fHeight/ 2.0F;
	float z = fDepth / 2.0F;

	const float dec = 0.2F;

	DrawLine(pRender, Vector3(x, y, z), Vector3(x - fWidth * dec, y, z), col1, col2);
	DrawLine(pRender, Vector3(x, y, z), Vector3(x, y - fHeight * dec, z), col1, col2);
	DrawLine(pRender, Vector3(x, y, z), Vector3(x, y, z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(-x, -y, -z), Vector3(-x + fWidth * dec, -y, -z), col1, col2);
	DrawLine(pRender, Vector3(-x, -y, -z), Vector3(-x, -y + fHeight * dec, -z), col1, col2);
	DrawLine(pRender, Vector3(-x, -y, -z), Vector3(-x, -y, -z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(-x, y, z), Vector3(-x + fWidth * dec, y, z), col1, col2);
	DrawLine(pRender, Vector3(-x, y, z), Vector3(-x, y - fHeight * dec, z), col1, col2);
	DrawLine(pRender, Vector3(-x, y, z), Vector3(-x, y, z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(x, -y, z), Vector3(x - fWidth * dec, -y, z), col1, col2);
	DrawLine(pRender, Vector3(x, -y, z), Vector3(x, -y + fHeight * dec, z), col1, col2);
	DrawLine(pRender, Vector3(x, -y, z), Vector3(x, -y, z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(x, y, -z), Vector3(x - fWidth * dec, y, -z), col1, col2);
	DrawLine(pRender, Vector3(x, y, -z), Vector3(x, y - fHeight * dec, -z), col1, col2);
	DrawLine(pRender, Vector3(x, y, -z), Vector3(x, y, -z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(-x, -y, z), Vector3(-x + fWidth * dec, -y, z), col1, col2);
	DrawLine(pRender, Vector3(-x, -y, z), Vector3(-x, -y + fHeight * dec, z), col1, col2);
	DrawLine(pRender, Vector3(-x, -y, z), Vector3(-x, -y, z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(-x, y, -z), Vector3(-x + fWidth * dec, y, -z), col1, col2);
	DrawLine(pRender, Vector3(-x, y, -z), Vector3(-x, y - fHeight * dec, -z), col1, col2);
	DrawLine(pRender, Vector3(-x, y, -z), Vector3(-x, y, -z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(x, -y, -z), Vector3(x - fWidth * dec, -y, -z), col1, col2);
	DrawLine(pRender, Vector3(x, -y, -z), Vector3(x, -y + fHeight * dec, -z), col1, col2);
	DrawLine(pRender, Vector3(x, -y, -z), Vector3(x, -y, -z + fDepth * dec), col1, col2);

	return true;
}

bool	VSmallModel_DrawBindingBox
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		min,
 const Vector3&		max,
 D3DCOLOR col1,
 D3DCOLOR col2
 )
{
	if (pRender == NULL)
		return false;

	const float dec = 0.2F;
	float fWidth    = max.x - min.x;
	float fHeight   = max.y - min.y;
	float fDepth    = max.z - min.z;

	DrawLine(pRender, Vector3(max.x, max.y, max.z), Vector3(max.x - fWidth * dec, max.y, max.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, max.y, max.z), Vector3(max.x, max.y - fHeight * dec, max.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, max.y, max.z), Vector3(max.x, max.y, max.z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(min.x, min.y, min.z), Vector3(min.x + fWidth * dec, min.y, min.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, min.y, min.z), Vector3(min.x, min.y + fHeight * dec, min.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, min.y, min.z), Vector3(min.x, min.y, min.z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(min.x, max.y, max.z), Vector3(min.x + fWidth * dec, max.y, max.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, max.y, max.z), Vector3(min.x, max.y - fHeight * dec, max.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, max.y, max.z), Vector3(min.x, max.y, max.z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(max.x, min.y, max.z), Vector3(max.x - fWidth * dec, min.y, max.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, min.y, max.z), Vector3(max.x, min.y + fHeight * dec, max.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, min.y, max.z), Vector3(max.x, min.y, max.z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(max.x, max.y, min.z), Vector3(max.x - fWidth * dec, max.y, min.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, max.y, min.z), Vector3(max.x, max.y - fHeight * dec, min.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, max.y, min.z), Vector3(max.x, max.y, min.z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(min.x, min.y, max.z), Vector3(min.x + fWidth * dec, min.y, max.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, min.y, max.z), Vector3(min.x, min.y + fHeight * dec, max.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, min.y, max.z), Vector3(min.x, min.y, max.z - fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(min.x, max.y, min.z), Vector3(min.x + fWidth * dec, max.y, min.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, max.y, min.z), Vector3(min.x, max.y - fHeight * dec, min.z), col1, col2);
	DrawLine(pRender, Vector3(min.x, max.y, min.z), Vector3(min.x, max.y, min.z + fDepth * dec), col1, col2);

	DrawLine(pRender, Vector3(max.x, min.y, min.z), Vector3(max.x - fWidth * dec, min.y, min.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, min.y, min.z), Vector3(max.x, min.y + fHeight * dec, min.z), col1, col2);
	DrawLine(pRender, Vector3(max.x, min.y, min.z), Vector3(max.x, min.y, min.z + fDepth * dec), col1, col2);

	return true;
}

bool	VSmallModel_DrawBindingWall
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		min,
 const Vector3&		max,
 D3DCOLOR crLine,
 D3DCOLOR crPlane,
 float 	fOffset
 )
{
	if (pRender == NULL)
		return false;

	Vector3 vec;
	// Far plane triangle1///////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Far plane triangle2//////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Near plane triangle1///////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Near plane triangle2//////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = max.y + fOffset;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);


	// Left plane triangle1/////////////////////////////////////
	vec.x = min.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = max.y + fOffset;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Left plane triangle2/////////////////////////////////////
	vec.x = min.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Right plane triangle1/////////////////////////////////////
	vec.x = max.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Right plane triangle2/////////////////////////////////////
	vec.x = max.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = max.y + fOffset;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	vec.x = max.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crPlane, D3DPT_TRIANGLELIST);

	// Draw line 1///////////////////////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = min.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 2///////////////////////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = max.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 3///////////////////////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = max.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 4///////////////////////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = max.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 5///////////////////////////////////////////////////////
	vec.x = max.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = min.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 6///////////////////////////////////////////////////////
	vec.x = min.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 7///////////////////////////////////////////////////////
	vec.x = min.x;
	vec.y = max.y;
	vec.z = max.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = min.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	// Draw line 8///////////////////////////////////////////////////////
	vec.x = min.x;
	vec.y = max.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	vec.x = min.x;
	vec.y = min.y;
	vec.z = min.z;
	PushVertex(pRender, vec, crLine, D3DPT_LINELIST);

	return true;
}

bool	VSmallModel_DrawCylinder
(
 LPDIRECT3DDEVICE9 pRender,
 float 	fHeight,
 float 	fRadius, 
 int 		nSteps,
 D3DCOLOR colTop, 
 D3DCOLOR colBottom
 )
{
	VSmallModel_DrawRound(pRender, 0.0F, fRadius, nSteps, colBottom, colBottom);

	//pRender->StoreTransform(D3DTS_WORLD);
	{
		D3DXMATRIX mtx, mtxWorld, mtxOldWorld;
		pRender->GetTransform(D3DTS_WORLD, &mtxWorld);

		mtxOldWorld = mtxWorld;

		D3DXMatrixTranslation(&mtx, 0.0F, fHeight, 0.0F);
		D3DXMatrixMultiply(&mtxWorld, &mtx, &mtxWorld);

		pRender->SetTransform(D3DTS_WORLD, &mtxWorld);
		VSmallModel_DrawRound(pRender, 0.0F, fRadius, nSteps, colTop, colTop);

		pRender->SetTransform(D3DTS_WORLD, &mtxOldWorld);
	}
	//pRender->RestoreTransform(D3DTS_WORLD);

	TVERTEX* pTemp = new TVERTEX[nSteps * 2 + 2];
	int x;

	for (x = 0; x<nSteps*2; x+=2)
	{
		float delta		= (float)x / nSteps * VWPI;
		pTemp[x].x		= cos(delta) * fRadius;
		pTemp[x].y		= 0.0F;
		pTemp[x].z		= sin(delta) * fRadius;
		pTemp[x].c		= colBottom;

		pTemp[x+1].x	= cos(delta) * fRadius;
		pTemp[x+1].y	= fHeight;
		pTemp[x+1].z	= sin(delta) * fRadius;
		pTemp[x+1].c	= colTop;
	}

	pTemp[nSteps * 2].x	= fRadius;
	pTemp[nSteps * 2].y = 0.0F;
	pTemp[nSteps * 2].z = 0.0F;
	pTemp[nSteps * 2].c = colBottom;

	pTemp[nSteps * 2 + 1].x	= fRadius;
	pTemp[nSteps * 2 + 1].y = fHeight;
	pTemp[nSteps * 2 + 1].z = 0.0F;
	pTemp[nSteps * 2 + 1].c = colTop;

	pRender->SetVertexShader(NULL);
	pRender->SetPixelShader(NULL);
	pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	pRender->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, nSteps * 2, pTemp, sizeof(TVERTEX));

	delete[] pTemp;

	return true;
}

bool	VSmallModel_DrawUniformMesh
(
 LPDIRECT3DDEVICE9 pRender,
 float 	fXStep, 
 float 	fZStep,
 int 		nWidth, 
 int 		nHeight,
 float* 	pHeight
 )
{
	return false;
}

bool	VSmallModel_DrawUniformVertex
(
 LPDIRECT3DDEVICE9 pRender,
 Vector3* pVertex,
 int 		nWidth,
 int 		nHeight,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	int x, y;

	for (y = 0; y<nHeight; y++)
	{
		for (x = 0; x<nWidth; x++)
		{
			//pRender->StoreTransform(D3DTS_WORLD);
			{
				D3DXMATRIX mtxWorld, oldWorld;

				pRender->GetTransform(D3DTS_WORLD, &oldWorld);
				D3DXMatrixTranslation(&mtxWorld, 
					pVertex[y * nWidth + x].x,
					pVertex[y * nWidth + x].y,
					pVertex[y * nWidth + x].z);

				pRender->SetTransform(D3DTS_WORLD, &mtxWorld);

				// Draw Start
				//VSmallModel_DrawCylinder(pRender, 0.5F, 0.1F, 30, col, col/2.0F);
				// Draw End

				pRender->SetTransform(D3DTS_WORLD, &oldWorld);
			}
			//pRender->RestoreTransform(D3DTS_WORLD);
		}
	}

	return true;
}

bool	VSmallModel_DrawText
(
 LPDIRECT3DDEVICE9 pRender,
 int 		x,
 int 		y,
 const char* pszText,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	//..pRender->Print(x, y, col, pszText);

	return true;
}

bool	VSmallModel_DrawText3D
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		pos,
 const char* pszText,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	D3DXMATRIX mtxView, mtxProj, mtxWorld;
	pRender->GetTransform(D3DTS_PROJECTION, &mtxProj);
	pRender->GetTransform(D3DTS_VIEW, &mtxView);
	pRender->GetTransform(D3DTS_WORLD, &mtxWorld);

	D3DVIEWPORT9 viewport;
	pRender->GetViewport(&viewport);

	D3DXVECTOR3 dst, src;
	src.x = pos.x;
	src.y = pos.y;
	src.z = pos.z;

	D3DXVec3Project(&dst, &src, &viewport, &mtxProj, &mtxView, &mtxWorld);

	//if (dst.z >= 0.0F && dst.z <= 1.0F)
	//..pRender->Print(dst.x + 15, dst.y + 15, col, pszText);

	return true;
}

bool	VSmallModel_DrawFlag
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		pos,
 D3DCOLOR col
 )
{
	return false;
}

bool	VSmallModel_DrawLittleFrame
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		pos,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	const float LITTLE_FRAME_SIZE	= 0.2F;

	DrawLine(pRender, 
		Vector3(pos.x-LITTLE_FRAME_SIZE, pos.y, pos.z-LITTLE_FRAME_SIZE),
		Vector3(pos.x+LITTLE_FRAME_SIZE, pos.y, pos.z-LITTLE_FRAME_SIZE), col, col);

	DrawLine(pRender, 
		Vector3(pos.x+LITTLE_FRAME_SIZE, pos.y, pos.z-LITTLE_FRAME_SIZE),
		Vector3(pos.x+LITTLE_FRAME_SIZE, pos.y, pos.z+LITTLE_FRAME_SIZE), col, col);

	DrawLine(pRender, 
		Vector3(pos.x+LITTLE_FRAME_SIZE, pos.y, pos.z+LITTLE_FRAME_SIZE),
		Vector3(pos.x-LITTLE_FRAME_SIZE, pos.y, pos.z+LITTLE_FRAME_SIZE), col, col);

	DrawLine(pRender, 
		Vector3(pos.x-LITTLE_FRAME_SIZE, pos.y, pos.z+LITTLE_FRAME_SIZE),
		Vector3(pos.x-LITTLE_FRAME_SIZE, pos.y, pos.z-LITTLE_FRAME_SIZE), col, col);

	return true;
}

bool	VSmallModel_DrawTriangle
(
 LPDIRECT3DDEVICE9 pRender,
 const Vector3&		vec1,
 const Vector3&		vec2,
 const Vector3&		vec3,
 D3DCOLOR col
 )
{
	if (pRender == NULL)
		return false;

	PushVertex(pRender, vec1, col, D3DPT_TRIANGLELIST);
	PushVertex(pRender, vec2, col, D3DPT_TRIANGLELIST);
	PushVertex(pRender, vec3, col, D3DPT_TRIANGLELIST);

	return true;
}

bool FlushDrawBuffer(LPDIRECT3DDEVICE9 pRender)
{
	if (s_stream.pVB == NULL)
		return false;

	if (pRender == NULL)
		return false;

	int nPTNum;
	switch(s_stream.drawType)
	{
	case D3DPT_POINTLIST:
		nPTNum = s_stream.nUsed / 2;
		break;

	case D3DPT_LINELIST:
		nPTNum = s_stream.nUsed / 2;
		break;

	case D3DPT_LINESTRIP:
		nPTNum = s_stream.nUsed - 1;
		break;

	case D3DPT_TRIANGLELIST:
		nPTNum = s_stream.nUsed / 3;
		break;

	case D3DPT_TRIANGLESTRIP:
		nPTNum = s_stream.nUsed - 2;
		break;

	case D3DPT_TRIANGLEFAN:
		nPTNum = s_stream.nUsed - 2;
		break;

	default:
		s_stream.nUsed = 0;
		return false;
	}

	if (nPTNum == 0)
		return true;

	pRender->SetVertexShader(NULL);
	pRender->SetPixelShader(NULL);
	pRender->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	pRender->DrawPrimitiveUP(s_stream.drawType, nPTNum, s_stream.pVB, sizeof(TVERTEX));
	s_stream.nUsed = 0;

	return true;
}

_X2DADV_NS_END