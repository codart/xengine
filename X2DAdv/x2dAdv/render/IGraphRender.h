#pragma once

#include "common/math/vector3.h"
#include "x2d/render/IExtensionRender.h"

_X2DADV_NS_BEGIN

enum DrawCommandType
{
	DRAW_CMD_MOVE_TO,
	DRAW_CMD_LINE_TO,

	DRAW_CMD_FILL_COLOR,
	DRAW_CMD_LINE_COLOR,

	DRAW_CMD_RECTANGLE,
	DRAW_CMD_SOLID_RECTANGLE,
	DRAW_CMD_OUTLINED_RECTANGLE,

	DRAW_CMD_ELLIPSE,
	DRAW_CMD_SOLID_ELLIPSE,
	DRAW_CMD_OUTLINED_ELLIPSE,

};


class DrawCommand
{
public:
	DrawCommandType nCmdType;

	Vector3 pt1;
	Vector3 pt2;
	Vector3 pt3;
	Vector3 pt4;

	DWORD param1;
	DWORD param2;
	DWORD param3;
	DWORD param4;
};


typedef vector<DrawCommand> DrawCommandList;
typedef vector<DrawCommand>::iterator DrawCommandListIt;

class GraphData;
class IGraphRender : public IExtensionRender
{
public:
	virtual void RenderGraph(GraphData* pGraphData) = 0;
};

_X2DADV_NS_END