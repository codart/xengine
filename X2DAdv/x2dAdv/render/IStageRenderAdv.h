#pragma once

#include "x2dAdv/camera/Camera.h"
#include "common/core/EventDispatcher.h"

_X2DADV_NS_BEGIN


class ViewportEffect;
class IStageRenderAdv : public IStageRender
{
public:
	virtual void 
		SetCamera(Camera2D* pCamera) = 0;
	virtual float* 
		GetViewProjMatrix(float* pMatrix) = 0;
	virtual void
		AddCompositor(const String& strName) = 0;
};


_X2DADV_NS_END