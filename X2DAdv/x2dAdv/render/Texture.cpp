#include "stdafx.h"
#include "Texture.h"

_X2DADV_NS_BEGIN

Texture::Texture()
{
	_pDirect3DTexture9 = NULL;
}

Texture::~Texture()
{
	RELEASE_CHECK(_pDirect3DTexture9, 0)
	SAFE_RELEASE(_pDirect3DTexture9);
}

int Texture::GetWidth()
{
	XASSERT(0);
	return 0;
}

int Texture::GetHeight()
{
	XASSERT(0);
	return 0;	
}

void Texture::internalInit(IDirect3DTexture9* pD3DTexture)
{
	_pDirect3DTexture9 = pD3DTexture;
}

_X2DADV_NS_END