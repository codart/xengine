#include "stdafx.h"
#include "GraphData.h"
#include "x2dAdv/render/DXGraphRender.h"

_X2DADV_NS_BEGIN

GraphData::GraphData()
{
	renderType = RENDER_GRAPH;
	_pGraphRender = NULL;

	_ptCurrent.x = _ptCurrent.y = _ptCurrent.z = 0;

	_lineColor = 0xffff00ff;
	_fillColor = 0xffff00ff;

}

GraphData::~GraphData()
{

}

void GraphData::_SetGraphRender(IGraphRender* pGraphRender)
{
	_pGraphRender = pGraphRender;
}

void GraphData::Clear()
{
	_drawCmdList.clear();
}

int GraphData::Render(RenderState& renderState)
{
	//.._pGraphRender->ApplayRenderState(renderState);
	_pGraphRender->RenderGraph(this);
	return 0;
}

void GraphData::MoveTo(Vector3 ptMoveTo)
{
	DrawCommand drawCommand;
	drawCommand.nCmdType = DRAW_CMD_MOVE_TO;
	drawCommand.pt1 = ptMoveTo;

	_drawCmdList.push_back(drawCommand);
}

void GraphData::LineTo(Vector3 ptLineTo)
{
	DrawCommand drawCommand;
	drawCommand.nCmdType = DRAW_CMD_LINE_TO;
	drawCommand.pt1 = ptLineTo;

	_drawCmdList.push_back(drawCommand);
}


_X2DADV_NS_END