#pragma once

#include "common/math/vector3.h"

_X2DADV_NS_BEGIN

const float VWPI					= 3.14159265f;
const float VWPI2					= 2*VWPI;
const float VWPI1_2					= 0.5f * VWPI;

enum ROUNDDIR
{
	ERD_HORIZONTAL = 0,
	ERD_SCREENDIR,
	ERD_VERTICAL
};

#define		D3DRGBA(r, g, b, a)		(D3DCOLOR)(((DWORD)((a)<<24))|((DWORD)((r)<<16))|((WORD)((g)<<8))|((BYTE)(b)))

bool	SimpleDrawRelease();

bool	VSmallModel_DrawPoint
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		pos,
	D3DCOLOR			col,
	int					nSize
);

bool	DrawLine
(
	LPDIRECT3DDEVICE9			pRender, 
	const Vector3&		vec1, 
	const Vector3&		vec2, 
	D3DCOLOR			col1, 
	D3DCOLOR			col2
);

bool	VSmallModel_DrawPlane
(
	LPDIRECT3DDEVICE9			pRender,
	float				fWidth,
	float				fHeight,
	D3DCOLOR			col
);

bool	VSmallModel_DrawRound
(
	LPDIRECT3DDEVICE9			pRender, 
	float				fHeight,
	float				fRadius,
	int					nSteps,
	D3DCOLOR			colTop,
	D3DCOLOR			colBottom,
	float				fBaseHeight = 0.0f,
	ROUNDDIR			eDirType = ERD_VERTICAL,
	bool				is2DCircle = false
);

bool	VSmallModel_DrawCone
(
	LPDIRECT3DDEVICE9			pRender,
	float				fHeight,
	float				fRadius, 
	int					nSteps,
	D3DCOLOR			colTop,
	D3DCOLOR			colBottom,
	float				fBaseHeight = 0.0f,
	ROUNDDIR			eDirType = ERD_VERTICAL 
);

bool	VSmallModel_DrawBox
(
	LPDIRECT3DDEVICE9			pRender,
	float				fWidth,
	float				fHeight,
	float				fDepth,
	D3DCOLOR			col
);

bool	VSmallModel_DrawSurroundBox
(
 LPDIRECT3DDEVICE9		pRender,
 float				fWidth,
 float				fHeight,
 float				fDepth,
 D3DCOLOR			col1,
 D3DCOLOR			col2
);

bool	VSmallModel_DrawBindingBox
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		min,
	const Vector3&		max,
	D3DCOLOR			col1,
	D3DCOLOR			col2
);

bool	VSmallModel_DrawBindingWall
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		min,
	const Vector3&		max,
	D3DCOLOR			crLine,
	D3DCOLOR			crPlane,
	float				fOffset
);

bool	VSmallModel_DrawCylinder
(
	LPDIRECT3DDEVICE9			pRender,
	float				fHeight,
	float				fRadius, 
	int					nSteps,
	D3DCOLOR			colTop, 
	D3DCOLOR			colBottom
);

bool	VSmallModel_DrawUniformMesh
(
	LPDIRECT3DDEVICE9			pRender,
	float				fXStep, 
	float				fZStep,
	int					nWidth, 
	int					nHeight,
	float*				pHeight
);

bool	VSmallModel_DrawUniformVertex
(
	LPDIRECT3DDEVICE9			pRender,
	Vector3*			pVertex,
	int					nWidth,
	int					nHeight,
	D3DCOLOR			col
);

bool	VSmallModel_DrawText
(
	LPDIRECT3DDEVICE9			pRender,
	int					x,
	int					y,
	const char*			pszText,
	D3DCOLOR			col
);

bool	VSmallModel_DrawText3D
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		pos,
	const char*			pszText,
	D3DCOLOR			col
);

bool	VSmallModel_DrawFlag
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		pos,
	D3DCOLOR			col
);

bool	VSmallModel_DrawLittleFrame
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		pos,
	D3DCOLOR			col
);

bool	VSmallModel_DrawTriangle
(
	LPDIRECT3DDEVICE9			pRender,
	const Vector3&		vec1,
	const Vector3&		vec2,
	const Vector3&		vec3,
	D3DCOLOR			col
);

bool	FlushDrawBuffer
(
	LPDIRECT3DDEVICE9			pRender
);

_X2DADV_NS_END


