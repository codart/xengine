#pragma once

#include "d3d9.h"
#include "TextureRenderTarget.h"


_X2DADV_NS_BEGIN


/** Abstract class representing a Texture that can be used as render target.
    @remarks
 */
class _X2DAdvExport RenderTexture : public Texture
{
	friend class DXRenderAdv;

public:
	RenderTexture();
	virtual ~RenderTexture();


public:
	virtual TextureRenderTarget*
		GetRenderTarget();


public:
	final void
		internalInit(IDirect3DTexture9*);


protected:
	RefCountPtr<TextureRenderTarget>
		_pTextureRenderTarget;

};

_X2DADV_NS_END