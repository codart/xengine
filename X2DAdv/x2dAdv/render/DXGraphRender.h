#include <d3d9.h>
#include "x2dAdv/render/IGraphRender.h"
#include "common/core/NonCopyble.h"

_X2DADV_NS_BEGIN

class _X2DAdvExport DXGraphRender : public IGraphRender
								  , public EventDispatcher
{
	DECLARE_NON_COPYABLE(DXGraphRender)

public:
	DXGraphRender();
	virtual ~DXGraphRender();

public:
	override void 
		Init(IStageRender* pStageRender);
	override void 
		Destroy();

	override void 
		RenderGraph(GraphData* pGraphData);

	override String
		GetRenderName();

	/*
	Fill_Color
	Line_Color

	Rectangle
	Solid_Rectangle
	Outlined_Rectangle

	Ellipse
	Solid_Ellipse
	Outlined_Ellipse
	*/

protected:
	// Implement IEventDispatcher
	override void 
		DispatchEvent(Event& event);
	override int 
		AddEventListener(int nEventType, MemberFunctor functor);
	override int 
		RemoveEventListener(int nEventType, MemberFunctor functor);
	override void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = FALSE);
	override BOOL 
		IsEventListener(int nEventType, MemberFunctor functor);


protected:
	// Draw methods
	INLINE override void 
		_moveTo(GraphData* pGraphData, const DrawCommand& drawCommand);
	INLINE override void 
		_lineTo(GraphData* pGraphData, const DrawCommand& drawCommand);


protected:
	LPDIRECT3DDEVICE9
		_pD3DDevice;
		
};


_X2DADV_NS_END
