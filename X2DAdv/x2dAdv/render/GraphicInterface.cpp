#include "stdafx.h"
#include "GraphicInterface.h"

#include "x2dadv/render/Texture.h"
#include "x2d/error/LoadResFailedException.h"

_X2DADV_NS_BEGIN

void GraphicSystem::internalInit(IDirect3DDevice9* pD3DDevice)
{
	_pDirect3DDevice9 = pD3DDevice;

	// Create some resources
	_createInternalResource();
}

void GraphicSystem::internalUninit()
{
	DWORD dwRef = _pQuadVertexBuffer->Release();
	dwRef = _pQuadIndexBuffer->Release();
	//SAFE_RELEASE(_pQuadVertexBuffer);
	//SAFE_RELEASE(_pQuadIndexBuffer);
}

void GraphicSystem::_onDeviceLost()
{
	SAFE_RELEASE(_pQuadIndexBuffer);
	SAFE_RELEASE(_pQuadVertexBuffer);
}

void GraphicSystem::_onDeviceReset()
{
	_createInternalResource();
}


void GraphicSystem::_createInternalResource()
{
	if( FAILED (_pDirect3DDevice9->CreateVertexBuffer(4 * sizeof(QuadVertex),
												D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
												QuadVertex::FVF_QUADVERTEX,
												D3DPOOL_DEFAULT, &_pQuadVertexBuffer
												, NULL)))
	{
		XTHROWEX("CreateVertexBuffer failed!!!");
	}

	// Create index buffer
	if( FAILED( _pDirect3DDevice9->CreateIndexBuffer(QUAD_COUNT_PER_DRAW*6*sizeof(WORD),
												D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY
												, D3DFMT_INDEX16, D3DPOOL_DEFAULT
												, &_pQuadIndexBuffer, NULL ) ) )
	{
		XTHROWEX("CreateIndexBuffer failed!!!");
	}

	WORD *pIndices, n=0;
	_pQuadIndexBuffer->Lock( 0, 0, (void**)&pIndices, 0 );

	for(int i=0; i<QUAD_VERTEX_BUF_SIZE/4; i++) 
	{
		// When use triangle strip, two triangle only need 4 indices
/*
		*pIndices++=n;		*pIndices++=n+1;	*pIndices++=n+2;
		*pIndices++=n+3;*/

		// When use triangle list, two triangle need 6 indices
		*pIndices++=n;		*pIndices++=n+1;	*pIndices++=n+2;
		*pIndices++=n+2;	*pIndices++=n+3;	*pIndices++=n;

		n+=4;
	}

	_pQuadIndexBuffer->Unlock();
}

IDirect3DDevice9* GraphicSystem::getD3D9Device()
{
	return _pDirect3DDevice9;
}

void GraphicSystem::SetTexture(int iTextureStage, Texture* pTexture)
{
	_pDirect3DDevice9->SetTexture(iTextureStage, pTexture->_pDirect3DTexture9);
}

void GraphicSystem::TextureToTarget(TextureRenderTarget* pSrcRenderTarget, RenderTarget* pDestTarget)
{
	int ret = 0;
	ret = _pDirect3DDevice9->BeginScene();
	XASSERT(SUCCEEDED(ret));

	ret = _pDirect3DDevice9->SetRenderTarget(0, pDestTarget->getRenderSurface());
	XASSERT(SUCCEEDED(ret));

	ret = _pDirect3DDevice9->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL
							, COLOR_XRGB(0, 255, 255), 1.0f, 0 );
	XASSERT(SUCCEEDED(ret));

	SetTexture(0, pSrcRenderTarget->GetOwnerTexture());


	_pQuadVertexBuffer->Lock( 0, 0, (void**)&_pQuadVertexArray 
		, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE);

	// Copy one Quad's vertexes to s_pVertexArray

	Quad quad;
	quad.xRect.topLeft.x = 0; 
	quad.xRect.topLeft.y = 0;
	quad.xRect.topLeft.z = 0;
	quad.xRect.topLeft.color = 0xffffffff;
	quad.xRect.topLeft.uv1.u = 0;
	quad.xRect.topLeft.uv1.v = 0;

	quad.xRect.topRight.x = (float)pDestTarget->GetWidth();
	quad.xRect.topRight.y = 0;
	quad.xRect.topRight.z = 0;
	quad.xRect.topRight.color = 0xffffffff;
	quad.xRect.topRight.uv1.u = 1;
	quad.xRect.topRight.uv1.v = 0;

	quad.xRect.bottomLeft.x = 0;
	quad.xRect.bottomLeft.y = (float)pDestTarget->GetHeight();
	quad.xRect.bottomLeft.z = 0;
	quad.xRect.bottomLeft.color = 0xffffffff;
	quad.xRect.bottomLeft.uv1.u = 0;
	quad.xRect.bottomLeft.uv1.v = 1;

	quad.xRect.bottomRight.x = (float)pDestTarget->GetWidth();
	quad.xRect.bottomRight.y = (float)pDestTarget->GetHeight();
	quad.xRect.bottomRight.z = 0;
	quad.xRect.bottomRight.color = 0xffffffff;
	quad.xRect.bottomRight.uv1.u = 1;
	quad.xRect.bottomRight.uv1.v = 1;

	memcpy(_pQuadVertexArray, quad.vertex, sizeof(QuadVertex) * Quad::VETEXT_COUNT);

	_pQuadVertexBuffer->Unlock();

	// Should this be applied when begin the ViewportEffect chain????
	_pDirect3DDevice9->SetFVF(QuadVertex::FVF_QUADVERTEX); // Need restore FVF?
	_pDirect3DDevice9->SetStreamSource(0, _pQuadVertexBuffer, 0, sizeof(QuadVertex));
	_pDirect3DDevice9->SetIndices(_pQuadIndexBuffer);
	_pDirect3DDevice9->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

	ret = _pDirect3DDevice9->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
	//int ret = s_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 4, 0, 2);
	XASSERT(SUCCEEDED(ret));

	ret = _pDirect3DDevice9->EndScene();
	XASSERT(SUCCEEDED(ret));
}

Texture* GraphicSystem::CreateNormalTexture(const String& strTextureName)
{
	LPDIRECT3DTEXTURE9 _pTexture = NULL;

	if( FAILED( D3DXCreateTextureFromFileEx( GetD3DDevice(), strTextureName.c_str(), 
									D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, 
									1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
									D3DX_FILTER_NONE, D3DX_FILTER_NONE, 
									0, NULL, NULL, &_pTexture) ) )
	{
		XTHROW(LoadResFailedException); 
	}

	Texture* pTexture = new Texture;
	pTexture->internalInit(_pTexture);

	return pTexture;
}

RenderTexture* GraphicSystem::CreateRenderTexture(int nWidth, int nHeight)
{
	LPDIRECT3DTEXTURE9 _pD3D9Texture = NULL;

	HRESULT hResult = _pDirect3DDevice9->CreateTexture(nWidth, nHeight
												, 1 , D3DUSAGE_RENDERTARGET
												, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT
												, &_pD3D9Texture, NULL);

	//D3DERR_INVALIDCALL, D3DERR_OUTOFVIDEOMEMORY, E_OUTOFMEMORY

	if(hResult !=  D3D_OK)
	{
		XTHROW(LoadResFailedException); 
	}

	RenderTexture* pRenderTexture = new RenderTexture;
	pRenderTexture->internalInit(_pD3D9Texture);

	return pRenderTexture;
}

_X2DADV_NS_END