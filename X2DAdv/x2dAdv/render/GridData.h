
#pragma once

#include "x2dAdv/render/RenderDataAdv.h"
#include "x2dAdv/render/IStageRenderAdv.h"
#include "common/math/Point.h"

_X2DADV_NS_BEGIN

class HaredwareVertexBuffer
{
public:
protected:
private:
};


/*
	Note: After device lost and then restored, the grid data will be also reset. It means that all the operation
	performed using SetPointPos and SetPointColor will lost.
*/
class _X2DAdvExport GridData : public RenderDataAdv
{
public:
	static const int GRID_3X3_SIZE = 9;

public:
	GridData();
	virtual ~GridData();


public:
	override int 
		Render(RenderState& renderState);

	void 
		CreateGrid(IStageRenderAdv* pRender, int width, int height
					, int nRow, int nColumn);

	INLINE int 
		GetCellCount();

	INLINE int 
		GetVertexCount();

	INLINE void 
		SetPointPos(int nRow, int nColumn, PointF position);

	INLINE void 
		SetPointColor(int nRow, int nColumn, COLOR color);


public:
	// Implement RenderData's interface
	override void
		Swap(RenderData* pRenderData);


public:
	// Vertex row count
	int nRowCount;

	// Vertex column count
	int nColumnCount;

	int width;
	int height;

	enum FVF
	{
		FVF_QUADVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1
	};

	// Vertex buffer
	LPDIRECT3DVERTEXBUFFER9
		_pGridVertexBuffer;

	LPDIRECT3DINDEXBUFFER9 
		_pGridIndexBuffer;


protected:
	void _onDeviceLost(const Event* pEvent);
	void _onDeviceReset(const Event* pEvent);

	void _removeDeviceListener();


private:
	INLINE void __createDefaultPoolData();
	INLINE void __constructGridData();

	INLINE int __getRowByVertexOrder(int vertexOrder);
	INLINE int __getColumnByVertexOrder(int vertexOrder);

};

_X2DADV_NS_END