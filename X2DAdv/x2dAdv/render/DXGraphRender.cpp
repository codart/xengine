#include "stdafx.h"
#include "DXGraphRender.h"
#include "IGraphRender.h"
#include "graphData.h"
#include "SimpleDraw.h"

_X2DADV_NS_BEGIN

DXGraphRender::DXGraphRender()
{
}

DXGraphRender::~DXGraphRender()
{
	SimpleDrawRelease();
}

void DXGraphRender::RenderGraph(GraphData* pGraphData)
{
	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);

	// Texture stage0 alpha blend option
	_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);


	XASSERT(pGraphData);
	DrawCommandList& cmdList = pGraphData->_GetDrawCommandList();

	DrawCommandListIt it = cmdList.begin();
	for ( ; it != cmdList.end(); ++it)
	{
		DrawCommand& drawCommand = *it;

		switch (drawCommand.nCmdType)
		{
		case DRAW_CMD_MOVE_TO:
			_moveTo(pGraphData, drawCommand);
			break;

		case DRAW_CMD_LINE_TO:
			_lineTo(pGraphData, drawCommand);
			break;

		default:
			XASSERT(0);
		}
	}

	FlushDrawBuffer(_pD3DDevice);

}

void DXGraphRender::_moveTo(GraphData* pGraphData, const DrawCommand& drawCommand)
{
	pGraphData->_ptCurrent = drawCommand.pt1;
}

void DXGraphRender::_lineTo(GraphData* pGraphData, const DrawCommand& drawCommand)
{
	DrawLine(_pD3DDevice, pGraphData->_ptCurrent, drawCommand.pt1, 0xffff00ff, 0xffff00ff);
	pGraphData->_ptCurrent = drawCommand.pt1;
}

void DXGraphRender::Init(IStageRender* pStageRender)
{
	XASSERT(pStageRender);
	_pD3DDevice = (LPDIRECT3DDEVICE9)pStageRender->GetDevice();
}

String DXGraphRender::GetRenderName()
{
	return L"IGraphRender";
}

void DXGraphRender::Destroy()
{
	delete this;
}

void DXGraphRender::DispatchEvent(Event& event)
{
	EventDispatcher::DispatchEvent(event);
}

int DXGraphRender::AddEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::AddEventListener(nEventType, functor);
}

int DXGraphRender::RemoveEventListener(int nEventType, MemberFunctor functor)
{
	return RemoveEventListener(nEventType, functor);
}

void DXGraphRender::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	EventDispatcher::RemoveEvent(nEventType, bLazyRelease);
}

BOOL DXGraphRender::IsEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::IsEventListener(nEventType, functor);
}


_X2DADV_NS_END
