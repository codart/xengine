#include "stdafx.h"
#include "TextureRenderTarget.h"
#include "Texture.h"

_X2DADV_NS_BEGIN

TextureRenderTarget::TextureRenderTarget(Texture* pOwnerTexture) 
{
	XASSERT(pOwnerTexture);
	_pOwnerTexture= pOwnerTexture;
}

TextureRenderTarget::~TextureRenderTarget()
{

}

void TextureRenderTarget::internalInit(IDirect3DTexture9* pD3DTexture9)
{
	XASSERT(_pRenderSurface == 0);

	IDirect3DSurface9* pRenderSurface;
	pD3DTexture9->GetSurfaceLevel(0, &pRenderSurface);

	RenderTarget::internalInit(pRenderSurface, NULL);
}

_X2DADV_NS_END