#pragma once

#include "common/math/matrix.h"
#include "x2dAdv/render/IStageRenderAdv.h"

_X2DADV_NS_BEGIN


enum RENDER_TYPE_ADV
{
	RENDER_TYPE_ADV_START = RENDER_TYPE_END,

	RENDER_PARTICLE_SYS,
	RENDER_GRID,
	RENDER_GRID_BITMAP,

	RENDER_TYPE_ADV_END,

};

class _X2DAdvExport RenderDataAdv : public RenderData
{
public:
	RenderDataAdv(void);
	virtual ~RenderDataAdv(void);

public:
	INLINE virtual void
		SetTransform(Matrix4 worldMatrix)
	{
		_worldMatrix = worldMatrix;
	}

	INLINE virtual const Matrix4*
		GetTransform()
	{
		return &_worldMatrix;
	};


public:
	// Implement RenderData's interface
	override void
		Swap(RenderData* pRenderData);

public:
	Matrix4
		_worldMatrix;

	IStageRenderAdv*
		_pStageRender;

};


_X2DADV_NS_END