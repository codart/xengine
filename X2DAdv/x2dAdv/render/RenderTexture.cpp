#include "stdafx.h"
#include "RenderTexture.h"

_X2DADV_NS_BEGIN


RenderTexture::RenderTexture()
{
	_pTextureRenderTarget = NULL;
}

RenderTexture::~RenderTexture()
{
	
}

TextureRenderTarget* RenderTexture::GetRenderTarget()
{
	return _pTextureRenderTarget;
}

void RenderTexture::internalInit(IDirect3DTexture9* pD3DTexture9)
{
	Texture::internalInit(pD3DTexture9);

	XASSERT(_pTextureRenderTarget == 0);

	_pTextureRenderTarget = new TextureRenderTarget(this);

	_pTextureRenderTarget->internalInit(pD3DTexture9);
}

_X2DADV_NS_END