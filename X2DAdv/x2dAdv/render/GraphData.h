#pragma once

#include "X2DAdv/render/RenderDataAdv.h"
#include "x2dadv/render/IGraphRender.h"

_X2DADV_NS_BEGIN


class _X2DAdvExport GraphData : public RenderDataAdv
{
public:
	GraphData();
	virtual ~GraphData();

public:
	override int 
		Render(RenderState& renderState);

	void 
		Clear();

	void
		MoveTo(Vector3 ptMoveTo);

	void 
		LineTo(Vector3 ptLineTo);


	INLINE void 
		_SetGraphRender(IGraphRender* pGraphRender);
	INLINE DrawCommandList&
		_GetDrawCommandList()
	{
		return _drawCmdList;
	}

public:
	Vector3
		_ptCurrent;

	COLOR
		_lineColor;

	COLOR
		_fillColor;

	DrawCommandList
		_drawCmdList;

	IGraphRender*
		_pGraphRender;


};

_X2DADV_NS_END