
#pragma once

#include "x2dAdv/render/RenderDataAdv.h"
#include "x2d/render/IStageRender.h"
#include "x2dAdv/render/GridData.h"

_X2DADV_NS_BEGIN

/*
	This class is the combination of a GridData and a BitmapData. Use this class to deform a bitmap
	with Grid.
*/
class _X2DAdvExport GridBitmapData : public RenderDataAdv
{
	friend class DXStageRenderBase;

public:
	GridBitmapData(void);
	virtual ~GridBitmapData(void);

public:
	void LoadFromBmpFile(IStageRenderAdv* pRender, LPCWSTR pszBmpFileName
						, int nRowCount, int nColumnCount, COLORREF keyColor = NO_KEY_COLOR);

	void SetPointPos(int nRow, int nColumn, PointF fPosition)
	{
		XASSERT(_pGridData);
		_pGridData->SetPointPos(nRow, nColumn, fPosition);
	}

	void SetPointColor(int nRow, int nColumn, COLOR color)
	{
		XASSERT(_pGridData);
		_pGridData->SetPointColor(nRow, nColumn, color);
	}


	//--- Override RenderDataAdv methods
	INLINE override int 
		Render(RenderState& renderState)
	{
		XASSERT(_pStageRender);

		LPDIRECT3DDEVICE9 pDevice = (LPDIRECT3DDEVICE9)_pStageRender->GetDevice();
		pDevice->SetTexture(0, _pBitmapData->_$pTexture);

		return _pGridData->GridData::Render(renderState);

	}

	INLINE override void
		SetTransform(Matrix4 worldMatrix)
	{
		RenderDataAdv::SetTransform(worldMatrix);
		_pGridData->SetTransform(worldMatrix);
	}


public:
	// Implement RenderData's interface
	override void
		Swap(RenderData* pRenderData){XTHROW(NoImplementationException);};


protected:
	SharedPtr<BitmapData>
		_pBitmapData;

	SharedPtr<GridData>
		_pGridData;

};

_X2DADV_NS_END