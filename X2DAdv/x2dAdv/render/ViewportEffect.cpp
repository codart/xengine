#include "stdafx.h"
#include "ViewportEffect.h"
#include "x2dAdv/resource/TextureManager.h"

_X2DADV_NS_BEGIN

ViewportEffect::ViewportEffect(RenderTarget* pDestRenderTarget)
{
	XASSERT(pDestRenderTarget);
	_pRenderTexture = NULL;
	_pDestRenderTarget = pDestRenderTarget;
}

ViewportEffect::~ViewportEffect()
{
}

void ViewportEffect::Init()
{
	XASSERT(_pRenderTexture == NULL);
	XASSERT(_pDestRenderTarget);

	_pRenderTexture = TextureManager::Instance().CreateRenderTexture(L"RenderTexture1"
													, _pDestRenderTarget->GetWidth()
													, _pDestRenderTarget->GetHeight());
}

TextureRenderTarget* ViewportEffect::GetTextureRenderTarget()
{
	if(!_pRenderTexture)
	{
		Init();
	}
	return _pRenderTexture->GetRenderTarget();
}

void ViewportEffect::SetDestRenderTarget(RenderTarget* pRenderTarget)
{
	_pDestRenderTarget = pRenderTarget;
}

void ViewportEffect::RenderToDestTarget()
{
	XASSERT(_pDestRenderTarget);
	
	// Set PS shader
	

	// Set VS shader
	

}

_X2DADV_NS_END