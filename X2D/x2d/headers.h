#pragma once

#include <atlmisc.h>
#include "map"
#include "vector"
#include "list"
#include "string"
#include "assert.h"

using namespace std;
using namespace ATL;

#include "x2d/colordefs.h"
#include "x2d/util/ObjectPtr.h"
#include "x2d/util/SharedPtr.h"

