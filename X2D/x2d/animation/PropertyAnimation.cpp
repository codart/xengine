#include "StdAfx.h"
#include "PropertyAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN


PropertyAnimation::PropertyAnimation(AnimatableProperty* pProperty, BOOL bWeakReference) 
: TweenAnimation(bWeakReference)
{
	_pProperty = pProperty;
	if(!_bWeakReference) pProperty->AddRef();

	valueFrom = pProperty->GetValue();
	valueTo = valueFrom;

	_deltaValue = 0;
	_currentValue = 0;
}

PropertyAnimation::PropertyAnimation(PropertyAnimation& srcAnimation) 
: TweenAnimation(srcAnimation)
{
	_pProperty = srcAnimation._pProperty;
	valueTo = srcAnimation.valueTo;
	valueFrom = srcAnimation.valueFrom;
}


PropertyAnimation::~PropertyAnimation(void)
{
	if(!_bWeakReference) _pProperty->Release();
}


void PropertyAnimation::_calculateFrameInfo()
{
	XASSERT(_nDuration >= _nFrameInterval);
	_nFrameCounts = _nDuration/_nFrameInterval;

	_deltaValue = ((float)valueTo - (float)valueFrom)/_nFrameCounts;
	_currentValue = (float)valueFrom;
}


void PropertyAnimation::_aniStep()
{
	_currentValue += _deltaValue;
	_pProperty->SetValue(_currentValue);
}


void PropertyAnimation::_resetObject()
{
	_currentValue = (float)valueFrom;
	_pProperty->SetValue(valueFrom);
}

_X2D_NS_END