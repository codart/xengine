#include "StdAfx.h"
#include "CompositeAnimation.h"

#include "x2d/event/AnimationEvent.h"

_X2D_NS_BEGIN

CompositeAnimation::CompositeAnimation(void)
{
	_nChildCount = 0;
}

CompositeAnimation::~CompositeAnimation(void)
{
	RemoveAllChildren();
}

void CompositeAnimation::RemoveAllChildren()
{
	Animation* pAnimation = (Animation*) pFirstChild;
	Animation* pTempObj = NULL;
	while(pAnimation)
	{
		pTempObj = (Animation*) pAnimation->pNextSibling;
		RemoveChild(pAnimation);
		pAnimation = pTempObj;
	}
}

int CompositeAnimation::AddChild(Animation* pAnimation)
{
	XASSERT(pAnimation != this);

	if(pAnimation->pParent == this) return 0;

	LinkedTreeNode<DWORD>::AddChildNode(pAnimation);

	pAnimation->AddEventListener(AnimationEvent::ANIMATION_END
								, MFUNC(&CompositeAnimation::_onChildAnimationEnd, this));

	_nChildCount++;

	return 0;
}

BOOL CompositeAnimation::RemoveChild(Animation* pAnimation)
{
	// Add a ref to pAnimation, make sure it is not deleted by RemoveChildNode
	RefCountPtr<Animation> pTempAnimation = pAnimation; 

	BOOL bFind;
	if(bFind = LinkedTreeNode<DWORD>::RemoveChildNode(pAnimation)) 
	{
		pAnimation->RemoveEventListener(AnimationEvent::ANIMATION_END
									, MFUNC(&CompositeAnimation::_onChildAnimationEnd, this));

		_nChildCount--;
	}

	return bFind;
}

int CompositeAnimation::GetChildCount() const
{
	return _nChildCount;
}

void CompositeAnimation::_onChildAnimationEnd(const Event* pEvent)
{
	XTHROW(NoImplementationException);
}

_X2D_NS_END
