#pragma once

#include "x2d/animation/CompositeAnimation.h"

_X2D_NS_BEGIN

class _X2DExport ParallelAnimation : public CompositeAnimation
{
public:
	ParallelAnimation(void);
	virtual ~ParallelAnimation(void);

public:
	override void 
		Play();

	override void 
		Pause();

	override void 
		Resume();

	override void 
		Stop();

protected:
	int
		_nEndedChildCount;

protected:
	override void
		_onChildAnimationEnd(const Event* pEvent);

};

_X2D_NS_END