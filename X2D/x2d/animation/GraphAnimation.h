#pragma once

#include "x2d/animation/TweenAnimation.h"

_X2D_NS_BEGIN

enum GraphMode
{
	Y_EQUAL_FX,
	X_EQUAL_FY
};

typedef float (*GraphFunc)(float);

float _X2DExport DefaultGraphFunction(float variable);

float _X2DExport Graph1(float variable);


class _X2DExport GraphAnimation : public TweenAnimation
{
public:
	GraphAnimation(DisplayObject* pDestObj, BOOL bWeakReference = TRUE);
	GraphAnimation(GraphAnimation& srcAnimation);
	virtual ~GraphAnimation(void);

public:
	void
		SetGraphFunction(GraphFunc pGraphFunc);

	GraphMode
		mode;

	// Valid only when _mode == POINT_TO_POINT
	float
		xFrom;
	float 
		xTo;

	float
		yFrom;
	float
		yTo;

	float
		_delta;

protected:
	override void 
		_aniStep();
	override void 
		_resetObject();
	override void 
		_calculateFrameInfo();


protected:
	DisplayObject*
		_destDisplayObj;

	GraphFunc
		_pGraphFunc;


};

_X2D_NS_END