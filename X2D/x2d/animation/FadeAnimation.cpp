#include "StdAfx.h"
#include "FadeAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

FadeAnimation::FadeAnimation(DisplayObject* pDestObj, BOOL bWeakReference)
								: TweenAnimation(bWeakReference)
{
	_destDisplayObj = pDestObj;
	if(!_bWeakReference) pDestObj->AddRef();

	alphaFrom = (float)pDestObj->GetAlpha();
	alphaTo = 0;
	_fCurrentAlpha = 0;

	_deltaAlpha = 2;
}

FadeAnimation::~FadeAnimation(void)
{
	if(!_bWeakReference) _destDisplayObj->Release();
}

FadeAnimation::FadeAnimation(FadeAnimation& srcAnimation)
							: TweenAnimation(srcAnimation)
{
	_destDisplayObj = srcAnimation._destDisplayObj;
	alphaTo = srcAnimation.alphaTo;
	alphaFrom = srcAnimation.alphaFrom;
}

void FadeAnimation::_calculateFrameInfo()
{
	XASSERT(_nDuration >= _nFrameInterval);
	_nFrameCounts = _nDuration/_nFrameInterval;
	_deltaAlpha = (alphaTo - alphaFrom)/_nFrameCounts;

	_fCurrentAlpha = alphaFrom;
}

void FadeAnimation::_aniStep()
{
	_fCurrentAlpha = _destDisplayObj->GetAlpha() + _deltaAlpha;
	_destDisplayObj->SetAlpha((int)_fCurrentAlpha);
}

void FadeAnimation::_resetObject()
{
	_fCurrentAlpha = alphaFrom;
	_destDisplayObj->SetAlpha((int)_fCurrentAlpha);
}

_X2D_NS_END