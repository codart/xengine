#include "StdAfx.h"
#include "TweenAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

TweenAnimation::TweenAnimation(BOOL bWeakReference) : _bWeakReference(bWeakReference)
{
	_aniTimerID = 0;
	_aniTimer = new Timer;

	_nDuration = DEFAULT_DURATION;
	_nFrameInterval = DEFAULT_FRAME_INTERVAL;

	_bSingleTarget = TRUE;

	_nFrameCounts = 0;
	_nCurFrame = 0;
}

TweenAnimation::TweenAnimation(TweenAnimation& srcAnimation) : Animation(srcAnimation)
{
	_aniTimer = new Timer;
	_aniTimerID = 0;

	_bWeakReference = srcAnimation._bWeakReference;
	_bSingleTarget = srcAnimation._bSingleTarget;
	_nDuration = srcAnimation._nDuration;
	_nFrameInterval = srcAnimation._nFrameInterval;

	_nFrameCounts = 0;
	_nCurFrame = 0;
}

TweenAnimation::~TweenAnimation(void)
{
	if(_aniTimerID)
	{
		_aniTimer->KillTimer(_aniTimerID);
		_aniTimerID = 0;
	}
}

void TweenAnimation::Play()
{
	switch(GetState())
	{
	case ANI_PAUSED:
		Resume();
		break;

	case ANI_STOPPED:
		if(!_aniTimerID) _createAniTimer();

		_resetAnimation();

		AnimationEvent event(AnimationEvent::ANIMATION_START);
		DispatchEvent(event);
		break;
	}

	_animationState = ANI_PLAYING;
}

void TweenAnimation::Pause()
{
	switch(GetState())
	{
	case ANI_PLAYING:
		_aniTimer->PauseTimer(_aniTimerID);
		_animationState = ANI_PAUSED;
		break;

	case ANI_STOPPED:
		return;
		break;
	}
}

void TweenAnimation::Resume()
{
	if( ANI_PAUSED != GetState()) return;

	_aniTimer->ResumeTimer(_aniTimerID);
	_animationState = ANI_PLAYING;
}

void TweenAnimation::Stop()
{
	switch(GetState())
	{
	case ANI_PLAYING:
	case ANI_PAUSED:
		_aniTimer->KillTimer(_aniTimerID);
		_aniTimerID = 0;

		_animationState = ANI_STOPPED;
		break;
	}
}
 
void TweenAnimation::SetFrameInterval(int interval)
{
	// Note: interval should >= game engine tick interval (10 ms)
	XASSERT(interval >= 10); 
	_nFrameInterval = interval;
	if(_aniTimerID) _aniTimer->SetTimerInterval(_aniTimerID, _nFrameInterval);
}

int TweenAnimation::GetFrameInterval() const
{
	return _nFrameInterval;
}

void TweenAnimation::SetDuration(int nDuration)
{
	_nDuration = nDuration;
}

int TweenAnimation::GetDuration(int nDuration) const
{
	return _nDuration;
}

void TweenAnimation::_resetAnimation()
{
	_resetObject();

	_nCurFrame = 0;

	_aniTimer->ResetTimer(_aniTimerID);
	_nLoopCounter = (_nLoopTimes == INFINITE)? 0:_nLoopTimes;
}

void TweenAnimation::_createAniTimer()
{
	_calculateFrameInfo();

	if(!_aniTimerID)
	{
		_aniTimerID = _aniTimer->SetTimer(_nFrameInterval);

		if(_bSingleTarget) // One movieclip
		{
			if(_nLoopTimes > 1 || _nLoopTimes == INFINITE)
			{
				_aniTimer->AddEventListener(_aniTimerID, MFUNC(&TweenAnimation::_onLoopAniTimer, this));
			}
			else _aniTimer->AddEventListener(_aniTimerID, MFUNC(&TweenAnimation::_onAniTimer, this));
		}
		else	// multi-movieclip
		{
			if(_nLoopTimes > 1)
			{
				_aniTimer->AddEventListener(_aniTimerID, MFUNC(&TweenAnimation::_onLoopAnisTimer, this));
			}
			else _aniTimer->AddEventListener(_aniTimerID, MFUNC(&TweenAnimation::_onAnisTimer, this));
		}
		_aniTimer->PauseTimer(_aniTimerID);
	}
	else
	{
		_aniTimer->SetTimerInterval(_aniTimerID, _nFrameInterval);
	}

}

void TweenAnimation::_stopAnimation()
{
	_aniTimer->KillTimer(_aniTimerID);
	_aniTimerID = 0;
	_animationState = ANI_STOPPED;

	AnimationEvent event(AnimationEvent::ANIMATION_END);
	event.pSourceObj = this;
	DispatchEvent(event);
}

void TweenAnimation::_onAniTimer(const Event* pEvent)
{
	_aniStep();

	XASSERT(_nCurFrame <= _nFrameCounts);
	_nCurFrame++;
	if(_nCurFrame == _nFrameCounts) _stopAnimation();
}

void TweenAnimation::_onLoopAniTimer(const Event* pEvent)
{
	_aniStep();

	_nCurFrame ++;
	XASSERT(_nCurFrame <= _nFrameCounts);
	if(_nCurFrame < _nFrameCounts) return;

	XASSERT(_nLoopCounter >= 0);
	if(_nLoopTimes != INFINITE && --_nLoopCounter == 0)
	{
		_stopAnimation();
	}
	else
	{
		_nCurFrame = 0;
		_resetObject();
	}
}

void TweenAnimation::_onAnisTimer(const Event* pEvent)
{
	XTHROW(NoImplementationException);
}

void TweenAnimation::_onLoopAnisTimer(const Event* pEvent)
{
	XTHROW(NoImplementationException);
}

void TweenAnimation::_aniStep()
{
	XTHROW(NoImplementationException);
}

void TweenAnimation::_resetObject()
{
	XTHROW(NoImplementationException);
}

void TweenAnimation::_calculateFrameInfo()
{
}

_X2D_NS_END