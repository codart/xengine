#include "StdAfx.h"
#include "ParallelAnimation.h"

#include "x2d/event/AnimationEvent.h"

_X2D_NS_BEGIN

ParallelAnimation::ParallelAnimation(void)
{
	_nEndedChildCount = 0;
}

ParallelAnimation::~ParallelAnimation(void)
{
}

void ParallelAnimation::Play()
{
	switch (GetState())
	{
	case ANI_PLAYING:
		return;
		break;

	case ANI_PAUSED:
		Resume();
		break;

	case ANI_STOPPED:
		_nEndedChildCount = 0;
		_nLoopCounter = (_nLoopTimes == INFINITE)? 0:_nLoopTimes;
		TRAVERSE_CHILD_NODE_HEAD
			Animation* pAnimation = (Animation*)pCurNode;
			pAnimation->Play();
		TRAVERSE_CHILD_NODE_TAIL
		break;
	}

	_animationState = ANI_PLAYING;
}

void ParallelAnimation::Pause()
{
	TRAVERSE_CHILD_NODE_HEAD
		Animation* pAnimation = (Animation*)pCurNode;
		pAnimation->Pause();
	TRAVERSE_CHILD_NODE_TAIL

	_animationState = ANI_PAUSED;
}

void ParallelAnimation::Resume()
{
	TRAVERSE_CHILD_NODE_HEAD
		Animation* pAnimation = (Animation*)pCurNode;
		pAnimation->Resume();
	TRAVERSE_CHILD_NODE_TAIL

	_animationState = ANI_PLAYING;
}

void ParallelAnimation::Stop()
{
	if(GetState() == ANI_STOPPED) return;

	TRAVERSE_CHILD_NODE_HEAD
		Animation* pAnimation = (Animation*)pCurNode;
		pAnimation->Stop();
	TRAVERSE_CHILD_NODE_TAIL

	_animationState = ANI_STOPPED;
}

void ParallelAnimation::_onChildAnimationEnd(const Event* pEvent)
{
	_nEndedChildCount++;

	if(_nEndedChildCount == GetChildCount())
	{
		_nEndedChildCount = 0;
		_animationState = ANI_STOPPED;

		// All child animation is ended, dispatch end event for ParallelAnimation
		AnimationEvent event(AnimationEvent::ANIMATION_END);
		event.pSourceObj = this;
		DispatchEvent(event);
	}
}

_X2D_NS_END