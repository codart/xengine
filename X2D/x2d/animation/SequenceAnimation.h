#pragma once

#include "x2d/animation/CompositeAnimation.h"

_X2D_NS_BEGIN

class _X2DExport SequenceAnimation : public CompositeAnimation
{
public:
	SequenceAnimation(void);
	virtual ~SequenceAnimation(void);

public:
	override void 
		Play();

	override void 
		Pause();

	override void 
		Resume();

	override void 
		Stop();

protected:
	Animation*
		_pCurrentAnimation;

protected:
	override void
		_onChildAnimationEnd(const Event* pEvent);	
};

_X2D_NS_END