#include "StdAfx.h"
#include "Animation.h"

#include "x2d/event/AnimationEvent.h"

_X2D_NS_BEGIN

Animation::Animation()
{
	_animationState = ANI_STOPPED;

	_nLoopTimes = 1;
	_nLoopCounter = 1;
}

Animation::Animation(Animation& srcAnimation)
{
	_animationState = ANI_STOPPED;

	_nLoopTimes = srcAnimation._nLoopTimes;
	_nLoopCounter = srcAnimation._nLoopCounter;
}

Animation::~Animation(void)
{
}

void Animation::SetLoopTimes(int nLoopTimes)
{
	_nLoopTimes = nLoopTimes;
}

int Animation::GetLoopTimes() const
{
	return _nLoopTimes;
}

AnimationState Animation::GetState() const
{
	return _animationState;
}

void Animation::Play()
{
	XTHROW(NoImplementationException);
}

void Animation::Pause()
{
	XTHROW(NoImplementationException);
}

void Animation::Resume()
{
	XTHROW(NoImplementationException);
}

void Animation::Stop()
{
	XTHROW(NoImplementationException);
}

void Animation::CopyData(Animation* pDestAnimation)
{
	pDestAnimation->_nLoopTimes = _nLoopTimes;
	pDestAnimation->_nLoopCounter = _nLoopCounter;
}

_X2D_NS_END