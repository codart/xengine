#pragma once

#include "x2d/animation/TweenAnimation.h"

_X2D_NS_BEGIN

class _X2DExport RotateAnimation : public TweenAnimation
{
public:
	RotateAnimation(DisplayObject* pDestObj, BOOL bWeakReference = TRUE);
	RotateAnimation(RotateAnimation& srcAnimation);
	virtual ~RotateAnimation(void);

public:
	float 
		angleFrom;
	float
		angleTo;


protected:
	override void 
		_aniStep();
	override void 
		_resetObject();
	override void 
		_calculateFrameInfo();


protected:
	DisplayObject* 
		_destDisplayObj;

	float 
		_deltaAngle;

};

_X2D_NS_END