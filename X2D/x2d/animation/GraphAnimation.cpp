#include "StdAfx.h"
#include "GraphAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

float DefaultGraphFunction(float variable)
{
	return variable;
}

float Graph1(float variable)
{
	return variable*variable/10;
}

GraphAnimation::GraphAnimation(DisplayObject* pDestObj, BOOL bWeakReference)
								: TweenAnimation(bWeakReference)
{
	mode =  Y_EQUAL_FX;
	_destDisplayObj = pDestObj;
	if(!bWeakReference) pDestObj->AddRef();

	xFrom = pDestObj->GetX();
	yFrom = pDestObj->GetY();
	xTo = yTo = 0;

	_delta = 0;
	_pGraphFunc = DefaultGraphFunction;
}

GraphAnimation::GraphAnimation(GraphAnimation& srcAnimation)
								: TweenAnimation(srcAnimation)
{
	mode = srcAnimation.mode;

	_destDisplayObj = srcAnimation._destDisplayObj;

	xFrom = srcAnimation.xFrom;
	xTo = srcAnimation.xTo;
	yFrom = srcAnimation.yFrom;
	yTo = srcAnimation.yTo;

}

GraphAnimation::~GraphAnimation(void)
{
	if(!_bWeakReference && _destDisplayObj) _destDisplayObj->Release();
}

void GraphAnimation::SetGraphFunction(GraphFunc pGraphFunc)
{
	XASSERT(pGraphFunc);
	_pGraphFunc = pGraphFunc;
}

void GraphAnimation::_calculateFrameInfo()
{
	_nFrameCounts = _nDuration/_nFrameInterval;

	if(Y_EQUAL_FX == mode)
	{
		_delta = (xTo - xFrom)/_nFrameCounts;
	}
	else
	{
		_delta = (yTo - yFrom)/_nFrameCounts;
	}
}

void GraphAnimation::_aniStep()
{
	if(Y_EQUAL_FX == mode)
	{
		_destDisplayObj->SetPosition(_destDisplayObj->GetX() + _delta
			, _pGraphFunc(_destDisplayObj->GetX() + _delta));
	}
	else
	{
		_destDisplayObj->SetPosition(_pGraphFunc(_destDisplayObj->GetY() + _delta)
			, _destDisplayObj->GetY() + _delta);
	}
}

void GraphAnimation::_resetObject()
{
	_destDisplayObj->SetPosition(xFrom, yFrom);
}


_X2D_NS_END