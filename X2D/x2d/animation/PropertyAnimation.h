#pragma once

#include "x2d/core/Numeric.h"
#include "x2d/animation/TweenAnimation.h"

_X2D_NS_BEGIN


class _X2DExport AnimatableProperty : public Object
									, public ReferenceCounted
{
public:
	virtual void SetValue(Numeric fValue) = 0;
	virtual Numeric GetValue() = 0;
};


class _X2DExport PropertyAnimation : public TweenAnimation
{
public:
	PropertyAnimation(AnimatableProperty* pProperty, BOOL bWeakReference = TRUE);

	PropertyAnimation(PropertyAnimation& srcAnimation);

	virtual ~PropertyAnimation(void);


public:
	Numeric 
		valueFrom;
	Numeric
		valueTo;


protected:
	override void 
		_aniStep();

	override void 
		_resetObject();

	override void 
		_calculateFrameInfo();


protected:
	RefCountPtr<AnimatableProperty> 
		_pProperty;

	float
		_currentValue;
	float 
		_deltaValue;

};


_X2D_NS_END