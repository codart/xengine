#pragma once

#include "common/util/LinkedTreeNode.h"
#include "common/core/EventDispatcher.h"

_X2D_NS_BEGIN

enum AnimationState
{
	ANI_PLAYING,
	ANI_PAUSED,
	ANI_STOPPED,
};

class _X2DExport Animation : public LinkedTreeNode<DWORD>
							, public EventDispatcher
{
public:
	const static int DEFAULT_FRAME_INTERVAL = 200;
	const static int DEFAULT_DURATION = 1000;

public:
	Animation();
	Animation(Animation& srcAnimation);
	virtual ~Animation(void);

public:
	virtual void 
		Play();

	virtual void 
		Stop();

	virtual void 
		Pause();

	virtual void 
		Resume();

	/* 
		If 0, loop is disabled.
		Only effected at the next play.
	*/
	virtual void 
		SetLoopTimes(int nLoopTimes = INFINITE);

	virtual int 
		GetLoopTimes() const;

	virtual AnimationState
		GetState() const;


protected:
	void 
		CopyData(Animation* pDestAnimation);


protected:
	AnimationState
		_animationState;

	INT
		_nLoopTimes;
	INT
		_nLoopCounter;
};

_X2D_NS_END