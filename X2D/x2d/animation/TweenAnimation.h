#pragma once

#include "x2d/display/DisplayObject.h"
#include "x2d/animation/Animation.h"
#include "Common/time/Timer.h"

_X2D_NS_BEGIN

/*
	Base class for all tween effect animation. 
*/
class _X2DExport TweenAnimation : public Animation
{
public:
	TweenAnimation(BOOL bWeakReference);
	TweenAnimation(TweenAnimation& srcAnimation);
	virtual ~TweenAnimation(void);

public:
	override void 
		Play();

	override void 
		Pause();

	override void 
		Resume();

	override void 
		Stop();


public:
	virtual void 
		SetDuration(int nDuration);
	virtual int 
		GetDuration(int nDuration) const;

	virtual void
		SetFrameInterval(int interval);
	virtual int
		GetFrameInterval() const;


protected:
	virtual void 
		_aniStep();
	virtual void 
		_resetObject();
	virtual void
		_calculateFrameInfo();

	//-----
	virtual void 
		_onAniTimer(const Event* pEvent);
	virtual void 
		_onLoopAniTimer(const Event* pEvent);

	virtual void 
		_onAnisTimer(const Event* pEvent);
	virtual void 
		_onLoopAnisTimer(const Event* pEvent);

	virtual INLINE void 
		_resetAnimation();

	virtual INLINE void 
		_createAniTimer();


protected:
	final INLINE void 
		_stopAnimation();


protected:
	BOOL 
		_bWeakReference;
	BOOL
		_bSingleTarget;
	int
		_nDuration;

	int 
		_nCurFrame;
	int
		_nFrameCounts;
	int 
		_nFrameInterval;

	RefCountPtr<Timer>
		_aniTimer;
	DWORD 
		_aniTimerID;

};

_X2D_NS_END