#pragma once

#include "Animation.h"

_X2D_NS_BEGIN

/*
	The CompositeEffect class is the parent class for the Parallel and Sequence classes,
	X2D supports two methods to combine, or composite, animations: parallel and sequence. 
	When you combine multiple animations in parallel, the effects play at the same time. 
	When you combine multiple animations in sequence, one effect must complete before 
	the next effect starts.
*/
class _X2DExport CompositeAnimation : public Animation
{
public:
	CompositeAnimation(void);
	virtual ~CompositeAnimation(void);

public:
	//-------
	int 
		AddChild(Animation* pAnimation);

	BOOL 
		RemoveChild(Animation* pAnimation);

	void 
		RemoveAllChildren();

	int 
		GetChildCount() const;


protected:
	virtual void
		_onChildAnimationEnd(const Event* pEvent);


protected:
	int
		_nChildCount;

};

_X2D_NS_END