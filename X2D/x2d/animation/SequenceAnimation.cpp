#include "StdAfx.h"
#include "SequenceAnimation.h"

#include "x2d/event/AnimationEvent.h"

_X2D_NS_BEGIN

SequenceAnimation::SequenceAnimation(void)
{
	_pCurrentAnimation = NULL;
}

SequenceAnimation::~SequenceAnimation(void)
{
}

void SequenceAnimation::Play()
{
	switch (GetState())
	{
	case ANI_PLAYING:
		return;
		break;

	case ANI_PAUSED:
		Resume();
		break;

	case ANI_STOPPED:
		if(!(_pCurrentAnimation = (Animation*)pFirstChild)) return;
		_nLoopCounter = (_nLoopTimes == INFINITE)? 0:_nLoopTimes;
		_pCurrentAnimation->Play();
		break;
	}

	_animationState = ANI_PLAYING;
}

void SequenceAnimation::Pause()
{
	if(_pCurrentAnimation)
	{
		_pCurrentAnimation->Pause();
		_animationState = ANI_PAUSED;
	}
}

void SequenceAnimation::Resume()
{
	if(_pCurrentAnimation)
	{
		_pCurrentAnimation->Resume();
		_animationState = ANI_PLAYING;
	}
}

void SequenceAnimation::Stop()
{
	if(_pCurrentAnimation)
	{
		_pCurrentAnimation->Stop();
		_pCurrentAnimation = NULL;
	}

	_animationState = ANI_STOPPED;
}


void SequenceAnimation::_onChildAnimationEnd(const Event* pEvent)
{
	if(_pCurrentAnimation = (Animation*)_pCurrentAnimation->pNextSibling)
	{
		_pCurrentAnimation->Play();
		return;
	}

	if(_nLoopTimes == INFINITE)
	{
		_animationState = ANI_STOPPED;
		Play();
	}
	else
	{
		_nLoopCounter--;
		XASSERT(_nLoopCounter >= 0);
		if(_nLoopCounter == 0)
		{
			_animationState = ANI_STOPPED;

			AnimationEvent event(AnimationEvent::ANIMATION_END);
			event.pSourceObj = this;
			DispatchEvent(event);
		}
	}

	return;
}

_X2D_NS_END