#pragma once

#include "x2d/animation/TweenAnimation.h"
#include "x2d/display/sprite.h"

_X2D_NS_BEGIN

/*
	Used to animate a movie clip. 
	
	Note: If animates multi-movieclips, all the movieclips should have same frame count.
*/
class _X2DExport FrameAnimation : public TweenAnimation
{
public:
	FrameAnimation(Sprite* pDestObject, BOOL bWeakReference = TRUE);
	FrameAnimation(vector<Sprite*>& vecMovieClips, BOOL bWeakReference = TRUE);
	FrameAnimation(vector< RefCountPtr<Sprite> >& vecMovieClips, BOOL bWeakReference = TRUE);
	virtual ~FrameAnimation(void);

public:
	override void 
		Attach(Sprite* pDestObj);

	void 
		AppendMovieclips(vector<Sprite*>& vecMovieClips);
	void 
		AppendMovieclips(vector< RefCountPtr<Sprite> >& vecMovieClips);


	void
		SetStartFrame(int nFrameIndex);
	void
		SetEndFrame(int nFrameIndex);


protected:
	override void
		_resetObject();


protected:
	override void 
		_onAniTimer(const Event* pEvent);
	override void 
		_onLoopAniTimer(const Event* pEvent);

	override void 
		_onAnisTimer(const Event* pEvent);
	override void 
		_onLoopAnisTimer(const Event* pEvent);


protected:
	int 
		_nStartFrame;
	int 
		_nEndFrame;

	Sprite*
		_destSprite;

	typedef vector<Sprite*>::iterator VecAutoPtrIt;

	vector<Sprite*>
		_vecDestSprites;

};

_X2D_NS_END