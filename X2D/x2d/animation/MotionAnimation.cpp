#include "StdAfx.h"
#include "MotionAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

MotionAnimation::MotionAnimation(DisplayObject* pDestObj, BOOL bWeakReference)
								: TweenAnimation(bWeakReference)
{
	mode =  POINT_TO_POINT;
	_destDisplayObj = pDestObj;
	if(!bWeakReference) pDestObj->AddRef();

	xFrom = pDestObj->GetX();
	yFrom = pDestObj->GetY();
	xTo = yTo = 0;

	xSpeed = 0;
	ySpeed = 0;
}

MotionAnimation::MotionAnimation(MotionAnimation& srcAnimation)
								: TweenAnimation(srcAnimation)
{
	mode = srcAnimation.mode;

	_destDisplayObj = srcAnimation._destDisplayObj;
	xFrom = srcAnimation.xFrom;
	xTo = srcAnimation.xTo;
	yFrom = srcAnimation.yFrom;
	yTo = srcAnimation.yTo;

	xSpeed = srcAnimation.xSpeed;
	ySpeed = srcAnimation.ySpeed;
}

MotionAnimation::~MotionAnimation(void)
{
	if(!_bWeakReference && _destDisplayObj) _destDisplayObj->Release();
}

void MotionAnimation::_calculateFrameInfo()
{
	_nFrameCounts = _nDuration/_nFrameInterval;

	if(POINT_TO_POINT == mode)
	{
		xSpeed = (xTo - xFrom)/_nFrameCounts;
		ySpeed = (yTo - yFrom)/_nFrameCounts;
	}
}

void MotionAnimation::_aniStep()
{
	_destDisplayObj->SetPosition(_destDisplayObj->GetX() + xSpeed
								, _destDisplayObj->GetY() + ySpeed);	
}

void MotionAnimation::_resetObject()
{
	if(POINT_TO_POINT == mode) _destDisplayObj->SetPosition(xFrom, yFrom);
}

_X2D_NS_END