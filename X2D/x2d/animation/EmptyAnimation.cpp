#include "StdAfx.h"
#include "EmptyAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "x2d/core/Timer.h"

_X2D_NS_BEGIN

EmptyAnimation::EmptyAnimation(BOOL bWeakReference) : TweenAnimation(bWeakReference)
{
	
}

EmptyAnimation::~EmptyAnimation(void)
{
}

void EmptyAnimation::_resetAnimation()
{
	
}

void EmptyAnimation::_createAniTimer()
{
	XASSERT(_nDuration >= _nFrameInterval);
	_nFrameCounts = _nDuration/_nFrameInterval;
	_deltaAlpha = (alphaTo - alphaFrom)/_nFrameCounts;

	TweenAnimation::_createAniTimer();
}

void EmptyAnimation::_onAniTimer(Event* pEvent)
{
	XASSERT(_destDisplayObj);
	_destDisplayObj->SetAlpha(_destDisplayObj->GetAlpha() + (int)_deltaAlpha);

	XASSERT(_nCurFrame <= _nFrameCounts);
	_nCurFrame++;
	if(_nCurFrame == _nFrameCounts) _stopAnimation();
}

void EmptyAnimation::_onLoopAniTimer(Event* pEvent)
{
	XASSERT(_destDisplayObj);
	_destDisplayObj->SetAlpha(_destDisplayObj->GetAlpha() + (int)_deltaAlpha);
	_nCurFrame++;

	XASSERT(_nCurFrame <= _nFrameCounts);
	if(_nCurFrame < _nFrameCounts) return;

	XASSERT(_nLoopCounter >= 0);
	if(_nLoopTimes != INFINITE && --_nLoopCounter == 0)
	{
		_stopAnimation();
	}
	else
	{
		_nCurFrame = 0;
		_destDisplayObj->SetAlpha((int)alphaFrom);
	}
}


_X2D_NS_END