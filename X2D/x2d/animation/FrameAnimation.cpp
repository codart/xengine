#include "StdAfx.h"
#include "FrameAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

FrameAnimation::FrameAnimation(Sprite* pDestObject, BOOL bWeakReference) 
								: TweenAnimation(bWeakReference)
{
	_bSingleTarget = TRUE;
	_destSprite = pDestObject;
	if(!bWeakReference) pDestObject->AddRef();
	_nStartFrame = 0;
	_nEndFrame = _destSprite->GetFrameCount() - 1;
}

FrameAnimation::FrameAnimation(vector<Sprite*>& vecMovieClips, BOOL bWeakReference)
									: TweenAnimation(bWeakReference)
{
	_nFrameInterval = DEFAULT_FRAME_INTERVAL;
	_nStartFrame = 0;

	if( 0 == vecMovieClips.size()) return;

	_bSingleTarget = FALSE;
	_vecDestSprites.clear();

	VecAutoPtrIt it = vecMovieClips.begin();

	XASSERT(*it);
	_nEndFrame = (*it)->GetFrameCount() - 1;

#ifdef _DEBUG
	int nFrameCount = _nEndFrame + 1;
#endif // _DEBUG

	for ( ; it != vecMovieClips.end(); ++it)
	{
#ifdef _DEBUG
		// If animate multi-movieclips, every movieclip should has consistent frame count.
		int nTempFrameCount = (*it)->GetFrameCount();
		XASSERT(nTempFrameCount == nFrameCount);
#endif // _DEBUG
		_vecDestSprites.push_back(*it);
		if(!bWeakReference) (*it)->AddRef();
	}
}

FrameAnimation::FrameAnimation(vector< RefCountPtr<Sprite> >& vecMovieClips
							, BOOL bWeakReference) : TweenAnimation(bWeakReference)
{
	_destSprite = NULL;
	_nFrameInterval = DEFAULT_FRAME_INTERVAL;
	_nStartFrame = 0;

	if( 0 == vecMovieClips.size()) return;

	_bSingleTarget = FALSE;
	_vecDestSprites.clear();

	vector< RefCountPtr<Sprite> >::iterator it = vecMovieClips.begin();

	XASSERT(*it);
	_nEndFrame = (*it)->GetFrameCount() - 1;

#ifdef _DEBUG
	int nFrameCount = _nEndFrame + 1;
#endif // _DEBUG

	for ( ; it != vecMovieClips.end(); ++it)
	{
#ifdef _DEBUG
		// If animate multi-movieclips, every movieclip should has consistent frame count.
		int nTempFrameCount = (*it)->GetFrameCount();
		XASSERT(nTempFrameCount == nFrameCount);
#endif // _DEBUG
		_vecDestSprites.push_back(*it);
		if(!bWeakReference) (*it)->AddRef();
	}
}

FrameAnimation::~FrameAnimation(void)
{
	if(!_bWeakReference)
	{
		SAFE_RELEASE(_destSprite);

		VecAutoPtrIt it = _vecDestSprites.begin();
		for ( ; it != _vecDestSprites.end(); ++it)
		{
			(*it)->Release();
		}
	}
}

void FrameAnimation::SetStartFrame(int nFrameIndex)
{
	_nStartFrame = nFrameIndex;
}

void FrameAnimation::SetEndFrame(int nFrameIndex)
{
	_nEndFrame = nFrameIndex;
}

void FrameAnimation::_resetObject()
{
	if(_bSingleTarget) _destSprite->GotoFrame(_nStartFrame);
	else
	{
		VecAutoPtrIt it = _vecDestSprites.begin();
		for ( ; it != _vecDestSprites.end(); ++it)
		{
			(*it)->GotoFrame(_nStartFrame);
		}
	}
}

void FrameAnimation::Attach(Sprite* pDestObj)
{
	_destSprite = pDestObj;
}

void FrameAnimation::AppendMovieclips(vector<Sprite*>& vecMovieClips)
{
	VecAutoPtrIt it = vecMovieClips.begin();

	for ( ; it != vecMovieClips.end(); ++it)
	{
		_vecDestSprites.push_back(*it);
		if(!_bWeakReference) (*it)->AddRef();
	}
}

void FrameAnimation::AppendMovieclips(vector< RefCountPtr<Sprite> >& vecMovieClips)
{
	vector< RefCountPtr<Sprite> >::iterator it = vecMovieClips.begin();

	for ( ; it != vecMovieClips.end(); ++it)
	{
		_vecDestSprites.push_back(*it);
		if(!_bWeakReference) (*it)->AddRef();
	}
}

void FrameAnimation::_onAniTimer(const Event* pEvent)
{
	if(_destSprite->GetFrameIndex() == _nEndFrame)
	{
		_stopAnimation();
	}
	else _destSprite->GotoNextFrame();
}

void FrameAnimation::_onLoopAniTimer(const Event* pEvent)
{
	XASSERT(_destSprite->GetFrameIndex() <= _nEndFrame);
	if(_destSprite->GetFrameIndex() != _nEndFrame)
	{
		_destSprite->GotoNextFrame();
		return;
	}

	if(_nLoopTimes != INFINITE)
	{
		_nLoopCounter--;
		XASSERT(_nLoopCounter >=0);
		if(_nLoopCounter == 0)
		{
			_stopAnimation();
		}
		else _destSprite->GotoFrame(0);
	}
	else _destSprite->GotoFrame(0);
}

//---
void FrameAnimation::_onAnisTimer(const Event* pEvent)
{
	VecAutoPtrIt it = _vecDestSprites.begin();
	for ( ; it != _vecDestSprites.end(); ++it)
	{
		if((*it)->GetFrameIndex() == _nEndFrame)
		{
			_stopAnimation();
			return;
		}
		else (*it)->GotoNextFrame();
	}
}

void FrameAnimation::_onLoopAnisTimer(const Event* pEvent)
{
	VecAutoPtrIt it = _vecDestSprites.begin();
	for ( ; it != _vecDestSprites.end(); ++it)
	{
		if((*it)->GetFrameIndex() == _nEndFrame)
		{
			(*it)->GotoFrame(0);

			// For multi-ani, how to dispatch ANIMATION_STOPPED event? Each ani has seperate event?
			/*
			_nLoopCounter--;
			if(_nLoopCounter == 0)
			{
			AnimationEvent event(AnimationEvent::ANIMATION_STOPPED);
			event.pSourceObj = this;
			DispatchEvent(event);
			}
			*/
		}
		else (*it)->GotoNextFrame();
	}
	return;
}

_X2D_NS_END