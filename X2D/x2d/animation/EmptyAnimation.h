#pragma once

#include "x2d/display/DisplayObject.h"
#include "x2d/animation/Animation.h"

_X2D_NS_BEGIN

class Timer;

class _X2DExport EmptyAnimation : public Animation
{
public:
	EmptyAnimation(BOOL bWeakReference);
	virtual ~EmptyAnimation(void);

public:
	override void 
		Play();

	override void 
		Pause();

	override void 
		Resume();

	override void 
		Stop();


public:
	virtual void 
		SetDuration(int nDuration);
	virtual int 
		GetDuration(int nDuration) const;


protected:
	virtual void 
		_onAniTimer(Event* pEvent);

	virtual INLINE void 
		_resetAnimation();

	virtual INLINE void 
		_createAniTimer();


protected:
	final INLINE void 
		_stopAnimation();


protected:
	int
		_nDuration;
	int 
		_nFrameInterval;

	ObjectPtr<Timer>
		_aniTimer;
	DWORD 
		_aniTimerID;
};

_X2D_NS_END