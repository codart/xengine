#pragma once

#include "x2d/animation/TweenAnimation.h"

_X2D_NS_BEGIN

/*
	Note: Make sure the dest object's blendCode is BLEND_ALPHA, or else it will has no effect.
*/
class _X2DExport FadeAnimation : public TweenAnimation
{
public:
	FadeAnimation(DisplayObject* pDestObj, BOOL bWeakReference = TRUE);
	FadeAnimation(FadeAnimation& srcAnimation);
	virtual ~FadeAnimation(void);

public:
	float 
		alphaFrom;
	float
		alphaTo;


protected:
	override void 
		_aniStep();
	override void 
		_resetObject();
	override void 
		_calculateFrameInfo();


protected:
	DisplayObject* 
		_destDisplayObj;

	float 
		_deltaAlpha;
	float
		_fCurrentAlpha;

};

_X2D_NS_END