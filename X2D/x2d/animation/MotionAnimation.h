#pragma once

#include "x2d/animation/TweenAnimation.h"

_X2D_NS_BEGIN

enum MotionMode
{
	POINT_TO_POINT,		// With this mode, object move from (xFrom, yFrom) to (xTo, yTo)
	MOVE_BY_SPEED,		// Object' s positon is calculated by xSpeed and ySpeed each frame
};

class _X2DExport MotionAnimation : public TweenAnimation
{
public:
	MotionAnimation(DisplayObject* pDestObj, BOOL bWeakReference = TRUE);
	MotionAnimation(MotionAnimation& motionAnimation);
	virtual ~MotionAnimation(void);

public:
	MotionMode
		mode;

	// Valid only when _mode == POINT_TO_POINT
	float
		xFrom;
	float 
		xTo;
	float
		yFrom;
	float
		yTo;

	// Valid only when _mode == BY_XY_SPEED
	float
		xSpeed;
	float
		ySpeed;

protected:
	override void 
		_aniStep();
	override void 
		_resetObject();
	override void 
		_calculateFrameInfo();


protected:
	DisplayObject*
		_destDisplayObj;

};

_X2D_NS_END