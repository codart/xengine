#include "StdAfx.h"
#include "RotateAnimation.h"

#include "x2d/event/AnimationEvent.h"
#include "common/time/Timer.h"

_X2D_NS_BEGIN

RotateAnimation::RotateAnimation(DisplayObject* pDestObj, BOOL bWeakReference)
								: TweenAnimation(bWeakReference)
{
	_destDisplayObj = pDestObj;
	if(!_bWeakReference) pDestObj->AddRef();

	angleFrom = (float)pDestObj->GetRotation();
	angleTo = 0;

	_deltaAngle = 2;
}

RotateAnimation::~RotateAnimation(void)
{
	if(!_bWeakReference) _destDisplayObj->Release();
}

RotateAnimation::RotateAnimation(RotateAnimation& srcAnimation)
							: TweenAnimation(srcAnimation)
{
	_destDisplayObj = srcAnimation._destDisplayObj;
	angleTo = srcAnimation.angleTo;
	angleFrom = srcAnimation.angleFrom;
}

void RotateAnimation::_calculateFrameInfo()
{
	XASSERT(_nDuration >= _nFrameInterval);
	_nFrameCounts = _nDuration/_nFrameInterval;
	_deltaAngle = (angleTo - angleFrom)/_nFrameCounts;
}

void RotateAnimation::_aniStep()
{
	_destDisplayObj->SetRotation(_destDisplayObj->GetRotation() 
								+ _deltaAngle);
}

void RotateAnimation::_resetObject()
{
	_destDisplayObj->SetRotation(angleFrom);
}

_X2D_NS_END