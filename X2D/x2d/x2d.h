#pragma once

#include "x2dmacros.h"
#include "x2dheaders.h"

extern "C"
{
_X2D_NS_BEGIN
	_X2DExport X2DManager* GetX2DManager();
_X2D_NS_END
}
