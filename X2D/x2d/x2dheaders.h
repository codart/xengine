#pragma once

#include "map"
#include "vector"
#include "list"
#include "string"
#include "assert.h"

using namespace std;

#include "Common/core/NoImplementationException.h"
#include "Common/core/IncorrectAddressException.h"
#include "Common/debug/Tracer.h"
#include "common/file/ModuleUtil.h"

#include "x2d/colordefs.h"
#include "common/util/SharedPtr.h"
#include "X2DManager.h"

#include "x2d/display/stage.h"
#include "x2d/display/Bitmap.h"


