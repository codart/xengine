#pragma once

#include "common/core/Exception.h"

_X2D_NS_BEGIN

class EmptyStageException : public Exception
{
public:
	EmptyStageException(void) : Exception("Display object dosen't belong to any stage!!!")
	{
	};

};


_X2D_NS_END