#pragma once
#include "common/core/exception.h"

_X2D_NS_BEGIN

class _X2DExport LoadResFailedException : public Exception
{
public:
	LoadResFailedException(void) : Exception("Load resource failed!!!")
	{
	}
};


_X2D_NS_END