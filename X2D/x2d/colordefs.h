#pragma once

#define ARGB_24 3
#define ARGB_32 4
#define XRGB_32 5

#define KEY_COLOR RGB(255, 0, 255)

#define NO_KEY_COLOR 0x00000000
#define MAX_BMP_SIZE 400000000


#define COLOR_ARGB(a,r,g,b) \
	((COLOR)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define COLOR_XRGB(r,g,b)   COLOR_ARGB(0xff,r,g,b)

#define COLOR_FROM_ALPHA(__alpha) (BYTE)(__alpha)<<24
#define COLOR_TO_ALPHA(color) (int)color>>24

#define BLACK_COLOR COLOR_ARGB(0, 0, 0, 0)
