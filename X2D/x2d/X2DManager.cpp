#include "stdafx.h"
#include "X2DManager.h"

#include "x2d/Library.h"

#include "x2d/display/RenderWindow.h"
#include "display/_StageMgr.h"
#include "common/time/timerMgr.h"
#include "common/core/_SingletonMgr.h"
#include "x2d/Library.h"
#include "common/input/_InputManager.h"

// Resource managers


_X2D_NS_BEGIN

const float DEFAULT_FRAME_INTERVAL = 0.01f; // 10 Milliseconds

X2DManager::X2DManager(DWORD_PTR hModule)
{
	//_GraphicInterface = NULL;
	_hModule = hModule;
	_hResourceModule = NULL;
}

X2DManager::~X2DManager()
{
	
}

void X2DManager::Init(DWORD_PTR hResModuleHandle)
{
	if(!hResModuleHandle)
	{
		hResModuleHandle = (DWORD_PTR)::GetModuleHandle(NULL);
	}

	timeBeginPeriod(1);

	_SingletonMgr::Init();

	Tracer::Instance().Create(NULL);

	Stage::s_emptyStage = new Stage;

	DXStageRenderBase::_$InitRenderEngine(NULL);
	InputManager::Instance().Init(_hModule);

	//....
	//ModuleUtil::Init();
	_TimerMgr::Initialize();
	Library::Instance().Init();
}

void X2DManager::Uninit()
{
	Library::Instance().Uninit();
	_TimerMgr::Uninitialize();

	InputManager::Instance().UnInit();
	DXStageRenderBase::_$UninitRenderEngine();

	//...ModuleUtil::Uninit();

	delete Stage::s_emptyStage;

	Tracer::Instance().Destroy(); //..????
	_SingletonMgr::UnInit();

	timeEndPeriod(1);

	_hResourceModule = NULL;
}

void X2DManager::StartEngine()
{
	MSG msg;

	InputManager& inputManager = InputManager::Instance();
	_StageMgr& stageMgr = _StageMgr::Instance();

	for(;;)
	{
		// Update timer
		_TimerMgr::BeginFrame();
		
		_TimerMgr::DispatchTimerEvent();

		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{ 
			if (msg.message == WM_QUIT)	break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
			//continue;
		}

		// Update input manager
		inputManager.Tick();

		// Render all StageWindow
		stageMgr.UpdateAllStage();

		// Yield CPU time
#ifdef YIELD_CPU_TIME
		float fRenderTime = _TimerMgr::EndFrame();
		if(fRenderTime > DEFAULT_FRAME_INTERVAL) continue;
		Sleep((int)(1000 * (DEFAULT_FRAME_INTERVAL - fRenderTime)));
		_TimerMgr::_AdjustDeltaTime(DEFAULT_FRAME_INTERVAL);
#endif 
	}

}

DWORD_PTR X2DManager::GetModuleHandle()
{
	return _hModule;
}

void X2DManager::SetResModuleHandle(DWORD_PTR hResModule)
{
	_hResourceModule = hResModule;
}

DWORD_PTR X2DManager::GetResModuleHandle()
{
	return _hResourceModule;
}


/*

GraphicInterface* X2DAdvManager::GetGraphicInterface()
{

}

RenderWindow* X2DAdvManager::AddRenderWindow(const String& strWindowName)
{
	//RenderWindow* pRenderWindow = _GraphicInterface->CreateRenderWindow();
	
	return NULL;
}
*/

_X2D_NS_END