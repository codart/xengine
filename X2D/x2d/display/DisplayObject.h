#pragma once
#pragma warning(disable: 4275)

#include "x2d/render/DisplayData.h"
#include "common/core/EventDispatcher.h"
#include "common/math/Rect.h"

_X2D_NS_BEGIN


class _X2DExport DisplayObject : public LinkedTreeNode<DWORD>
								, public DisplayData
								, public EventDispatcher
{
	friend class DisplayObjectContainer;
	friend class SimpleButton;
	friend class Stage;
	friend class InteractiveObject;

public:
	DisplayObject();
	virtual ~DisplayObject(void);

public:
	//----
	final INLINE Stage*
		GetStage();

	final INLINE void 
		SetVisible(BOOL bVisible)
	{
		_visible = bVisible;

		_dirty();
	}

	final INLINE BOOL 
		GetVisible() const
	{
		return _visible;
	}

	final INLINE void
		SetAlpha(int alpha)
	{
		__SetAlpha(alpha);
	}

	// Set the color to be blend with texture color. The blend mode is multiply now.
	final INLINE void
		SetColor(int r, int g, int b)
	{
		__SetColor(r, g, b);
	}

	final INLINE void
		GetColor(int& r, int& g, int& b)
	{
		__GetColor(r, g, b);
	}

	final INLINE void 
		SetARGB(DWORD dwColor)
	{
		__SetARGB(dwColor);
	}

	final INLINE void
		SetAlphaEx(int alpha)
	{
		__SetAlpha(alpha);

		TRAVERSE_CHILD_NODE_HEAD
			((DisplayObject*)pCurNode)->SetAlphaEx(alpha);
		TRAVERSE_CHILD_NODE_TAIL	
	}

	/**
		Set the relative x position
	*/
	virtual INLINE void
		SetX(float newRelativeX)
	{
		_setAbsoluteX(newRelativeX - _x);
		_x = newRelativeX;
	}

	final INLINE float
		GetX() const
	{
		return _x;
	}

	/**
		Set the relative y position
	*/
	virtual INLINE void
		SetY(float newRelativeY)
	{
		_setAbsoluteY(newRelativeY - _y);
		_y = newRelativeY;
	}

	final INLINE float
		GetY() const
	{
		return _y;
	}

	final INLINE void
		SetZ(float newZ)
	{
		zOffset = newZ - __y;
		__SetZ(newZ);
	}

	final INLINE float
		GetZ() const
	{
		return __z;
	}

	final INLINE void
		SetHotX(float fHotX)
	{
		__SetHotX(fHotX);
	}

	final INLINE float
		GetHotX()
	{
		return __GetHotX();
	}

	final INLINE void 
		SetHotY(float fHotY)
	{
		__SetHotY(fHotY);
	}

	final INLINE float
		GetHotY()
	{
		return __GetHotY();
	}

	final INLINE void
		SetZOffset(float newZOffset)
	{
		zOffset = newZOffset;
		if(_bBindZToY) __SetZ(__y + newZOffset);
	}

	final INLINE void
		SetPosition(float newRelativeX, float newRelativeY)
	{
		_setAbsoluteXY(newRelativeX - _x, newRelativeY - _y);
		_x = newRelativeX;
		_y = newRelativeY;
	}

#ifdef X_GAME
	final INLINE void
		SetScaleX(float fScaleX)
	{
		__SetScaleX(fScaleX);
	}

	final INLINE void
		SetScaleY(float fScaleY)
	{
		__SetScaleY(fScaleY);
	}

	final INLINE void
		SetScale(float fScaleX, float fScaleY)
	{
		__SetScale(fScaleX, fScaleY);
	}

	final INLINE float
		GetScaleX()
	{
		return __GetScaleX();
	}

	final INLINE float
		GetScaleY()
	{
		return __GetScaleY();
	}

	final INLINE void
		SetRotation(float fRotation)
	{
		return __SetRotation(fRotation);
	}

	final INLINE float
		GetRotation()
	{
		return __GetRotation();
	}
#endif

	final INLINE void
		BindZToY(BOOL bBind)
	{
		_bBindZToY = bBind;
	}

	final INLINE BOOL 
		HitTestPoint(Point& pt, BOOL bShapeFlag = FALSE) const
	{
		if(!_visible) return FALSE;
		Rect rect;
		GetRect(&rect);
		if(rect.PtInRect(&pt)) return TRUE;
		return FALSE;
	}


protected:
	// The relative coordinates (to parent)
	float
		_x;	
	float
		_y;

	BOOL 
		_bBindZToY;
	// If _bBindZToY == TRUE, __z = __y + zOffset
	float
		zOffset;

	Stage*
		_pStage;
	BOOL
		_bManagedRenderData;


protected:
	final INLINE void 
		_$UpdateForAddToStage(float newAbsoluteX, float newAbsoluteY, Stage* pNewHostStage);

	final INLINE void 
		_$UpdateForRemoveFromStage();


protected:
	/*
		If a object is dirtied and its _pStage is not EMPTY_STAGE, it will be redrawn 
		in the next render procedure.
	*/
	final INLINE void
		_dirty();

	// update absolute x, also update the invalidate region
	INLINE void 
		_setAbsoluteX(float changeX);

	// update absolute y, also update the invalidate region
	INLINE void 
		_setAbsoluteY(float changeY);

	INLINE void 
		_setAbsoluteXY(float changeX, float changeY);

	final void 
		_recursiveHitTest(Point& pt, vector<DisplayObject*>& __vecHittedObjList);

	final INLINE void 
		_CopyData(DisplayObject* pDestObject);

};


_X2D_NS_END