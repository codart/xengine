#include "StdAfx.h"
#include "Bitmap.h"

#include "x2d/render/BitmapData.h"
#include "x2d/display/Stage.h"
#include "x2d/event/FrameEvent.h"
#include "x2d/event/DisplayListEvent.h"

_X2D_NS_BEGIN

Bitmap::Bitmap(void)
{
	objType |= OBJ_BITMAP_OBJECT;
	_fUScrollSpeed = 0.0f;
	_fVScrollSpeed = 0.0f;

	AddEventListener(DisplayListEvent::ADD_TO_STAGE
		, MFUNC(&Bitmap::_onAddToStage, this));
	AddEventListener(DisplayListEvent::REMOVE_FROM_STAGE
		, MFUNC(&Bitmap::_onRemoveFromStage, this));

	_bListenerFrameEvent = FALSE;
}

Bitmap::~Bitmap(void)
{
	RemoveEventListener(DisplayListEvent::ADD_TO_STAGE
		, MFUNC(&Bitmap::_onAddToStage, this));
	RemoveEventListener(DisplayListEvent::REMOVE_FROM_STAGE
		, MFUNC(&Bitmap::_onRemoveFromStage, this));
}

void Bitmap::_CopyData(Bitmap* pDestObject)
{
	Bitmap* pBmp = (Bitmap*)pDestObject;

	DisplayObject::_CopyData(pBmp);

	pBmp->objType = objType;
	pBmp->blendMode = blendMode;
	pBmp->SetAlpha(__alpha);

	pBmp->bitmapData = bitmapData;
	pBmp->_bManagedRenderData = FALSE;

	pBmp->__SetWidth(width);
	pBmp->__SetHeight(height);
	pBmp->SetZOffset(zOffset);
}

Bitmap::Bitmap(Bitmap& srcBitmap)
{
	srcBitmap._CopyData(this);
}

Bitmap& Bitmap::operator =(Bitmap& srcBitmap)
{
	srcBitmap._CopyData(this);
	return *this;
}

Bitmap* Bitmap::Clone(BOOL bUseReference, Object* pDestObject)
{
	Bitmap* pBmp = pDestObject? (Bitmap*)pDestObject : new Bitmap;

	_CopyData(pBmp);

	return pBmp;
}


ReferenceCounted* Bitmap::Clone(BOOL bUseReference)
{
	Bitmap* pBmp = new Bitmap;

	DisplayObject::_CopyData(pBmp);

	pBmp->objType = objType;
	pBmp->blendMode = blendMode;
	pBmp->SetAlpha(__alpha);

	pBmp->bitmapData = bitmapData;
	pBmp->pRenderData = pRenderData;
	pBmp->_bManagedRenderData = FALSE;

	pBmp->__SetWidth(width);
	pBmp->__SetHeight(height);
	pBmp->SetZOffset(zOffset);

	return pBmp;
}

void Bitmap::SetSize(int nWidth, int nHeight, int nTileU, int nTileV)
{
	width = (float)nWidth;
	height = (float)nHeight;

	_bPosChanged = TRUE;

	bitmapData->SetSize(nWidth, nHeight, nTileU, nTileV);
}

void Bitmap::SetUVScrollSpeed(float fUScrollSpeed, float fVScrollSpeed)
{
	if(0 == fUScrollSpeed && 0 == fVScrollSpeed)
	{
		if(GetStage() && _bListenerFrameEvent)
		{
			_pStage->RemoveEventListener(FrameEvent::FRAME_START, MFUNC(&Bitmap::_onUVScroll, this));
		}
		_bListenerFrameEvent = FALSE;
	}
	else
	{
		if(GetStage() && !_bListenerFrameEvent) 
		{
			_pStage->AddEventListener(FrameEvent::FRAME_START, MFUNC(&Bitmap::_onUVScroll, this));
		}
		_bListenerFrameEvent = TRUE;		
	}
	
	_fUScrollSpeed = fUScrollSpeed;
	_fVScrollSpeed = fVScrollSpeed;
}

void Bitmap::_onAddToStage(const Event* pEvent)
{
	if(_bListenerFrameEvent) 
	{
		_pStage->AddEventListener(FrameEvent::FRAME_START, MFUNC(&Bitmap::_onUVScroll, this));
	}
}

void Bitmap::_onRemoveFromStage(const Event* pEvent)
{
	if(_bListenerFrameEvent)
	{
		_pStage->RemoveEventListener(FrameEvent::FRAME_START, MFUNC(&Bitmap::_onUVScroll, this));
	}
}

void Bitmap::_onUVScroll(const Event* pEvent)
{
	static float s_fUOffset = 0;
	static float s_fVOffset = 0;
	s_fUOffset += _fUScrollSpeed;
	s_fVOffset += _fVScrollSpeed;
	bitmapData->SetUVOffset(s_fUOffset, s_fVOffset);
	_bTextureUVChanged = TRUE;
}

void Bitmap::SetBitmapData(BitmapData* pBmpData)
{
	bitmapData = pBmpData;
}

void Bitmap::LoadBitmap(int resID, COLORREF _keyColor)
{
	try
	{
		BitmapData tempBmpData;
		tempBmpData.LoadFromResource(resID, _keyColor);

		_createRenderData();
		bitmapData->Swap(&tempBmpData);
		_updateInternalData(_keyColor);
	}
	catch(Exception& e)
	{
		DbgMsgBoxA(e.what());
	}
}

void Bitmap::LoadBitmap(const String& strFileName, COLORREF _keyColor)
{
	try
	{
		BitmapData tempBmpData;
		tempBmpData.LoadFromBmpFile(strFileName.c_str(), _keyColor);

		_createRenderData();
		bitmapData->Swap(&tempBmpData);
		_updateInternalData(_keyColor);
	}
	catch(Exception& e)
	{
		DbgMsgBoxA(e.what());
	}
}

void Bitmap::_createRenderData()
{
	if(!_bManagedRenderData) 
	{
		_bManagedRenderData = TRUE;
		bitmapData = NULL;
	}

	if(	!bitmapData) bitmapData = new BitmapData;

	pRenderData = bitmapData;
}

void Bitmap::_updateInternalData(COLORREF _keyColor)
{
	__SetWidth((float)bitmapData->width);
	__SetHeight((float)bitmapData->height);

	if(_keyColor != NO_KEY_COLOR)
	{
		blendMode = BLEND_ALPHA;
	}
}

_X2D_NS_END