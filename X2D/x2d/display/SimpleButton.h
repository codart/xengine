#pragma once

#include "x2d/display/DisplayObjectContainer.h"
#include "x2d/display/StaticText.h"
#include "x2d/display/InteractiveObject.h"

_X2D_NS_BEGIN

class _X2DExport SimpleButton : public DisplayObject
							  , public InteractiveObject
							  , public DisplayObjectContainer
{
public:
	SimpleButton(void);
	virtual ~SimpleButton(void);

public:
	void SetText(LPCWSTR pszText);
	int SetUpState(DisplayObject* pNewState);
	int SetOverState(DisplayObject* pNewState);
	int SetDownState(DisplayObject* pNewState);

	void 
		SetUseHandCursor(BOOL bUseHandCurosr);
	BOOL 
		GetUseHandCursor();

public:
	/// override AddEventListener and RemoveEventListener
	override int AddEventListener(int nEventType
		, MemberFunctor functor)
	{
		return AddEventListenerEx(nEventType, functor);
	}

	override int RemoveEventListener(int nEventType
		, MemberFunctor functor)
	{
		return RemoveEventListenerEx(nEventType, functor);
	}


protected:
	enum BTN_STATE
	{
		STATE_UP,
		STATE_OVER,
		STATE_DOWN,
	};

	void _onMouseMove(const Event* pEvent); //.. Should remove this if window class's cursor is null.
	void _onMouseOver(const Event* pEvent);
	void _onMouseDown(const Event* pEvent);
	void _onMouseLeave(const Event* pEvent);
	void _onMouseUp(const Event* pEvent);

	void _modifyState(BTN_STATE state, DisplayObject*& pStateObj, DisplayObject* pNewState);


protected:
	RefCountPtr<StaticText>
		_btnText;

	DisplayObject* _pUpState;
	DisplayObject* _pOverState;
	DisplayObject* _pDownState;

	BTN_STATE
		_curState;
	BOOL 
		_bUseHandCursor;

};


_X2D_NS_END