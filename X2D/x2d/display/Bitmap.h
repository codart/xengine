#pragma once

#include "x2d/display/displayobject.h"
#include "x2d/render/BitmapData.h"

_X2D_NS_BEGIN

class _X2DExport Bitmap : public DisplayObject
{
public:
	Bitmap(void);
	virtual ~Bitmap(void);
	Bitmap(Bitmap& srcBitmap);
	Bitmap& operator =(Bitmap& srcBitmap);


public:
	override ReferenceCounted*
		Clone(BOOL bUseReference = TRUE);

	Bitmap*
		Clone(BOOL bUseReference, Object* pDestObject = NULL);

	void 
		_CopyData(Bitmap* pDestObject);


public:
	virtual void 
		LoadBitmap(int ID, COLORREF keyColor = NO_KEY_COLOR);

	virtual void
		LoadBitmap(const String& strFileName, COLORREF keyColor = NO_KEY_COLOR);

	virtual void 
		SetSize(int nWidth, int nHeight, int nTileU = 1, int nTileV = 1);

	virtual void 
		SetUVScrollSpeed(float fUScrollSpeed, float fVScrollSpeed);

	// For test
	void SetBitmapData(BitmapData* pBmpData);


public:
	SharedPtr<BitmapData>
		bitmapData;


protected:
	override void 
		_createRenderData();
	void 
		_releaseBmpData();
	void 
		_updateInternalData(COLORREF _keyColor);


	// event handlers
	void 
		_onUVScroll(const Event* pEvent);
	void 
		_onAddToStage(const Event* pEvent);
	void 
		_onRemoveFromStage(const Event* pEvent);

protected:
	float 
		_fUScrollSpeed;

	float
		_fVScrollSpeed;

	BOOL
		_bListenerFrameEvent;

};

_X2D_NS_END