#pragma once

#include "displayobject.h"

_X2D_NS_BEGIN

/*
	The InteractiveObject class is the base class for all display objects with 
	which the user can interact, using the mouse and keyboard. 

	You should not instantiate the InteractiveObject class directly.

	The InteractiveObject class itself does not include any APIs for rendering content 
	onscreen. For that reason, if you want create a custom subclass of the InteractiveObject 
	class, you will want to extend one of its subclasses that do have APIs for rendering 
	content onscreen, such as the Sprite, SimpleButton, TextField, or MovieClip class.
*/
class _X2DExport InteractiveObject //: public DisplayObject
{
	DECLARE_EVENT_RECEIVER(InteractiveObject);

	friend class DisplayObject;
	friend class DisplayObjectContainer;
	friend class Stage;

public:
	InteractiveObject(DisplayObject* pDelegant);
	virtual ~InteractiveObject(void);

public:
	int AddEventListenerEx(int nEventType
						, MemberFunctor functor);

	int RemoveEventListenerEx(int nEventType
							, MemberFunctor functor);


protected:
	DisplayObject*
		_pDelegant;

	map<int, EventBinder*>
		_aRawEventBinders;
	BOOL
		_bIsOver;

private:
	//--- Stage event handler
	void __OnEnterFrame(const Event* pEvent);
	void __OnKeyDown(const Event* pEvent);
	void __OnKeyUp(const Event* pEvent);
	void __OnMouseMove(const Event* pEvent);
	void __OnMouseLeave(int x, int y, DWORD virtualKey);
	void __OnLButtonDown(const Event* pEvent);
	void __OnLButtonUp(const Event* pEvent);

	void __onAddToStage(const Event* pEvent);
	void __onRemoveFromStage(const Event* pEvent);

	void __updateStageEvtHook();

private:
	Bool
		__bNeedListenMouseMove;
	Bool
		__bNeedListenerMouseDown;
	Bool
		__bNeedListenerMouseUp;
	Bool
		__bNeedListenerKeyDown;
	Bool
		__bNeedListenerKeyUp;
	Bool
		__bNeedListenerFrameEvt;

	Bool
		__bMouseMoveListened;
	Bool
		__bMouseDownListened;
	Bool
		__bMouseUpListened;
	Bool
		__bKeyDownListened;
	Bool
		__bKeyUpListened;
	Bool
		__bFrameEvtListened;

};

_X2D_NS_END