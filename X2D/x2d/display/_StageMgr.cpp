#include "StdAfx.h"
#include "_StageMgr.h"

_X2D_NS_BEGIN

_StageMgr::_StageMgr(void)
{
}

_StageMgr::~_StageMgr(void)
{
}

void _StageMgr::AddStage(Stage* pStage)
{
#ifdef _DEBUG
	VecStageIt it = find(_stageList.begin(), _stageList.end(), pStage);
	XASSERT(it == _stageList.end());
#endif // _DEBUG

	_stageList.push_back(pStage);
}

void _StageMgr::RemoveStage(Stage* pStage)
{
	VecStageIt it = _stageList.begin();
	for ( ; it != _stageList.end(); ++it)
	{
		XASSERT(*it);
		if((*it) == pStage)
		{
			_stageList.erase(it);
			return;
		}
	}
}


_X2D_NS_END