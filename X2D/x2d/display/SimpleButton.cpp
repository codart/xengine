#include "StdAfx.h"
#include "SimpleButton.h"

#include "x2d/display/stage.h"
#include "common/input/MouseEvent.h"

_X2D_NS_BEGIN

SimpleButton::SimpleButton(void) : DisplayObjectContainer(this), InteractiveObject(this)
{
	_pUpState = NULL;
	_pOverState = NULL;
	_pDownState = NULL;

	_curState = STATE_UP;
	_bUseHandCursor = TRUE;

	AddEventListener(MouseEvent::MOUSE_MOVE, 
		MFUNC(&SimpleButton::_onMouseMove, this));

	AddEventListener(MouseEvent::MOUSE_OVER,
		MFUNC(&SimpleButton::_onMouseOver, this));

	AddEventListener(MouseEvent::MOUSE_DOWN,
		MFUNC(&SimpleButton::_onMouseDown, this));

	AddEventListener(MouseEvent::MOUSE_LEAVE,
		MFUNC(&SimpleButton::_onMouseLeave, this));

	AddEventListener(MouseEvent::MOUSE_UP, 
		MFUNC(&SimpleButton::_onMouseUp, this));

}

SimpleButton::~SimpleButton(void)
{
	RemoveEventListener(MouseEvent::MOUSE_MOVE,
		MFUNC(&SimpleButton::_onMouseMove, this));

	RemoveEventListener(MouseEvent::MOUSE_OVER,
		MFUNC(&SimpleButton::_onMouseOver, this));

	RemoveEventListener(MouseEvent::MOUSE_DOWN,
		MFUNC(&SimpleButton::_onMouseDown, this));

	RemoveEventListener(MouseEvent::MOUSE_LEAVE,
		MFUNC(&SimpleButton::_onMouseLeave, this));

	RemoveEventListener(MouseEvent::MOUSE_UP, 
		MFUNC(&SimpleButton::_onMouseUp, this));
}

void SimpleButton::SetText(LPCWSTR pszText)
{
	if(!_btnText)
	{
		_btnText = new StaticText;
		AddChild(_btnText);
	}

	_btnText->SetText(pszText);
	_btnText->SetX((width - _btnText->width)/2);
	_btnText->SetY((height - _btnText->height)/2);
}

int SimpleButton::SetUpState(DisplayObject* pNewState)
{
	_modifyState(STATE_UP, _pUpState, pNewState);
	return 0;
}

int SimpleButton::SetOverState(DisplayObject* pNewState)
{
	_modifyState(STATE_OVER, _pOverState, pNewState);
	return 0;
}

int SimpleButton::SetDownState(DisplayObject* pNewState)
{
	_modifyState(STATE_DOWN, _pDownState, pNewState);
	return 0;
}

void SimpleButton::SetUseHandCursor(BOOL bUseHandCurosr)
{
	_bUseHandCursor = bUseHandCurosr;
}

BOOL SimpleButton::GetUseHandCursor()
{
	return _bUseHandCursor;
}

//---------------
void SimpleButton::_modifyState(BTN_STATE state, DisplayObject*& pStateObj, DisplayObject* pNewState)
{
	if(pStateObj && NULL == pNewState)
	{
		RemoveChild(pStateObj);
		pStateObj = NULL;
		return;
	}

	AddChild(pNewState);
	pStateObj = pNewState;

	__SetWidth(pStateObj->width);
	__SetHeight(pStateObj->height);

	if(_curState != state) pStateObj->SetVisible(FALSE);
}

void SimpleButton::_onMouseMove(const Event* pEvent)
{
	if(_bUseHandCursor)
	{
		::SetCursor(::LoadCursor(NULL, IDC_HAND));
	}
}

void SimpleButton::_onMouseOver(const Event* pEvent)
{
	if(_bUseHandCursor)
	{
		::SetCursor(::LoadCursor(NULL, IDC_HAND));
	}

	_curState = STATE_OVER;
	if(!_pOverState) return;

	if(_pUpState) _pUpState->SetVisible(FALSE);
	if (_pDownState) _pDownState->SetVisible(FALSE);

	if(_pOverState) _pOverState->SetVisible(TRUE);
	return;
}

void SimpleButton::_onMouseDown(const Event* pEvent)
{
	_curState = STATE_DOWN;

	if(_pUpState) _pUpState->SetVisible(FALSE);
	if (_pOverState) _pOverState->SetVisible(FALSE);

	if(_pDownState) _pDownState->SetVisible(TRUE);
	return;
}

void SimpleButton::_onMouseLeave(const Event* pEvent)
{
	if(_bUseHandCursor)
	{
		::SetCursor(::LoadCursor(NULL, IDC_ARROW));
	}
	_curState = STATE_UP;

	if(_pDownState) _pDownState->SetVisible(FALSE);
	if (_pOverState) _pOverState->SetVisible(FALSE);

	if(_pUpState) _pUpState->SetVisible(TRUE);
	return;
}

void SimpleButton::_onMouseUp(const Event* pEvent)
{
	_curState = STATE_OVER;

	if(_pDownState) _pDownState->SetVisible(FALSE);
	if (_pOverState) _pOverState->SetVisible(FALSE);

	if (_pOverState) _pOverState->SetVisible(TRUE);
	else if(_pUpState) _pUpState->SetVisible(TRUE);
	return;
}


_X2D_NS_END