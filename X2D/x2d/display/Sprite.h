#pragma once

#include "x2d/display/DisplayObjectContainer.h"
#include "x2d/render/BitmapArray.h"

_X2D_NS_BEGIN

/*
	Sprite is a two dimension array of BitmapData. 
*/
class _X2DExport Sprite : public DisplayObject
{
public:
	Sprite(void);
	virtual ~Sprite(void);

public:
	virtual int 
		LoadAction(const String& strFileName, int nBmpsPerAction, 
							COLORREF keyColor = NO_KEY_COLOR,
							BMPS_ALIGN bmpAlign = H_BMPS);

	virtual int
		LoadAction(int ID, int nCount, COLORREF keyColor = NO_KEY_COLOR,
							BMPS_ALIGN bmpAlign = H_BMPS);

	virtual int
		LoadActions(const String& strFileName, int nActionCount, int nBmpsPerAction, 
							COLORREF keyColor = NO_KEY_COLOR, 
							BMPS_ALIGN bmpAlign = H_BMPS);

	virtual int
		LoadActions(int ID, int nActionCount, int nBmpsPerAction, 
							COLORREF keyColor = NO_KEY_COLOR, 
							BMPS_ALIGN bmpAlign = H_BMPS);

	final INLINE int
		GetActionCount()
	{
		return (*_bitmapArray).size();
	}

	final INLINE int
		GetFrameCount(int iActionIndex)
	{
		XASSERT(iActionIndex < GetActionCount());
		return ((*_bitmapArray)[iActionIndex])->size();
	}

	final INLINE void 
		SetActionIndex(int actionIndex, int frameIndex = 0)
	{
		if(actionIndex == __currentActionIndex) return;

		__currentActionIndex = actionIndex;
		__currentAction = (*_bitmapArray)[actionIndex];

		GotoFrame(frameIndex);
	}

	final INLINE void 
		GotoFrame(int nIndex)
	{
		pRenderData = (*__currentAction)[nIndex];
		__currentFrameIndex = nIndex;

		_bTextureUVChanged = TRUE;
		_dirty();
	}

	final INLINE void
		GotoNextFrame()
	{
		__currentFrameIndex++;

		XASSERT(__currentFrameIndex < (int)__currentAction->size());

		pRenderData = (*__currentAction)[__currentFrameIndex];

		_bTextureUVChanged = TRUE;
		_dirty();
	}

	final INLINE int
		GetFrameIndex() const
	{
		return __currentFrameIndex;
	}

	final INLINE int
		GetFrameCount() const
	{
		XASSERT(__currentAction);
		return __currentAction->size();
	}

	//---
	override ReferenceCounted*
		Clone(BOOL bUseReference = FALSE);


protected:
	override void 
		_createRenderData();
	virtual void 
		_updateInternalData(COLORREF _keyColor);

	void 
		_goFirstActionFirstFrame();
	void 
		_calculateBmpSize(int nActionCount, int nBmpsPerAction, 
								int largeBmpWidth, int LargeBmpHeight, 
								BMPS_ALIGN bmpAlign = H_BMPS);
	void 
		_CopyData(Sprite* pDestMovieClip, BOOL bReferenceToResource);


protected:
	SharedPtr<BitmapArray>
		_bitmapArray;
		
//----
	Action*
		__currentAction;

	int 
		__currentActionIndex;
	int 
		__currentFrameIndex;
};


_X2D_NS_END