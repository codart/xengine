#pragma once

#include "x2d/display/bitmap.h"
#include "d3d9.h"

_X2D_NS_BEGIN


class _X2DExport DCObject : public Bitmap
{
public:
	DCObject(void);
	virtual ~DCObject(void);

public:
	virtual void
		LoadBitmap(LPCWSTR strFileName, COLORREF keyColor = NO_KEY_COLOR);

	virtual void 
		CreateEmptyCanvas(int width, int height
						, COLOR color = COLOR_XRGB(255, 255, 255));


	// BeginPaint will return a DC handle. Use this handle to do further paint work.
	INLINE virtual DWORD_PTR
		BeginPaint();
	INLINE virtual void 
		EndPaint();

	// The color is in GDI COLORREF format. Use RGB(r,g,b) macro to construct this color.
	INLINE virtual void
		Clear(COLORREF color);

protected:
	void 
		_createTexture(int width, int height);


protected:
	IDirect3DSurface9*
		_pTextureSurface;

private:
	HDC
		__hSurfaceDC;

};




_X2D_NS_END