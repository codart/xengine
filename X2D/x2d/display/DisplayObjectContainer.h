#pragma once

#include "DisplayObject.h"

_X2D_NS_BEGIN

class _X2DExport DisplayObjectContainer
{
public:
	DisplayObjectContainer(DisplayObject* pDelegant);
	virtual ~DisplayObjectContainer(void);

public:
	virtual int 
		AddChild(DisplayObject* pDisplayObj);

	virtual BOOL 
		RemoveChild(DisplayObject* pDisplayObj);

	virtual void 
		RemoveAllChildren();


protected:
	DisplayObject*
		_pDelegant;

};


_X2D_NS_END