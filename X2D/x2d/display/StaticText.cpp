#include "StdAfx.h"
#include "StaticText.h"

#include "x2d/render/BitmapData.h"

_X2D_NS_BEGIN


StaticText::StaticText(void)
{
	blendMode = BLEND_ALPHA;
}

StaticText::~StaticText(void)
{
}

void StaticText::SetText(LPCWSTR pszText)
{
	_text = pszText;

	if(!_pTextureSurface) CreateEmptyCanvas(16, 16); 

	pRenderData->renderType = RENDER_TEXT;

	SIZE size;
	CDCHandle tempDC((HDC)BeginPaint());
	tempDC.GetTextExtent(pszText, wcslen(pszText), &size);
	EndPaint();

	CreateEmptyCanvas(size.cx, size.cy);

	CDCHandle dc((HDC)BeginPaint());
	dc.SetBkColor(RGB(0, 0, 0));
	dc.SetTextColor(RGB(255, 255, 255));
	dc.DrawText(pszText, lstrlen(pszText)
				, Rect(0, 0, (int)width, (int)height)
				, DT_WORDBREAK);
	EndPaint();

}

_X2D_NS_END