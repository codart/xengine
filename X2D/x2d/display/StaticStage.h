#pragma once

#include "x2d/display/Stage.h"
#include "x2d/render/DXStaticRender.h"

_X2D_NS_BEGIN

class _X2DExport StaticStage : public Stage
{
	DECLARE_NON_COPYABLE(StaticStage)

public:
	StaticStage(void);
	virtual ~StaticStage(void);

protected:
	virtual IStageRender*
		createStageRender();

protected:
	virtual int 
		_onPaint(Rect* pDirtyRect);

};


_X2D_NS_END