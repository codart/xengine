#include "StdAfx.h"
#include "canvas.h"

#include "x2d/render/BitmapData.h"
#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

DCObject::DCObject(void)
{
	_pTextureSurface = NULL;
	__hSurfaceDC = NULL;
}

DCObject::~DCObject(void)
{
	SAFE_RELEASE(_pTextureSurface);
}

void DCObject::LoadBitmap(LPCWSTR strFileName, COLORREF keyColor)
{
	Bitmap::LoadBitmap(strFileName, keyColor);

	// Get surface from texture
	if(bitmapData && bitmapData->_$pTexture)
	{
		bitmapData->_$pTexture->GetSurfaceLevel(0, &_pTextureSurface);
	}
}

void DCObject::CreateEmptyCanvas(int width_, int height_, COLOR color)
{
	if(_pTextureSurface)
	{
		XASSERT(__hSurfaceDC == NULL);
		SAFE_RELEASE(_pTextureSurface);
	}

	_createTexture(width_, height_);

	// Clear canvas
	BeginPaint();
	Clear(0x00ffffff);
	EndPaint();
}

DWORD_PTR DCObject::BeginPaint()
{
	XASSERT(NULL == __hSurfaceDC);

	_pTextureSurface->GetDC(&__hSurfaceDC);
	return (DWORD_PTR)__hSurfaceDC;
}

void DCObject::EndPaint()
{
	XASSERT(__hSurfaceDC);
	_pTextureSurface->ReleaseDC(__hSurfaceDC);
	__hSurfaceDC = NULL;
}

void DCObject::Clear(COLORREF color)
{
	XASSERT(__hSurfaceDC);

	Rect rect(0, 0, (int)width, (int)height);

	CBrush brush;
	brush.CreateSolidBrush(color);
	::FillRect(__hSurfaceDC, rect, brush.m_hBrush);
}


void DCObject::_createTexture(int width, int height)
{
	try
	{
		BitmapData tempBmpData;

		// Note: GetDC only supports D3DFMT_R5G6B5, D3DFMT_X1R5G5B5, D3DFMT_R8G8B8, and D3DFMT_X8R8G8B8.
		tempBmpData.CreateBitmap(width, height, XRGB_32);

		_createRenderData();
		bitmapData->Swap(&tempBmpData);
		_updateInternalData(NO_KEY_COLOR);

		// Get surface from texture
		if(_pTextureSurface)
		{
			int nRefCount = _pTextureSurface->Release();			
		}
		bitmapData->_$pTexture->GetSurfaceLevel(0, &_pTextureSurface);

	}
	catch(LoadResFailedException& e)
	{
		DbgMsgBoxA(e.what());
	}
}

_X2D_NS_END