#pragma once

#include "InteractiveObject.h"
#include "x2d/display/DisplayObjectContainer.h"
#include "x2d/render/EmptyStageRender.h"
#include "common/core/NonCopyble.h"
#include "x2d/render/RenderGroup.h"
#include "common/input/Mouse.h"
#include "common/input/keyboard.h"


_X2D_NS_BEGIN

#define EMPTY_STAGE Stage::s_emptyStage

class RenderWindow;

class _X2DExport Stage : public DisplayObject
					   , public InteractiveObject 
					   , public DisplayObjectContainer
{
	DECLARE_NON_COPYABLE(Stage)

	friend class _StageMgr;
	friend class RenderWindow;
	friend class DisplayObject;
	friend class DisplayObjectContainer;
	friend class InteractiveObject;

public:
	Stage(void);
	virtual ~Stage(void);

	static Stage* s_emptyStage;

public:
	// Use XRGB() to set this color in DX render
	COLOR
		background;

public:
	void 
		SetMaskObject(DisplayObject* pMaskObject);
	void 
		SetLightMap(DisplayObject* pLightedBmp, DisplayObject* pLightMap);

	BOOL 
		CreateStageWindow(HWND hWndParent, const Rect& rect);
	void 
		DestroyStageWindow();

	void
		ShowWindow(int bShowCmd);

	virtual IStageRender*
		GetStageRender();

	INLINE int 
		AddEventListener(int nEventType, 
						MemberFunctor functor);

	INLINE int 
		RemoveEventListener(int nEventType, 
						MemberFunctor functor);

	INLINE float
		GetFPS(){return _fFPS;};

	Keyboard*
		GetKeyboard();
	Mouse*
		GetMouse();


protected:
	virtual void 
		onInit();
	virtual void 
		onUninit();

	virtual IStageRender*
		createStageRender();


protected:
	virtual void 
		_onStageWindowCreate(HWND hWnd);
	virtual void
		_onStageWindowDestroy();


	// Mouse event
	int 
		_onKeyDown(DWORD vKeyCode);
	int 
		_onKeyUp(DWORD vKeyCode);
	int 
		_onMouseMove(int x, int y, DWORD virtualKey);
	int 
		_onLButtonDown(int x, int y, DWORD KeyIndicator);
	int 
		_onLButtonUp(int x, int y, DWORD KeyIndicator);

	int 
		_onSize(int width, int height);

	virtual int 
		_onPaint(Rect* pDirtyRect);
	int 
		_update();

protected:
	void 
		_$AddRawEventsBinders(map<int, EventBinder*>& eventBinders);

	void 
		_$RemoveRawEventsBinders(map<int, EventBinder*>& eventBinders);

	void 
		_$ClearObjList(vector<DisplayObject*>& vecObjList);
	void 
		_$AddHittedObject(DisplayObject* pObject);


protected:
	RenderWindow*
		_pStageWindow;

	IStageRender*
		_$pStageRender;

	RenderGroup
		_renderGroup;

	Keyboard*
		_pKeyboard;
	Mouse*
		_pMouse;

	float 
		_fFPS;

	//.. Need add focusMgr to handle UI focus
	InteractiveObject* 
		focus;


private:
	typedef vector<DisplayObject*>::iterator DisplayObjectIt;

	vector<DisplayObject*>
		__vecHittedObjList;	


private:
	INLINE void
		__updateStatus();
	INLINE void
		__render();

private:
	DWORD 
		__nframeCount;   // The number of frames that have occurred.
	float 
		__fTimeElapsed;  // The time that has elapsed since last reset.


};


_X2D_NS_END