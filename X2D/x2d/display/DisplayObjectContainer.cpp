#include "StdAfx.h"
#include "DisplayObjectContainer.h"
#include "stage.h"

_X2D_NS_BEGIN

DisplayObjectContainer::DisplayObjectContainer(DisplayObject* pDelegant)
{
	XASSERT(_pDelegant);
	_pDelegant = pDelegant;
}

DisplayObjectContainer::~DisplayObjectContainer(void)
{
	// Remove all child
	RemoveAllChildren();
}

void DisplayObjectContainer::RemoveAllChildren()
{
	DisplayObject* pDisplayObj = (DisplayObject*) _pDelegant->pFirstChild;
	DisplayObject* pTempObj = NULL;
	while(pDisplayObj)
	{
		pTempObj = (DisplayObject*) pDisplayObj->pNextSibling;
		RemoveChild(pDisplayObj);
		pDisplayObj = pTempObj;
	}
}

int DisplayObjectContainer::AddChild(DisplayObject* pDisplayObj)
{
	XASSERT(pDisplayObj && pDisplayObj != _pDelegant);
	if(pDisplayObj->pParent == _pDelegant) return 0;

	pDisplayObj->_$UpdateForAddToStage(_pDelegant->__GetX() + pDisplayObj->_x
									, _pDelegant->__GetY() + pDisplayObj->_y, _pDelegant->_pStage);

	_pDelegant->LinkedTreeNode<DWORD>::AddChildNode(pDisplayObj);

	return 0;
}

BOOL DisplayObjectContainer::RemoveChild(DisplayObject* pDisplayObj)
{
	XASSERT(pDisplayObj && pDisplayObj != _pDelegant);

	// Add a ref to pDisplayObj, make sure it is not deleted by RemoveChildNode
	RefCountPtr<DisplayObject> pTempDisplayObj = pDisplayObj;

	_pDelegant->LinkedTreeNode<DWORD>::RemoveChildNode(pDisplayObj);

	pDisplayObj->_$UpdateForRemoveFromStage();

	return 0;
}


_X2D_NS_END