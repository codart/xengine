#pragma once
#include "interactiveobject.h"

_X2D_NS_BEGIN

class TextField : public DisplayObject
				, public InteractiveObject
{
public:
	TextField(void);
	virtual ~TextField(void);
};


_X2D_NS_END