#include "StdAfx.h"
#include "stage.h"

#include "algorithm"
#include "common/time/TimerMgr.h"
#include "x2d/display/RenderWindow.h"
#include "x2d/display/_StageMgr.h"
#include "x2d/render/DXRenderBase.h"
#include "common/input/_InputManager.h"

#include "common/input/Keyboard.h"
#include "common/input/Mouse.h"

#include "x2d/event/StageEvent.h"
#include "x2d/event/FrameEvent.h"
#include "common/input/KeyboardEvent.h"
#include "common/input/MouseEvent.h"

_X2D_NS_BEGIN

Stage* Stage::s_emptyStage = NULL;

Stage::Stage(void) : DisplayObjectContainer(this), InteractiveObject(this)
{
	objType |= OBJ_STAGE_OBJECT;
	_$pStageRender = EMPTY_RENDER;
	_pStage = this;
	focus = this;
	background = COLOR_XRGB(255, 255, 255);

	_pStageWindow = NULL;

	__nframeCount = 0;
	__fTimeElapsed = 0;

	// Input objects
	_pKeyboard = NULL;
	_pMouse = NULL;

	_renderGroup._pOpaqueDisplayTree = this;
	_renderGroup._pMaskObject = NULL;
	_renderGroup._pLightMap = NULL;
	_renderGroup._pLightedBmp = NULL;
}

Stage::~Stage(void)
{

}

void Stage::SetLightMap(DisplayObject* pLightedBmp, DisplayObject* pLightMap)
{
	_renderGroup._pLightedBmp = pLightedBmp;
	_renderGroup._pLightMap = pLightMap;
}

void Stage::SetMaskObject(DisplayObject* pMaskObject)
{
	_renderGroup._pMaskObject = pMaskObject;
}

Keyboard* Stage::GetKeyboard()
{
	if(!_pKeyboard)
	{
		_pKeyboard = (Keyboard*)InputManager::Instance().AddInputObject(INPUT_OBJ_KEYBOARD
																, (DWORD_PTR)_pStageWindow->m_hWnd);
	}
	return _pKeyboard;
}

Mouse* Stage::GetMouse()
{
	if(!_pMouse)
	{
		_pMouse = (Mouse*)InputManager::Instance().AddInputObject(INPUT_OBJ_MOSUE
																, (DWORD_PTR)_pStageWindow->m_hWnd);
	}
	return _pMouse;	
}


BOOL Stage::CreateStageWindow(HWND hWndParent, const Rect& rect)
{
	if(!_pStageWindow) _pStageWindow = new RenderWindow(this);
	
	AdjustWindowRect((RECT*)&rect, WS_OVERLAPPEDWINDOW, FALSE);
	_pStageWindow->Create(hWndParent, (RECT*)&rect);

	return true;
}

void Stage::DestroyStageWindow()
{
	XASSERT(_pStageWindow);

	if(_pStageWindow)
	{
		_pStageWindow->DestroyWindow();
		delete _pStageWindow;
		_pStageWindow = NULL;
	}
}

void Stage::ShowWindow(int bShowCmd)
{
	if(!_pStageWindow) return;

	_pStageWindow->ShowWindow(bShowCmd);
}

int Stage::AddEventListener(int nEventType, 
							MemberFunctor functor)
{
	EventDispatcher::AddEventListener(nEventType, functor);
	return 0;
}

int Stage::RemoveEventListener(int nEventType, 
							   MemberFunctor functor)
{
	EventDispatcher::RemoveEventListener(nEventType, functor);
	return 0;
}

IStageRender* Stage::GetStageRender()
{
	XASSERT(_$pStageRender);
	return _$pStageRender;
}

IStageRender* Stage::createStageRender()
{
	return new DXStageRenderBase;
}

void Stage::_onStageWindowCreate(HWND hWnd)
{
	if(_$pStageRender == EMPTY_RENDER)
	{
		_$pStageRender = createStageRender();
		_$pStageRender->SetHostWindow(hWnd);
		_StageMgr::Instance().AddStage(this);
	}

	StageEvent e(StageEvent::STAGE_CREATE);
	e.pSourceObj = this;
	DispatchEvent(e);

	onInit();
}

void Stage::_onStageWindowDestroy()
{
	onUninit();

	StageEvent e(StageEvent::STAGE_DESTROY);
	e.pSourceObj = this;
	DispatchEvent(e);

	XASSERT(_$pStageRender != EMPTY_RENDER);
	if(_$pStageRender && _$pStageRender != EMPTY_RENDER)
	{
		_$pStageRender->Destroy();
	}

	_$pStageRender = EMPTY_RENDER;

	if(_pKeyboard)
	{
		InputManager::Instance().RemoveInputObject((InputObject*)_pKeyboard);
	}
	if(_pMouse)
	{
		InputManager::Instance().RemoveInputObject((InputObject*)_pMouse);
	}
	_StageMgr::Instance().RemoveStage(this);
}

void Stage::onInit()
{

}

void Stage::onUninit()
{

}

int Stage::_onSize(int _width, int _height)
{
	width = (float)_width;
	height = (float)_height;

	_$pStageRender->SetRenderRectSize(_width, _height);

	StageEvent e(StageEvent::STAGE_SIZE);
	e.pSourceObj = this;
	DispatchEvent(e);

	return 0;
}

int Stage::_update()
{
	__updateStatus();
	__render();
	return 0;
}

void Stage::__updateStatus()
{
	// Calculate FPS 
	__nframeCount++;
	__fTimeElapsed += _TimerMgr::GetDeltaTime();

	if(__fTimeElapsed >= 1.0f)
	{
		_fFPS = (float)__nframeCount / __fTimeElapsed;

		__fTimeElapsed	= 0.0f;
		__nframeCount	= 0;
	}

}

int Stage::_onPaint(Rect* pDirtyRect)
{
	//.._$pStageRender->InvalidateRect(pDirtyRect);

	if(!_$pStageRender->BeginRender()) return -1;

	_$pStageRender->ClearBackground(background);
	//.._$pStageRender->RenderDisplayList(this);
	_$pStageRender->RenderStage(&_renderGroup);

	_$pStageRender->EndRender();

	return 0;
}

void Stage::__render()
{
	FrameEvent event(FrameEvent::FRAME_START);
	event.pSourceObj = this;
	DispatchEvent(event);
	
	if(_$pStageRender->BeginRender())
	{
		_$pStageRender->ClearBackground(background);
		_$pStageRender->RenderStage(&_renderGroup);
		_$pStageRender->EndRender();
	}

	event.nType = FrameEvent::FRAME_END;
	DispatchEvent(event);
}

int Stage::_onKeyDown(DWORD vKeyCode)
{
	// Dispatch key press event to focus object
	//..focus->__OnKeyDown(vKeyCode);

	// Dispatch key down event
	KeyboardEvent e(KeyboardEvent::KEY_DOWN);
	e.keyCode = (KeyCode)vKeyCode;
	e.pSourceObj = this;
	DispatchEvent(e);

	return 0;
}

int Stage::_onKeyUp(DWORD vKeyCode)
{
	//focus->__OnKeyUp(vKeyCode);

	KeyboardEvent e(KeyboardEvent::KEY_UP);
	e.keyCode = (KeyCode)vKeyCode;
	e.pSourceObj = this;
	DispatchEvent(e);

	return 0;
}

int Stage::_onMouseMove(int x, int y, DWORD virtualKey)
{
	vector<DisplayObject*>  vecOldHittedObjs = __vecHittedObjList;

	__vecHittedObjList.clear();

	MouseEvent e(MouseEvent::MOUSE_MOVE);
	e.mouseX = x; e.mouseY = y;
	e.pSourceObj = this;
	DispatchEvent(e);

	// Generate a mouse leave event
	if (vecOldHittedObjs.size()>0)
	{
		vector<DisplayObject*>::iterator it = vecOldHittedObjs.begin();
		for ( ; it != vecOldHittedObjs.end(); ++it)	
		{													
			ATLASSERT(*it);								
			if(std::find(__vecHittedObjList.begin(), __vecHittedObjList.end(), *it) 
				!= __vecHittedObjList.end()) continue;
			

			InteractiveObject* pInteractiveObj = DYNAMIC_CAST(*it, InteractiveObject*);
			pInteractiveObj->__OnMouseLeave(x, y, virtualKey);	
		}	
	}

	_$ClearObjList(vecOldHittedObjs);
	return 0;	
}

int Stage::_onLButtonDown(int x, int y, DWORD keyIndicator)
{
	MouseEvent e(MouseEvent::MOUSE_DOWN);
	e.mouseX = x; e.mouseY= y;
	e.pSourceObj = this;
	DispatchEvent(e);
	return 0;
}

int Stage::_onLButtonUp(int x, int y, DWORD keyIndicator)
{
	MouseEvent e(MouseEvent::MOUSE_UP);
	e.mouseX = x; e.mouseY = y;
	e.pSourceObj = this;
	DispatchEvent(e);
	return 0;
}

//////////////////////////////////////////////////////////////////////////
void Stage::_$AddHittedObject(DisplayObject* pObject)
{
	__vecHittedObjList.push_back(pObject);
	pObject->AddRef();
}

void Stage::_$ClearObjList(vector<DisplayObject*>& vecObjList)
{
	DisplayObjectIt it = vecObjList.begin();
	for ( ; it != vecObjList.end(); ++it)
	{
		(*it)->Release();
	}
	vecObjList.clear();
}

void Stage::_$AddRawEventsBinders(map<int, EventBinder*>& eventBinders)
{
	ItEventBinders it = eventBinders.begin();
	for ( ; it != eventBinders.end(); ++it)
	{
		EventBinder* pEventBinder = (*it).second;
		XASSERT(pEventBinder);

		int nEventType = pEventBinder->nEventType;

		MemberFunctorListIt itFunctor = pEventBinder->_aMemberFunctors.begin();
		for ( ; itFunctor != pEventBinder->_aMemberFunctors.end(); ++itFunctor)
		{
			// Bind to stage
			EventBinder* pBinder = GetBinderByEventType(nEventType, TRUE);

			pBinder->BindMemberFunction(&(*itFunctor), FALSE);
		}
	}
}

void Stage::_$RemoveRawEventsBinders(map<int, EventBinder*>& eventBinders)
{
	ItEventBinders it = eventBinders.begin();
	for ( ; it != eventBinders.end(); ++it)
	{
		EventBinder* pEventBinder = (*it).second;
		XASSERT(pEventBinder);

		int nEventType = pEventBinder->nEventType;

		MemberFunctorListIt itFunctor = pEventBinder->_aMemberFunctors.begin();
		for ( ; itFunctor != pEventBinder->_aMemberFunctors.end(); ++itFunctor)
		{
			EventBinder* pBinder = GetBinderByEventType(nEventType);
			
			if(pBinder) pBinder->UnbindMemberFunction(&(*itFunctor)
								, _curDispatchedEventType == nEventType);
		}
	}
}

_X2D_NS_END