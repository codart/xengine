#pragma once
#include "DisplayObject.h"

#include "x2d/display/Canvas.h"

_X2D_NS_BEGIN

class _X2DExport Font
{
public:
	Font(){};
	//virtual ~Font(){};

	void
		Destroy() { XDELETE this; }

protected:
private:
};

/*
	*. Alpha blended text
	*. Use canvas(DC) or D3DXFont to implement static text?
*/

/*
	Static text
*/
class _X2DExport StaticText : public DCObject
{
public:
	StaticText(void);
	virtual ~StaticText(void);

public:
	void SetText(LPCWSTR pszText);
		
protected:
	String
		_text;

	SharedPtr<Font>
		_font;


};


_X2D_NS_END