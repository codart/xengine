#include "StdAfx.h"
#include "DisplayObject.h"
#include "x2d/render/EmptyStageRender.h"
#include "x2d/display/stage.h"
#include "x2d/error/EmptyStageException.h"
#include "x2d/render/IStaticStageRender.h"
#include "x2d/event/DisplayListEvent.h"

_X2D_NS_BEGIN

DisplayObject::DisplayObject()
{
	_x = _y = 0;
	_bBindZToY = FALSE;
	zOffset = 0;

	objType |= OBJ_DISPLAY_OBJECT;

	_pStage = EMPTY_STAGE;
	_bManagedRenderData = FALSE;
}

DisplayObject::~DisplayObject(void)
{

}

Stage* DisplayObject::GetStage()
{
	if(_pStage == EMPTY_STAGE)
		return NULL;

	return _pStage;
}

void DisplayObject::_dirty()
{
#ifdef STATIC_RENDER
	if(!pRenderData) return;
	((IStaticStageRender*)_pStage->_$pStageRender)->InvalidateObject(this);
#endif // STATIC_RENDER
}

void DisplayObject::_CopyData(DisplayObject* pDestObject)
{
	//??? Need copy DisplayData??
	DisplayData::CopyData(pDestObject);

	pDestObject->_pStage = EMPTY_STAGE;
	pDestObject->_bBindZToY = _bBindZToY;
	pDestObject->_x = _x;
	pDestObject->_y = _y;
	pDestObject->zOffset = zOffset;
	//..pDestObject->_bManagedRenderData = _bManagedRenderData;	
}

void DisplayObject::_setAbsoluteX(float changeX)
{
#ifdef STATIC_RENDER
	if(!pRenderData)
	{
		_bAllDirtied = TRUE;
		Rect rcRect;
		GetRect(&rcRect);

		if(changeX > 0) rcRect.right += (int)changeX;
		else rcRect.left += (int)changeX;

		((IStaticStageRender*)_pStage->_$pStageRender)->InvalidateRect(&rcRect);
	}
#endif // STATIC_RENDER

	__SetX(__x + changeX);

	//Update children's x
	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_setAbsoluteX(changeX);
	TRAVERSE_CHILD_NODE_TAIL
};

void DisplayObject::_setAbsoluteY(float changeY)
{
#ifdef STATIC_RENDER
	if(!pRenderData)
	{
		_bAllDirtied = TRUE;
		Rect rcRect;
		GetRect(&rcRect);

		if(changeY > 0) rcRect.bottom += (int)changeY;
		else rcRect.top += (int)changeY;

		((IStaticStageRender*)_pStage->_$pStageRender)->InvalidateRect(&rcRect);
	}
#endif // STATIC_RENDER

	__SetY(__y + changeY);
	if(_bBindZToY) __SetZ(__y + zOffset);

	// Update children's y
	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_setAbsoluteY(changeY);
	TRAVERSE_CHILD_NODE_TAIL	
};

void DisplayObject::_setAbsoluteXY(float changeX, float changeY)
{
#ifdef STATIC_RENDER
	if(pRenderData)
	{
		_bAllDirtied = TRUE;
		Rect rcRect;
		GetRect(&rcRect);

		if(changeX > 0) rcRect.right += (int)changeX;
		else rcRect.left += (int)changeX;

		if(changeY > 0) rcRect.bottom += (int)changeY;
		else rcRect.top += (int)changeY;

		((IStaticStageRender*)_pStage->_$pStageRender)->InvalidateRect(&rcRect);
	}
#endif // STATIC_RENDER

	__SetX(__x + changeX);
	__SetY(__y + changeY);
	if(_bBindZToY) __SetZ(__y + zOffset);

	//Update children's x
	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_setAbsoluteXY(changeX, changeY);
	TRAVERSE_CHILD_NODE_TAIL
};

void DisplayObject::_$UpdateForAddToStage(float newAbsoluteX, float newAbsoluteY, Stage* pNewStage)
{
	XASSERT(pNewStage);

	//Dirty the object on the old stage
	_dirty();

	__SetX(newAbsoluteX);
	__SetY(newAbsoluteY);
	if(_bBindZToY) __SetZ(__y + zOffset);


	// Change _pStage to new stage and dispatch remove/add events
	if(_pStage != EMPTY_STAGE)
	{
		DisplayListEvent event(DisplayListEvent::REMOVE_FROM_STAGE);
		event.pSourceObj = this;
		DispatchEvent(event);
	}

	Stage* pOldStage = _pStage;
	_pStage = pNewStage;

	if(pNewStage != EMPTY_STAGE && pNewStage != pOldStage)
	{
		DisplayListEvent event(DisplayListEvent::ADD_TO_STAGE);
		event.pSourceObj = this;
		DispatchEvent(event);
	}

	// Dirty the object on the new stage
	_dirty();

	// Enum all children
	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_$UpdateForAddToStage(__GetX() + ((DisplayObject*)pCurNode)->_x
										, __GetY() + ((DisplayObject*)pCurNode)->_y, pNewStage);
	TRAVERSE_CHILD_NODE_TAIL
}

void DisplayObject::_$UpdateForRemoveFromStage()
{
	if(pRenderData) _dirty();

	if(objType & OBJ_INTERACTIVE_OBJECT)
	{
		InteractiveObject* pInteractiveObj = dynamic_cast<InteractiveObject*>(this);
		if(_pStage != EMPTY_STAGE) _pStage->_$RemoveRawEventsBinders(pInteractiveObj->_aRawEventBinders);
	}
	
	_pStage = EMPTY_STAGE;

	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_$UpdateForRemoveFromStage();
	TRAVERSE_CHILD_NODE_TAIL
}

void DisplayObject::_recursiveHitTest(Point& pt
								, vector<DisplayObject*>& __vecHittedObjList)
{
	if (HitTestPoint(pt))
	{
		__vecHittedObjList.push_back(this);
	}

	TRAVERSE_CHILD_NODE_HEAD
		((DisplayObject*)pCurNode)->_recursiveHitTest(pt, __vecHittedObjList);
	TRAVERSE_CHILD_NODE_TAIL
}

_X2D_NS_END