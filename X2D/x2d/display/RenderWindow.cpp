// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RenderWindow.h"

#include "x2d/display/stage.h"

_X2D_NS_BEGIN

RenderWindow::RenderWindow(Stage* pStage)
{
	_pStage = pStage;
}

RenderWindow::~RenderWindow()
{
}

LRESULT RenderWindow::_onCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CRect clientRect;
	GetClientRect(&clientRect);

	_pStage->__SetWidth((float)clientRect.Width());
	_pStage->__SetHeight((float)clientRect.Height());
	_pStage->_onStageWindowCreate(m_hWnd);


	// center the dialog on the screen
	CenterWindow();

	return 0;
}

LRESULT RenderWindow::_onDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_pStage->_onStageWindowDestroy();
	return 0;
}

LRESULT RenderWindow::_onPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	// Need the paint dirty rect only.
	PAINTSTRUCT ps;
	HDC tempDC = ::BeginPaint(m_hWnd, &ps);

	::EndPaint(m_hWnd, &ps);

	_pStage->_onPaint((Rect*)&ps.rcPaint);

	return 0;
}

LRESULT RenderWindow::_onKeyDown(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onKeyDown(DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onKeyUp(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onKeyUp(DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onMouseMove(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onMouseMove(LOWORD(lParam), HIWORD(lParam), DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onLButtonDown(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onLButtonDown(LOWORD(lParam), HIWORD(lParam), DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onLButtonDBClick(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onLButtonDown(LOWORD(lParam), HIWORD(lParam), DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onLButtonUp(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onLButtonUp(LOWORD(lParam), HIWORD(lParam), DWORD(wParam));
	return 0;
}

LRESULT RenderWindow::_onSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	_pStage->_onSize(LOWORD(lParam), HIWORD(lParam));
	return 0;
}

LRESULT RenderWindow::_onClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CloseDialog(0);
	return 0;
};

void RenderWindow::CloseDialog(int nVal)
{
	::PostQuitMessage(nVal);
}


_X2D_NS_END