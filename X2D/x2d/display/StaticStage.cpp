#include "StdAfx.h"
#include "x2d/display/StaticStage.h"

#include "algorithm"
#include "common/time/TimerMgr.h"

_X2D_NS_BEGIN

StaticStage::StaticStage(void)
{
#ifndef STATIC_RENDER
	// Should not use static stage if not compiled with STATIC_RENDER
	XTHROWEX("Should not use static stage if not compiled with STATIC_RENDER");
#endif // STATIC_RENDER
}

StaticStage::~StaticStage(void)
{

}

IStageRender* StaticStage::createStageRender()
{
	IStaticStageRender* pStaticRender = new DXStaticStageRender;
	return pStaticRender; // Or new SoftStaticRender
}

int StaticStage::_onPaint(Rect* pDirtyRect)
{
	((IStaticStageRender*)_$pStageRender)->InvalidateRect(pDirtyRect);

	Stage::_onPaint(pDirtyRect);
	return 0;
}


_X2D_NS_END