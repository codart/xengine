#include "StdAfx.h"
#include "Sprite.h"

#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

Sprite::Sprite(void)
{
	__currentAction = 0;
	__currentFrameIndex = 0;
	__currentActionIndex = 0;

	objType |= OBJ_MOVIECLIP_OBJECT;
}

Sprite::~Sprite(void)
{
}

ReferenceCounted* Sprite::Clone(BOOL bUseReference)
{
	Sprite* pMovieClip = new Sprite;

	_CopyData(pMovieClip, bUseReference);

	return pMovieClip;
}

void Sprite::_CopyData(Sprite* pDestMovieClip, BOOL bReferenceToResource)
{
	DisplayObject::_CopyData(pDestMovieClip);

	// Copy basic data
	pDestMovieClip->objType = objType;
	pDestMovieClip->__SetWidth(width);
	pDestMovieClip->__SetHeight(height);
	pDestMovieClip->SetZOffset(zOffset);

	pDestMovieClip->_bManagedRenderData = FALSE;
	pDestMovieClip->blendMode = blendMode;
	pDestMovieClip->SetAlpha(__alpha);

	pDestMovieClip->_bitmapArray = _bitmapArray;

	pDestMovieClip->_goFirstActionFirstFrame();

	return;
}

int Sprite::LoadAction(const String& strFileName, int nBmpsPerAction, 
									COLORREF _keyColor, BMPS_ALIGN bmpAlign)
{
	_createRenderData();

	_bitmapArray->LoadAction( strFileName.c_str(), nBmpsPerAction, _keyColor, bmpAlign);

	_updateInternalData(_keyColor);
	return 0;
}

int Sprite::LoadActions(const String& strFileName, int nActionCount, int nBmpsPerAction, 
										COLORREF _keyColor,  BMPS_ALIGN bmpAlign)
{
	_createRenderData();

	_bitmapArray->LoadActions(strFileName.c_str(), nActionCount, nBmpsPerAction, 
		_keyColor,  bmpAlign);

	_updateInternalData(_keyColor);

	return 0;
}

int Sprite::LoadAction(int ID, int nBmpsPerAction, COLORREF _keyColor, 
									BMPS_ALIGN bmpAlign)
{	
	_createRenderData();

	_bitmapArray->LoadAction(ID, nBmpsPerAction, _keyColor, bmpAlign);

	_updateInternalData(_keyColor);

	return 0;
}

int Sprite::LoadActions(int ID, int nActionCount, int nBmpsPerAction, 
										COLORREF _keyColor, BMPS_ALIGN bmpAlign)
{
	_createRenderData();

	_bitmapArray->LoadActions(ID, nActionCount, nBmpsPerAction, 
		_keyColor, bmpAlign);

	_updateInternalData(_keyColor);

	return 0;
}

void Sprite::_goFirstActionFirstFrame()
{
	XASSERT(_bitmapArray->size() > 0);
	XASSERT((*_bitmapArray)[0]->size() > 0);

	ActionIt ItFirst = (*(*_bitmapArray)[0]).begin();
	if(ItFirst != (*(*_bitmapArray)[0]).end())
	{
		pRenderData = *ItFirst;
	}

	__currentAction = (*_bitmapArray)[0];
}

void Sprite::_createRenderData()
{
	if(!_bManagedRenderData) 
	{
		_bManagedRenderData = TRUE;
		_bitmapArray = NULL;
	}

	if(	!_bitmapArray) _bitmapArray = new BitmapArray;
}

void Sprite::_updateInternalData(COLORREF _keyColor)
{
	__SetWidth(_bitmapArray->width);
	__SetHeight(_bitmapArray->height);

	if(_keyColor != NO_KEY_COLOR)
	{
		blendMode = BLEND_ALPHA;
	}

	//Let the pData pointer to the first frame
	_goFirstActionFirstFrame();
}


_X2D_NS_END