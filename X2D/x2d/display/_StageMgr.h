#pragma once

#include "Stage.h"

_X2D_NS_BEGIN

class _X2DExport _StageMgr : public Singleton<_StageMgr, AutoRelease>
{
public:
	_StageMgr(void);
	virtual ~_StageMgr(void);

public:
	INLINE void 
		AddStage(Stage* pStage);

	INLINE void 
		RemoveStage(Stage* pStage);

	INLINE void 
		UpdateAllStage()
	{
		VecStageIt it = _stageList.begin();
		for ( ; it != _stageList.end(); ++it)
		{
			XASSERT(*it);
			(*it)->_update();
		}
	}

protected:
	typedef vector<Stage*>::iterator VecStageIt;

	vector<Stage*> 
		_stageList;
};


_X2D_NS_END