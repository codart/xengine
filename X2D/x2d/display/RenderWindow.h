// StageWindow.h : interface of the StageWindow class
//
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "common/debug/Tracer.h"
#include "common/time/TimerMgr.h"

_X2D_NS_BEGIN

#define STAGE_WND_STYLE WS_MINIMIZEBOX | WS_MAXIMIZEBOX |	\
		WS_OVERLAPPED | WS_SYSMENU | WS_THICKFRAME
typedef CWinTraits<STAGE_WND_STYLE> CHostWndTraits;


class Stage;

class RenderWindow :  public CWindowImpl<RenderWindow, CWindow, CHostWndTraits>
							  , public CUpdateUI<RenderWindow>
{
protected:
	Stage* 
		_pStage;


public:
	_X2DExport RenderWindow(Stage* pStage);
	_X2DExport virtual ~RenderWindow();


public:
	DECLARE_WND_CLASS_EX(L"Stage", CS_BYTEALIGNCLIENT | CS_OWNDC, -1)

	BEGIN_UPDATE_UI_MAP(RenderWindow)
	END_UPDATE_UI_MAP()

	BEGIN_MSG_MAP(RenderWindow)
		MESSAGE_HANDLER(WM_KEYDOWN, _onKeyDown)
		MESSAGE_HANDLER(WM_KEYUP, _onKeyUp)		
		MESSAGE_HANDLER(WM_MOUSEMOVE, _onMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, _onLButtonDown)
		MESSAGE_HANDLER(WM_LBUTTONDBLCLK, _onLButtonDBClick)
		MESSAGE_HANDLER(WM_LBUTTONUP, _onLButtonUp)
		MESSAGE_HANDLER(WM_PAINT, _onPaint)
		MESSAGE_HANDLER(WM_CLOSE, _onClose)
		MESSAGE_HANDLER(WM_SIZE, _onSize)
		MESSAGE_HANDLER(WM_CREATE, _onCreate)
		MESSAGE_HANDLER(WM_DESTROY, _onDestroy)
	END_MSG_MAP()

protected:
	LRESULT _onClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onKeyDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onKeyUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onLButtonDBClick(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT _onDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

protected:
	void CloseDialog(int nVal);
};


_X2D_NS_END