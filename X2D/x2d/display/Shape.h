#pragma once

#include "displayobject.h"

_X2D_NS_BEGIN

class Shape : public DisplayObject
{
public:
	Shape(void)
	{
		m_graphics = RGB(122, 122, 122);
	}
	virtual ~Shape(void);

public:
	COLORREF 
		m_graphics;
};


_X2D_NS_END