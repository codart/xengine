#include "StdAfx.h"
#include "InteractiveObject.h"

#include "x2d/display/stage.h"
#include "common/input/MouseEvent.h"
#include "common/input/KeyboardEvent.h"
#include "x2d/event/FrameEvent.h"
#include "x2d/event/DisplayListEvent.h"


_X2D_NS_BEGIN

InteractiveObject::InteractiveObject(DisplayObject* pDelegant)
{
	XASSERT(pDelegant);
	_pDelegant = pDelegant;
	_pDelegant->objType =  _pDelegant->objType | OBJ_INTERACTIVE_OBJECT;
	_bIsOver = FALSE;

	_pDelegant->AddEventListener(DisplayListEvent::ADD_TO_STAGE
					, MFUNC(&InteractiveObject::__onAddToStage, this));
	_pDelegant->AddEventListener(DisplayListEvent::REMOVE_FROM_STAGE
					, MFUNC(&InteractiveObject::__onRemoveFromStage, this));

	__bNeedListenMouseMove = false;
	__bNeedListenerMouseDown = false;
	__bNeedListenerMouseUp = false;
	__bNeedListenerKeyDown = false;
	__bNeedListenerKeyUp = false;
	__bNeedListenerFrameEvt = false;

	__bMouseMoveListened = false;
	__bMouseDownListened = false;
	__bMouseUpListened = false;
	__bKeyDownListened = false;
	__bKeyUpListened = false;
	__bFrameEvtListened = false;
}

InteractiveObject::~InteractiveObject(void)
{
	_pDelegant->RemoveEventListener(DisplayListEvent::ADD_TO_STAGE
		, MFUNC(&InteractiveObject::__onAddToStage, this));
	_pDelegant->RemoveEventListener(DisplayListEvent::REMOVE_FROM_STAGE
		, MFUNC(&InteractiveObject::__onRemoveFromStage, this));

	__bNeedListenMouseMove = false;
	__bNeedListenerMouseDown = false;
	__bNeedListenerMouseUp = false;
	__bNeedListenerKeyDown = false;
	__bNeedListenerKeyUp = false;
	__bNeedListenerFrameEvt = false;

	__updateStageEvtHook();
}

void InteractiveObject::__onAddToStage(const Event* pEvent)
{
	XASSERT(_pDelegant->_pStage);

	//???? objType can not have OBJ_INTERACTIVE_OBJECT bit????
	if(_pDelegant->objType & OBJ_INTERACTIVE_OBJECT)
	{
		__updateStageEvtHook();
		//..._pDelegant->_pStage->_$AddRawEventsBinders(_aRawEventBinders);
	}
}

void InteractiveObject::__updateStageEvtHook()
{
	if(_pDelegant->_pStage == Stage::s_emptyStage) return;

	if(__bNeedListenMouseMove && !__bMouseMoveListened)
	{
		__bMouseMoveListened = true;
		_pDelegant->_pStage->AddListener(MouseEvent::MOUSE_MOVE, __OnMouseMove);
	}
	else if(!__bNeedListenMouseMove && __bMouseMoveListened)
	{
		__bMouseMoveListened = false;
		_pDelegant->_pStage->RemoveListener(MouseEvent::MOUSE_MOVE, __OnMouseMove);
	}

	if(__bNeedListenerFrameEvt && !__bFrameEvtListened)
	{
		__bFrameEvtListened = true;
		_pDelegant->_pStage->AddListener(FrameEvent::FRAME_START, __OnEnterFrame);
	}
	else if(!__bNeedListenerFrameEvt && __bFrameEvtListened)
	{
		__bFrameEvtListened = false;
		_pDelegant->_pStage->RemoveListener(FrameEvent::FRAME_START, __OnEnterFrame);
	}

	if(__bNeedListenerMouseUp && !__bMouseUpListened)
	{
		__bMouseUpListened = true;
		_pDelegant->_pStage->AddListener(MouseEvent::MOUSE_UP, __OnLButtonUp);
	}
	else if(!__bNeedListenerMouseUp && __bMouseUpListened)
	{
		__bMouseUpListened = false;
		_pDelegant->_pStage->RemoveListener(MouseEvent::MOUSE_UP, __OnLButtonUp);
	}

	if(__bNeedListenerMouseDown && !__bMouseDownListened)
	{
		__bMouseDownListened = true;
		_pDelegant->_pStage->AddListener(MouseEvent::MOUSE_DOWN, __OnLButtonDown);
	}
	else if(!__bNeedListenerMouseDown && __bMouseDownListened)
	{
		__bMouseDownListened = false;
		_pDelegant->_pStage->RemoveListener(MouseEvent::MOUSE_DOWN, __OnLButtonDown);
	}

	if(__bNeedListenerKeyDown && !__bKeyDownListened)
	{
		__bKeyDownListened = true;
		_pDelegant->_pStage->AddListener(KeyboardEvent::KEY_DOWN, __OnKeyDown);
	}
	else if(!__bNeedListenerKeyDown && __bKeyDownListened)
	{
		__bKeyDownListened = false;
		_pDelegant->_pStage->RemoveListener(KeyboardEvent::KEY_DOWN, __OnKeyDown);
	}
	
	if(__bNeedListenerKeyUp && !__bKeyUpListened)
	{
		__bKeyUpListened = true;
		_pDelegant->_pStage->AddListener(KeyboardEvent::KEY_DOWN, __OnKeyUp);
	}
	else if(!__bNeedListenerKeyUp && __bKeyUpListened)
	{
		__bKeyUpListened = false;
		_pDelegant->_pStage->RemoveListener(KeyboardEvent::KEY_DOWN, __OnKeyUp);
	}
}

void InteractiveObject::__onRemoveFromStage(const Event* pEvent)
{
	XASSERT(_pDelegant->_pStage);
	
	if(_pDelegant->objType & OBJ_INTERACTIVE_OBJECT)
	{
		__updateStageEvtHook();
		//..._pDelegant->_pStage->_$RemoveRawEventsBinders(_aRawEventBinders);
	}
}

void InteractiveObject::__OnEnterFrame(const Event* pEvent)
{
	FrameEvent e(FrameEvent::FRAME_START);
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnKeyDown(const Event* pEvent)
{
	//... Need modify later: Only if current object has focus, then dispatch keyboard event
	KeyboardEvent e(KeyboardEvent::KEY_DOWN);
	e.keyCode = ((KeyboardEvent*)pEvent)->keyCode;
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnKeyUp(const Event* pEvent) 
{
	//... Need modify later: Only if current object has focus, then dispatch keyboard event
	KeyboardEvent e(KeyboardEvent::KEY_UP);
	e.keyCode = ((KeyboardEvent*)pEvent)->keyCode;
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnMouseMove(const Event* pEvent)
{
	MouseEvent* pMouseEvent = (MouseEvent*)pEvent;
	Point pt((int)pMouseEvent->mouseX, (int)pMouseEvent->mouseY);
	if(!_pDelegant->HitTestPoint(pt)) return;

	if(!_bIsOver)
	{
		MouseEvent e(MouseEvent::MOUSE_OVER);
		e.mouseX = (int)_pDelegant->GetX(); e.mouseY = (int)_pDelegant->GetY(); 
		e.pSourceObj = this;
		_pDelegant->DispatchEvent(e);
	}

	_bIsOver = TRUE;

	_pDelegant->_pStage->_$AddHittedObject(_pDelegant);

	MouseEvent e(MouseEvent::MOUSE_MOVE);
	e.mouseX = (int)_pDelegant->GetX(); e.mouseY = (int)_pDelegant->GetY();
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnMouseLeave(int x, int y, DWORD virtualKey)
{
	_bIsOver = FALSE;

	MouseEvent e(MouseEvent::MOUSE_LEAVE);
	e.mouseX = x; e.mouseY = y;
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnLButtonDown(const Event* pEvent)
{
	MouseEvent* pMouseEvent = (MouseEvent*)pEvent;
	Point pt((int)pMouseEvent->mouseX, (int)pMouseEvent->mouseY);
	if(!_pDelegant->HitTestPoint(pt)) return;

	MouseEvent e(MouseEvent::MOUSE_DOWN);
	e.mouseX = pt.x; e.mouseY = pt.y;
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

void InteractiveObject::__OnLButtonUp(const Event* pEvent)
{
	MouseEvent* pMouseEvent = (MouseEvent*)pEvent;
	Point pt((int)pMouseEvent->mouseX, (int)pMouseEvent->mouseY);
	if(!_pDelegant->HitTestPoint(pt)) return;

	MouseEvent e(MouseEvent::MOUSE_UP);
	e.mouseX = pt.x; e.mouseY = pt.y;
	e.pSourceObj = this;
	_pDelegant->DispatchEvent(e);
}

int InteractiveObject::AddEventListenerEx(int nEventType, MemberFunctor functor)
{
	Stage* pStage = _pDelegant->_pStage;
	// For stage originated event
	switch (nEventType)
	{
	case FrameEvent::FRAME_START:
		__bNeedListenerFrameEvt = true;
		break;

	case KeyboardEvent::KEY_DOWN:
		__bNeedListenerKeyDown = true;
		break;

	case MouseEvent::MOUSE_DOWN:
		__bNeedListenerMouseDown = true;
		break;

	case MouseEvent::MOUSE_UP:
		__bNeedListenerMouseUp = true;
		break;

	case MouseEvent::MOUSE_OVER:
	case MouseEvent::MOUSE_MOVE:
	case MouseEvent::MOUSE_LEAVE:
		__bNeedListenMouseMove = true;
		break;
	}

	__updateStageEvtHook();

	_pDelegant->EventDispatcher::AddEventListener(nEventType, functor);
	return 0;
}

int InteractiveObject::RemoveEventListenerEx(int nEventType, MemberFunctor functor)
{
	Stage* pStage = _pDelegant->_pStage;
	// For stage originated event
	switch (nEventType)
	{
	case FrameEvent::FRAME_START:
		__bNeedListenerFrameEvt = false;
		break;

	case KeyboardEvent::KEY_DOWN:
		__bNeedListenerKeyDown = false;
		break;

	case MouseEvent::MOUSE_DOWN:
		__bNeedListenerMouseDown = false;
		break;

	case MouseEvent::MOUSE_UP:
		__bNeedListenerMouseUp = false;
		break;

	case MouseEvent::MOUSE_OVER:
	case MouseEvent::MOUSE_MOVE:
	case MouseEvent::MOUSE_LEAVE:
		__bNeedListenMouseMove = false;
		break;
	}

	__updateStageEvtHook();

	_pDelegant->EventDispatcher::RemoveEventListener(nEventType, functor);
	return 0;
}

_X2D_NS_END