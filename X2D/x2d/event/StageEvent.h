#pragma once
#include "common/core/event.h"

_X2D_NS_BEGIN

class StageEvent : public Event
{
public:
	enum StageEventType
	{
		STAGE_EVENT_BASE = 1200,

		STAGE_SIZE,
		STAGE_CREATE,
		STAGE_DESTROY,
	};

	StageEvent(int nType):Event(nType)
	{

	}
};

_X2D_NS_END