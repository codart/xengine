#pragma once
#include "common/core/event.h"

_X2D_NS_BEGIN

class AnimationEvent : public Event
{
public:
	enum AnimationEventType
	{
		ANIMATION_EVENT_BASE = 400,

		ANIMATION_START,  // Only dispatched when Play() or Resume()
		ANIMATION_PAUSED,		
		ANIMATION_END,
	};

	AnimationEvent(int nType);
};

_X2D_NS_END