#pragma once
#include "common/core/event.h"

_X2D_NS_BEGIN

class DisplayListEvent : public Event
{
public:
	enum DisplayListEventType
	{
		DISPLAY_LIST_EVENT_BASE = 1300,

		ADD_TO_STAGE,
		REMOVE_FROM_STAGE,
	};

	DisplayListEvent(int nType):Event(nType)
	{

	}

};

_X2D_NS_END