#pragma once
#include "common/core/event.h"

_X2D_NS_BEGIN

class FrameEvent : public Event
{
public:
	enum FrameEventType
	{
		FRAME_EVENT_BASE = 300,

		FRAME_START,
		FRAME_END,
	};

	FrameEvent(int nType):Event(nType)
	{

	}
};

_X2D_NS_END