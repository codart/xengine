#pragma once

#include "d3d9.h"

_X2D_NS_BEGIN


/** Abstract class representing a Texture that can be used as render target.
    @remarks
 */
class X2DManager
{
public:
	X2DManager(DWORD_PTR hModule);
	virtual ~X2DManager();


public:
	virtual void 
		Init(DWORD_PTR hResModuleHandle = NULL);
	virtual void
		Uninit();
	virtual DWORD_PTR
		GetModuleHandle();
	/*
		Set the resource module handle. All resources that load from resource ID will use 
		load from this resource module. 
	*/
	virtual void
		SetResModuleHandle(DWORD_PTR hResModule);
	virtual DWORD_PTR
		GetResModuleHandle();

	virtual void 
		StartEngine();

	/*
	virtual GraphicInterface*
		GetGraphicInterface();

	virtual RenderWindow*
		AddRenderWindow(const String& strWindowName);
	*/

protected:
	//GraphicInterface*
	//	_GraphicInterface;
	DWORD_PTR
		_hModule;
	DWORD_PTR
		_hResourceModule;

};

_X2D_NS_END