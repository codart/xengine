#pragma once

#include "common/core/NonCopyble.h"
#include "display/DisplayObject.h"
#include "display/Bitmap.h"
#include "display/sprite.h"

_X2D_NS_BEGIN

class _X2DExport Library : public Singleton<Library, AutoRelease>
{
	DECLARE_NON_COPYABLE(Library)

public:
	void 
		Init(){};

	void 
		Uninit();

	/* User this method to load a XML file that contains the resource defines such 
	 as <Bitmap>, <MovieClip>
		
	The resource can be locate on network also. So we should define a LoadEvent to 
	notify client that the library has loaded.

	To edit the library, we should has a library/Resource editor
	 */
	BOOL
		LoadLibrary(LPCWSTR strFileName);

	int
		AddToLibrary(ReferenceCounted* pObject, LPCWSTR name, BOOL createNew = TRUE);

	BOOL 
		RemoveFromLibrary(LPCWSTR name);

	ReferenceCounted* 
		CreateObject(LPCWSTR name);

public:
	Library(void);
	virtual ~Library(void);

protected:
	typedef map<String, ReferenceCounted*>::iterator MapObjectIt;

	map<wstring, ReferenceCounted*>
		_objList;
};


_X2D_NS_END