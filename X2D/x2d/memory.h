#pragma once

void* alloc16(const unsigned int bytes);
void free16(void* ptr);

void memcpy_sse2(void* dest, const void* src, const unsigned long size_t);

void _fast_memcpy6(void* dst, void* src, int len);
void memcpyMMX(void* destination, const void* sorce, const unsigned long size_t);