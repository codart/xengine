// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"

#ifdef _DEBUG
	#include <stdlib.h>
	#include <crtdbg.h>
#endif // _DEBUG

#include <new.h>

#include "common/time/TimerMgr.h"
#include "x2d/x2d.h"

int X2DNewHandler (size_t size);

x2d::X2DManager* __pX2DManager = NULL;

bool APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	HRESULT hRes = 0;

	// This may cause an unexceptional crash if not run in VC with debug mode.
	// Comment it after the Alloc is useless
	//_CrtSetBreakAlloc(1346);

	HINSTANCE hInstRich = NULL;

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		__pX2DManager = new x2d::X2DManager((DWORD_PTR)hModule);

		_set_new_handler(X2DNewHandler);
		_set_new_mode(1);   // use NewHandler for malloc as well

		hInstRich = LoadLibrary(CRichEditCtrl::GetLibraryName()); 
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		delete __pX2DManager;
		FreeLibrary(hInstRich);
		//_CrtDumpMemoryLeaks();
		break;
	}
	return true;
}

int X2DNewHandler (size_t size) 
{
	/*
	if(common::PoolBase::IsAllocateForPool())
	{
		throw bad_alloc("Operator new couldn't allocate memory");
	}
	else
	{
		//?? Try to notify a up layer to free some unused memorys ( eg: ObjectPool::Shrink).

	}
	*/
	return 0;
}


extern "C"
{
_X2D_NS_BEGIN

	_X2DExport X2DManager* GetX2DManager()
	{
		return __pX2DManager;
	}

_X2D_NS_END
};

