#pragma once

_X2D_NS_BEGIN

template<typename T>
class PointT
{
public:
	PointT()
	{
		x = 0;
		y = 0;
	}

	PointT(T x, T y)
	{
		SetPoint(x, y);
	}

	PointT(const PointT<T>& point)
	{
		x = point.x;
		y = point.y;
	}

	~PointT(void){}

#ifdef _WINDOWS_
	operator LPPOINT ()
	{
		return NoImplementationException;
	}
#endif

	INLINE void SetPoint(T x, T y)
	{
		this->x = x;
		this->y = y;
	}

	INLINE void OffsetPoint(T x, T y)
	{
		this->x += x;
		this->y += y;
	}

	INLINE void Reset()
	{
		x = 0;
		y = 0;
	}

public:
	T	x;
	T	y;
};

#ifdef _WINDOWS_
template<> 
PointT<int>::operator LPPOINT()
{
	return (LPPOINT)this;
}
#endif

class _X2DExport Point : public PointT<int>
{
public:
	Point(){}
	Point(int x, int y) : PointT<int>(x, y) 
	{}

};

typedef PointT<float> PointF;

_X2D_NS_END