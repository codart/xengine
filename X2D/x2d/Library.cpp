#include "StdAfx.h"
#include "Library.h"

_X2D_NS_BEGIN

Library::Library(void)
{
}

Library::~Library(void)
{

}

void Library::Uninit()
{
	MapObjectIt it = _objList.begin();
	for ( ; it != _objList.end(); ++it)
	{
		XASSERT((*it).second != NULL);
		(*it).second->Release();
	}

	_objList.clear();
}

int Library::AddToLibrary(ReferenceCounted* pObject, LPCWSTR name, BOOL bCreateNew)
{
	XASSERT(pObject);

	ReferenceCounted* pLibObject = bCreateNew? pObject->Clone(TRUE) : pObject;

	_objList.insert(make_pair(name, pLibObject));
	pLibObject->AddRef();

	return 0;
}

ReferenceCounted* Library::CreateObject(LPCWSTR name)
{
	MapObjectIt it = _objList.find(name);

	XASSERT(it != _objList.end());
	if(it == _objList.end()) return NULL;

	ReferenceCounted* pNewObject = (*it).second->Clone();

	if(!pNewObject) XTHROWEX("Library: Error create new object");
	return pNewObject;
}

_X2D_NS_END