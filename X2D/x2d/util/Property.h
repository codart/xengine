#pragma once

_X2D_NS_BEGIN

template<typename HostClass, typename PropertyType>
class PropertyBase
{
public:
	PropertyBase()
	{
		_pHostClass = NULL;
		_propertySetFunc = NULL;
		_propertyGetFunc = NULL;
		_pData = NULL;
	}

protected:
	typedef void (HostClass::* PropertySetFunc)(PropertyType);
	typedef PropertyType (HostClass::* PropertyGetFunc)();

	HostClass* 
		_pHostClass;

	PropertySetFunc 
		_propertySetFunc;
	PropertyGetFunc 
		_propertyGetFunc;

	PropertyType* 
		_pData;

};

template<typename HostClass, typename PropertyType>
class Property : public PropertyBase<HostClass, PropertyType>
{
public:
	void bind(HostClass* pHost
		, PropertySetFunc propertySetFunc
		, PropertyGetFunc propertyGetFunc)
	{
		_pHostClass = pHost;
		_propertySetFunc = propertySetFunc;
		_propertyGetFunc = propertyGetFunc;
	}

	INLINE Property& operator=(PropertyType newData)
	{
		XASSERT(_pHostClass && _propertySetFunc);

		(_pHostClass->*_propertySetFunc)(newData);
		return *this;
	}

	INLINE operator PropertyType() const 
	{
		XASSERT(_pHostClass && _propertyGetFunc);

		return (_pHostClass->*_propertyGetFunc)(); 
	}

};


_X2D_NS_END