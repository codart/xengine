#pragma once

#include "Property.h"

_X2D_NS_BEGIN

template<typename HostClass, typename PropertyType>
class _X2DExport PropertyGet : public PropertyBase<HostClass, PropertyType>
{
public:
	void bind(HostClass* pHost
		, PropertyType* pData
		, PropertyGetFunc propertyGetFunc)
	{
		_pHostClass = pHost;
		_propertyGetFunc = propertyGetFunc;
		_pData = pData;
	}

	INLINE PropertyGet& operator=(PropertyType newData)
	{
		XASSERT(_pData);
		*_pData = newData;
		return *this;
	}

};


_X2D_NS_END