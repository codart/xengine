#pragma once

#include "Property.h"

_X2D_NS_BEGIN

template<typename HostClass, typename PropertyType>
class _X2DExport PropertySet : public PropertyBase<HostClass, PropertyType>
{
public:
	void bind(HostClass* pReceiver
		, PropertySetFunc propertySetFunc
		, PropertyType* pData)
	{
		_pHostClass = pReceiver;
		_propertySetFunc = propertySetFunc;
		_pData = pData;
	}

	INLINE PropertySet& operator=(PropertyType newData)
	{
		XASSERT(_pHostClass && _propertySetFunc);

		(_pHostClass->*_propertySetFunc)(newData);
		return *this;
	}

	INLINE operator PropertyType() const 
	{
		XASSERT(_pData);
		return *_pData; 
	}

};

_X2D_NS_END