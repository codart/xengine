#pragma once

#include "common/core/ObjectPool.h"

_X2D_NS_BEGIN

#include <list>
using namespace std;

// Unit test data
static int ConstData[] = {34, 23, 10, 77, 22, 45, 44, 33, 23, 56	\
, 87, 39, 65, 32, 90, 83, 12, 42, 85, 20
, 43, 23, 65,123, 643, 234, 12, 543, 234, 887
, 123, 543, 126, 874, 3, 26, 8, 43, 0, 110
, 34, 674, 1234, 7789, 4, 7, 980, 37, 92, 12};

template<typename UserType>
struct _ListNode
{
	UserType itemData;
	_ListNode* pNext;
};


template<typename UserType>
class List
{
public:
	class _ListNodePool : public Singleton<_ListNodePool, AutoRelease>
						, public ObjectPool<_ListNode<UserType>, 2000> {};


	List();
	~List();

	_ListNode<UserType>* 
		begin();

	void 
		push_back(UserType itemData);

	void 
		QuickSort();

	void 
		BubbleSort();

	void
		MergeSort();

protected:
	typedef _ListNode<UserType> NodeType;
	typedef _ListNode<UserType>* NodePtr;

	NodePtr
		_pHead;
	NodePtr
		_pTail;

	int
		_nCount;

protected:
	INLINE void 
		_partition(NodePtr pSmallListHead
					, NodePtr pStart
					, NodePtr pEnd);

	INLINE void _merge(_ListNode<UserType>* pStart1
						, _ListNode<UserType>* pEnd1
						, _ListNode<UserType>* pStart2
						, _ListNode<UserType>* pEnd2)
	{
		
	}
}; 

//--------------------------------
template<typename UserType>
List<UserType>::List()
{
	_nCount = 0;

	_pHead = new _ListNode<UserType>;
	//_pHead = _ListNodePool::Instance().GetInstance();
	_pHead->itemData = 0;
	_pHead->pNext = NULL;

	_pTail = _pHead;
}

template<typename UserType>
List<UserType>::~List()
{
	NodePtr pCurNode = _pHead->pNext;
	NodePtr pTmp = NULL;
	while (pCurNode)
	{
		pTmp = pCurNode->pNext;
		delete pCurNode;
		//_ListNodePool::Instance().FreeInstance(pCurNode);
		pCurNode = pTmp;
	}
	delete _pHead;
	//_ListNodePool::Instance().FreeInstance(pCurNode);
}

template<typename UserType>
_ListNode<UserType>* List<UserType>::begin()
{
	return _pHead;
}

template<typename UserType>
void List<UserType>::push_back(UserType itemData)
{
	NodePtr pNewItem = new NodeType;
	//NodePtr pNewItem = _ListNodePool::Instance().GetInstance();
	pNewItem->itemData = itemData;

	pNewItem->pNext = NULL;
	_pTail->pNext = pNewItem;
	_pTail = pNewItem;

	++_nCount;
}

template<typename UserType>
void List<UserType>::QuickSort()
{
	if(_nCount < 2) return;

	_partition(_pHead, _pHead->pNext, NULL);

	//Find the tail
	NodePtr pCurNode = _pHead->pNext;
	NodePtr pPreNode = NULL;
	while(pCurNode)
	{
		pPreNode = pCurNode;
		pCurNode = pCurNode->pNext;
	}

	_pTail = pPreNode;
}

template<typename UserType>
void List<UserType>::_partition(NodePtr pSmallListHead
					   , NodePtr pStart
					   , NodePtr pEnd)
{
	if(pStart == pEnd) return;

	NodePtr pPreNode = pStart;
	NodePtr pCurNode = pStart->pNext;

	UserType refValue = pStart->itemData;

	while(pCurNode != pEnd)
	{
		if(pCurNode->itemData < refValue)
		{
			// Swap node
			pPreNode->pNext = pCurNode->pNext;
			pCurNode->pNext = pSmallListHead->pNext;
			pSmallListHead->pNext = pCurNode;

			// Update cursor. The pPreNode is not change
			pCurNode = pPreNode->pNext;
		}
		else
		{
			// Move to next node
			pPreNode = pCurNode;
			pCurNode = pCurNode->pNext;
		}
	}

	// Sort small list
	_partition(pSmallListHead, pSmallListHead->pNext, pStart);

	// Sort big list
	_partition(pStart, pStart->pNext, pEnd);

}

template<typename UserType>
void List<UserType>::BubbleSort()
{
	if(_nCount < 2) return;

	NodePtr pPreNode = _pHead;
	NodePtr pCurNode = _pHead->pNext;
	NodePtr pNextNode = pCurNode->pNext;

	BOOL bHasSwaped = FALSE;

	for (int i=_nCount-1; i>0; i--)
	{
		pPreNode = _pHead;
		pCurNode = _pHead->pNext;
		pNextNode = pCurNode->pNext;

		bHasSwaped = FALSE;

		for (int j=0; j<i; ++j)
		{
			if(pCurNode->itemData > pNextNode->itemData)
			{
				pPreNode->pNext = pNextNode;
				pCurNode->pNext = pNextNode->pNext;
				pNextNode->pNext = pCurNode;

				pPreNode = pNextNode;
				pNextNode = pCurNode->pNext;

				bHasSwaped = TRUE;
			}
			else
			{
				pPreNode = pCurNode;
				pCurNode = pNextNode;
				pNextNode = pNextNode->pNext;
			}
		}

		// This can be optimized. The _nCount-1 should be implemented 
		// separately.
		if(i==_nCount-1) _pTail = pCurNode; 
		if(!bHasSwaped) break;
	}

}

_X2D_NS_END