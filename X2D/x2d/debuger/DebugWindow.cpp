// DebugWindow.cpp: implementation of the CDebugConsole class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DebugWindow.h"
#include "strsafe.h"

/////////////////////////////////////////////
///////////////////////// CWindowPrinter //////////
LRESULT CWindowPrinter::OnSysKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ShowPrintBoard(SW_HIDE);
	return 0;
}

LRESULT CWindowPrinter::OnRightButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
//	PrintVariablesValue();
	return 0;
}

LRESULT CWindowPrinter::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	::SetTimer(m_hWnd,111,500,NULL);
	return 0;
}

LRESULT CWindowPrinter::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
//	PrintVariablesValue();
	return 0;
}

LRESULT CWindowPrinter::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ShowWindow(SW_HIDE);
	return 0;
}

LRESULT CWindowPrinter::InitPrinter()
{
	RECT rect;
	rect.top = 0;
	rect.left = 0;
	rect.right =350;
	rect.bottom =600;
	
	Create(m_hWnd, rect, _T("Debug Console"),WS_OVERLAPPEDWINDOW );
	return 0;
}

LRESULT CWindowPrinter::PrintDebugText(int x, int y, LPCTSTR lpszString, int nCount)
{
	CWindowDC dc(m_hWnd);
	dc.TextOut(x, y, lpszString, nCount);
	return 0;
}

LRESULT CWindowPrinter::ShowPrintBoard(DWORD nCmdShow)
{
	return ShowWindow(nCmdShow);
}
//////////////////////////////////////////////////////////////////////
////////////////////////// CDebugConsole ///////////////
CDebugConsole::~CDebugConsole()
{
	
}

CDebugConsole::CDebugConsole(CDebugSink* pSinkedClient)
{
	InitConsole(pSinkedClient);
}

LRESULT CDebugConsole::ShowConsole()
{
	m_pDebugPrinter->ShowPrintBoard(SW_SHOW);
	return ACD_Error_Success;
}

VOID CALLBACK CDebugConsole::TimerProc(
						HWND hwnd,         // handle to window
						UINT uMsg,         // WM_TIMER message
						UINT_PTR idEvent,  // timer identifier
						DWORD dwTime       // current system time
						)
{
	CDebugConsole* pDebugConsole = (CDebugConsole*)idEvent;
	ATLASSERT(pDebugConsole);
	pDebugConsole->PrintVariablesValue();
	return;
}

void CDebugConsole::PrintVariablesValue()
{
	PrintString(NULL, NULL);

	for(int i=0; i< m_aSimpleDebugData.size(); i++)
	{
		CDebugValue* pDebugValue = (CDebugValue*)m_aSimpleDebugData[i];
		PrintString(pDebugValue->GetValueName(), pDebugValue->GetValueData());
	}

}

LRESULT CDebugConsole::InitConsole(CDebugSink* pSinkedClient)
{
	ATLASSERT(::IsWindow(pSinkedClient->GetParentWndHandle()));
	m_pSinkedClient = pSinkedClient;
	m_iYPos = 25;

	//You can specify a concrete printer here
	m_pDebugPrinter =dynamic_cast<CDebugPrinter *> ( new CWindowPrinter());
	m_pDebugPrinter->InitPrinter();

	SetTimer(WAGetMainFrame()->m_hWnd, (UINT)this, 500,(TIMERPROC)TimerProc);
	//Create hide window
	//Create(m_hWnd, rcDefault, _T("Debug Console"),WS_OVERLAPPEDWINDOW);

	return 0;
}

void CDebugConsole::PrintString(LPCTSTR szLable,LPCTSTR szString)
{
	if(szLable == NULL)
	{
		m_iYPos = 25;
		return;
	}
	
	TCHAR szValue[STRING_BUFFER_SIZE] = {0};
	StringCchPrintf(szValue, STRING_BUFFER_SIZE,_T("%s: %s"),szLable,szString);
	m_pDebugPrinter->PrintDebugText(5,m_iYPos,szValue,lstrlen(szValue));
	m_iYPos+=20;
}

LRESULT CDebugConsole::AddDebugValue(LPCTSTR szString, LPCTSTR szValueName)
{
	CDebugStringValue* pValue = new CDebugStringValue(szString, szValueName);
	m_aSimpleDebugData.push_back((CDebugValue*)pValue);
	return 0;
}

LRESULT CDebugConsole::AddDebugValue(int value, LPCTSTR szValueName)
{
	CDebugConstIntValue* pValue = new CDebugConstIntValue(value, szValueName);
	m_aSimpleDebugData.push_back((CDebugValue*)pValue);
	return 0;
}

LRESULT CDebugConsole::AddDebugValue(int* pInt, LPCTSTR szValueName)
{
	CDebugIntValue* pValue = new CDebugIntValue(pInt, szValueName);
	m_aSimpleDebugData.push_back((CDebugValue*)pValue);
	return 0;
}

LRESULT CDebugConsole::AddDebugValue(CMainFrame* pMainFrame, IntProc lpfnIntProc, LPCTSTR szValueName)
{
	CDebugIntRetValue* pValue = new CDebugIntRetValue(pMainFrame, lpfnIntProc, szValueName);
	m_aSimpleDebugData.push_back((CDebugValue*)pValue);
	return 0;
}

LRESULT CDebugConsole::AddDebugValue(DWORD* pDWORD, LPCTSTR szValueName)
{
	AddDebugValue((int*)(pDWORD),szValueName);
	return 0;
}

///////////////////////////////
/////// CDebugIntValue ////////
LPCTSTR	CDebugIntValue::GetValueData()
{
	StringCchPrintf(m_szValueData, sizeof(m_szValueData)/sizeof(m_szValueData[0]), _T("%d"), *m_pData);
	return m_szValueData;
}

LPCTSTR  CDebugIntValue::GetValueName()
{
	return m_szValueName;
}

///////////////////////////////
///////////////////////////////
LPCTSTR	CDebugConstIntValue::GetValueData()
{
	StringCchPrintf(m_szValueData, sizeof(m_szValueData)/sizeof(m_szValueData[0]), _T("%d"), m_pData);
	return m_szValueData;
}

LPCTSTR  CDebugConstIntValue::GetValueName()
{
	return m_szValueName;
}

///////////////////////////////
///////////////////////////////
CDebugIntRetValue::CDebugIntRetValue(CMainFrame* pMainFrame, IntProc lpfnIntProc , LPCTSTR szValueName)
{
	m_pMainFrame = pMainFrame;
	m_lpfnIntProc = lpfnIntProc;
	lstrcpyn(m_szValueName, szValueName, STRING_BUFFER_SIZE);
}

LPCTSTR CDebugIntRetValue::GetValueData()
{
	int intValue = (m_pMainFrame->*m_lpfnIntProc)();
	StringCchPrintf(m_szValueData, sizeof(m_szValueData)/sizeof(m_szValueData[0]), _T("%d"), intValue);
	return m_szValueData;
}

LPCTSTR CDebugIntRetValue::GetValueName()
{
	return m_szValueName;
}

///////////////////////////////
///////////////////////////////
LPCTSTR	CDebugStringValue::GetValueData()
{
	return m_szString;
}

LPCTSTR  CDebugStringValue::GetValueName()
{
	return m_szValueName;
}
