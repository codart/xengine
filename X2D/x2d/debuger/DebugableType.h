// DebugableType.h: interface for the DebugableType class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <assert.h>

using namespace std;

_X2D_NS_BEGIN

#define ENABLE_DEBUG_TYPE 
#define  UNKNOWN_FILE_PATH "Unknown file"

#ifdef ENABLE_DEBUG_TYPE
#define ASSIGN_VALUE(debugData, newData)	\
	debugData.SetCurrentDbgInfo(__FILE__, __LINE__);	\
	debugData = newData;

	//----
	#define ASSIGN_NAME(debugData, szName)	\
	debugData.SetName(szName);

	//----
	#define ADD_CHECK_VALUE(debugData, checkType, checkValue)	\
	debugData.AddCheckValue(checkType, checkValue);

#else

	#define ASSIGN_VALUE(debugData, newData)	\
	debugData = newData;

	//----
	#define ASSIGN_NAME(debugData, szName)

	//----
	#define ADD_CHECK_VALUE(debugData, checkType, checkValue)	\
	
#endif // ENABLE_DEBUG_TYPE

enum CheckType
{
	EQUAL_CHECK,
	GREATER_CHECK,
	LESS_CHECK,
};

class DebugableType
{
public:
	_X2DExport DebugableType()
	{
		_pszCurFile = UNKNOWN_FILE_PATH;
		_nCurLine = NULL;
	}
	_X2DExport virtual ~DebugableType() { };

public:
	_X2DExport void 
		SetCurrentDbgInfo(LPCSTR pszFile, int nLine);
	_X2DExport void 
		SetName(LPCSTR pszName);
	
	
protected:
	// Data name
	string
		_name;
	
	// Debug info
	LPCSTR 
		_pszCurFile;
	int 
		_nCurLine;
	
};

_X2D_NS_END
