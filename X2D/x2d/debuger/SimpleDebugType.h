// SimpleDebugType.h: interface for the SimpleDebugType class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "math.h"
#include "DebugableType.h"
#include "common/debug/Tracer.h"

_X2D_NS_BEGIN


class DebugablePtr
{
public:
protected:
private:
};

//////////////////////////////////////////////////////////////////////////
#ifdef ENABLE_DEBUG_TYPE
	#define DBYTE SimpleDebugType<char>
	#define DINT SimpleDebugType<int>
	#define DSHORT SimpleDebugType<short>
	#define DBOOL SimpleDebugType<BOOL>
	#define DFLOAT SimpleDebugType<float>
	#define DDOUBLE SimpleDebugType<double>
#else
	#define DBYTE char
	#define DINT int
	#define DSHORT short
	#define DBOOL BOOL
	#define DFLOAT float
	#define DDOUBLE double
#endif // ENABLE_DEBUG_TYPE

//////////////////////////////////////////////////////////////////////////

#define LOG_INT_EQUAL_CHECK "File: %s \nLine: %d \n%s changed from %d to %d"
#define LOG_FLOAT_EQUAL_CHECK "File: %s \nLine: %d \n%s changed from %f to %f"

#define FORMAT_BUFFER_SIZE 512

template<typename UserClass>
class SimpleDebugType: public DebugableType
{
public:
	void AddCheckValue(CheckType checkType, UserClass nCheckValue)
	{
		assert(_equalCheckList.end() == find(_equalCheckList.begin()
										, _equalCheckList.end()
										, nCheckValue));

		_equalCheckList.push_back(nCheckValue);
		
	}

	void RemoveCheckValue(CheckType checkType, UserClass nCheckValue)
	{
		vector<UserClass>::iterator it = find(_equalCheckList.begin()
									, _equalCheckList.end()
									, nCheckValue);
		
		if(it != _equalCheckList.end()) _equalCheckList.erase(it);
	}

    UserClass operator=(UserClass intData)
    {
		_equalCheck(intData);
        _data = intData;

		return _data;
    }
	
    operator UserClass()
    {
		return _data;
    }
	
	SimpleDebugType& operator++()
	{
		_equalCheck(_data + 1);
		_data++;
		return *this;
	}

	UserClass operator++(int)
	{
		_equalCheck(_data + 1);
		_data++;
		return _data;
	}

	SimpleDebugType& operator--()
	{
		_equalCheck(_data - 1);
		_data--;
		return *this;
	}

	UserClass operator--(int)
	{
		_equalCheck(_data - 1);
		_data--;
		return _data;
	}

protected:
    UserClass 
		_data;
    
	vector<UserClass>
		_equalCheckList;
	vector<UserClass>
		_greaterCheckList;
	vector<UserClass>
		_lessCheckList;
    

protected:
	void _equalCheck(UserClass checkValue)
	{
		for (int i=0; i<(int)_equalCheckList.size(); ++i)
		{
			if(_equalCheckList[i] == checkValue)
			{				
				char strBuffer[FORMAT_BUFFER_SIZE] = {0};
				sprintf_s(strBuffer, FORMAT_BUFFER_SIZE, LOG_INT_EQUAL_CHECK
					, _pszCurFile, _nCurLine, _name.c_str()
					, _data, checkValue);
				
				common::Tracer::Instance().WriteTrace(strBuffer, TRUE);
				continue;
			}
		}
	}

};

template<> 
void SimpleDebugType<float>::_equalCheck(float checkValue)
{
	for (int i=0; i<(int)_equalCheckList.size(); ++i)
	{
		if(fabs(_equalCheckList[i] - checkValue) < 0.01)
		{
			char strBuffer[FORMAT_BUFFER_SIZE] = {0};
			sprintf_s(strBuffer, FORMAT_BUFFER_SIZE, LOG_FLOAT_EQUAL_CHECK
					, _pszCurFile, _nCurLine, _name.c_str(), _data, checkValue);
			
			common::Tracer::Instance().WriteTrace(strBuffer, TRUE);
		}
	}
}

template<> 
void SimpleDebugType<double>::_equalCheck(double checkValue)
{
	for (int i=0; i<(int)_equalCheckList.size(); ++i)
	{
		if(fabs(_equalCheckList[i] - checkValue) < 0.01)
		{
			char strBuffer[FORMAT_BUFFER_SIZE] = {0};
			sprintf_s(strBuffer, FORMAT_BUFFER_SIZE, LOG_FLOAT_EQUAL_CHECK
					, _pszCurFile, _nCurLine, _name.c_str(), _data, checkValue);
			
			Tracer::Instance().WriteTrace(strBuffer, TRUE);

		}
	}
}

_X2D_NS_END