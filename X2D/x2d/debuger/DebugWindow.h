// DebugWindow.h: interface for the CDebugConsole class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGWINDOW_H__8F9EC5D1_F7EA_4FD8_8DC2_6B8645275321__INCLUDED_)
#define AFX_DEBUGWINDOW_H__8F9EC5D1_F7EA_4FD8_8DC2_6B8645275321__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef int (CMainFrame::*IntProc)();

//This is the super class that all debug classes should inherit
class CDebugObject
{
	virtual void Foo(){};
	virtual DWORD GetDebugClassID(){return -1;};
};

//Store debug info
class CDebugValue : public CDebugObject
{
public:
	virtual LRESULT SetValueName(){return 0;};
	virtual LPCTSTR	GetValueName(){return 0;};
	virtual LRESULT SetValueData(){return 0;};
	virtual LPCTSTR  GetValueData(){return 0;};

protected:
	TCHAR m_szValueName[STRING_BUFFER_SIZE];
	TCHAR m_szValueData[STRING_BUFFER_SIZE];

};

class CDebugStringValue: public CDebugValue
{
public:
	virtual LPCTSTR	GetValueName();
	virtual LPCTSTR  GetValueData();
	CDebugStringValue(LPCTSTR szString, LPCTSTR szValueName)
	{
		m_szString = szString;
		lstrcpyn(m_szValueName, szValueName, STRING_BUFFER_SIZE);
	}
private:
	LPCTSTR m_szString;
};

class CDebugIntValue: public CDebugValue
{
public:
	virtual LPCTSTR	GetValueName();
	virtual LPCTSTR  GetValueData();
	CDebugIntValue(int* pInt, LPCTSTR szValueName)
	{
		m_pData = pInt;
		lstrcpyn(m_szValueName, szValueName, STRING_BUFFER_SIZE);
	}
private:
	int* m_pData;

};

class CDebugConstIntValue: public CDebugValue
{
public:
	virtual LPCTSTR	GetValueName();
	virtual LPCTSTR  GetValueData();
	CDebugConstIntValue(int value, LPCTSTR szValueName)
	{
		m_pData = value;
		lstrcpyn(m_szValueName, szValueName, STRING_BUFFER_SIZE);
	}
private:
	int m_pData;

};

class CDebugIntRetValue: public CDebugValue
{
public:
	CDebugIntRetValue(CMainFrame* pMainFrame, IntProc lpfnIntProc , LPCTSTR szValueName);

	virtual LPCTSTR	
		GetValueName();
	virtual LPCTSTR  
		GetValueData();

private:
	IntProc 
		m_lpfnIntProc;
	CMainFrame* 
		m_pMainFrame;
};

//Include sink need be implemented at the client end
class CDebugSink : public CDebugObject
{
public:
	virtual LRESULT 
		GetDebugInfo() = 0;
	virtual HWND 
		GetParentWndHandle() = 0;
	virtual LRESULT 
		OnDebugValueChanged() = 0;
};
typedef CDebugSink* LPDEBUGSINK;

class CDebugPrinter : public CDebugObject
{
public:
	virtual LRESULT 
		InitPrinter() = 0;
	virtual LRESULT 
		PrintDebugText(int x, int y, LPCTSTR lpszString, int nCount = -1) = 0;
	virtual LRESULT 
		ShowPrintBoard(DWORD nCmdShow){return 0;};
};
typedef CDebugPrinter* LPDEBUGPRINTER;

class CWindowPrinter :public CDebugPrinter, CWindowImpl<CWindowPrinter>
{

	BEGIN_MSG_MAP(CWindowPrinter)
		MESSAGE_HANDLER(WM_CREATE,OnCreate)
		MESSAGE_HANDLER(WM_RBUTTONUP,OnRightButtonUp)
		MESSAGE_HANDLER(WM_SYSKEYDOWN,OnSysKeyDown)
		MESSAGE_HANDLER(WM_CLOSE,OnClose)
		MESSAGE_HANDLER(WM_TIMER,OnTimer)
	END_MSG_MAP()
protected:
	LRESULT OnSysKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnRightButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

public:	
	virtual LRESULT 
		InitPrinter();
	virtual LRESULT 
		PrintDebugText(int x, int y, LPCTSTR lpszString, int nCount = -1);
	virtual LRESULT 
		ShowPrintBoard(DWORD nCmdShow);

};

////////////////////////////
////////////////////////////
//Debug manage
class CDebugConsole : public CDebugObject
{
public:

	CDebugConsole(CDebugSink* pSinkedClient);
	virtual ~CDebugConsole();

public:
	LRESULT 
		ShowConsole();
	LRESULT 
		ClearDebugData();

	LRESULT 
		AddDebugValue(CMainFrame* pMainFrame, IntProc lpfnIntProc, LPCTSTR szValueName);
	LRESULT 
		AddDebugValue(LPCTSTR szString, LPCTSTR szValueName);
	LRESULT 
		AddDebugValue(int value, LPCTSTR szValueName);
	LRESULT 
		AddDebugValue(int* pInt, LPCTSTR szValueName);
	LRESULT 
		AddDebugValue(DWORD* pDWORD, LPCTSTR szValueName);

private:
	LRESULT 
		InitConsole(CDebugSink* pSinkedClient);
	void 
		PrintVariablesValue();
	void 
		PrintString(LPCTSTR szLable,LPCTSTR szString);

	static VOID CALLBACK 
		TimerProc(
		HWND hwnd,         // handle to window
		UINT uMsg,         // WM_TIMER message
		UINT_PTR idEvent,  // timer identifier
		DWORD dwTime       // current system time
		);

private:
	int 
		m_iYPos;
	LPDEBUGSINK 
		m_pSinkedClient;
	LPDEBUGPRINTER 
		m_pDebugPrinter;
	//Debug data
	map<int, CDebugValue*, less<int> > 
		m_mapDebugValueArray;
	vector<CDebugValue*> 
		m_aSimpleDebugData;
};

#endif // !defined(AFX_DEBUGWINDOW_H__8F9EC5D1_F7EA_4FD8_8DC2_6B8645275321__INCLUDED_)
