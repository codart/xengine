// DebugableType.cpp: implementation of the DebugableType class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DebugableType.h"


_X2D_NS_BEGIN

void DebugableType::SetCurrentDbgInfo(LPCSTR pszFile, int nLine)
{
	_pszCurFile = pszFile;
	_nCurLine = nLine;
}

void DebugableType::SetName(LPCSTR pszName)
{
	_name = pszName;
}

_X2D_NS_END