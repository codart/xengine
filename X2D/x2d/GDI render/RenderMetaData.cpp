#include "StdAfx.h"
#include "RenderMetadata.h"

RenderMetaData::RenderMetaData(void)
{
	_bManagedData = TRUE;

	_visible = TRUE;

	__x = __y = 0;
	width = height = 0;

#ifdef GAME_ENGINE
	scaleX = 1;
	scaleY = 1;;
	rotation = 0;
#endif // GAME_ENGINE

	useAlphaChannel = FALSE;
	renderType = RENDER_EMPTY;
	pRenderData = NULL;

	blendCode = NO_BLEND;
	alpha = 255;

#ifdef STATIC_UI
	_bAllDirtied = FALSE;
#endif // STATIC_UI

	zOffset = 0;
}

RenderMetaData::~RenderMetaData(void)
{

}