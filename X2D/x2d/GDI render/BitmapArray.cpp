#include "StdAfx.h"
#include "BitmapArray.h"
#include "x2d/error/LoadResFailedException.h"

BitmapArray::BitmapArray(void)
{
	width = 0;
	height = 0;
}

BitmapArray::~BitmapArray(void)
{
	Dispose();
}

int BitmapArray::LoadAction(LPCWSTR strFileName, int nBmpsPerAction, 
						  COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Dispose();

	AutoPtr<BitmapData> largeBmpData = new BitmapData;
	if(!largeBmpData->LoadFromBmpFile(strFileName)) throw LoadResFailedException();

	_calculateBmpSize(1, nBmpsPerAction, largeBmpData->width, largeBmpData->height, 
		bmpAlign);

	_loadActionBmps(*largeBmpData, 0, nBmpsPerAction, keyColor, bmpAlign);

	return 0;
}

int BitmapArray::LoadActions(LPCWSTR strFileName, int nActionCount, int nBmpsPerAction, 
						   COLORREF keyColor,  BMPS_ALIGN bmpAlign)
{
	Dispose();

	AutoPtr<BitmapData> largeBmpData = new BitmapData;
	if(!largeBmpData->LoadFromBmpFile(strFileName)) throw LoadResFailedException();

	_calculateBmpSize(nActionCount, nBmpsPerAction, largeBmpData->width, 
		largeBmpData->height, bmpAlign);

	for (int nActionIndex = 0; nActionIndex<nActionCount; ++nActionIndex)
	{
		_loadActionBmps(*largeBmpData, nActionIndex, nBmpsPerAction, 
			keyColor, bmpAlign);
	}

	return 0;
}

int BitmapArray::LoadAction(int ID, int nBmpsPerAction, COLORREF keyColor, 
						  BMPS_ALIGN bmpAlign)
{
	Dispose();

	CBitmap bitmap;
	bitmap.LoadBitmap(ID);
	BITMAP bmpInfo;
	bitmap.GetBitmap(&bmpInfo);

	_calculateBmpSize(1, nBmpsPerAction, bmpInfo.bmWidth, bmpInfo.bmHeight , 
		bmpAlign);

	AutoPtr<BitmapData> largeBmpData = new BitmapData;
	largeBmpData->CreateBitmap(bmpInfo.bmWidth, bmpInfo.bmHeight, RGBA_32);
	bitmap.GetBitmapBits(largeBmpData->_$byteCount, largeBmpData->_$pBmpBuf);

	_loadActionBmps(*largeBmpData, 0, nBmpsPerAction, keyColor, bmpAlign);

	return 0;
}

int BitmapArray::LoadActions(int ID, int nActionCount, int nBmpsPerAction, 
						   COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Dispose();

	CBitmap bitmap;
	bitmap.LoadBitmap(ID);

	BITMAP bmpInfo;
	bitmap.GetBitmap(&bmpInfo);

	_calculateBmpSize(nActionCount, nBmpsPerAction, 
		bmpInfo.bmWidth, bmpInfo.bmHeight, bmpAlign);

	BitmapData largeBmpData;
	largeBmpData.CreateBitmap(bmpInfo.bmWidth, bmpInfo.bmHeight, 4);
	bitmap.GetBitmapBits(largeBmpData._$byteCount, largeBmpData._$pBmpBuf);

	Dispose();
	for (int nActionIndex = 0; nActionIndex<nActionCount; ++nActionIndex)
	{
		_loadActionBmps(largeBmpData, nActionIndex, nBmpsPerAction, 
			keyColor, bmpAlign);
	}

	return 0;
}

void BitmapArray::_loadActionBmps(BitmapData& largeBmpData,
							   int nActionIndex, int nBmpsPerAction, 
							   COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Action* pAction =  new Action;
	push_back(pAction);

	BitmapData* pBmpData = NULL;
	Rect<int> rect;
	for (int nBmpIndex=0; nBmpIndex<nBmpsPerAction; ++nBmpIndex)
	{
		pBmpData = new BitmapData;
		pBmpData->CreateBitmap((int)width, (int)height, RGBA_32);

		if(H_BMPS == bmpAlign)
		{
			rect.left = nBmpIndex * (int)width;
			rect.right = rect.left + (int)width;
			rect.top = nActionIndex * (int)height;
			rect.bottom = rect.top + (int)height;
		}
		else
		{
			rect.left = nActionIndex * (int)width;
			rect.right = rect.left + (int)width;
			rect.top = nBmpIndex * (int)height;
			rect.bottom = rect.top + (int)height;
		}

		pBmpData->CopyPixels(&largeBmpData, rect, CPoint(0, 0));
		pAction->push_back(pBmpData);
	}
}

void BitmapArray::_calculateBmpSize(int nActionCount, int nBmpsPerAction, 
								 int largeBmpWidth, int LargeBmpHeight, 
								 BMPS_ALIGN bmpAlign)
{
	if (H_BMPS == bmpAlign)
	{
		width = float(largeBmpWidth / nBmpsPerAction);
		height = float(LargeBmpHeight / nActionCount);
	}
	else
	{
		width = float(largeBmpWidth / nActionCount);
		height = float(LargeBmpHeight / nBmpsPerAction);
	}
}

void BitmapArray::Dispose()
{
	Action* pAction = NULL;
	BitmapData* pBmpData = NULL;

	VecActionIt actionIt = begin();
	for ( ; actionIt != end(); ++actionIt)
	{
		pAction = *actionIt;
		assert(pAction);

		ActionIt bmpDataIt = pAction->begin();
		for ( ; bmpDataIt != pAction->end(); ++bmpDataIt)
		{
			pBmpData = *bmpDataIt;
			assert(pBmpData);
			delete pBmpData;
		}

		delete pAction;
	}

	width = height = 0;
}