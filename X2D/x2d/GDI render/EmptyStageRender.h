#pragma once

#include "x2d/render/IStageRender.h"

class EmptyStageRender : public IStageRender
{
public:
	static EmptyStageRender
		s_emptyRender;
public:
	override void 
		Destroy() {};

	override void 
		InvalidateRect(Rect<int>* pRect){};
	override int
		SetRenderDC(HDC renderDC, HWND hWnd){return 0;};
	override int 
		SetRenderRectSize(int width, int height){return 0;};
	override void 
		InvalidateObject(RenderMetaData* pRenderData){};

	override int 
		BeginRender(){return 0;};
	override void 
		RenderDisplayList(RenderMetaData* pRenderData){};
	override int 
		Render(){return 0;};
	override int 
		EndRender(){return 0;};
	override void 
		ClearBackground(COLORREF bgColor){};
	override BOOL
		NeedRedraw(){return FALSE;};
};

#define EMPTY_RENDER &EmptyStageRender::s_emptyRender