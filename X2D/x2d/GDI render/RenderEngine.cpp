#include "StdAfx.h"
#include "RenderEngine.h"
#include "x2d/debuger/Tracer.h"

RenderEngine::RenderEngine(void)
{
	for (int i=0; i<4; ++i)
	{
		_quadrantRect[i].SetRectEmpty();
	}

	__pBmpInfo = 0;
	__pBackBuff.CreateBitmap(1, 1, 4);

	Rect<int> rect;
	rect.SetRectEmpty();
	for(int i = 0; i < 4; ++i) _vecInvalidRectList.push_back(rect);

#ifdef STATIC_UI
	bNeedRedraw = FALSE;
#endif // STATIC_UI
}

RenderEngine::~RenderEngine(void)
{
	if(__pBmpInfo) delete __pBmpInfo;
	__pBackBuff.Dispose();
}

void RenderEngine::Destroy()
{
	delete this;
}

int RenderEngine::SetRenderDC(HDC renderDC, HWND hWnd)
{
	m_destWindow = renderDC;
	return 0;
}

int RenderEngine::SetRenderRectSize(int _width, int _height)
{
	width = _width;
	height = _height;

	__create_bitmap_info();

	__pBackBuff.Dispose();

	// Need optimize: We can allocate a bmp that has the screen's resolution, and use its 
	// clipped rect as backbuffer. It can improve resize efficiency.
	__pBackBuff.CreateBitmap(width, height, 4);

	__pBackBuff.Clear();

	// Calculate quadrant
	int midX = width/2;
	int midY = height/2;
	//_quadrantRect[3].SetRect(0, 0, width, height);

	_quadrantRect[0].SetRect(0, 0, midX, midY+5);
	_quadrantRect[1].SetRect(midX, 0, width, midY);
	_quadrantRect[2].SetRect(0, midY, midX, height);
	_quadrantRect[3].SetRect(midX, midY, width, height);

	_vecInvalidRectList.clear();
	Rect<int> rect;
	rect.SetRectEmpty();
	for(int i = 0; i < 4; ++i) _vecInvalidRectList.push_back(rect);

	renderRect.SetRect(0, 0, width, height);

	return 0;
}

void RenderEngine::InvalidateRect(Rect<int>* pRect)
{
#ifdef STATIC_UI
	// Judge which quadrant belong to
	Rect<int> rcTemp;
	for (int i=0; i<4; ++i)
	{
		if(rcTemp.IntersectRect(&_quadrantRect[i], pRect))
		{

			_vecInvalidRectList.at(i).UnionRect(&_vecInvalidRectList.at(i), &rcTemp);
		}
	}
#endif // STATIC_UI
}

void RenderEngine::InvalidateObject(RenderMetaData* pRenderData)
{
#ifdef STATIC_UI
	pRenderData->_bAllDirtied = TRUE;

	Rect<int> rect;
	pRenderData->GetRect(&rect);

	InvalidateRect(&rect);
#endif // STATIC_UI
}

int RenderEngine::BeginRender()
{
#ifdef STATIC_UI
	bNeedRedraw = _needRedraw();
	if(!bNeedRedraw) return 0;
#endif // STATIC_UI

	_zOrderedObjList.clear();

	_vecInvalidRectListCopy = _vecInvalidRectList;

	for(int i = 0; i < 4; ++i) 
	{
		_vecInvalidRectList.at(i).SetRectEmpty();
	}

	return 0;
}

void RenderEngine::RenderDisplayList(RenderMetaData* pRenderData)
{
#ifdef STATIC_UI
	if(!bNeedRedraw) return;
#endif // STATIC_UI

	_insertTreeIntoZOrderedList(pRenderData);

	// Render ordered dirty obj list
	_renderZOrderedObjList();
}

void RenderEngine::_insertTreeIntoZOrderedList(RenderMetaData* pRenderData)
{
	if(	!pRenderData->_visible) return;

	_insertObjIntoZOrderedList(pRenderData);

	NodeTypePtr pCurNode = pRenderData->pFirstChild;	
	while (pCurNode)					
	{
		_insertTreeIntoZOrderedList((RenderMetaData*)pCurNode);
		pCurNode = pCurNode->pNextSibling;
	}	
}

void RenderEngine::_insertObjIntoZOrderedList(RenderMetaData* pRenderData)
{
#ifdef STATIC_UI
	if(pRenderData->_bAllDirtied) 
	{
		//Insert to dirty obj map (sort by Z pos)
		_zOrderedObjList.insert(make_pair(Z_POS, pRenderData));
	}
	else
	{
		pRenderData->GetRect(&pRenderData->_dirtyRect);
		if(_intersectInvalidRects(&pRenderData->_dirtyRect))
		{
			//Insert to dirty obj map (sort by Z pos)
			_zOrderedObjList.insert(make_pair(Z_POS, pRenderData));
		}

	}
#else // STATIC_UI
	//Sort all objs by Z pos 
	_zOrderedObjList.insert(make_pair(Z_POS, pRenderData));
#endif // STATIC_UI
}

void RenderEngine::_renderZOrderedObjList()
{
	MapRenderDataIt it  = _zOrderedObjList.begin();
	for ( ; it != _zOrderedObjList.end(); ++it)
	{
		RenderMetaData* pRenderData = (*it).second;
		assert(pRenderData);

#ifdef STATIC_UI
		if(pRenderData->_bAllDirtied)
		{
			// Prepare dirty rect
			pRenderData->GetRect(&pRenderData->_dirtyRect);
			pRenderData->_dirtyRect.IntersectRect( &pRenderData->_dirtyRect, &renderRect );

			_drawPrimitive(pRenderData);

			pRenderData->_bAllDirtied = FALSE;
		}
		else 
		{
			_drawPrimitive(pRenderData);
		}
#else
		_drawPrimitive(pRenderData);
#endif // STATIC_UI
	}

	_zOrderedObjList.clear();
}

int RenderEngine::EndRender()
{
#ifdef STATIC_UI
	if(!bNeedRedraw) return 0;
#endif // STATIC_UI

	// Render ordered dirty obj list
	_renderZOrderedObjList();

#ifdef STATIC_UI
	// Copy the off screen to the real window
	for (VecRectIt it = _vecInvalidRectListCopy.begin(); it != _vecInvalidRectListCopy.end(); ++it)
	{
		Rect<int>* pInvalidRect = &(*it);
		if(pInvalidRect->IsRectEmpty()) continue;

		::SetDIBitsToDevice( m_destWindow,            // handle to device context
							pInvalidRect->left,         // x-coordinate of upper-left corner of 
							pInvalidRect->top,          // y-coordinate of upper-left corner of 
							pInvalidRect->Width(),		// source rectangle width
							pInvalidRect->Height(),		// source rectangle height
							pInvalidRect->left,				  // x-coordinate of lower-left corner of 
							height - pInvalidRect->bottom,    // y-coordinate of lower-left corner of 
							0,               // first scan line in array
							height,			// number of scan lines
							__pBackBuff._$pBmpBuf,    // address of array with DIB bits
							__pBmpInfo,           // address of structure with bitmap info.
							DIB_RGB_COLORS   // RGB or palette indexes	
							);	
	}
#else
	::SetDIBitsToDevice( m_destWindow,            // handle to device context
		renderRect.left,         // x-coordinate of upper-left corner of 
		renderRect.top,          // y-coordinate of upper-left corner of 
		renderRect.Width(),		// source rectangle width
		renderRect.Height(),		// source rectangle height
		renderRect.left,				  // x-coordinate of lower-left corner of 
		height - renderRect.bottom,    // y-coordinate of lower-left corner of 
		0,               // first scan line in array
		height,			// number of scan lines
		__pBackBuff._$pBmpBuf,    // address of array with DIB bits
		__pBmpInfo,           // address of structure with bitmap info.
		DIB_RGB_COLORS   // RGB or palette indexes	
		);	
#endif // STATIC_UI

	return 0;
}

void RenderEngine::RenderObject(RenderMetaData* pRenderData)
{
	if(pRenderData->renderType == RENDER_EMPTY) return;

#ifdef STATIC_UI
	if(pRenderData->_bAllDirtied) 
	{
		//Insert to dirty obj map (sort by Z pos)
		_zOrderedObjList.insert(make_pair(Z_POS, pRenderData));

	}
	else
	{
		pRenderData->GetRect(&pRenderData->_dirtyRect);
		if(_intersectInvalidRects(&pRenderData->_dirtyRect))
		{
			//Insert to dirty obj map (sort by Z pos)
			_zOrderedObjList.insert(make_pair(Z_POS, pRenderData));
		}

	}
#else // STATIC_UI
	//Sort all objs by Z pos 
	__zOrderedObjList.insert(make_pair(Z_POS, pRenderData));
#endif // STATIC_UI
}

void RenderEngine::__create_bitmap_info()
{
	if(__pBmpInfo) delete __pBmpInfo;

	unsigned line_len = width * 24;
	unsigned img_size = line_len * height;
	unsigned rgb_size = 0;
	unsigned full_size = sizeof(BITMAPINFOHEADER) + rgb_size + img_size;

	__pBmpInfo = (BITMAPINFO *) new unsigned char[full_size];

	__pBmpInfo->bmiHeader.biSize   = sizeof(BITMAPINFOHEADER);
	__pBmpInfo->bmiHeader.biWidth  = width;
	__pBmpInfo->bmiHeader.biHeight = -height;
	__pBmpInfo->bmiHeader.biPlanes = 1;
	__pBmpInfo->bmiHeader.biBitCount = (unsigned short)32;
	__pBmpInfo->bmiHeader.biCompression = 0;
	__pBmpInfo->bmiHeader.biSizeImage = img_size;
	__pBmpInfo->bmiHeader.biXPelsPerMeter = 0;
	__pBmpInfo->bmiHeader.biYPelsPerMeter = 0;
	__pBmpInfo->bmiHeader.biClrUsed = 0;
	__pBmpInfo->bmiHeader.biClrImportant = 0;
}

void RenderEngine::ClearBackground(COLORREF bgColor)
{
#ifdef STATIC_UI
	if(bNeedRedraw)
	{
		for (VecRectIt it = _vecInvalidRectListCopy.begin(); 
			it != _vecInvalidRectListCopy.end(); ++it)
		{
			__pBackBuff.ClearClipRect(&(*it));
		}
	}

#else
	__pBackBuff.Clear();
#endif // STATIC_UI	

}

void RenderEngine::_drawPrimitive(RenderMetaData* pRenderData)
{
	switch (pRenderData->renderType)
	{
	case RENDER_BITMAP:
		_drawBitmap(pRenderData);
		break;
	case RENDER_VECTOR:
		//..DrawVector(pRenderData);
		break;
	}		
}

BOOL RenderEngine::_intersectInvalidRects(Rect<int>* pDestRect)
{
	Rect<int> rc[4];
	for (int i=0; i<4; ++i)
	{
		rc[i].IntersectRect( pDestRect, &_vecInvalidRectListCopy.at(i) );
	}

	rc[0].UnionRect( &rc[0], &rc[1] );
	rc[2].UnionRect( &rc[2], &rc[3] );

	pDestRect->UnionRect( &rc[0], &rc[2] );

	return pDestRect->IsRectEmpty()? FALSE:TRUE;
}

BOOL RenderEngine::_needRedraw()
{
	for (VecRectIt it = _vecInvalidRectList.begin(); it != _vecInvalidRectList.end(); ++it)
	{
		if(!(*it).IsRectEmpty()) return TRUE;
	}
	return FALSE;
}

void RenderEngine::_drawBitmap(RenderMetaData* pRenderData)
{
	BitmapData* pBitmapData = (BitmapData*)pRenderData->pRenderData;

	if(pRenderData->keyColor == KEY_COLOR)
	{
		_drawBmpWithKeyColor(pRenderData);
	}
	else
	{
		if(pRenderData->useAlphaChannel)
		{
			_drawBmpwithAlphaChanel(pRenderData);
		}
		else if(pRenderData->alpha != 255)
		{
			_drawAlphaBlendBmp(pRenderData);
		}
		else
		{
			_drawSimpleBmp(pRenderData);
		}

	}
}


#ifdef STATIC_UI
#define PREPARE_SRC_RECT		\
	CPoint destPt((int)pRenderData->_dirtyRect.left, \
	(int)pRenderData->_dirtyRect.top);	\
	\
	Rect<int> srcRect(pRenderData->_dirtyRect);	\
	srcRect.OffsetRect(-int(pRenderData->__x), -int(pRenderData->__y));
#else
#define PREPARE_SRC_RECT		\
	Rect<int> srcRect;			\
	pRenderData->GetRect(&srcRect);	\
	if(!srcRect.IntersectRect(&srcRect, &renderRect)) return;	\
	\
	CPoint destPt(srcRect.left,	\
	srcRect.top);	\
	\
	srcRect.OffsetRect(-int(pRenderData->__x), -int(pRenderData->__y));
#endif // STATIC_UI

void RenderEngine::_drawSimpleBmp(RenderMetaData* pRenderData)
{
	PREPARE_SRC_RECT

		__pBackBuff.CopyPixels((BitmapData*)pRenderData->pRenderData, 
		srcRect, destPt);
}

void RenderEngine::_drawBmpWithKeyColor(RenderMetaData* pRenderData)
{
	PREPARE_SRC_RECT

		__pBackBuff.CopyPixelsWithKeyColor((BitmapData*)pRenderData->pRenderData,
		srcRect, destPt);
}

void RenderEngine::_drawBmpwithAlphaChanel(RenderMetaData* pRenderData)
{
	PREPARE_SRC_RECT

		__pBackBuff.CopyPixelsWithAlphaChanel((BitmapData*)pRenderData->pRenderData, 
		srcRect, destPt);
}

void RenderEngine::_drawAlphaBlendBmp(RenderMetaData* pRenderData)
{
	PREPARE_SRC_RECT

		__pBackBuff.CopyPixelsWithAlphaBlend((BitmapData*)pRenderData->pRenderData, 
		srcRect, destPt, pRenderData->alpha);
}

void RenderEngine::_drawVector(RenderMetaData* pRenderData)
{
	Rect<int> rect;
	/*
	pRenderData->GetRect(&rect);
	m_destWindow.FillRect(&rect, (HBRUSH)::GetStockObject(GRAY_BRUSH));
	*/
}