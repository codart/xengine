#pragma once

#include "x2d/util/RectEx.h"
#include "x2d/render/RenderMetaData.h"

class IStageRender
{
public:
	virtual int
		SetRenderDC(HDC renderDC, HWND hWnd) = 0;
	virtual int 
		SetRenderRectSize(int width, int height) = 0;
	virtual void 
		InvalidateRect(Rect<int>* pRect) = 0;
	virtual void 
		InvalidateObject(RenderMetaData* pRenderData) = 0;

	virtual int 
		BeginRender() = 0;
	virtual void 
		ClearBackground(COLORREF bgColor) = 0;
	virtual void 
		RenderDisplayList(RenderMetaData* pRenderData) = 0;
	virtual int 
		EndRender() = 0;

	virtual void
		Destroy() = 0;
};
