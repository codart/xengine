#pragma once

#include "x2d/core/Object.h"
#include "x2d/util/RectEx.h"

#define NO_KEY_COLOR 0x00000000
#define MAX_BMP_SIZE 400000000

class _XEngineExport BitmapData : public Object
{
	friend class RenderEngine;
	friend class BitmapArray;

public:
	BitmapData(void);
	virtual ~BitmapData(void);

public:
	int width;

	int height;

public:
	// Copy bitmap bits of srcClipRect from pSrcBmpData to self's buffer	
	void 
		CopyPixels(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint);

	void 
		CopyPixelsWithKeyColor(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint);

	void 
		CopyPixelsWithAlphaChanel(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint);

	void 
		CopyPixelsWithAlphaBlend(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint, int _alpha);

	//.. Instead with Agg::pixel_map later
	BOOL 
		LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor = NO_KEY_COLOR);

	BOOL 
		LoadFromResource(int resID);

	INLINE void 
		CreateBitmap(int _width, int _height, int bitsPixel)
	{
		assert(_$pBmpBuf == NULL);
		_$byteCount = _width * _height * RGBA_32;
		_$pBmpBuf = (BYTE*)_mm_malloc(_$byteCount, 16); 

		width = _width;
		height = _height;
		__bmWidthBytes = width * RGBA_32;//..Need byte aligned
	}

	// Clear with white 
	INLINE void 
		Clear()
	{
		assert(_$pBmpBuf);
		memset(_$pBmpBuf, 255, _$byteCount);
	}

	INLINE void
		ClearClipRect(Rect<int>* pClipRect)
	{
		int nRectWidth = pClipRect->Width();
		int nRectHeight = pClipRect->Height();

		int nBmpWidthBytes = pClipRect->Width() * 4;

		int nBackHeight = height;
		int nBackWidth = width;
		int nBackWidthBytes = width * 4;

		BYTE* pBackLinkCur = _$pBmpBuf + 
							nBackWidthBytes * pClipRect->top + pClipRect->left * 4;

		for (int y = 0 ; y < nRectHeight; ++y)
		{
			memset(pBackLinkCur, 255, nBmpWidthBytes);
			pBackLinkCur += nBackWidthBytes;
		}
	}

	// Dispose bitmap data, reset all members.
	INLINE void 
		Dispose()
	{
		assert(_$pBmpBuf);
		_mm_free(_$pBmpBuf) ;

		_$pBmpBuf = NULL;
		__bmWidthBytes = width = height = 0;
		//keyColor = NO_KEY_COLOR;
	}

protected:
	BYTE* 
		_$pBmpBuf;
	int 
		_$byteCount;

private:
	int 
		__bmWidthBytes;
	int 
		__nRawBitCount;

protected:
	void 
		_RGB24ToRGB32(BYTE* pSrcBuf, BYTE* pDestBuf);

};
