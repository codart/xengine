#pragma once

#include "x2d/util/RectEx.h"
#include "x2d/util/Tree.h"

#define TOP_MOST 40000.0
#define BOTTOM_MOST -40000.0

enum RENDER_TYPE
{
	RENDER_EMPTY, 

	RENDER_BITMAP,
	RENDER_VECTOR,
	RENDER_VIDEO,
};

enum BLEND_CODE
{
	NO_BLEND,
	COLOR_KEY,
	ALPHA_CHANEL,
	ALPHA_BLEND,
};

/**
	Render meta data. Render engine will use this meta data to get detailed render info.	
*/
class _XEngineExport RenderMetaData : public TreeNode<DWORD>
{
public:
	RenderMetaData(void);
	~RenderMetaData(void);

public:
	INLINE void __SetX(float newX)
	{
		__x = newX;
	}

	INLINE void __SetY(float newY)
	{
		__y = newY;
	}

	INLINE void __SetWidth(float newWidth)
	{
		width = newWidth;
	}

	INLINE void __SetHeight(float newHeight)
	{
		height = newHeight;
	}

	INLINE void SetAlpha(int newAlpha)
	{
		alpha = newAlpha;
	}

	INLINE int GetAlpha()
	{
		return alpha;
	}

	INLINE void GetRect(Rect<int>* pRect)
	{
		pRect->SetRect( (int)__x, (int)__y, 
			int(__x + width),  int(__y + height) );
	}

public:
	BOOL
		_visible;

	// The absolute coordinates (Stage)
	float
		__x;	
	float
		__y;

	float 
		width;
	float
		height;

#ifdef GAME_ENGINE
	float
		scaleX;
	float 
		scaleY;

	float 
		rotation;
#endif // GAME_ENGINE

	// graphic data
	RENDER_TYPE 
		renderType;

	void*	
		pRenderData;

	BOOL
		_bManagedData;
	BOOL
		useAlphaChannel;
	BLEND_CODE
		blendCode;

	int
		alpha; // 0~255

	COLORREF
		keyColor;

#ifdef STATIC_UI
	BOOL
		_bAllDirtied;
#endif // STATIC_UI

	Rect<int> 
		_dirtyRect;

	float
		zOffset;
};

