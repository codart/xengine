#pragma once

#include "BitmapData.h"

enum BMPS_ALIGN
{
	H_BMPS,
	V_BMPS
};

typedef vector<BitmapData*> Action;
typedef vector<Action*>::iterator VecActionIt;
typedef Action::iterator ActionIt;

class BitmapArray : public Object
						, public vector<Action*>
{
public:
	BitmapArray(void);
	virtual ~BitmapArray(void);

public:
	float width;

	float height;

public:
	int LoadAction(LPCWSTR strFileName, int nBmpsPerAction, 
		COLORREF keyColor = NO_KEY_COLOR,
		BMPS_ALIGN bmpAlign = H_BMPS);

	int
		LoadAction(int ID, int nCount, COLORREF keyColor = NO_KEY_COLOR,
		BMPS_ALIGN bmpAlign = H_BMPS);

	int
		LoadActions(LPCWSTR strFileName, int nActionCount, int nBmpsPerAction, 
		COLORREF keyColor = NO_KEY_COLOR, 
		BMPS_ALIGN bmpAlign = H_BMPS);

	int
		LoadActions(int ID, int nActionCount, int nBmpsPerAction, 
		COLORREF keyColor = NO_KEY_COLOR, 
		BMPS_ALIGN bmpAlign = H_BMPS);

	void 
		Dispose();

protected:
	void 
		_loadActionBmps(BitmapData& largeBmpData,
		int nActionIndex, int nBmpsPerAction, 
		COLORREF keyColor, BMPS_ALIGN bmpAlign);

	void 
		_calculateBmpSize(int nActionCount, int nBmpsPerAction, 
		int largeBmpWidth, int LargeBmpHeight, 
		BMPS_ALIGN bmpAlign = H_BMPS);


};
