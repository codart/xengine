#include "StdAfx.h"
#include "BitmapData.h"
#include "mmintrin.h"

#define ALIGNED_WIDTH_BYTES(width, bytePerPixel)	\
	(width*bytePerPixel + bytePerPixel) & (~bytePerPixel);

#define PREPARE_PIXEL_POINTER		\
	int nBmpWidth = srcClipRect.Width();	\
	int nBmpHeight = srcClipRect.Height();	\
	\
	int nBmpWidthBytes = pSrcBmpData->__bmWidthBytes;	\
	int nClipRectWidthBytes = nBmpWidth * RGBA_32;	\
	\
	int nBackHeight = height;	\
	int nBackWidth = width;	\
	int nBackWidthBytes = this->__bmWidthBytes;	\
	\
	BYTE* pBmpLineCur = pSrcBmpData->_$pBmpBuf + nBmpWidthBytes * srcClipRect.top	\
	+ srcClipRect.left * RGBA_32;		\
	BYTE* pBackLineCur = this->_$pBmpBuf + nBackWidthBytes * destPoint.y	\
	+ destPoint.x * RGBA_32;	\
	\
	BYTE* pBmpByteCur = pBmpLineCur;	\
	BYTE* pBackByteCur = pBackLineCur;	

BitmapData::BitmapData(void)
{
	_$pBmpBuf = NULL;
	__nRawBitCount = 0;
}

BitmapData::~BitmapData(void)
{
	if (_$pBmpBuf) _mm_free(_$pBmpBuf);
}
/*

void BitmapData::CopyPixels(BitmapData* pSrcBmpData, CRect& srcClipRect, CPoint& destPoint)
{
	PREPARE_PIXEL_POINTER;

	int nLineLoop = nClipRectWidthBytes >> 4; // Copy 4 pixel (128bit) each time, loop time is Width / 4

	// Adjust pointer to 16 byte aligned

	byte* alignedPtr = (byte *) ( ( (int) pBmpLineCur ) + 15 & ~15 );
	if ( alignedPtr - pBmpLineCur < 4 ) {
		alignedPtr += 16;
	}
	int nOffsetByte = alignedPtr - (BYTE*)pBmpLineCur;
	nBmpWidthBytes = 496;
	pBmpLineCur = alignedPtr;
	
	//-----
	alignedPtr = (byte *) ( ( (int) pBackLineCur ) + 15 & ~15 );
	if ( alignedPtr - pBackLineCur < 4 ) {
		alignedPtr += 16;
	}
	nOffsetByte = alignedPtr - (BYTE*)pBackLineCur;
	nBackWidthBytes = 496;
	pBackLineCur = alignedPtr;

	__asm
	{

		mov eax, nBmpWidthBytes
		mov edx, nBackWidthBytes

		mov ecx, nBmpHeight;	// Height loop counter

loop_height:

		mov esi, pBmpLineCur
		mov edi, pBackLineCur

		mov ebx, nLineLoop

loop_width:
		movdqa xmm0,  0[ESI]
		movdqa  0[EDI], xmm0

		add esi, 16
		add edi, 16
		dec ebx
		jnz loop_width	

		add dword ptr[pBmpLineCur], eax
		add dword ptr[pBackLineCur], edx

		dec ecx
		jnz loop_height

	}

	_mm_empty();
}*/


void BitmapData::CopyPixels(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint)
{
	PREPARE_PIXEL_POINTER;

	for (int y = 0 ; y < nBmpHeight; ++y)
	{
		memcpy(pBackLineCur, pBmpLineCur, nClipRectWidthBytes);

		pBmpLineCur += nBmpWidthBytes;
		pBackLineCur += nBackWidthBytes;
	}
}

void BitmapData::CopyPixelsWithKeyColor(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint)
{
	PREPARE_PIXEL_POINTER;

	for (int y = 0 ; y < nBmpHeight; ++y)
	{
		for (int x = 0; x < nBmpWidth; ++x
			, pBmpByteCur+=4
			, pBackByteCur+=4)
		{	
			if( (*((int*)pBmpByteCur) & 0x00FFFFFF) == 0x00FF00FF ) continue;

			*((int*)pBackByteCur) = *((int*)pBmpByteCur);
		}

		pBmpLineCur += nBmpWidthBytes;
		pBmpByteCur = pBmpLineCur;

		pBackLineCur += nBackWidthBytes;
		pBackByteCur = pBackLineCur;
	}
}

// Only judge the pSrcBmp's alpha data
void BitmapData::CopyPixelsWithAlphaChanel(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint)
{
	PREPARE_PIXEL_POINTER;

	for (int y = 0 ; y < nBmpHeight; ++y)
	{
		for (int x = 0; x < nBmpWidth; ++x
			, pBmpByteCur+=4
			, pBackByteCur+=4)
		{
			// pBmpByteCur[3] is the alpha data
			pBackByteCur[0] = ( (pBackByteCur[0] << 8) +  pBmpByteCur[3] * ( pBmpByteCur[0] - pBackByteCur[0]) )>>8;
			pBackByteCur[1] = ( (pBackByteCur[1] << 8) +  pBmpByteCur[3] * ( pBmpByteCur[1] - pBackByteCur[1]) )>>8;
			pBackByteCur[2] = ( (pBackByteCur[2] << 8) +  pBmpByteCur[3] * ( pBmpByteCur[2] - pBackByteCur[2]) )>>8;
		}

		pBmpLineCur += nBmpWidthBytes;
		pBmpByteCur = pBmpLineCur;

		pBackLineCur += nBackWidthBytes;
		pBackByteCur = pBackLineCur;
	}		
}

// Only judge the pSrcBmp's alpha data
void BitmapData::CopyPixelsWithAlphaBlend(BitmapData* pSrcBmpData, Rect<int>& srcClipRect, CPoint& destPoint, int _alpha)
{
	PREPARE_PIXEL_POINTER;

	for (int y = 0 ; y < nBmpHeight; ++y)
	{
		for (int x = 0; x < nBmpWidth; ++x
			, pBmpByteCur+=4
			, pBackByteCur+=4)
		{
			pBackByteCur[0] = ( (pBackByteCur[0] << 8) +  _alpha * ( pBmpByteCur[0] - pBackByteCur[0]) )>>8;
			pBackByteCur[1] = ( (pBackByteCur[1] << 8) +  _alpha * ( pBmpByteCur[1] - pBackByteCur[1]) )>>8;
			pBackByteCur[2] = ( (pBackByteCur[2] << 8) +  _alpha * ( pBmpByteCur[2] - pBackByteCur[2]) )>>8;
		}

		pBmpLineCur += nBmpWidthBytes;
		pBmpByteCur = pBmpLineCur;

		pBackLineCur += nBackWidthBytes;
		pBackByteCur = pBackLineCur;
	}		
}

BOOL BitmapData::LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor)
{
	FILE *fd = _wfopen(strFileName, L"rb");
	if(!fd)	return FALSE;

	BITMAPFILEHEADER  bmpFileHeader;
	BITMAPINFO       *bmpInfo = 0;
	unsigned          bmpSize;

	size_t nReadedSize = fread(&bmpFileHeader, sizeof(bmpFileHeader), 1, fd);
	if(bmpFileHeader.bfType != 0x4D42) goto bmperr;

	bmpSize = bmpFileHeader.bfSize - sizeof(BITMAPFILEHEADER);

	if(bmpSize > MAX_BMP_SIZE) goto bmperr;

	bmpInfo = (BITMAPINFO*) new BYTE[bmpSize];
	if(fread(bmpInfo, 1, bmpSize, fd) != bmpSize) goto bmperr;
	BYTE* pSrcBuf = ((BYTE*)bmpInfo) + sizeof(BITMAPINFOHEADER);

	if(_$pBmpBuf) Dispose();

	width  = bmpInfo->bmiHeader.biWidth;
	height = bmpInfo->bmiHeader.biHeight;
	__nRawBitCount = bmpInfo->bmiHeader.biBitCount;

	if(height < 0) height = - height;
	__bmWidthBytes =  width * RGBA_32;
	_$byteCount = __bmWidthBytes * height ;

	_$pBmpBuf =  (BYTE*)_mm_malloc(_$byteCount, 16);

	if(bmpInfo->bmiHeader.biBitCount == 24) _RGB24ToRGB32(pSrcBuf, _$pBmpBuf);
	else memcpy(_$pBmpBuf, pSrcBuf, _$byteCount);

	delete []((BYTE*)bmpInfo);
	fclose(fd);

	return TRUE;

bmperr:
	fclose(fd);
	if(bmpInfo) delete [] (BYTE*) bmpInfo;
	return FALSE;
}

BOOL BitmapData::LoadFromResource(int resID)
{
	CBitmap bitmap;
	if(!bitmap.LoadBitmap(resID)) return FALSE;

	BITMAP bmp;
	bitmap.GetBitmap(&bmp);
	width = bmp.bmWidth;
	height = bmp.bmHeight;

	width = bmp.bmWidth;
	height = bmp.bmHeight;

	int nByteCount = bmp.bmWidthBytes * bmp.bmHeight;
	_$byteCount = nByteCount;

	BYTE* pBuf = (BYTE*)_mm_malloc(nByteCount, 16);
	bitmap.GetBitmapBits(nByteCount, pBuf);

	//.. Should convert bits to 32 if is 16 bit.
	__bmWidthBytes = bmp.bmWidthBytes;
	_$pBmpBuf = pBuf;

	return TRUE;
}


void BitmapData::_RGB24ToRGB32(BYTE* pSrcBuf, BYTE* pDestBuf)
{
	int nSrcWidthBytes = ALIGNED_WIDTH_BYTES(width, RGBA_24);

	BYTE* pLineCur = pSrcBuf + (height-1) * nSrcWidthBytes;
	BYTE* pByteCur = pLineCur;

	// Convert to 32Bit bmp
	//.. Optimize with SSE later
	for (int y = 0 ; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			pDestBuf[0] = pByteCur[0];
			pDestBuf[1] = pByteCur[1];
			pDestBuf[2] = pByteCur[2];
			pDestBuf[3] = 0xFF;

			pByteCur += 3;
			pDestBuf += 4;
		}

		pLineCur -= nSrcWidthBytes;
		pByteCur = pLineCur;
	}
}
