#pragma once

/************************************************************************
Issues:
	Feature:
	//. Support add child to sprite										/
	//. Support Zoom out/in
	//. Support vector graphic
	//. Alpha blend														/
	//. Mouse input														/..
	//. Text filed/text input
	//. Animation														/
	//. Invalidate pRect list											/
	//. Move RenderObject into RenderEngine								/
	Issues:
	//. Allocate data in memory pool
	//. Classify the build macros such as STATIC_UI/Z_BUFFER/LOW_FRAME_RATE

************************************************************************/
#include "memory.h"

#include "x2d/render/RenderMetaData.h"
#include "x2d/render/BitmapData.h"
#include "x2d/util/RectEx.h"
#include "x2d/error/Exception.h"
#include "x2d/render/IStageRender.h"

class RenderEngine : public IStageRender
{
	friend class Stage;
	friend _XEngineExport BOOL InitEngine(HWND hParentWnd);
	friend _XEngineExport void UnInitEngine();

public:
	RenderEngine(void);
	virtual ~RenderEngine(void);

public:
	override void
		Destroy();

	override int
		SetRenderDC(HDC renderDC, HWND hWnd);
	override int 
		SetRenderRectSize(int width, int height);
	override void 
		InvalidateRect(Rect<int>* pRect);
	override void 
		InvalidateObject(RenderMetaData* pRenderData);

	override int 
		BeginRender();

	override void 
		RenderDisplayList(RenderMetaData* pRenderData);

	override int 
		EndRender();

	// Note: should call it between BeginRender()/EndRender().
	override void 
		ClearBackground(COLORREF bgColor);

	override void 
		RenderObject(RenderMetaData* pRenderData);


protected:
	int 
		width;

	int 
		height;

	Rect<int>
		renderRect;

	CDCHandle
		m_destWindow;


protected:
	typedef multimap<int, RenderMetaData*>::iterator MapRenderDataIt;

	multimap<int, RenderMetaData*> 
		_zOrderedObjList;


#ifdef STATIC_UI
	typedef vector< Rect<int> >::iterator VecRectIt;

	Rect<int> 
		_quadrantRect[4];

	vector< Rect<int> >
		_vecInvalidRectList;

	vector< Rect<int> >
		_vecInvalidRectListCopy;

	BOOL 
		bNeedRedraw;
#endif // STATIC_UI


protected:
	INLINE void
		_drawPrimitive(RenderMetaData* pRenderData);

	// Intersect prcDest with 'invalidate rect list'. 
	INLINE BOOL 
		_intersectInvalidRects(Rect<int>* pDestRect);

	INLINE void 
		_insertTreeIntoZOrderedList(RenderMetaData* pRenderData);
	INLINE void 
		_insertObjIntoZOrderedList(RenderMetaData* pRenderData);
	INLINE void 
		_renderZOrderedObjList();

	INLINE BOOL
		_needRedraw();
	INLINE void 
		_drawBitmap(RenderMetaData* pRenderData);

	// Draw simple bitmap with clip rect
	INLINE void
		_drawSimpleBmp(RenderMetaData* pRenderData);

	INLINE void 
		_drawBmpWithKeyColor(RenderMetaData* pRenderData);

	INLINE void
		_drawBmpwithAlphaChanel(RenderMetaData* pRenderData);

	INLINE void
		_drawAlphaBlendBmp(RenderMetaData* pRenderData);

	INLINE void
		_drawVector(RenderMetaData* pRenderData);

protected:
	static int 
		_$InitRenderEngine(HWND hWnd){return 0;};
	static int 
		_$UninitRenderEngine(){return 0;};

private:
	BITMAPINFO*
		__pBmpInfo;

	BitmapData 
		__pBackBuff;


private:
	void 
		__create_bitmap_info();

// Only for avoid error usage
public:
	RenderEngine(RenderEngine& renderEngine)
	{
		throw Exception();	// Disable copy constructor;		
	}

	RenderEngine& operator=(RenderEngine& renderEngine)	
	{
		throw Exception();	// Disable = operator
		return *this;
	}
};
