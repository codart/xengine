#pragma once

#define _MSVC

#define _X2D_NS_BEGIN namespace x2d {
#define _X2D_NS_END		}
#define _X2D  ::x2d::


#if defined(X2D_EXPORTS)
#define _X2DExport __declspec( dllexport )
#else
#define _X2DExport __declspec( dllimport )
#endif

#ifdef _MSVC
	#pragma inline_recursion(on)
	#pragma inline_depth(3)
#endif // _DEBUG



// Switch macros
//#define _MSVC
#define YIELD_CPU_TIME

#define FASTCALL __fastcall

//#define STATIC_RENDER

#ifndef STATIC_RENDER
	#define X_GAME
#endif // STATIC_RENDER

// treat Y coordinate as Z coordinate
//#define Z_POS int(pRenderData->__y + pRenderData->zOffset)
#define Z_TOP_MOST 2000000000


//----------
#if _DEBUG
#define RELEASE_CHECK(pUserData, nCheckedRefCount)		\
	if(pUserData)							\
		{										\
		int nRef = pUserData->AddRef();		\
		XASSERT((nCheckedRefCount + 2) == nRef);					\
		pUserData->Release();				\
		}
#else
#define RELEASE_CHECK(pUserData, nCheckedRefCount)
#endif


#ifdef _DEBUG
#define PROTECTED public
#else
#define PROTECTED protected
#endif // _DEBUG


// for pImpl //////////////////////////////////////////////////////////////////////////
//-------------------
enum BMPS_ALIGN
{
	H_BMPS,
	V_BMPS
};


typedef map<int, EventBinder*>::iterator ItEventBinders;