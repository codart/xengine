#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include "common/math/point.h"
#include "common/core/Object.h"
#include "common/math/Rect.h"
#include "x2d/render/DXRenderBase.h"

_X2D_NS_BEGIN

class _X2DExport MemBitmapData //: public Object
{
	friend class BitmapData;

public:
	MemBitmapData(void);
	~MemBitmapData(void);

public:
	int width;

	int height;

public:
	BOOL 
		LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor = NO_KEY_COLOR);

	BOOL 
		LoadFromResource(int resID);

	void 
		CreateBitmap(int _width, int _height, int bitsPixel);

	// Dispose bitmap data, reset all members.
	void 
		Dispose();

	//--- The flowing method only valid when the texture is in SM or AM
	// Copy bitmap bits of srcClipRect from pSrcBmpData to self's buffer	
	void 
		CopyPixels(MemBitmapData* pSrcBmpData, Rect& srcClipRect, Point& destPoint);

	// Clear with white 
	void 
		Clear();

	void 
		ClearClipRect(Rect* pClipRect);

protected:
	LPDIRECT3DTEXTURE9
		_$pTexture;
};


_X2D_NS_END