#pragma once

_X2D_NS_BEGIN


enum RENDER_TYPE
{
	RENDER_TYPE_START =0, 
	RENDER_EMPTY, 

	RENDER_BITMAP,	// A bitmap is rendered by a textured Quad
	RENDER_GRAPH,  
	RENDER_VIDEO,
	RENDER_TEXT,

	RENDER_TYPE_END,
};

class RenderState;

class _X2DExport RenderData
{
public:
	RenderData(void);
	virtual ~RenderData(void);

public:
	/*
		Note: 
		Some render data, such as BitmapData/BitmapArray, will use batch 
		render to improve performance. It means they no need implement 
		Render() method. 
	*/
	virtual int 
		Render(RenderState& renderState);

	/*
		In order to ensure data integrity, should use Swap() to create 
		RenderData instance. Reference BitmapData::LoadBitmap() for more 
		details.
	*/
	virtual void
		Swap(RenderData* pRenderData) = 0;

	virtual void
		Destroy() { XDELETE this; }


public:
	// graphic data
	DWORD 
		renderType;

};


_X2D_NS_END