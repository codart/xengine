#include "StdAfx.h"
#include "x2d/render/DXRenderBase.h"
#include "x2d/render/WindowRenderTarget.h"

#include "algorithm"

_X2D_NS_BEGIN 

LPDIRECT3D9 DXStageRenderBase::s_pD3D = NULL; 
LPDIRECT3DDEVICE9 DXStageRenderBase::s_pd3dDevice = NULL;
D3DPRESENT_PARAMETERS* DXStageRenderBase::s_pD3DPresentParam = NULL;
LPDIRECT3DINDEXBUFFER9 DXStageRenderBase::s_pQuadIndexBuffer = NULL;
LPDIRECT3DVERTEXBUFFER9 DXStageRenderBase::s_pQuadVertexBuffer= NULL;
QuadVertex* DXStageRenderBase::s_pVertexArray = NULL;

BOOL DXStageRenderBase::s_bDeviceLost = FALSE;

DXStageRenderBase::DXStageRenderBase(void)
{
	_hostWnd = NULL;

	_pWindowRenderTarget = NULL;
	__bDeviceReady = TRUE;

	__pViewMatrix = new D3DXMATRIX;
	__pProjectionMatrix =  new D3DXMATRIX;

	_pWindowRenderTarget = new WindowRenderTarget;


	_bZSort = FALSE;
	_orderedList.reserve(1000);
}

DXStageRenderBase::~DXStageRenderBase(void)
{
	SAFE_DELETE(_pWindowRenderTarget);
	SAFE_DELETE(__pViewMatrix);
	SAFE_DELETE(__pProjectionMatrix);
	//..SAFE_DELETE(_pZOrderedObjList);
}

void DXStageRenderBase::Destroy()
{
	delete this;
}

IExtensionRender* DXStageRenderBase::GetExtensionRender(const String& strRenderName)
{
	XASSERT(0);
	return NULL;
}

DWORD_PTR DXStageRenderBase::GetDevice()
{
	return (DWORD_PTR)s_pd3dDevice;
}

int DXStageRenderBase::SetHostWindow(HWND hWnd)
{
	_hostWnd = hWnd;
	return 0;
}

void DXStageRenderBase::_setRenderTarget(RenderTarget* pRenderTarget)
{
	XASSERT(pRenderTarget);
	s_pd3dDevice->SetRenderTarget(0, pRenderTarget->getRenderSurface());

	if(pRenderTarget->getDepthStencilSurface())
	{
		s_pd3dDevice->SetDepthStencilSurface(pRenderTarget->getDepthStencilSurface());
	}
	else
	{
		// Use default window render target's depth buffer.
		s_pd3dDevice->SetDepthStencilSurface(_pWindowRenderTarget->getDepthStencilSurface());
	}
}

BOOL DXStageRenderBase::BeginRender()
{
	if(!__bDeviceReady)
	{
		if(__tryRestoreDevice() != D3D_OK) return FALSE;
	}

	XASSERT(_pWindowRenderTarget);

	_setRenderTarget(_pWindowRenderTarget);

	// Begin the scene
	if( !SUCCEEDED( s_pd3dDevice->BeginScene() ) )
	{
		XTHROWEX("D3D BeginScene failed!!!");
	}

	// Set view and projection matrix
	s_pd3dDevice->SetTransform(D3DTS_PROJECTION, __pProjectionMatrix);
	s_pd3dDevice->SetTransform(D3DTS_VIEW, __pViewMatrix);

	return TRUE;
}

int DXStageRenderBase::EndRender()
{	
	// End the scene
	s_pd3dDevice->EndScene();

	HRESULT hRet = _pWindowRenderTarget->PresentToWindow();

	if(hRet == D3DERR_DEVICELOST)
	{
		_onDeviceLost();
		s_bDeviceLost = TRUE;
		__bDeviceReady = FALSE;
	}

	return 0;
}

int DXStageRenderBase::_$InitRenderEngine(HWND hWnd)
{
	SharedPtr<CWindow, false> tempWindow;

	if(NULL == hWnd)
	{
		//??? Is this suitable for all condition?
		tempWindow = new CWindow;
		RECT rect = {0, 0, 10, 10};
		tempWindow->Create(L"Button", NULL);
		hWnd = tempWindow->m_hWnd;
	}

	if(!s_pd3dDevice) __initD3D(hWnd);

	if(tempWindow) tempWindow->DestroyWindow();

	return 0;
}

int DXStageRenderBase::_$UninitRenderEngine()
{
	SAFE_RELEASE(s_pQuadVertexBuffer);
	SAFE_RELEASE(s_pQuadIndexBuffer);
	SAFE_RELEASE(s_pd3dDevice);
	SAFE_RELEASE(s_pD3D);

	if(s_pD3DPresentParam) delete s_pD3DPresentParam;

	return 0;
}

void DXStageRenderBase::_onDeviceLost()
{
	_pWindowRenderTarget->ReleaseAllSurface();

	SAFE_RELEASE(s_pQuadVertexBuffer);
	SAFE_RELEASE(s_pQuadIndexBuffer);
}

void DXStageRenderBase::_onDeviceReset()
{
	// Override by subclass

}

int DXStageRenderBase::__tryRestoreDevice()
{
	try
	{
		if(!s_bDeviceLost)
		{
			__createWindowRenderTarget(_width, _height);
			_onDeviceReset();
			__bDeviceReady = TRUE;
			return D3D_OK;
		}


		HRESULT hr = s_pd3dDevice->TestCooperativeLevel();
		if (hr == D3DERR_DEVICELOST) return D3DERR_DEVICELOST;

		if(hr == D3DERR_DEVICENOTRESET)
		{
			s_pd3dDevice->SetIndices(NULL);
			s_pd3dDevice->SetStreamSource(0, NULL, 0, 0);

			if(D3D_OK == s_pd3dDevice->Reset(s_pD3DPresentParam))
			{
				__resetSharedResource();
				__resetBasicState();
				s_bDeviceLost = FALSE;
				//__bDeviceReady = TRUE;
			}
		}
	}
	catch(Exception& e)
	{
		DbgMsgBoxA(e.what());
	}

	return D3DERR_DEVICELOST;
}

void DXStageRenderBase::__resetSharedResource()
{
	__createDefaultPoolData();

	s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));
	s_pd3dDevice->SetIndices(s_pQuadIndexBuffer);
	
	s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX);
}

void DXStageRenderBase::__resetBasicState()
{
	s_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	s_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	s_pd3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_FALSE);

	//.. No need this???
	//s_pd3dDevice->SetRenderState( D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1 );

	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	// Texture stage0 alpha blend option
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	// Frame blend option
	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

}

HRESULT DXStageRenderBase::__initD3D(HWND hWnd)
{
	try
	{
		// Create the D3D object.
		if(NULL == s_pD3D && !(s_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
		{
			XTHROWEX("Direct3DCreate9 failed");
		}

		__initWndPresentParameters(hWnd);

		// Create the D3DDevice
		if( FAILED( s_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
										D3DCREATE_SOFTWARE_VERTEXPROCESSING,
										s_pD3DPresentParam, &s_pd3dDevice ) ) )
		{
			return E_FAIL;
		}

		__resetSharedResource();
		__resetBasicState();
	}
	catch (Exception& e)
	{
		DbgMsgBoxA(e.what());
	}

	return S_OK;
}

void DXStageRenderBase::__initWndPresentParameters(HWND hWnd)
{
	if(!s_pD3DPresentParam) 
	{
		s_pD3DPresentParam = new D3DPRESENT_PARAMETERS;
		memset(s_pD3DPresentParam, 0, sizeof(D3DPRESENT_PARAMETERS));
	}

	// Use the current display mode. 
	D3DDISPLAYMODE mode;
	if(FAILED(s_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT , &mode))) 
	{
		XTHROWEX("GetAdapterDisplayMode failed!!!");	
	}

	s_pD3DPresentParam->Windowed = TRUE;
	s_pD3DPresentParam->SwapEffect = D3DSWAPEFFECT_COPY;
	s_pD3DPresentParam->BackBufferFormat = D3DFMT_UNKNOWN;
	s_pD3DPresentParam->EnableAutoDepthStencil = TRUE;
	s_pD3DPresentParam->AutoDepthStencilFormat = D3DFMT_D24S8;
	s_pD3DPresentParam->BackBufferWidth = 16;
	s_pD3DPresentParam->BackBufferHeight = 16;
	s_pD3DPresentParam->hDeviceWindow = hWnd;
	s_pD3DPresentParam->PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
}

void DXStageRenderBase::__createDefaultPoolData()
{
	if( FAILED (s_pd3dDevice->CreateVertexBuffer(4 * sizeof(QuadVertex),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		QuadVertex::FVF_QUADVERTEX,
		D3DPOOL_DEFAULT, &s_pQuadVertexBuffer
		, NULL)))
	{
		XTHROWEX("CreateVertexBuffer failed!!!");
	}

	// Create index buffer
	if( FAILED( s_pd3dDevice->CreateIndexBuffer(QUAD_COUNT_PER_DRAW*6*sizeof(WORD),
												D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY
												, D3DFMT_INDEX16, D3DPOOL_DEFAULT
												, &s_pQuadIndexBuffer, NULL ) ) )
	{
		XTHROWEX("CreateIndexBuffer failed!!!");
	}

	WORD *pIndices, n=0;
	s_pQuadIndexBuffer->Lock( 0, 0, (void**)&pIndices, 0 );
	
	for(int i=0; i<QUAD_VERTEX_BUF_SIZE/4; i++) 
	{
		/* When use triangle strip, two triangle only need 4 indices. 
		 But if need batch render Quads, need degenerate triangle, which will also need 
		 6 indices
		 */
/*
		// incorrect, need degenerate triangle between two Quad.
		*pIndices++=n;		*pIndices++=n+1;	*pIndices++=n+2;
		*pIndices++=n+3;
*/

		// When use triangle list, two triangle need 6 indices
		*pIndices++=n;		*pIndices++=n+1;	*pIndices++=n+2;
		*pIndices++=n+2;	*pIndices++=n+3;	*pIndices++=n;

		n+=4;
	}

	s_pQuadIndexBuffer->Unlock();
	s_pd3dDevice->SetIndices(s_pQuadIndexBuffer);
}

int DXStageRenderBase::CompareZ(DisplayData* p1, DisplayData* p2)
{
	if(p1->__z < p2->__z) return 1;
	return 0;
}

void DXStageRenderBase::_renderZOrderedObjList()
{
	if(_bZSort)
	{
		sort(_orderedList.begin(), _orderedList.end(), CompareZ);

		VecDisplayDataIt it = _orderedList.begin();
		for ( ; it != _orderedList.end(); ++it)
		{
			_drawPrimitive(*it);
		}

		_orderedList.clear();
	}
}

void DXStageRenderBase::__setupProjectionMatrix()
{
/*
	D3DXMatrixOrthoLH(__pProjectionMatrix, (float)_width, (float)_height, 0, 1000);
	D3DXMatrixScaling(__pViewMatrix, 1.0f, -1.0f, 1.0f);
	__pViewMatrix->_41 += -(float)_width/2;
	__pViewMatrix->_42 += (float)_height/2;*/

	/* 注意：D3DXMatrixOrthoOffCenterLH产生的矩阵做了两件事：
		1. 将-1, 1空间转换到实际屏幕空间（例如800，600）
		2. 将屏幕上下颠倒，使得远点（0，0）在屏幕左上角，并且x, y轴朝右下延伸。
	*/
	D3DXMatrixOrthoOffCenterLH(__pProjectionMatrix, 0, (float)_width, (float)_height, 0, -1.0f, 1.0f);

/*
	D3DXMATRIX tmp;
	D3DXMatrixScaling(__pProjectionMatrix, 1.0f, -1.0f, 1.0f);
	D3DXMatrixTranslation(&tmp, 0.0f, (float)_height, 0.0f);
	D3DXMatrixMultiply(__pProjectionMatrix, __pProjectionMatrix, &tmp);
	D3DXMatrixOrthoOffCenterLH(&tmp, 0, (float)_width, 0, (float)_height, 0.0f, 1000.0f);
	D3DXMatrixMultiply(__pProjectionMatrix, __pProjectionMatrix, &tmp);
*/

	D3DXMatrixIdentity(__pViewMatrix);
}

int DXStageRenderBase::__createWindowRenderTarget(int _width, int _height)
{
	if(s_bDeviceLost) return -1;

	_pWindowRenderTarget->ReleaseAllSurface();

	D3DPRESENT_PARAMETERS& d3dPresentParams = _pWindowRenderTarget->_presentParameters;
	d3dPresentParams.Windowed = TRUE;
	d3dPresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dPresentParams.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dPresentParams.EnableAutoDepthStencil = FALSE;
	d3dPresentParams.AutoDepthStencilFormat = D3DFMT_D24S8;
	d3dPresentParams.BackBufferWidth = (0 == _width)? 1:_width;
	d3dPresentParams.BackBufferHeight =  (0 == _height)? 1:_height;
	d3dPresentParams.hDeviceWindow = _hostWnd;
	d3dPresentParams.BackBufferCount = 1;
	d3dPresentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	IDirect3DSwapChain9* pSwapChain = NULL;
	HRESULT hResult = s_pd3dDevice->CreateAdditionalSwapChain(&d3dPresentParams
															, &pSwapChain);
	if(hResult == D3DERR_DEVICELOST)
	{
		XTHROWEX("CreateAdditionalSwapChain failed");
	}

	// Create depth stencil buffer
	IDirect3DSurface9* pDepthStencilSurface = NULL;
	hResult = s_pd3dDevice->CreateDepthStencilSurface( 
							// Depth stencil buffer should has the same size as back buffer
							d3dPresentParams.BackBufferWidth, 
							d3dPresentParams.BackBufferHeight, 
							D3DFMT_D24S8, 
							D3DMULTISAMPLE_NONE, 
							0, 
							TRUE,  // discard true or false?
							&pDepthStencilSurface, 
							NULL);

	if(hResult != D3D_OK)
	{
		XTHROWEX("CreateDepthStencilSurface failed");
	}

	_pWindowRenderTarget->internalInit(pSwapChain, pDepthStencilSurface);

	return 0;
}

int DXStageRenderBase::SetRenderRectSize(int width, int height)
{
	_width = width;
	_height = height;

	// Recreate swap chain
	// ???Copy from original swap chain?
	__createWindowRenderTarget(_width, _height);

	// Reset matrix
	__setupProjectionMatrix();

	return 0;
}

/*
void DXStageRenderBase::RenderDisplayList(DisplayObject* pDisplayData)
{
	_insertTreeIntoZOrderedList(pDisplayData);

	// Render ordered dirty obj list
	_renderZOrderedObjList();

	// End the scene
	s_pd3dDevice->EndScene();
}
*/

void DXStageRenderBase::RenderStage(const RenderGroup* pRenderGroup)
{
	_insertTreeIntoZOrderedList(pRenderGroup->_pOpaqueDisplayTree);

	// Render ordered dirty obj list
	_renderZOrderedObjList();

	// End the scene
	s_pd3dDevice->EndScene();
}
void DXStageRenderBase::_insertTreeIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(	!pDisplayData->_visible) return;

	if(pDisplayData->pRenderData) _insertObjIntoZOrderedList(pDisplayData);

	NodeTypePtr pCurNode = pDisplayData->pFirstChild;	
	while (pCurNode)					
	{
		_insertTreeIntoZOrderedList((DisplayObject*)pCurNode);
		pCurNode = pCurNode->pNextSibling;
	}	
}

void DXStageRenderBase::_insertObjIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(_bZSort)
	{
		_orderedList.push_back(pDisplayData);
	}
	else _drawPrimitive(pDisplayData);
}

void DXStageRenderBase::ClearBackground(COLORREF bgColor)
{
	s_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, bgColor, 1.0f, 0 );
}

void DXStageRenderBase::_drawPrimitive(DisplayData* pDisplayData)
{
	switch (pDisplayData->pRenderData->renderType)
	{
	case RENDER_BITMAP:
		_drawBitmap(pDisplayData);
		break;

	case RENDER_TEXT:
		_drawText(pDisplayData);
		break;

	//case RENDER_VECTOR:
		//..DrawVector(pDisplayData);
	//	break;
	}		
}

void DXStageRenderBase::_drawText(DisplayData* pDisplayData)
{
	IDirect3DBaseTexture9* pOldTexture = NULL;
	DWORD bAlphaEnabled = FALSE;

	//s_pd3dDevice->GetTexture(0, &pOldTexture);
	s_pd3dDevice->GetRenderState(D3DRS_ALPHABLENDENABLE, &bAlphaEnabled);

	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

	DXStageRenderBase::_drawSimpleBmp(pDisplayData);

	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, bAlphaEnabled);
	//s_pd3dDevice->SetTexture(0, pOldTexture);
}

void DXStageRenderBase::_drawBitmap(DisplayData* pDisplayData)
{
	if(pDisplayData->blendMode)
	{
		_drawBmpwithAlphaChanel(pDisplayData);
	}
	else
	{
		if(pDisplayData->GetAlpha() == 255)
		{
			_drawSimpleBmp(pDisplayData);
		}
		else
		{
			_drawAlphaBlendBmp(pDisplayData);
		}
	}
}

void DXStageRenderBase::_drawSimpleBmp(DisplayData* pDisplayData)
{
	BitmapData* pBitmapData = (BitmapData*)pDisplayData->pRenderData;

	s_pd3dDevice->SetTexture( 0, pBitmapData->_$pTexture);
	s_pQuadVertexBuffer->Lock( 0, 0, (void**)&s_pVertexArray
								, D3DLOCK_DISCARD|D3DLOCK_NOOVERWRITE);

	// Copy one Quad's vertexes to s_pVertexArray

	Quad* pQuad = pDisplayData->RetrieveQuad(pBitmapData->_textureRect, pBitmapData->_lightMapUVRect);
	memcpy(s_pVertexArray, pQuad->vertex, sizeof(QuadVertex)*Quad::VETEXT_COUNT);

	s_pQuadVertexBuffer->Unlock();

	//s_pd3dDevice->SetFVF(QuadVertex::FVF_QUADVERTEX); // Need restore FVF?
	//s_pd3dDevice->SetStreamSource(0, s_pQuadVertexBuffer, 0, sizeof(QuadVertex));

	int ret = s_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
	//int ret = s_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 4, 0, 2);
	XASSERT(SUCCEEDED(ret));
}

void DXStageRenderBase::_drawBmpwithAlphaChanel(DisplayData* pDisplayData)
{
	if(pDisplayData->GetAlpha() == 255)	// No need vertex alpha
	{
		s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
		s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	}
	else	// Vertex alpha + texture alpha
	{
		s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
		s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
	}

	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);  

	// Draw bitmap...
	_drawSimpleBmp(pDisplayData);

	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE); 
}

void DXStageRenderBase::_drawAlphaBlendBmp(DisplayData* pDisplayData)
{
	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
	s_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

	int alpha = pDisplayData->GetAlpha();
	s_pd3dDevice->SetRenderState(D3DRS_BLENDFACTOR, D3DCOLOR_XRGB(alpha, alpha, alpha));
	s_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_BLENDFACTOR);
	s_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVBLENDFACTOR); 


	// Draw bitmap...
	_drawSimpleBmp(pDisplayData);

	s_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE); 
}

LPDIRECT3DDEVICE9 DXStageRenderBase::_$GetD3DDevice()
{
	XASSERT(DXStageRenderBase::s_pd3dDevice); 
	return DXStageRenderBase::s_pd3dDevice;
}

void DXStageRenderBase::_drawVector(DisplayData* pDisplayData)
{
	Rect rect;
	/*
	pDisplayData->GetRect(rect);
	m_destDC.FillRect(&rect, (HBRUSH)::GetStockObject(GRAY_BRUSH));
	*/
}


void DXStageRenderBase::DispatchEvent(Event& event)
{
	EventDispatcher::DispatchEvent(event);
}

int DXStageRenderBase::AddEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::AddEventListener(nEventType, functor);
}

int DXStageRenderBase::RemoveEventListener(int nEventType, MemberFunctor functor)
{
	return RemoveEventListener(nEventType, functor);
}

void DXStageRenderBase::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	EventDispatcher::RemoveEvent(nEventType, bLazyRelease);
}

BOOL DXStageRenderBase::IsEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::IsEventListener(nEventType, functor);
}



_X2D_NS_END