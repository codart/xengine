#pragma once

#include "d3d9types.h"
#include "common/math/Rect.h"

_X2D_NS_BEGIN

class DisplayData;

struct UVCoordinate
{
	float	u, v;		// texture coordinates
};

struct UVRect
{
	// Should use this order if use triangle list
	UVCoordinate		tvTopLeft;
	UVCoordinate		tvTopRight;
	UVCoordinate		tvBottomRight;
	UVCoordinate		tvBottomLeft;

public:
	void BuildFromSubRect(const Rect& parent, const Rect& subRect)
	{
		BuildFromSubRect(RectF((float)parent.left, (float)parent.top, (float)parent.right, (float)parent.bottom)
						, RectF((float)subRect.left, (float)subRect.top, (float)subRect.right, (float)subRect.bottom));
	}

	void BuildFromSubRect(const RectF& parent, const RectF& subRect)
	{
		tvTopLeft.u = subRect.left/parent.Width();
		tvTopLeft.v = subRect.top/parent.Height();
		
		tvTopRight.u = (subRect.left + subRect.Width())/parent.Width();
		tvTopRight.v = tvTopLeft.v;

		tvBottomRight.u = tvTopRight.u;
		tvBottomRight.v = subRect.bottom/parent.Height();

		tvBottomLeft.u = tvTopLeft.u;
		tvBottomLeft.v = tvBottomRight.v;
	}
};

struct QuadVertex
{
	enum FVF
	{
		FVF_QUADVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEX2
	};

	float	x, y;		// screen position    
	float	z;			// Z-buffer depth 0..1
	DWORD	color;		// color of D3DRGBA(r, g, b, a)

	// texture coordinates
	UVCoordinate uv1;
	UVCoordinate uv2;
};

struct XRECT
{
	// Should use this order if use triangle list
	QuadVertex		topLeft;
	QuadVertex		topRight;
	QuadVertex		bottomRight;
	QuadVertex		bottomLeft;

/*
	// Should use this order if use triangle strip
	QuadVertex		bottomLeft;
	QuadVertex		topLeft;
	QuadVertex		bottomRight;
	QuadVertex		topRight;
*/
};

#define SIZE_OF_QUAD sizeof(QuadVertex)*Quad::VETEXT_COUNT

struct _X2DExport Quad
{
	static const int VETEXT_COUNT  = 4;
	
	//---
	union
	{
		QuadVertex vertex[4];
		XRECT xRect;
	};

public:
	// For static render only
#ifdef STATIC_RENDER
	void 
		QuadFromRenderData(DisplayData* pDisplayData);
#endif // STATIC_RENDER

	void 
		UpdatePosAndSize(DisplayData* pDisplayData);

	void 
		UpdateTextureUV(const UVRect& _textureRect, const UVRect& lightMapUVRect);
};


_X2D_NS_END