#include "StdAfx.h"
#include "MemBitmapData.h"

#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

MemBitmapData::MemBitmapData(void)
{
	width = height = 0;
	_$pTexture = NULL;
}

MemBitmapData::~MemBitmapData(void)
{
	if(_$pTexture) _$pTexture->Release();
}

BOOL MemBitmapData::LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor)
{
	if( FAILED( D3DXCreateTextureFromFileEx( GetD3DDevice(), strFileName, 
									D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, 
									1, 0, D3DFMT_UNKNOWN, D3DPOOL_SYSTEMMEM,
									D3DX_FILTER_NONE, D3DX_FILTER_NONE, 
									keyColor, NULL, NULL, &_$pTexture) ) )
	{
		XTHROW(LoadResFailedException);
	}

	D3DSURFACE_DESC TDesc;
	_$pTexture->GetLevelDesc(0, &TDesc);

	width = TDesc.Width;
	height = TDesc.Height;

	return TRUE;
}

void MemBitmapData::CreateBitmap(int _width, int _height, int bitsPixel)
{
	XASSERT(_$pTexture == NULL);
	int ret = 0;

	ret = GetD3DDevice()->CreateTexture(_width, _height
								, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM
								, &_$pTexture, NULL);

	XASSERT(SUCCEEDED(ret));

	width = _width;
	height = _height;
}

void MemBitmapData::CopyPixels(MemBitmapData* pSrcBmpData, 
								Rect& srcClipRect, 
								Point& destPoint)
{
	XASSERT(pSrcBmpData);
	XASSERT(pSrcBmpData->_$pTexture);
	XASSERT(_$pTexture);

	int ret = 0;
	
	// Lock texture to get data pointer and width byte count
	D3DLOCKED_RECT lockedRect;
	ret = pSrcBmpData->_$pTexture->LockRect(0, &lockedRect, srcClipRect, 0);
	XASSERT(SUCCEEDED(ret));

	BYTE* pSrcLinePtr = (BYTE*)lockedRect.pBits;
	int nSrcPitch = lockedRect.Pitch;

	CRect destRect(destPoint.x, destPoint.y, 
					destPoint.x + srcClipRect.Width(), 
					destPoint.y + srcClipRect.Height());
	ret = _$pTexture->LockRect(0, &lockedRect, &destRect, 0);
	XASSERT(SUCCEEDED(ret));

	BYTE* pDestLinePtr = (BYTE*)lockedRect.pBits;
	int nDestPitch = lockedRect.Pitch;

	// Copy pixels
	int height = srcClipRect.Height();
	int nWidthBytes = width * ARGB_32;

	for (int y = 0; y < height; ++y)
	{
		memcpy(pDestLinePtr, pSrcLinePtr, nWidthBytes);

		pSrcLinePtr += nSrcPitch;
		pDestLinePtr += nDestPitch;
	}

	_$pTexture->UnlockRect(0);
	pSrcBmpData->_$pTexture->UnlockRect(0);
}

void MemBitmapData::Dispose()
{
	XASSERT(_$pTexture);

	_$pTexture->Release();
	_$pTexture = NULL;
	width = height = 0;
}


_X2D_NS_END