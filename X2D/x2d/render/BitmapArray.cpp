#include "StdAfx.h"
#include "BitmapArray.h"
#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

BitmapArray::BitmapArray(void)
{
	width = 0;
	height = 0;
}

BitmapArray::~BitmapArray(void)
{
	Dispose();
}

int BitmapArray::LoadAction(LPCWSTR strFileName, int nBmpsPerAction, 
						  COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Dispose();

#ifdef STATIC_RENDER
	SharedPtr<BitmapData> _sharedBmpData = new BitmapData;
	_sharedBmpData->LoadFromBmpFile(strFileName, keyColor);
#else
	try
	{
		SharedPtr<BitmapData> largeBmpData(new BitmapData);
		largeBmpData->LoadFromBmpFile(strFileName, keyColor);
		_sharedBmpData = new BitmapData;
		_sharedBmpData->Swap(largeBmpData);
	}
	catch (LoadResFailedException& e)
	{
		DbgMsgBoxA(e.what());
	}
#endif // STATIC_RENDER

	_calculateBmpSize(1, nBmpsPerAction
					, _sharedBmpData->width, _sharedBmpData->height
					, bmpAlign);

	_loadActionBmps(*_sharedBmpData, 0, nBmpsPerAction, keyColor, bmpAlign);

	return 0;
}

int BitmapArray::LoadActions(LPCWSTR strFileName, int nActionCount, int nBmpsPerAction, 
						   COLORREF keyColor,  BMPS_ALIGN bmpAlign)
{
	Dispose();

#ifdef STATIC_RENDER
	SharedPtr<BitmapData> _sharedBmpData = new BitmapData;
	_sharedBmpData->LoadFromBmpFile(strFileName, keyColor);
#else
	try
	{
		SharedPtr<BitmapData> largeBmpData(new BitmapData);
		largeBmpData->LoadFromBmpFile(strFileName, keyColor);
		_sharedBmpData = new BitmapData;
		_sharedBmpData->Swap(largeBmpData);
	}
	catch (LoadResFailedException& e)
	{
		DbgMsgBoxA(e.what());
	}
#endif // STATIC_RENDER

	_calculateBmpSize(nActionCount, nBmpsPerAction
					, _sharedBmpData->width, _sharedBmpData->height
					, bmpAlign);

	for (int nActionIndex = 0; nActionIndex<nActionCount; ++nActionIndex)
	{
		_loadActionBmps(*_sharedBmpData, nActionIndex, nBmpsPerAction, 
			keyColor, bmpAlign);
	}

	return 0;
}

void BitmapArray::_loadActionBmps(BitmapData& largeBmpData,
							   int nActionIndex, int nBmpsPerAction, 
							   COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Action* pAction =  new Action;
	push_back(pAction);

	Rect parentRect(0, 0, largeBmpData.width, largeBmpData.height);

	for (int nBmpIndex=0; nBmpIndex<nBmpsPerAction; ++nBmpIndex)
	{
		BitmapData* pBmpData = NULL;

#ifdef STATIC_RENDER
		pBmpData = new BitmapData;
		pBmpData->CreateBitmap((int)width, (int)height, ARGB_32);
#endif

		Rect subBmpRect;
		if(H_BMPS == bmpAlign)
		{
			subBmpRect.left = (int)(nBmpIndex * width);
			subBmpRect.right = (int)(subBmpRect.left + width);
			subBmpRect.top = (int)(nActionIndex * height);
			subBmpRect.bottom = (int)(subBmpRect.top + height);
		}
		else
		{
			subBmpRect.left = (int)(nActionIndex * width);
			subBmpRect.right = (int)(subBmpRect.left + width);
			subBmpRect.top = (int)(nBmpIndex * height); 
			subBmpRect.bottom = (int)(subBmpRect.top + height); 
		}

#ifdef STATIC_RENDER
		pBmpData->CopyPixels(&largeBmpData, subBmpRect, CPoint(0, 0));
#else
		pBmpData = largeBmpData.CreateBitmapDataFromRect(subBmpRect);
#endif

		pAction->push_back(pBmpData);
	}

}

void BitmapArray::_calculateBmpSize(int nActionCount, int nBmpsPerAction, 
								 int largeBmpWidth, int LargeBmpHeight, 
								 BMPS_ALIGN bmpAlign)
{
	if (H_BMPS == bmpAlign)
	{
		width = float(largeBmpWidth / nBmpsPerAction);
		height = float(LargeBmpHeight / nActionCount);
	}
	else
	{
		width = float(largeBmpWidth / nActionCount);
		height = float(LargeBmpHeight / nBmpsPerAction);
	}
}

void BitmapArray::Dispose()
{
	Action* pAction = NULL;
	BitmapData* pBmpData = NULL;

	VecActionIt actionIt = begin();
	for ( ; actionIt != end(); ++actionIt)
	{
		pAction = *actionIt;
		XASSERT(pAction);

		ActionIt bmpDataIt = pAction->begin();
		for ( ; bmpDataIt != pAction->end(); ++bmpDataIt)
		{
			pBmpData = *bmpDataIt;
			XASSERT(pBmpData);
			delete pBmpData;
		}

		delete pAction;
	}

	width = height = 0;
}

int BitmapArray::LoadAction(int ID, int nBmpsPerAction, COLORREF keyColor, 
						  BMPS_ALIGN bmpAlign)
{
	Dispose();

/*
	CBitmap bitmap;
	bitmap.LoadBitmap(ID);
	BITMAP bmpInfo;
	bitmap.GetBitmap(&bmpInfo);

	_calculateBmpSize(1, nBmpsPerAction, bmpInfo.bmWidth, bmpInfo.bmHeight , 
		bmpAlign);

	AutoPtr<BitmapData> largeBmpData = new BitmapData;
	largeBmpData->CreateBitmap(bmpInfo.bmWidth, bmpInfo.bmHeight, RGBA_32);
	bitmap.GetBitmapBits(largeBmpData->__nBmpByteCount, largeBmpData->pBmpBuf);

	_loadActionBmps(*largeBmpData, 0, nBmpsPerAction, keyColor, bmpAlign);*/


	return 0;
}

int BitmapArray::LoadActions(int ID, int nActionCount, int nBmpsPerAction, 
						   COLORREF keyColor, BMPS_ALIGN bmpAlign)
{
	Dispose();

/*
	CBitmap bitmap;
	bitmap.LoadBitmap(ID);

	BITMAP bmpInfo;
	bitmap.GetBitmap(&bmpInfo);

	_calculateBmpSize(nActionCount, nBmpsPerAction, 
		bmpInfo.bmWidth, bmpInfo.bmHeight, bmpAlign);

	BitmapData largeBmpData;
	largeBmpData.CreateBitmap(bmpInfo.bmWidth, bmpInfo.bmHeight, 4);
	bitmap.GetBitmapBits(largeBmpData.__nBmpByteCount, largeBmpData.pBmpBuf);

	Dispose();
	for (int nActionIndex = 0; nActionIndex<nActionCount; ++nActionIndex)
	{
		_loadActionBmps(largeBmpData, nActionIndex, nBmpsPerAction, 
			keyColor, bmpAlign);
	}
*/

	return 0;
}


_X2D_NS_END