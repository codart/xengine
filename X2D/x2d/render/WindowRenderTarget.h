#pragma once

#include "RenderTarget.h"

_X2D_NS_BEGIN


class _X2DExport WindowRenderTarget : public RenderTarget
{
public:
	WindowRenderTarget();
	virtual ~WindowRenderTarget();


public:
	INLINE virtual HRESULT
		PresentToWindow()
	{
		return _pSwapChain->Present(NULL, NULL, NULL, NULL
						, D3DPRESENT_INTERVAL_IMMEDIATE);
	}

	virtual void 
		ReleaseAllSurface();


public:
	final void
		internalInit(IDirect3DSwapChain9* pSwapChain, IDirect3DSurface9* pDepthStencilSurface);


public:
	IDirect3DSwapChain9* 	
		_pSwapChain;
	D3DPRESENT_PARAMETERS	
		_presentParameters;				// Present parameters of the render window.
	BOOL					
		_acquired;						// True if resources acquired.

};



_X2D_NS_END