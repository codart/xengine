#include "StdAfx.h"
#include "BitmapData.h"

#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

BitmapData::BitmapData(void)
{
	_nTileU = _nTileV = 1;
	width = height = 0;

	_$pTexture = NULL;
	_nTextureRefCounter = 0;
	_pTextureHost = NULL;

	keyColor = NO_KEY_COLOR;
	_bSharedTexture = FALSE;

	renderType = RENDER_BITMAP;

	_textureRect.tvTopLeft.u = 0;
	_textureRect.tvTopLeft.v = 0;

	_textureRect.tvTopRight.u = 1;
	_textureRect.tvTopRight.v = 0;

	_textureRect.tvBottomLeft.u = 0;
	_textureRect.tvBottomLeft.v = 1;

	_textureRect.tvBottomRight.u = 1;
	_textureRect.tvBottomRight.v = 1;

	//---
	_lightMapUVRect.tvTopLeft.u = -0.5;
	_lightMapUVRect.tvTopLeft.v = -0.5;

	_lightMapUVRect.tvTopRight.u = 1.5;
	_lightMapUVRect.tvTopRight.v = -0.5;

	_lightMapUVRect.tvBottomLeft.u = -0.5;
	_lightMapUVRect.tvBottomLeft.v = 1.5;

	_lightMapUVRect.tvBottomRight.u = 1.5;
	_lightMapUVRect.tvBottomRight.v = 1.5;

}

BitmapData::~BitmapData(void)
{
	if(_$pTexture && !_bSharedTexture) 
	{
		XASSERT(_nTextureRefCounter == 0);
		_$pTexture->Release();
	}
	else if(_$pTexture && _bSharedTexture)
	{
		_pTextureHost->_DecreaseTextureCounter();
		_pTextureHost = NULL;
	}
}

void BitmapData::SetSize(int nWidth, int nHeight, int nTileU, int nTileV)
{
	width = nWidth;
	height = nHeight;

	_nTileU = nTileU;
	_nTileV = nTileV;

	SetUVOffset(0, 0);	
}

void BitmapData::SetUVOffset(float fUOffset, float fVOffset)
{
	_textureRect.tvTopLeft.u = fUOffset;
	_textureRect.tvTopLeft.v = fVOffset;

	_textureRect.tvTopRight.u = fUOffset + _nTileU;
	_textureRect.tvTopRight.v = fVOffset;

	_textureRect.tvBottomLeft.u = fUOffset;
	_textureRect.tvBottomLeft.v = fVOffset + _nTileV;

	_textureRect.tvBottomRight.u = fUOffset + _nTileU;
	_textureRect.tvBottomRight.v = fVOffset + _nTileV;	
}

void BitmapData::_IncreaseTextureCounter()
{
	_nTextureRefCounter++;
}

void BitmapData::_DecreaseTextureCounter()
{
	_nTextureRefCounter--;
}

BitmapData* BitmapData::CreateBitmapDataFromRect(const Rect& subBmpRect)
{
	BitmapData* pBmpData = new BitmapData;
	pBmpData->_$pTexture = _$pTexture;
	pBmpData->_pTextureHost = this;
	pBmpData->_bSharedTexture = TRUE;

	_IncreaseTextureCounter();

	Rect parentRect(0, 0, width, height);
	pBmpData->_textureRect.BuildFromSubRect(parentRect, subBmpRect);

	return pBmpData;
}

BOOL BitmapData::LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor_)
{
	LPDIRECT3DTEXTURE9 _pTexture = NULL;

	if( FAILED( D3DXCreateTextureFromFileEx( GetD3DDevice(), strFileName, 
									D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, 
									1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
									D3DX_FILTER_NONE, D3DX_FILTER_NONE, 
									keyColor_, NULL, NULL, &_pTexture) ) )
	{
		XTHROW(LoadResFailedException); 
	}

	SAFE_RELEASE(_$pTexture);

	_$pTexture = _pTexture;
	keyColor = keyColor_;

	D3DSURFACE_DESC TDesc;
	_$pTexture->GetLevelDesc(0, &TDesc);

	width = TDesc.Width;
	height = TDesc.Height;
	
	return true;
}

BOOL BitmapData::LoadFromResource(int resID, COLORREF keyColor_)
{
	keyColor = keyColor_;
	//...

	return TRUE;
}

D3DFORMAT D3DFmtFromXFmt(int pixelFomat)
{
	switch (pixelFomat)
	{
	case XRGB_32:
		return D3DFMT_X8R8G8B8;
	case ARGB_32:
		return D3DFMT_A8R8G8B8;
	}

	return D3DFMT_UNKNOWN;
}

void BitmapData::CreateBitmap(int _width, int _height, int pixelFmt)
{
	XASSERT(_$pTexture == NULL);
	int ret = 0;

	D3DFORMAT pixelFomat = D3DFmtFromXFmt(pixelFmt);
	if( FAILED(GetD3DDevice()->CreateTexture(_width, _height, 1, 0
											, pixelFomat, D3DPOOL_MANAGED
											, &_$pTexture, NULL)))
	{
		XTHROWEX("Create D3D texture failed!!!");
	}

	width = _width;
	height = _height;
}

void BitmapData::_$UpdateFromMemBmpData(MemBitmapData* pSrcBmpData)
{
	XASSERT(_$pTexture);
	int ret = 0;
	LPDIRECT3DTEXTURE9 _pSrcTexture = pSrcBmpData->_$pTexture;

	//Rect rect(32, 32, 64, 64);
	//_pSrcTexture->AddDirtyRect(rect);
	//_$pTexture->AddDirtyRect(rect);
	ret = GetD3DDevice()->UpdateTexture(_pSrcTexture, _$pTexture);
	XASSERT(SUCCEEDED(ret));
}

void BitmapData::CopyPixels(MemBitmapData* pSrcBmpData, Rect& srcClipRect, Point& destPoint)
{
	XASSERT(pSrcBmpData);
	XASSERT(pSrcBmpData->_$pTexture);
	XASSERT(_$pTexture);
	XASSERT(!_bSharedTexture);

	LPDIRECT3DTEXTURE9 pSrcTexture = pSrcBmpData->_$pTexture;

	_copyTexturePixels(pSrcTexture, srcClipRect, destPoint);
}

void BitmapData::CopyPixels(BitmapData* pSrcBmpData, Rect& srcClipRect, Point& destPoint)
{
	XASSERT(pSrcBmpData);
	XASSERT(pSrcBmpData->_$pTexture);
	XASSERT(_$pTexture);
	XASSERT(!_bSharedTexture);

	LPDIRECT3DTEXTURE9 pSrcTexture = pSrcBmpData->_$pTexture;

	_copyTexturePixels(pSrcTexture, srcClipRect, destPoint);
}

void BitmapData::_copyTexturePixels(LPDIRECT3DTEXTURE9 pSrcTexture, Rect& srcClipRect, Point& destPoint)
{
	//...Should not modify pixels if share texture enabled. 
	XASSERT(!_bSharedTexture);

	// Lock texture to get data pointer and width byte count
	D3DLOCKED_RECT lockedRect;
	int ret = pSrcTexture->LockRect(0, &lockedRect, srcClipRect, D3DLOCK_READONLY);
	XASSERT(SUCCEEDED(ret));

	BYTE* pSrcLinePtr = (BYTE*)lockedRect.pBits;
	int nSrcPitch = lockedRect.Pitch;

	Rect destRect(destPoint.x, destPoint.y, 
					destPoint.x + srcClipRect.Width(), 
					destPoint.y + srcClipRect.Height());
	ret = _$pTexture->LockRect(0, &lockedRect, destRect, 0);
	XASSERT(SUCCEEDED(ret));

	BYTE* pDestLinePtr = (BYTE*)lockedRect.pBits;
	int nDestPitch = lockedRect.Pitch;

	// Copy pixels
	int clipHeight = srcClipRect.Height();
	int clipWidth = srcClipRect.Width();
	int nWidthBytes = clipWidth * ARGB_32;

	for (int y = 0; y < clipHeight; ++y)
	{
		memcpy(pDestLinePtr, pSrcLinePtr, nWidthBytes);

		pSrcLinePtr += nSrcPitch;
		pDestLinePtr += nDestPitch;
	}

	_$pTexture->UnlockRect(0);
	pSrcTexture->UnlockRect(0);
}

// Clear with white 
void BitmapData::Clear()
{
	//...Should not modify pixels if share texture enabled. 
	XASSERT(!_bSharedTexture);
	XASSERT(_$pTexture);

	int ret = 0;
	D3DLOCKED_RECT lockedRect;
	if(FAILED(ret = _$pTexture->LockRect(0, &lockedRect, NULL, 0))) 
	{
		XASSERT(0);
		return;
	}
	
	int byteCount = lockedRect.Pitch * height;
	memset(lockedRect.pBits, 255, byteCount);
	ret = _$pTexture->UnlockRect(0);
	XASSERT(!FAILED(ret));
}

void BitmapData::ClearClipRect(Rect* pClipRect)
{
	XASSERT(!_bSharedTexture);

	D3DLOCKED_RECT lockedRect;
	if(FAILED(_$pTexture->LockRect(0, &lockedRect, (RECT*)pClipRect, 0))) 
	{
		XASSERT(0);
		//throw exception
		return;
	}

	BYTE* pLineCur = (BYTE*)lockedRect.pBits;
	BYTE* pByteCur = pLineCur;
	int pitch = lockedRect.Pitch;
	int rectHeight = pClipRect->Height();
	int widthBytes = pClipRect->Width() * ARGB_32;

	for (int y = 0 ; y < rectHeight; ++y)
	{
		memset(pLineCur, 255, widthBytes);
		pLineCur += pitch;
	}

	int ret = _$pTexture->UnlockRect(0);
	XASSERT(!FAILED(ret));
}

void BitmapData::Dispose()
{
	XASSERT(_$pTexture);

	_$pTexture->Release();
	_$pTexture = NULL;
	width = height = 0;
}

void BitmapData::Swap(RenderData* pRenderData)
{
	XASSERT(pRenderData && pRenderData->renderType == RENDER_BITMAP);

	BitmapData* pBmpData = (BitmapData*)pRenderData;
	LPDIRECT3DTEXTURE9 pTempTexture = _$pTexture;
	_$pTexture = pBmpData->_$pTexture;
	pBmpData->_$pTexture = pTempTexture;

	int tempInt = width;
	width = pBmpData->width;
	pBmpData->width = tempInt;

	tempInt = height;
	height = pBmpData->height;
	pBmpData->height = tempInt;
}


_X2D_NS_END