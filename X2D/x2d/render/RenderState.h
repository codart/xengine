#pragma once

#include "x2d/render/RenderData.h"

_X2D_NS_BEGIN

/*
	RenderState record the key information for batch rendering. If RenderState is changed, must call the 
	last render's endBatch before start a batch.
*/
class RenderState
{
public:
	void Reset()
	{
		_curRenderType	= RENDER_EMPTY;
		_pCurTexture	= NULL;
		_curBlendMode	= BLEND_NONE;
	}

public:
	RENDER_TYPE
		_curRenderType;

	DWORD_PTR
		_pCurTexture;

	BLEND_MODE
		_curBlendMode;
};


_X2D_NS_END