#pragma once

#include "x2d/render/IStageRender.h"

_X2D_NS_BEGIN

#define EMPTY_RENDER &_EmptyStageRender::s_emptyRender

class _X2DExport _EmptyStageRender : public IStageRender
{
public:
	static _EmptyStageRender
		s_emptyRender;
public:
	override void 
		InvalidateRect(Rect* pRect){};
	override int
		SetHostWindow(HWND hWnd){return 0;};
	override int 
		SetRenderRectSize(int width, int height){return 0;};
	override void 
		InvalidateObject(DisplayData* pRenderData){};

	override BOOL 
		BeginRender(){return 0;};
	//..override void 
	//	RenderDisplayList(DisplayObject* pRenderData){};
	override void 
		RenderStage(const RenderGroup* pRenderGroup){};
	override int 
		Render(){return 0;};
	override int 
		EndRender(){return 0;};
	override void 
		ClearBackground(COLORREF bgColor){};
	override DWORD_PTR 
		GetDevice(){return 0;}
	override void 
		Destroy() {};
	IExtensionRender* 
		GetExtensionRender(const String& strRenderName){XASSERT(0);return NULL;}


	override BOOL
		NeedRedraw(){return FALSE;};

	// Implement IEventDispatcher
	override void 
		DispatchEvent(Event& event){};
	override int 
		AddEventListener(int nEventType, MemberFunctor functor)
						{return 0;};
	override int 
		RemoveEventListener(int nEventType, MemberFunctor functor)
						{return 0;};
	override void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = false){};

	override BOOL 
		IsEventListener(int nEventType, MemberFunctor functor){return false;}

};


_X2D_NS_END
