#pragma once

/************************************************************************
Issues:
	Feature:
	//. Should make displayObject include DisplayData as member but not inherit
	//. The XGame should extend XEngine2D to support batch render of huge objects.
	//. Write a simple window framework instead WTL. 
		Need Set cursor to NULL in window class. 
	//. Use scan-line or agg(Anti-Grain) to optimize GDI render
	//. GDI render: One thread do blt and one thread do calculate
	//. Support Zoom out/in
	//. Support vector graphic
	//. Mouse input														/..
	//. Text filed/text input
	//. Animation														/..
	//. Rotate for DX render

	Issues:
	//. Allocate data in memory pool (STL should use user allocator)
	//. Classify the build macros such as STATIC_UI/Z_BUFFER/LOW_FRAME_RATE
	//. If SetX,Y Pos multi-times during one frame, it will display garbage when 
		use multi invalidate rect. Root cause is that the Object's dirty rect is big
		than the sum of all invalidate rects. 

	Fixed issue:
	//. Alpha blend														/
	//. Support add child to sprite										/
	//. Invalidate pRect list											/
	//. Move RenderObject into RenderEngine								/
	//. Should reference head file like this form: x2d/display/Bitmap.h; xui/controls/button.h	/

************************************************************************/
#include "memory.h"

#include "x2d/render/DXRenderBase.h"
#include "x2d/render/IStaticStageRender.h"

_X2D_NS_BEGIN

class DXStaticStageRender : public DXStageRenderBase
					 , implements IStaticStageRender
{
	DECLARE_NON_COPYABLE(DXStaticStageRender)

public:
	DXStaticStageRender(void);
	virtual ~DXStaticStageRender(void);

public:
	override void
		Destroy();
	override DWORD_PTR 
		GetDevice();

	override int
		SetHostWindow(HWND hWnd);
	override int 
		SetRenderRectSize(int width, int height);
	override void
		InvalidateRect(Rect* pRect);
	override void 
		InvalidateObject(DisplayData* pRenderData);

	override BOOL
		BeginRender();
	// Note: should call it between BeginRender()/EndRender().
	override void 
		ClearBackground(COLORREF bgColor);
	//override void 
	//	RenderDisplayList(DisplayObject* pRenderData);

	override void 
		RenderStage(const RenderGroup* pRenderGroup);

	override int
		EndRender();

	override IExtensionRender* 
		GetExtensionRender(const String& strRenderName);
	

protected:
	// Implement IEventDispatcher
	override void 
		DispatchEvent(Event& event);

	override int 
		AddEventListener(int nEventType, MemberFunctor functor);

	override int 
		RemoveEventListener(int nEventType, MemberFunctor functor);

	override void 
		RemoveEvent(int nEventType, BOOL bLazyRelease = FALSE);

	override BOOL 
		IsEventListener(int nEventType, MemberFunctor functor);

	// For static render, the swapchain's swapeffect should be D3DSWAPEFFECT_COPY
	virtual int 
		__createWindowRenderTarget(int _width, int _height){XASSERT(0); return 0; /* Implement later! */};


protected:
	Rect
		_invalidRect;
	Rect
		_invalidRectCopy;
	Rect
		renderRect;


protected:
	INLINE void 
		_insertTreeIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_insertObjIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_renderZOrderedObjList();

	// Intersect prcDest with 'invalidate rect list'. 
	INLINE BOOL 
		_intersectInvalidRects(Rect* pDestRect);
	INLINE BOOL
		_needRedraw();

protected:

	// This flag has a valid value between the BenginRender/EndRender only.
	BOOL 
		bNeedRedraw;

};


_X2D_NS_END