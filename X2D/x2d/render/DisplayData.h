#pragma once

#include "common/math/Rect.h"
#include "common/util/LinkedTreeNode.h"
#include "x2d/render/Quad.h"
#include "x2d/render/RenderData.h"

_X2D_NS_BEGIN

#define TOP_MOST 40000.0
#define BOTTOM_MOST -40000.0

enum BLEND_MODE
{
	BLEND_NONE	= 0,
	BLEND_ALPHA = 1,
	BLEND_ADD	= 2,
};

/**
	Render meta data. Render engine will use this meta data to get detailed render info(RenderData).	
*/
class _X2DExport DisplayData 
{
public:
	DisplayData(void);
	~DisplayData(void);

protected:
	virtual void _createRenderData();

	INLINE void CopyData(DisplayData* pDestData)
	{
		memcpy(pDestData, this, sizeof(DisplayData));
	}


public:
	RenderData*	
		pRenderData;

public:
	INLINE void __SetX(float newX)
	{
#ifdef X_GAME
		_bPosChanged = TRUE;
#endif // X_GAME
		__x = newX;
	}

	INLINE float __GetX()
	{
		return __x;
	}

	INLINE void __SetY(float newY)
	{
#ifdef X_GAME
		_bPosChanged = TRUE;
#endif // X_GAME
		__y = newY;
	}

	INLINE float __GetY()
	{
		return __y;
	}

	INLINE void __SetZ(float newZ)
	{
		__z = newZ;
	}

	INLINE float __GetZ()
	{
		return __z;
	}

	INLINE void __SetHotY(float newHotY)
	{
		_hotY = newHotY;
	}

	INLINE float __GetHotY()
	{
		return _hotY;
	}

	INLINE void __SetHotX(float newHotX)
	{
		_hotX = newHotX;
	}

	INLINE float __GetHotX()
	{
		return _hotX;
	}

#ifdef X_GAME 
	INLINE void __SetScaleX(float fScaleX)
	{
		_bPosChanged = TRUE;
		scaleX = fScaleX;
	}

	INLINE void __SetScaleY(float fScaleY)
	{
		_bPosChanged = TRUE;
		scaleY = fScaleY;
	}

	INLINE void __SetScale(float fScaleX, float fScaleY)
	{
		_bPosChanged = TRUE;
		scaleX = fScaleX;
		scaleY = fScaleY;
	}

	INLINE float __GetScaleX()
	{
		return scaleX;
	}

	INLINE float __GetScaleY()
	{
		return scaleY;
	}

	INLINE void __SetRotation(float fRotation)
	{
		_bPosChanged = TRUE;
		rotation = fRotation;
	}

	INLINE float __GetRotation()
	{
		return rotation;
	}

#endif

	INLINE void __SetWidth(float newWidth)
	{
#ifdef X_GAME
		_bPosChanged = TRUE;
#endif // X_GAME
		width = newWidth;
	}

	INLINE void __SetHeight(float newHeight)
	{
#ifdef X_GAME
		_bPosChanged = TRUE;
#endif // X_GAME
		height = newHeight;
	}

	INLINE void __SetAlpha(int newAlpha)
	{
		__alpha = newAlpha;

		*((BYTE*)(&_quad.vertex[0].color)+3) = newAlpha;
		*((BYTE*)(&_quad.vertex[1].color)+3) = newAlpha;
		*((BYTE*)(&_quad.vertex[2].color)+3) = newAlpha;
		*((BYTE*)(&_quad.vertex[3].color)+3) = newAlpha;

		// Should set blendMode????
		//..if(__alpha != 255) blendMode = ALPHA_BLEND;
	}


	INLINE void __SetARGB(DWORD dwColor)
	{
		__alpha = (dwColor & 0xff000000)>>24;

		_quad.vertex[0].color = dwColor;
		_quad.vertex[1].color = dwColor;
		_quad.vertex[2].color = dwColor;
		_quad.vertex[3].color = dwColor;
	}

	INLINE void __SetColor(int r, int g, int b)
	{
		// Note: Alpha is not changed
		_quad.vertex[0].color = D3DCOLOR_ARGB(*((BYTE*)(&_quad.vertex[0].color)+3), r, g, b);
		_quad.vertex[1].color = D3DCOLOR_ARGB(*((BYTE*)(&_quad.vertex[0].color)+3), r, g, b);
		_quad.vertex[2].color = D3DCOLOR_ARGB(*((BYTE*)(&_quad.vertex[0].color)+3), r, g, b);
		_quad.vertex[3].color = D3DCOLOR_ARGB(*((BYTE*)(&_quad.vertex[0].color)+3), r, g, b);
	}

	INLINE void __GetColor(int& r, int& g, int& b)
	{
		DWORD color = _quad.vertex[0].color;
		r = (color & 0x00ff0000) >> 16;
		g = (color & 0x0000ff00) >> 8;
		b = color & 0x000000ff;
	}

	INLINE int GetAlpha() const
	{
		return __alpha;
	}

	INLINE void GetRect(Rect* pRect) const
	{
		pRect->SetRect( (int)__x, (int)__y, 
			int(__x + width),  int(__y + height) );
	}

	INLINE Quad* RetrieveQuad(const UVRect& textureRect, const UVRect& lightMapUVRect);

public:
	// The absolute coordinates (Stage)
	float
		__x;	
	float
		__y;
	float
		__z;


	/* 
		Re-position, scale or rotate will all relative to the hot point.
		The hot point is relative to the __x __y, not a absolute position.
	*/
	float 
		_hotX;
	float
		_hotY;

	int
		__alpha; // 0~255

	BOOL
		_visible;

	float 
		width;
	float
		height;

	float
		scaleX;
	float 
		scaleY;

	float 
		rotation;

	BLEND_MODE
		blendMode;

	Quad 
		_quad;

	BOOL
		_bAllDirtied;
	Rect 
		_dirtyRect;

#ifdef X_GAME
	BOOL
		_bPosChanged;
	BOOL
		_bTextureUVChanged;
#endif

};


_X2D_NS_END