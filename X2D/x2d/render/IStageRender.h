#pragma once

#include "common/math/Rect.h"
#include "x2d/display/DisplayObject.h"
#include "common/core/EventDispatcher.h"
#include "x2d/render/IExtensionRender.h"
//#include "x2d/display/MaskObject.h"
#include "x2d/render/RenderGroup.h"

_X2D_NS_BEGIN


class IStageRender : public IEventDispatcher
{
public:
	virtual int
		SetHostWindow(HWND hWnd) = 0;
	virtual int 
		SetRenderRectSize(int width, int height) = 0;
	virtual BOOL 
		BeginRender() = 0;
	virtual void 
		ClearBackground(COLORREF bgColor) = 0;
	//virtual void 
	//	RenderDisplayList(DisplayObject* pRenderData) = 0;
	virtual void
		RenderStage(const RenderGroup* pRenderGroup) = 0;
	virtual int 
		EndRender() = 0;

	virtual DWORD_PTR
		GetDevice() = 0;
	virtual void
		Destroy() = 0;

	virtual IExtensionRender*
		GetExtensionRender(const String& strRenderName) = 0;

};


_X2D_NS_END