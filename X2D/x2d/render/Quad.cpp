#include "StdAfx.h"
#include "Quad.h"
#include "common/math/Rect.h"
#include "x2d/render/DisplayData.h"
#include "math.h"

_X2D_NS_BEGIN

#ifdef STATIC_RENDER
void Quad::QuadFromRenderData(DisplayData* pDisplayData)
{
/*	
	//..???
	int diffuseColor = COLOR_FROM_ALPHA(pDisplayData->__alpha);
	vertex[0].color = diffuseColor;
	vertex[1].color = diffuseColor;
	vertex[2].color = diffuseColor;
	vertex[3].color = diffuseColor;*/


	const Rect* pClipRect = &pDisplayData->_dirtyRect;
	Rect localClipRect = *pClipRect;
	// Convert to local rect
	localClipRect.OffsetRect( -(int)pDisplayData->__GetX(), -(int)pDisplayData->__GetY()); 

	float txLeft = localClipRect.left/pDisplayData->width;
	float txRight = localClipRect.right/pDisplayData->width;
	float tyTop = localClipRect.top/pDisplayData->height;
	float tyBottom = localClipRect.bottom/pDisplayData->height;

	//----
	xRect.topLeft.x = (float)pClipRect->left;
	xRect.topLeft.y = (float)pClipRect->top;
	xRect.topLeft.uv1.u = txLeft;
	xRect.topLeft.uv1.v = tyTop;

	xRect.topRight.x = (float)pClipRect->right;
	xRect.topRight.y = (float)pClipRect->top;
	xRect.topRight.uv1.u = txRight;
	xRect.topRight.uv1.v = tyTop;

	xRect.bottomLeft.x = (float)pClipRect->left;
	xRect.bottomLeft.y = (float)pClipRect->bottom;
	xRect.bottomLeft.uv1.u = txLeft;
	xRect.bottomLeft.uv1.v = tyBottom;

	xRect.bottomRight.x = (float)pClipRect->right;
	xRect.bottomRight.y = (float)pClipRect->bottom;
	xRect.bottomRight.uv1.u = txRight;
	xRect.bottomRight.uv1.v = tyBottom;
}
#endif // STATIC_RENDER

void Quad::UpdatePosAndSize(DisplayData* pDisplayData)
{
	float fRealWidth, fRealHeight; 
	float sint, cost;

#ifdef X_GAME
	fRealWidth = pDisplayData->width * pDisplayData->scaleX;
	fRealHeight = pDisplayData->height * pDisplayData->scaleY;
#else
	// static render hard to support scale and rotation now
	fRealWidth = pDisplayData->width;
	fRealHeight = pDisplayData->height;
	sint = 0;
	cost = 0;
#endif

	//xRect.topLeft.x = pDisplayData->__GetX() - (fRealWidth - pDisplayData->width)/2;
	//xRect.topLeft.y = pDisplayData->__GetY() - (fRealHeight - pDisplayData->height)/2;

	float scaledHotX = pDisplayData->__GetHotX() * pDisplayData->scaleX;
	float scaledHotY = pDisplayData->__GetHotY() * pDisplayData->scaleY;

	xRect.topLeft.x = pDisplayData->__GetX() - (scaledHotX - pDisplayData->__GetHotX());
	xRect.topLeft.y = pDisplayData->__GetY() - (scaledHotY - pDisplayData->__GetHotY());

	xRect.topRight.x = xRect.topLeft.x + fRealWidth;
	xRect.topRight.y = xRect.topLeft.y;

	xRect.bottomLeft.x = xRect.topLeft.x;
	xRect.bottomLeft.y = xRect.topLeft.y + fRealHeight;

	xRect.bottomRight.x = xRect.topRight.x;
	xRect.bottomRight.y = xRect.bottomLeft.y;

#ifdef X_GAME
	if(pDisplayData->rotation != 0)
	{
		sint = sin(pDisplayData->rotation);
		cost = cos(pDisplayData->rotation);

		// Absolute hot X/Y
		float __hotX = pDisplayData->__GetX() + pDisplayData->__GetHotX();
		float __hotY = pDisplayData->__GetY() + pDisplayData->__GetHotY();

		// Rotate around __hotX/__hotY
		XRECT srcRect = xRect;

		xRect.topLeft.x = __hotX + (srcRect.topLeft.x - __hotX) * cost - (srcRect.topLeft.y - __hotY) * sint;
		xRect.topLeft.y = __hotY + (srcRect.topLeft.y - __hotY) * cost + (srcRect.topLeft.x - __hotX) * sint;

		xRect.topRight.x = __hotX + (srcRect.topRight.x - __hotX) * cost - (srcRect.topRight.y - __hotY) * sint;
		xRect.topRight.y = __hotY + (srcRect.topRight.y - __hotY) * cost + (srcRect.topRight.x - __hotX) * sint;

		xRect.bottomLeft.x = __hotX + (srcRect.bottomLeft.x - __hotX) * cost - (srcRect.bottomLeft.y - __hotY) * sint;
		xRect.bottomLeft.y = __hotY + (srcRect.bottomLeft.y - __hotY) * cost + (srcRect.bottomLeft.x - __hotX) * sint;

		xRect.bottomRight.x = __hotX + (srcRect.bottomRight.x - __hotX) * cost - (srcRect.bottomRight.y - __hotY) * sint;
		xRect.bottomRight.y = __hotY + (srcRect.bottomRight.y - __hotY) * cost + (srcRect.bottomRight.x - __hotX) * sint;
	}
#endif // X_GAME
	// Rotation

}

void Quad::UpdateTextureUV(const UVRect& _textureRect, const UVRect& lightMapUVRect)
{
	xRect.topLeft.uv1 = _textureRect.tvTopLeft;
	xRect.topRight.uv1 = _textureRect.tvTopRight;
	xRect.bottomLeft.uv1 = _textureRect.tvBottomLeft;
	xRect.bottomRight.uv1 = _textureRect.tvBottomRight;

	xRect.topLeft.uv2 = lightMapUVRect.tvTopLeft;
	xRect.topRight.uv2 = lightMapUVRect.tvTopRight;
	xRect.bottomLeft.uv2 = lightMapUVRect.tvBottomLeft;
	xRect.bottomRight.uv2 = lightMapUVRect.tvBottomRight;
}

_X2D_NS_END