#include "stdafx.h"
#include "WindowRenderTarget.h"


_X2D_NS_BEGIN


WindowRenderTarget::WindowRenderTarget()
{
	_pSwapChain = NULL;
	memset(&_presentParameters, 0, sizeof(_presentParameters));
	_acquired = false;
}

WindowRenderTarget::~WindowRenderTarget()
{
	ReleaseAllSurface();
}

void WindowRenderTarget::ReleaseAllSurface()
{
	RELEASE_CHECK(_pRenderSurface, 0);
	SAFE_RELEASE(_pRenderSurface);
	RELEASE_CHECK(_pDepthStencilSurface, 0);
	SAFE_RELEASE(_pDepthStencilSurface);
	
	RELEASE_CHECK(_pSwapChain, 0);
	SAFE_RELEASE(_pSwapChain);
}

void WindowRenderTarget::internalInit(IDirect3DSwapChain9* pSwapChain, IDirect3DSurface9* pDepthStencilSurface)
{
	_pSwapChain = pSwapChain;

	IDirect3DSurface9* pRenderSurface = NULL;
	_pSwapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &pRenderSurface);

	RenderTarget::internalInit(pRenderSurface, pDepthStencilSurface);
}

_X2D_NS_END