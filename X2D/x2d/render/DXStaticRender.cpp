#include "StdAfx.h"
#include "x2d/render/DXStaticRender.h"

_X2D_NS_BEGIN

DXStaticStageRender::DXStaticStageRender(void)
{
	_invalidRect.SetRectEmpty();
	bNeedRedraw = FALSE;
}

DXStaticStageRender::~DXStaticStageRender(void)
{

}

void DXStaticStageRender::Destroy()
{
	DXStageRenderBase::Destroy();
}

IExtensionRender* DXStaticStageRender::GetExtensionRender(const String& strRenderName)
{
	XASSERT(0);
	return NULL;
}

DWORD_PTR DXStaticStageRender::GetDevice()
{
	return DXStageRenderBase::GetDevice();
}

int DXStaticStageRender::SetRenderRectSize(int _width, int _height)
{
	DXStageRenderBase::SetRenderRectSize(_width, _height);

	renderRect.SetRect(0, 0, _width, _height);

	_invalidRect.SetRectEmpty();
	InvalidateRect(&renderRect);

	return 0;
}

/*
void DXStaticStageRender::RenderDisplayList(DisplayObject* pDisplayData)
{
	if(!bNeedRedraw) return;

	_insertTreeIntoZOrderedList(pDisplayData);

	// Render ordered dirty obj list
	_renderZOrderedObjList();

	// End the scene
	s_pd3dDevice->EndScene();
}
*/

void DXStaticStageRender::RenderStage(const RenderGroup* pRenderGroup)
{
	if(!bNeedRedraw) return;

	_insertTreeIntoZOrderedList(pRenderGroup->_pOpaqueDisplayTree);

	// Render ordered dirty obj list
	_renderZOrderedObjList();

	// End the scene
	s_pd3dDevice->EndScene();
}

int DXStaticStageRender::SetHostWindow(HWND hWnd)
{
	DXStageRenderBase::SetHostWindow(hWnd);
	return 0;
}

void DXStaticStageRender::InvalidateRect(Rect* pRect)
{
	// Intersect with each quadrant
	_invalidRect.UnionRect(&_invalidRect, pRect);
}

void DXStaticStageRender::InvalidateObject(DisplayData* pRenderData)
{
	pRenderData->_bAllDirtied = TRUE;

	Rect rect;
	pRenderData->GetRect(&rect);

	InvalidateRect(&rect);
}

BOOL DXStaticStageRender::BeginRender()
{
	bNeedRedraw = _needRedraw();
	if(!bNeedRedraw) return TRUE;

	_invalidRectCopy = _invalidRect;
	_invalidRect.SetRectEmpty();

	return DXStageRenderBase::BeginRender();
}

void DXStaticStageRender::ClearBackground(COLORREF bgColor)
{
#ifdef _DEBUG
	static int R = 80;
	R = 255 - R;
	DWORD fillColor = D3DCOLOR_XRGB( 255, 255, 255);
#endif // _DEBUG

	if(bNeedRedraw)
	{
		s_pd3dDevice->Clear(1, (D3DRECT*)&_invalidRectCopy
			, D3DCLEAR_TARGET, bgColor, 1.0f, 0);
	}
}

int DXStaticStageRender::EndRender()
{
	if(!bNeedRedraw) return 0;

	DXStageRenderBase::EndRender();

	return 0;
}

//----------------------------
void DXStaticStageRender::_renderZOrderedObjList()
{
	VecDisplayDataIt it  = _orderedList.begin();
	for ( ; it != _orderedList.end(); ++it)
	{
		DisplayData* pRenderData = *it;
		XASSERT(pRenderData);

		if(pRenderData->_bAllDirtied)
		{
			// Prepare dirty rect
			pRenderData->GetRect(&pRenderData->_dirtyRect);
			pRenderData->_dirtyRect.IntersectRect( &pRenderData->_dirtyRect, &renderRect );

			_drawPrimitive(pRenderData);

			pRenderData->_bAllDirtied = FALSE;
		}
		else 
		{
			_drawPrimitive(pRenderData);
		}
	}

	_orderedList.clear();
}

void DXStaticStageRender::_insertTreeIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(	!pDisplayData->_visible) return;

	if(pDisplayData->pRenderData) _insertObjIntoZOrderedList(pDisplayData);

	NodeTypePtr pCurNode = pDisplayData->pFirstChild;	
	while (pCurNode)					
	{
		_insertTreeIntoZOrderedList((DisplayObject*)pCurNode);
		pCurNode = pCurNode->pNextSibling;
	}	
}

void DXStaticStageRender::_insertObjIntoZOrderedList(DisplayObject* pDisplayData)
{
	if(pDisplayData->_bAllDirtied) 
	{
		if(_bZSort)
		{
			//Insert to dirty obj map (sort by Z pos)
			_orderedList.push_back(pDisplayData);
		}
		else _drawPrimitive(pDisplayData);
	}
	else
	{
		pDisplayData->GetRect(&pDisplayData->_dirtyRect);
		if(_intersectInvalidRects(&pDisplayData->_dirtyRect))
		{
			if(_bZSort)
			{
				//Insert to dirty obj map (sort by Z pos)
				_orderedList.push_back(pDisplayData);
			}
			else _drawPrimitive(pDisplayData);
		}

	}

}

BOOL DXStaticStageRender::_needRedraw()
{
	if(!_invalidRect.IsRectEmpty()) return TRUE;
	return FALSE;
}

BOOL DXStaticStageRender::_intersectInvalidRects(Rect* pDestRect)
{
	pDestRect->IntersectRect( pDestRect, &_invalidRectCopy);
	return pDestRect->IsRectEmpty()? FALSE:TRUE;
}


void DXStaticStageRender::DispatchEvent(Event& event)
{
	EventDispatcher::DispatchEvent(event);
}

int DXStaticStageRender::AddEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::AddEventListener(nEventType, functor);
}

int DXStaticStageRender::RemoveEventListener(int nEventType, MemberFunctor functor)
{
	return RemoveEventListener(nEventType, functor);
}

void DXStaticStageRender::RemoveEvent(int nEventType, BOOL bLazyRelease)
{
	EventDispatcher::RemoveEvent(nEventType, bLazyRelease);
}

BOOL DXStaticStageRender::IsEventListener(int nEventType, MemberFunctor functor)
{
	return EventDispatcher::IsEventListener(nEventType, functor);
}

_X2D_NS_END