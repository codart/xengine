#pragma once

#include "common/core/EventDispatcher.h"
#include "x2d/render/IStageRender.h"

_X2D_NS_BEGIN

class IStageRender;

class IExtensionRender : public IEventDispatcher
{
public:
	virtual void
		Init(IStageRender* pStageRender) = 0;
	virtual void
		Destroy() = 0;
	virtual wstring
		GetRenderName() = 0;

};


_X2D_NS_END