#pragma once

#include "x2d/display/DisplayObject.h"

_X2D_NS_BEGIN

class RenderGroup
{
public:
	DisplayObject*
		_pOpaqueDisplayTree;

	RefCountPtr<DisplayObject>
		_pMaskObject;

	RefCountPtr<DisplayObject>
		_pLightedBmp;
	RefCountPtr<DisplayObject>
		_pLightMap;


protected:
private:
};


_X2D_NS_END