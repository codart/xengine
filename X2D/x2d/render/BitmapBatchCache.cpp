#include "StdAfx.h"
#include "BitmapBatchCache.h"

#include "x2d/error/LoadResFailedException.h"

_X2D_NS_BEGIN

BitmapBatchCache::BitmapBatchCache(void)
{
	width = height = 0;
	_$pTexture = NULL;
	keyColor = NO_KEY_COLOR;

	renderType = RENDER_BITMAP;
}

BitmapBatchCache::~BitmapBatchCache(void)
{
	if(_$pTexture) _$pTexture->Release();
}

BOOL BitmapBatchCache::LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor_)
{
	LPDIRECT3DTEXTURE9 _pTexture = NULL;

	if( FAILED( D3DXCreateTextureFromFileEx( GetD3DDevice(), strFileName, 
									D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, 
									1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED,
									D3DX_FILTER_POINT, D3DX_FILTER_POINT, 
									keyColor, NULL, NULL, &_pTexture) ) )
	{
		throw LoadResFailedException();
	}

	SAFE_RELEASE(_$pTexture);

	_$pTexture = _pTexture;
	keyColor = keyColor_;

	D3DSURFACE_DESC TDesc;
	_$pTexture->GetLevelDesc(0, &TDesc);

	width = TDesc.Width;
	height = TDesc.Height;
	
	return TRUE;
}

BOOL BitmapBatchCache::LoadFromResource(int resID, COLORREF keyColor_)
{
	keyColor = keyColor_;
	//...

	return TRUE;
}

D3DFORMAT D3DFmtFromXFmt(int pixelFomat)
{
	switch (pixelFomat)
	{
	case XRGB_32:
		return D3DFMT_X8R8G8B8;
	case ARGB_32:
		return D3DFMT_A8R8G8B8;
	}

	return D3DFMT_UNKNOWN;
}

void BitmapBatchCache::CreateBitmap(int _width, int _height, int pixelFmt)
{
	XASSERT(_$pTexture == NULL);
	int ret = 0;

	D3DFORMAT pixelFomat = D3DFmtFromXFmt(pixelFmt);
	if( FAILED(GetD3DDevice()->CreateTexture(_width, _height, 1, 0
											, pixelFomat, D3DPOOL_MANAGED
											, &_$pTexture, NULL)))
	{
		throw Exception();
	}

	width = _width;
	height = _height;
}

void BitmapBatchCache::_$UpdateFromMemBmpData(MemBitmapData* pSrcBmpData)
{
	XASSERT(_$pTexture);
	int ret = 0;
	LPDIRECT3DTEXTURE9 _pSrcTexture = pSrcBmpData->_$pTexture;

	//Rect rect(32, 32, 64, 64);
	//_pSrcTexture->AddDirtyRect(rect);
	//_$pTexture->AddDirtyRect(rect);
	ret = GetD3DDevice()->UpdateTexture(_pSrcTexture, _$pTexture);
	XASSERT(SUCCEEDED(ret));
}

void BitmapBatchCache::CopyPixels(MemBitmapData* pSrcBmpData, Rect& srcClipRect, CPoint& destPoint)
{
	XASSERT(pSrcBmpData);
	XASSERT(pSrcBmpData->_$pTexture);
	XASSERT(_$pTexture);

	LPDIRECT3DTEXTURE9 pSrcTexture = pSrcBmpData->_$pTexture;

	_copyTexturePixels(pSrcTexture, srcClipRect, destPoint);
}

void BitmapBatchCache::CopyPixels(BitmapData* pSrcBmpData, Rect& srcClipRect, CPoint& destPoint)
{
	XASSERT(pSrcBmpData);
	XASSERT(pSrcBmpData->_$pTexture);
	XASSERT(_$pTexture);

	LPDIRECT3DTEXTURE9 pSrcTexture = pSrcBmpData->_$pTexture;

	_copyTexturePixels(pSrcTexture, srcClipRect, destPoint);
}

void BitmapBatchCache::_copyTexturePixels(LPDIRECT3DTEXTURE9 pSrcTexture, Rect& srcClipRect, CPoint& destPoint)
{
	// Lock texture to get data pointer and width byte count
	D3DLOCKED_RECT lockedRect;
	int ret = pSrcTexture->LockRect(0, &lockedRect, srcClipRect, D3DLOCK_READONLY);
	XASSERT(SUCCEEDED(ret));

	BYTE* pSrcLinePtr = (BYTE*)lockedRect.pBits;
	int nSrcPitch = lockedRect.Pitch;

	Rect destRect(destPoint.x, destPoint.y, 
					destPoint.x + srcClipRect.Width(), 
					destPoint.y + srcClipRect.Height());
	ret = _$pTexture->LockRect(0, &lockedRect, destRect, 0);
	XASSERT(SUCCEEDED(ret));

	BYTE* pDestLinePtr = (BYTE*)lockedRect.pBits;
	int nDestPitch = lockedRect.Pitch;

	// Copy pixels
	int clipHeight = srcClipRect.Height();
	int clipWidth = srcClipRect.Width();
	int nWidthBytes = clipWidth * ARGB_32;

	for (int y = 0; y < clipHeight; ++y)
	{
		memcpy(pDestLinePtr, pSrcLinePtr, nWidthBytes);

		pSrcLinePtr += nSrcPitch;
		pDestLinePtr += nDestPitch;
	}

	_$pTexture->UnlockRect(0);
	pSrcTexture->UnlockRect(0);
}

// Clear with white 
void BitmapBatchCache::Clear()
{
	XASSERT(_$pTexture);	

	int ret = 0;
	D3DLOCKED_RECT lockedRect;
	if(FAILED(ret = _$pTexture->LockRect(0, &lockedRect, NULL, 0))) 
	{
		XASSERT(0);
		return;
	}
	
	int byteCount = lockedRect.Pitch * height;
	memset(lockedRect.pBits, 255, byteCount);
	ret = _$pTexture->UnlockRect(0);
	XASSERT(!FAILED(ret));
}

void BitmapBatchCache::ClearClipRect(CRect* pClipRect)
{
	int ret = 0;
	D3DLOCKED_RECT lockedRect;
	if(FAILED(ret = _$pTexture->LockRect(0, &lockedRect, pClipRect, 0))) 
	{
		XASSERT(0);
		//throw exception
		return;
	}

	BYTE* pLineCur = (BYTE*)lockedRect.pBits;
	BYTE* pByteCur = pLineCur;
	int pitch = lockedRect.Pitch;
	int rectHeight = pClipRect->Height();
	int widthBytes = pClipRect->Width() * ARGB_32;

	for (int y = 0 ; y < rectHeight; ++y)
	{
		memset(pLineCur, 255, widthBytes);
		pLineCur += pitch;
	}

	ret = _$pTexture->UnlockRect(0);
	XASSERT(!FAILED(ret));
}

void BitmapBatchCache::Dispose()
{
	XASSERT(_$pTexture);

	_$pTexture->Release();
	_$pTexture = NULL;
	width = height = 0;
}

void BitmapBatchCache::Swap(BitmapData* bmpData)
{
	LPDIRECT3DTEXTURE9 pTempTexture = _$pTexture;
	_$pTexture = bmpData->_$pTexture;
	bmpData->_$pTexture = pTempTexture;

	int tempInt = width;
	width = bmpData->width;
	bmpData->width = tempInt;

	tempInt = height;
	height = bmpData->height;
	bmpData->height = tempInt;
	
}


_X2D_NS_END