#pragma once

#include "common/math/Rect.h"
#include "x2d/render/DisplayData.h"
#include "x2d//render/IStageRender.h"

_X2D_NS_BEGIN

class IStaticStageRender : public IStageRender
{
public:
	virtual void 
		InvalidateRect(Rect* pRect) = 0;
	virtual void 
		InvalidateObject(DisplayData* pRenderData) = 0;
};


_X2D_NS_END