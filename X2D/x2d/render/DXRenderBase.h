#pragma once

#include "memory.h"

#ifdef _DEBUG
	#pragma comment(lib,"d3dx9d.lib") 
#else
	#pragma comment(lib,"d3dx9.lib") 
#endif // _DEBUG

#pragma comment(lib, "d3d9.lib") 

#include <d3d9.h>
#include <d3dx9.h>

#include "common/core/NonCopyble.h"
#include "common/core/Exception.h"
#include "X2d/render/DisplayData.h"
#include "x2d/render/IStageRender.h"
#include "x2d/render/Quad.h"
#include "x2d/render/BitmapData.h"

_X2D_NS_BEGIN

#define GetD3DDevice() DXStageRenderBase::_$GetD3DDevice()
static const int QUAD_COUNT_PER_DRAW	= 1000;
static const int QUAD_VERTEX_BUF_SIZE	= QUAD_COUNT_PER_DRAW * 4;


/*
	StageRender should be an abstract layer. It encapsulate the common render logic. So the class hierarchy should 
	be like below:

		_Device
		/		\
	_D3DDevice	_OpenGLDevice

	---------------	Above is Graphics API abstract layer, below is the RenderEngine layer

						IStageRender	
							|		 \					\
						StageRender	  IStaticRender		IStage3DRender
							|			\					\
	X2DStageRender	X2DAdvStageRender	StaticStageRender	Stage3DRender

	Implement this design after I have a implement the basic feature of OpenGL render.

*/

class RenderTarget;
class WindowRenderTarget;

/*
	Base class for D3D9 render
*/
class DXStageRenderBase : implements IStageRender
					, public EventDispatcher
{
	DECLARE_NON_COPYABLE(DXStageRenderBase);

	friend class BitmapData;
	friend class MemBitmapData;
	friend class BitmapArray;
	friend class X2DManager;

public:
	_X2DExport DXStageRenderBase(void);
	_X2DExport virtual ~DXStageRenderBase(void);

public:
	// Implement IStageRender
	_X2DExport override int
		SetHostWindow(HWND hWnd);
	_X2DExport override int 
		SetRenderRectSize(int width, int height);

	_X2DExport override BOOL
		BeginRender();
	// Note: should call it between BeginRender()/EndRender().
	_X2DExport override void 
		ClearBackground(COLORREF bgColor);
	//.._X2DExport override void 
	//	RenderDisplayList(DisplayObject* pRenderData);
	_X2DExport override void
		RenderStage(const RenderGroup* pRenderGroup);
	_X2DExport override int
		EndRender();

	_X2DExport override DWORD_PTR
		GetDevice();
	_X2DExport override void
		Destroy();

	_X2DExport override IExtensionRender*
		GetExtensionRender(const String& strRenderName);


	//... This method need remove to GraphicInterface later!!!
	_X2DExport void 
		_setRenderTarget(RenderTarget* pRenderTarget);


protected:
	_X2DExport virtual void
		_onDeviceLost();

	_X2DExport virtual void
		_onDeviceReset();


protected:
	// Implement IEventDispatcher
	override void DispatchEvent(Event& event);

	override int AddEventListener(int nEventType, MemberFunctor functor);

	override int RemoveEventListener(int nEventType, MemberFunctor functor);

	override void RemoveEvent(int nEventType, BOOL bLazyRelease = FALSE);

	override BOOL IsEventListener(int nEventType, MemberFunctor functor);

protected:
	WindowRenderTarget*
		_pWindowRenderTarget;

	int 
		_width;
	int 
		_height;

	HWND
		_hostWnd;

	BOOL 
		_bZSort;

	typedef vector<DisplayData*>::iterator VecDisplayDataIt;

	vector<DisplayData*>
		_orderedList;

protected:
	INLINE void 
		_insertTreeIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_insertObjIntoZOrderedList(DisplayObject* pRenderData);
	INLINE void 
		_renderZOrderedObjList();


	INLINE void
		_drawPrimitive(DisplayData* pRenderData);
	_X2DExport INLINE void 
		_drawText(DisplayData* pDisplayData);
	_X2DExport INLINE void 
		_drawBitmap(DisplayData* pRenderMetaData);
	// Draw simple bitmap with clip rect
	_X2DExport INLINE void
		_drawSimpleBmp(DisplayData* pDisplayData);
	INLINE void 
		_drawBmpwithAlphaChanel(DisplayData* pRenderData);
	INLINE void
		_drawAlphaBlendBmp(DisplayData* pRenderData);

	INLINE void
		_drawVector(DisplayData* pRenderData);


protected:
	// The init\unint engine will be call internally only
	static int 
		_$InitRenderEngine(HWND hWnd);
	static int 
		_$UninitRenderEngine();

public:
	_X2DExport static LPDIRECT3DDEVICE9 
		_$GetD3DDevice();


protected:
	static HRESULT 
		__initD3D(HWND hWnd);
	static void
		__resetSharedResource();
	static void
		__resetBasicState();
	static void
		__createDefaultPoolData();
	static void
		__initWndPresentParameters(HWND hWnd);

	_X2DExport static int 
		CompareZ(DisplayData* p1, DisplayData* p2);


protected:
	_X2DExport int
		__tryRestoreDevice();
	_X2DExport virtual int 
		__createWindowRenderTarget(int _width, int _height);
	void 
		__setupProjectionMatrix();
	

protected:
	_X2DExport static LPDIRECT3D9             
		s_pD3D; // Used to create the D3DDevice

	_X2DExport static LPDIRECT3DINDEXBUFFER9	// Shared index buffer
		s_pQuadIndexBuffer; 

	_X2DExport static LPDIRECT3DVERTEXBUFFER9
		s_pQuadVertexBuffer;

	_X2DExport static QuadVertex*			
		s_pVertexArray;	// Note: This vertex buffer is shared by all stage now

	_X2DExport static LPDIRECT3DDEVICE9
		s_pd3dDevice; // Our rendering device

	_X2DExport static D3DPRESENT_PARAMETERS*
		s_pD3DPresentParam; 

	_X2DExport static BOOL
		s_bDeviceLost;


protected:
	//^^^^IDirect3DSwapChain9*
	//	__pSwapChain;

	int
		__bDeviceReady;
	D3DXMATRIX*
		__pViewMatrix;
	D3DXMATRIX*
		__pProjectionMatrix;

	D3DVIEWPORT9 
		__viewPort;

};


_X2D_NS_END