
#pragma once

#include "common/math/point.h"
#include "MemBitmapData.h"
#include "RenderData.h"

_X2D_NS_BEGIN

/*
	There are two type of BitmapData for DX render. 

	1. One is BitmapData, which has the same interface with GDI render's BitmapData. 
	2. Another is MemBitmapData, witch represent the texture that is allocated in 
	D3DPOOL_SYSTEMMEM. This type of texture cannot be used for render. They can be 
	copied to a D3DPOOL_DEFAULT texture, and then render that texture to screen.

	May be we can make all the bitmap data class implement same IBitmapData interface 
	later.
*/
class _X2DExport BitmapData : public RenderData
{
	friend class DXStageRenderBase;
	friend class MemBitmapData;
	friend class BitmapArray;

public:
	BitmapData(void);
	virtual ~BitmapData(void);

public:
	int width;

	int height;


	int 
		_nTextureRefCounter;
	LPDIRECT3DTEXTURE9
		_$pTexture;

public:
	BOOL 
		LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor = NO_KEY_COLOR);

	BOOL 
		LoadFromResource(int resID, COLORREF keyColor_);

	void 
		CreateBitmap(int _width, int _height, int pixelFmt = XRGB_32);

	// Dispose bitmap data, reset all members.
	void 
		Dispose();

	// Copy bitmap bits of srcClipRect from pSrcBmpData to self's buffer	
	void
		CopyPixels(MemBitmapData* pSrcBmpData, Rect& srcClipRect, Point& destPoint);

	void
		CopyPixels(BitmapData* pSrcBmpData, Rect& srcClipRect, Point& destPoint);

	// Clear with white 
	void 
		Clear();

	void 
		ClearClipRect(Rect* pClipRect);

	void 
		SetSize(int nWidth, int nHeight, int nTileU, int nTileV);

	void 
		SetUVOffset(float fUOffset, float fVOffset);

	BitmapData* 
		CreateBitmapDataFromRect(const Rect& subBmpRect);


public:
	// Implement RenderData's interface
	override void
		Swap(RenderData* pRenderData);


	void 
		_IncreaseTextureCounter();

	void 
		_DecreaseTextureCounter();


protected:
	void _copyTexturePixels(LPDIRECT3DTEXTURE9 pSrcTexture
							, Rect& srcClipRect, Point& destPoint);

	void 
		_$UpdateFromMemBmpData(MemBitmapData* pSrcBmpData);


protected:
	COLOR
		keyColor;

	int 
		_nTileU;
	int
		_nTileV;

	/**
		In order to use batch render, introduced shared texture mechanism. 

		TRUE: This BitmapData shares same texture with others with different texture
		coordinates. 
		
		FALSE: Not shares texture with others.

		The shared texture will be managed by BitmapBatchCache.
	*/

public:
	BOOL 
		_bSharedTexture;

	BitmapData*
		_pTextureHost;

	/**
		When _bSharedTexture is enabled, should use texture rect to render sub bitmap.
	*/
	UVRect
		_textureRect;

	/**
		Light Map UV 
	*/
	UVRect
		_lightMapUVRect;

};

_X2D_NS_END