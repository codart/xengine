#include "stdafx.h"
#include "RenderTarget.h"

_X2D_NS_BEGIN

RenderTarget::RenderTarget()  
{
	_pRenderSurface = NULL;
	_pDepthStencilSurface = NULL;

	memset(&_surfaceDesc, 0, sizeof(_surfaceDesc));
}

RenderTarget::~RenderTarget()
{
	SAFE_RELEASE(_pRenderSurface);

	RELEASE_CHECK(_pDepthStencilSurface, 0);
	SAFE_RELEASE(_pDepthStencilSurface);
}

void RenderTarget::internalInit(IDirect3DSurface9* pRenderSurface, IDirect3DSurface9* pDepthStencilSurface)
{
	_pRenderSurface = pRenderSurface;
	//if(_pRenderSurface) _pRenderSurface->AddRef();

	_pDepthStencilSurface = pDepthStencilSurface;

	if(pRenderSurface)
	{
		pRenderSurface->GetDesc(&_surfaceDesc);
	}
}

/*
void RenderTarget::Detach()
{
	SAFE_RELEASE(_pRenderSurface);
	SAFE_RELEASE(_pDepthStencilSurface);

	memset(&_surfaceDesc, 0, sizeof(_surfaceDesc));
}
*/

int RenderTarget::GetWidth()
{
	return _surfaceDesc.Width;
}

int RenderTarget::GetHeight()
{
	return _surfaceDesc.Height;
}

_X2D_NS_END