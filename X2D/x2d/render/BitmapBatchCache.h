
#pragma once

#include "MemBitmapData.h"
#include "RenderData.h"

_X2D_NS_BEGIN

/*
	There are two type of BitmapData for DX render. 

	1. One is BitmapData, which has the same interface with GDI render's BitmapData. 
	2. Another is MemBitmapData, witch represent the texture that is allocated in 
	D3DPOOL_SYSTEMMEM. This type of texture cannot be used for render. They can be 
	updated to a D3DPOOL_DEFAULT texture, and then render that texture to screen.

	May be we can make all the bitmap data class implement same IBitmapData interface 
	later.
*/
class _X2DExport BitmapBatchCache : public RenderData
{
	friend class DXRenderBase;
	friend class MemBitmapData;
	friend class BitmapArray;

public:
	BitmapBatchCache(void);
	virtual ~BitmapBatchCache(void);

public:
	int width;

	int height;

	LPDIRECT3DTEXTURE9
		_$pTexture;

public:
	BOOL 
		LoadFromBmpFile(LPCWSTR strFileName, COLORREF keyColor = NO_KEY_COLOR);

	BOOL 
		LoadFromResource(int resID, COLORREF keyColor_);

	void 
		CreateBitmap(int _width, int _height, int pixelFmt = XRGB_32);

	// Dispose bitmap data, reset all members.
	void 
		Dispose();

	//--- Flowing methods only valid when the texture is in SM or AM
	// Copy bitmap bits of srcClipRect from pSrcBmpData to self's buffer	
	void
		CopyPixels(MemBitmapData* pSrcBmpData, Rect& srcClipRect, CPoint& destPoint);

	void
		CopyPixels(BitmapData* pSrcBmpData, Rect& srcClipRect, CPoint& destPoint);

	// Clear with white 
	void 
		Clear();

	void 
		ClearClipRect(CRect* pClipRect);

	void
		Swap(BitmapData* bmpData);


PROTECTED:
	void _copyTexturePixels(LPDIRECT3DTEXTURE9 pSrcTexture
							, Rect& srcClipRect, CPoint& destPoint);

PROTECTED:
	void 
		_$UpdateFromMemBmpData(MemBitmapData* pSrcBmpData);

protected:
	COLOR
		keyColor;
};

_X2D_NS_END