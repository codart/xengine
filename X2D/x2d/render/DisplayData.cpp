#include "StdAfx.h"
#include "DisplayData.h"

_X2D_NS_BEGIN

DisplayData::DisplayData(void)
{
	_visible = TRUE;

	__SetX(0);
	__SetY(0);
	__SetZ(0);

	width = height = 0;
	_hotX = 0;
	_hotY = 0;

	scaleX = 1;
	scaleY = 1;;
	rotation = 0;

	pRenderData = NULL;

	blendMode = BLEND_NONE;
	__SetAlpha(255);

	_bAllDirtied = FALSE;
	_bPosChanged = FALSE;
	_bTextureUVChanged = TRUE;

	//------
	memset(&_quad, 0, sizeof(_quad));

	// Set diffuse color to white, alpha to 255
	_quad.vertex[0].color = 0xffffffff; // ARGB
	_quad.vertex[1].color = 0xffffffff; 
	_quad.vertex[2].color = 0xffffffff; 
	_quad.vertex[3].color = 0xffffffff;
}

DisplayData::~DisplayData(void)
{
	width = height = 0;
	pRenderData = NULL;

#ifndef STATIC_RENDER
	memset(&_quad, 0, sizeof(_quad));
#endif
}

void DisplayData::_createRenderData()
{
	XTHROW(NoImplementationException);
}

Quad* DisplayData::RetrieveQuad(const UVRect& textureRect, const UVRect& lightMapUVRect)
{
#ifdef STATIC_RENDER
	_quad.QuadFromRenderData(this);
#else
	if(_bPosChanged)
	{
		_quad.UpdatePosAndSize((DisplayData*)this);
		_bPosChanged = FALSE;
	}

	if(_bTextureUVChanged)
	{
		_quad.UpdateTextureUV(textureRect, lightMapUVRect);
		_bTextureUVChanged = FALSE;
	}
#endif // STATIC_RENDER
	return &_quad;
}

_X2D_NS_END