#pragma once

#include "d3d9.h"


_X2D_NS_BEGIN

class GraphicBuffer
{
public:

protected:
	//..GraphicInterface*
	//..	_pDevice;

private:
};

/** 
	A 'canvas' which can receive the results of a rendering
    operation.

    @remarks
 */

class DXStageRenderBase;

class _X2DExport RenderTarget : public Object
							, public ReferenceCounted
{
	friend class DXStageRenderBase;

public:
	RenderTarget();
	virtual ~RenderTarget();

public:
	virtual int 
		GetWidth();
	virtual int
		GetHeight();

	//----------------
	INLINE void
		internalInit(IDirect3DSurface9* pRenderSurface, IDirect3DSurface9* pDepthStencilSurface);


	virtual IDirect3DSurface9* 
		getRenderSurface(){return _pRenderSurface;};
	INLINE IDirect3DSurface9*
		getDepthStencilSurface(){return _pDepthStencilSurface;};


protected:
	IDirect3DSurface9* 
		_pRenderSurface;
	IDirect3DSurface9*	 	
		_pDepthStencilSurface;

	D3DSURFACE_DESC
		_surfaceDesc;

};

_X2D_NS_END