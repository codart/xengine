#include "StdAfx.h"
#include "x2d/render/RenderData.h"


_X2D_NS_BEGIN

RenderData::RenderData(void)
{
	renderType = RENDER_EMPTY;
}

RenderData::~RenderData(void)
{
	renderType = RENDER_EMPTY;
}

int RenderData::Render(RenderState& renderState)
{
	XTHROW(NoImplementationException);
}

_X2D_NS_END