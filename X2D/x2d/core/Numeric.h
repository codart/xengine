#pragma once

_X2D_NS_BEGIN


enum NumericType
{
	NUMERIC_EMPTY,
	NUMERIC_INT,
	NUMERIC_FLOAT,
	NUMERIC_BYTE,
	NUMERIC_DOUBLE,

};

class _X2DExport Numeric
{
public:
	INLINE Numeric()
	{
		_init();
	}

	INLINE Numeric& operator= (const Numeric& srcValue)
	{
		type = srcValue.type;
		switch (type)
		{
		case NUMERIC_FLOAT:
			floatValue = srcValue.floatValue;
			break;

		case NUMERIC_INT:
			intValue = srcValue.intValue;
			break;

		default:
			XASSERT(0);
		}

		return *this;
	}

	// FLOAT
	INLINE Numeric(FLOAT fValue)
	{
		*this = fValue;
	}

	INLINE Numeric& operator= (FLOAT fValue)
	{
		type = NUMERIC_FLOAT;
		floatValue = fValue;
		return *this;
	}

	INLINE operator FLOAT()
	{
		if(type != NUMERIC_FLOAT)
		{
			switch (type)
			{
			case NUMERIC_INT:
				return (FLOAT)intValue;
				break;

			default:
				XASSERT(0);
				return 0;
			}
		}
		else return floatValue;
	}


	// INT
	INLINE Numeric(INT fValue)
	{
		*this = fValue;
	}

	INLINE Numeric& operator= (INT iValue)
	{
		type = NUMERIC_INT;
		intValue = iValue;
		return *this;
	}

	INLINE operator INT()
	{
		if(type != NUMERIC_INT)
		{
			switch (type)
			{
			case NUMERIC_FLOAT:
				return (INT)floatValue;
				break;

			default:
				XASSERT(0);
				return 0;
			}
		}
		else return intValue;
	}

protected:
	NumericType type;

	union 
	{
		BYTE byteValue;
		//SHORT shortValue;
		INT intValue;		
		FLOAT floatValue;
		//DOUBLE doubleValue;
		//LONGLONG longlongValue;
	};


protected:
	INLINE void _init()
	{
		type = NUMERIC_EMPTY;
	}

	INLINE void _changeType(NumericType destType)
	{
		switch (destType)
		{
		case NUMERIC_FLOAT:
			switch (type)
			{
			case NUMERIC_INT:
				floatValue = (FLOAT)intValue;
				break;

			default:
				XASSERT(0);
			}
			break;

		case NUMERIC_INT:
			switch (type)
			{
			case NUMERIC_FLOAT:
				intValue = (INT)floatValue;
				break;

			default:
				XASSERT(0);
			}
			break;

		default:
			XASSERT(0);
		}
		type = destType;
	}

};

_X2D_NS_END